<?php
class geocellid {
// CLASS VARIABLES
	private $db_Link;
	private $id;
	private $number;
	private $status;
	private $sms;
	
// INITIALISATION
	public function __construct(){		
	}

	public function geocellid(){		
	}

	public function getid() {
		return $this->id;
	}

	public function getnumber() {
		return $this->number;
	}

	public function checkstatus($token) {
		return getstatus($token);
	}
	
	public function getstatus($token) {
		$request = db_fetch("select * from site_users where token='".$token."'");
		$currentdate = date("Y-m-d H:i:s");
		$cdate = strtotime($currentdate);
		$sdate = strtotime($request["lastdate"]);
		if($cdate-$sdate < 1800) {
			db_query("update site_users set lastdate='".$currentdate."' where token='".$token."'");
			return 1;
		} else {
			return -1;
		}
	}

	public function gettoken() {
		$headers = apache_request_headers();
		$token = "";
		foreach ($headers as $key => $value) {
		    if(strtolower(substr($key,0,5)) == "token")
		    	$token = $value;
		}
		if($token == "") {
			$token = isset($_GET["token"]) ? $_GET["token"] : "";
		}
		return $token;
	}

	public function smscoderequest($number) {
		if($this->checknumber($number)==true) {
			if(substr($number,0,3) !='995') 
				$number ='995' . $number;
			$cnt = db_fetch("select count(*) as cnt from site_users where mobile='".$number."'");
			if($cnt["cnt"]==0) {
				db_query("insert into site_users (mobile, smstime) values ('".$number."', '1970-01-01 00:00:00')");
			}
			$user = db_fetch("select * from site_users where mobile='".$number."'");
			$datetime1 = strtotime($user["smstime"]);
			$datetime2 = strtotime(date("Y-m-d H:i:s"));  // (date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"));
			$interval = abs($datetime2-$datetime1);
//			echo $datetime1."<br />";
//			echo $datetime2."<br />";
//			echo $interval; die();
			if($interval<180) {
				return -2;
			}
			$this->sms = $this->generatesms();
			$this->smssend($number, $this->sms);
			$this->number = $number;
			db_query("update site_users set sms='".$this->sms."', smstime='".date("Y-m-d H:i:s")."', status=1 where mobile='".$this->number."'");
//			die();
			return $this->sms;
		} else {
			return -1;
		}
	}

	public function portalsmscoderequest($number) {
		if($this->checknumber($number)==true) {
			if(substr($number,0,3) !='995') 
				$number ='995' . $number;
			$cnt = db_fetch("select count(*) as cnt from site_users where mobile='".$number."'");
			if($cnt["cnt"]==0) {
				db_query("insert into site_users (mobile, smstime) values ('".$number."', '1970-01-01 00:00:00')");
			}
			$user = db_fetch("select * from site_users where mobile='".$number."'");
			if($user["userpass"]=="") 
				$requestsms=1;
			else
				$requestsms=0;

			if($requestsms==0) {
				return -3;
			}
			
			$datetime1 = strtotime($user["smstime"]);
			$datetime2 = strtotime(date("Y-m-d H:i:s"));  // (date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"));
			$interval = abs($datetime2-$datetime1);
//			echo $datetime1."<br />";
//			echo $datetime2."<br />";
//			echo $interval; die();
			if($interval<180) {
				return -2;
			}
			$this->sms = $this->generatesms();
			$this->smssend($number, $this->sms);
			$this->number = $number;
			db_query("update site_users set sms='".$this->sms."', smstime='".date("Y-m-d H:i:s")."', status=1 where mobile='".$this->number."'");
//			die();
			return $this->sms;
		} else {
			return -1;
		}
	}

	public function savesmscoderequest($token) {
		if($this->getstatus($token)==1) {
			$user = db_fetch("select * from site_users where token='".$token."'");
			if(!empty($user)) {
				$number = $user["mobile"];
				$this->sms = $this->generatesms();
				$this->smssend($number, $this->sms);
				db_query("update site_users set savesms='".$this->sms."', status=1 where mobile='".$number."'");
				return $this->sms;
			} else {
				return -1;
			}
		} else {
			return -2;
		}
	}

	public function getinfo($token) {
		if($this->getstatus($token)==1) {
			$user = db_fetch("select * from site_users where token='".$token."'");
			if(!empty($user)) {
				return $user;
			} else {
				return -1;
			}
		} else {
			return -2;
		}
	}

	public function saveinfo($token, $sms) {
		$set = array();
		if($this->getstatus($token)==1) {
			$user = db_fetch("select * from site_users where token='".$token."'");
			if(!empty($user)) {
				if($user["savesms"]==$sms) {
					$username=isset($_POST["username"]) ? db_escape($_POST["username"]) : '';
					$password=isset($_POST["password"]) ? db_escape($_POST["password"]) : '';
					$firstname=isset($_POST["firstname"]) ? db_escape($_POST["firstname"]) : '';
					$lastname=isset($_POST["lastname"]) ? db_escape($_POST["lastname"]) : '';
					$email=isset($_POST["email"]) ? db_escape($_POST["email"]) : '';
					$personalid=isset($_POST["personalid"]) ? db_escape($_POST["personalid"]) : '';
					$birthdate=isset($_POST["birthdate"]) ? db_escape($_POST["birthdate"]) : '';
// HERE SAVE PHOTO					
					$avatar = "";
					if(isset($_FILES['photo'])) {
						$extension = strtolower(substr(strrchr($_FILES['photo']['name'], '.'), 1));
						if($extension=='jpg' || $extension=='png' || $extension=='jpeg') {
							$avatar = "files/" . $user["id"] . "-" . sha1(date("Y-m-d H:i:s")) . '.' . $extension;
							move_uploaded_file($_FILES["photo"]["tmp_name"], $avatar);
							$avatar = 'http://mobile.geocell.ge/geocellid/' . $avatar;
						}
					}
// HERE SAVE PHOTO					
					if($username != '') $set[] = "username='".$username."'";
					if($password != '') $set[] = "userpass='".$password."'";
					if($firstname != '') $set[] = "firstname='".$firstname."'";
					if($lastname != '') $set[] = "lastname='".$lastname."'";
					if($email != '') $set[] = "email='".$email."'";
					if($personalid != '') $set[] = "personalid='".$personalid."'";
					if($birthdate != '') $set[] = "birthdate='".$birthdate."'";
					if($avatar != '') $set[] = "avatar='".$avatar."'";
					$qry = implode(",", $set);
					db_query("update site_users set ".$qry.", lastdate='".date("Y-m-d H:i:s")."' where token='".$token."'");
				} else {
					return -3;
				}
			} else {
				return -1;
			}
		} else {
			return -2;
		}
	}
	
	public function applysms($number, $sms) {
		if(substr($number,0,3) != '995') 
			$number ='995' . $number;
		$cnt = db_fetch("select count(*) as cnt from site_users where mobile='".$number."' and sms='".$sms."'"); //  and status=1
		if($cnt["cnt"]==0) {
			return -1;
		} else {
			$token = sha1($number."-".$sms."-".date("Y-m-d H:i:s"));
			db_query("update site_users set token='".$token."', status=2, lastdate='".date("Y-m-d H:i:s")."' where mobile='".$number."' and sms='".$sms."'");
			return $token;
		}		
	}

	public function smssend($number, $sms) {
		file_get_contents("http://smsend.intra:7777/pls/sms/phttp2sms.Process?src=35080&dst=".$number."&txt=".$sms);
	}

	public function logout($token) {
		if($this->getstatus($token)==1) {
			$user = db_fetch("select * from site_users where token='".$token."'");
			if(!empty($user)) {
				db_query("update site_users set lastdate='2000-01-01 00:00:00' where token='".$token."'");
				return 1;
			} else {
				return -1;
			}
		} else {
			return -2;
		}
	}

	public function checknumber($number) {
		if(!(strlen($number)==9 || strlen($number)==12))
			return false;
		return true;
	}

	public function generatecomplexsms($length=6, $strength=0) {
		$vowels = 'aeuy';
		$consonants = 'bdghjmnpqrstvz';
		if ($strength & 1) {
			$consonants .= 'BDGHJLMNPQRSTVWXZ';
		}
		if ($strength & 2) {
			$vowels .= "AEUY";
		}
		if ($strength & 4) {
			$consonants .= '23456789';
		}
		if ($strength & 8) {
			$consonants .= '@#$%';
		}
		$sms = '';
		$alt = time() % 2;
		for ($i = 0; $i < $length; $i++) {
			if ($alt == 1) {
				$sms .= $consonants[(rand() % strlen($consonants))];
				$alt = 0;
			} else {
				$sms .= $vowels[(rand() % strlen($vowels))];
				$alt = 1;
			}
		}
		return $sms;
	}

	public function generatesms($length=6) {
		$chars = '0123456789';
		$sms = '';
		for ($i = 0; $i < $length; $i++) {
			$sms .= $chars[(rand() % strlen($chars))];
		}
		return $sms;
	}

}	
?>