<?php
define('PRODUCTION', FALSE);
define('DIR', TRUE);
error_reporting(PRODUCTION ? 0 : E_ALL & ~E_STRICT);
ini_set('display_errors', PRODUCTION ? FALSE : TRUE);
// Setup configuration
require 'config.php';
require 'functions.php';
require 'sms/smppclass.php';
// DB
$db_link = mysql_connect($c['database.hostname'], $c['database.username'], $c['database.password']) OR exit('Could not connect to database.');
mysql_select_db($c['database.name'], $db_link) OR exit('Database table does not exists.');
date_default_timezone_set('Asia/Tbilisi');
db_query('SET NAMES UTF-8;');
// Route URI
$router = router();
$language = $router['language'];
$slug = $router['slug'];
$segments = $router['segments'];
foreach($_GET as $k=>$v) {
	$_GET[$k] = db_escape($v);
}

require_once 'class.geocellid.php';
$geocellid = new geocellid();
if(file_exists($slug.'.php'))
	require_once $slug.'.php';
else
	require_once 'error.php';

// file_get_contents("http://smsend.intra:7777/pls/sms/phttp2sms.Process?src=35080&dst=995577560600&txt=test");
// die();


//$number="995577560600";
//$sms = $geocellid->smscoderequest($number);
//echo $sms;
//echo '<br />';
//$token = $geocellid->applysms($number, $sms);
//echo $token;
//echo '<br />';
//$status = $geocellid->getstatus('0fe3d5372fbb522c298e5869fc7f8e9dd776ca70');
//echo $status;
?>