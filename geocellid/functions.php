<?php
ini_set('display_errors',1);
require('sms/smppclass.php');
function sendsms($number, $sms) {
	$smpphost = "10.10.50.5"; 
	$smppport = 16000; 
	$systemid = "mobility"; 
	$password = "MB45LT98"; 
	$system_type = ""; 
	$from = "Geocell";

	$smpp = new SMPPClass(); 

	$smpp->SetSender($from); 

	/* bind to smpp server */ 
	$smpp->Start($smpphost, $smppport, $systemid, $password, $system_type); 

	/* send enquire link PDU to smpp server */ 
	$smpp->TestLink(); 

	/* send single message; large messages are automatically split */ 
	$smpp->Send($number, $sms, true); //995595700008

	/* unbind from smpp server */ 
	$smpp->End(); 
}

//echo "1". sendsms('577188398','test');
	
	
function c($key = NULL, $default = NULL)
{
	global $c;
    if (empty($key))
        return $c;
    return array_key_exists($key, $c) ? $c[$key] : $default;
}

function sef($string)
{
    $string = preg_replace('/[^-ა-ჰa-zA-Z0-9_ ]/', NULL, $string);
    $string = preg_replace('/[-_ ]+/', '-', trim($string));
    return strtolower($string);
}

function curl_get($url)
{
    $request = curl_init($url);
    if (FALSE === $request)
        return FALSE;
    curl_setopt_array($request, array(
        CURLOPT_FOLLOWLOCATION => 1,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_TIMEOUT => 10
    ));
    $response = curl_exec($request);
    curl_close($request);
    return $response;
}

function router()
{
	global $c;
    $request_uri = explode('/', $_SERVER['REQUEST_URI']);
    $script_name = explode('/', $_SERVER['SCRIPT_NAME']);
    $segments = array_diff_assoc($request_uri, $script_name);
    if (empty($segments))
    {
        return array(
            'language' => c('languages.default'),
            'slug' => NULL,
            'segments' => array()
        );
    }
    $segments = array_values($segments);
    $language_id = rtrim(sef($segments[0]), '/');
    $has_language = in_array($language_id, c('languages.all'));
    $uri['language'] = $has_language ? $language_id : c('languages.default');
    $has_language AND $segments = array_slice($segments, 1);
	$last_slug = (count($segments) == 0) ? "" : $segments[count($segments) - 1];
    $qs_position = strpos($last_slug, '?');
    FALSE === $qs_position OR $segments[count($segments) - 1] = substr($last_slug, 0, $qs_position);
    foreach ($segments as $key => $value)
    {
        $segments[$key] = rtrim(sef($value), '/');
        if (empty($segments[$key]))
            unset($segments[$key]);
    }
    $uri['slug'] = empty($segments) ? NULL : (string) trim(implode('/', $segments), '/');
    $uri['segments'] = empty($segments) ? array() : $segments;
	$uri['request_uri'] = $request_uri;
	$uri['haslanguage'] = $has_language;
    return $uri;
}

function db_query($sql)
{
	global $db_link;
    if (($result = mysql_query($sql, $db_link)) === FALSE)
        return FALSE;
    return $result;
}

function db_fetch($sql)
{
    $result = db_query($sql);
    if ($result === FALSE)
        return FALSE;
    return mysql_fetch_assoc($result);
}

function db_fetch_all($sql)
{
    $result = db_query($sql);
    if ($result === FALSE)
        return FALSE;
    $rows = array();
    while ($row = mysql_fetch_assoc($result))
        $rows[] = $row;
    return $rows;
}

function db_retrieve($column, $table, $field, $value, $other = NULL, $log = FALSE)
{
    is_numeric($value) OR $value = "'{$value}'";
    empty($other) OR $other = ' ' . $other;
    $sql = "SELECT `{$column}` FROM `{$table}` WHERE `{$field}` = {$value}{$other};";
    $log AND trace($sql, 'db_retrieve() SQL');
    $result = db_fetch($sql);
    if (empty($result))
        return FALSE;
    return $result[$column];
}

function db_escape($value)
{
	global $db_link;
    if ($value === '')
        return 'NULL';
    switch (gettype($value))
    {
        case 'string':
            $value = mysql_real_escape_string($value, $db_link);
            break;
        case 'boolean':
            $value = (int) $value;
            break;
        case 'double':
            $value = sprintf('%F', $value);
            break;
        default:
            $value = $value === NULL ? 'NULL' : $value;
            break;
    }
    return (string) $value;
}

function db_insert($table, $data = array())
{
    $fields = array();
    $columns = array();
    foreach ($data as $field => $value)
    {
        $fields[] = "`{$field}`";
        $value = db_escape($value);
        $columns[] = (is_numeric($value) OR 'NULL' === $value) ? $value : '\'' . $value . '\'';
    }
    return 'INSERT INTO `' . $table . '` (' . implode(', ', $fields) . ') VALUES (' . implode(', ', $columns) . ');';
}

function db_update($table, $data = array(), $other = NULL)
{
    $set = array();
    foreach ($data as $field => $value)
    {
        $value = db_escape($value);
        $set[] = "`{$field}` = " . ((is_numeric($value) OR 'NULL' === $value) ? $value : '\'' . $value . '\'');
    }
    return 'UPDATE `' . $table . '` SET ' . implode(', ', $set) . (empty($other) ? NULL : ' ' . $other) . ';';
}

function get_ip()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        return $_SERVER['HTTP_CLIENT_IP'];
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
    else
        return $_SERVER['REMOTE_ADDR'];
}

function redirect($url)
{
    if (headers_sent())
    {
        $js = '<script type=\"text/javascript\"><!-- ';
        $js .= 'window.location.replace(' . $url . ');';
        $js .= ' //--></script>';
        $html = '<noscript>';
        $html .= '<meta http-equiv="refresh" content="0; url=' . $url . '" />';
        $html .= '</noscript>';
        echo exit($js . $html);
    }
    header('HTTP/1.1 301 Moved Permanently');
    header('Location: ' . $url);
    header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
    exit;
}

function html_decode($value, $sec = ""){
	switch($sec){
		case "pdf":
		$ret = html_entity_decode($value, ENT_COMPAT, 'UTF-8');
		$ret = str_replace("&nbsp;"," ",$ret);
		return $ret;
		break;
		default:
		return html_entity_decode($value, ENT_COMPAT, 'UTF-8');
		break;
	}
}
