<?php
	$status = $geocellid->logout($_GET["token"]);
	if($status<0) {
		$body["errorMessage"] = 'Unauthorized';
		$body["error"] = -1;
		$body["success"] = 0;
	} else {
		$body["errorMessage"] = 'Logoff';
		$body["error"] = 0;
		$body["success"] = 1;
	}
	echo json_encode($body);
?>