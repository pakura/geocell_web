<?php
	$status = $geocellid->getstatus($_GET["token"]);
	if($status==-1) {
		$body["errorMessage"] = 'Unauthorized';
		$body["error"] = -1;
		$body["success"] = 0;
	} else {
		$body["errorMessage"] = 'Authorized';
		$body["token"] = $_GET["token"];
		$body["success"] = 1;
	}
	echo json_encode($body);
?>