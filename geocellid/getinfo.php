<?php
	$user = $geocellid->getinfo($_GET["token"]);
	if($user==-1) {
		$body["errorMessage"] = 'Incorrect Number';
		$body["error"] = -1;
		$body["success"] = 0;
	}elseif($user==-2) {
		$body["errorMessage"] = 'Unauthorised';
		$body["error"] = -2;
		$body["success"] = 0;
	} else {
		$body["username"] = $user["username"];
		$body["firstname"] = $user["firstname"];
		$body["lastname"] = $user["lastname"];
		$body["email"] = $user["email"];
		$body["personalid"] = $user["personalid"];
		$body["birthdate"] = $user["birthdate"];
		$body["photo"] = $user["avatar"];
		$body["errorMessage"] = 'Success';
		$body["error"] = 0;
		$body["success"] = 1;
	}
	echo json_encode($body);
?>
