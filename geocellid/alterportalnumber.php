<?php
	$sms = $geocellid->portalsmscoderequest($_GET["number"]);
	if($sms==-1) {
		$body["errorMessage"] = 'Incorrect Number';
		$body["needspassword"] = 0;
		$body["error"] = -1;
		$body["success"] = 0;
	} elseif($sms==-2) {
		$body["errorMessage"] = 'SMS already sent. Try again in 3 min.';
		$body["needspassword"] = 0;
		$body["error"] = -1;
		$body["success"] = 0;
	} elseif($sms==-3) {
		$body["errorMessage"] = 'Password needed.';
		$body["needspassword"] = 1;
		$body["error"] = 0;
		$body["success"] = 1;
	} else {
		$body["errorMessage"] = 'Success';
		$body["needspassword"] = 0;
		$body["error"] = 0;
//		$body["sms"] = $sms;
		$body["success"] = 1;
	}
	echo json_encode($body);
?>