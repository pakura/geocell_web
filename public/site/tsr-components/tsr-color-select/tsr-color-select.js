/* TSR - COLOR SELECT */
;(function(document,$) {

  window.tsrComponentColorSelect = window.tsrComponentColorSelect || {};

  // ********************************************************************************
  // TSR - Init
  tsrComponentColorSelect.tsrInit = function() {
    tsrComponentColorSelect.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Component Init
  tsrComponentColorSelect.tsrSectionInit = function() {
    $('.tsr-color-select').not('.tsr-color-select-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $component = $(this).addClass('tsr-color-select-processed');
      var $colors = $('li', this);
      var $input = $('input[type="hidden"]', this);

      // ********************************************************************************
      // Execute
      $colors.on('click', function(event) {
        $colors.removeClass('is-choosen');
        $(this).addClass('is-choosen')
        $input.val($(this).attr('data-color'));
        event.preventDefault();
      });

      function update() {
        $colors.removeClass('is-choosen').filter('[data-color="' + $input.val() + '"]').addClass('is-choosen');
      }

      $(window).on('tsr-update', update);
      $component.on('tsr-update', update);
      
      update();

    });
  };

  // ********************************************************************************
  // Page Ready
  $(tsrComponentColorSelect.tsrInit);

})(document,jQuery);
