/*
 * jQuery form-controls plugin 1.0.0
 *
 * Copyright (c) 2015 Andria Klimov
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 */
;(function($) {

  // **************************************************
  // CUSTOM TEXT

  $.fn.customText = function(className) {

    return this.filter(':text, :password, textarea').each(function() {

      // set class name
      if (!className)
        className = 'cc-text';

      // instance control
      var $instance = $(this);

      // replace control
      var $wrapper = $('<span></span>');

      // setup instance
      $instance.after($wrapper.addClass(className)).attr('style', null);
      $wrapper.append($instance);

    });
  };

  // **************************************************
  // CUSTOM SELECT

  $.fn.customSelect = function(className) {

    return this.filter('select').each(function() {

      // set class name
      if (!className)
        className = 'cc-select';

      // instance control
      var $instance = $(this);

      // replace control
      var $wrapper = $('<span><span></span></span>');

      // additional layers
      var $holder = $('<span class="cc-holder"></span>');
      var $selector = $('<span class="cc-selector"><a href="#"><span></span></a></span>');
      var $optionor = $('<span class="cc-optionor"><span class="cc-options"></span></span>');
      var $options = $('.cc-options', $optionor);
      $wrapper.children().append($holder);
      $holder.append($selector).append($optionor)

      // sender control
      var $control = $('a', $wrapper);

      // functions
      function isSelected() {
        return $optionor.is(':visible');
      }

      function isDisabled() {
        return $instance.prop('disabled');
      }

      function isFocused() {
        return $control.is(':focus');
      }

      function hasState(state) {
        return $wrapper.children().prop('class').indexOf(state) >= 0;
      }

      function setState(disabled, selected, focused, hover) {
        var state = 'cc-state'
          + ((disabled === null && hasState('-disabled') || disabled) ? '-disabled' : '')
          + ((selected === null && hasState('-selected') || selected) ? '-selected' : '')
          + ((focused  === null && hasState('-focused')  || focused)  ? '-focused'  : '')
          + ((hover    === null && hasState('-hover')    || hover)    ? '-hover'    : '');
        $wrapper.children().attr('class', '').addClass('cc-states').addClass(state);
      }

      function update(rebuild) {
        if (rebuild) {
          rebuildOptions();
        }
        updateControl();
        setState(isDisabled(), isSelected(), isFocused(), isDisabled() ? false : null);
      }

      function updateControl() {
        $options.children().removeClass('cc-option-selected');
        var index = $instance.prop('selectedIndex');
        if (index >= 0 && index < $options.children().length) {
          $options.children().eq(index).addClass('cc-option-selected');
          $control.children().html($options.children().eq(index).html());
        }
      }

      function rebuildOptions() {
        $options.empty();
        $('option', $instance).each(function(index) {
          var $option = $('<span />');
          $options.append($option);
          $option.addClass('cc-option').html($(this).html()).on('mouseenter', function(event) {
            $option.addClass('cc-option-hover');
          }).on('mouseleave', function(event) {
            $option.removeClass('cc-option-hover');
          }).on('click', function(event) {
            toggleOptions(false);
            delegateEvent('click');
            if ($instance.prop('selectedIndex') != index) {
              $instance.prop('selectedIndex', index);
              updateControl();
              delegateEvent('change');
            } else {
              delegateEvent('keep');
            }
            event && event.preventDefault();
          });
        });
        $options.children().last().addClass('cc-option-last');
        $options.children().first().addClass('cc-option-first');
      }

      function toggleOptions(toggle) {
        if (toggle === undefined) {
          toggle = !$optionor.is(':visible');
        }
        // process optionor
        if (toggle) {
          if ($options.children().length) {
            $(document).bind('click', onLeave);
            setState(null, true, null, null);
            // set optionor height to fit exactly 8 options (after show)
            $options.height($options.children().outerHeight() * ($options.children().length > 8 ? 8 : $options.children().length)).scrollTop(0);
          }
        } else {
          $(document).unbind('click', onLeave);
          setState(null, false, null, null);
        }
      }

      function delegateEvent(type) {
        // some events (such as blur or focus) will not work on a hidden elements,
        // so we will trigger additional custom events...
        $instance.trigger(type);
        $instance.trigger('cc-' + type);
      }

      function onEvent(event) {
        if (!isDisabled()) {
          delegateEvent(event.type);
          update();
        }
      }

      function onMouseEnter(event) {
        if (!isDisabled()) {
          setState(isDisabled(), isSelected(), isFocused(), true);
          delegateEvent(event.type);
          update();
        }
      }

      function onMouseLeave(event) {
        if (!isDisabled()) {
          setState(isDisabled(), isSelected(), isFocused(), false);
          delegateEvent(event.type);
          update();
        }
      }

      function onBlur(event) {
        setState(isDisabled(), isSelected(), isFocused(), isDisabled() ? false : null);
        if (!isDisabled()) {
          delegateEvent(event.type);
        }
        update();
      }

      function onFocus(event) {
        setState(isDisabled(), isSelected(), isFocused(), isDisabled() ? false : null);
        if (!isDisabled()) {
          delegateEvent(event.type);
        }
        update();
      }

      function onClick(event) {
        if (!isDisabled()) {
          delegateEvent(event.type);
          toggleOptions();
          update();
        }
        event && event.preventDefault();
      }

      function onKeypress(event) {
        if (!isDisabled()) {
          delegateEvent(event.type);
          update();
          var key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
          if (key == 0x25 || key == 0x26) {
            // todo left
          } else if (key == 0x27 || key == 0x28) {
            // todo right
          }
        }
      }

      function onUpdate(event, rebuild) {
        update(rebuild);
      }

      function onLeave(event) {
        if (event.target != $instance.get(0) && event.target != $wrapper.get(0) && $(event.target).closest($wrapper.get(0)).length == 0) {
          setState(null, false, null, null);
        }
      }

      function onScroll(event) {
        var delta = event.wheelDelta || (event.originalEvent && event.originalEvent.wheelDelta) || -event.detail;
        var topOverflow = this.scrollTop <= 0;
        var botOverflow = this.scrollTop + $(this).outerHeight() - this.scrollHeight >= 0;
        if ((delta < 0 && botOverflow) || (delta > 0 && topOverflow)) {
          event.preventDefault();
        }
      }

      // setup instance
      $instance.hide().after($wrapper.addClass(className));

      // bind events
      $instance.on('change cc-update', onUpdate);
      $control
        .on('click', onClick)
        .on('keypress', onKeypress)
        .on('blur', onBlur)
        .on('focus', onFocus)
        .on('mouseenter', onMouseEnter)
        .on('mouseleave', onMouseLeave)
        .on('dblclick mousedown mousemove mouseout mouseover mouseup keydown keyup', onEvent);

      // delivery focus to control
      $control.contents().on('dblclick mousedown mousemove mouseout mouseover mouseup keydown keyup', function(event) {
        event.preventDefault();
        event.stopPropagation();
      }).on('mousedown', function(event) {
        $control.trigger('focus');
        event.preventDefault();
        event.stopPropagation();
      });

      // prevent page scrolling when scrolling an element
      $options.on('mousewheel DOMMouseScroll', onScroll);

      // init
      update(true);

    });
  }

  // **************************************************
  // CUSTOM RADIO

  $.fn.customRadio = function(className) {

    return this.filter(':radio').each(function() {

      // set class name
      if (!className)
        className = 'cc-radio';

      // instance control
      var $instance = $(this);

      // replace control
      var $wrapper = $('<span><span><a href="#"></a></span></span>');

      // sender control
      var $control = $('a', $wrapper);

      // functions
      function isChecked() {
        return $instance.prop('checked');
      }

      function isDisabled() {
        return $instance.prop('disabled');
      }

      function isFocused() {
        return $control.is(':focus');
      }

      function hasState(state) {
        return $wrapper.children().prop('class').indexOf(state) >= 0;
      }

      function setState(checked, disabled, focused, hover) {
        var state = 'cc-state'
          + ((checked  == null && hasState('-checked')  || checked)  ? '-checked'  : '')
          + ((disabled == null && hasState('-disabled') || disabled) ? '-disabled' : '')
          + ((focused  == null && hasState('-focused')  || focused)  ? '-focused'  : '')
          + ((hover    == null && hasState('-hover')    || hover)    ? '-hover'    : '');
        $wrapper.children().attr('class', '').addClass('cc-states').addClass(state);
      }

      function update(rebuild) {
        if (!rebuild && $instance.prop('name')) {
          $('input[name="' + $instance.prop('name') + '"]:radio').trigger('cc-update', true);
        } else {
          setState(isChecked(), isDisabled(), isFocused(), isDisabled() ? false : null);
        }
      }

      function delegateEvent(type) {
        // some events (such as blur or focus) will not work on a hidden elements,
        // so we will trigger additional custom events...
        $instance.trigger(type);
        $instance.trigger('cc-' + type);
      }

      function onEvent(event) {
        if (!isDisabled()) {
          delegateEvent(event.type);
          update();
        }
      }

      function onClick(event) {
        if (!isDisabled()) {
          delegateEvent(event.type);
          update();
        }
        event && event.preventDefault();
      }

      function onKeypress(event) {
        if (!isDisabled()) {
          delegateEvent(event.type);
          update();
          var key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
          if (key == 0x20) {
            $control.trigger('click');
          }
        }
      }

      function onMouseEnter(event) {
        if (!isDisabled()) {
          setState(isChecked(), isDisabled(), isFocused(), true);
          delegateEvent(event.type);
          update();
        }
      }

      function onMouseLeave(event) {
        setState(isChecked(), isDisabled(), isFocused(), false);
        if (!isDisabled()) {
          delegateEvent(event.type);
          update();
        }
      }

      function onBlur(event) {
        setState(isChecked(), isDisabled(), isFocused(), isDisabled() ? false : null);
        if (!isDisabled()) {
          delegateEvent(event.type);
        }
        update();
      }

      function onFocus(event) {
        setState(isChecked(), isDisabled(), isFocused(), isDisabled() ? false : null);
        if (!isDisabled()) {
          delegateEvent(event.type);
        }
        update();
      }

      function onUpdate(event, rebuild) {
        update(rebuild);
      }

      // setup instance
      $instance.hide().after($wrapper.addClass(className));

      // bind events
      $instance.on('change propertychange cc-update', onUpdate);
      $control
        .on('click', onClick)
        .on('keypress', onKeypress)
        .on('blur', onBlur)
        .on('focus', onFocus)
        .on('mouseenter', onMouseEnter)
        .on('mouseleave', onMouseLeave)
        .on('dblclick mousedown mousemove mouseout mouseover mouseup keydown keyup', onEvent);

      // bind events to labels
      if ($instance.prop('id')) {
        $('label[for="' + $instance.prop('id') + '"]').on('click', onUpdate);
      }

      // init
      update(true);

    });
  };

  // **************************************************
  // CUSTOM CHECKBOX

  $.fn.customCheckbox = function(className) {

    return this.filter(':checkbox').each(function() {

      // set class name
      if (!className)
        className = 'cc-checkbox';

      // instance control
      var $instance = $(this);

      // replace control
      var $wrapper = $('<span><span><a href="#"></a></span></span>');

      // sender control
      var $control = $('a', $wrapper);

      // functions
      function isChecked() {
        return $instance.prop('checked');
      }

      function isDisabled() {
        return $instance.prop('disabled');
      }

      function isFocused() {
        return $control.is(':focus');
      }

      function hasState(state) {
        return $wrapper.children().prop('class').indexOf(state) >= 0;
      }

      function setState(checked, disabled, focused, hover) {
        var state = 'cc-state'
          + ((checked  == null && hasState('-checked')  || checked)  ? '-checked'  : '')
          + ((disabled == null && hasState('-disabled') || disabled) ? '-disabled' : '')
          + ((focused  == null && hasState('-focused')  || focused)  ? '-focused'  : '')
          + ((hover    == null && hasState('-hover')    || hover)    ? '-hover'    : '');
        $wrapper.children().attr('class', '').addClass('cc-states').addClass(state);
      }

      function update() {
        setState(isChecked(), isDisabled(), isFocused(), isDisabled() ? false : null);
      }

      function delegateEvent(type) {
        // some events (such as blur or focus) will not work on a hidden elements,
        // so we will trigger additional custom events...
        $instance.trigger(type);
        $instance.trigger('cc-' + type);
      }

      function onEvent(event) {
        if (!isDisabled()) {
          delegateEvent(event.type);
          update();
        }
      }

      function onClick(event) {
        if (!isDisabled()) {
          delegateEvent(event.type);
          update();
        }
        event && event.preventDefault();
      }

      function onKeypress(event) {
        if (!isDisabled()) {
          delegateEvent(event.type);
          update();
          var key = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode;
          if (key == 0x20) {
            $control.trigger('click');
          }
        }
      }

      function onMouseEnter(event) {
        if (!isDisabled()) {
          setState(isChecked(), isDisabled(), isFocused(), true);
          delegateEvent(event.type);
          update();
        }
      }

      function onMouseLeave(event) {
        if (!isDisabled()) {
          setState(isChecked(), isDisabled(), isFocused(), false);
          delegateEvent(event.type);
          update();
        }
      }

      function onBlur(event) {
        setState(isChecked(), isDisabled(), isFocused(), isDisabled() ? false : null);
        if (!isDisabled()) {
          delegateEvent(event.type);
        }
        update();
      }

      function onFocus(event) {
        setState(isChecked(), isDisabled(), isFocused(), isDisabled() ? false : null);
        if (!isDisabled()) {
          delegateEvent(event.type);
        }
        update();
      }

      function onUpdate(event) {
        update();
      }

      // setup instance
      $instance.hide().after($wrapper.addClass(className));

      // bind events
      $instance.on('change propertychange cc-update', onUpdate);
      $control
        .on('click', onClick)
        .on('keypress', onKeypress)
        .on('blur', onBlur)
        .on('focus', onFocus)
        .on('mouseenter', onMouseEnter)
        .on('mouseleave', onMouseLeave)
        .on('dblclick mousedown mousemove mouseout mouseover mouseup keydown keyup', onEvent);

      // bind events to labels
      if ($instance.prop('id')) {
        $('label[for="' + $instance.prop('id') + '"]').on('click', onUpdate);
      }

      // init
      update();

    });
  };

})(jQuery);




// ********************************************************************************
/* TSR - FORMS */
;(function(document,$) {

  window.tsrForms = window.tsrForms || {};

  // ********************************************************************************
  // TSR - Init
  tsrForms.tsrInit = function() {
    tsrForms.tsrFormsInit();
  };

  // ********************************************************************************
  // TSR - Component Init
  tsrForms.tsrFormsInit = function() {
    $('.tsr-forms').each(function() {

      var self = this;

      /* form-controls */

      /* text */
      /*
      if ($.isFunction($.fn.customText)) {
        $('textarea, input[type="text"], input[type="password"]', self).not('.cc-processed').addClass('cc-processed').customText('cc-text');
      }
      */
      
      if ($.isFunction($.fn.customSelect))
        $('select', self).not('.cc-processed').addClass('cc-processed').customSelect('cc-select');

      if ($.isFunction($.fn.customRadio))
        $('input[type="radio"]', self).not('.cc-processed').addClass('cc-processed').customRadio('cc-radio');

      if ($.isFunction($.fn.customCheckbox))
        $('input[type="checkbox"]', self).not('.cc-processed').addClass('cc-processed').customCheckbox('cc-checkbox');

      if ($.isFunction($.fn.customRadio) || $.isFunction($.fn.customCheckbox)) {
        $('.cc-radio, .cc-checkbox', self).parent('.cc-label').addClass('cc-label-cbox');
      }

    });
  };

  // ********************************************************************************
  // Page Ready
  $(tsrForms.tsrInit);

})(document,jQuery);
