/* TSR - HROnline */
;(function(document,$) {

  window.tsrSectionHROnline = window.tsrSectionHROnline || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionHROnline.tsrInit = function() {
    tsrSectionHROnline.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionHROnline.tsrSectionInit = function() {
    $('.tsr-section-hronline').not('.tsr-section-hronline-processed').each(function() {

      // ********************************************************************************
      // Vars

      var $window  = $(window);
      var $section = $(this).addClass('tsr-section-hronline-processed');

      var $spaceA = $('.tsr-hronline-space', $section);
      var $spaceB = $('.tsr-hronline-sticker-space', $section);

      var $fixator = $('.tsr-hronline-side-fixator', $section);
      var $sticker = $('.tsr-hronline-side-sticker', $section);

      // ********************************************************************************
      // Execute

      if ($spaceA.length && $spaceB.length && $spaceA.length && $fixator.length && $sticker.length) {

        var xshiftTop = 20;
        var xshiftBot = 20;

        var tsrOnUpdate = function(event) {
          $fixator.height('auto').height($sticker.height());
          var offsetA = $spaceA.offset();
          var offsetB = $spaceB.offset();
          var shifter = offsetB.top - offsetA.top + xshiftTop;
          var a = $window.scrollTop() - offsetA.top + xshiftTop;
          if (a > shifter) {
            var b = a + xshiftBot - $spaceB.height() + $sticker.height() - offsetB.top + offsetA.top;
            $sticker.addClass('tsr-fixed').css({'top': b > 0 ? a - b : a});
          } else {
            $sticker.removeClass('tsr-fixed').css({'top': shifter});
          }
        };

        var tsrEnableFixation = function() {
          $window.on('tsr-update scroll resize', tsrOnUpdate);
          tsrOnUpdate();
        };

        var tsrRemoveFixation = function() {
          $fixator.height('');
          $sticker.removeClass('tsr-fixed').css({'top': ''});
          $window.off('tsr-update scroll resize', tsrOnUpdate);
        };

        enquire.register("screen and (max-width: 1020px)", {
          setup : function() {
            tsrEnableFixation();
          },
          match : function() {
            tsrRemoveFixation();
          },
          unmatch : function() {
            tsrEnableFixation();
          }
        });

      }
      
      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Page Ready
  $(tsrSectionHROnline.tsrInit);

})(document,jQuery);
