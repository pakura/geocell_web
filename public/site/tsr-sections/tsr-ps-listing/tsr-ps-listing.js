/* TSR - PRODUCTS AND SERVICES LISTING */
;(function(document,$) {

  window.tsrSectionPSListing = window.tsrSectionPSListing || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionPSListing.tsrInit = function() {
    tsrSectionPSListing.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionPSListing.tsrSectionInit = function() {
    $('.tsr-section-ps-listing').not('.tsr-section-ps-listing-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $section = $(this).addClass('tsr-section-ps-listing-processed');

      // ********************************************************************************
      // Items
      var tsrItems = new (function() {

        var self = this;
        self.tsrFlagEqualHeights = false;

        self.tsrEnableEqualHeights = function() {
          self.tsrFlagEqualHeights = true;
        };

        self.tsrDisableEqualHeights = function() {
          self.tsrFlagEqualHeights = false;
        };

        self.tsrEqualHeights = function() {
          // Service
          tsrCore.equalHeights($('.tsr-module-service-title',  $section));
          tsrCore.equalHeights($('.tsr-module-service-intro',  $section));
          tsrCore.equalHeights($('.tsr-module-service-price',  $section));
          tsrCore.equalHeights($('.tsr-module-service',        $section));
          // Product
          tsrCore.equalHeights($('.tsr-module-product-title',  $section));
          tsrCore.equalHeights($('.tsr-module-product-intro',  $section));
          tsrCore.equalHeights($('.tsr-module-product-price',  $section));
          tsrCore.equalHeights($('.tsr-module-product-print',  $section));
          tsrCore.equalHeights($('.tsr-module-product-colors', $section));
          tsrCore.equalHeights($('.tsr-module-product',        $section));
        };

        self.tsrResetHeights = function() {
          // Service
          tsrCore.resetHeights($('.tsr-module-service-title',  $section));
          tsrCore.resetHeights($('.tsr-module-service-intro',  $section));
          tsrCore.resetHeights($('.tsr-module-service-price',  $section));
          tsrCore.resetHeights($('.tsr-module-service',        $section));
          // Product
          tsrCore.resetHeights($('.tsr-module-product-title',  $section));
          tsrCore.resetHeights($('.tsr-module-product-intro',  $section));
          tsrCore.resetHeights($('.tsr-module-product-price',  $section));
          tsrCore.resetHeights($('.tsr-module-product-print',  $section));
          tsrCore.resetHeights($('.tsr-module-product-colors', $section));
          tsrCore.resetHeights($('.tsr-module-product',        $section));
        };

        self.tsrOnResize = function() {
          if (self.tsrFlagEqualHeights) {
            self.tsrEqualHeights();
          } else {
            self.tsrResetHeights();
          }
        };

        return self;
      });

      // ********************************************************************************
      // Execute

      enquire.register("screen and (max-width: 600px)", {
        setup : function() {
          //console.log('600 setup');
          tsrItems.tsrEnableEqualHeights();
          tsrItems.tsrEqualHeights();
        },
        match : function() {
          //console.log('600 match');
          tsrItems.tsrDisableEqualHeights();
          tsrItems.tsrResetHeights();
        },
        unmatch : function() {
          //console.log('600 unmatch');
          tsrItems.tsrEnableEqualHeights();
          tsrItems.tsrEqualHeights();
        }
      });

      $(window).on('tsr-update', function() {
        tsrItems.tsrOnResize();
      });

      $(window).on('load', function() {
        tsrItems.tsrOnResize();
      });

      $(window).on('resize', tsrCore.debounce(function() {
        tsrItems.tsrOnResize();
      }, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Ready
  $(tsrSectionPSListing.tsrInit);

})(document,jQuery);
