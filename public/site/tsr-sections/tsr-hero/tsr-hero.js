/* TSR - HERO */
;(function(document,$) {

  window.tsrSectionHero = window.tsrSectionHero || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionHero.tsrInit = function() {
    tsrSectionHero.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionHero.tsrSectionInit = function() {
    $('.tsr-section-hero').not('.tsr-section-slideshow-hero').each(function() {

      // ********************************************************************************
      // Vars

      var $section   = $(this).addClass('tsr-section-hero-processed');

      // ********************************************************************************
      // Swipe Events

      var toggleSwipeEvents = function(selector, toggle) {
        var $selector = $(selector);
        var handler = $selector.data('swipeHandler');
        if (toggle) {
          if (!handler) {
            handler = new (function() {
              var self = this, coords = null;
              self.getCoords = function(event) {
                if (event.touches && event.touches.length === 1) {
                  return [event.touches[0].pageX, event.touches[0].pageY];
                }
                return null;
              };
              self.onTouchStart = function(event) {
                coords = self.getCoords(event.originalEvent);
                event.stopPropagation();
              };
              self.onTouchMove = function(event) {
                if (coords) {
                  var newcoords = self.getCoords(event.originalEvent);
                  if (Math.abs(newcoords[0] - coords[0]) > Math.abs(newcoords[1] - coords[1])) {
                    if ((newcoords[0] - coords[0]) > 0) {
                      // swipe left
                      $(event.target).trigger('swipe-left')
                    } else {
                      // swipe right
                      $(event.target).trigger('swipe-right')
                    }
                  } else {
                    if ((newcoords[1] - coords[1]) > 0) {
                      // swipe up
                      $(event.target).trigger('swipe-up')
                    } else {
                      // swipe down
                      $(event.target).trigger('swipe-down')
                    }
                  }
                }
                coords = null;
                event.stopPropagation();
              };
              self.onTouchEnd = function(event) {
                coords = null;
                event.stopPropagation();
              };
              self.onTouchCancel = function(event) {
                coords = null;
                event.stopPropagation();
              };
              return self;
            })();

            $selector.data('swipeHandler', handler)
            .on('touchcancel', handler.onTouchCancel)
            .on('touchstart', handler.onTouchStart)
            .on('touchmove', handler.onTouchMove)
            .on('touchend', handler.onTouchEnd);
          }
        } else {
          if (handler) {
            $selector.data('swipeHandler', undefined)
            .off('touchcancel', handler.onTouchCancel)
            .off('touchstart', handler.onTouchStart)
            .off('touchmove', handler.onTouchMove)
            .off('touchend', handler.onTouchEnd);
          }
        }
      };

      // ********************************************************************************
      // Hero
      
      $('.tsr-hero', $section).each(function() {

        var $hero = $(this).addClass('tsr-loader');

        var $stage = $('.tsr-hero-stage', $hero);
        var $slideshow = $stage.children('.tsr-slides').first();
        var $items = $slideshow.children('.tsr-slide');
        var $itemsShowical = $('.tsr-hero-showical', $items);
        var $itemsPromocal = $('.tsr-hero-promocal', $items);

        if ($items.length > 1) {
          $hero.append('<div class="tsr-hero-nav"><ul>' + (new Array($items.length + 1).join('<li><\/li>')) + '<\/ul><\/div>');
          $stage.append('<div class="tsr-hero-dirs"><div class="tsr-hero-prev"><\/div><div class="tsr-hero-next"><\/div><\/div>');
        }

        var $navs  = $('.tsr-hero-nav li', $hero);
        var $prev  = $('.tsr-hero-prev', $stage);
        var $next  = $('.tsr-hero-next', $stage);

        $slideshow.slideshow({
          animation   : 'scroll',
          play        : false,
          continuous  : true,
          reverse     : false,
          index       : 0,
          pauseTime   : 7000,
          switchTime  : 500,
          easing      : 'easeOutExpo',
          callback    : function(index, reason) {
            $navs.removeClass('tsr-active').eq(index).addClass('tsr-active');
          }
        });

        $navs.on('click', function(event) {
          var slideshow = $slideshow.data('slideshow');
          if (slideshow) {
            slideshow.stop();
            slideshow.show($navs.index(this));
            $navs.removeClass('tsr-active');
            $(this).addClass('tsr-active');
            event.preventDefault();
          }
        }).first().addClass('tsr-active');

        $prev.on('click', function(event) {
          var slideshow = $slideshow.data('slideshow');
          if (slideshow) {
            slideshow.stop();
            slideshow.prev();
          }
          event.preventDefault();
        }).on('mousedown', function(event) {
          event.preventDefault();
        });

        $next.on('click', function(event) {
          var slideshow = $slideshow.data('slideshow');
          if (slideshow) {
            slideshow.stop();
            slideshow.next();
          }
          event.preventDefault();
        }).on('mousedown', function(event) {
          event.preventDefault();
        });

        toggleSwipeEvents($slideshow, true);
        $slideshow.on('swipe-left', function() {
          var slideshow = $slideshow.data('slideshow');
          if (slideshow) {
            slideshow.stop();
            slideshow.prev();
          }
        }).on('swipe-right', function() {
          var slideshow = $slideshow.data('slideshow');
          if (slideshow) {
            slideshow.stop();
            slideshow.next();
          }
        });

        // preload
        var preload = function(version) {
          var id = preload.id = preload.id ? ++preload.id : 0;
          var slideshow = $slideshow.data('slideshow');
          if (slideshow) {
            slideshow.stop();
          }
          $hero.addClass('tsr-initialized').addClass('tsr-loader');
          $itemsShowical.each(function() {
            var $this = $(this), bgi = $this.attr('data-bgi-' + version);
            if (bgi) {
              $this.css('background-image', 'url("' + bgi + '")');
            }
          });
          tsrCore.waitForImages($hero, function() {
            if (id == preload.id) {
              $hero.removeClass('tsr-loader');
              if (slideshow) {
                slideshow.play(true);
              }
            }
          });
        };
        enquire.register("screen and (max-width: 640px)", {
          setup : function() {
            preload('v1');
          },
          match : function() {
            preload('v2');
          },
          unmatch : function() {
            preload('v1');
          }
        });

        // auto resize
        var hasEqualHeight = false;
        var equalHeight = function() {
          $slideshow.height('').height(Math.ceil(Math.max.apply(null, $itemsPromocal.map(function() {
            return $(this).height();
          }).get())));
        };
        var resetHeight = function() {
          $slideshow.height('');
        };
        enquire.register("screen and (max-width: 800px)", {
          setup : function() {
            hasEqualHeight = false;
            resetHeight();
          },
          match : function() {
            hasEqualHeight = true;
            equalHeight();
          },
          unmatch : function() {
            hasEqualHeight = false;
            resetHeight();
          }
        });
        $(window).on('resize', function() {
          if (hasEqualHeight) {
            equalHeight();
          } else {
            resetHeight();
          }
        });

      });

      // $(window).on('load', function() {});
      // $(window).on('resize', tsrCore.debounce(function() {}, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Ready
  $(tsrSectionHero.tsrInit);

})(document,jQuery);
