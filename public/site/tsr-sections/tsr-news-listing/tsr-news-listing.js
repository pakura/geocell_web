/* TSR - NEWS LISTING */
;(function(document,$) {

  window.tsrSectionNewsListing = window.tsrSectionNewsListing || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionNewsListing.tsrInit = function() {
    tsrSectionNewsListing.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionNewsListing.tsrSectionInit = function() {
    $('.tsr-section-news-listing').not('.tsr-section-news-listing-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $section = $(this).addClass('tsr-section-news-listing-processed');

      // ********************************************************************************
      // Items
      var tsrItems = new (function() {

        var self = this;
        self.tsrFlagEqualHeights = false;

        self.tsrEnableEqualHeights = function() {
          self.tsrFlagEqualHeights = true;
        };

        self.tsrDisableEqualHeights = function() {
          self.tsrFlagEqualHeights = false;
        };

        self.tsrEqualHeights = function() {
          tsrCore.equalHeights($('.tsr-module-news-title',  $section));
          tsrCore.equalHeights($('.tsr-module-news-intro',  $section));
          tsrCore.equalHeights($('.tsr-module-news-date',   $section));
          tsrCore.equalHeights($('.tsr-module-news',        $section));
        };

        self.tsrResetHeights = function() {
          tsrCore.resetHeights($('.tsr-module-news-title',  $section));
          tsrCore.resetHeights($('.tsr-module-news-intro',  $section));
          tsrCore.resetHeights($('.tsr-module-news-date',   $section));
          tsrCore.resetHeights($('.tsr-module-news',        $section));
        };

        self.tsrOnResize = function() {
          if (self.tsrFlagEqualHeights) {
            self.tsrEqualHeights();
          } else {
            self.tsrResetHeights();
          }
        };

        return self;
      });

      // ********************************************************************************
      // Execute

      enquire.register("screen and (max-width: 600px)", {
        setup : function() {
          //console.log('600 setup');
          tsrItems.tsrEnableEqualHeights();
          tsrItems.tsrEqualHeights();
        },
        match : function() {
          //console.log('600 match');
          tsrItems.tsrDisableEqualHeights();
          tsrItems.tsrResetHeights();
        },
        unmatch : function() {
          //console.log('600 unmatch');
          tsrItems.tsrEnableEqualHeights();
          tsrItems.tsrEqualHeights();
        }
      });

      $(window).on('tsr-update', function() {
        tsrItems.tsrOnResize();
      });

      $(window).on('load', function() {
        tsrItems.tsrOnResize();
      });

      $(window).on('resize', tsrCore.debounce(function() {
        tsrItems.tsrOnResize();
      }, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Ready
  $(tsrSectionNewsListing.tsrInit);

})(document,jQuery);
