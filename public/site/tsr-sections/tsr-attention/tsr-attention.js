/* TSR - ATTENTION LISTING */
;(function(document,$) {

  window.tsrSectionAttention = window.tsrSectionAttention || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionAttention.tsrInit = function() {
    tsrSectionAttention.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionAttention.tsrSectionInit = function() {
    $('.tsr-section-attention').not('.tsr-section-attention-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $section = $(this).addClass('tsr-section-attention-processed');

      // ********************************************************************************
      // Execute
      $('.tsr-module-attention-close', $section).on('click', function() {
        $section.slideUp(300, 'easeOutExpo', function() {
          $section.remove();
        });
      });


      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Ready
  $(tsrSectionAttention.tsrInit);

})(document,jQuery);
