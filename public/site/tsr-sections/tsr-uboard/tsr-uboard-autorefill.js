/* TSR - ARFILL */
;(function(document,$) {

  window.tsrComponentUBoardARFill = window.tsrUBoardARFill || {};

  // ********************************************************************************
  // TSR - Init
  tsrComponentUBoardARFill.tsrInit = function() {
    tsrComponentUBoardARFill.tsrComponentInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrComponentUBoardARFill.tsrComponentInit = function() {
    $('.tsr-uboard-arfill').not('.tsr-uboard-arfill-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $window  = $(window);
      var $component = $(this).addClass('tsr-uboard-arfill-processed');

      var $fixator = $('.tsr-uboard-arfill-fixator', $component);
      var $sticker = $('.tsr-uboard-arfill-sticker', $component);

      // ********************************************************************************
      // Execute

      if ($fixator.length && $sticker.length) {

        var xshiftTop = 20;
        var xshiftBot = 40;

        var tsrOnUpdate = function(event) {
          $fixator.height('auto').height($sticker.height());
          var offset = $component.offset();
          var a = $window.scrollTop() - offset.top + xshiftTop;
          if (a > 0) {
            var b = a + xshiftBot - $component.height() + $sticker.height();
            $sticker.addClass('fixed').css({'top': b > 0 ? a - b : a});
          } else {
            $sticker.removeClass('fixed').css({'top': ''});
          }
        };

        var tsrEnableFixation = function() {
          $window.on('tsr-update scroll resize', tsrOnUpdate);
          tsrOnUpdate();
        };

        var tsrRemoveFixation = function() {
          $sticker.removeClass('fixed').css({'top': ''});
          $window.off('tsr-update scroll resize', tsrOnUpdate);
        };

        enquire.register("screen and (max-width: 1020px)", {
          setup : function() {
            tsrEnableFixation();
          },
          match : function() {
            tsrRemoveFixation();
          },
          unmatch : function() {
            tsrEnableFixation();
          }
        });

      }
      
      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Ready
  $(tsrComponentUBoardARFill.tsrInit);

})(document,jQuery);
