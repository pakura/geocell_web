/* TSR - TEMPLATE */
;(function(document,$) {

  window.tsrComponentUBoardServices = window.tsrUBoardServices || {};

  // ********************************************************************************
  // TSR - Init
  tsrComponentUBoardServices.tsrInit = function() {
    tsrComponentUBoardServices.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrComponentUBoardServices.tsrSectionInit = function() {
    $('.tsr-uboard-services').not('.tsr-uboard-services-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $component = $(this).addClass('tsr-uboard-services-processed');
      var $services  = $('.tsr-uboard-service', $component);

      if ($component.hasClass('tsr-uboard-services-scenario-a')) {

        // SCENARIO A

        var collapse = function() {
          $services.removeClass('is-selected').removeClass('is-hidden');
          $('.tsr-uboard-service-body:visible', $services).slideUp(300, 'easeOutExpo');
        };

        var expand = function(self) {
          collapse(self);
          $services.addClass('is-hidden');
          $(self).addClass('is-selected').removeClass('is-hidden');
          $('.tsr-uboard-service-body', self).slideDown(300, 'easeOutExpo');
        };

        $services.each(function() {
          var self = this;
          $('.tsr-uboard-service-link', self).on('click', function(event) {
            if ($(self).hasClass('is-selected')) {
              collapse();
            } else if (!$services.filter('.is-selected').length) {
              collapse();
              expand(self);
            }
            event.preventDefault();
          });
          $('.tsr-uboard-service-body-close', self).children().on('click', function(event) {
            collapse();
            event.preventDefault();
          });
        });

      }

      // ********************************************************************************
      // Execute

      //$(window).on('load', function() {});
      //$(window).on('resize', tsrCore.debounce(function() {}, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Ready
  $(tsrComponentUBoardServices.tsrInit);

})(document,jQuery);
