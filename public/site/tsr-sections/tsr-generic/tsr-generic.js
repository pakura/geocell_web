/* TSR - GENERIC */
;(function(document,$) {

  window.tsrSectionGeneric = window.tsrSectionGeneric || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionGeneric.tsrInit = function() {
    tsrSectionGeneric.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionGeneric.tsrSectionInit = function() {
    $('.tsr-section-generic').not('.tsr-section-generic-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $section = $(this).addClass('tsr-section-generic-processed');
      var $boxes = $('.tsr-section-generic-box', $section);
      var fixes = new Array(
        $('.tsr-section-generic-fix-0', $section),
        $('.tsr-section-generic-fix-1', $section),
        $('.tsr-section-generic-fix-2', $section),
        $('.tsr-section-generic-fix-3', $section),
        $('.tsr-section-generic-fix-4', $section),
        $('.tsr-section-generic-fix-5', $section),
        $('.tsr-section-generic-fix-6', $section),
        $('.tsr-section-generic-fix-7', $section),
        $('.tsr-section-generic-fix-8', $section),
        $('.tsr-section-generic-fix-9', $section)
      );

      // ********************************************************************************
      // Lines
      var tsrLines = new (function() {

        var self = this;
        self.tsrFlagEqualHeights = false;

        self.tsrEnableEqualHeights = function() {
          self.tsrFlagEqualHeights = true;
        };

        self.tsrDisableEqualHeights = function() {
          self.tsrFlagEqualHeights = false;
        };

        self.tsrEqualHeights = function() {
          for (var i = 0; i < fixes.length; i++) {
            if (fixes[i].length) {
              tsrCore.equalHeights(fixes[i]);
            }
          }
          tsrCore.equalHeights($boxes);
        };

        self.tsrResetHeights = function() {
          for (var i = 0; i < fixes.length; i++) {
            if (fixes[i].length) {
              tsrCore.resetHeights(fixes[i]);
            }
          }
          tsrCore.resetHeights($boxes);
        };

        self.tsrOnResize = function() {
          if (self.tsrFlagEqualHeights) {
            self.tsrEqualHeights();
          } else {
            self.tsrResetHeights();
          }
        };

        return self;
      });

      // ********************************************************************************
      // Execute

      enquire.register("screen and (max-width: 480px)", {
        setup : function() {
          tsrLines.tsrEnableEqualHeights();
          tsrLines.tsrEqualHeights();
        },
        match : function() {
          tsrLines.tsrDisableEqualHeights();
          tsrLines.tsrResetHeights();
        },
        unmatch : function() {
          tsrLines.tsrEnableEqualHeights();
          tsrLines.tsrEqualHeights();
        }
      });

      $(window).on('tsr-update', function() {
        tsrLines.tsrOnResize();
      });

      $(window).on('load', function() {
        tsrLines.tsrOnResize();
      });

      $(window).on('resize', tsrCore.debounce(function() {
        tsrLines.tsrOnResize();
      }, 250));



      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Page Ready
  $(tsrSectionGeneric.tsrInit);

})(document,jQuery);
