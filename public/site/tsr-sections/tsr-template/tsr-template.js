/* TSR - TEMPLATE */
;(function(document,$) {

  window.tsrSectionTemplate = window.tsrSectionTemplate || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionTemplate.tsrInit = function() {
    tsrSectionTemplate.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionTemplate.tsrSectionInit = function() {
    $('.tsr-section-template').not('.tsr-section-template-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $section = $(this).addClass('tsr-section-template-processed');

      // ********************************************************************************
      // Execute

      enquire.register("screen and (max-width: 600px)", {
        setup : function() {
          //console.log('600 setup');
        },
        match : function() {
          //console.log('600 match');
        },
        unmatch : function() {
          //console.log('600 unmatch');
        }
      }, true);

      $(window).bind('load', function() {
        // to do
      });

      $(window).bind('resize', tsrCore.debounce(function() {
        // to do
      }, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Ready
  $(tsrSectionTemplate.tsrInit);

})(document,jQuery);
