/*
 * jQuery placeholder plugin 1.0.0
 *
 * Copyright (c) 2009 Andria Klimov
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 */
(function($) {

  $.fn.placeholder = function(options) {

    // default configuration
    var config = {
      className : 'placeholder',
      callback  : null
    };

    // update configuration
    if (options && typeof(options) == 'string')
      config.value = options;
    else if (options)
      $.extend(config, options);

    // initialize plugin
    return this.filter('input, textarea').each(function() {

      var $instance = $(this);

      var value = $instance.attr('placeholder');
      if (!value) {
        value = $instance.attr('title');
        $instance.attr('title', '');
      }

      if ($instance.val() == '' || $instance.val() == value) {
        $instance.val(value).addClass(config.className);
      }
        
      $instance.focus(function() {
        if ($instance.hasClass(config.className)) {
          $(this).removeClass(config.className).val('');
          if ($.isFunction(config.callback)) {
            $.proxy(config.callback, $instance.get(0))(true);
          }
        };
      }).blur(function() {
        if (this.value == '') {
          $(this).addClass(config.className).val(value);
          if ($.isFunction(config.callback)) {
            $.proxy(config.callback, $instance.get(0))(false);
          }
        };
      });

      if (this.form) {
        $(this.form).submit(function() {
          if ($instance.hasClass(config.className)) {
            $instance.removeClass(config.className).val('');
            if ($.isFunction(config.callback)) {
              $.proxy(config.callback, $instance.get(0))(false);
            }
          }
        });
      }
      
      if ($.isFunction(config.callback)) {
        $.proxy(config.callback, $instance.get(0))(false);
      }

    });
  };
})(jQuery);
