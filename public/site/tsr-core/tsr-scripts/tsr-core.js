/* TSR - CORE */
;(function(document,$) {

  //****************************************************************
  // Core Object
  //****************************************************************
  window.tsrCore = window.tsrCore || (function tsrCore() {

    //****************************************************************
    // Save this
    //****************************************************************
    var core = this;

    //****************************************************************
    // Create Instance by Call
    //****************************************************************
    if (!(core instanceof tsrCore)) {
      return new tsrCore();
    }

    //****************************************************************
    // Override toString
    //****************************************************************
    core.toString = function toString() {
      return "[object Core]";
    };

    //****************************************************************
    // Parse Background-Image URL
    //****************************************************************
    core.parseBackgroundImageUrl = function(string) {
      return string.replace(/^url\(["\']?/, '').replace(/["\']?\)$/, '');
    };

    //****************************************************************
    // Wait For Images
    //****************************************************************
    core.waitForImages = (function() {
      function IMGHelper(selector, options) {
        if (!(this instanceof IMGHelper)) {
          return new IMGHelper(selector, options);
        }
        var self = this, queue = 0, stack = new Array(), state = null;
        function notifyCancel() {
          if (false === state) {
            if (options.canceled instanceof Function) {
              options.canceled.call(self, stack.length, queue);
            }
            if (options.complete instanceof Function) {
              options.complete.call(self, stack.length, queue);
            }
          }
        }
        function notifyDone() {
          if (true === state) {
            if (options.done instanceof Function) {
              options.done.call(self, stack.length, queue);
            }
            if (options.complete instanceof Function) {
              options.complete.call(self, stack.length, queue);
            }
          }
        }
        function notifyProgress() {
          if (null === state) {
            if (options.progress instanceof Function) {
              options.progress.call(self, stack.length, queue);
            }
          }
          if (queue <= 0) {
            state = true;
            notifyDone();
          }
        }
        function handle() {
          if (this.complete) {
            $(this).off('load error', handle);
            --queue;
            notifyProgress();
          }
        }
        $(selector).add('*', selector).each(function() {
          var src, bgi = $(this).css('background-image');
          if (bgi && bgi.match(RegExp('^url\\(["\']?[^"\']+["\']?\\)$', 'gi')) != null) {
            src = bgi.replace(RegExp('^url\\(["\']?|["\']?\\)$', 'gi'), '');
          }
          if (this.tagName && this.tagName.toUpperCase() === 'IMG' && this.src) {
            src = this.src;
          }
          if (src) {
            var img = new Image();
            img.src = src;
            stack.push(img);
          }
        });
        if (stack.length) {
          queue = stack.length;
          notifyProgress();
          $(stack).each(function() {
            if (this.complete) {
              --queue;
              notifyProgress();
            } else {
              $(this).on('load error', handle);
            }
          });
        }
        this.cancel = function() {
          $(stack).off('load error', handle);
          canceled = true;
          notifyCancel();
        };
      }
      return function(selector, options) {
        return (options instanceof Function) ? new IMGHelper(selector, {complete: options}) : new IMGHelper(selector, options);
      };
    })();

    //****************************************************************
    // Get ScrollBar Size
    //****************************************************************
    core.getScrollBarSize = (function() {
      var size = null;
      return function() {
        if (size === null) {
          var test = $('<textarea></textarea>').css({
            'overflow': 'scroll',
            'width': '1000px', 'height': '1000px',
            'padding:': '0', 'border': '0', 'margin': '0',
            'position': 'absolute', 'visibility': 'hidden'
          }).get(0);
          $(document.body).append(test);
          if (test.clientWidth !== undefined && test.offsetWidth !== undefined) {
            size = [test.offsetWidth - test.clientWidth, test.offsetHeight - test.clientHeight]
          }
          $(test).remove();
        }
      return size;
      };
    })();

    //****************************************************************
    // Equal Widths
    //****************************************************************
    core.equalWidths = function(selector, extra) {
      var $selector = $(selector);
      $selector.width('auto').width((extra || 0) + Math.ceil(Math.max.apply(null, $selector.map(function() {
        return $(this).width();
      }).get())));
    };

    //****************************************************************
    // Equal Heights
    //****************************************************************
    core.equalHeights = function(selector, extra) {
      var $selector = $(selector);
      $selector.height('auto').height((extra || 0) + Math.ceil(Math.max.apply(null, $selector.map(function() {
        return $(this).height();
      }).get())));
    };

    //****************************************************************
    // Reset Widths
    //****************************************************************
    core.resetWidths = function(selector) {
      $(selector).width('');
    };

    //****************************************************************
    // Reset Heights
    //****************************************************************
    core.resetHeights = function(selector) {
      $(selector).height('');
    };

    //****************************************************************
    // Debounce Function
    //****************************************************************
    core.debounce = function(func, threshold, execAsap) {
      var timeout = null;
      return function() {
        var obj = this, args = arguments;
        if (timeout) {
          clearTimeout(timeout);
        } else if (execAsap) {
          func.apply(obj, args);
        }
        timeout = window.setTimeout(function() {
          if (!execAsap) {
            func.apply(obj, args);
          }
          timeout = null;
        }, threshold || 200);
      };
    };

    //****************************************************************
    // Scroll
    //****************************************************************
    core.scrollTo = function(target, duration, offset) {
      var $scroll = $('html,body');
      var $target = $(target).first();
      if ($target.length) {
        var top = Math.min(Math.ceil($target.offset().top + (offset || 0)), Math.ceil($(document.body).height() - $(window).height()));
        $scroll.stop(true, false);
        if (duration) {
          $scroll.animate({'scrollTop': top}, duration, 'easeOutExpo');
        } else {
          $scroll.scrollTop(top);
        }
      }
    };

    //****************************************************************
    // Is Mobile Device
    //****************************************************************
    core.isMobile = function() {
      return !!(jQuery.browser && jQuery.browser.mobile);
    };

    //****************************************************************
    // Is Touch Device
    //****************************************************************
    core.isTouch = function() {
      return !!(('ontouchstart' in window) || ('DocumentTouch' in window && document instanceof DocumentTouch) || ('maxTouchPoints' in navigator));
    };

    //****************************************************************
    // Handles
    //****************************************************************
    core.handle = new (function() {
      var storageName = '__handles__';
      var handle = this;
      handle.fn = {'sample': function(el) { return {}; }};
      handle.fx = {};
      handle.get = function(selector, name) {
        var handles = $(selector).data(storageName) || {};
        return name in handles ? handles[name] : handles;
      };
      handle.bind = function(selector, names) {
        $(selector).each(function() {
          var el = this, $el = $(el), handles = $el.data(storageName) || {};
          $.each((names || $el.data('handle') || String()).split(' '), function() {
            if (this in handle.fn && !(this in handles)) {
              handles[this] = handle.fn[this].call(el) || {};
              $el.data(storageName, handles);
            }
          });
        });
      };
      handle.unbind = function(selector, names) {
        $(selector).each(function() {
          var el = this, $el = $(el), handles = $el.data(storageName) || {};
          $.each((names || $el.data('handle') || String()).split(' '), function() {
            if (this in handles) {
              delete handles[this];
            }
          });
        });
      };
    });

    //****************************************************************

  })();

  //****************************************************************
  // Ready
  //****************************************************************
  $(document).on('ready', function(){

    /* document initialization */
    $(document).trigger('tsr-init');

  });

  //****************************************************************
  // Init
  //****************************************************************
  $(document).on('tsr-init', function(event) {
    var selector = event.target;

    // bind handles
    tsrCore.handle.bind($('[data-handle]', selector));

    // scrollbar
    if ($.fn.scrollbar) {
      $('.tsr-scrollpane', selector).scrollbar();
    }

    // input placeholders
    if ($.fn.placeholder) {
      if (!('placeholder' in document.createElement('input'))) {
        $('input[placeholder], textarea[placeholder]', selector).addClass('placeholder');
      }
      $('input.placeholder, textarea.placeholder', selector).placeholder();
    }

    // inputmask
    if ($.fn.inputmask && !tsrCore.isMobile()) {
      $('[data-inputmask]', selector).inputmask();
    }

    // scroll to
    $('a.js-scroll-to', selector).on('click', function(event) {
      tsrCore.scrollTo(this.hash, 500);
      event.preventDefault();
    });

  });

  //****************************************************************
  // Scroll
  //****************************************************************
  // $(document).on('scroll', function() {});

  //****************************************************************
  // Resize
  //****************************************************************
  // $(window).on('resize', function() {});

  //****************************************************************
  // Debug Resolution
  //****************************************************************
  (function() {
    if (window.location && window.location.host && window.location.host.indexOf('geocell.me') >= 0) {
      var $helper = $('<div style="display: block; z-index: 1000; position: fixed; bottom: 20px; left: 20px; padding: 10px; background: #404040; color: #FFFFFF;"></div>');
      var resize = function() {
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
          $helper.html(w + 'x' + h);
        };
      $(document.body).append($helper);
      $(window).resize(resize);
      resize();
    }
  })();

})(document,jQuery);
