/* TSR - HANDLES */
;(function(core,handle,$) {

  //****************************************************************
  // Simple Collapse
  //****************************************************************
  handle.fn['simple-collapse'] = function() {

    var holder = this;

    var anchorAttr = 'data-simple-collapse-anchor';
    var targetAttr = 'data-simple-collapse-target';

    var $anchors = $('[' + anchorAttr + ']', holder);
    var $targets = $('[' + targetAttr + ']', holder);

    $targets.on('click', function(event) {
      var name = $(this).attr(targetAttr);
      var $anchors = $('[' + anchorAttr + '="' + name + '"]', holder);
      var $targets = $('[' + targetAttr + '="' + name + '"]', holder);
      if ($anchors.is(':visible')) {
        $targets.removeClass('is-selected').addClass('is-deselect');
        $anchors.stop(true, false).slideUp(300, 'easeOutExpo');
      } else {
        $targets.removeClass('is-deselect').addClass('is-selected');
        $anchors.stop(true, false).slideDown(300, 'easeOutExpo');
      }
      event.preventDefault();
    });

    if (!$anchors.is(':visible')) {
      $targets.removeClass('is-selected').addClass('is-deselect');
      $anchors.stop(true, false).hide();
    } else {
      $targets.removeClass('is-deselect').addClass('is-selected');
      $anchors.stop(true, false).show();
    }

  };

  //****************************************************************
  // Fancyboxes
  //****************************************************************
  (function() {

    var fancyboxSpeed  = 150;
    var fancyboxEffect = $('html').hasClass('touch') ? 'none' : 'elastic';

    window.fancyboxAjaxOptions = {
      'fitToView'     : false,
      'autoSize'      : false,
      'width'         : '100%',
      'height'        : 'auto',
      'maxWidth'      : 720,
      'scrolling'     : 'visible',
      'openSpeed'     : fancyboxSpeed,
      'closeSpeed'    : fancyboxSpeed,
      'prevSpeed'     : fancyboxSpeed,
      'nextSpeed'     : fancyboxSpeed,
      'openEffect'    : fancyboxEffect,
      'closeEffect'   : fancyboxEffect,
      'prevEffect'    : fancyboxEffect,
      'nextEffect'    : fancyboxEffect,
      'margin'        : 0,
      'padding'       : 0,
      'beforeShow'    : function() {
        window.tsrForms.tsrInit();
      },
      'afterShow'     : function() {
        $('.fancybox-close').attr('title', null);
      },
      'afterClose': function() {
        $(window).trigger('fancybox-close');
      },
      'helpers'       : {
        'overlay'       : { 'closeClick': false }
      }
    };

    window.fancybox = function(options) {
      function open(options) {
        $.fancybox.open(options);
      };
      if ($.fancybox.isOpen) {
        $(window).one('fancybox-close', function() {
          open(options);
        });
        $.fancybox.close();
      } else {
        open(options);
      }
      return false;
    };


  })();

  handle.fn['fancybox'] = function() {
    $(this).fancybox($.extend({
      'type': $(this).attr('data-fancybox-type'),
      'maxWidth': window.parseInt($(this).attr('data-fancybox-width')) || window.fancyboxAjaxOptions.maxWidth
    }, window.fancyboxAjaxOptions));
  };

  handle.fn['fancybox-chain'] = function() {
    $(this).click(function(event) {
      window.fancybox($.extend({
        'href': $(this).attr('data-fancybox-href') || this.href,
        'type': $(this).attr('data-fancybox-type'),
        'maxWidth': window.parseInt($(this).attr('data-fancybox-width')) || window.fancyboxAjaxOptions.maxWidth
      }, window.fancyboxAjaxOptions));
      event.preventDefault();
    });
  };

  //****************************************************************
  // Equal Heights
  //****************************************************************
  handle.fn['equalh'] = function() {

    var $holder = $(this);
    var $window = $(window);

    var names = $holder.attr('data-equalh') || String();
    var packs = new Object();

    var cssget = 'outerHeight';
    var cssset = 'min-height';

    var rebuild = function() {
      packs = new Object();
      $.each(names.split(' '), function() {
        if (this) {
          packs[this] = $('[data-equalh="' + this + '"]', $holder);
        }
      });
    };

    var reset = function() {
      for (name in packs) {
        if (packs.hasOwnProperty(name)) {
          packs[name].css(cssset, '');
        }
      }
    };

    var equal = function() {
      var groups, pos;
      for (name in packs) {
        if (packs.hasOwnProperty(name)) {
          groups = new Object();
          for (var i = 0; i < packs[name].length; ++i) {
            if (!packs[name][i].hasAttribute('data-equalh-suspend')) {
              pos = Math.ceil(packs[name].eq(i).offset().top);
              groups[pos] = groups.hasOwnProperty(pos) ? groups[pos].add(packs[name][i]) : packs[name].eq(i);
            }
          }
          for (pos in groups) {
            if (groups.hasOwnProperty(pos)) {
              if (groups[pos].length > 1) {
                groups[pos].css(cssset, 'auto').css(cssset, Math.max.apply(null, groups[pos].map(function() {
                  return $(this)[cssget]();
                }).get()));
              }
            }
          }
        }
      }
    };

    var equalDebounced = core.debounce(equal, 120);

    var onResize = function() {
      reset();
      equalDebounced();
    };

    var onUpdate = function() {
      rebuild();
      reset();
      equal();
    };

    rebuild();
    equal();

    $window.on('resize', onResize);
    $window.on('tsr-update', onUpdate);

  };

  //****************************************************************
  // popup-sticker-a
  //****************************************************************
  handle.fn['popup-sticker-bot'] = function() {

    var $holder = $(this);
    var $window = $(window);

    var $sticker = $('[data-sticker-role="sticker"]', $holder);
    var $close   = $('[data-sticker-role="close"]', $holder);
    
    var attrVis = 'data-sticker-visible';

    var toggleVisibility = function(toggle) {
      $sticker.attr(attrVis, toggle ? 'yes' : null);
    };

    var matchOn = function(offset) {
      return offset >= Math.ceil($holder.offset().top - $window.height());
    };

    var update = function() {
      var hasVis = matchOn($window.scrollTop());
      if (hasVis) {
        toggleVisibility(true);
        $window.off('resize scroll com-update', update);
      }
    };

    $close.on('click', function(event) {
      toggleVisibility(false);
      event.preventDefault();
    });

    $window.on('resize scroll com-update', update);
    update();

  };

  //****************************************************************
  // dropgrid
  //****************************************************************
  handle.fn['dropgrid'] = function() {

    var $holder = $(this);
    var $window = $(window);

    var dropgrid = new (function() {

      var self = this;

      var $element = $holder;

      var attrRole = 'data-dropgrid-role';
      var attrName = 'data-dropgrid-name';

      var $anchorsOuterTemplate = $();
      var $targetsOuterTemplate = $();

      var selectRole = function(role, context) {
        return $('[' + attrRole + '="' + role + '"]', context);
      };

      var closestRole = function(role, context) {
        return $(context).closest('[' + attrRole + '="' + role + '"]');
      };

      var filterName = function(context, name) {
        return $(context).filter(function() { return $(this).attr(attrName) == name; });
      };

      var getName = function(context) {
        return $(context).attr(attrName);
      };

      var repack = function(toggle) {
        reset();
        var $anchorsOuters = selectRole('anchors-outer', $element);
        var $targetsOuters = selectRole('targets-outer', $element);
        var $anchorsInners = selectRole('anchors-inner', $anchorsOuters.first());
        var $targetsInners = selectRole('targets-inner', $targetsOuters.first());
        var $anchorsOuterTemplate = $anchorsOuters.first().clone();
        var $targetsOuterTemplate = $targetsOuters.first().clone();
        selectRole('anchors-inner', $anchorsOuterTemplate).empty();
        selectRole('targets-inner', $targetsOuterTemplate).empty();
        selectRole('anchor', $anchorsOuters.slice(1)).each(function() {
          $anchorsInners.append(this);
        });
        selectRole('target', $targetsOuters.slice(1)).each(function() {
          $targetsInners.append(this);
        });
        if (toggle) {
          var groups = new Object(), pos;
          var $anchors = selectRole('anchor', $anchorsInners);
          var $targets = selectRole('target', $targetsInners);
          $anchors.each(function() {
            pos = Math.ceil($(this).offset().top);
            if (!groups.hasOwnProperty(pos)) {
              groups[pos] = {
                'anchors': $(),
                'targets': $()
              };
            }
            groups[pos]['anchors'] = groups[pos]['anchors'].add(this);
            groups[pos]['targets'] = groups[pos]['targets'].add(filterName($targets, getName(this)));
          });
          for (pos in groups) {
            if (groups.hasOwnProperty(pos)) {
              var $anchorsOuterClone = $anchorsOuterTemplate.clone();
              var $targetsOuterClone = $targetsOuterTemplate.clone();
              $element.append($anchorsOuterClone).append($targetsOuterClone);
              selectRole('anchors-inner', $anchorsOuterClone).append(groups[pos]['anchors']);
              selectRole('targets-inner', $targetsOuterClone).append(groups[pos]['targets']);
            }
          }
        }
        $anchorsOuters.remove();
        $targetsOuters.remove();
      };

      var reset = function() {
        var $anchors = selectRole('anchor', $element);
        var $targets = selectRole('target', $element);
        var $switchs = selectRole('switch', $anchors);
        $switchs.removeClass('is-selected').removeClass('is-deselect');
        $anchors.removeClass('is-selected').removeClass('is-deselect');
        $targets.stop(true, true).hide();
      };

      var onClick = function(event) {
        var name = getName(closestRole('anchor', this));
        var $anchor = filterName(selectRole('anchor', $element), name);
        var $target = filterName(selectRole('target', $element), name);
        var $anchors = selectRole('anchor', closestRole('anchors-inner', $anchor)).not($anchor);
        var $targets = selectRole('target', closestRole('targets-inner', $target)).not($target).filter(':visible');
        var $switch  = selectRole('switch', $anchor);
        var $switchs = selectRole('switch', $anchors);
        if ($target.is(':visible')) {
          $switch.removeClass('is-selected');
          $anchor.removeClass('is-selected');
          $switchs.removeClass('is-deselect');
          $anchors.removeClass('is-deselect');
          $target.stop(true, false).slideUp(300, 'easeOutExpo');
        } else {
          $switchs.removeClass('is-selected').addClass('is-deselect');
          $anchors.removeClass('is-selected').addClass('is-deselect');
          var promise = $targets.stop(true, false).slideUp(300, 'easeOutExpo').promise().done(function() {
            $switch.removeClass('is-deselect').addClass('is-selected');
            $anchor.removeClass('is-deselect').addClass('is-selected');
            $target.stop(true, false).slideDown(300, 'easeOutExpo');
          });
        }
        event.preventDefault();
      };

      var registerEvents = function() {
        selectRole('switch', $element).on('click', onClick);
      };

      var unregisterEvents = function() {
        selectRole('switch', $element).off('click', onClick);
      };

      var enquireQuery = {
        'b2b-benefits': ['screen and (max-width: 1020px)', 'screen and (max-width: 560px)']
      };

      var enquireValue = {
        setup : function() {
          repack(true);
        },
        match : function() {
          repack(true);
        },
        unmatch : function() {
          repack(true);
        }
      };
      
      var registerEnquire = function() {
        var name = getName($element);
        if (enquireQuery.hasOwnProperty(name)) {
          for (var i = 0, length = enquireQuery[name].length; i < length; ++i) {
            enquire.register(enquireQuery[name][i], enquireValue);
          }
        }
      };

      var unregisterEnquire = function() {
        var name = getName($element);
        if (enquireQuery.hasOwnProperty(name)) {
          for (var i = 0, length = enquireQuery[name].length; i < length; ++i) {
            enquire.unregister(enquireQuery[name][i], enquireValue);
          }
        }
      };

      self.on = function() {
        registerEnquire();
        registerEvents();
      };

      self.off = function() {
        unregisterEnquire();
        unregisterEvents();
      };

    })();

    dropgrid.on();

    return dropgrid;

  };

  //****************************************************************

})(tsrCore,tsrCore.handle,jQuery);
