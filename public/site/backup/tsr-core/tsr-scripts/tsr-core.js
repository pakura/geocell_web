/* TSR - CORE */
;(function(document,$) {

  //****************************************************************
  // Core Object
  //****************************************************************
  window.tsrCore = window.tsrCore || (function tsrCore() {

    //****************************************************************
    // Save this
    //****************************************************************
    var core = this;

    //****************************************************************
    // Create Instance by Call
    //****************************************************************
    if (!(core instanceof tsrCore))
      return new tsrCore();

    //****************************************************************
    // Override toString
    //****************************************************************
    core.toString = function toString() {
      return "[object Core]";
    };

    //****************************************************************
    // Parse Background-Image URL
    //****************************************************************
    core.parseBackgroundImageUrl = function(string) {
      return string.replace(/^url\(["\']?/, '').replace(/["\']?\)$/, '');
    };

    //****************************************************************
    // Wait For Images
    //****************************************************************
    core.waitForImages = (function() {
      var getBackgroundImage = function(el) { return el.style ? el.style.backgroundImage : undefined; }
      if (window.getComputedStyle) {
        getBackgroundImage = function(el) { return el.ownerDocument.defaultView.opener ? el.ownerDocument.defaultView.getComputedStyle(el, null).backgroundImage : window.getComputedStyle(el, null).backgroundImage; }
      } else if (document.documentElement.currentStyle) {
        getBackgroundImage = function(el) { return el.currentStyle.backgroundImage; }
      }
      return function(selector, callback) {
        var $selector = $(selector).add('*', selector);
        var images = new Array();
        var queue = $selector.length;
        var deque = function(src) {
          var img = new Image();
          images.push(img);
          img.src = src;
          if (img.complete) {
            --queue;
          } else {
            $(img).one('load error', function() {
              if (!--queue) {
                callback();
              }
            });
          }
        }
        $selector.each(function() {
          var bgi = getBackgroundImage(this);
          if (!bgi || bgi == 'none' || bgi.match(/^url\(["\']?[^"\']+["\']?\)$/gi) == null) {
            --queue;
          } else {
            deque(bgi.replace(/^url\(["\']?|["\']?\)$/gi, ''));
          }
          if (this.tagName && this.tagName.toUpperCase() === 'IMG' && this.src) {
            ++queue;
            deque(this.src);
          }
        });
        if (!queue) {
          callback();
        }
      };
    })();

    //****************************************************************
    // Get ScrollBar Size
    //****************************************************************
    core.getScrollBarSize = (function() {
      var size = null;
      return function() {
        if (size === null) {
          var test = $('<textarea></textarea>').css({
            'overflow': 'scroll',
            'width': '1000px', 'height': '1000px',
            'padding:': '0', 'border': '0', 'margin': '0',
            'position': 'absolute', 'visibility': 'hidden'
          }).get(0);
          $(document.body).append(test);
          if (test.clientWidth !== undefined && test.offsetWidth !== undefined) {
            size = [test.offsetWidth - test.clientWidth, test.offsetHeight - test.clientHeight]
          }
          $(test).remove();
        }
      return size;
      };
    })();

    //****************************************************************
    // Equal Widths
    //****************************************************************
    core.equalWidths = function(selector, extra) {
      var $selector = $(selector);
      $selector.width('auto').width((extra || 0) + Math.ceil(Math.max.apply(null, $selector.map(function() {
        return $(this).width();
      }).get())));
    };

    //****************************************************************
    // Equal Heights
    //****************************************************************
    core.equalHeights = function(selector, extra) {
      var $selector = $(selector);
      $selector.height('auto').height((extra || 0) + Math.ceil(Math.max.apply(null, $selector.map(function() {
        return $(this).height();
      }).get())));
    };

    //****************************************************************
    // Reset Widths
    //****************************************************************
    core.resetWidths = function(selector) {
      $(selector).width('');
    };

    //****************************************************************
    // Reset Heights
    //****************************************************************
    core.resetHeights = function(selector) {
      $(selector).height('');
    };

    //****************************************************************
    // Debounce Function
    //****************************************************************
    core.debounce = function(func, threshold, execAsap) {
      var timeout = null;
      return function() {
        var obj = this, args = arguments;
        if (timeout) {
          clearTimeout(timeout);
        } else if (execAsap) {
          func.apply(obj, args);
        }
        timeout = window.setTimeout(function() {
          if (!execAsap) {
            func.apply(obj, args);
          }
          timeout = null;
        }, threshold || 200);
      };
    };

    //****************************************************************
    // Scroll
    //****************************************************************
    core.scrollTo = function(target, duration, offset) {
      var $scroll = $('html,body');
      var $target = $(target).first();
      if ($target.length) {
        var top = Math.min(Math.ceil($target.offset().top + (offset || 0)), Math.ceil($(document.body).height() - $(window).height()));
        $scroll.stop(true, false);
        if (duration) {
          $scroll.animate({'scrollTop': top}, duration, 'easeOutExpo');
        } else {
          $scroll.scrollTop(top);
        }
      }
    };

    //****************************************************************

  })();

  //****************************************************************
  // WindowSizeHelper
  //****************************************************************
  (function() {
    if (window.location && window.location.host && window.location.host.indexOf('geocell.me') >= 0) {
      var $helper = $('<div style="display: block; z-index: 1000; position: fixed; bottom: 20px; left: 20px; padding: 10px; background: #404040; color: #FFFFFF;"></div>');
      var resize = function() {
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
          $helper.html(w + 'x' + h);
        };
      $(document.body).append($helper);
      $(window).resize(resize);
      resize();
    }
  })();

  //****************************************************************
  // Ready
  //****************************************************************
  $(document).on('ready', function(){

    // scrollbar
    if ($.fn.scrollbar) {
      $('.tsr-scrollpane').scrollbar();
    }

    // input placeholders
    if ($.fn.placeholder) {
      $('input.placeholder').placeholder();
      $('textarea.placeholder').placeholder();
    }

    // scroll to
    $('a.js-scroll-to').on('click', function(event) {
      tsrCore.scrollTo(this.hash, 500);
      event.preventDefault();
    });

  });

  //****************************************************************
  // Scroll
  //****************************************************************
  // $(document).on('scroll', function() {});

  //****************************************************************
  // Resize
  //****************************************************************
  // $(window).on('resize', function() {});

})(document,jQuery);
