/*
 * jQuery simple slide-show plugin 1.0.0 (2015.03.01)
 *
 * Copyright (c) 2012 Andria Klimov
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 */
(function($) {

  cssHasSupport = (function(){
    function comp(compVal, initVal, name) {
      return compVal === initVal;
    }
    function test(el, name, val, compare) {
      var style = { name: null };
      if (window.getComputedStyle && el.ownerDocument.defaultView.opener) {
        style = el.ownerDocument.defaultView.getComputedStyle(el, null);
      } else if (window.getComputedStyle) {
        style = window.getComputedStyle(el, null);
      } else if (document.documentElement.currentStyle) {
        style = el.currentStyle;
      }
      el.style[name] = '';
      el.style[name] = val;
      return style[name] !== undefined ? compare(style[name] + '', val, name) : false;
    }
    return function cssHasSupport(names, compare) {
      var support = false, el = document.createElement('link');
      for (var name in names) {
        if (names.hasOwnProperty(name)) {
          if (el.style[name] !== undefined) {
            if (!names[name] || test(el, name, names[name], compare || comp)) {
              support = name;
              break;
            }
          }
        }
      }
      return support;
    };
  })();

  var cssTransform3D = cssHasSupport({
          'transform': 'translate3d(0,0,0)',
         'OTransform': 'translate3d(0,0,0)',
        'msTransform': 'translate3d(0,0,0)',
       'MozTransform': 'translate3d(0,0,0)',
    'WebkitTransform': 'translate3d(0,0,0)'
  }, function(compVal, initVal) {
    return compVal.replace(/\s/g,'') === 'matrix(1,0,0,1,0,0)';
  });
  var cssTransform = cssHasSupport({
          'transform': '',
         'OTransform': '',
        'msTransform': '',
       'MozTransform': '',
    'WebkitTransform': ''
  });

  $.fn.slideshow = function(options) {

    // default configuration properties
    var config = {
      data          : 'slideshow',
      animation     : 'fade',
      play          : true,
      continuous    : true,
      reverse       : false,
      index         : 0,
      pauseTime     : 2000,
      switchTime    : 500,
      easing        : 'linear',
      callback      : null
    };

    // update configuration properties
    if (options)
      $.extend(config, options);

    return this.each(function() {

      // slideshow
      function Slideshow(instance) {

        var slideshow = this;

        // slide-objects
        var current = false;
        var replace = false;
        var inqueue = false;

        // play / stop / timeout identificator
        var timeout = false;

        // animation helper
        var helper = {value: 0};

        // animation types
        var animations = {
          fade: {
            init: function(instance, current) {
              $(instance).children().css({zIndex: 0, opacity: 1});
              $(current).css({zIndex: 1});
            },
            reset: function(instance) {
              $(instance).children().stop().css({zIndex: '', opacity: ''});
            },
            animate: function(instance, current, replace, callback, action) {
              $(replace).css({zIndex: 2, opacity: 0}).animate({opacity: 1}, config.switchTime, config.easing, function() {
                $(current).css({zIndex: 0});
                $(replace).css({zIndex: 1});
                callback();
              });
            }
          },
          scroll: {
            init: function(instance, current) {
              if (cssTransform3D) {
                $(instance).children().css({zIndex: 0, opacity: 0}).css(cssTransform3D, 'translate3d(0,0,0)');
                $(current).css({zIndex: 1, opacity: 1});
              } else if (cssTransform) {
                $(instance).children().css({zIndex: 0, opacity: 0}).css(cssTransform, 'translateX(0)');
                $(current).css({zIndex: 1, opacity: 1});
              } else {
                $(instance).children().css({zIndex: 0, opacity: 0, left: 0});
                $(current).css({zIndex: 1, opacity: 1});
              }
            },
            reset: function(instance) {
              if (cssTransform3D) {
                $(helper).stop();
                $(instance).children().css({zIndex: '', opacity: ''}).css(cssTransform3D, '');
              } else if (cssTransform) {
                $(helper).stop();
                $(instance).children().css({zIndex: '', opacity: ''}).css(cssTransform, '');
              } else {
                $(helper).stop();
                $(instance).children().css({zIndex: '', opacity: '', left: ''});
              }
            },
            animate: function(instance, current, replace, callback, action) {
              var dir = $(replace).index() - $(current).index();
              if (Math.abs(dir) == $(instance).children().length - 1 && action != 'show') {
                dir = dir > 0 ? false : true;
              } else {
                dir = dir > 0 ? true : false;
              }
              $(replace).css({zIndex: 1, opacity: 1});
              helper.value = 0;
              $(helper).animate({value: 1}, {
                duration: config.switchTime,
                easing: config.easing,
                step: dir ? function(now, fx) {
                  if (cssTransform3D) {
                    $(current).css(cssTransform3D, 'translate3d(' + (100 * ( 0 - fx.pos)) + '%,0,0)');
                    $(replace).css(cssTransform3D, 'translate3d(' + (100 * (+1 - fx.pos)) + '%,0,0)');
                  } else if (cssTransform) {
                    $(current).css(cssTransform, 'translateX(' + (100 * ( 0 - fx.pos)) + '%)');
                    $(replace).css(cssTransform, 'translateX(' + (100 * (+1 - fx.pos)) + '%)');
                  } else {
                    $(current).css({left: (100 * ( 0 - fx.pos)) + '%'});
                    $(replace).css({left: (100 * (+1 - fx.pos)) + '%'});
                  }
                } : function(now, fx) {
                  if (cssTransform3D) {
                    $(current).css(cssTransform3D, 'translate3d(' + (100 * ( 0 + fx.pos)) + '%,0,0)');
                    $(replace).css(cssTransform3D, 'translate3d(' + (100 * (-1 + fx.pos)) + '%,0,0)');
                  } else if (cssTransform) {
                    $(current).css(cssTransform, 'translateX(' + (100 * ( 0 + fx.pos)) + '%)');
                    $(replace).css(cssTransform, 'translateX(' + (100 * (-1 + fx.pos)) + '%)');
                  } else {
                    $(current).css({left: (100 * ( 0 + fx.pos)) + '%'});
                    $(replace).css({left: (100 * (-1 + fx.pos)) + '%'});
                  }
                },
                complete: function() {
                  if (cssTransform3D) {
                    $(current).css({zIndex: 0, opacity: 0}).css(cssTransform3D, 'translate3d(0,0,0)');
                    $(replace).css({zIndex: 1, opacity: 1}).css(cssTransform3D, 'translate3d(0,0,0)');
                  } else if (cssTransform) {
                    $(current).css({zIndex: 0, opacity: 0}).css(cssTransform, 'translateX(0)');
                    $(replace).css({zIndex: 1, opacity: 1}).css(cssTransform, 'translateX(0)');
                  } else {
                    $(current).css({zIndex: 0, opacity: 0, left: 0});
                    $(replace).css({zIndex: 1, opacity: 1, left: 0});
                  }
                  callback();
                }
              });
            }
          },
          showhide: {
            init: function(instance, current) {
              $(instance).children().css({zIndex: 0, opacity: 0, display: 'none'});
              $(current).css({zIndex: 1, opacity: 1, display: ''});
            },
            reset: function(instance) {
              $(instance).children().css({zIndex: '', opacity: '', display: ''});
            },
            animate: function(instance, current, replace, callback, action) {
              $(current).animate({opacity: 0}, Math.ceil(config.switchTime / 2), config.easing, function() {
                $(current).css({zIndex: 0, opacity: 0, display: 'none'});
                $(replace).css({zIndex: 1, opacity: 0, display: ''}).animate({opacity: 1}, Math.ceil(config.switchTime / 2), config.easing, function() {
                  callback();
                });
              });
            }
          }
        };

        // initialize slideshow
        function init() {
          current = $(instance).children().get(config.index) || $(instance).children().get(0) || false;
          replace = inqueue = false;
          animations[config.animation].init(instance, current);
        }

        // reset slideshow
        function reset() {
          animations[config.animation].reset(instance);
          current = replace = inqueue = false;
        }

        // animate slideshow
        function animate(index, action) {
          var slide = $(instance).children().get(index);
          if (slide) {
            if (replace) {
              if (replace != slide) {
                inqueue = [slide, action];
              }
            } else {
              if (timeout && timeout !== true) {
                window.clearTimeout(timeout);
                timeout = true;
              }
              if (current == slide) {
                if (timeout) {
                  timeout = window.setTimeout(function() {
                    slideshow.once();
                  }, config.pauseTime);
                }
              } else {
                if ($.isFunction(config.callback)) {
                  config.callback.call(instance, $(slide).index(), false, false != inqueue);
                }
                animations[config.animation].animate(instance, current, slide, function() {
                  replace = false;
                  if ($.isFunction(config.callback)) {
                    config.callback.call(instance, $(current).index(), true, false != inqueue);
                  }
                  if (inqueue) {
                    animate($(inqueue[0]).index(), inqueue[1]);
                    inqueue = false;
                  } else if (timeout && (config.continuous || $(current).index() < $(instance).children().length - 1)) {
                    timeout = setTimeout(function() {
                      slideshow.once();
                    }, config.pauseTime);
                  }
                }, action);
                current = replace = slide;
              }
            }
          }
        }

        // create instance by call
        if (!(slideshow instanceof Slideshow)) {
          return new Slideshow();
        }

        // override toString
        slideshow.toString = function toString() {
          return '[object Slideshow]';
        }

        // init
        slideshow.init = function() {
          reset();
          init();
          if (config.play) {
            slideshow.play(true);
          }
        },

        // reset
        slideshow.reset = function() {
          slideshow.stop();
          reset();
        },
        
        // show by index
        slideshow.show = function(index) {
          animate(index || 0, 'show');
        },

        // show previous
        slideshow.prev = function() {
          animate($(current).index() - 1, 'prev');
        },

        // show next
        slideshow.next = function() {
          animate($(current).index() < $(instance).children().length - 1 ? $(current).index() + 1 : 0, 'next');
        },

        // once
        slideshow.once = function() {
          if (config.reverse) {
            slideshow.prev();
          } else {
            slideshow.next();
          }
        },

        // play
        slideshow.play = function(pause) {
          if (pause) {
            if (timeout && timeout !== true) {
              window.clearTimeout(timeout);
            }
            timeout = window.setTimeout(function() {
              slideshow.play();
            }, config.pauseTime);
          } else {
            timeout = true;
            slideshow.once();
          }
        },

        // stop
        slideshow.stop = function() {
          if (timeout && timeout !== true) {
            window.clearTimeout(timeout);
          }
          timeout = false;
        }
      }
      
      // create slideshow
      var slideshow = new Slideshow(this);

      // initialize slideshow
      slideshow.init();

      // store slideshow to element data
      $(this).data(config.data, slideshow);

    });
  };
})(jQuery);
