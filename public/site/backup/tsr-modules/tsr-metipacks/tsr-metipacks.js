/* TSR - MODULE METIPACKS */
;(function(document,$) {

  window.tsrModuleMetipacks = window.tsrModuleMetipacks || {};

  // ********************************************************************************
  // TSR - Init
  tsrModuleMetipacks.tsrInit = function() {
    tsrModuleMetipacks.tsrModuleInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrModuleMetipacks.tsrModuleInit = function() {
    $('.tsr-module-metipacks').not('.tsr-module-metipacks-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $module = $(this).addClass('tsr-module-metipacks-processed');
      var $boxes = $('.tsr-module-metipack', $module);

      // ********************************************************************************
      // Lines
      var tsrLines = new (function() {

        var self = this;
        self.tsrFlagEqualHeights = false;

        self.tsrEnableEqualHeights = function() {
          self.tsrFlagEqualHeights = true;
        };

        self.tsrDisableEqualHeights = function() {
          self.tsrFlagEqualHeights = false;
        };

        self.tsrEqualHeights = function() {
          tsrCore.equalHeights($boxes);
        };

        self.tsrResetHeights = function() {
          tsrCore.resetHeights($boxes);
        };

        self.tsrOnResize = function() {
          if (self.tsrFlagEqualHeights) {
            self.tsrEqualHeights();
          } else {
            self.tsrResetHeights();
          }
        };

        return self;
      });

      // ********************************************************************************
      // Execute

      enquire.register("screen and (max-width: 480px)", {
        setup : function() {
          tsrLines.tsrEnableEqualHeights();
          tsrLines.tsrEqualHeights();
        },
        match : function() {
          tsrLines.tsrDisableEqualHeights();
          tsrLines.tsrResetHeights();
        },
        unmatch : function() {
          tsrLines.tsrEnableEqualHeights();
          tsrLines.tsrEqualHeights();
        }
      });

      $(window).on('tsr-update', function() {
        tsrLines.tsrOnResize();
      });

      $(window).on('load', function() {
        tsrLines.tsrOnResize();
      });

      $(window).on('resize', tsrCore.debounce(function() {
        tsrLines.tsrOnResize();
      }, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Page Ready
  $(tsrModuleMetipacks.tsrInit);

})(document,jQuery);
