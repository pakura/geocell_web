/* TSR - SLIDESHOW */
;(function(document,$) {

  window.tsrSectionSlideshow = window.tsrSectionSlideshow || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionSlideshow.tsrInit = function() {
    tsrSectionSlideshow.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionSlideshow.tsrSectionInit = function() {
    $('.tsr-section-slideshow').not('.tsr-section-slideshow-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $section   = $(this).addClass('tsr-section-slideshow-processed');


      // ********************************************************************************
      // Execute
      $('.tsr-slideshow', $section).each(function() {
        var $slideshow = $(this);

        // ********************************************************************************
        if ($slideshow.hasClass('js-slideshow-type-usual')) {

          var $stage = $('.tsr-slideshow-stage', $slideshow);
          var $list  = $stage.children('ul').first();
          var $items = $list.children('li');

          if ($items.length > 1) {
            if ($slideshow.hasClass('js-slideshow-has-nav')) {
              $slideshow.append('<div class="tsr-slideshow-nav"><ul>' + (new Array($items.length + 1).join('<li><\/li>')) + '<\/ul></div>');
            }
            if ($slideshow.hasClass('js-slideshow-has-dir')) {
              $stage.append('<div class="tsr-slideshow-prev"></div>');
              $stage.append('<div class="tsr-slideshow-next"></div>');
            }
          }

          $slideshow.addClass('tsr-loader');

          tsrCore.waitForImages($slideshow, function() {

            $slideshow.removeClass('tsr-loader');

            var $navs  = $('.tsr-slideshow-nav > ul > li', $slideshow);
            var $prev  = $('.tsr-slideshow-prev', $stage);;
            var $next  = $('.tsr-slideshow-next', $stage);;

            var autoplay = $slideshow.hasClass('js-slideshow-autoplay');

            $list.slideshow({
              animation   : 'scroll',
              play        : autoplay,
              continuous  : true,
              reverse     : false,
              index       : 0,
              pauseTime   : 7000,
              switchTime  : 500,
              easing      : 'easeOutExpo',
              callback    : function(index, reason) {
                $navs.removeClass('tsr-active').eq(index).addClass('tsr-active');
              }
            });

            $navs.on('click', function(event) {
              var slideshow = $list.data('slideshow');
              if (slideshow) {
                slideshow.stop();
                slideshow.show($navs.index(this));
                $navs.removeClass('tsr-active');
                $(this).addClass('tsr-active');
                event.preventDefault();
              }
            }).first().addClass('tsr-active');

            $prev.on('click', function(event) {
              var slideshow = $list.data('slideshow');
              if (slideshow) {
                slideshow.stop();
                slideshow.prev();
              }
              event.preventDefault();
            }).on('mousedown', function(event) {
              event.preventDefault();
            });

            $next.on('click', function(event) {
              var slideshow = $list.data('slideshow');
              if (slideshow) {
                slideshow.stop();
                slideshow.next();
              }
              event.preventDefault();
            }).on('mousedown', function(event) {
              event.preventDefault();
            });
            
            (function() {
              var coords = null;
              function getCoords(event) {
                if (event.touches && event.touches.length === 1) {
                  return [event.touches[0].pageX, event.touches[0].pageY];
                }
                return null;
              };
              function onTouchStart(event) {
                coords = getCoords(event.originalEvent);
                event.stopPropagation();
              };
              function onTouchMove(event) {
                if (coords) {
                  var newcoords = getCoords(event.originalEvent);
                  if (Math.abs(newcoords[0] - coords[0]) > Math.abs(newcoords[1] - coords[1])) {
                    if ((newcoords[0] - coords[0]) > 0) {
                      /* swipe left */
                      var slideshow = $list.data('slideshow');
                      if (slideshow) {
                        slideshow.stop();
                        slideshow.prev();
                      }
                    } else {
                      /* swipe right */
                      var slideshow = $list.data('slideshow');
                      if (slideshow) {
                        slideshow.stop();
                        slideshow.next();
                      }
                    }
                  } else {
                    if ((newcoords[1] - coords[1]) > 0) {
                      /* swipe up */
                    } else {
                      /* swipe down */
                    }
                  }
                }
                coords = null;
                event.stopPropagation();
              };
              function onTouchEnd(event) {
                coords = null;
                event.stopPropagation();
              };
              function onTouchCancel(event) {
                coords = null;
                event.stopPropagation();
              };
              $stage.on('touchstart', onTouchStart).on('touchmove', onTouchMove).on('touchend', onTouchEnd).on('touchcancel', onTouchCancel);
            })();

          });
          
        }
        // ********************************************************************************

      });

      // $(window).on('load', function() {});
      // $(window).on('resize', tsrCore.debounce(function() {}, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Ready
  $(tsrSectionSlideshow.tsrInit);

})(document,jQuery);
