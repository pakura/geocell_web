/* TSR - FOOTER */
;(function(document,$) {

  window.tsrSectionFooter = window.tsrSectionFooter || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionFooter.tsrInit = function() {
    tsrSectionFooter.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionFooter.tsrSectionInit = function() {
    $('.tsr-section-footer').not('.tsr-section-footer-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $section    = $(this).addClass('tsr-section-footer-processed');
      var $navTrigger = $('.tsr-footer-nav-trigger', this);
      var $nav        = $('.tsr-footer-nav', this);
      var $toTop      = $('.tsr-btn-toTop', this);
      var $beGeocell  = $('.tsr-btn-beGeocell', this);

      // ********************************************************************************
      // Initialization
      $('.tsr-nav-level-01', $nav).parent().addClass('has-sub');
      $('.tsr-nav-level-02', $nav).parent().addClass('has-sub');

      // ********************************************************************************
      // Nav
      var tsrNav = new (function() {

        var self = this;

        self.tsrInit = function() {
          self.tsrCollapseAll();
        };

        self.tsrCollapseAll = function() {
          $nav.hide();
          $('.is-expanded', $nav).removeClass('is-expanded');
        };

        self.tsrEnableMobile = function() {
          self.tsrCollapseAll();
          $('.tsr-nav-level-01 > ul > li.has-sub > a', $nav).on('click', function () {
            $(this).parent().toggleClass('is-expanded');
            return false;
          });
        };

        self.tsrDisableMobile = function() {
          self.tsrCollapseAll();
          $('.tsr-nav-level-01 > ul > li.has-sub > a', $nav).off('click');
        };

        return self;
      });

      // ********************************************************************************
      // Execute

      enquire.register("screen and (max-width: 800px)", {
        setup : function() {
          tsrNav.tsrInit();
          tsrNav.tsrDisableMobile();
        },
        match : function() {
          tsrNav.tsrEnableMobile();
        },
        unmatch : function() {
          tsrNav.tsrDisableMobile();
        }
      });

      $navTrigger.on('click', function(event) {
        $nav.slideToggle(300, 'easeOutExpo', function() {
          if ($nav.is(':visible')) {
            $('html,body').animate({'scrollTop': $(document.body).height() - $(window).height()}, 300, 'easeOutExpo');
          }
          event.preventDefault();
        });
      });

      $toTop.on('click',function (event) {
        $('html,body').animate({'scrollTop': 0}, 300, 'easeOutExpo');
        event.preventDefault();
      });

      (function() {
        var fancyboxSpeed  = 150;
        var fancyboxEffect = $('html').hasClass('touch') ? 'none' : 'elastic';
        $beGeocell.fancybox({
          'fitToView'     : false,
          'autoSize'      : false,
          'width'         : '100%',
          'height'        : 'auto',
          'maxWidth'      : 940,
          'scrolling'     : 'visible',
          'openSpeed'     : fancyboxSpeed,
          'closeSpeed'    : fancyboxSpeed,
          'prevSpeed'     : fancyboxSpeed,
          'nextSpeed'     : fancyboxSpeed,
          'openEffect'    : fancyboxEffect,
          'closeEffect'   : fancyboxEffect,
          'prevEffect'    : fancyboxEffect,
          'nextEffect'    : fancyboxEffect,
          'margin'        : 0,
          'padding'       : 0,
          'beforeShow'    : function() {
            if (window.tsrForms) {
              window.tsrForms.tsrInit();
            }
          },
          'afterShow'     : function() {
            $('.fancybox-close').attr('title', null);
          },
          'helpers'       : {
            'overlay'       : { 'closeClick': false }
          }
        });
      })();

      // $(window).on('load', function() {});
      // $(window).on('resize', tsrCore.debounce(function() {}, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Page Ready
  $(tsrSectionFooter.tsrInit);

})(document,jQuery);
