/* TSR - SUPPORT */
;(function(document,$) {

  window.tsrSectionSupportListing = window.tsrSectionSupportListing || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionSupportListing.tsrInit = function() {
    tsrSectionSupportListing.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionSupportListing.tsrSectionInit = function() {
    $('.tsr-section-support-listing').not('.tsr-section-support-listing-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $section = $(this).addClass('tsr-section-support-listing-processed');
      var $items   = $('.tsr-module-support', this);

      // ********************************************************************************
      // Items
      var tsrItems = new (function() {

        var self = this;
        self.tsrFlagEqualHeights = false;

        self.tsrEnableEqualHeights = function() {
          self.tsrFlagEqualHeights = true;
        };

        self.tsrDisableEqualHeights = function() {
          self.tsrFlagEqualHeights = false;
        };

        self.tsrEqualHeights = function() {
          tsrCore.equalHeights($('.tsr-module-support-title', $section));
          tsrCore.equalHeights($('.tsr-module-support-intro', $section));
        };

        self.tsrResetHeights = function() {
          tsrCore.resetHeights($('.tsr-module-support-title', $section));
          tsrCore.resetHeights($('.tsr-module-support-intro', $section));
        };

        self.tsrOnResize = function() {
          if (self.tsrFlagEqualHeights) {
            self.tsrEqualHeights();
          } else {
            self.tsrResetHeights();
          }
        };

        return self;
      });

      // ********************************************************************************
      // Execute

      $section.addClass('tsr-count-' + $items.length);

      enquire.register("screen and (max-width: 480px)", {
        setup : function() {
          tsrItems.tsrEnableEqualHeights();
          tsrItems.tsrEqualHeights();
        },
        match : function() {
          tsrItems.tsrDisableEqualHeights();
          tsrItems.tsrResetHeights();
        },
        unmatch : function() {
          tsrItems.tsrEnableEqualHeights();
          tsrItems.tsrEqualHeights();
        }
      });

      $(window).on('tsr-update', function() {
        tsrItems.tsrOnResize();
      });

      $(window).on('load', function() {
        tsrItems.tsrOnResize();
      });

      $(window).on('resize', tsrCore.debounce(function() {
        tsrItems.tsrOnResize();
      }, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Ready
  $(tsrSectionSupportListing.tsrInit);

})(document,jQuery);
