/* TSR - CONTACTUS LISTING */
;(function(document,$) {

  window.tsrSectionContactusListing = window.tsrSectionContactusListing || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionContactusListing.tsrInit = function() {
    tsrSectionContactusListing.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionContactusListing.tsrSectionInit = function() {
    $('.tsr-section-contactus-listing').not('.tsr-section-contactus-listing-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $section = $(this).addClass('tsr-section-contactus-listing-processed');
      var $items   = $('.tsr-module-contactus', this);

      // ********************************************************************************
      // Items
      var tsrItems = new (function() {

        var self = this;
        self.tsrFlagEqualHeights = false;

        self.tsrEnableEqualHeights = function() {
          self.tsrFlagEqualHeights = true;
        };

        self.tsrDisableEqualHeights = function() {
          self.tsrFlagEqualHeights = false;
        };

        self.tsrEqualHeights = function() {
          tsrCore.equalHeights($('.tsr-module-contactus-title',  $section));
          tsrCore.equalHeights($('.tsr-module-contactus-intro',  $section));
          tsrCore.equalHeights($('.tsr-module-contactus',        $section));
        };

        self.tsrResetHeights = function() {
          tsrCore.resetHeights($('.tsr-module-contactus-title',  $section));
          tsrCore.resetHeights($('.tsr-module-contactus-intro',  $section));
          tsrCore.resetHeights($('.tsr-module-contactus',        $section));
        };

        self.tsrOnResize = function() {
          if (self.tsrFlagEqualHeights) {
            self.tsrEqualHeights();
          } else {
            self.tsrResetHeights();
          }
        };

        return self;
      });

      // ********************************************************************************
      // Execute

      $section.addClass('tsr-count-' + $items.length);

      enquire.register("screen and (max-width: 800px)", {
        setup : function() {
          tsrItems.tsrEnableEqualHeights();
          tsrItems.tsrEqualHeights();
        },
        match : function() {
          tsrItems.tsrDisableEqualHeights();
          tsrItems.tsrResetHeights();
        },
        unmatch : function() {
          tsrItems.tsrEnableEqualHeights();
          tsrItems.tsrEqualHeights();
        }
      });

      $(window).on('tsr-update', function() {
        tsrItems.tsrOnResize();
      });

      $(window).on('load', function() {
        tsrItems.tsrOnResize();
      });

      $(window).on('resize', tsrCore.debounce(function() {
        tsrItems.tsrOnResize();
      }, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Ready
  $(tsrSectionContactusListing.tsrInit);

})(document,jQuery);
