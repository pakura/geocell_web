/* TSR - FAQ LISTING */
;(function(document,$) {

  window.tsrSectionFaqListing = window.tsrSectionFaqListing || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionFaqListing.tsrInit = function() {
    tsrSectionFaqListing.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionFaqListing.tsrSectionInit = function() {
    $('.tsr-section-faq-listing').not('.tsr-section-faq-listing-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $section = $(this).addClass('tsr-section-faq-listing-processed');

      // ********************************************************************************
      // Execute
      $('.tsr-module-faq', $section).each(function() {
        var $button = $('.tsr-module-faq-question', this);
        var $container = $('.tsr-module-faq-answer', this).hide();
        $button.on('click', function(event) {
          if ($container.is(':visible')) {
            $container.stop(true, false).slideUp(300, 'easeOutExpo');
          } else {
            $container.stop(true, false).slideDown(300, 'easeOutExpo');
          }
          event.preventDefault();
        });
      });


      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Ready
  $(tsrSectionFaqListing.tsrInit);

})(document,jQuery);
