/* TSR - HEADER */
;(function(document,$) {

  window.tsrSectionHeader = window.tsrSectionHeader || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionHeader.tsrInit = function() {
    tsrSectionHeader.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionHeader.tsrSectionInit = function() {
    $('.tsr-section-header').not('.tsr-section-header-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $section   = $(this).addClass('tsr-section-header-processed');
      var $nav       = $('.tsr-header-nav', this);
      var $sub       = $('.tsr-header-sub', this);

      // ********************************************************************************
      // Initialize
      $('.tsr-nav-level-01', $nav).parent().addClass('has-sub');
      $('.tsr-nav-level-02', $nav).parent().addClass('has-sub');
      $('.tsr-nav-level-03', $nav).parent().addClass('has-sub');

      // ********************************************************************************
      // Exp
      var tsrExp = new (function() {

        var self = this;

        var anchor = 'data-header-exp-anchor';
        var target = 'data-header-exp-target';

        function onResizeDesktop(event) {
          if (event.data) {
            $section.stop().css({'padding-bottom': event.data.outerHeight()});
          }
        };

        self.tsrCollapse = function() {
          $('[' + target + '="' + $section.attr('data-header-exp') + '"]', $section).removeClass('is-selected');
          $('[' + anchor + '="' + $section.attr('data-header-exp') + '"]', $section).removeClass('is-expanded');
          $section.attr('data-header-exp', null);
        };

        self.tsrCollapseNav = function() {
          $('.is-selected', $nav).removeClass('is-selected');
          $('.is-expanded', $nav).removeClass('is-expanded');
        };

        self.tsrEnableMobile = function() {
          self.tsrCollapse();
          self.tsrCollapseNav();
          // append global clone to nav
          $('.tsr-nav-level-01', $nav).first().append($('.tsr-global-left ul', $section).clone().addClass('tsr-nav-extra'));
          // inner navigation
          $('.has-sub > a', $nav).on('click', function (event) {
            $(this).toggleClass('is-selected');
            $(this).parent().children('[class^="tsr-nav-level-"]').toggleClass('is-expanded');
            event.preventDefault();
          });
          // target/anchors
          $('.tsr-header-nav', $section).each(function() {
            $(this).attr(anchor, 'nav').children('.tsr-btn-close').attr(target, 'nav');
          }).add('.tsr-header-sub', $section).each(function() {
            $('[' + target + '="' + $(this).attr(anchor) + '"]', $section).on('click', function(event) {
              var $target = $('[' + target + '="' + $(this).attr(target) + '"]', $section);
              var $anchor = $('[' + anchor + '="' + $(this).attr(target) + '"]', $section);
              if ($target.hasClass('is-selected')) {
                $target.removeClass('is-selected');
                $anchor.removeClass('is-expanded');
                $section.attr('data-header-exp', null);
              } else {
                self.tsrCollapse();
                self.tsrCollapseNav();
                $target.addClass('is-selected');
                $anchor.addClass('is-expanded');
                $section.attr('data-header-exp', $target.attr(target));
              }
              event.preventDefault();
            });
          });
        };

        self.tsrDisableMobile = function() {
          self.tsrCollapse();
          self.tsrCollapseNav();
          // inner navigation
          $('.has-sub > a', $nav).off('click');
          // target/anchors
          var $nav = $('.tsr-header-nav', $section);
          $nav.add('.tsr-header-sub', $section).each(function() {
            $('[' + target + '="' + $(this).attr(anchor) + '"]', $section).off('click');
          });
          $nav.each(function() {
            $(this).attr(anchor, 'nav').children('.tsr-btn-close').attr(target, null);
          });
          // remove global clone from nav
          $('.tsr-nav-extra', $nav).remove();
        };

        self.tsrEnableDesktop = function() {
          self.tsrCollapse();
          $('.tsr-nav-level-02', $nav).each(function(index) {
            var $anchor = $(this);
            $anchor.attr(anchor, 'nav-' + index);
            $anchor.parent().children('a').attr(target, 'nav-' + index);
            $anchor.children('.tsr-btn-close').attr(target, 'nav-' + index);
          }).add($('.tsr-header-sub', $section)).each(function() {
            $('[' + target + '="' + $(this).attr(anchor) + '"]', $section).on('click', function(event) {
              var $target = $('[' + target + '="' + $(this).attr(target) + '"]', $section);
              var $anchor = $('[' + anchor + '="' + $(this).attr(target) + '"]', $section);
              $(window).off('resize', onResizeDesktop);
              if ($target.hasClass('is-selected')) {
                $section.stop().animate({'padding-bottom': 0}, 300, 'easeOutExpo', function() {
                  $target.removeClass('is-selected');
                  $anchor.removeClass('is-expanded');
                  $section.attr('data-header-exp', null);
                });
              } else {
                self.tsrCollapse();
                $target.addClass('is-selected');
                $anchor.addClass('is-expanded');
                $section.stop().animate({'padding-bottom': $anchor.outerHeight()}, 300, 'easeOutExpo', function() {
                  $(window).on('resize', $anchor, onResizeDesktop);
                });
                $section.attr('data-header-exp', $target.attr(target));
              }
              event.preventDefault();
            });
          });
        };

        self.tsrDisableDesktop = function() {
          self.tsrCollapse();
          var $navs = $('.tsr-nav-level-02', $nav);
          $navs.add($('.tsr-header-sub', $section)).each(function() {
            $('[' + target + '="' + $(this).attr(anchor) + '"]', $section).off('click');
          });
          $navs.each(function() {
            var $anchor = $(this);
            $anchor.attr(anchor, null);
            $anchor.parent().children('a').attr(target, null);
            $anchor.children('.tsr-btn-close').attr(target, null);
          })
          $section.stop().css({'padding-bottom': ''});
          $(window).off('resize', onResizeDesktop);
        };

        return self;
      });

      // ********************************************************************************
      // Execute

      enquire.register("screen and (max-width: 800px)", {
        setup : function() {
          tsrExp.tsrDisableMobile();
          tsrExp.tsrEnableDesktop();
        },
        match : function() {
          tsrExp.tsrDisableDesktop();
          tsrExp.tsrEnableMobile();
        },
        unmatch : function() {
          tsrExp.tsrDisableMobile();
          tsrExp.tsrEnableDesktop();
        }
      });

      // $(window).on('load', function() {});
      // $(window).on('resize', tsrCore.debounce(function() {}, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Page Ready
  $(tsrSectionHeader.tsrInit);

})(document,jQuery);
