/* TSR - REFINER */
;(function(document,$) {

  window.tsrSectionRefiner = window.tsrSectionRefiner || {};

  // ********************************************************************************
  // TSR - Init
  tsrSectionRefiner.tsrInit = function() {
    tsrSectionRefiner.tsrSectionInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrSectionRefiner.tsrSectionInit = function() {
    $('.tsr-section-refiner').not('.tsr-section-refiner-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $section   = $(this).addClass('tsr-section-refiner-processed');
      var $header    = $('.tsr-refiner-header', this);
      var $footer    = $('.tsr-refiner-footer', this);

      // ********************************************************************************
      // Exp
      var tsrExp = new (function() {

        var self = this;

        var anchor = 'data-refiner-exp-anchor';
        var target = 'data-refiner-exp-target';

        var $selects = $('.tsr-refiner-select', $header);

        self.tsrCollapse = function() {
          $footer.hide();
          $('[' + target + ']', $header).removeClass('is-selected');
          $('[' + anchor + ']', $footer).removeClass('is-expanded');
        };

        self.tsrSelectCollapse = function() {
          $selects.children('a').removeClass('is-selected')
          $selects.children('ul').removeClass('is-expanded');
        };

        self.tsrEnable = function() {
          self.tsrCollapse();
          /* selects */
          $('body').on('click', self.tsrSelectCollapse);
          $('ul a', $selects).on('click', function() {
            $('ul a', $selects).removeClass('is-choosen');
            $(this).addClass('is-choosen');
            self.tsrSelectCollapse();
          });
          $selects.children('a').on('click', function(event) {
            if ($(this).hasClass('is-selected')) {
              self.tsrSelectCollapse();
            } else {
              self.tsrSelectCollapse();
              $(this).addClass('is-selected').parent().children('ul').addClass('is-expanded');
            }
            event.preventDefault();
            event.stopPropagation();
          });
          /* expanders */
          $('.tsr-refiner-sub', $footer).each(function() {
            $('[' + target + '="' + $(this).attr(anchor) + '"]', $header).on('click', function(event) {
              var $this = $(this);
              var pending = $this.attr(target);
              var current = $footer.attr('data-refiner-exp');
              if (current) {
                $footer.attr('data-refiner-exp', '');
                $footer.slideUp(300, 'easeOutExpo', function() {
                  $footer.attr('data-refiner-exp', null);
                  $('[' + target + '="' + current + '"]', $header).removeClass('is-selected');
                  $('[' + anchor + '="' + current + '"]', $footer).removeClass('is-expanded');
                  if (pending != current) {
                    $this.trigger('click');
                  }
                });
              } else if (current === undefined || current === false) {
                $footer.attr('data-refiner-exp', '');
                $('[' + target + '="' + $(this).attr(target) + '"]', $header).addClass('is-selected');
                $('[' + anchor + '="' + $(this).attr(target) + '"]', $footer).addClass('is-expanded');
                $footer.stop().slideDown(300, 'easeOutExpo', function() {
                  $footer.attr('data-refiner-exp', $this.attr(target));
                });
              }
              event.preventDefault();
            });
          });
        };

        self.tsrDisable = function() {
          self.tsrCollapse();
          $('body').off('click', self.tsrSelectCollapse);
          $('ul a', $selects).off('click');
          $selects.children('a').off('click');
          $('.tsr-refiner-sub', $footer).each(function() {
            $('[' + target + '="' + $(this).attr(anchor) + '"]', $header).off('click');
          });
        };

        return self;
      });

      // ********************************************************************************
      // Execute
      
      tsrExp.tsrEnable();

      // $(window).on('load', function() {});
      // $(window).on('resize', tsrCore.debounce(function() {}, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Page Ready
  $(tsrSectionRefiner.tsrInit);

})(document,jQuery);
