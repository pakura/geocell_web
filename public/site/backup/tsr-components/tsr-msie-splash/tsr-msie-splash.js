/*****************************************************************/
/* MSIE SPLASH */
(function() {
  var html = '\
    <div class="msie-splash">\
      <div class="msie-splash-content">\
        <h1>Update your browser!<\/h1>\
        <p>In order to view this site properly you need to update to the latest version of the following supported browsers:<\/p>\
        <a class="msie-splash-browser msie-splash-ch" target="_blank" href="http://www.google.com/chrome">Chrome<\/a>\
        <a class="msie-splash-browser msie-splash-fx" target="_blank" href="http://www.firefox.com/">Firefox<\/a>\
        <a class="msie-splash-browser msie-splash-op" target="_blank" href="http://www.opera.com/">Opera<\/a>\
        <a class="msie-splash-browser msie-splash-sf" target="_blank" href="http://www.apple.com/safari/">Safari<\/a>\
        <a class="msie-splash-browser msie-splash-ie" target="_blank" href="http://windows.microsoft.com/ie">Explorer<\/a>\
      <\/div>\
    <\/div>';
  document.write(html);
})();
