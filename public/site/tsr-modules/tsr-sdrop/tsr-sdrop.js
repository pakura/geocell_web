/* TSR - SDROP */
;(function(document,$) {

  window.tsrModuleSDrop = window.tsrModuleSDrop || {};

  // ********************************************************************************
  // TSR - Init
  tsrModuleSDrop.tsrInit = function() {
    tsrModuleSDrop.tsrModuleInit();
  };

  // ********************************************************************************
  // TSR - Module Init
  tsrModuleSDrop.tsrModuleInit = function() {
    $('.tsr-module-sdrop').not('.tsr-module-sdrop-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $module = $(this).addClass('tsr-module-sdrop-processed');

      // ********************************************************************************
      // Execute
      $module.each(function() {
        var $button = $('.tsr-module-sdrop-btn', this);
        var $expand = $('.tsr-module-sdrop-cnt', this);
        if ($module.hasClass('is-expanded')) {
          $button.addClass('is-selected');
          $expand.show();
        } else {
          $button.removeClass('is-selected');
          $expand.hide();
        }
        $button.on('click', function(event) {
          if ($expand.is(':visible')) {
            $module.removeClass('is-expanded');
            $button.removeClass('is-selected');
            $expand.stop(true, false).slideUp(300, 'easeOutExpo');
          } else {
            $module.addClass('is-expanded');
            $button.addClass('is-selected');
            $expand.stop(true, false).slideDown(300, 'easeOutExpo');
          }
          event.preventDefault();
        });
      });

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Page Ready
  $(tsrModuleSDrop.tsrInit);

})(document,jQuery);
