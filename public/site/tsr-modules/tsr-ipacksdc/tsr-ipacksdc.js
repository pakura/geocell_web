/* TSR - DATA CALC */
;(function(document,$) {

  window.tsrModuleIPDC = window.tsrModuleIPDC || {};

  // ********************************************************************************
  // TSR - Init
  tsrModuleIPDC.tsrInit = function() {
    tsrModuleIPDC.tsrModuleInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrModuleIPDC.tsrModuleInit = function() {
    $('.tsr-module-ipacksdc').not('.tsr-module-ipacksdc-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $module = $(this).addClass('tsr-module-ipacksdc-processed');

      // ********************************************************************************
      // Items
      var tsrItems = new (function() {

        var self = this;
        self.tsrFlagEqualHeights = false;

        self.tsrEnableEqualHeights = function() {
          self.tsrFlagEqualHeights = true;
        };

        self.tsrDisableEqualHeights = function() {
          self.tsrFlagEqualHeights = false;
        };

        self.tsrEqualHeights = function() {
          tsrCore.equalHeights($('.tsr-module-ipacksdc-item', $module));
        };

        self.tsrResetHeights = function() {
          tsrCore.resetHeights($('.tsr-module-ipacksdc-item', $module));
        };

        self.tsrOnResize = function() {
          if (self.tsrFlagEqualHeights) {
            self.tsrEqualHeights();
          } else {
            self.tsrResetHeights();
          }
        };

        return self;
      });

      // ********************************************************************************
      // Execute

      enquire.register("screen and (max-width: 480px)", {
        setup : function() {
          tsrItems.tsrEnableEqualHeights();
          tsrItems.tsrEqualHeights();
        },
        match : function() {
          tsrItems.tsrDisableEqualHeights();
          tsrItems.tsrResetHeights();
        },
        unmatch : function() {
          tsrItems.tsrEnableEqualHeights();
          tsrItems.tsrEqualHeights();
        }
      });

      $(window).on('tsr-update', function() {
        tsrItems.tsrOnResize();
      });

      $(window).on('load', function() {
        tsrItems.tsrOnResize();
      });

      $(window).on('resize', tsrCore.debounce(function() {
        tsrItems.tsrOnResize();
      }, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Ready
  $(tsrModuleIPDC.tsrInit);

})(document,jQuery);
