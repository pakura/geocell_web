/* TSR - TABULAR */
;(function(document,$) {

  window.tsrModuleTabular = window.tsrModuleTabular || {};

  // ********************************************************************************
  // TSR - Init
  tsrModuleTabular.tsrInit = function() {
    tsrModuleTabular.tsrModuleAInit();
    tsrModuleTabular.tsrModuleBInit();
  };

  // ********************************************************************************
  // TSR - Module A Init
  tsrModuleTabular.tsrModuleAInit = function() {
    $('.tsr-module-tabular-a').not('.tsr-module-tabular-a-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $module = $(this).addClass('tsr-module-tabular-a-processed');
      var $header = $('.tsr-module-tabular-header', $module);
      var $footer = $('.tsr-module-tabular-footer', $module);

      // ********************************************************************************
      // Exp
      var tsrExp = new (function() {

        var self = this;

        var anchor = 'data-tabular-exp-anchor';
        var target = 'data-tabular-exp-target';

        var $targets = $('[' + target + ']', $header);
        var $anchors = $('[' + anchor + ']', $footer);

        function tsrTarget(pending) { return $targets.filter('[' + target + '="' + pending + '"]'); };
        function tsrAnchor(pending) { return $anchors.filter('[' + anchor + '="' + pending + '"]'); };

        function tsrCollapse() {
          $targets.removeClass('is-selected');
          $anchors.removeClass('is-expanded');
        };

        function tsrToggle(pending, state) {
          tsrTarget(pending).toggleClass('is-selected', state);
          tsrAnchor(pending).toggleClass('is-expanded', state);
        };

        function tsrExpandDefault() {
          tsrToggle($targets.filter('.is-choosen').attr(target) || $targets.attr(target), true);
        };

        self.tsrEnableMobile = function() {
          tsrCollapse();
          $targets.each(function() { $(this).after(tsrAnchor($(this).attr(target))); });
          $targets.on('click', function(event) {
            var pending = $(this).attr(target);
            tsrToggle(pending, !$(this).hasClass('is-selected'));
            $(window).trigger('tsr-update');
            event.preventDefault();
          });
        };

        self.tsrDisableMobile = function() {
          tsrCollapse();
          $targets.each(function() { $footer.prepend(tsrAnchor($(this).attr(target))); });
          $targets.off('click');
        };

        self.tsrEnableDesktop = function() {
          tsrCollapse();
          if (!$module.is('[data-tabular-collapsed]')) {
            tsrExpandDefault();
          }
          $targets.on('click', function(event) {
            var pending = $(this).attr(target);
            if (!$footer.is(':animated')) {
              if (false && $(this).hasClass('is-selected')) {
                var height = $footer.height();
                $footer.stop().css({'height': height}).animate({'height': 0}, 300, 'easeOutExpo', function() {
                  tsrToggle(pending, false);
                });
              } else {
                var height = $footer.height();
                tsrCollapse();
                tsrToggle(pending, true);
                $(window).trigger('tsr-update');
                $footer.stop().css({'height': height}).animate({'height': tsrAnchor(pending).outerHeight()}, 300, 'easeOutExpo', function() {
                  $footer.css({'height': ''});
                });
              }
            }
            event.preventDefault();
          });
        };

        self.tsrDisableDesktop = function() {
          tsrCollapse();
          $targets.off('click');
        };

        self.tsrFlagEqualHeights    = false;
        self.tsrEnableEqualHeights  = function() { self.tsrFlagEqualHeights = true; };
        self.tsrDisableEqualHeights = function() { self.tsrFlagEqualHeights = false; };

        self.tsrEqualHeights = function() {
          tsrCore.equalHeights($targets);
        };
        self.tsrResetHeights = function() {
          tsrCore.resetHeights($targets);
        };

        self.tsrOnResize = function() {
          if (self.tsrFlagEqualHeights) {
            self.tsrEqualHeights();
          } else {
            self.tsrResetHeights();
          }
        };

        return self;
      });

      // ********************************************************************************
      // Execute

      $module.addClass('tsr-count-' + $('.tsr-module-tabular-but', $header).length);

      enquire.register("screen and (max-width: 800px)", {
        setup : function() {
          tsrExp.tsrDisableMobile();
          tsrExp.tsrEnableDesktop();
          tsrExp.tsrEnableEqualHeights();
          tsrExp.tsrEqualHeights();
        },
        match : function() {
          tsrExp.tsrDisableDesktop();
          tsrExp.tsrEnableMobile();
          tsrExp.tsrDisableEqualHeights();
          tsrExp.tsrResetHeights();
        },
        unmatch : function() {
          tsrExp.tsrDisableMobile();
          tsrExp.tsrEnableDesktop();
          tsrExp.tsrEnableEqualHeights();
          tsrExp.tsrEqualHeights();
        }
      });

      $(window).on('tsr-update', function() {
        tsrExp.tsrOnResize();
      });

      $(window).on('load', function() {
        tsrExp.tsrOnResize();
      });

      $(window).on('resize', tsrCore.debounce(function() {
        tsrExp.tsrOnResize();
      }, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // TSR - Module B Init
  tsrModuleTabular.tsrModuleBInit = function() {
    $('.tsr-module-tabular-b').not('.tsr-module-tabular-b-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $module = $(this).addClass('tsr-module-tabular-b-processed');
      var $header = $('.tsr-module-tabular-header', $module);
      var $footer = $('.tsr-module-tabular-footer', $module);

      // ********************************************************************************
      // Exp
      var tsrExp = new (function() {

        var self = this;

        var anchor = 'data-tabular-exp-anchor';
        var target = 'data-tabular-exp-target';

        var $targets = $('[' + target + ']', $header);
        var $anchors = $('[' + anchor + ']', $footer);

        function tsrTarget(pending) { return $targets.filter('[' + target + '="' + pending + '"]'); };
        function tsrAnchor(pending) { return $anchors.filter('[' + anchor + '="' + pending + '"]'); };

        function tsrCollapse() {
          $targets.removeClass('is-selected');
          $anchors.removeClass('is-expanded');
        };

        function tsrToggle(pending, state) {
          tsrTarget(pending).toggleClass('is-selected', state);
          tsrAnchor(pending).toggleClass('is-expanded', state);
        };

        function tsrExpandDefault() {
          tsrToggle($targets.filter('.is-choosen').attr(target) || $targets.attr(target), true);
        };

        self.tsrEnable = function() {
          tsrCollapse();
          if (!$module.is('[data-tabular-collapsed]')) {
            tsrExpandDefault();
          }
          $targets.on('click', function(event) {
            var pending = $(this).attr(target);
            tsrCollapse();
            tsrToggle(pending, !$(this).hasClass('is-selected'));
            $(window).trigger('tsr-update');
            event.preventDefault();
          });
        };

        self.tsrDisable = function() {
          tsrCollapse();
          $targets.off('click');
        };

        self.tsrEnableMobile = function() {
          $targets.each(function() { $(this).after(tsrAnchor($(this).attr(target))); });
        };

        self.tsrDisableMobile = function() {
          $targets.each(function() { $footer.prepend(tsrAnchor($(this).attr(target))); });
        };

        self.tsrFlagEqualHeights    = false;
        self.tsrEnableEqualHeights  = function() { self.tsrFlagEqualHeights = true; };
        self.tsrDisableEqualHeights = function() { self.tsrFlagEqualHeights = false; };

        self.tsrEqualHeights = function() {
          tsrCore.equalHeights($targets);
        };
        self.tsrResetHeights = function() {
          tsrCore.resetHeights($targets);
        };

        self.tsrOnResize = function() {
          if (self.tsrFlagEqualHeights) {
            self.tsrEqualHeights();
          } else {
            self.tsrResetHeights();
          }
        };

        return self;
      });

      // ********************************************************************************
      // Execute

      $module.addClass('tsr-count-' + $('.tsr-module-tabular-but', $header).length);

      enquire.register("screen and (max-width: 800px)", {
        setup : function() {
          tsrExp.tsrDisableMobile();
          tsrExp.tsrEnableEqualHeights();
          tsrExp.tsrEqualHeights();
        },
        match : function() {
          tsrExp.tsrEnableMobile();
          tsrExp.tsrDisableEqualHeights();
          tsrExp.tsrResetHeights();
        },
        unmatch : function() {
          tsrExp.tsrDisableMobile();
          tsrExp.tsrEnableEqualHeights();
          tsrExp.tsrEqualHeights();
        }
      });

      tsrExp.tsrEnable();

      $(window).on('tsr-update', function() {
        tsrExp.tsrOnResize();
      });

      $(window).on('load', function() {
        tsrExp.tsrOnResize();
      });

      $(window).on('resize', tsrCore.debounce(function() {
        tsrExp.tsrOnResize();
      }, 250));

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Page Ready
  $(tsrModuleTabular.tsrInit);

})(document,jQuery);
