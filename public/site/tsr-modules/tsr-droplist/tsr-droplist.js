/* TSR DROPLIST */
;(function(document,$) {

  window.tsrModuleDroplist = window.tsrModuleDroplist || {};

  // ********************************************************************************
  // TSR - Init
  tsrModuleDroplist.tsrInit = function() {
    tsrModuleDroplist.tsrModuleInit();
  };

  // ********************************************************************************
  // TSR - Section Init
  tsrModuleDroplist.tsrModuleInit = function() {
    $('.tsr-module-droplist').not('.tsr-module-droplist-processed').each(function() {

      // ********************************************************************************
      // Vars
      var $module = $(this).addClass('tsr-module-droplist-processed');

      // ********************************************************************************
      // Execute
      $('.tsr-module-droplist-item', $module).each(function() {
        var $button = $('.tsr-module-droplist-header', this);
        var $container = $('.tsr-module-droplist-footer', this);
        if ($container.is(':visible')) {
          $button.addClass('is-choosen');
        } else {
          $button.removeClass('is-choosen');
        }
        $button.on('click', function(event) {
          if ($container.is(':visible')) {
            $button.removeClass('is-choosen');
            $container.stop(true, false).slideUp(300, 'easeOutExpo');
          } else {
            $button.addClass('is-choosen');
            $container.stop(true, false).slideDown(300, 'easeOutExpo');
          }
          event.preventDefault();
        });
      });

      // ********************************************************************************

    });
  };

  // ********************************************************************************
  // Ready
  $(tsrModuleDroplist.tsrInit);

})(document,jQuery);
