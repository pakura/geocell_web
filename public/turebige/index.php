<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Geocell</title>

    <meta charset="utf-8" />

    <meta name="viewport"     content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1" />
    <meta name="author"       content="" />
    <meta name="keywords"     content="" />
    <meta name="description"  content="" />

    <!--[if IE]><meta http-equiv="X-UA-Compatible" static="IE=edge,chrome=1" /><![endif]-->

    <link type="image/x-icon" rel="icon"          href="static/images/requisites/favicon.ico" />
    <link type="image/x-icon" rel="shortcut icon" href="static/images/requisites/favicon.ico" />

    <link type="text/css" href="static/bundles/bundle.css" rel="stylesheet" media="all" />

    <script type="text/javascript" src="static/bundles/bundle-lib.js"></script>
    <script type="text/javascript" src="static/bundles/bundle-ext.js"></script>
    
  </head>
  <body>



    <div class="com-bnr" id="start" style="position: absolute">
      <div class="com-bnr-outer">
        <div class="com-bnr-inner">
          <div class="com-bnr-logo"><a href="http://geocell.ge/"><img src="static/images/elements/logo-flat-mono-ka.png" alt="" /></a></div>
          <div class="com-bnr-slogan">იაფი როუმინგი<br/>ჯეოსელისგან</div>
          <div class="com-bnr-sidebar">
            <div class="com-bnr-title">სად მიდიხარ?</div>
            <div class="com-bnr-search">
              <div class="com-search-box">
                <input type="text" name="search" value="" id="countries-search" data-handle="placeholder" placeholder="მიუთითეთ ქვეყნის სახელი" autocomplete="off" />
                <input type="submit" name="submit" value="&#x31;" onclick="chooseCountry()" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="com-bnr" id="carier" style="display: none; position: absolute;">
      <div class="com-bnr-outer">
        <div class="com-bnr-inner">
          <div class="com-bnr-logo"><a href="http://geocell.ge/"><img src="static/images/elements/logo-flat-mono-ka.png" alt="" /></a></div>
          <div class="com-bnr-slogan">იაფი როუმინგი<br/>ჯეოსელისგან</div>
          <div class="com-bnr-sidebar">
            <div class="com-bnr-title">რომელი ნომრით გსურს მოგზაურობა?</div>
            <div class="com-bnr-buttons"><!--
              --><div class="com-bnr-button"><a href="#" onclick="chooseCarier(1)" class="com-btn com-btn-choice">ჯეოსელი<img src="static/images/elements/check-white.png" alt="" class="com-icon" /></a></div><!--
              --><div class="com-bnr-button"><a href="#" onclick="chooseCarier(2)" class="com-btn com-btn-choice">ლაილაი<img src="static/images/elements/check-white.png" alt="" class="com-icon" /></a></div><!--
            --></div>
          </div>
        </div>
      </div>
    </div>


    <div class="com-bnr" id="final" style="display: none; position: absolute">
      <div class="com-bnr-outer">
        <div class="com-bnr-inner">
          <div class="com-bnr-logo"><a href="http://geocell.ge/"><img src="static/images/elements/logo-flat-mono-ka.png" alt="" /></a></div>
          <div class="com-bnr-fullbar">
            <div class="com-bnr-title">შენი როუმინგის შეთავაზება მზადაა</div>
            <div class="com-bnr-buttons"><!--
              --><div class="com-bnr-button"><a href="#" onclick="openPage()" class="com-btn com-btn-normal com-btn-blue">ნახე დეტალურად</a></div><!--
            --></div>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">/*<![CDATA[*/
      var options = [
        {"id":  1, "name": "Portugal"},
        {"id":  2, "name": "პორტუგალია"},
        {"id":  3, "name": "Canada"},
        {"id":  4, "name": "კანადა"},
        {"id":  5, "name": "China"},
        {"id":  6, "name": "ჩინეთი"},
        {"id":  7, "name": "Singapore"},
        {"id":  1, "name": "სინგაპური"},
        {"id":  1, "name": "Iraq"},
        {"id":  1, "name": "ერაყი"},
        {"id":  1, "name": "Mexico"},
        {"id":  1, "name": "მექსიკა"},
        {"id":  1, "name": "Maldives"},
        {"id":  1, "name": "მალდივი"},
        {"id":  1, "name": "Lithuania"},
        {"id":  1, "name": "ლიტვა"},
        {"id":  1, "name": "Latvia"},
        {"id":  1, "name": "ლატვია"},
        {"id":  1, "name": "Austria"},
        {"id":  1, "name": "ავსტრია"},
        {"id":  1, "name": "Afghanistan"},
        {"id":  1, "name": "ავღანეთი"},
        {"id":  1, "name": "Liechtenstein"},
        {"id":  1, "name": "ლიხტენშტაინი"},
        {"id":  1, "name": "Slovenia"},
        {"id":  1, "name": "სლოვენია"},
        {"id":  1, "name": "Cameroon"},
        {"id":  1, "name": "კამერუნი"},
        {"id":  1, "name": "Anguilla"},
        {"id":  1, "name": "ანგუილა"},
        {"id":  1, "name": "Andorra"},
        {"id":  1, "name": "ანდორა"},
        {"id":  1, "name": "Antigua & Barbuda"},
        {"id":  1, "name": "ანტიგუა და ბარბუდა"},
        {"id":  1, "name": "Barbados"},
        {"id":  1, "name": "ბარბადოსი"},
        {"id":  1, "name": "Belgium"},
        {"id":  1, "name": "ბელგია"},
        {"id":  1, "name": "Cyprus"},
        {"id":  1, "name": "კვიპროსი"},
        {"id":  1, "name": "Belarus"},
        {"id":  1, "name": "ბელორუსი"},
        {"id":  1, "name": "Bermuda"},
        {"id":  1, "name": "ბერმუდა"},
        {"id":  1, "name": "Brazil"},
        {"id":  1, "name": "ბრაზილია"},
        {"id":  1, "name": "Bulgaria"},
        {"id":  1, "name": "ბულგარეთი"},
        {"id":  1, "name": "Luxembourg"},
        {"id":  1, "name": "ლუქსემბურგი"},
        {"id":  1, "name": "Grenada"},
        {"id":  1, "name": "გრენადა"},
        {"id":  1, "name": "Denmark"},
        {"id":  1, "name": "დანია"},
        {"id":  1, "name": "Dominica"},
        {"id":  1, "name": "დომინიკა"},
        {"id":  1, "name": "Egypt"},
        {"id":  1, "name": "ეგვიპტე"},
        {"id":  1, "name": "Ethiopia"},
        {"id":  1, "name": "ეთიოპია"},
        {"id":  1, "name": "Syrian Arab Republic"},
        {"id":  1, "name": "სირია"},
        {"id":  1, "name": "Estonia"},
        {"id":  1, "name": "ესტონეთი"},
        {"id":  1, "name": "Venezuela"},
        {"id":  1, "name": "ვენესუელა"},
        {"id":  1, "name": "Turkmenistan"},
        {"id":  1, "name": "თურქმენეთი"},
        {"id":  1, "name": "Turks & Caicos"},
        {"id":  1, "name": "თურქსისა და კაიკოს კუნძულები"},
        {"id":  1, "name": "Jamaica"},
        {"id":  1, "name": "იამაიკა"},
        {"id":  1, "name": "Netherlands"},
        {"id":  1, "name": "ნიდერლანდები"},
        {"id":  1, "name": "Montenegro"},
        {"id":  1, "name": "მონტენეგრო"},
        {"id":  1, "name": "Uruguay"},
        {"id":  1, "name": "ურუგვაი"},
        {"id":  1, "name": "Hungary"},
        {"id":  1, "name": "უნგრეთი"},
        {"id":  1, "name": "India"},
        {"id":  1, "name": "ინდოეთი"},
        {"id":  1, "name": "Algeria"},
        {"id":  1, "name": "ალჟირი"},
        {"id":  1, "name": "Uganda"},
        {"id":  1, "name": "უგანდა"},
        {"id":  1, "name": "Trinidad & Tobago"},
        {"id":  1, "name": "ტრინიდადი და ტობაგო"},
        {"id":  1, "name": "Tajikistan"},
        {"id":  1, "name": "ტაჯიკეთი"},
        {"id":  1, "name": "Indonesia"},
        {"id":  1, "name": "ინდონეზია"},
        {"id":  1, "name": "Jordan"},
        {"id":  1, "name": "იორდანია"},
        {"id":  1, "name": "Ireland"},
        {"id":  1, "name": "ირლანდია"},
        {"id":  1, "name": "Iceland"},
        {"id":  1, "name": "ისლანდია"},
        {"id":  1, "name": "Russian Federation"},
        {"id":  1, "name": "რუსეთი"},
        {"id":  1, "name": "Armenia"},
        {"id":  1, "name": "სომხეთი"},
        {"id":  1, "name": "Slovakia"},
        {"id":  1, "name": "სლოვაკეთი"},
        {"id":  1, "name": "Serbia"},
        {"id":  1, "name": "სერბეთი"},
        {"id":  1, "name": "France"},
        {"id":  1, "name": "საფრანგეთი"},
        {"id":  1, "name": "Greece"},
        {"id":  1, "name": "საბერძნეთი"},
        {"id":  1, "name": "Pakistan"},
        {"id":  1, "name": "პაკისტანი"},
        {"id":  1, "name": "Vietnam"},
        {"id":  1, "name": "ვიეტნამი"},
        {"id":  1, "name": "Mongolia"},
        {"id":  1, "name": "მონღოლეთი"},
        {"id":  1, "name": "Romania"},
        {"id":  1, "name": "რუმინეთი"},
        {"id":  1, "name": "Peru"},
        {"id":  1, "name": "პერუ"},
        {"id":  1, "name": "Italy"},
        {"id":  1, "name": "იტალია"},
        {"id":  1, "name": "Cayman Islands"},
        {"id":  1, "name": "კაიმანის კუნძულები"},
        {"id":  1, "name": "Cambodia"},
        {"id":  1, "name": "კამბოჯა"},
        {"id":  1, "name": "Panama"},
        {"id":  1, "name": "პანამა"},
        {"id":  1, "name": "Poland"},
        {"id":  1, "name": "პოლონეთი"},
        {"id":  1, "name": "Palestine"},
        {"id":  1, "name": "პალესტინა"},
        {"id":  1, "name": "Nigeria"},
        {"id":  1, "name": "ნიგერია"},
        {"id":  1, "name": "Kenya"},
        {"id":  1, "name": "კენია"},
        {"id":  1, "name": "Malta"},
        {"id":  1, "name": "მალტა"},
        {"id":  1, "name": "Morocco"},
        {"id":  1, "name": "მაროკო"},
        {"id":  1, "name": "Korea (South)"},
        {"id":  1, "name": "კორეა (სამხრეთი)"},
        {"id":  1, "name": "Mauritius"},
        {"id":  1, "name": "მავრიკი"},
        {"id":  1, "name": "Moldova, Republic of"},
        {"id":  1, "name": "მოლდოვა"},
        {"id":  1, "name": "Kyrgyz Republic"},
        {"id":  1, "name": "ყირგიზეთი"},
        {"id":  1, "name": "Finland"},
        {"id":  1, "name": "ფინეთი"},
        {"id":  1, "name": "Philippines"},
        {"id":  1, "name": "ფილიპინები"},
        {"id":  1, "name": "Haiti"},
        {"id":  1, "name": "ჰაიტი"},
        {"id":  1, "name": "St. Lucia"},
        {"id":  1, "name": "წმინდა ლუსიას კუნძულები"},
        {"id":  1, "name": "St. Kitts & Nevis"},
        {"id":  1, "name": "წმინდა კიტსის და ნევისის კუნძულები"},
        {"id":  1, "name": "St.Vincent"},
        {"id":  1, "name": "წმინდა ვინსენტის კუნძულები"},
        {"id":  1, "name": "Sweden"},
        {"id":  1, "name": "შვედეთი"},
        {"id":  1, "name": "Sudan"},
        {"id":  1, "name": "სუდანი"},
        {"id":  1, "name": "Azerbaijan"},
        {"id":  1, "name": "აზერბაიჯანი"},
        {"id":  1, "name": "United Arab Emirates"},
        {"id":  1, "name": "არაბთა გაერთიანებული საამიროები"},
        {"id":  1, "name": "United States"},
        {"id":  1, "name": "აშშ"},
        {"id":  1, "name": "New Zealand"},
        {"id":  1, "name": "ახალი ზელანდია"},
        {"id":  1, "name": "Germany"},
        {"id":  1, "name": "გერმანია"},
        {"id":  1, "name": "Spain"},
        {"id":  1, "name": "ესპანეთი"},
        {"id":  1, "name": "Turkey"},
        {"id":  1, "name": "თურქეთი"},
        {"id":  1, "name": "Israel"},
        {"id":  1, "name": "ისრაელი"},
        {"id":  1, "name": "Nepal"},
        {"id":  1, "name": "ნეპალი"},
        {"id":  1, "name": "Norway"},
        {"id":  1, "name": "ნორვეგია"},
        {"id":  1, "name": "Iran, Islamic Republic of"},
        {"id":  1, "name": "ირანი"},
        {"id":  1, "name": "Kazakhstan"},
        {"id":  1, "name": "ყაზახეთი"},
        {"id":  1, "name": "Ukraine"},
        {"id":  1, "name": "უკრაინა"},
        {"id":  1, "name": "Aruba"},
        {"id":  1, "name": "არუბა"},
        {"id":  1, "name": "Uzbekistan"},
        {"id":  1, "name": "უზბეკეთი"},
        {"id":  1, "name": "Czech Republic"},
        {"id":  1, "name": "ჩეხეთის რესპუბლიკა"},
        {"id":  1, "name": "Croatia"},
        {"id":  1, "name": "ხორვატია"},
        {"id":  1, "name": "South Africa"},
        {"id":  1, "name": "სამხრეთ აფრიკა"},
        {"id":  1, "name": "Albania"},
        {"id":  1, "name": "ალბანეთი"},
        {"id":  1, "name": "Great Britain"},
        {"id":  1, "name": "დიდი ბრიტანეთი"},
        {"id":  1, "name": "Namibia"},
        {"id":  1, "name": "ნამიბია"},
        {"id":  1, "name": "Burundi"},
        {"id":  1, "name": "ბურუნდი"},
        {"id":  1, "name": "Zimbabwe"},
        {"id":  1, "name": "ზიმბაბვე"},
        {"id":  1, "name": "Cuba"},
        {"id":  1, "name": "კუბა"},
        {"id":  1, "name": "Macedonia, The Former Yugoslav Republic of"},
        {"id":  1, "name": "მაკედონია"},
        {"id":  1, "name": "Sierra Leone"},
        {"id":  1, "name": "სიერა ლეონე"},
        {"id":  1, "name": "Australia"},
        {"id":  1, "name": "ავსტრალია"},
        {"id":  1, "name": "Argentina"},
        {"id":  1, "name": "არგენტინა"},
        {"id":  1, "name": "Sri Lanka"},
        {"id":  1, "name": "შრი-ლანკა"},
        {"id":  1, "name": "Malaysia"},
        {"id":  1, "name": "მალაიზია"},
        {"id":  1, "name": "Japan"},
        {"id":  1, "name": "იაპონია"},
        {"id":  1, "name": "Switzerland"},
        {"id":  1, "name": "შვეიცარია"},
        {"id":  1, "name": "Oman"},
        {"id":  1, "name": "ომანი"},
        {"id":  1, "name": "Tunisia"},
        {"id":  1, "name": "ტუნისი"},
        {"id":  1, "name": "Kuwait"},
        {"id":  1, "name": "ქუვეითი"},
        {"id":  1, "name": "Qatar"},
        {"id":  1, "name": "კატარი"},
        {"id":  1, "name": "Thailand"},
        {"id":  1, "name": "ტაილანდი"},
        {"id":  1, "name": "Monaco"},
        {"id":  1, "name": "მონაკო"},
        {"id":  1, "name": "Saudi Arabia"},
        {"id":  1, "name": "საუდის არაბეთი"},
        {"id":  1, "name": "Taiwan, Province of China"},
        {"id":  1, "name": "ტაივანი"},
        {"id":  1, "name": "Bosnia & Herzegovina"},
        {"id":  1, "name": "ბოსნია-ჰერცეგოვინა"},
        {"id":  1, "name": "Seychelles"},
        {"id":  1, "name": "სეიშელი"},
        {"id":  1, "name": "Mozambique"},
        {"id":  1, "name": "მოზამბიკი"},
        {"id":  1, "name": "Madagascar"},
        {"id":  1, "name": "მადაგასკარი"},
        {"id":  1, "name": "Hong Kong"},
        {"id":  1, "name": "ჰონკონგი"},
        {"id":  1, "name": "Lebanon"},
        {"id":  1, "name": "ლიბანი"},
        {"id":  1, "name": "Bangladesh"},
        {"id":  1, "name": "ბანგლადეში"},
        {"id":  1, "name": "Bahrain"},
        {"id":  1, "name": "ბაჰრეინი"},
        {"id":  1, "name": "Gibraltar"},
        {"id":  1, "name": "გიბრალტარი"},
        {"id":  1, "name": "Yemen"},
        {"id":  1, "name": "იემენი"},
        {"id":  1, "name": "Cote d\'Ivoire"},
        {"id":  1, "name": "კოტ დ\'ივუარი(სპილოს ძვლის სანაპირო)"},
        {"id":  1, "name": "Isle of Man"},
        {"id":  1, "name": "კუნძული მანი"},
        {"id":  1, "name": "Macau"},
        {"id":  1, "name": "მაკაო"},
        {"id":  1, "name": "Mali"},
        {"id":  1, "name": "მალი"},
        {"id":  1, "name": "Faroe Islands"},
        {"id":  1, "name": "ფარერის კუნძულები"},
        {"id":  1, "name": "Jersey"},
        {"id":  1, "name": "ჯერსი"}
      ];
      $(function() {
        $('#countries-search').bind('keypress', function(e) {
          if(e.keyCode==13){
            chooseCountry();
          }
        });
       $('#countries-search').autoComplete({
          minChars: 1,
          menuClass: 'com-bnr-ac',
          source: function(term, suggest) {
            term = term.toLowerCase();
            var matches = [];
            for (var i = 0, len = options.length; i < len; ++i) {
              if (options[i].name.toLowerCase().indexOf(term) === 0) {
                matches.push(options[i].name);
                if (matches.length >= 3) {
                  break;
                }
              }
            }
            suggest(matches);
          },
          renderItem: function (item, search, index) {
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            return (index ? ', ' : '') + '<div class="autocomplete-suggestion" data-val="' + item + '">' + item.replace(re, "<b>$1</b>") + '</div>';
          },
          onSelect: function(event, term, item) {
            chooseCountry(term);
          }
        });

      });


      var country = '';
      function chooseCountry(arg) {
          if(typeof arg != 'undefined'){
              country = arg;
          } else {
              if($('#countries-search').val() != ''){
                  country = $('#countries-search').val();
              } else {
                  return false;
              }
          }
        var equals = false;
        for(var ii in options){
            if(country == options[ii].name){
              equals = true;
            }
        }

        showCariers();
//        if(equals === true){
//          showCariers();
//        }
      }


      function showCariers() {
        $('#carier').fadeIn(600, function () {
          $('#start').fadeOut(1000);
          $('#final').fadeOut(1000);
        });

//          document.getElementById('carier').style.display = 'block';
//          document.getElementById('start').style.display = 'none';
//          document.getElementById('final').style.display = 'none';
      }
      var carrier = 1;
      function chooseCarier(arg){
        carrier = arg;

        $('#final').fadeIn(600, function () {
          $('#carier').fadeOut(1000);
          $('#start').fadeOut(1000);
        });
//        document.getElementById('final').style.display = 'block';
//        document.getElementById('carier').style.display = 'none';
//        document.getElementById('start').style.display = 'none';
      }

      function openPage() {
          var url = "https://geocell.ge/developer_version/public/ge/private/roaming/cheap-roaming/roaming-landing?name="+country+"&carrier="+carrier;
          var win = window.open(url, '_blank');
          win.focus();
        carrier = 1;
        country = '';
        $('#countries-search').val('');

        document.getElementById('start').style.display = 'block';
        document.getElementById('carier').style.display = 'none';
        document.getElementById('final').style.display = 'none';
      }


    /*]]>*/</script>


  </body>
</html>
