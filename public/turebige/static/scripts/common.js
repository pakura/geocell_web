//****************************************************************
// Startup
//****************************************************************
(function(core) {

  //****************************************************************
  // Document Ready
  //****************************************************************
  $(document).on('ready', function(event) {

    /* document initialization */
    $(document).trigger('com-init');

    /* update dinamic elements */
    $(window).trigger('com-update');
    
  });

  //****************************************************************
  // Window Load
  //****************************************************************
  $(window).on('load', function() {

    /* update dinamic elements */
    $(window).trigger('com-update');

  });

  //****************************************************************
  // Fix Content
  //****************************************************************
  $(document).on('com-init', function(event) {
    var selector = event.target;

    // bind handles
    handle.bind($('[data-handle]', selector));

  });

  //****************************************************************

})(window.core, window.handle);
