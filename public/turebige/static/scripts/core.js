//****************************************************************
// JavaScript Core and Functions
//****************************************************************
(function() {
  //****************************************************************
  // Use Strict Mode
  //****************************************************************
  "use strict";
  
  //****************************************************************
  // Core Object
  //****************************************************************
  window.core = new (function Core() {

    //****************************************************************
    // Save this
    //****************************************************************
    var core = this;

    //****************************************************************
    // CSS Has Support
    //****************************************************************
    core.cssHasSupport = (function(){
      function comp(name, val) {
        return this[name] === val;
      }
      function test(el, name, val, compare) {
        var style = { name: null };
        if (window.getComputedStyle && el.ownerDocument.defaultView.opener) {
          style = el.ownerDocument.defaultView.getComputedStyle(el, null);
        } else if (window.getComputedStyle) {
          style = window.getComputedStyle(el, null);
        } else if (document.documentElement.currentStyle) {
          style = el.currentStyle;
        }
        el.style[name] = '';
        el.style[name] = val;
        return style[name] !== undefined ? compare.call(names, name, style[name] + '') : false;
      }
      return function cssHasSupport(names, compare) {
        var support = false, el = document.createElement('link');
        for (var name in names) {
          if (names.hasOwnProperty(name)) {
            if (el.style[name] !== undefined) {
              if (!names[name] || test(el, name, names[name], compare || comp)) {
                support = name;
                break;
              }
            }
          }
        }
        return support;
      };
    })();

    //****************************************************************
    // Image Preloader
    //****************************************************************
    core.imagePreloader = new (function imagePreloader() {
      if (!(this instanceof imagePreloader))
        return new imagePreloaders();
      var instance = new Object();
      this.set = function(id, src) {
        if (document && document.images) {
          instance[id] = new Image();
          instance[id].src = src;
        }
        return instance[id];
      }
      this.get = function(id, params) {
        if (instance[id]) {
          var image = new Image();
          image.src = self[id].src;
          if (params) {
            for (property in params) {
              if (params.hasOwnProperty(property)) {
                instance[id][property] = params[property];
              }
            }
          }
          return image;
        }
        return false;
      }
    });

    //****************************************************************
    // Wait For Images
    //****************************************************************
    core.waitForImages = (function() {
      var getBackgroundImage = function(el) { return el.style ? el.style.backgroundImage : undefined; }
      if (window.getComputedStyle) {
        getBackgroundImage = function(el) { return el.ownerDocument.defaultView.opener ? el.ownerDocument.defaultView.getComputedStyle(el, null).backgroundImage : window.getComputedStyle(el, null).backgroundImage; }
      } else if (document.documentElement.currentStyle) {
        getBackgroundImage = function(el) { return el.currentStyle.backgroundImage; }
      }
      return function(selector, callback) {
        var $selector = $(selector).add('*', selector);
        var images = new Array();
        var queue = $selector.length;
        var deque = function(src) {
          var img = new Image();
          images.push(img);
          img.src = src;
          if (img.complete) {
            --queue;
          } else {
            $(img).one('load error', function done() {
              if (!--queue) {
                callback();
              }
            });
          }
        }
        $selector.each(function() {
          var bgi = getBackgroundImage(this);
          if (!bgi || bgi == 'none' || bgi.match(RegExp('^url\\(["\']?[^"\']+["\']?\\)$', 'gi')) == null) {
            --queue;
          } else {
            deque(bgi.replace(RegExp('^url\\(["\']?|["\']?\\)$', 'gi'), ''));
          }
          if (this.tagName && this.tagName.toUpperCase() === 'IMG' && this.src) {
            ++queue;
            deque(this.src);
          }
        });
        if (!queue) {
          callback();
        }
      };
    })();

    //****************************************************************
    // Parse Background-Image URL
    //****************************************************************
    core.parseBackgroundImageUrl = function(bgi) {
      if (bgi.match(RegExp('^url\\(["\']?[^"\']+["\']?\\)$', 'gi')) !== null) {
        return bgi.replace(RegExp('^url\\(["\']?|["\']?\\)$', 'gi'), '');
      }
    };

    //****************************************************************
    // Get ScrollBar Size
    //****************************************************************
    core.getScrollBarSize = (function() {
      var size = null;
      var test = $('<textarea></textarea>').css({
        'width': '1000px', 'height': '1000px',
        'overflow': 'scroll', 'z-index': '-1000',
        'padding:': '0', 'border': '0', 'margin': '0',
        'position': 'absolute', 'visibility': 'hidden'
      }).get(0);
      var update = function() {
        if (document.body) {
          $(document.body).append(test);
          if (test.clientWidth !== undefined && test.offsetWidth !== undefined) {
            size = [test.offsetWidth - test.clientWidth, test.offsetHeight - test.clientHeight]
          }
          $(test).remove();
        }
      };
      $(window).on('resize', update);
      return function() {
        if (null === size) {
          update();
        }
        return size;
      };
    })();

    //****************************************************************
    // Viewport Width
    //****************************************************************
    core.viewportWidth = function() {
      return window.innerWidth || document.documentElement.clientWidth || document.body.clientHeight;
    };

    //****************************************************************
    // Viewport Width
    //****************************************************************
    core.viewportHeight = function() {
      return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    };

    //****************************************************************
    // Equal Widths
    //****************************************************************
    core.equalWidths = function(selector, extra) {
      var $selector = $(selector);
      $selector.width('auto').width((extra || 0) + Math.max.apply(null, $selector.map(function() {
        return $(this).width();
      }).get()));
    };

    //****************************************************************
    // Equal Heights
    //****************************************************************
    core.equalHeights = function(selector, extra) {
      var $selector = $(selector);
      $selector.height('auto').height((extra || 0) + Math.max.apply(null, $selector.map(function() {
        return $(this).height();
      }).get()));
    };

    //****************************************************************
    // Reset Widths
    //****************************************************************
    core.resetWidths = function(selector) {
      $(selector).width('');
    };

    //****************************************************************
    // Reset Heights
    //****************************************************************
    core.resetHeights = function(selector) {
      $(selector).height('');
    };

    //****************************************************************
    // Debounce Function
    //****************************************************************
    core.debounce = function(func, threshold, execAsap) {
      var timeout = null;
      return function() {
        var obj = this, args = arguments;
        if (timeout) {
          clearTimeout(timeout);
        } else if (execAsap) {
          func.apply(obj, args);
        }
        timeout = window.setTimeout(function() {
          if (!execAsap) {
            func.apply(obj, args);
          }
          timeout = null;
        }, threshold || 200);
      };
    };

    //****************************************************************
    // Scroll
    //****************************************************************
    core.scrollTo = (function() {
      var anima = {value: 0}, $anima = $(anima);
      var $window = $(window), $document = $(document);
      var $document = $(document);
      return function(target, duration, offset, easing, callback) {
        var pos = $window.scrollTop(), step = null;
        offset = offset || 0;
        if (typeof target == typeof Number()) {
          step = function(now, tween) {
            var top = target + offset;
            top = Math.max(0, Math.min(top, $document.height() - $window.height()));
            $window.scrollTop(pos + (top - pos) * now);
          };
        } else {
          var $el = $(target).first();
          if ($el.length) {
            step = function(now, tween) {
              var top = $el.offset().top + offset;
              top = Math.max(0, Math.min(top, $document.height() - $window.height()));
              $window.scrollTop(pos + (top - pos) * now);
            };
          }
        }
        if (step !== null) {
          $anima.stop(true, false);
          if (duration) {
            step(anima.value = 0);
            $anima.animate({value: 1}, {
              duration: duration,
              easing: easing || 'swing',
              complete: callback,
              step: step
            });
          } else {
            step(1);
            if ($.isFunction(callback)) {
              callback();
            }
          }
        } else {
          $anima.stop(true, false);
        }
      };
    })();

    //****************************************************************
    // Is Mobile Device
    //****************************************************************
    core.isMobile = function() {
      return !!(jQuery.browser && jQuery.browser.mobile);
    };

    //****************************************************************
    // Is Touch Device
    //****************************************************************
    core.isTouch = function() {
      return !!(('ontouchstart' in window) || ('DocumentTouch' in window && document instanceof DocumentTouch) || ('maxTouchPoints' in navigator));
    };

    //****************************************************************
    // Toggle Swipe Events
    //****************************************************************
    core.toggleSwipeEvents = function(selector, toggle) {
      $(selector).each(function() {
        var $selector = $(this);
        var dataStoreName = '__swipe_handler__';
        var handler = $selector.data(dataStoreName);
        if (toggle) {
          if (!handler) {
            handler = new (function() {
              var self = this, coords = null;
              self.getCoords = function(event) {
                if (event.touches && event.touches.length === 1) {
                  return [event.touches[0].pageX, event.touches[0].pageY];
                }
                return null;
              };
              self.onTouchStart = function(event) {
                coords = self.getCoords(event.originalEvent);
                event.stopPropagation();
              };
              self.onTouchMove = function(event) {
                if (coords) {
                  var newcoords = self.getCoords(event.originalEvent);
                  if (Math.abs(newcoords[0] - coords[0]) > Math.abs(newcoords[1] - coords[1])) {
                    if ((newcoords[0] - coords[0]) > 0) {
                      // swipe left
                      $(event.target).trigger('swipe-left')
                    } else {
                      // swipe right
                      $(event.target).trigger('swipe-right')
                    }
                  } else {
                    if ((newcoords[1] - coords[1]) > 0) {
                      // swipe up
                      $(event.target).trigger('swipe-up')
                    } else {
                      // swipe down
                      $(event.target).trigger('swipe-down')
                    }
                  }
                }
                coords = null;
                event.stopPropagation();
              };
              self.onTouchEnd = function(event) {
                coords = null;
                event.stopPropagation();
              };
              self.onTouchCancel = function(event) {
                coords = null;
                event.stopPropagation();
              };
              return self;
            })();
            $selector.data(dataStoreName, handler)
            .on('touchcancel', handler.onTouchCancel)
            .on('touchstart', handler.onTouchStart)
            .on('touchmove', handler.onTouchMove)
            .on('touchend', handler.onTouchEnd);
          }
        } else {
          if (handler) {
            $selector.data(dataStoreName, undefined)
            .off('touchcancel', handler.onTouchCancel)
            .off('touchstart', handler.onTouchStart)
            .off('touchmove', handler.onTouchMove)
            .off('touchend', handler.onTouchEnd);
          }
        }
      });
    };

    //****************************************************************
    // Input Filter
    //****************************************************************
    core.ifilter = (function() {
      var timeout = null;
      var flick = function(el) {
        $(el).trigger('cc-error', true);
        if (timeout) {
          window.clearTimeout(timeout);
        }
        timeout = window.setTimeout(function() {
          timeout = null;
          $(el).trigger('cc-error', false);
        }, 300);
      };
      var $select = function(selector) {
        return $(selector).filter('input[type="text"], input[type="search"], input[type="password"], textarea');
      };
      var isAllowed = function(code, shift, ctrl) {
        return $.inArray(code, [8, 9, 13, 27, 46]) !== -1 || // Allow: backspace, tab, enter, escape, delete
          (code == 65 && ctrl === true) || (code == 67 && ctrl === true) || (code == 88 && ctrl === true) || // Ctrl+A, Ctrl+C, Ctrl+X
          (code >= 33 && code <= 40); // pageup, pagedown, home, end, left, right, top, down
      };
      var isDigit = function(code, shift, ctrl) {
        return (code >= 48 && code <= 57);
      };
      var filterDigits = {
        keypress : function(e) {
          var code = e.keyCode || e.which, shift = !!e.shiftKey, ctrl = !!e.ctrlKey;
          if (!isAllowed(code, shift, ctrl) && !isDigit(code, shift, ctrl)) {
            flick(e.target);
            e.preventDefault();
          }
        },
        change : function(e) {
          var $self = $(e.target);
          if ($self.val().replace(/[0-9]/ig, '').length) {
            $self.val('');
          }
        }
      };
      return {
        on: function(selector, filter) {
          if (filter == 'digits') {
            $select(selector).on('keypress', filterDigits.keypress).on('change', filterDigits.change);
          }
        },
        off: function(selector, filter) {
          if (filter == 'digits' || !filter) {
            $select(selector).off('keypress', filterDigits.keypress).off('change', filterDigits.change);
          }
        }
      };
    })();

    //****************************************************************
    // En2Ka
    //****************************************************************
    core.en2ka = (function() {
      var table = new Object();
      table[65]  = 4304;
      table[66]  = 4305;
      table[67]  = 4329;
      table[68]  = 4307;
      table[69]  = 4308;
      table[70]  = 4324;
      table[71]  = 4306;
      table[72]  = 4336;
      table[73]  = 4312;
      table[74]  = 4319;
      table[75]  = 4313;
      table[76]  = 4314;
      table[77]  = 4315;
      table[78]  = 4316;
      table[79]  = 4317;
      table[70]  = 4318;
      table[81]  = 4325;
      table[82]  = 4326;
      table[83]  = 4328;
      table[84]  = 4311;
      table[85]  = 4323;
      table[86]  = 4309;
      table[87]  = 4333;
      table[88]  = 4334;
      table[89]  = 4327;
      table[90]  = 4331;
      table[97]  = 4304;
      table[98]  = 4305;
      table[99]  = 4330;
      table[100] = 4307;
      table[101] = 4308;
      table[102] = 4324;
      table[103] = 4306;
      table[104] = 4336;
      table[105] = 4312;
      table[106] = 4335;
      table[107] = 4313;
      table[108] = 4314;
      table[109] = 4315;
      table[110] = 4316;
      table[111] = 4317;
      table[112] = 4318;
      table[113] = 4325;
      table[114] = 4320;
      table[115] = 4321;
      table[116] = 4322;
      table[117] = 4323;
      table[118] = 4309;
      table[119] = 4332;
      table[120] = 4334;
      table[121] = 4327;
      table[122] = 4310;
      table[192] = 4337;
      table[193] = 4338;
      table[194] = 4339;
      table[195] = 4340;
      table[196] = 4341;
      var pasteTo = function (field, value) {
        field.focus();
        if (document.selection) {
          var selection = document.selection;
          var range = selection.createRange();
          range.colapse;
          if (range) {
            range.text = value;
          }
        } else if (field.selectionStart || field.selectionEnd) {
          var scrollTop = field.scrollTop, start = field.selectionStart, end = field.selectionEnd;
          var value = field.value.substring(0, start) + value + field.value.substring(end, field.value.length);
          field.value = value;
          field.scrollTop = scrollTop;
          field.selectionStart = field.selectionEnd = start + value.length;
        } else {
          field.value += value;
          field.selectionStart = field.selectionEnd = field.value.length;
        }
      }
      var $select = function(selector) {
        return $(selector).filter('input[type="text"], input[type="search"], input[type="password"], textarea');
      };
      var filterEn2Ka = {
        keypress : function(e) {
          var code = e.keyCode || e.which;
          if (code && table[code] !== undefined) {
            pasteTo(this, String.fromCharCode(table[code]));
            e.preventDefault();
          }
        }
      };
      return {
        on: function(selector) {
          $select(selector).on('keypress', filterEn2Ka.keypress);
        },
        off: function(selector) {
          $select(selector).off('keypress', filterEn2Ka.keypress);
        }
      };
    })();

    //****************************************************************
    // Lightbox
    //****************************************************************
    core.lightbox = (function() {

      var $window = $(window);

      var countLightbox = (function() {
        var counter = 0;
        var updater = function() {
          $window.trigger('com-lightbox', counter);
        };
        return function(toggle) {
          if (toggle) {
            if (!counter++) {
              $window.on('resize', updater);
            }
          } else {
            if (!--counter) {
              $window.off('resize', updater);
            }
          }
          updater();
        };
      })();

      return function(options) {

        var defaults = {
          content       : null,
          method        : null,
          params        : null,
          animate       : true,
          cssclass      : null
        };

        var config = defaults;
        if (options) {
          $.extend(config, options);
        }

        if (config.content) {

          var animaTime = 200;

          var $modal = $('<div class="com-lightbox"><\/div>');
          var $outer = $('<div class="com-lightbox-outer"><\/div>');
          var $inner = $('<div class="com-lightbox-inner"><\/div>');

          var $content = $();

          var onWindowResize = function(event) {
            $modal.trigger('com-lightbox-resize');
          }

          var onCloseClick = function(event) {
            $modal.trigger('com-lightbox-close');
            event.preventDefault();
          }

          var eventDispatcher = function(event, options) {
            if (event.currentTarget == event.target) {
              switch (event.type) {
                case 'com-lightbox-open':
                  contentHide(function() {
                    var config = defaults;
                    if (options) {
                      $.extend(config, options);
                    }
                    contentLoad(config, function() {
                      contentShow();
                    });
                  })
                  break;
                case 'com-lightbox-close':
                 $outer.fadeOut(animaTime);
                  contentHide(function() {
                    modalDestroy();
                  });
                  break;
              }
            }
            return false;
          }

          var modalCreate = function(callback) {
            countLightbox(true);
            $(document.body).append($modal);
            $modal.on('com-lightbox-open com-lightbox-close', eventDispatcher);
            $window.on('resize', onWindowResize);
            if ($.isFunction(callback)) {
              callback();
            }
          }

          var modalDestroy = function(callback) {
            $window.off('resize', onWindowResize);
            $modal.off('com-lightbox-open com-lightbox-close', eventDispatcher);
            $modal.remove();
            countLightbox(false);
            if ($.isFunction(callback)) {
              callback();
            }
          }

          var contentVisibility = function() {
            return parseFloat($content.css('opacity')) || 0.0;
          }

          var contentShow = function(callback) {
            var visibility = 1.0 - contentVisibility();
            if (!visibility) {
              $('.com-lightbox-close', $content).on('click', onCloseClick);
              if ($.isFunction(callback)) {
                callback();
              }
            } else {
              $content.stop().animate({'opacity': '1'}, ($.support.opacity !== false && config.animate) ? animaTime * visibility : 0, function() {
                $content.css({'opacity': ''});
                contentShow(callback);
              });
            }
          }

          var contentHide = function(callback) {
            var visibility = contentVisibility();
            $('.com-lightbox-close', $content).off('click', onCloseClick);
            if (!visibility) {
              if ($.isFunction(callback)) {
                callback();
              }
            } else {
              $content.stop().animate({'opacity': '0'}, ($.support.opacity !== false && config.animate) ? animaTime * visibility : 0, function() {
                $content.css({'opacity': '0'});
                contentHide(callback);
              });
            }
          }

          var contentLoad = function(config, callback) {
            function contentPlace(data, callback) {
              $content = $('<div class="com-lightbox-content">' + data + '<\/div>').hide();
              $inner.html($content).trigger('com-init');
              $modal.trigger('com-lightbox-ready');
              core.waitForImages($content, function() {
                $content.show().css({'opacity': '0'});
                $inner.removeClass('com-lightbox-loader');
                $modal.trigger('com-lightbox-load');
                if ($.isFunction(callback)) {
                  callback();
                }
              });
            }
            $inner.empty().addClass('com-lightbox-loader');
            if (config.method == 'get' || config.method == 'post') {
              $.ajax({
                url: config.content,
                type: config.method,
                data: config.params,
                dataType: 'html',
                success: function(data, textStatus, XMLHttpRequest) {
                  if (XMLHttpRequest.status == 200 && data) {
                    contentPlace(data, callback);
                  } else {
                    modalDestroy();
                    //alert('HTTP Error: ' + XMLHttpRequest.status);
                  }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                  modalDestroy();
                  //alert('HTTP Error: ' + XMLHttpRequest.status);
                }
              });
            } else {
              contentPlace(config.content, callback);
            }
          }

          $modal.append($outer).append($inner).addClass(config.cssclass);

          modalCreate(function() {
            $outer.hide().fadeIn(animaTime);
            contentLoad(config, function() {
              contentShow();
            });
          });
        }

        return false;
      };

    })();

    //****************************************************************
    // Convert Background To Image
    //****************************************************************
    core.convertBackgroundToImage = function(selector) {
      var bgi = $(selector).css('background-image');
      if (typeof bgi == typeof String() && bgi.length && bgi !== 'none') {
        var img = new Image();
        img.src = core.parseBackgroundImageUrl(bgi);
        if (img.complete) {
          $(window).trigger('com-update');
        } else {
          $(img).one('load error', function() {
            $(window).trigger('com-update');
          });
        }
        return img;
      }
      return null;
    };

    //****************************************************************
    // Browser Resolution Hinter
    //****************************************************************
    core.comDebug = function() {
      var $helper = $('<div id="debug" style="display: block; z-index: 1000000; position: fixed; bottom: 20px; left: 20px; padding: 10px; font-size: 14px; background: #000000; color: #FFFFFF; border-radius: 4px; opacity: 0.66;"></div>');
      var resize = function() {
        var x = document.body.clientWidth;
        var y = document.body.clientHeight;
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
          $helper.html(w + 'x' + h + ' (' + x + 'x' + y + ')');
        };
      $(document.body).append($helper);
      $(window).on('resize', resize);
      resize();
    };

    //****************************************************************

  });

  //****************************************************************
})();
