//****************************************************************
// Handle
//****************************************************************
window.handle = new (function(){
  var storageName = '__handles__';
  var handle = this;
  handle.fn = {'sample': function(el) { return {}; }};
  handle.get = function(selector, name) {
    var handles = $(selector).data(storageName);
    return (handles && name in handles) ? handles[name] : handles;
  };
  handle.bind = function(selector, names) {
    $(selector).each(function() {
      var el = this, $el = $(el), handles = $el.data(storageName) || {};
      $.each((names || $el.data('handle') || String()).split(' '), function() {
        if (this in handle.fn && !(this in handles)) {
          handles[this] = handle.fn[this].call(el) || {};
          $el.data(storageName, handles);
        }
      });
    });
  };
  handle.unbind = function(selector, names) {
    $(selector).each(function() {
      var el = this, $el = $(el), handles = $el.data(storageName) || {};
      $.each((names || $el.data('handle') || String()).split(' '), function() {
        if (this in handles) {
          delete handles[this];
        }
      });
    });
  };
});
