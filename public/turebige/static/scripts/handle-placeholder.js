//****************************************************************
// placeholder
//****************************************************************
(function(handle) {

  var supportPlaceholder = 'placeholder' in document.createElement('input');

  handle.fn['placeholder'] = function() {

    //****************************************************************

    if (!supportPlaceholder && $.fn.placeholder) {
      $(this).placeholder({'className': 'input-placeholder'});
    }

    //****************************************************************

  };
})(window.handle);
