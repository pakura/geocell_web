 
$(function () {
   
  /* Collections Section */

  /*show editable field for collection*/
	$('.editcollection').on('click',function(){
      event.preventDefault();
      var elem =  $(this).parent().siblings('.changeTitle');
    
      $('a',elem).hide(); 
      $(this).siblings('button').show();
      $(this).hide();        
      $('.form-block',elem).removeClass('hide');         

    });
  /* cancel editing */
   $('.cancel').on('click',function(){
      event.preventDefault();
      var parent =  $(this).parent();
      var elem =  $(parent).siblings('.changeTitle');
    
      $('a',elem).show();   
      
      $('.form-control',elem).val($('a',elem).html());
      $('.form-block',elem).addClass('hide');   
      $('button',parent).hide();              
      $('.editcollection',parent).show();              

    });
    
    /*update collection   (it is also used for settings value update)*/ 
    $('.save').on('click',function(){
      var parent =  $(this).parent();
      var elem =  $(parent).siblings('.changeTitle');
      var url = $('a',elem).data('url');
      var type = $('a',elem).data('type');
      var value= $('.form-control',elem).val();
      var token = $('input[name="_token"]').val();
      var arr = url.split('/');
      var id = parseInt( arr[arr.length-1]);
      if($(this).data('updatelist')) {
         var collection = $('#collections li[data-id='+id+']');
      }
     
     
      var posting = $.post( url, { _token: token, generic_title: value } );
      posting.done(function( data ) {
          if(type=='image'){
            $('a img',elem).attr('src',value);
          } 
          else if(type=='list'){
             var txt = $('.form-control option[value="'+value+'"]',elem).html();
            $('a',elem).html(txt);    
          }
          else {

            $('a',elem).html(value);    
          }
          $('a',elem).show();

          $('.form-block',elem).addClass('hide');  
          $('button',parent).hide();              
          $('.editcollection',parent).show(); 
          if(collection) {
            $('a',collection).html(value);  
          }

       });
    
    });

    $('body').on('keypress', '.changeTitle input', function(args) {
      if (args.keyCode == 13) {
          $(this).parent().parent().siblings().find(".save").click();
          return false;
      }
    });

   
    /* remove collection from a page  */
     $(document).on('click','.tag span[data-role="remove"]', function(event) {
          var url = $(this).data('url');
          var token = $('input[name="_token"]').val();

          $.post( url, { _token: token } );
          //$(this).parent().remove(); 
           window.location.reload();
    });               
   
     /* delete collection */
     $('.deleteColl').on('click',function(){
        event.preventDefault();
        var url = $(this).attr('href');

        if($(this).hasClass('disable')){
          alert("Please, delete connected items first");
          
        }
        else{

          var conf = confirm("are you sure you want to delete item?");
          if(conf) {  window.location.href=url;   }
         
        }

    });

     /* end Collections Section */

     /*disable other tabs before saving general information */

     /*show-hide description*/
     $('.expand-desc-icon').on('click',function(){
         $(this).parent().removeClass('collapsed-desc');
         $(this).siblings('.hide_description').slideDown();
     });
     $('.collapse-desc-icon').on('click',function(){
         $(this).parent().addClass('collapsed-desc');
         $(this).siblings('.hide_description').slideUp();
     });

     /*File uploads/edit/delete */

     /* show gallery when files tab is active*/
  
    $('.nav-tabs li.showGallery:not(.disabled)').on('click',function(){
           $('.gallery-env').slideDown();
     });

    $('.nav-tabs li:not(.showGallery)').on('click',function(){
           $('.gallery-env').hide();
     });
     
     /*call when delete*/
     function deleteItem(item){
        event.preventDefault();
        var parent =$(item).parents('div[data-action="parent"]');
        var url = $(item).data('url');
        var conf = confirm("are you sure you want to delete item?");
          if(conf) { 
             
             var posting = $.post( url, { } );
              posting.done(function( data ) {
                    
                  $(parent).remove();
               });
          }
     }
      /* delete file */  
     $('.image-options a[data-action="trash"]').on('click',function(){
          
          deleteItem($(this));
          
     });

     /*edit image title*/ 
     $('.image-options a[data-action="edit"]').on('click',function(){
          event.preventDefault();
          var fields = $(this).parent().siblings('.imagetitle');
          $(fields).slideDown('fast');

     });

      $('.imagetitle input').on('blur',function(){
          
          var parent = $(this).parent();
          var title= $(this).val();
          var url = $(this).data('url');
        
          $.post( url, {title:title }, function( data ) {
                    
                //$(parent).hide();   
          });
          
     });

      /* make default photo */
      $(".default_foto").change(function() {
          var checked = 0;

          if(this.checked) { checked=1; }
          var ths = $(this);
          var url = ths.data('url'); 
          var relid = ths.data('relid'); 
         
          $.post( url,{checked:checked,relid:relid}, function( data ) {
              if(checked){ 
                 $('.album-images').find('.cbr-replaced').removeClass('cbr-checked');
                 ths.parents('.cbr-replaced').addClass('cbr-checked');
                 toastr.success(data,null,{"timeOut": "5000"}); 

              }
              else {
                toastr.warning(data,null,{"timeOut": "5000"}); 
              }      
          });    
          

      }); /****/

      /* on page type change show collection list accordingly */
      $('#page_type').change(function() {
          var types = ['4','6','8','9','12','13'];
          val = $(this).val();
          
          if($.inArray(val, types)!=-1){
           
             $.post(cms_slug+'/collections/getlist/'+val,{ }, function( data ) {
                 if(data.length){
                    var txt = '<option value="0"></option>';

                    for(i=0;i<data.length;i++) {
                      txt+='<option value="'+data[i].id+'">'+data[i].generic_title+'</option>';
                    }

                    $('.coll_list').fadeIn();
                    $('#attached_collection_id').html(txt);  
                 }
                
            });    
          
          }
          else {
 
              $('#attached_collection_id').html('');
              $('.coll_list').hide();
          }
      });  

      /* change item visibility */

        $('.visibility').on('click',function(){   
          event.preventDefault();         
          var url = $(this).data('url');
          var visibility;
          ths = $(this);
          if(ths.hasClass('eye-off')) {
            visibility =1; 
             ths.removeClass('eye-off');
          } 
          else { 
            visibility =0;
            ths.addClass('eye-off');
          }
         
            $.post( url,{visibility:visibility}, function( data ) {
                  
                  toastr.success(data,null,{"timeOut": "5000"}); 
                
            });           
           
          
      });
        

      /* Nested items */
      var applied = true; 

      $('.dd').nestable({ /* config options */ })
      .on('change', function(e) {
          $('button[data-action="updateMenuTree"]').removeClass('disabled'); 
          applied =false; 
          var list   = e.length ? e : $(e.target),
              output = $("#nestable-output");
          if (window.JSON) {
             output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
          } else {
              output.val('JSON browser support required for this demo.');
          }
      });

       $('button[data-action="updateMenuTree"]').on('click',function(){ 
          var jsn = $('#nestable-output').val();
         
          var url = $(this).data('url');
          if(jsn){
             jsn = JSON.parse(jsn);
             $.post( url,{jsn:jsn}, function( data ) {
               
                  toastr.success(data,null,{"timeOut": "5000"}); 
                
            });    
          
          }
           applied = true; 
          $(this).addClass('disabled');
      });
        
      window.onbeforeunload = function(){
          if(!applied){
              return 'Your have unsaved changes. If you leave now, they will be lost. Click Apply button to save them.';
          }
      };

        
        /*sort files*/
          $('#item_sort').sortable({
              update: function(event, ui) {
                var itemsOrder = $(this).sortable('toArray');
                var url = $('#item_sort').data('url');
                console.log(url);
                $.post( url,{itemsOrder:itemsOrder}, function( data ) {
                   
                      toastr.success(data,null,{"timeOut": "5000"}); 
                    
                }); 
              }
            });
        
      
      /* attach collection to a page   */
         /*get pages tree*/
       $('.attach_collection').on('click',function(){ 
          event.preventDefault(); 
          var elem =  $('#modal-7');
          elem.modal('show', {backdrop: 'static'});
          elem.data('collid',$(this).data('collid'));
          elem.data('colltype',$(this).data('colltype'));

          if(!elem.hasClass('loaded')){
               var url = $(this).data('url');
       
              $.ajax({
                url: url,
                success: function(response)
                {
                  $('.modal-body',elem).html(response);
                  elem.addClass('loaded');
  
                }
              });

          }        

      });
      


      /* Tabs click change content*/
      
      $(document).on('click','.nav.tabs-vertical li',function(){ 

          var parent = $(this).parent();
          var ths = $(this);
          var div = $('a',ths).attr('href');
         
          $('li',parent).removeClass('active');
          ths.addClass('active');
          parent.siblings().find('.tab-pane').removeClass('active');
          parent.siblings().find(div).addClass('active');

         
      });  

       /*update atached pages*/
     
     $(document).on('click','.choosepage',function(){ 
          var elem =  $('#modal-7');
          var ths = $(this);
          var id = ths.val();
          var txt = ths.siblings().html();
          var collid = elem.data('collid');
          var type = elem.data('colltype');
          var removeurl = cms_slug+'/pages/removeCollection/'+id+'';
          var addurl = cms_slug+'/pages/addCollection/'+id+'';

          var pagesDiv = $('.attached_pages_'+collid);
          var pageExist = pagesDiv.find("#page-"+id);
        
          var html = '<span class="tag  label-info btn" id="page-'+id+'">'
                       + '<a href="'+cms_slug+'/pages/edit/'+id+'">'+txt+'</a>'
                       +'<span data-role="remove" data-url="'+removeurl+'"></span>' 
                       +'</span>';

          if(ths.is(':checked')){
              if(!pageExist.length){
                
                  $.post(addurl, { collid: collid,type:type}, function(data){
                        pagesDiv.append(html);
                        toastr.success(data,null,{"timeOut": "3000"}); 
                       
                    });

              }
             
          }
          else {
              
              $.post(removeurl, { }, function(data){
                    
                    pagesDiv.find("#page-"+id).remove();
                    toastr.success(data,null,{"timeOut": "3000"}); 
                   
                });
          }  

     }); /*end pages collection attachment */

  

   /***** products ******/


   /*  save product option value */
      $(document).on('click','.save-option',function(){ 
           var url = $(this).data('url');
           var parent = $(this).parent().parent();
          //var product_id = $('#product_id').val();
           var option_value = $('.option_value', parent).val();
           var sku = $('.sku_code', parent).val();
           //console.log(option_value);
          $.post(url, { option_value: option_value,sku_code: sku}, function(data){
               toastr.success(data,null,{"timeOut": "3000"}); 
          });  
 
        });


    /* add product in stocks */
    $(document).on('click','.add-in-stock',function(){ 
          
          var url = $(this).data('url');
          var product_id = $('#product_id').val();
          var stock_id = $('#stock_id').val();
          var amount = $('#amount').val();
          
          if(!stock_id){  
              $('#stock_id').parent().addClass('has-error');
              return;
           }
           else { $('#stock_id').parent().removeClass('has-error'); }
            
            if(!amount){  
              $('#amount').parent().addClass('has-error');
              return
            }
            else { $('#amount').parent().removeClass('has-error'); }


          $.post(url, { product_id: product_id,stock_id:stock_id,amount_in_stock:amount}, function(data){
              $('#stock_id option:selected').attr('disabled', 'disabled'); 

              $('#stocklist').html(data);
              $('#amount').val(''); $('#stock_id').val('');
          });  
 
      });   
      
      /*remove product from stock */ 

      $(document).on('click','.remove-from-stock',function(){
          var id = $(this).data('stockid');
          deleteItem($(this));
          $('#stock_id option[value="'+id+'"]').removeAttr('disabled'); 
          
     });

      /* Add product price */

      $(document).on('click','.add-price',function(){ 
          
          var url = $(this).data('url');
          var product_id = $('#product_id').val();
          var stock_id = $('#product_stock_id').val();
          var title = $('#price_title').val();
          var title_en = $('#price_title_en').val();
          var price = $('#price_value').val();
          var currency = $('#currency').val();
          var price_type = $('#price_type').val();
          
          if(!title){  
              $('#price_title').parent().addClass('has-error');
              return;
           }
           else { $('#price_title').parent().removeClass('has-error'); }
            
            if(!price){  
              $('#price_value').parent().addClass('has-error');
              return
            }
            else { $('#price_value').parent().removeClass('has-error'); }


          $.post(url, { product_id: product_id,stock_id:stock_id,title_ge:title,title_en:title_en,value:price,currency:currency,type: price_type}, function(data){
              $('#price_list').append(data);
          });  
 
      });  
      
       /*remove product price */ 

      $(document).on('click','.remove-price',function(){
         
          deleteItem($(this));
          
     });

      /* show stock prices */ 

      $(document).on('click','.show-prices',function(){
          var stockId = $(this).data('stockid');
          
          $('.nav.nav-tabs a[href="#prices"]').trigger('click');
          $('#product_stock_id option[value="'+stockId+'"]').attr('selected','selected'); 
         // $('#price_list').find('[class*="parent-"]:not(.parent-'+stockId+')').hide();
          $('#price_list').find('[class*="parent-"]').hide();
          $('#price_list').find('.parent-'+stockId).show();
     });
      

      /* on product_stock_id change , change prices */


      $(document).on('change','#product_stock_id',function(){
          var stockId = $(this).val();

          if(!stockId || stockId=='default'){
              $('#price_list').find('[class*="parent-"]').show();
          }
          else {
            $('#price_list').find('[class*="parent-"]').hide();
            $('#price_list').find('.parent-'+stockId).show();
          }
          
     });
      

    /* add user role */
    $(document).on('click','.add-role',function(){ 
          
          var url = $(this).data('url');
           
          var role_id = $('#role_id').val();
          var user_id = $('#user_id').val();
           
          if(!role_id){  
              $('#role_id').parent().addClass('has-error');
              return;
           }
           else { $('#role_id').parent().removeClass('has-error'); }


          $.post(url, { role_id: role_id,user_id:user_id}, function(data){
              $('#role_id option:selected').attr('disabled', 'disabled'); 

              $('#roles-list').html(data);
             
          });  
 
      });  
     
      /*remove role from user*/ 

      $(document).on('click','.remove-role',function(){
          var id = $(this).data('roleid');
          deleteItem($(this));
          $('#role_id option[value="'+id+'"]').removeAttr('disabled'); 
          
     });


      /* role pages */  

      $(document).on('change','.onerowhover select, .onerowhover .choosepage',function(){ 
           var obj = $(this).closest('.onerowhover');
           if($(".updateRolePermission",obj).length == 0) {
              obj.append('<button class="btn btn-success updateRolePermission"><i class="fa-check"></i></button');
            } 
           
      });

      $(document).on('click','.onerowhover .updateRolePermission',function(){ 
           event.preventDefault();  
     
          var ths = $(this);
          var obj = ths.parent('.onerowhover');
          var input = $('.choosepage',obj);
          var role_id = $('#role_id').val();
          var collection_id = input.val();
          var access_type = $('select',obj).val();
          var type = input.data('type');
          console.log(collection_id);
          if(input.is(':checked')){
            var url = cms_slug+'/admin-user-roles/add-permission/'+role_id;
           } 
          else {
            var url = cms_slug+'/admin-user-roles/remove-permission/'+role_id;
          } 

    
          $.post(url, { collection_id: collection_id,access_type:access_type,type: type}, function(data){
              toastr.success(data,null,{"timeOut": "3000"}); 
              ths.remove();
          });  
      });
     /********************/

});


 