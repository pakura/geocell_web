<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
*/

ini_set('opcache.revalidate_freq', '0');
Route::get('/', 'HomeController@index');


/* user log in (ajax)*/
Route::get('/{lang}/activate/service/{id}', 'ActivateServiceController@index');
Route::get('/{lang}/activate/service-meti/{id}', 'ActivateServiceMetiController@index');

Route::get('/{lang}/activate/service-for-friend/{id}', 'ActivateServiceController@forFriend');
Route::post('/{lang}/buy/service-for-friend/{id}', 'ActivateServiceController@buyForFriend');


Route::any('/{lang}/confirm/service/{id}', 'ActivateServiceController@confirm');

Route::any('/{lang}/buy/service/{id}', 'ActivateServiceController@buy');

Route::get('/{lang}/buy/service-meti/{id}', 'ActivateServiceMetiController@buy');


Route::post('/{lang}/international-line/search-ajax', 'InternationalLineController@search_ajax');

Route::get('/{lang}/fill-balance/', 'UserDashboardController@fill_balance');
Route::get('/{lang}/initregister/', 'DirectDebitController@initActivation');
Route::get('/{lang}/initmeti', 'DirectDebitController@initMeti');
Route::get('/{lang}/initfixed', 'DirectDebitController@initFixed');
Route::get('/{lang}/initlimit', 'DirectDebitController@initLimit');
Route::get('/{lang}/initmeti/other', 'DirectDebitController@initMetiOther');
Route::get('/{lang}/initfixed/other', 'DirectDebitController@initFixedOther');
Route::get('/{lang}/initlimit/other', 'DirectDebitController@initLimitOther');
Route::get('/{lang}/initmeti/edit/{gsm}', 'DirectDebitController@initMetiEdit');
Route::get('/{lang}/initfixed/edit/{gsm}', 'DirectDebitController@initFixedEdit');
Route::get('/{lang}/initlimit/edit/{gsm}', 'DirectDebitController@initLimitEdit');
Route::get('/{lang}/changecard', 'DirectDebitController@changeCard');
Route::get('/{lang}/activecard', 'DirectDebitController@activeCard');
Route::post('/{lang}/gethistory', 'DirectDebitController@getHistory');
Route::get('/{lang}/debitdeactive', 'DirectDebitController@debitdeactive');
Route::post('/{lang}/initReq', 'DirectDebitController@initReq');
Route::get('/{lang}/checkgsm', 'DirectDebitController@checkGSM');
Route::get('/{lang}/deactivation', 'DirectDebitController@deactivation');
Route::post('/{lang}/changestatus', 'DirectDebitController@deactivationSecrive');
Route::get('/{lang}/debitbalance', 'DirectDebitController@fillbalance');
Route::get('/{lang}/debiterror', 'DirectDebitController@debiterror');
Route::get('/{lang}/debitsucc', 'DirectDebitController@debitSucc');
Route::get('/{lang}/debitalready', 'DirectDebitController@debitalready');
Route::get('/{lang}/notallow', 'DirectDebitController@notAllow');
Route::get('/{lang}/nolailai', 'DirectDebitController@nolailai');
Route::get('/{lang}/limitdeactive', 'DirectDebitController@limitdeactive');

//Route::post('/{lang}/fill-balance-confirmation/', 'UserDashboardController@fill_balance_confirmation');


//b2b
Route::post('{lang}/partner2/order', 'B2BPartner2Controller@order');
Route::get('{lang}/partner2/done', 'B2BPartner2Controller@doneMsg');
Route::get('{lang}/partner2/{id}', 'B2BPartner2Controller@orderShow');


Route::get('/{lang}/termsncondition', 'preOrderController@terms');
Route::get('/{lang}/getoffices/{city_id}', 'preOrderController@getOfiices');

Route::post('/{lang}/getroamingitem', 'RoamingLandingController@initCountryName');
Route::post('/{lang}/getroamingitems', 'RoamingLandingController@show');
Route::get('/{lang}/api/getcountry', 'RoamingLandingController@getCountry');



Route::get('/{lang}/dashboard/remove-service/{service_key}', 'UserDashboardController@removeService');

Route::get('/{lang}/dashboard/deactivate-service/{service_key}', 'UserDashboardController@deactivateService');

Route::get('/{lang}/dashboard/activate-service/{service_key}', 'UserDashboardController@activateService');

Route::get('/{lang}/dashboard/get-geocredit-info', 'UserDashboardController@getGeocreditInfo');



Route::post('/{lang}/user/save-profile', 'UserSettingsController@saveDetails');

Route::get('/{lang}/dashboard/add-service', 'UserDashboardController@addService');
/*Triple-0 save numbers*/
Route::post('/{lang}/save-numbers/{id}','ActivateServiceController@saveNumbers');

Route::get('/{lang}/remove-number/{number}','UserDashboardController@removeNumber');
Route::get('/{lang}/remove-number-conf/{number}','UserDashboardController@removeNumberConfirmation');

Route::post('/{lang}/dashboard/add-number','UserDashboardController@addNumber');

Route::get('/{lang}/fill-balance/enter-number', 'FillBalanceController@fill_balance_enter_number');
Route::post('/{lang}/fill-balance/choose-action', 'FillBalanceController@fill_balance_choose_action');
Route::post('/{lang}/fill-balance/enter-amount', 'FillBalanceController@fill_balance_enter_amount');
Route::post('/{lang}/fill-balance/confirmation/', 'FillBalanceController@fill_balance_confirmation');
Route::post('/{lang}/fill-balance/buy-meti/', 'FillBalanceController@fill_balance_buy_meti');

Route::get('/{lang}/fill-balance/enter-number-test', 'FillBalanceController@fill_balance_enter_number_test');

Route::get('/{lang}/login', 'GeocellIdController@logInTemplate');
Route::any('/{lang}/alternumber', 'GeocellIdController@alterNumber');
Route::get('/{lang}/forgot-password', 'GeocellIdController@forgotPass');
Route::post('/{lang}/altersms', 'GeocellIdController@alterSms');
Route::get('/{lang}/logout', 'GeocellIdController@logOut');



Route::get('/{lang}/getdocumentbyid/{docId}', 'EsignatureController@download');
Route::get('/{lang}/clearsignature', 'EsignatureController@deleteDocs');


Route::get('/{lang}/hlron/{serv}', 'ActivateServiceController@activehlr');
Route::get('/{lang}/hlroff/{serv}', 'ActivateServiceController@deactivehlr');

Route::post('/{lang}/getdetails', 'UserCallDetailsController@getDetails');
Route::get('/{lang}/removedetails', 'UserCallDetailsController@removedetails');
Route::get('/{lang}/removeinvoice', 'UserCallDetailsController@removeinvoice');
Route::post('/{lang}/checkupdate', 'UserCallDetailsController@checkUpdate');
Route::any('/{lang}/getinvoices', 'UserCallDetailsController@getInvoices');
Route::get('/{lang}/croninit', 'UserCallDetailsController@cronInit');
Route::get('/{lang}/download-as-pdf', 'userCalldetailsListController@download');
Route::get('/{lang}/download-invoice', 'UserCallDetailsController@download');
Route::get('/{lang}/detailfillbalance/{bal}', 'UserCallDetailsController@fillBalance');
Route::get('/{lang}/test-calldetails/{gsm}', 'UserCallDetailsController@getDetails');

Route::get('/{lang}/callcenter', 'CallcenterDashboardController@index');

Route::get('/{lang}/search', 'SearchController@index');

Route::post('/{lang}/contact/send', 'ContactController@send');
Route::post('/{lang}/fraud-contact/send', 'FraudContactController@send');

Route::get('/{lang}', 'HomeController@locale');
Route::get('/{lang}/myip', function(){
    echo bcrypt('123412');
});

/*301 redirection for old urls*/
Route::get('{lang}/useful/news/{id}', function($lang,$id)
{
	return Redirect::to($lang.'/company/geocell-news/'.$id, 301);

});

Route::get('/{lang}/mygeocell', function(){
    return Redirect::to('/private/private-cabinet/dashboard', 301);
});

//installment close ajax call
Route::get('{lang}/dashboard/closeinstallment', 'UserDashboardController@closeinstall');
Route::any('{lang}/products/{id}/{color}', 'ProductsController@changeColor');
//Route::post('{lang}/iphone-se/step3', 'preOrderController@complited');

//email_subscriber
Route::post('{lang}/emailsubscriber', 'newsLetterController@addmail');
Route::get('{lang}/newsletterpopup',  'newsLetterController@showPopup');
Route::get('{lang}/oknewsletter',  'newsLetterController@okSubscribtion');
Route::get('{lang}/alreadynewsletter',  'newsLetterController@alreadySubscribtion');
Route::get('{lang}/unsubscribe/{email}', 'newsLetterController@unSubscribe');
Route::get('{lang}/unsub/{email}', 'newsLetterController@unSubscribeemail');
Route::get('{lang}/disablenewsletter', 'newsLetterController@disableNewsletter');
//reditects
Route::get('{lang}/roamingdata', function($lang){
    return Redirect::to($lang.'/business/roaming/roaming-services/roaming-internet-packs');
});
Route::get('{lang}/bonusprogram', function($lang){
    return Redirect::to($lang.'/roaming/rewards/');
});
Route::get('{lang}/cheaproaming', function($lang){
    return Redirect::to($lang.'/business/roaming/calls-onlly-for-35-tetri/roamin-for-geocell-subscribers');
});

Route::get('{lang}/calculator', 'CalculatorController@index');
Route::get('{lang}/test', 'TestController@index');

//Route::get('{lang}/seccode', 'SecurityPageController@index');
Route::any('{lang}/private/secsend/{num1}/{num2}/{num3}/{key}', 'SecurityPageController@show');

Route::get('{lang}/partner/flow', 'B2BHomeController@partner');
Route::post('{lang}/partner/send', 'B2BHomeController@PartnerSend');

foreach(config('oldlinks.links') as $old => $new){
    Route::get($old, function() use($new) {
        return Redirect::to($new, 301);
    });
}

/*End 301 redirection for old urls*/

Route::get('{lang}/{slug}/{params?}', 'PagesController@getPage')->where('params', '(.*)');




/*
Event::listen('illuminate.query', function($query)
{
    var_export($query.'</br> </br>');
 
});
 
 */