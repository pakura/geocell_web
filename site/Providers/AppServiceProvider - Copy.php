<?php namespace Site\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		
		view()->composer('site.header',function($view)
 		{
 			$pages = $this->getByGroupId(1);
 			dd($pages);
            $subPages=[];
            $subsSubPages = [];
            $breadcrumbs = [];
            $slug = \Request::segment(2);
      
            if(!$slug) {
            	$subPages = \DB::table('pages')->where('parent_id','=',$pages[0]->id)
            								   ->orderby('position')
            								   ->get();
            }
            else {

            	$subSlug = \Request::segment(3);

            	for($i=0;$i<count($pages);$i++){

	 				if ($slug==$pages[$i]->slug)
					{
					    $breadcrumbs[$slug] = $pages[$i]->generic_title;
					    $subPages = \DB::table('pages')->where('parent_id','=',$pages[$i]->id)
					    							   ->orderby('position')
					    							   ->get();

					  		for($j=0;$j<count($subPages);$j++){
            		 
				 				if ($subSlug==$subPages[$j]->slug)
								{
								    $breadcrumbs[$subPages[$j]->full_slug] = $subPages[$j]->generic_title;

								    $subsSubPages = \DB::table('pages')->where('parent_id','=',$subPages[$j]->id)
								    							   ->orderby('position')
								    							   ->get();


								    break;
								}
				 			}  							   


					    break;
					}
	 			}
            }
 			
 			
 			$view->with('pages',$pages)->with('subPages',$subPages)->with('subsSubPages',$subsSubPages)->with('breadcrumbs',$breadcrumbs);
 		});

 		view()->composer('site.footer-banners',function($view)
 		{
 			$footerBanners = \Site\Models\Pages::whereGroupId(3)->whereParentId(0)->orderby('position')->get();
 										 
 			$view->with('footerBanners',$footerBanners);
 		});
		
		
	}


	public function getByGroupId($group_id){

		return  \Site\Models\Pages::whereGroupId($group_id)->orderby('position')->get();
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		 
	}

}
