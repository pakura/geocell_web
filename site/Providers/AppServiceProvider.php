<?php namespace Site\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
	public $orderedPages=[];
	public $attachedProducts = [];
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		


		view()->composer('site',function($view)
 		{
 			 
 			$pages = $this->getByGroupId(1);
 			

 			$this->orderPages($pages,0);

 			$products =\Site\Models\Products::withTitle()->whereIn('collection_id',$this->attachedProducts)->where('products.visibility','=',1)->get()->groupBy('collection_id');	
 			
 			//dd($products[62]);
           // dd($this->attachedProducts);
            $subParentId=null;

            $breadcrumbs=[];

            $pgSlug = \Request::segment(2);

            $slug = $pgSlug;

             if(!$slug) {
             	$slug = $pages[0]->slug;
            }
           /* dd( $slug);*/
            $parentid = $this->getIdByFullSlug($pages,$slug); 
            
            if($pgSlug) { 
            	$pgSlug == 'search'?$breadcrumbs['search'] = trans('search'): $breadcrumbs[@$parentid->full_slug] = @$parentid->title; 
            }  


            if($subSlug = \Request::segment(3)) {

            	$subParentId = $this->getIdByFullSlug($pages,$slug.'/'.$subSlug);

            	if ($subParentId) { $breadcrumbs[$subParentId->full_slug] = $subParentId->title; }   
            	 
            }

            $geocellId = new \Site\Models\GeocellId;
       
            if(!$geocellId->isAuthed()) { $geocellId->forgetUserSessionData();  }

          
 			/*get floating menu,footer,mobile apps banners */
 			$banners = \Site\Models\Banners::whereIn('collection_id',[80,84,78,99])->withContent()->get()->groupBy('collection_id');
 			/*site settings*/
 			$settings = \App\Models\Settings::whereLanguage(\app::getlocale())->get()->groupBy('settings_key');
            
            //dd($settings);
 			$view->with('pages',$this->orderedPages)
 				 ->with('parentid',@$parentid->id)
 				 ->with('subParentId',@$subParentId->id)
 				 ->with('products',@$products)
 				 ->with('banners',@$banners)
 				 ->with('settings',@$settings)
 				 ->with('breadcrumbs',$breadcrumbs);

 		});



 		view()->composer('site.user.usercabinet-head',function($view)
 		{
 			$menuItems = \Site\Models\Pages::whereParentId(22)->where('pages.visibility','=',1)->withContent()->orderby('position')->get();

 			$model = new \App\Models\BillingFacadeServices;

 
	        /* dd(\Session::get('userInfo'));*/

	        
	        $check4G = $model->check4GInfo(\Session::get('phone'));	


	        if(\Session::has('userInfo')) {
	         	 $userInfo = \Session::get('userInfo');
	         } else {
	         	 $userInfo = $model->_getUserInfo(\Session::get('phone'));
	         	 \Session::put('userInfo',$userInfo);
	         }

 			$view->with('menuItems',$menuItems)
 			     ->with('check4G',$check4G)
 				 ->with('userInfo',$userInfo);
 		});

		
		view()->composer('site.fraud.head',function($view)
 		{
 			$parentPage = \Site\Models\Pages::where('pages.id','=',113)->withContent()->first();
 				 				 
 			$view->with('parentPage',$parentPage);
 		});
		
	}

	public function orderPages($pages,$parent_id=0){
		 
		 
		foreach ($pages as $page){
		  
		    if ($page->parent_id != $parent_id) { continue; } 
		    	
		    	/*collect attached products collection ids */
		    	if($page->page_type==4 ) { $this->attachedProducts[] = $page->attached_collection_id; }

		    	$this->orderedPages[$page->parent_id][] = $page;	
		    	
		    	if ($this->has_children($pages,$page->id) ){
			          
			        $this->orderPages($pages,$page->id);

			    }
			 
		}

		//dd($this->orderedPages);
		return $this->orderedPages;
	}

	public function has_children($pages,$id) {
	  foreach ($pages as $page) {
	    if ($page->parent_id == $id)
	      return true;
	  }
	  return false;
	}

	public function getIdByFullSlug($pages,$slug){
		for($i=0;$i<count($pages);$i++){

			if ($slug==$pages[$i]->full_slug)
				{
					//return $pages[$i]->id;
					return $pages[$i];
				}	
		}
		return 0;		
	}

	public function getByGroupId($group_id){

		return  \Site\Models\Pages::getByGroupID($group_id);
	}


	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		 
	}

}
