<?php namespace Site\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
	public $orderedPages=[];
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		
		view()->composer('site.header',function($view)
 		{
 			$pages = $this->getByGroupId(1);
 			$this->orderPages($pages,0);
	        
 			$view->with('pages',$this->orderedPages);
 		});

 		view()->composer('site.footer-banners',function($view)
 		{
 			$footerBanners = \Site\Models\Pages::whereGroupId(3)->whereParentId(0)->orderby('position')->get();
 										 
 			$view->with('footerBanners',$footerBanners);
 		});
		
		
	}

	public function orderPages($pages,$parent_id=0){
		 
		foreach ($pages as $page){
		  
		    if ($page->parent_id == $parent_id) {
		    	$parentSlug = $this->getParentSlug($pages,$page->parent_id);
		    	 
		    	$this->orderedPages[$parentSlug][] = $page;	
		    	if ($this->has_children($pages,$page->id) ){
			          
			        $this->orderPages($pages,$page->id);

			    }

		    }	
		}
		return $this->orderedPages;
	}

	public function has_children($pages,$id) {
	  foreach ($pages as $page) {
	    if ($page->parent_id == $id)
	      return true;
	  }
	  return false;
	}

	public function getParentSlug($pages,$id){
		for($i=0;$i<count($pages);$i++){

			if ($id==$pages[$i]->id)
				{
					return $pages[$i]->slug;
				}	
		}
		return 0; 	
	}

	public function getByGroupId($group_id){

		return  \Site\Models\Pages::whereGroupId($group_id)->orderby('position')->get();
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		 
	}

}
