<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Libs\fpdf;
use Site\Models\GeocellId;
use Site\Models\Services;

class userCalldetailsListController extends Controller {

    protected $model;
    protected $geocellId;
    protected $userInfo = [];
    protected $actServices = [];
    protected $phone;
    protected $pdf;

    public function __construct(BillingFacadeServices $model,GeocellId $geocellIdModel)
    {

        $this->geocellId=$geocellIdModel;
        $this->model=$model;
        $this->phone= \Session::get('phone');
        $this->pdf = new fpdf();

    }


    public function index() {
        $details = null;
        $details_req = \DB::table('init_call_details')->where('phone', $this->phone)->whereIn('status', [3,4])->orderBy('id', 'desc')->first();
        if(isset($details_req)){
            $details = \DB::table('call_details')->where('phone', $this->phone)->orderBy('id', 'desc')->get();
            if(isset($details)){
                if(!is_array($details)){
                    $t = $details;
                    $details = array($t);
                }
            }
        }

        $resUserInfo = $this->model->_getUserInfo($this->phone);
        $res = \DB::table('init_call_details')->where('id', $details_req->id)->update(['status' => 4]);
        return view('site.user.calldetailList')->with('details', $details)->with('details_req', $details_req)->with('resUserInfo', $resUserInfo->return);
    }

    public function download(){
        ini_set('max_execution_time', 600);
        ini_set("memory_limit", "4096M");
        $details = null;
        $details_req = \DB::table('init_call_details')->where('phone', $this->phone)->whereIn('status', [3,4])->orderBy('id', 'desc')->first();
        $profile = \DB::table('call_details_profile')->where('phone', $this->phone)->first();
        $details_req->fromdate = isset($profile->changes_date)?$profile->changes_date:$details_req->fromdate;
        if(isset($details_req)){
            $details = \DB::table('call_details')->where('phone', $this->phone)->orderBy('id', 'desc')->get();
            if(isset($details)){
                if(!is_array($details)){
                    $t = $details;
                    $details = array($t);
                }
            }
        }
//        return view('pdf.details',  ['details' => $details, 'details_req' => $details_req, 'phone' => $this->phone]);
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('pdf.details', ['details' => $details, 'details_req' => $details_req, 'phone' => $this->phone]);

        return $pdf->stream();
//        return $dompdf->download('details.pdf');
    }


    public function initinvoice(){
        $details = null;
        $details_req = \DB::table('init_call_details')->where('phone', $this->phone)->whereIn('status', [3,4])->orderBy('id', 'desc')->first();
        if(isset($details_req)){
            $details = \DB::table('call_details')->where('phone', $this->phone)->get();
            if(isset($details)){
                if(!is_array($details)){
                    $t = $details;
                    $details = array($t);
                }
            }
        }
    }

}

