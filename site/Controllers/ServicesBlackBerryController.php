<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Products;

class ServicesBlackBerryController extends Controller {

    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index($page)
	{
		$subPages = \Site\Models\Pages::whereParentId($page->parent_id)->withContent()->get();

		$services = Products::collection($page->attached_collection_id)->withContentOnly()->get();
		return view('site.services.blackberry.list',compact('services','subPages'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function show($slug)
	{
		return 0;
		/*$service =  Products::whereSlug($slug)->withContentOnly()->first();
		
		if(!$service){ abort(404); }
		 
		return view('site.services.meti.view',compact('service')); */ 
	}

	 

}
