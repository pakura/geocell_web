<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use  Site\Models\Gallery;
use  Site\Models\Products;
use  Site\Models\Articles;
use Mail;

class B2BPartner2Controller extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {
        $partner2_packs = \DB::table('partner_2_packs')->get();
        return view('site.b2b.partner2')->with('packs', $partner2_packs);
    }

    public function orderShow($lang, $id){
        $pack = \DB::table('partner_2_packs')->where('id', $id)->first();
        return view('site.lightbox.partner2')->with('pack', $pack);
    }

    public function order(){
        $data = (object)\Input::all();

        $message = $data;
        \Mail::send('emails.partner2', ['title' => 'Partner 2', 'data' => $message], function ($message)
        {
            $message->from('no-reply@geocell.ge', 'Geocell.ge');
            $message->to('request@geocell.ge');
//            $message->to('vajasin@gmail.com');
            $message->subject('Partner 2');
        });
        \Session::set('mail', true);
        return redirect('/business/services/tariffs-for-sme/partner-2?done=true');

    }


    public function doneMsg(){
        \Session::forget('mail');
        if(\App::getLocale() == 'en')
            $msg = 'Your request has been sent';
        else
            $msg = 'თქვენი მოთხოვნა გაგზავნილია.';
        return view('site.lightbox.msg')->with('msg', $msg);
    }


}
