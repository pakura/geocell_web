<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Models\GeocellId;
use Site\Models\Services;
use Illuminate\Http\RedirectResponse;


class newsLetterController extends Controller {

    protected $model;
    protected $geocellId;
    protected $actServices = [];
    protected $phone;
    protected $email;
    protected $insert;

    public function __construct(BillingFacadeServices $model,GeocellId $geocellIdModel)
    {

//        $this->middleware('site_user');
        $this->geocellId=$geocellIdModel;
        $this->model=$model;
        $this->phone= \Session::get('phone');
        $this->insert = (object) array(
            'brand' => '',
            'isStaff' => '',
            'gsm' => '',
            'g4' => '',
            'phone_name' => '',
            'email' => ''
        );

    }

    public function addmail($lang){
        $token = str_random(16);
        $this->email = \Input::get('email');
        $blacklisted = \DB::table('blacklisted_mails')->where('email', $this->email)->first();
        if(isset($blacklisted)){
            return 'disabled';
        }
        $sub = \DB::table('email_sub')->where('email', $this->email)->first();
        if(isset($sub->id)){
            $res = \DB::table('email_sub_status_action')->where('email', $this->email)->where('action', 0)->get();
            if(isset($res) && sizeof($res)>2){
                return 'disabled';
            } else {
                if($sub->status == 1){
                    return 'already';
                }
            }
            $sub = \DB::table('email_sub')->where('email', $this->email)->delete();
        }
        if($this->geocellId->isAuthed()){
            $res = \DB::table('email_sub')->where('email', $this->email)->first();
            if(isset($res)){
                return 'already';
            } else {
                $resUserInfo = $this->model->_getUserInfo($this->phone);
                $g4 = $this->model->__call('portalCheck4gCompatibility',[$this->phone]);
                $isb2b = $this->model->__call('portalGetSubscriberData',[$this->phone]);
                $isb2b = isset($isb2b->return->isPrivate)?$isb2b->return->isPrivate:2;
                if(isset($g4->return->resultCode) && $g4->return->resultCode == 1){
                    $this->insert->phone_name = $g4->return->model;
                    $this->insert->g4 = $g4->return->isSIM4GCompatibility;
                }
                $this->insert->brand = $resUserInfo->return->brand;
                $this->insert->isStaff = $resUserInfo->return->isStaff;
                $this->insert->gsm = $this->phone;
                $this->insert->email = $this->email;
                $res = \DB::table('email_sub')->insert(array('email' => $this->insert->email, 'phone' => $this->insert->gsm,
                    'gsm_type' => $this->insert->brand, 'sub_type' => $this->insert->isStaff, 'phone_name' => $this->insert->phone_name,
                    'g4' => $this->insert->g4, 'b2b' => $isb2b));
                return 'ok';
            }
        } else {
            $res = \DB::table('email_sub')->where('email', $this->email)->first();
            if(isset($res)){
                return 'already';
            } else {
                $res = \DB::table('email_sub')->insert(array('email' => $this->email));
                return 'ok';
            }
        }
    }

    public function showPopup(){
        return view('site.lightbox.newsletter');
    }

    public function okSubscribtion(){
        $lang = $lang = \App::getLocale();
        if($lang == 'en'){
            return view('site.lightbox.msg')->with('msg', 'Your Email is registered successfully');
        } else {
            return view('site.lightbox.msg')->with('msg', 'თქვენ მეილი წარმატებით დარეგისტრირდა');
        }

    }

    public function alreadySubscribtion(){
        $lang = $lang = \App::getLocale();
        if($lang == 'en'){
            return view('site.lightbox.msg')->with('msg', 'You are already in list');
        } else {
            return view('site.lightbox.msg')->with('msg', 'თქვენ უკვე ხართ სიაში');
        }
    }

    public function disableNewsletter(){
        $lang = $lang = \App::getLocale();
        if($lang == 'en'){
            return view('site.lightbox.msg')->with('msg', 'You won’t be able to subscribe with following Email');
        } else {
            return view('site.lightbox.msg')->with('msg', 'მითითებული მეილით ვეღარ შეძლებთ სიახლეების გამოწერას');
        }
    }

    public function unSubscribe($lang, $email){
        return view('unsubscribe')->with('email', $email);
    }

    public function unSubscribeemail($lang, $email){
        $sub = \DB::table('email_sub')->where('email', $email)->where('status', 1)->first();
        if(!isset($sub)){
            return redirect('https://geocell.ge');
        }
        $res = \DB::table('email_sub')->where('email', $email)->where('status', 1)->update(array('status' => 0));
        $res = \DB::table('email_sub_status_action')->insert(
            array('email' => $email, 'action' => 0)
        );
        return redirect('https://geocell.ge');
    }

}
