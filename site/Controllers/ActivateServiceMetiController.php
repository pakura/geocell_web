<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Models\Products;
use Site\Models\Services;


class ActivateServiceMetiController extends Controller {
 
  protected $model;
    

    public function __construct(BillingFacadeServices $model)
  {
      $this->middleware('site_user');
      $this->model=$model; 
      
  }
  
  
  public function index($lang,$id) {
       // dd($id);
     $service = Products::withPrice()->find($id); 

     $userInfo = \Session::get('userInfo');
     $allowBuy = $this->checkMeti($service->sku2, $service->value*100);
     if( ($userInfo->return->balance > $service->value) || ( isset($userInfo->return->accountMethod ) && $userInfo->return->accountMethod==1 && $userInfo->return->balance<0 && (($userInfo->return->creditLimit+$userInfo->return->balance) > $service->value) ) ) {
        return view('site.lightbox.activate-service-meti',compact('service','userInfo', 'allowBuy'));
     }
     else {
        /* not enough money */
         return view('site.lightbox.activate-service-meti-fill-balance',compact('service','userInfo', 'allowBuy'));
     } 
     
  } 

    /*public function chooseType(){

    }*/

    public function buy($lang,$id) {
       $periodic = \Request::get('periodic');

       $service = Products::withPrice()->find($id);

       /* choose servise type */
       if(!$periodic) { return view('site.lightbox.choose-service-type',compact('service'));  }

       if($periodic==1) { $service->sku=$service->sku.'_PERIODIC'; }
       //dd($service->sku);
       /*dd(\Session::get('phone'));*/
       $res = $this->model->__call('portalActivateService',[\Session::get('phone'),'',$service->sku]);  
       //dd($res);
       if(isset($res->return->resultCode) &&  $res->return->resultCode<0) {


          if($res->return->resultCode==-1007) { 
               // $userInfo = $this->model->__call('portalGetGSMInfo',[$this->phone]);
              return  view('site.lightbox.activate-service-meti-fill-balance',compact('service','userInfo')); 
          }
          elseif(\Lang::has('site.error_'.abs($res->return->resultCode)) ){ 
               // $userInfo = $this->model->__call('portalGetGSMInfo',[$this->phone]);
                $msg = trans('site.error_'.abs($res->return->resultCode));
                return  view('site.lightbox.msg',compact('msg')); 
          }

          elseif(isset($res->return->restrictionKeys)){
              
              return $this->restrictions($service,$res->return->restrictionKeys,$lang);

            } 

         //   $msg = "Biling Facade Error: ".$res->return->resultCode;
             $msg = trans('site.error_1001');
            \Session::flash('msg', $msg);
            return $this->index($lang,$id); 
       }

       else
       {
            return view('site.lightbox.activate-service-notification',compact('service')); 
       }
       
    } 


    private function restrictions($service, $restriction_key,$lang='en'){
        //$restriction_key = $res->return->restrictionKeys; 
         // dd($restriction_key->key);  
         $service_keys = [];

         if(is_array($restriction_key)) { 
            foreach ($restriction_key as $service_key) {
             // $service_keys[] =str_replace('_PERIODIC', '', $service_key->key);
              $service_keys[] =$service_key->key;
            }
             
          }

         else {
            //$service_keys[] =  str_replace('_PERIODIC', '', $restriction_key->key);
            $service_keys[] = $restriction_key->key;
         }

          // dd($service_keys); 
          $restrictions=[];

          $restrictions = Services::whereIn('service_key',$service_keys)->select('*','service_key as sku','service_title_'.$lang.' as title')->groupby('service_key')->get(); 

          return  view('site.lightbox.service-meti-restriction',compact('service','restrictions'));  
    }


    public function checkMeti($bundleid, $bundleprice){
        $paymentid = rand(1000000000,9999999999);
        $id = rand(1000000000,9999999999);
        $phone = \Session::get('phone');
        $phone = substr($phone.'', 3);
        $url = "http://91.151.128.64:7070/bundleAccept/bundleRequest";
        $params = array(
            "source" => "mobility",
            "acquirer" => "mobility",
            "time" => date('y/m/d-H:i:s'),
            "language" => "en",
            "amount" => 300,
            "terminal" => "internet",
            "service" => "verification",
            "phone" => $phone,
            "currency" => 981,
            "paymentid" => $paymentid,
            "id" => $id,
            "pass" => "Gd3mYsn80j"
        );
        $hash = '';
        foreach ($params as $kay => $value){
            if ($hash == ''){
                $url .= "?".$kay."=".$value;
            } else {
                $url .= "&".$kay."=".$value;
            }
            $hash .= $kay."=".$value.";";
        }
        $url .= "&mac=".md5($hash);
        $url .= "&bundleid=$bundleid&bundleprice=$bundleprice";
        $response = file_get_contents($url);
        if (isset($response)){
            $response = explode('&', $response)[0];
            $response = explode('=',$response)[1];
            if ($response == 0){
                return true;
            } else {
                return false;
            }
        }
    }
}
