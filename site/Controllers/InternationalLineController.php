<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\InternationalLine;

class InternationalLineController extends Controller {

    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index($page)
	{
 	
		$services = InternationalLine::getList(\App::getLocale());
//		dd($services);
	    $symbols =  $this->getDistinctLabel($services);

	    //dd(count($services));
	    if($page->id==184) {
	    	return view('site.services.international-line.list_b2b',compact('services','symbols'));
	    }
		return view('site.services.international-line.list',compact('services','symbols'));
		 
		 
	}
	
    /* Get distinc alphabet */
	private function getDistinctLabel($data) {
        $symbols = [];

		$prevSymbol = null;

			for($i=0;$i<count($data);$i++){

				$currSymbol = strtoupper($data[$i]->letter);

				if($data[$i]->letter=='' || $currSymbol==$prevSymbol) { continue; }
			        
			        $prevSymbol = $currSymbol;
			        $symbols[] = $prevSymbol;
			    
			}
        return $symbols; 
	}
    
    public function search_ajax(){

    	$key_word = \Request::get('key_word');
    	$lines = InternationalLine::where('title', 'LIKE', "%$key_word%")->orderBy('title')->get();  

    	return view('site.services.international-line.search-result',compact('lines','key_word'));
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function show($slug)
	{
	   return 0;
	}

	 

}
