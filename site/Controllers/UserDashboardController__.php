<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Models\GeocellId;
use Site\Models\Services;


class UserDashboardController extends Controller {
 
    protected $model;
    protected $geocellId;
    protected $actServices = [];
    protected $phone;

    protected $bonusesKeys=['S_SOCIAL_NETWORK_PACKAGE','S_SMARTPHONE_PACKAGE','TEST_DRIVE','S_ROAMING_10MB','S_ROAMING_50MB','S_ROAMING_100MB'];
    

    public function __construct(BillingFacadeServices $model,GeocellId $geocellIdModel)
	{
	    
        $this->middleware('site_user');

        $this->geocellId=$geocellIdModel; 
        $this->model=$model; 
        $this->phone= \Session::get('phone');
       
	}
	
 	
	public function index() {

        \Session::put('url',\Request::url());    
        
         if(!$this->geocellId->isAuthed()) { return view('site.user.auth-sign-in'); }
            
         $userInfo = $this->getUserInfo();   
        //dd($userInfo);

         if($userInfo->return->resultCode<0) {         
                return view('site.user.msg')->with('msg',trans('site.error_1001'));
             } 
      
		 $userServices = $this->model->__call('portalGetActiveServices',[$this->phone]);

             //dd($userServices);

         if(isset($userServices->return->list) ){
            \Session::put('userServices',$userServices);
         }
         $userServices = \Session::has('userServices')?\Session::get('userServices'):$userServices;

   
         /*get services keys*/
         $services_keys = $this->getActiveServicesKey($userServices);
         
         // dd($services_keys);
         
         /* take info from database according keys */
         $actServices = $this->getServicesForList($services_keys,$userInfo->return->brand);
        // dd($actServices);
         /*merge facade's services info with DB services info */
         $actServices =  $this->getOrderedServiceData($actServices);   
         //dd($actServices);
         $numbers = [];
         if(in_array('S_TRIPLE_ZERO', $services_keys)) {
            $res = $this->model->__call('portalGetFriendList',[$this->phone]);
            
            $numbers= [];  
            if( isset($res->return->list) && is_array($res->return->list)) { $numbers = $res->return->list; } 
            elseif( isset($res->return->list) ){  $numbers[] = $res->return->list;  }
         }
        // dd($installment_status->return->planedPayments[0]);
            //dd($services_keys);
         //dd($numbers);

        /*get installment status*/
        $installment_status =  $this->model->__call('portalGetPhoneInstallmentStatus',[$this->phone]);
        //dd($installment_status);

         /*get available services for the number*/
         $available_services= $this->addService($userInfo->return->brand);
		 // dd($available_services);
		 return view('site.user.dashboard')->with('userInfo',$userInfo)
                                           ->with('actServices',$actServices)      
                                           ->with('numbers',$numbers)      
                                           ->with('installment',$installment_status->return)      
		 								   ->with('available_services',$available_services);  	
	} 

    public function getUserInfo(){

        // dd(\Session::get('phone'));
        // $resUserInfo = $this->model->_getUserInfo($this->phone);
         //dd($resUserInfo);
         /*if(isset($resUserInfo->return->resultCode) && $resUserInfo->return->resultCode==1){
            \Session::put('userInfo',$resUserInfo);
         }*/

	         if(\Session::has('userInfo')) {
	         	 $userInfo = \Session::get('userInfo');
	         } else {
	         	 $userInfo = $this->model->_getUserInfo($this->phone);	
	         	 \Session::put('userInfo',$userInfo);
	         }


         //$userInfo = \Session::has('userInfo')?\Session::get('userInfo'):$this->model->_getUserInfo($this->phone);

        return $userInfo;
    }

    /*public function check4GInfo(){

        // dd(\Session::get('phone'));
         return $this->model->__call('portalCheck4gCompatibility',[$this->phone]);

    }*/

    public function fill_balance(){
         
        //$amount = \Input::get('amount');
        return view('site.lightbox.fill-balance')->with('phone',$this->phone);
    }
    
  /*  public function fill_balance_confirmation(){
        $amount = \Input::get('amount');
        
        return view('site.lightbox.fill-balance-confirmation',compact('amount'));
    }*/

    public function deactivateService($lang,$service_key){
        $jsn = \Input::get('jsn');
        $response = $this->model->__call('portalTerminateService',[$this->phone,$service_key]);
       // dd($response);
       
        if(isset($response->return->resultCode) &&  $response->return->resultCode<0)  {
           $msg = trans('site.error_1001');
           if($jsn) { return \Response::json(['error'=>$msg]); }
        } 
        else {
             $msg  = trans('site.deactivate_service');

             if($jsn) { return \Response::json(['success'=>1]); }
        }

        return view('site.lightbox.msg',compact('msg'));
    }

    public function removeService($lang, $service_key){
        $service = Services::whereServiceKey($service_key)->select('*','description_'.$lang.' as description')->first();

    
        return view('site.lightbox.deactivate-service-notification',compact('service'));
       

        // return view('site.lightbox.couldnot-deactivate-service',compact('service'));
    }
    
    public function  activateService($lang,$service_key){
        $response = $this->model->__call('portalActivateService',[$this->phone,'',$service_key]);
        dd($response);
    }
    
    public function getGeocreditInfo(){
        $response = $this->model->__call('portalGetGeoCreditInfo',[$this->phone]);
       // dd($response);
   
        return view('site.lightbox.geo-credit-info',compact('response'));
    }
    
    public function removeNumber($lang,$number){
    	$response =  $this->model->__call('portalFriendSlotAction',[$this->phone,$number,2]);
        if($response->return->resultCode==1) {
        	return view('site.lightbox.msg')->with('msg',trans('site.remove_num'));
        } 
    }

    public function removeNumberConfirmation($lang,$number){
    
        return view('site.lightbox.remove-number-confirmation',compact('number'));
      
    }

    public function addNumber(){
    	 $number = \Request::get('friend_number');
    	 if($number) {
            $number ='995'.$number; 

            if($number==$this->phone) { return \Response::json(['msg'=>trans('site.error_your_number')]); }

    	 	$response = $this->model->__call('portalFriendSlotAction',[$this->phone,$number,1]);
            if(isset($response->return->resultCode) && $response->return->resultCode==1) {
               $tpl = view('site.lightbox.add-number',compact('number'))->render();
               return \Response::json(['tpl'=>$tpl]);
            } 

            elseif(isset($response->return->resultCode) && $response->return->resultCode==-1006){
                return \Response::json(['msg'=>trans('site.not_geocell')]);
            } 

            else {
                return \Response::json(['msg'=>trans('site.error_1001')]);
            }
            
    	 }
    	 
    	 
    }

    public function addService($brand=2){

        $services = $this->model->__call('portalGetAvailableServices',[$this->phone]);
       //dd($services);
        if(isset($services->return->list) ){
            \Session::put('availableServices',$services);
         }
        $services = \Session::has('availableServices')?\Session::get('availableServices'):$services;

        $services = $this->getServices($this->getAllAvailableServicesKey($services),$brand);
        
        return $services;
       // return view('site.lightbox.add-service',compact('services'));
    }

	private function getServices ($keys,$brand=2){

		//$keys = $this->getActiveServicesKey($obj);
         //dd($keys);
        $lang = \App::getLocale();
		//$brand = isset($this->userInfo->return->brand) ? $this->userInfo->return->brand : 2;
        $services = Services::join('products as p','p.sku','=','service_key')
                            ->leftjoin('products_attachments as a', function ($join) {

                                  $join->on('a.product_id','=','p.id')
                                       ->where('a.file_type','=',1);
                             }) 
                             ->leftjoin('products_attachments as m', function ($join) {

                                  $join->on('m.product_id','=','p.id')
                                       ->where('m.file_type','=',5);
                             })    
                            ->whereBrand($brand)       			        
                            ->whereIn('service_key',$keys)
                            ->select('p.id as product_id','services_list.*','services_list.description_'.$lang.' as description','a.file','m.file as meti_file')
                            ->orderby('position')
                            ->groupBy('service_key')
                            ->get(); 
        //dd($services);
        //return $this->getOrderedServiceData($services);	
        return $services;	   
	}
    
    private function getServicesForList ($keys,$brand=2){

        //$keys = $this->getActiveServicesKey($obj);
        // dd($keys);
        $lang = \App::getLocale();
    	//$brand = isset($this->userInfo->return->brand) ? $this->userInfo->return->brand : 2;
    	

        $services = Services::leftjoin('products as p','p.sku','=','service_key')
                            ->leftjoin('products_attachments as a', function ($join) {

                                  $join->on('a.product_id','=','p.id')
                                       ->where('a.file_type','=',1);
                             })  
                            ->leftjoin('products_attachments as m', function ($join) {

                                  $join->on('m.product_id','=','p.id')
                                       ->where('m.file_type','=',5);
                             }) 
                            ->whereBrand($brand)                           
                            ->whereIn('service_key',$keys)  
                            ->select('*','description_'.$lang.' as description','a.file','m.file as meti_file')                    
                            ->get()/*->groupBy('service_key')*/; 

        //dd($services);
        //return $this->getOrderedServiceData($services);   
        return $services;      
    }

    private function getOrderedServiceData($services){
    	$data = [];
        //$service - list from DB;
        //$this->actServices - service list from wsdl;
        //dd($services);
       // dd($this->actServices);
    	foreach( $services as $service) {

            $service['data'] =  $this->actServices[$service->service_key];
    		$data[$service->service_type]['data'][] =$service;
    		 
            /*set amount to array*/
    		if(!array_key_exists('amount',$data[$service->service_type])) { 

                $data[$service->service_type]['amount'] = 0;


    		}
            if($data[$service->service_type]['amount']==-1 || ($service->service_key=='S_METI_MINUTE_ONNET' || $service->service_key=='S_METI_MINUTE_ONNET_PERIODIC')) { continue; }

            $amt = (isset($this->actServices[$service->service_key]->amount)?$this->actServices[$service->service_key]->amount:0);

    		$data[$service->service_type]['amount'] =(($amt==-1)?$amt:$data[$service->service_type]['amount'] + $amt);
            


    	}

    	 //dd($data);

    	return $data;
    }

	private function getActiveServicesKey($obj){
		$arr = []; 
        if(isset($obj->return->list)) {
		  
           
    		if(is_array($obj->return->list)){
	            foreach ($obj->return->list as $service) { 

	                 if(isset($service->key)) {
	                 	if(in_array($service->key, $this->bonusesKeys) && $service->bonus==true) { $service->key = $service->key.'_BONUS'; }
                        

	    		 	 	$arr[]=$service->key;
	    		 	 	$this->actServices[$service->key] = $service;
	    		 	 }

	    		 }

              
    		}
    		else {
          
	                  
	                 if(isset($obj->return->list->key)) {
	    		 	 	$arr[]=$obj->return->list->key;
	    		 	 	$this->actServices[$obj->return->list->key] = $obj->return->list;

                         
	    		 	 }

    		}


        }
        //dd($arr);
        return $arr;
	}

    private function getAllAvailableServicesKey($obj){
        $arr = []; 
        if(isset($obj->return->list)) {
        
            foreach ($obj->return->list as $service) { 

                    $arr[]=$service->key;                  
             }
        }
       // dd($arr);
        return $arr;
    }


    public function portalGetPhoneInstallmentStatus(){
    	return $status = $this->model->__call('portalGetPhoneInstallmentStatus',[$this->phone]);
    	 
    }
   
}
