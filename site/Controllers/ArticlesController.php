<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Articles;

class ArticlesController extends Controller {

	protected $model;
    
    public function __construct()
	{
		$this->model = new Articles;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	public function index($page)
	{
		$year = \Request::get('year');
		$month = \Request::get('month');

		$newsList = Articles::WithContent()->whereCollectionId($page->attached_collection_id)
							->year($year)->month($month)->OrderBy('creation_Date','DESC')->paginate(4);

        $startYear = $this->model->getFirstYear();

		return view('site.articles.list',compact('newsList','startYear'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		$article =  Articles::where('articles.slug','=',$slug)->withContent()->first();
      
		if(!$article){ abort(404); }
 
		return view('site.articles.view',compact('article'));  
	}

	 

}
