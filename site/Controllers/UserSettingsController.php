<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Models\GeocellId;
use Site\Models\SiteUser;
use App\Http\Requests\SiteUserRequest;


class UserSettingsController extends Controller {
 
    protected $model;
    protected $geocellId;
    protected $userInfo = [];
    protected $actServices = [];
    protected $phone;
    protected $facad;
    protected $insert;
    protected $directdebit;


    public function __construct(SiteUser $model,GeocellId $geocellIdModel, BillingFacadeServices $facade)
	{
	    
        $this->middleware('site_user');

        $this->geocellId=$geocellIdModel; 
        
        $this->model=$model; 
        $this->phone= \Session::get('phone');

        $this->facad = $facade;
        $this->insert = (object) array(
            'brand' => '',
            'isStaff' => '',
            'gsm' => '',
            'g4' => '',
            'phone_name' => '',
            'email' => ''
        );

	}
	
 	
	public function index() {
       
         if(!$this->geocellId->isAuthed()) { return view('site.user.auth-sign-in'); }

         if (!$this->model->wherePhone($this->phone)->exists()) {
              
              $this->model->phone = $this->phone;
              $this->model->save();
            }

        $unsub = false;
        $user = $this->model->wherePhone($this->phone)->first();
        //dd($user);
        $cities= \DB::table('dictionary_cities')->select('id','name_'.\App::getlocale().' as city_name')->get();
        $res = \DB::table('email_sub')->where('email', $user->email)->where('status', 0)->first();
        if(isset($res)){
            $unsub = true;
        }
        if (isset($this->directdebit->return->result->account)){
            $this->directdebit = true;
        } else {
            $this->directdebit = false;
        }
		return view('site.user.user_settings',compact('user','cities', 'unsub'))->with('directdebit', $this->directdebit);
	} 

    public function saveDetails($lang,SiteUserRequest $request){

        //dd($request->all());

        $user=$this->model->wherePhone($this->phone)->first();
        $sub = \DB::table('email_sub')->where('phone', $this->phone)->first();
//        dd($sub);
        $oldmail = $user->email;
        $user->first_name=$request->first_name;
        $user->last_name = $request->last_name;
        $user->personal_number = $request->personal_number;
        $user->address = $request->address;
        $user->address2 = $request->address2;
        $user->city = $request->city;
        $user->city2 = $request->city2;
        $user->district = $request->district;
        $user->district2 = $request->district2;
        $user->birth_date = $request->birth_date;
        $user->email = $request->email;
         $user->save();
        if(\Input::get('subscribe') == 1 && $user->email != ''){
            $this->subscribe($user->email);
//            dd('sub');
            if(isset($sub->email) && $user->email != $sub->email){
                $this->unsubscribe($sub->email);
            }
        }
        if(!\Input::get('subscribe') == 1){
//            dd('uns');
            $this->unsubscribe($user->email);
        }
        if($oldmail != $user->email && $oldmail != ''){
//            dd('yes');
            $this->unsubscribe($oldmail);
        }

        return redirect()->back(); 
    }


    private function subscribe($email){
        $sub = \DB::table('email_sub')->where('email', $email)->first();
        $blacklisted = \DB::table('blacklisted_mails')->where('email', $email)->first();
        if(isset($blacklisted)){
            return 0;
        }
        if($sub){
            if($sub->status == 0){
                $res = \DB::table('email_sub_status_action')->where('email', $email)->where('action', 0)->get();

                if(sizeof($res)<3 || empty($res)){
                    $resUserInfo = $this->facad->_getUserInfo($this->phone);
                    $g4 = $this->facad->__call('portalCheck4gCompatibility',[$this->phone]);
                    $isb2b = $this->facad->__call('portalGetSubscriberData',[$this->phone]);
                    $isb2b = isset($isb2b->return->isPrivate)?$isb2b->return->isPrivate:2;
                    if(isset($g4->return->resultCode) && $g4->return->resultCode == 1){

                        $this->insert->phone_name = $g4->return->model;
                        $this->insert->g4 = $g4->return->isSIM4GCompatibility;
                    }
                    $this->insert->brand = $resUserInfo->return->brand;
                    $this->insert->isStaff = $resUserInfo->return->isStaff;
                    $this->insert->gsm = $this->phone;
                    $this->insert->email = $email;
                    $res = \DB::table('email_sub')->where('email', $this->insert->email)->update(array('phone' => $this->insert->gsm,
                        'gsm_type' => $this->insert->brand, 'sub_type' => $this->insert->isStaff, 'phone_name' => $this->insert->phone_name,
                        'g4' => $this->insert->g4, 'b2b' => $isb2b, 'status' => 1));
                }
            }
        } else {
            $resUserInfo = $this->facad->_getUserInfo($this->phone);
            $g4 = $this->facad->__call('portalCheck4gCompatibility',[$this->phone]);
            $isb2b = $this->facad->__call('portalGetSubscriberData',[$this->phone]);
            $isb2b = isset($isb2b->return->isPrivate)?$isb2b->return->isPrivate:2;
            if(isset($g4->return->resultCode) && $g4->return->resultCode == 1){

                $this->insert->phone_name = $g4->return->model;
                $this->insert->g4 = $g4->return->isSIM4GCompatibility;
            }
            $this->insert->brand = $resUserInfo->return->brand;
            $this->insert->isStaff = $resUserInfo->return->isStaff;
            $this->insert->gsm = $this->phone;
            $this->insert->email = $email;
            $res = \DB::table('email_sub')->insert(array('email' => $this->insert->email, 'phone' => $this->insert->gsm,
                'gsm_type' => $this->insert->brand, 'sub_type' => $this->insert->isStaff, 'phone_name' => $this->insert->phone_name,
                'g4' => $this->insert->g4, 'b2b' => $isb2b));
        }
    }


    private function unsubscribe($email){
        $sub = \DB::table('email_sub')->where('email', $email)->first();
        if($sub){
            $sub = \DB::table('email_sub')->where('email', $email)->update(['status' => 0]);
            $res = \DB::table('email_sub_status_action')->insert(['email' => $email, 'action' => 0]);
        }

    }
 

}
