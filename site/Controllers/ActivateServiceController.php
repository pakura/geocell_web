<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Models\Products;
use Site\Models\Services;

class ActivateServiceController extends Controller {
 
    protected $model;
    protected $phone;

    public function __construct(BillingFacadeServices $model)
	{
		$this->middleware('site_user');
        $this->model=$model;
        $this->phone= \Session::get('phone');
	}
	
 	
	public function index($lang,$id) {
      //var_dump($id);
	    $service = Products::withPrice()->find($id);
        $isBundle = \Request::get('is_bundle');
        $userInfo = \Session::get('userInfo');
        $periodic = 2;
//dd($service->collection_id);
        if ($service->collection_id == 66){
//            return view('site.lightbox.active-service_type',compact('service','isBundle'));
//            $periodic = 1;
            if($userInfo->return->isStaff == false){
                return view('site.lightbox.active-service_type',compact('service', 'isBundle', 'periodic'));
            } else {
                return view('site.lightbox.activate-service',compact('service', 'isBundle', 'periodic'));
            }
        } else {
            if(@$userInfo->return->balance<$service->value) {
                return  view('site.lightbox.service-fill-balance',compact('service','userInfo'));
            }
            return view('site.lightbox.activate-service',compact('service', 'isBundle', 'periodic'));
        }

	   // return view('site.lightbox.activate-service',compact('service','isBundle')); 
	} 

    public function confirm($lang,$id){
        $service = Products::withPrice()->find($id);
        $userInfo = \Session::get('userInfo');
        $periodic = \Request::get('periodic');
        $isBundle = \Request::get('is_bundle');
        $isBundle = $isBundle?1:0;
        if(@$userInfo->return->balance<$service->value) {
            return  view('site.lightbox.service-fill-balance',compact('service','userInfo'));
        }
        return view('site.lightbox.activate-service',compact('service', 'isBundle', 'periodic'));
    }

    public function buy($lang,$id) {
       
        $phone_num = \Session::get('phone');
        $service = Products::withPrice()->find($id);
        $periodic = \Request::get('periodic');

        if($periodic==1) { $service->sku=$service->sku.'_PERIODIC'; } else { $periodic = 0; }

        $userInfo = \Session::get('userInfo');
        if($userInfo->return->isStaff == true && (
                $service->sku == 'S_INTERNET_6' ||
                $service->sku == 'S_INTERNET_19' ||
                $service->sku == 'S_INTERNET_26' ||
                $service->sku == 'S_INTERNET_40' ||
                $service->sku == 'S_INTERNET_15'
            )) $service->sku = $service->sku.'_FOR_STAFF';
//        dd( $service->sku);
        if(@$userInfo->return ->balance<$service->value) {
          return  view('site.lightbox.service-fill-balance',compact('service','userInfo'));
      }  
       //dd( $service);
       /*dd(\Session::get('phone'));*/
       $res = $this->model->__call('portalActivateService',[$phone_num,'',$service->sku]);
       /*dd($res);*/
       //dd($res->return->restrictionKeys);
      
       if(isset($res->return->resultCode) &&  $res->return->resultCode<0) {
            if($res->return->resultCode==-1004) {
                $msg = (isset($userInfo->return->brand) && $userInfo->return->brand==1)?trans('site.error_1004_1'):trans('site.error_1004_2');
                return  view('site.lightbox.msg',compact('msg')); 
            }
                 /* არასაკმარისი თანხა */
            elseif($res->return->resultCode==-1007) { 
               // $userInfo = $this->model->__call('portalGetGSMInfo',[$this->phone]);
                return  view('site.lightbox.service-fill-balance',compact('service','userInfo')); 
            }
            elseif(\Lang::has('site.error_'.abs($res->return->resultCode)) ){ 
               // $userInfo = $this->model->__call('portalGetGSMInfo',[$this->phone]);
                $msg = trans('site.error_'.abs($res->return->resultCode));
                return  view('site.lightbox.msg',compact('msg')); 
            }
            elseif(isset($res->return->restrictionKeys)){
              
              return $this->restrictions($service,$res->return->restrictionKeys, $periodic);

            } 
           
            
           
            $msg = trans('site.error_1001');

            \Session::flash('msg', $msg);

            return  view('site.lightbox.activate-service',compact('service'));  
       }
       else
       {
            
           /* if($service->sku=='S_TRIPLE_ZERO') {

               $res = $this->model->__call('portalGetFriendList',[$this->phone]);

               $numbers = isset($res->return->list)?$res->return->list:[]; 

              return view('site.lightbox.service-triple',compact('service','numbers'));
            } */

            
           //if($service->sku=='S_TRIPLE_ZERO') {   $this->saveNumbers(); }

            return view('site.lightbox.activate-service-notification',compact('service')); 
       }
       
    } 

    
    private function restrictions($service, $restriction_key, $periodic){
    		//$restriction_key = $res->return->restrictionKeys;	
         // dd($restriction_key->key); 
         $removable=true; 

         $service_keys = [];

         if(is_array($restriction_key)) { 
            foreach ($restriction_key as $service_key) {
            
             $service_keys[] = $service_key->key;
             if(!$service_key->removable){$removable=false;}
             //$service_keys[] = str_replace("_PERIODIC", "", $service_key->key);
            }
             
          }

         else {
            
            //$service_keys[] =  str_replace("_PERIODIC", "", $restriction_key->key);
            $service_keys[] = $restriction_key->key;
             if(!$restriction_key->removable){$removable=false;}
         }
          
           // dd($restriction_key->key); 
        	$restrictions=[];

           $lang = \App::getLocale();

        	$restrictions = Services::whereIn('service_key',$service_keys)->select('*','service_key as sku','service_title_'.$lang.' as title')->groupby('service_key')->get(); 

        	return  view('site.lightbox.service-restriction',compact('service','restrictions','removable', 'periodic'));
    }

    public function forFriend($lang,$id){
       $service = Products::withPrice()->find($id); 
       return view('site.lightbox.activate-service-for-friend',compact('service')); 
    }

    public function buyForFriend($lang,$id){ 
          $number = \Request::get('phone');
 
          if(!$number) { return $this->forFriend($lang,$id); }

          $service = Products::withPrice()->find($id);

          $res = $this->model->__call('portalActivateService',[$this->phone, '995'.$number,$service->sku]);

           
           if(isset($res->return->resultCode) &&  $res->return->resultCode<0) {
            
            /* არასაკმარისი თანხა */
 
            if($res->return->resultCode==-1007) {
                $userInfo = $this->model->__call('portalGetGSMInfo',[$this->phone]);
                return  view('site.lightbox.service-fill-balance',compact('service','userInfo')); 
            }
            elseif(\Lang::has('site.error_'.abs($res->return->resultCode)) ){ 

                $msg = trans('site.error_'.abs($res->return->resultCode));
                return  view('site.lightbox.msg',compact('msg')); 
            }
            
             $msg = trans('site.error_1001');

            \Session::flash('msg', $msg);

            return  view('site.lightbox.activate-service-for-friend',compact('service'));  
       }
       else
       {

          return view('site.lightbox.activate-service-for-friend-notification',compact('service','number')); 
       }

    }
        


    public function saveNumbers($lang,$id){

    	$service = Products::withPrice()->find($id);

      $numbers = \Request::get('friend_phone');
    	 
    	if(is_array($numbers) && count($numbers>0)) {

    		foreach ($numbers as $number) {
    			 $this->model->__call('portalFriendSlotAction',[$this->phone,'995'.$number,1]);
    		}
    	}
       return view('site.lightbox.activate-service-notification',compact('service'));   
    	//dd($numbers);
    }


    public function activehlr($lang, $serv){

        $res = $this->model->__call('portalActivateService',[$this->phone,'',$serv]);
        if ($res->return->resultCode == 1){
            echo 'ok';
        } elseif($res->return->resultCode == -1007) {
            $service = \DB::table('services_list')->where('service_key', $serv)->first();
            if($lang == 'en'){
                $service->title = $service->service_title_en;
            } else {
                $service->title = $service->service_title_ge;
            }
            if ($service->price<1){
                $service->price = 1;
            }
            $service->value = $service->price;
            $userInfo = $this->model->__call('portalGetGSMInfo',[$this->phone]);
            return view('site.lightbox.service-fill-balance', compact('service', 'userInfo'));
        } else {
            $msg = trans('site.error_1001');
            if(isset($res->return->resultCode) &&  $res->return->resultCode<0)  {
                $msg = trans('site.error_'.abs($res->return->resultCode));
            }
            echo $msg;
        }
    }

    public function deactivehlr($lang, $serv){
        $res = $this->model->__call('portalTerminateService',[$this->phone,$serv]);
        if ($res->return->resultCode == 1){
            echo 'ok';
        } else {
            $msg = trans('site.error_1001');
            if(isset($res->return->resultCode) &&  $res->return->resultCode<0)  {
                $msg = trans('site.error_'.abs($res->return->resultCode));
            }
            echo $msg;
        }
    }


}
