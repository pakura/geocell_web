<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Models\GeocellId;

class UserInstallmentController extends Controller {
 
    protected $model;
    protected $geocellId;
    protected $userInfo = [];
    protected $actServices = [];
    protected $phone;

    public function __construct(BillingFacadeServices $model,GeocellId $geocellIdModel)
	{
	    
        $this->middleware('site_user');

        $this->geocellId=$geocellIdModel; 
        
        $this->model=$model; 
        $this->phone= \Session::get('phone');
       
	}
	
 	
	public function index() {
       
         if(!$this->geocellId->isAuthed()) { return view('site.user.auth-sign-in'); }
        
        $installment_status =  $this->model->__call('portalGetPhoneInstallmentStatus',[$this->phone]);
        //dd($installment_status);
        $res = \DB::table('install_page_log')->insert(['mobile' => $this->phone]);
        return view('site.user.installment')->with('installment',$installment_status->return);  
	} 

  
   
 

}
