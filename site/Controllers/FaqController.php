<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

/*use App\Models\Collections;*/
use Site\Models\Faq;

class FaqController extends Controller {

    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	public function index($page)
	{

		$subPages = \Site\Models\Pages::whereParentId($page->parent_id)->wherePageType(12)->withContent()->get();
		
		$faqs = Faq::whereCollectionId($page->attached_collection_id)->withContent()->get();
      
		return view('site.faq.list',compact('faqs','subPages'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		$faq =  Faq::whereSlug($slug)->withContent()->first();
		if(!$faq){ abort(404); }
		return view('site.faq.view',compact('faq'));  
	}

	 

}
