<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Models\Products;
use Site\Models\Services;

class ActivateServiceController extends Controller {
 
    protected $model;
    protected $phone;

    public function __construct(BillingFacadeServices $model)
	{
		$this->middleware('site_user');
    $this->model=$model; 
    $this->phone= \Session::get('phone');
	}
	
 	
	public function index($lang,$id) {
      //var_dump($id);
	   $service = Products::withPrice()->find($id); 

     $isBundle = \Request::get('is_bundle');
     $isBundle = $isBundle?1:0;

     //dd( $service );
	   return view('site.lightbox.activate-service',compact('service','isBundle')); 
	} 

    public function buy($lang,$id) {

      $service = Products::withPrice()->find($id);

      $userInfo = \Session::get('userInfo');
      if(@$userInfo->return->balance<$service->value) {
          return  view('site.lightbox.service-fill-balance',compact('service','userInfo')); 
      }  
       //dd( $service);
       /*dd(\Session::get('phone'));*/
       $res = $this->model->__call('portalActivateService',[$this->phone,'',$service->sku]);  
       //dd($res);
       //dd($res->return->restrictionKeys);
      
       if(isset($res->return->resultCode) &&  $res->return->resultCode<0) {
            
            /* არასაკმარისი თანხა */

            if($res->return->resultCode==-1007) { 
               // $userInfo = $this->model->__call('portalGetGSMInfo',[$this->phone]);
                return  view('site.lightbox.service-fill-balance',compact('service','userInfo')); 
            }
            elseif($res->return->resultCode==-1005) { 
               // $userInfo = $this->model->__call('portalGetGSMInfo',[$this->phone]);
                $msg = trans('site.error_1005');
                return  view('site.lightbox.msg',compact('msg')); 
            }
            
            elseif(isset($res->return->restrictionKeys)){
            	
            	return $this->restrictions($service,$res->return->restrictionKeys);

            } 
            $msg = trans('site.error_1001');

            \Session::flash('msg', $msg);

            return  view('site.lightbox.activate-service',compact('service'));  
       }
       else
       {
            
           /* if($service->sku=='S_TRIPLE_ZERO') {

               $res = $this->model->__call('portalGetFriendList',[$this->phone]);

               $numbers = isset($res->return->list)?$res->return->list:[]; 

              return view('site.lightbox.service-triple',compact('service','numbers'));
            } */

            
           //if($service->sku=='S_TRIPLE_ZERO') {   $this->saveNumbers(); }

            return view('site.lightbox.activate-service-notification',compact('service')); 
       }
       
    } 

    
    private function restrictions($service, $restriction_key){
    		//$restriction_key = $res->return->restrictionKeys;	
         // dd($restriction_key->key); 
         $removable=true; 

         $service_keys = [];

         if(is_array($restriction_key)) { 
            foreach ($restriction_key as $service_key) {
            
             $service_keys[] = $service_key->key;
             if(!$service_key->removable){$removable=false;}
             //$service_keys[] = str_replace("_PERIODIC", "", $service_key->key);
            }
             
          }

         else {
            
            //$service_keys[] =  str_replace("_PERIODIC", "", $restriction_key->key);
            $service_keys[] = $restriction_key->key;
             if(!$restriction_key->removable){$removable=false;}
         }
          
           // dd($restriction_key->key); 
        	$restrictions=[];

           $lang = \App::getLocale();

        	$restrictions = Services::whereIn('service_key',$service_keys)->select('*','service_key as sku','service_title_'.$lang.' as title')->groupby('service_key')->get(); 

        	return  view('site.lightbox.service-restriction',compact('service','restrictions','removable'));  
    }

    public function forFriend($lang,$id){
       $service = Products::withPrice()->find($id); 
       return view('site.lightbox.activate-service-for-friend',compact('service')); 
    }

    public function buyForFriend($lang,$id){ 
          $number = \Request::get('phone');
 
          if(!$number) { return $this->forFriend($lang,$id); }

          $service = Products::withPrice()->find($id);

          $res = $this->model->__call('portalActivateService',[$this->phone, '995'.$number,$service->sku]);

           if(isset($res->return->resultCode) &&  $res->return->resultCode<0) {
            
            /* არასაკმარისი თანხა */

            if($res->return->resultCode==-1007) { 
                $userInfo = $this->model->__call('portalGetGSMInfo',[$this->phone]);
                return  view('site.lightbox.service-fill-balance',compact('service','userInfo')); 
            }
            
             $msg = trans('site.error_1001');

            \Session::flash('msg', $msg);

            return  view('site.lightbox.activate-service-for-friend',compact('service'));  
       }
       else
       {

          return view('site.lightbox.activate-service-for-friend-notification',compact('service','number')); 
       }

    }
        


    public function saveNumbers($lang,$id){

    	$service = Products::withPrice()->find($id);

      $numbers = \Request::get('friend_phone');
    	 
    	if(is_array($numbers) && count($numbers>0)) {

    		foreach ($numbers as $number) {
    			 $this->model->__call('portalFriendSlotAction',[$this->phone,'995'.$number,1]);
    		}
    	}
       return view('site.lightbox.activate-service-notification',compact('service'));   
    	//dd($numbers);
    }

}
