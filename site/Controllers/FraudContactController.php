<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Articles;
use Mail;

use App\Http\Requests\ContactRequest;

class FraudContactController extends Controller {

    protected $email = 'Fraud@geocell.ge';
    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	public function index($page)
	{
		$siblingPages = \Site\Models\Pages::whereParentId($page->parent_id)->withContent()->where('pages.visibility','=',1)->get();

		$parentPages = \Site\Models\Pages::whereParentId(\DB::raw('(select parent_id from pages where id = '.(int)$page->parent_id.')'))->withContent()->where('pages.visibility','=',1)->get();

		return view('site.fraud.contact',compact('siblingPages','parentPages'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		/*$faq =  Faq::whereSlug($slug)->withContent()->first();
		if(!$faq){ abort(404); }
		return view('site.faq.view',compact('faq'));  */
	}


	public function send(ContactRequest $request){
		$data = ['email'=>$request->email,'comment'=>strip_tags($request->comment),'phone'=>$request->department];
		
		$mail = Mail::send('emails.contact', $data, function ($message) {

		   $message->to('fraud@geocell.ge')->from('onlineportal@geocell.ge')->subject('Email from geocell.ge!');
	
		});	



		if($mail){
			return \Response::json(['msg'=>'თქვენი შეტყობინება გაიგზავნა']);
		}
		else{
			return \Response::json(['error'=>'დაფისქირდა შეცდომა. გთხოვთ ცადოთ მოგვიანებით.']);
		}
		
	}
	 

}
