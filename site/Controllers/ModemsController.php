<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Products;

class ModemsController extends Controller {

    
    public function __construct()
	{
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	public function index($page)
	{
        
		$products = Products::collection($page->attached_collection_id)->where('products.visibility','=',1)->orderBy('value', 'asc')->withPriceAndAttachment()->paginate(8);
		$options = [];
        $Cur_brands = Products::collection($page->attached_collection_id)->where('products.visibility','=',1)->groupBy('brand_id')->get();
		/* length(col),col*/
		foreach ($products as $product) {
			$options[$product->product_id] = \Site\Models\ProductOptions::orderBy(\DB::raw('CAST(products_options.value  AS DECIMAL(8,2))'))->getOptions	($product->product_id);
		}
		$brandid = array();

		foreach ($Cur_brands as $k => $v){
            $brandid[] = $v->brand_id;
        }
        $brands = \DB::table('brands')->where('visibility',1)->whereIn('brands.id', $brandid)->get();
        $arr = [];
        if(isset($_GET['price']) && ($_GET['price'] == 'asc' || $_GET['price'] == 'desc')) { $arr['price'] = $_GET['price']; }
        if(isset($_GET['brand']) && (float)$_GET['brand'] > 0 ) { $arr['brand'] = $_GET['brand']; }
		return view('site.products.modems',compact('products', 'options','brands','arr'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		$product =  Products::where('products.slug','=',$slug)->WithPriceAndAttachment()->first();
		$photos = Products::getAttachedFiles($product->product_id);
		$prices = Products::getAllPrices($product->product_id);
		$options = \Site\Models\ProductOptions::orderBy('pol.position','desc')->getOptions($product->product_id);
		if(!$product){ abort(404); }
		$productPage = true;
		return view('site.products.modem',compact('product','photos','productPage','options','prices'));
	}

	 

}
