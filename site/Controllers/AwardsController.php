<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Collections;
use Site\Models\Awards;

class AwardsController extends Controller {

    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	public function index($page)
	{
		$awards = Awards::withContent()->get(); 
		return view('site.awards.list',compact('awards'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/*public function show($slug)
	{
		$faq =  Faq::whereSlug($slug)->withContent()->first();
		if(!$faq){ abort(404); }
		return view('site.faq.view',compact('faq'));  
	}*/

	 

}
