<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Site\Models\Services;
use Site\Models\GeocellId;
use App\Models\BillingFacadeServices;
use Illuminate\Http\RedirectResponse;
use Site\Models\Products;

class SecurityPageController extends Controller {

    protected $model;
    protected $facade;
    protected $seccode;
    protected $phone;

    public function __construct(GeocellId $facade, BillingFacadeServices $model){
        $this->middleware('site_user');
        $this->model = $model;
        $this->facade = $facade;
    }

    public function index(){
        if(!$this->facade->isAuthed()) { return view('site.user.auth-sign-in'); }
        $this->phone= \Session::get('phone');
        $this->seccode = $this->model->__call('portalRequestSecretCodeIndexes', [$this->phone]);
        if(isset($this->seccode->return->secretCodeAccountId)){
            $date1 = explode(' ', $this->seccode->return->accountRegistrationDate);
            $date = explode('/', $date1[0]);
            $this->seccode->return->accountRegistrationDate = $date[2].'-'.$date[1].'-'.$date[0].' '.$date1[1];
//            dd($this->seccode->return->accountRegistrationDate);
            $oldUser = \DB::table('call_details_profile')->where('phone', $this->phone)->first();
            if(isset($oldUser->phone)){
                if($oldUser->account_id != $this->seccode->return->secretCodeAccountId){
                    $res1 = \DB::table('call_details_profile')->where('phone', $oldUser->phone)->update(['phone' => $this->phone, 'account_id' => $this->seccode->return->secretCodeAccountId, 'changes_date' => $this->seccode->return->accountRegistrationDate ]);
                    $res2 = \DB::table('call_details')->where('phone', $this->phone)->delete();
                    $res3 = \DB::table('init_call_details')->where('phone', $this->phone)->delete();
                    $res4 = \DB::table('invoice_archive')->where('phone', $this->phone)->delete();
                }
            } else {
                $res = \DB::table('call_details_profile')->insert(['phone' => $this->phone, 'account_id' => $this->seccode->return->secretCodeAccountId, 'changes_date' =>$this->seccode->return->accountRegistrationDate ]);
            }
        }
        return view('site.secretcode.view')->with('seccode',$this->seccode->return)->with('callback',false);
    }

    public function show($lang, $num1, $num2, $num3, $key){
        if ($num1 != "" && $num2 != "" && $num3 != ""){
            $this->phone = \Session::get('phone');
            $code = $num1.$num2.$num3;
            $data = array($this->phone, $code, $key);
            $data = implode(',', $data);
            $this->seccode = $this->model->__call('portalValidateSecretCode', array($data));
            if ($this->seccode->return->resultCode == 1){
                \Session::put('fullauth', true);
                if(\Session::get('accsesSign') == true ){
                    echo ('signature');
                }elseif(\Session::get('accsesDetails') == true ) {
                    echo ('details');
                } else {
                    echo 'ok';
                }

            } else {
                echo $this->seccode->return->resultCode;
            }
        } else {
            echo 'no';
        }
    }

}
