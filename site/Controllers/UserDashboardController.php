<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Models\GeocellId;
use Site\Models\Services;
use Illuminate\Http\RedirectResponse;


class UserDashboardController extends Controller {
 
    protected $model;
    protected $geocellId;
    protected $actServices = [];
    protected $phone;
    protected $directdebit;
    protected $userInfo;

    protected $bonusesKeys=['S_SOCIAL_NETWORK_PACKAGE','S_SMARTPHONE_PACKAGE','TEST_DRIVE','S_ROAMING_10MB','S_ROAMING_50MB','S_ROAMING_100MB','S_ROAMING_150MB'];
    

    public function __construct(BillingFacadeServices $model,GeocellId $geocellIdModel)
	{
        $this->middleware('site_user');
        $this->geocellId=$geocellIdModel;
        $this->model=$model;
        $this->phone= \Session::get('phone');
        if(!$this->geocellId->isAuthed()) { return view('site.user.auth-sign-in'); }
        $this->directdebit = $this->model->__call('getAccountsByGSM', [$this->phone]);

    }


	public function index() {

        \Session::put('url',\Request::url());    
         if(!$this->geocellId->isAuthed()) { return view('site.user.auth-sign-in'); }
           
        
         $userInfo = $this->getUserInfo();
        $this->userInfo = $userInfo;
         if($userInfo->return->resultCode<0) {
                return view('site.user.msg')->with('msg',trans('site.error_1001'));
             } 


		 $userServices = $this->model->__call('portalGetActiveServices',[$this->phone]);


         if(isset($userServices->return->resultCode) &&  $userServices->return->resultCode==1){
            \Session::put('userServices',$userServices);
         }
         $userServices = \Session::has('userServices')?\Session::get('userServices'):$userServices;


         /*get services keys*/
         $services_keys = $this->getActiveServicesKey($userServices);
         
//          dd($services_keys);

         /* take info from database according keys */
         $actServices = $this->getServicesForList($services_keys,$userInfo->return->brand);
//         dd($actServices);
         /*merge facade's services info with DB services info */
         $actServices =  $this->getOrderedServiceData($actServices);   
//         dd($actServices);
         $numbers = [];
         if(in_array('S_TRIPLE_ZERO', $services_keys)) {
            $res = $this->model->__call('portalGetFriendList',[$this->phone]);
            
            $numbers= [];  
            if( isset($res->return->list) && is_array($res->return->list)) { $numbers = $res->return->list; } 
            elseif( isset($res->return->list) ){  $numbers[] = $res->return->list;  }
         }
        // dd($installment_status->return->planedPayments[0]);
            //dd($services_keys);
         //dd($numbers);

        /*get installment status*/
        $installment_status =  $this->model->__call('portalGetPhoneInstallmentStatus',[$this->phone]);
        //dd($installment_status);

         /*get available services for the number*/
         $available_services= $this->addService($userInfo->return->brand);
//		  dd($available_services);
//        dd($actServices);
         $showInstallNot = \DB::table('installment_close_action')->where('phone_num', '=', $this->phone)->first();
        $lang = \App::getLocale();

         $hlrServ = \DB::table('services_list')->where('service_type', '=', 4)->where('brand', '=', $userInfo->return->brand)->select('*','service_title_'.$lang.' as title')->get();
//        dd($hlrServ);
        if (isset($this->directdebit->return->result->account)){
            $this->directdebit = true;
        } else {
            $this->directdebit = false;
        }
//        dd( $this->phone);
		 return view('site.user.dashboard')->with('userInfo',$userInfo)
                                           ->with('actServices',$actServices)      
                                           ->with('numbers',$numbers)
                                           ->with('installment',$installment_status->return)
                                           ->with('installmentNotification', $showInstallNot)
                                           ->with('hlrServ', $hlrServ)
                                           ->with('directdebit', $this->directdebit)
		 								   ->with('available_services',$available_services);  	
	} 

  
    public function getUserInfo(){

        // dd(\Session::get('phone'));
        $resUserInfo = $this->model->_getUserInfo($this->phone);
//         dd($resUserInfo);
       if(isset($resUserInfo->return->resultCode) && $resUserInfo->return->resultCode==1){
            \Session::put('userInfo',$resUserInfo);
         }

         $userInfo = \Session::has('userInfo')?\Session::get('userInfo'):$this->model->_getUserInfo($this->phone);

        return $userInfo;
    }

    /*public function check4GInfo(){

        // dd(\Session::get('phone'));
         return $this->model->__call('portalCheck4gCompatibility',[$this->phone]);

    }*/

    public function fill_balance(){
         
        //$amount = \Input::get('amount');
        return view('site.lightbox.fill-balance')->with('phone',$this->phone);
    }
    
  /*  public function fill_balance_confirmation(){
        $amount = \Input::get('amount');
        
        return view('site.lightbox.fill-balance-confirmation',compact('amount'));
    }*/

    public function deactivateService($lang,$service_key){
        $userInfo = \Session::get('userInfo');
        $jsn = \Input::get('jsn');
        $response = $this->model->__call('portalTerminateService',[$this->phone,$service_key]);
//        dd($response);
       
        if(isset($response->return->resultCode) &&  $response->return->resultCode<0)  {
           $msg = trans('site.error_1001');
           if($jsn) { return \Response::json(['error'=>$msg]); }
        } 
        else {
             $msg  = ((isset($userInfo->return->brand) && $userInfo->return->brand==1)?trans('site.deactivate_service_postpaid'):trans('site.deactivate_service'));

             if($jsn) { return \Response::json(['success'=>1]); }
        }

        return view('site.lightbox.msg',compact('msg'));
    }

    public function removeService($lang, $service_key){
        $service = Services::whereServiceKey($service_key)->select('*','description_'.$lang.' as description')->first();

    
        return view('site.lightbox.deactivate-service-notification',compact('service'));
       

        // return view('site.lightbox.couldnot-deactivate-service',compact('service'));
    }
    
    public function  activateService($lang,$service_key){
        $response = $this->model->__call('portalActivateService',[$this->phone,'',$service_key]);
//        dd($response);
    }
    
    public function getGeocreditInfo(){
        $response = $this->model->__call('portalGetGeoCreditInfo',[$this->phone]);
       // dd($response);
   
        return view('site.lightbox.geo-credit-info',compact('response'));
    }
    
    public function removeNumber($lang,$number){
    	$response =  $this->model->__call('portalFriendSlotAction',[$this->phone,$number,2]);
        if($response->return->resultCode==1) {
        	return view('site.lightbox.msg')->with('msg',trans('site.remove_num'));
        } 
    }

    public function removeNumberConfirmation($lang,$number){
    
        return view('site.lightbox.remove-number-confirmation',compact('number'));
      
    }

    public function addNumber(){
    	 $number = \Request::get('friend_number');
    	 if($number) {
            $number ='995'.$number; 

            if($number==$this->phone) { return \Response::json(['msg'=>trans('site.error_your_number')]); }

    	 	$response = $this->model->__call('portalFriendSlotAction',[$this->phone,$number,1]);
            if(isset($response->return->resultCode) && $response->return->resultCode==1) {
               $tpl = view('site.lightbox.add-number',compact('number'))->render();
               return \Response::json(['tpl'=>$tpl]);
            } 

            elseif(isset($response->return->resultCode) && $response->return->resultCode==-1006){
                return \Response::json(['msg'=>trans('site.not_geocell')]);
            } 

            else {
                return \Response::json(['msg'=>trans('site.error_1001')]);
            }
            
    	 }
    	 
    	 
    }

    public function addService($brand=2){

        $services = $this->model->__call('portalGetAvailableServices',[$this->phone]);
//        die(print_r($services));
        if(isset($services->return->list) ){
            \Session::put('availableServices',$services);
         }
        $services = \Session::has('availableServices')?\Session::get('availableServices'):$services;
//        die(print_r($services->return->list));
        $skus = array();
        foreach ($services->return->list as $val){
            $skus[$val->key] = ($val->removable == 1)?true:false;
        }
        $services = $this->getServices($this->getAllAvailableServicesKey($services),$brand);
        for ($i = 0; $i<sizeof($services); $i++){
             if ($skus[$services[$i]->service_key] == true){
                 $services[$i]->removable = true;
             } else {
                 $services[$i]->removable = false;
             }
        }
//        foreach($services as $key => $val){
//            if($this->userInfo->return->isStaff == true && $val->service_key == 'S_INTERNET_35') {
//                unset($services[$key]);
//            }
//        }
//        dd($services);
        return $services;
       // return view('site.lightbox.add-service',compact('services'));
    }

	private function getServices ($keys,$brand=2){
        
		//$keys = $this->getActiveServicesKey($obj);
//         dd($keys);
        $lang = \App::getLocale();
		//$brand = isset($this->userInfo->return->brand) ? $this->userInfo->return->brand : 2;
        $services = Services::join('products as p','p.sku','=','service_key')
                            ->leftjoin('products_attachments as a', function ($join) {

                                  $join->on('a.product_id','=','p.id')
                                       ->where('a.file_type','=',1);
                             }) 
                             ->leftjoin('products_attachments as m', function ($join) {

                                  $join->on('m.product_id','=','p.id')
                                       ->where('m.file_type','=',5);
                             })    
                            ->whereBrand($brand)       			        
                            ->whereIn('service_key',$keys)
                            ->select('p.id as product_id','services_list.*','services_list.description_'.$lang.' as description','a.file','m.file as meti_file')
                            ->orderby('position')
                            ->groupBy('service_key')
                            ->get(); 
//        dd($services);
        //return $this->getOrderedServiceData($services);	
        return $services;	   
	}
    
    private function getServicesForList ($keys,$brand=2){

        //$keys = $this->getActiveServicesKey($obj);
        // dd($keys);
        $lang = \App::getLocale();
    	//$brand = isset($this->userInfo->return->brand) ? $this->userInfo->return->brand : 2;


        $services = Services::leftjoin('products as p','p.sku','=',\DB::raw('replace(`service_key`, "_PERIODIC","")'))
                            ->leftjoin('products_attachments as a', function ($join) {

                                  $join->on('a.product_id','=','p.id')
                                       ->where('a.file_type','=',1);
                             })  
                            ->leftjoin('products_attachments as m', function ($join) {

                                  $join->on('m.product_id','=','p.id')
                                       ->where('m.file_type','=',5);
                             }) 
                            ->whereBrand($brand)                           
                            ->whereIn('service_key',$keys)  
                            ->select('*','description_'.$lang.' as description','a.file','m.file as meti_file')                    
                            ->get()->groupBy('service_key'); 

        //dd($services);
        //return $this->getOrderedServiceData($services);   
        return $services;
    }

/*private function dbg($d){
	echo "<pre>";
	var_export($d);
	echo "</pre>";
}*/

    private function getOrderedServiceData($services){
    	$data = [];
        $sortByDate = [];
        //$service - list from DB;
        //$this->actServices - service list from wsdl;
        //dd($services);
        //dd($this->actServices);

    	foreach($this->actServices as $key=>$srvc) {
             
         
            if(!isset($services[$srvc->key])){ continue; }

            
            $service = clone $services[$srvc->key][0];
            $service['data'] =  clone $srvc;
        
//           dd($service);

           /* if(!isset($data[$service->service_type]['data'])) { $data[$service->service_type]['data'] = [];}*/
//    		dd($service);
            $data[$service->service_type]['data'][] = $service;

//            dd($data);
           
            $sortByDate[$service->service_type]['data'][$key] = strtotime(str_replace('/', '-',isset($srvc->expirationDate)?$srvc->expirationDate:'')) ; 
    		

            /*set amount to array*/
    		if(!array_key_exists('amount',$data[$service->service_type])) { 

                $data[$service->service_type]['amount'] = 0;


    		}
            if($data[$service->service_type]['amount']==-1 || ($service->service_key=='S_METI_MINUTE_ONNET' || $service->service_key=='S_METI_MINUTE_ONNET_PERIODIC')) { continue; }

            $amt = (isset($srvc->amount)?$srvc->amount:0);

    		$data[$service->service_type]['amount'] =(($amt==-1)?$amt:$data[$service->service_type]['amount'] + $amt);
            


    	}

        if(isset($sortByDate[99]['data']) && isset($data[99]['data'])) { array_multisort($sortByDate[99]['data'], SORT_ASC, $data[99]['data']); }
        
    	/* $this->dbg($data);*/
    	return $data;
    }

	private function getActiveServicesKey($obj){
		$arr = []; 
        if(isset($obj->return->list)) {
		  
           
    		if(is_array($obj->return->list)){
	            foreach ($obj->return->list as $service) { 

	                 if(isset($service->key)) {
	                 	if(in_array($service->key, $this->bonusesKeys) && $service->bonus==true) { $service->key = $service->key.'_BONUS'; }
                        

	    		 	 	$arr[]=$service->key;
	    		 	 	//$this->actServices[$service->key] = $service;
	    		 	 }

	    		 }

                $this->actServices = $obj->return->list;
    		}
    		else {
          
	                  
	                 if(isset($obj->return->list->key)) {
	    		 	 	$arr[]=$obj->return->list->key;
	    		 	 	//$this->actServices[$obj->return->list->key] = $obj->return->list;

                        $this->actServices[] = $obj->return->list;
	    		 	 }

    		}


        }
       // dd($this->actServices);
        //dd($arr);
        return $arr;
	}

    private function getAllAvailableServicesKey($obj){
        $arr = []; 
        if(isset($obj->return->list)) {
        
            foreach ($obj->return->list as $service) {
                $arr[]=$service->key;
             }
        }
       // dd($arr);
        return $arr;
    }


    public function portalGetPhoneInstallmentStatus(){
    	return $status = $this->model->__call('portalGetPhoneInstallmentStatus',[$this->phone]);
    	 
    }


    public function closeinstall(){
        $phoneNum = \Session::get('phone');
        $res = \DB::table('installment_close_action')->where('phone_num', '=', $phoneNum)->first();
        if (!$res){
            \DB::table('installment_close_action')->insert(
                ['phone_num' => $phoneNum ]
            );
        } else {
            \DB::table('installment_close_action')
                ->where('phone_num', '=', $phoneNum)
                ->update(['updated' => 1]);
        }
        return 'ok';
    }




}
