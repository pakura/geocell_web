<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use  Site\Models\Gallery;
use  Site\Models\Products;
use  Site\Models\Articles;

class SearchController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$q = isset($_GET['q'])?$_GET['q']:'';
		$cs = new googleCustomSearch();

		//get the search terms from post and clean it, whatever you called them
		$searchterms = $cs->cleanInput($q);
		//if the start number was querystringed
		if(isset($_GET['s']))
		{
			$start = (int)$_GET['s'];
		}
		else
		{
			$start = 0;
		}

		//if the query was querystringed through (for pagination purposes)
		if($q)
		{
			$searchterms = $cs->cleanInput($q);
		}
		$language = 'en';

		//get the XML (as built in the class)
		$xml = $cs->getXML($searchterms,$start,$language);

		//get the number of results on this page
		$total = $cs->getPageTotal($xml);

		//get the *ESTIMATED* total results, only useful on the final page
		$totalnum = $cs->getTotalResults($xml);

		//to show the Y of "showing results x - y"
		$displaytotal = $total + $start;

		//search results
		$output_data = $cs->writeSearchResults($xml);
		if ($output_data == ''){
			$output_data = array();
		}


		return view('site.search', compact('output_data', 'displaytotal', 'total', 'start', 'q', 'totalnum'));
	}


}

class googleCustomSearch
{
	function googleCustomSearch()
	{
		//empty constructor
	}

	//a handy function to clean the inputs from a search query
	function cleanInput($input)
	{
		$prepared = explode("=", $input);
		$input = $prepared[0];
		//    $input = preg_replace('/[^a-zA-Z0-9\s]/', '', $input);
		$input = preg_replace("/[^\w\x7F-\xFF\s]/", " ", $input);
		$input = str_replace(" ","+",$input);
		$input = str_replace("++","+",$input);

		return $input;
	}

	function getXML($searchterms,$start, $lang)
	{
		//the unique identifier for this Google Custom Search
		$cseNumber = '003921928615769859463:uzre_v1twbg'; // CHANGE ME!!!!!!  this won't work otherwise
		$xmlfile = 'http://www.google.com/search?cx='.$cseNumber.'&client=google-csbe&start='.$start.'&num=10&ie=utf8&oe=utf8&output=xml_no_dtd&&hl='.$lang.'&q='.$searchterms;

		$xml = simplexml_load_string(file_get_contents($xmlfile));

		return $xml;
	}

	//get estimated total number of results.  this is awfully inaccurate until you're on the final page
	//so it's only useful for working out whether there is a next page or not, rather than "results 1-10 of 417
	function getTotalResults($xml)
	{
		$total = $xml->RES->M;
		return $total;
	}

	//total number of results for this page
	function getPageTotal($xml)
	{
		return count($xml->RES->R);
	}



	//this function needs customising if you want to modify how the search results are displayed
	function writeSearchResults($xml)
	{
		$output = '';
		$i = 0;
		if ($xml->RES->R)
		{
			foreach ($xml->RES->R as $key)
			{
				$output[$i]['url'] = (string)$key->U;
				$output[$i]['title'] = (string) $key->T;
				$output[$i]['text'] = (string) strip_tags($key->S, "<b>");
				$i++;
			}
		}
		return $output;
	}

	//"did you mean..."
	function writeSuggestion($xml)
	{
		$searchterm = $xml->Q;
		$suggestion = $xml->Spelling->Suggestion;

		if($suggestion != "")
			echo '<p>You searched for <strong>'.$searchterm.'</strong>, did you mean <strong><a href="?q='.strip_tags($suggestion).'">'.$suggestion.'</a></strong>?</p>';
	}
}