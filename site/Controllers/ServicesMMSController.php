<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
 
use Site\Models\MMS;


class ServicesMMSController extends Controller {
 
  
    protected $roamingList = [];

    public function __construct()
	{
	    
       
        $this->roamingList = MMS::Name()->get();
	}
	
 	
	public function index() {
         
        $price =(int) \Request::get('price'); 

        if($price){ 
            $roamingList = $this->getDistinctLabel($this->filterPrice($price));
        } 
        else {
            $roamingList = $this->getDistinctLabel($this->roamingList);
        }
              
         //dd($roamingList);
		 return view('site.services.mms.list',compact('roamingList'));	
	} 
    
    /* search by country name and get list of roaming operators*/
    public function show($cName){

        $roaming = array_filter($this->roamingList->return->roamingPartners, function($el) use ($cName) {
            
            return ( strpos($el->countryName, ucfirst(strtolower($cName))) !== false );

        });
         
        return view('site.services.roaming.view',compact('roaming','cName'));
    }
    
    /* search by price*/
    private function filterPrice($price){
         
        $roaming = array_filter($this->roamingList->toArray(), function($el) use ($price) {
            
            return (  $el['price'] == $price );

        });
         
        return $roaming;
    }

     

	 
    /* Get distinc letters + countries by alphabetical order */
    private function getDistinctLabel($data) {
        $symbols = [];
            
            foreach($data as $dt){
                // dd($dt['country']);
                $currSymbol = strtoupper(mb_substr($dt['country'], 0, 1));

                if(isset($symbols[$currSymbol]) && in_array($dt['country'],$symbols[$currSymbol]) ) {

                  $symbols[$currSymbol][$dt['country']][] = $dt;  
                  continue; 
                }
                
                $symbols[$currSymbol][$dt['country']][] = $dt;  
                    
                
            }
 
        ksort($symbols); 
         

        return $symbols;
    }

}
