<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Gallery;

class MagazinesController extends Controller {

	protected $model;
    
    public function __construct()
	{
		$this->model = new Gallery;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	public function index($page)
	{
		$year = \Request::get('year');
		$month = \Request::get('month');
		 
		$magazines = Gallery::WithContent()->whereCollectionId($page->attached_collection_id)
							->year($year)->month($month)->OrderByCreationDate('DESC')->paginate(12);
	 
        $startYear = $this->model->getFirstYear();

		return view('site.gallery.magazine',compact('magazines','startYear'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/*public function show($slug)
	{
		$article =  Gallery::whereSlug($slug)->withContent()->first();
      
		if(!$article){ abort(404); }

		return view('site.gallery.view',compact('article'));  
	}
*/
	 

}
