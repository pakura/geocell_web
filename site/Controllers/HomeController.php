<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use  Site\Models\Gallery;
use  Site\Models\Products;
use  Site\Models\Articles;

class HomeController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		 
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$gallery = Gallery::getItem(54);
		$products = Products::getForHomePage();
		$options = [];

		foreach ($products as $product) {
			$options[$product->product_id] =  \Site\Models\ProductOptions::getOptions($product->product_id);
		}

		$productItemsCount = Products::whereCollectionId(5)->count();
		$articles = Articles::whereCollectionId(58)->WithContent()->OrderById('DESC')->take(4)->get();
		$articlesItemsCount = Articles::where('articles.collection_id','=',58)->count();
        $homepage = true;
		return view('site.home',compact('gallery','products','articles','productItemsCount','articlesItemsCount','options','homepage'));
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function locale($lang)
	{

		return $this->index();
	}
}
