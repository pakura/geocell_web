<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Banners;
use Mail;
use App\Http\Requests\ContactRequest;

class ContactController extends Controller {

    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	public function index($page)
	{
		$subPages = \Site\Models\Pages::whereParentId($page->parent_id)->withContent()->get();
		
		$banners = Banners::whereCollectionId($page->attached_collection_id)->withContent()->get();
		

		return view('site.getintouch.contact',compact('banners','subPages'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		/*$faq =  Faq::whereSlug($slug)->withContent()->first();
		if(!$faq){ abort(404); }
		return view('site.faq.view',compact('faq'));  */
	}


	/*Send mail  */
	
	public function send(ContactRequest $request){
		$data = ['email'=>$request->email,'comment'=>strip_tags($request->comment),'phone'=>$request->department];
		
		$mail = Mail::send('emails.contact', $data, function ($message) {

		   $message->to('contact@geocell.ge')->from('onlineportal@geocell.ge')->subject('Email from geocell.ge!');
	
		});	



		if($mail){
			return \Response::json(['msg'=>'თქვენი შეტყობინება გაიგზავნა']);
		}
		else{
			return \Response::json(['error'=>'დაფისქირდა შეცდომა. გთხოვთ ცადოთ მოგვიანებით.']);
		}
		
	}

    
}
