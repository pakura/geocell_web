<?php namespace Site\Controllers;


use App\Http\Controllers\Controller;

use Site\Models\Pages;

class preOrderDone extends Controller {

    public function __construct()
    {

    }
    /*optimize later */


    public function index(){

        $name = \Input::get('name');
        $lastname = \Input::get('lastname');
        $email = \Input::get('email');
        $phone = \Input::get('phone');
        $pid = \Input::get('pid');
        $ordertype = \Input::get('order');
        $storage = \Input::get('storage');
        $color = \Input::get('iphone-color');
        $month = \Input::get('month');
        $bonus = \Input::get('bonus');
        $city = \Input::get('city');
        $office = \Input::get('office');
        $device = \Input::get('device');
        $colorcode = \Input::get('color-code');
        $lang = \App::getLocale();
        $responseCaptcha = \Input::get('g-recaptcha-response');
        $ip = $_SERVER['REMOTE_ADDR'];
        if($name == '' || $lastname == '' || $email == '' || $pid == '' || $phone == ''){
            return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/online-shop/phones/pre-order?f=no');
        }
        $preorder = array(
            'name' => $name,
            'email' => $email,
            'lastname' => $lastname,
            'phone' => $phone,
            'pid' => $pid,
            'ordertype' => $ordertype,
            'storage' => $storage,
            'color' => $color,
            'month' => $month,
            'bonus' => $bonus,
            'city' => $city,
            'office' => $office,
            'device' => $device,
        );

        \Session::put('preorder', $preorder);
        $res1 = \DB::table('stock')->where('product_id', $device)->where('storage', $storage)->get();
        $total = 0;
        foreach ($res1 as $key => $val){
            $total += $val->quantities;
        }
        if($total < 1){
            return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/online-shop/phones/pre-order?f=storage&name=
                '.$preorder['name'].'&lastname='.$preorder['lastname'].'&phone='.$preorder['phone'].'&pid='.$preorder['pid'].'&email='.$preorder['email'].
                '&option='.$preorder['ordertype'].'&storage='.$preorder['storage'].'&iphone-color='.$preorder['color'].'&month='.$preorder['month'].'&bonus='.$preorder['bonus'].'&order=
                '.$preorder['ordertype'].'&color-code='.$colorcode.'&device='.$device);
        }
        if($colorcode != ''){

            $res2 = \DB::table('stock')->where('product_id', $device)->where('storage', $storage)->where('color', $colorcode)->first();

            if($res2->quantities < 1){
                return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/online-shop/phones/pre-order?f=color&name=
                '.$preorder['name'].'&lastname='.$preorder['lastname'].'&phone='.$preorder['phone'].'&pid='.$preorder['pid'].'&email='.$preorder['email'].
                    '&option='.$preorder['ordertype'].'&storage='.$preorder['storage'].'&iphone-color='.$preorder['color'].'&month='.$preorder['month'].'&bonus='.$preorder['bonus'].'&order=
                '.$preorder['ordertype'].'&color-code='.$colorcode.'&device='.$device);
            }
        }
        $res = \DB::table('pre-order')->where('email', '=', $email)->first();
        if(isset($res)){
            return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/online-shop/phones/pre-order?f=email&name=
            '.$preorder['name'].'&lastname='.$preorder['lastname'].'&phone='.$preorder['phone'].'&pid='.$preorder['pid'].
                '&option='.$preorder['ordertype'].'&storage='.$preorder['storage'].'&iphone-color='.$preorder['color'].'&month='.$preorder['month'].'&bonus='.$preorder['bonus'].'&order=
                '.$preorder['ordertype'].'&color-code='.$colorcode.'&device='.$device);
        }
        $res = \DB::table('pre-order')->where('pid', '=', $pid)->first();
        if(isset($res)){
            return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/online-shop/phones/pre-order?f=pid&name=
                '.$preorder['name'].'&lastname='.$preorder['lastname'].'&phone='.$preorder['phone'].'&email='.$preorder['email'].
                '&option='.$preorder['ordertype'].'&storage='.$preorder['storage'].'&iphone-color='.$preorder['color'].'&month='.$preorder['month'].'&bonus='.$preorder['bonus'].'&order=
                '.$preorder['ordertype'].'&color-code='.$colorcode.'&device='.$device);
        }

        $res = \DB::table('pre-order')->where('phone', '=', $phone)->first();
        if(isset($res)){
            return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/online-shop/phones/pre-order?f=phone&name=
                '.$preorder['name'].'&lastname='.$preorder['lastname'].'&phone='.$preorder['phone'].'&email='.$preorder['email'].
                '&option='.$preorder['ordertype'].'&storage='.$preorder['storage'].'&iphone-color='.$preorder['color'].'&month='.$preorder['month'].'&bonus='.$preorder['bonus'].'&order=
                '.$preorder['ordertype'].'&color-code='.$colorcode.'&device='.$device);
        }



        $url = "https://www.google.com/recaptcha/api/siteverify?secret=6Lc8JhkTAAAAAAp_7Qvez1e6LSsIaRA-qC4yt_MI&response=$responseCaptcha&remoteip=$ip";

        $chaptcha = file_get_contents($url);
        $chaptcha = json_decode($chaptcha);
       if($chaptcha->success == false){
           return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/online-shop/phones/pre-order?f=captcha&name=
                '.$preorder['name'].'&lastname='.$preorder['lastname'].'&phone='.$preorder['phone'].'&email='.$preorder['email'].
               '&option='.$preorder['ordertype'].'&storage='.$preorder['storage'].'&iphone-color='.$preorder['color'].'&month='.$preorder['month'].'&bonus='.$preorder['bonus'].'&order=
                '.$preorder['ordertype'].'&color-code='.$colorcode.'&device='.$device);
       }
        $res = \DB::table('pre-order')->insert(
            $preorder
        );

        $quantities = $res2->quantities-1;
        $res = \DB::table('stock')->where('id', $res2->id)->update(['quantities' => $quantities]);
        \Session::forget('preorder');
        $lang = \App::getLocale();
        if($lang == 'en'){
            $se['title'] = 'Pre-order official iPhone SE and purchase before others';
            $se['description'] = 'Pre-ordered iPhone SE will be available at Geocell shop, #34 Chavchavadze ave.Estimated date: April 25';
        } else {

        }

        $se['img'] = 'https://geocell.ge/developer_version/public/site/tsr-components/tsr-custom/images/tsr-preorder-apple-iphone-se-02.jpg';
        $se['full_slug'] = 'https://geocell.ge/developer_version/public/private/online-shop/phones/apple-iphone-se';
        return view('site.products.preorderdone', compact('se'));
    }
}
