<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Products;

class ProductsController extends Controller {

    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	public function index($page)
	{
		$products = Products::getLatest($page->attached_collection_id);
//       	 dd($_SERVER['REMOTE_ADDR']);
//		10.0.6.25
        $Cur_brands = Products::collection($page->attached_collection_id)->where('products.visibility','=',1)->groupBy('brand_id')->get();
        /* length(col),col*/
        foreach ($products as $product) {
            $options[$product->product_id] =  \Site\Models\ProductOptions::orderBy(\DB::raw('CAST(products_options.value  AS DECIMAL(8,2))') )->getOptions($product->product_id);
        }
        $brandid = array();

        foreach ($Cur_brands as $k => $v){
            $brandid[] = $v->brand_id;
        }
        $brands = \DB::table('brands')->where('visibility',1)->whereIn('brands.id', $brandid)->get();
		$options = [];
		/* length(col),col*/


        $arr = [];
        if(isset($_GET['price']) && ($_GET['price'] == 'asc' || $_GET['price'] == 'desc')) { $arr['price'] = $_GET['price']; }
        if(isset($_GET['brand'])) { $arr['brand'] = explode(',', $_GET['brand']); }
//        dd($arr);
		return view('site.products.list',compact('products','options','brands','arr'));
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		$product =  Products::where('products.slug','=',$slug)->visible()->WithPriceAndAttachment()->WithBrand()->first();
//dd($product);

		if(!$product){ /*abort(404); */ return redirect('private/online-shop/phones');}

//		$accessories = Products::collection(61)->WithPriceAndAttachment()->withPageSlug()->orderBy('products.pinned','desc')->take(4)->get();
		$accCount = Products::collection(61)->count();
		$options = \Site\Models\ProductOptions::orderBy('pol.position','desc')->getOptions($product->product_id);
		$photos = Products::getAttachedFiles($product->product_id);
		$prices = Products::getAllPrices($product->product_id);

		$productPage = true;
        $preorder = false;
        if($product->id == 290){
            $preorder = true;
        }
		return view('site.products.view',compact('product','accessories','accCount','photos','options','option4count','prices', 'colors','productPage', 'preorder'));
	}

	 public function changeColor($lang, $id, $color){
//		 echo $id.' '.$color;
		 $img = \DB::table('products_attachments')->where('product_id', '=', $id)->where('color', '=', $color)->select('file')->get();
		 return @$img[0]->file;
	 }

}
