<?php namespace Site\Controllers;


use App\Http\Controllers\Controller;

use Site\Models\Pages;

class PagesController extends Controller {

    
    public function __construct()
	{
		
	}
    /*optimize later */
	public function getPage($lang,$slug, $params = null)
    {
        //$params = explode('/', $params);
        //dd($slug.$params);
        // dd(\Session::get('site_user'));

        $fullSlug=$params?$slug.'/'.$params:$slug;

        $page = PAGES::whereFullSlug($fullSlug)->Attachment()->first();


        /*Load plane page*/
        if($page) {

            if($page->redirect_link) {

                $url = parse_url($page->redirect_link);
                if(isset($url["scheme"]) && ($url["scheme"]=="http" || $url["scheme"]=="https")) {

                   return redirect($page->redirect_link);
                }
                return redirect($lang.'/'.$page->redirect_link);

            }

            \View::share('item', $page);
            if($page->controller){
//                dd($page->controller);
        	   return app('Site\Controllers\\'.$page->controller)->index($page);
            }
            else {

                if($page->view && method_exists($this, $page->view)) {
                    return call_user_func_array(array($this,$page->view),[$page]);
                }

                return view('site.textcontent');
            }

        }


        else {
           /*Complex page*/

            $params = explode('/', $params);
            $itemSlug = array_pop($params);
            $newParams = implode($params, '/');

            $newSlug = $newParams?$slug.'/'.$newParams:$slug;

            $page = PAGES::whereFullSlug($newSlug)->withContent()->first();
            if($page && $page->controller){

                \View::share('item', $page);
                 $s = 'Site\Controllers\\'.$page->controller;
                 $c = new $s;

                if($page->view && method_exists($c, $page->view)) {

                    return call_user_func_array(array($c,$page->view),[$itemSlug]);

                }
                else {
                    return app('Site\Controllers\\'.$page->controller)->show($itemSlug);
                }
            }
            else{
        	   abort(404);
            }
        }

    }



    private function about_us($page) {
        $aboutPages = PAGES::whereParentId($page->id)->withContent()->orderby('position')->get();
        return view('site.about.about_us',compact('aboutPages'));
    }

    private function default_show_siblings($page) {
        $subPages = PAGES::whereParentId($page->parent_id)->withContent()->orderby('position')->get();
        return view('site.textcontent_with_siblings',compact('subPages'));
    }

}
