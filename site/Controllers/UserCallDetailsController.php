<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Libs\fpdf;
use Site\Models\GeocellId;
use Site\Models\Services;


class UserCallDetailsController extends Controller {

    protected $model;
    protected $geocellId;
    protected $userInfo = [];
    protected $actServices = [];
    protected $phone;
    protected $pdf;
    protected $directdebit;

    public function __construct(BillingFacadeServices $model,GeocellId $geocellIdModel)
	{

        $this->geocellId=$geocellIdModel;
        $this->model=$model;
        $this->phone= \Session::get('phone');
        $this->pdf = new fpdf();
//
	}


	public function index() {
        $userinfo = $this->getUserInfo();
        $lang = \App::getLocale();
         if(!$this->geocellId->isAuthed()) { return view('site.user.auth-sign-in'); }
//         dd(\Session::has('fullauth'));
       if(\Session::has('fullauth') == false){
           \Session::put('accsesDetails', true);
           return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/seccode');
       }
//        \Session::forget('fullauth');
        \Session::forget('accsesDetails');
         $query = (object) $this->checkData();
//        dd($query->status);
        $phone =  \Session::get('phone');
        $profile = \DB::table('call_details_profile')->where('phone', $phone)->first();
        $day = date('d')-1;
        $mindate = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-2 month" ) );
        $mindate = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( $mindate ) ) . "-".$day." day" ) );
        $mindateInvoice = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-3 month" ) );
//dd($profile->changes_date);
        if(isset($profile->changes_date)){
            if(strtotime($profile->changes_date) > strtotime($mindate) ){
                $mindate = date('Y-m-d', strtotime($profile->changes_date));
            }
            if(strtotime($profile->changes_date) > strtotime($mindateInvoice)){
                $mindateInvoice = date('Y-m-d', strtotime($profile->changes_date));
            }
        }
        $lastmonth = date("Y-m-01");
        $invoiceDate = array();

        $invoiceArchove = \DB::table('invoice_archive')->where('phone', $phone)->first();


        while(strtotime($mindateInvoice)<strtotime($lastmonth)){
            $invoiceDate[] = $mindateInvoice;
            $mindateInvoice = date("Y-m-d", strtotime("first day of +1 months", strtotime($mindateInvoice)));
        }
        if (isset($this->directdebit->return->result->account)){
            $this->directdebit = true;
        } else {
            $this->directdebit = false;
        }
		return view('site.user.calldetails',compact('callDetails'),compact('query'))
            ->with('userinfo', $userinfo)->with('mindate', $mindate)->with('invoiceDate', $invoiceDate)
            ->with('invoiceArchove', $invoiceArchove)->with('directdebit', $this->directdebit);
	}

    public function getDetails($lang, $gsm=null){
        $phone =  \Session::get('phone');
        $from = \Input::get('from_date');
        $to =  \Input::get('to_date');
        if(!$from) { $from = date('01/m/Y 00:00:00'); }
        if(!$to) { $to = date('d/m/Y 00:00:00'); }
//        $callDetails = $this->model->__call('portalCallDetailByGSM',[$phone,date('d/m/Y 00:00:00', strtotime($from)),date('d/m/Y 23:59:59', strtotime($to))]);
//        dd($callDetails);
        $price = (strtotime($to)-strtotime($from))/86400+1;
        $price *= 0.5; // total cost;
        if($this->checkbalance($price) == false){
            $arr = ['result'=>true, 'status'=>$price, 'timeleft'=>0, 'percent' => 0, 'fromdate' => $from, 'todate' => $to, 'error' => -101];
            die(json_encode($arr));
        }
        $res = \DB::table('call_details')->where('phone', $phone)->delete();
        $res = \DB::table('init_call_details')->insert(['phone'=>$phone, 'fromdate'=>$from, 'todate'=>$to, 'current_date'=>$from,'status'=>1]);
        $query = $this->checkData();
        die(json_encode($query));

    }


    public function checkUpdate(){
        die(json_encode($this->checkData()));
    }


    private function checkData(){
        $phone =  \Session::get('phone');
        $data = \DB::table('init_call_details')->where('phone', $phone)->orderBy('id', 'desc')->first();
        if (!isset($data)){
            $arr = ['result'=>false, 'status'=>0, 'timeleft'=>0, 'percent' => 0];
            return $arr;
        }

        if($data->status == 1 || $data->status == 2){
            $queryData = \DB::table('init_call_details')->where('id', '<=', $data->id)->whereIn('status',[1,2])->orderBy('id', 'asc')->get();
            $timeleft = 0;
            $leftDay = 0;
            $tday = 0;
            $lday = 0;
            foreach ($queryData as $key => $qd){
                $qtime = strtotime($qd->todate) - strtotime($qd->current_date);
                $qtime = intval(round($qtime/86400));
                $timeleft+=$qtime;

            }
            $totalDay = strtotime($data->todate) - strtotime($data->fromdate);
            $totalDay = intval(round($totalDay/86400));
            if($totalDay == 0) $totalDay = 1;
            $leftDay = strtotime($data->current_date) - strtotime($data->fromdate);
            $leftDay = intval(round($leftDay/86400));
            $percent = intval(round($leftDay/$totalDay*100));

            $arr = ['result'=>true, 'status'=>$data->status, 'timeleft'=>$timeleft, 'percent' => $percent, 'fromdate' => $data->fromdate, 'todate' => $data->todate, 'error' => $data->error];
            return $arr;
        } else {
            $arr = ['result'=>true, 'status'=>$data->status, 'timeleft'=>0, 'percent' => 100, 'fromdate' => $data->fromdate, 'todate' => $data->todate, 'error' => $data->error];
            return $arr;
        }
    }

    public function getInvoices($lang){

        \Session::forget('invoice');
        $phone =  \Session::get('phone');
        $month = \Input::get('month');
        $checkmonth = \Input::get('month');
        $invoice = \DB::table('invoice_archive')->where('phone', $phone)->first();
        if(!isset($month)){
            $month = $invoice->month;
            $downmonth = $invoice->month;
        }

        $downmonth = date('Y').'-'.$month.'-01 00:00:00';
        $month = '01/'.$month.'/'.date('Y').' 00:00:00';
        $userInfo = $this->model->__call('portalGetGSMInfo',[$phone]);
        if(isset($userInfo->return->account)){
            $invoices1 = $this->model->__call('portalInvoceInformation',[$phone,$userInfo->return->account,$month]);
            $invoices2 = $this->model->__call('portalInvoceInformationByGSM',[$phone,$userInfo->return->account,$month]);
            if(isset($invoices2->return->list)){
                $invoices1->return->list->listinfo = $invoices2->return->list;
            }
            if(isset($invoices1->return->list)){
                \Session::put('userInfo',$invoices1->return->list);

                if(isset($invoice->month)){
                    $res = \DB::table('invoice_archive')->where('phone', $phone)->update(['month' => date('m', strtotime($downmonth))]);
                } else {
                    $res = \DB::table('invoice_archive')->insert(['phone' => $phone, 'month' => date('m', strtotime($downmonth))]);
                }
                if(!isset($checkmonth)){
                    $pdf = \App::make('dompdf.wrapper');
                    $pdf->loadView('pdf.invoice', ['invoice' => $invoices1->return->list, 'phone' => $phone]);
                    return $pdf->stream();
//                    return view('pdf.invoice', ['invoice' => $invoices1->return->list, 'phone' => $phone]);
                }
                return 1;
            }
            return 2;
        }

        return 0;

    }



    public function cronInit($lang){
        ini_set('max_execution_time', 600);
        $phone =  \Session::get('phone');
        $targetReq = \DB::table('init_call_details')->whereIn('status', [1,2])->orderBy('id', 'asc')->first();
        $profile = \DB::table('call_details_profile')->where('phone', $phone)->first();
//$profile->changes_date
        if($targetReq->status == 1){
            $targetReq->current_date = date('Y-m-d' ,strtotime($targetReq->current_date.'-1 day'));
        }
        if(strtotime($targetReq->current_date.'+1 day') > strtotime($targetReq->todate)){
            if($targetReq->status == 3 || $targetReq->status == 4){
                die('nothing to do');
            }
            $res = \DB::table('init_call_details')->where('id', $targetReq->id)->update(['status' => 3]);
            error_log('init_call_details >> WHERE id = '.$targetReq->id.' complated', 0);
            die('init_call_details >> WHERE id = '.$targetReq->id.' complated');
        }
        if(isset($profile->changes_date) && strtotime($targetReq->current_date.' 00:00:00') < strtotime($profile->changes_date)){
            $targetReq->current_date = $profile->changes_date;
        }

        $reqDateFROM = date('d/m/Y H:m:00' ,strtotime($targetReq->current_date));
        $reqDate2TO = date('d/m/Y 23:59:59' ,strtotime($targetReq->current_date));
        $callDetails = $this->model->__call('portalCallDetailByGSM',[$targetReq->phone,$reqDateFROM,$reqDate2TO]);
        print_r($callDetails);
        echo '<br>';
        if(isset($callDetails->return->list)){
            if(!is_array($callDetails->return->list)){
                $t = $callDetails->return->list;
                $callDetails->return->list = array($t);
            }
        } else {
            $lastdate =  date('Y-m-d' ,strtotime($targetReq->current_date.'+1 day'));
            if($lastdate == $targetReq->todate){
                $res = \DB::table('init_call_details')->where('id', $targetReq->id)->update(['current_date' => $lastdate, 'status' => 3, 'error' => $callDetails->return->resultCode]);
            } else {
                $res = \DB::table('init_call_details')->where('id', $targetReq->id)->update(['current_date' => $lastdate, 'status' => 2, 'error' => $callDetails->return->resultCode]);
            }
            error_log('Call detail: list is undefended >> WHERE phone = '.$targetReq->phone.' & Status = '.$callDetails->return->resultCode, 0);
            die('Call detail: list is undefended >> WHERE phone = '.$targetReq->phone.' & Status = '.$callDetails->return->resultCode);
        }
        foreach($callDetails->return->list as $key => $detail){
            $call_date = '';
            if(isset($detail->strt) && strpos($detail->strt, '/') != false){
                $call_date = explode(' ', $detail->strt);
                $dddate = explode('/', $call_date[0]);
                $dddate = $dddate[2].'-'.$dddate[1].'-'.$dddate[0];
                $call_date = $dddate.' '.$call_date[1].' '.$call_date[2];
            } else {
                $call_date = $detail->strt;
            }

            $insert = array(
                'init_id' => $targetReq->id,
                'phone' => $targetReq->phone,
                'call_date' => isset($call_date)?$call_date:'',
                'price' => isset($detail->charge)?$detail->charge:0,
                'called_phone' => isset($detail->a_number)?$detail->a_number:'',
                'type' => isset($detail->call_type)?$detail->call_type:'',
                'b_number' => isset($detail->b_number)?$detail->b_number:'',
                'unitValue' => isset($detail->unitValue)?$detail->unitValue:'',
                'zone_name' => isset($detail->zone_name)?$detail->zone_name:'',
                'description' => isset($detail->description)?$detail->description:'',
                'tarrifInformation' => isset($detail->tarrifInformation)?$detail->tarrifInformation:''

            );
            $res = \DB::table('call_details')->insert($insert);
        }
        $lastdate =  date('Y-m-d' ,strtotime($targetReq->current_date.'+1 day'));
        if($lastdate == $targetReq->todate){
            $res = \DB::table('init_call_details')->where('id', $targetReq->id)->update(['current_date' => $lastdate, 'status' => 3, 'error' => 0]);
        } else {
            $res = \DB::table('init_call_details')->where('id', $targetReq->id)->update(['current_date' => $lastdate, 'status' => 2, 'error' => 0]);
        }
        error_log('Call detail: collect is Done >> WHERE phone = '.$targetReq->phone.' & and date = '.$reqDateFROM, 0);
        die('Call detail: collect is Done >> WHERE phone = '.$targetReq->phone.' & and date = '.$reqDateFROM);
    }

    public function getUserInfo(){

        // dd(\Session::get('phone'));
        $resUserInfo = $this->model->_getUserInfo($this->phone);
//         dd($resUserInfo);
        if(isset($resUserInfo->return->resultCode) && $resUserInfo->return->resultCode==1){
            \Session::put('userInfo',$resUserInfo);
        }

        $userInfo = \Session::has('userInfo')?\Session::get('userInfo'):$this->model->_getUserInfo($this->phone);

        return $userInfo;
    }


    public function fillBalance($lang, $bal=0.5){
        $userInfo = $this->getUserInfo();
        $reqBalanc = intval(round($bal - $userInfo->return->balance));
        $service = (object) array(
            'value' => 1,
            'title' => ''
        );
        return view('site.lightbox.service-fill-balance', compact('service', 'userInfo'));
    }

    private function checkbalance($price){
        $userInfo = $this->getUserInfo();
        if($userInfo->return->balance < $price){
            return false;
        } else {
            return true;
        }
    }

    public function download($lang){
        $phone =  \Session::get('phone');
        $invoice = \Session::get('userInfo');
//        dd($invoice);
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('pdf.invoice', ['invoice' => $invoice, 'phone' => $phone]);
        return $pdf->stream();
    }




    private function unitCorrect($lang, $unit){
        if(strpos($unit, 'INTERNET_BYTE') > -1){

            $unit = floatval($unit);
            $unit = $unit / 1024;
//            return intval($unit);
            if($unit < 1024){
                if($lang == 'en')
                return intval($unit).' kb';
            } else {
                $unit = $unit / 1024;
                return intval($unit).' mb';
            }
        } elseif (strpos($unit, 'TIME_SECONDS') > -1){
            $unit = floatval($unit);
            if($unit < 60){
                return intval($unit).' second';
            } else {
                $min = intval($unit / 60);
                $sec = intval($unit % 60);
                return $min.' minutes and '.$sec.' seconds';
            }
        } elseif(strpos($unit, 'SMS') > -1) {
            return intval($unit).' SMS';
        } else {
            return '';
        }
    }


    public function removedetails(){
//        dd('ok');
        $phone = \Session::get('phone');
        $res = \DB::table('init_call_details')->where('phone', $phone)->delete();
        $res = \DB::table('call_details')->where('phone', $phone)->delete();
//        dd('ok');
        return redirect('private/private-cabinet/detailed');
    }

    public function removeinvoice(){
//        dd('ok');
        $phone = \Session::get('phone');
        $res = \DB::table('invoice_archive')->where('phone', $phone)->delete();
//        dd('ok');
        return redirect('private/private-cabinet/detailed');
    }
}

