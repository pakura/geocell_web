<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Pages;

class StandardPagesController extends Controller {

    
    public function __construct()
	{
		
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$item = Pages::where('pages.id','=',$id)->where('c.language',config('app.locale'))
			->join('pages_content as c','pages.id','=','c.page_id')
			->first();
        
		return view('site.textcontent',compact('item'));
		
	}

	 

}
