<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Libs\Curl;
use Site\Models\Services;
use Site\Models\GeocellId;
use Illuminate\Http\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class DirectDebitController extends Controller {

    private $ddInfo;
    protected $model;
    protected $facade;
    protected $phone;
    protected $directdebit;


    public function __construct(GeocellId $model, BillingFacadeServices $facade){
        $this->model = $model;
        $this->facade = $facade;

        $this->phone = \Session::get('phone');
        if($this->phone != null){
            $this->ddInfo = $this->facade->__call('getAccountsByGSM', [$this->phone]);
            if (isset($this->ddInfo->return->result->account->maskedPan)){
                $makspan = str_split($this->ddInfo->return->result->account->maskedPan, 4);
                $makspan = implode('-', $makspan);
                $this->ddInfo->return->result->account->maskedPan = strtoupper($makspan);
            } else {
                if (isset($this->ddInfo->return->result->account)){
                    $this->ddInfo->return->result->account->maskedPan = null;
                }
            }
            $this->ddInfo = isset($this->ddInfo->return->result)?$this->ddInfo->return->result:$this->ddInfo;
//            dd($this->ddInfo);
        }
        $this->directdebit = $this->facade->__call('getAccountsByGSM', [$this->phone]);
    }

    public function index(){
        if(!$this->model->isAuthed() || $this->phone == null) { return view('site.user.auth-sign-in'); }
        if(isset($this->ddInfo->subscribers)){
            if (!is_array($this->ddInfo->subscribers)){
                $subsc = $this->ddInfo->subscribers;
                $this->ddInfo->subscribers = array();
                $this->ddInfo->subscribers[] = $subsc;
            }
            foreach($this->ddInfo->subscribers as $key => $val){
                if($val->gsm == $this->phone){
                    $val->mygsm = true;
                } else {
                    $val->mygsm = false;
                }
            }
        }
        if (isset($this->directdebit->return->result->account)){
            $this->directdebit = true;
        } else {
            $this->directdebit = false;
        }
//        dd($this->ddInfo);
        return view('site.user.directdebit')->with('ddInfo', $this->ddInfo)->with('directdebit', $this->directdebit);
    }



    public function initActivation(){
        return view('site.lightbox.debit-activation-msg');
    }

    public function initMetiOther(){
        return view('site.lightbox.debit_active_meti');
    }

    public function initFixedOther(){
        return view('site.lightbox.debit_active_fixed');
    }

    public function initLimitOther(){
        return view('site.lightbox.debit_active_limit');
    }

    public function initMeti(){
        $phone = \Session::get('phone');
        return view('site.lightbox.debit_active_meti', compact('phone'));
    }

    public function initFixed(){
        $phone = \Session::get('phone');
        return view('site.lightbox.debit_active_fixed', compact('phone'));
    }

    public function initLimit(){
        $phone = \Session::get('phone');
        return view('site.lightbox.debit_active_limit', compact('phone'));
    }



    public function checkGSM(){
        $gsm = '995'.\Input::get('gsm');
        $res  = $this->facade->__call('portalGetSubscriberData',[$gsm]);
        if($res->return->resultCode == 1){
            echo 'ok';
         } else {
            echo $res->return->resultCode;
        }
    }

    public function initMetiEdit($lang, $gsm){
        $phone = \Session::get('phone');
        $data = null;
        $res = $this->facade->__call('getAccountsByGSM', [$phone]);
        if(isset($res->return->result->subscribers)){
            if(is_array($res->return->result->subscribers)){
                foreach ($res->return->result->subscribers as $serv){
                    if($serv->debitType == 2 && $serv->gsm == $gsm){
                        $data = $serv;
                    }
                }
            } else {
                if($res->return->result->subscribers->debitType == 2 && $res->return->result->subscribers->gsm == $gsm){
                    $data = $res->return->result->subscribers;
                }
            }
            return view('site.lightbox.debit_active_meti', compact('data'));
        } else {
            return view('site.lightbox.debit_active_meti', compact('phone'));
        }

    }

    public function initFixedEdit($lang, $gsm){
        $phone = \Session::get('phone');
        $data = null;
        $res = $this->facade->__call('getAccountsByGSM', [$phone]);
        if(isset($res->return->result->subscribers)) {
            if (is_array($res->return->result->subscribers)) {
                foreach ($res->return->result->subscribers as $serv) {
                    if ($serv->debitType == 1 && $serv->gsm == $gsm) {
                        $data = $serv;
                    }
                }
            } else {
                if ($res->return->result->subscribers->debitType == 1 && $res->return->result->subscribers->gsm == $gsm) {
                    $data = $res->return->result->subscribers;
                }
            }
            return view('site.lightbox.debit_active_fixed', compact('data'));
        } else {
            return view('site.lightbox.debit_active_fixed', compact('phone'));
        }
    }

    public function initLimitEdit($lang, $gsm){
        $phone = \Session::get('phone');
        $data = null;
        $res = $this->facade->__call('getAccountsByGSM', [$phone]);
        if(isset($res->return->result->subscribers)) {
            if (is_array($res->return->result->subscribers)) {
                foreach ($res->return->result->subscribers as $serv) {
                    if ($serv->debitType == 0 && $serv->gsm == $gsm) {
                        $data = $serv;
                    }
                }
            } else {
                if ($res->return->result->subscribers->debitType == 0 && $res->return->result->subscribers->gsm == $gsm) {
                    $data = $res->return->result->subscribers;
                }
            }
            return view('site.lightbox.debit_active_limit', compact('data'));
        } else {
            return view('site.lightbox.debit_active_limit', compact('phone'));
        }

    }

    public function initReq(){
        $pack = \Request::get('metibundle');
        $amount = \Request::get('amount');
        $day = \Request::get('day');
        $low = \Request::get('low');
        $top = \Request::get('top');
        $subId = \Request::get('subid');
        $ajax = \Request::get('ajax');
        $lang = \App::getLocale();
        $data = (object) array(
            'gsm' => '995'.\Request::get('phone'),
            'servname' => \Request::get('servname'),
            'debitType' => \Request::get('debittype'),
            'status' => \Request::get('status'),
            'bonusId' => isset($pack)?$pack:null,
            'fixedAmount' => isset($amount)?$amount:null,
            'dayOfMonth' => isset($day)?$day:null,
            'minimalBalance' => isset($low)?$low:null,
            'targetBalance' => isset($top)?$top:null,
            'subscriberId' => isset($subId)?$subId:null
        );

        $changes = false;
        $this->ddInfo = $this->facade->__call('getAccountsByGSM', [$this->phone]);
        if (isset($this->ddInfo->return->result->subscribers)){
            $newData = (object) array(
                'accountId' => $this->ddInfo->return->result->account->accountId,
                'date' =>  date('d/m/Y H:i:s'),
                'debitType' => $data->debitType,
                'dsc' => $data->servname,
                'gsm' => $data->gsm,
                'status' => intval($data->status),
                'bonusId' => $data->bonusId,
                'fixedAmount' => $data->fixedAmount,
                'dayOfMonth' => $data->dayOfMonth,
                'minimalBalance' => intval($data->minimalBalance),
                'targetBalance' => intval($data->targetBalance),
                'retry' => 0,
                'subscriberId' => $data->subscriberId
            );
            if(is_array($this->ddInfo->return->result->subscribers)){
                foreach ($this->ddInfo->return->result->subscribers as $sub){
                    if($sub->gsm == $data->gsm && $sub->debitType == $data->debitType){
                        $sub->bonusId = $newData->bonusId;
                        $sub->date = date('d/m/Y H:i:s');
                        $sub->dsc = $data->servname;
                        $sub->dayOfMonth = intval($newData->dayOfMonth);
                        $sub->fixedAmount = intval($newData->fixedAmount);
                        $sub->minimalBalance = $newData->minimalBalance;
                        $sub->targetBalance = $newData->targetBalance;
                        $sub->status = $newData->status;
                        $newData->subscriberId = $sub->subscriberId;
                        $changes = true;

                    }
                }
                if($changes === false){
                    $this->ddInfo->return->result->subscribers[] = $newData;
                }
            } else {
                if($this->ddInfo->return->result->subscribers->gsm == $data->gsm && $this->ddInfo->return->result->subscribers->debitType == $data->debitType) {
                    $newData->subscriberId = $this->ddInfo->return->result->subscribers->subscriberId;
                    $this->ddInfo->return->result->subscribers = $newData;
                    $changes = true;
                } else {
                    $oldData = $this->ddInfo->return->result->subscribers;
                    $this->ddInfo->return->result->subscribers = array();
                    $this->ddInfo->return->result->subscribers[] = $oldData;
                    $this->ddInfo->return->result->subscribers[] = $newData;
                }
            }
            $this->ddInfo->return->result->account->active = true;
            $res = $this->facade->__call('updateAccount', [$this->ddInfo->return->result]);
//            dd($res);
            if(!isset($res->return->resultCode) || $res->return->resultCode != 1){
                $rand = rand(1000,1000000);
                if($res->return->resultCode == -1 || $res->return->resultCode == -10){
                    return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/private-cabinet/direct-debit?status=balance&r='.$rand);
                }
                if($res->return->resultCode == -6){
                    return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/private-cabinet/direct-debit?status=already&r='.$rand);
                }
                if($res->return->resultCode == -5){
                    if(isset($ajax) && $ajax == 1){
                        die('limitdeactive');
                    }
                    return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/private-cabinet/direct-debit?status=limitdeactive &r='.$rand);
                }
                if($res->return->resultCode == -8){
                    return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/private-cabinet/direct-debit?status=notallow&r='.$rand);
                }
                if($res->return->resultCode == -11 || $res->return->resultCode == -12){
                    return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/private-cabinet/direct-debit?status=nolailai&r='.$rand);
                }
                return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/private-cabinet/direct-debit?status=f&r='.$rand);
            }
//            dd($res->return->resultCode);
            if(isset($ajax) && $ajax == 1){
                die(json_encode($res));
            }
        } else {

            if($this->ddInfo->return->resultCode != 1){
//                return redirect('https://geocell.ge/developer_version/public/ge/private/private-cabinet/direct-debit?status=f');
                return view('site.user.directdebit')->with('ddInfo', $this->ddInfo)->with('error', $this->ddInfo->return->resultCode);
            }

            $phone = \Session::get('phone');
            $account = (object) array(
				'masterGSM' =>  $phone,
				'active' => 1,
				'maxPaymentAmount' => 50,
				'maxRequestsAmount' => 50,
				'accountId' => null
            );
            $newData = (object) array(
                'accountId' => null,
                'date' =>  date('d/m/Y H:i:s'),
                'debitType' => $data->debitType,
                'dsc' => $data->servname,
                'gsm' => $data->gsm,
                'status' => boolval($data->status),
                'bonusId' => $data->bonusId,
                'fixedAmount' => $data->fixedAmount,
                'dayOfMonth' => $data->dayOfMonth,
                'retry' => 0,
                'minimalBalance' => intval($data->minimalBalance),
                'targetBalance' => intval($data->targetBalance)
            );
            $this->ddInfo->account = $account;
            $this->ddInfo->subscribers = $newData;

            $res = $this->facade->__call('registerAccount', [$this->ddInfo]);
//            dd($res);
            if($res->return->resultCode != 1){
                $rand = rand(100,1000);
                $rand = rand(1000,1000000);
                if($res->return->resultCode == -1 || $res->return->resultCode == -10){
                    return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/private-cabinet/direct-debit?status=balance&r='.$rand);
                }
                if($res->return->resultCode == -6){
                    return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/private-cabinet/direct-debit?status=already&r='.$rand);
                }
                if($res->return->resultCode == -5){
                    if(isset($ajax) && $ajax == 1){
                        die('limitdeactive');
                    }
                    return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/private-cabinet/direct-debit?status=limitdeactive &r='.$rand);
                }
                if($res->return->resultCode == -8){
                    return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/private-cabinet/direct-debit?status=notallow&r='.$rand);
                }
                if($res->return->resultCode == -11 || $res->return->resultCode == -12){
                    return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/private-cabinet/direct-debit?status=nolailai&r='.$rand);
                }
                return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/private-cabinet/direct-debit?status=f&r='.$rand);
            }
        }
        if(isset($res->return->account) && $res->return->account->active == false) {

            if ($res->return->resultCode == 1 && isset($res->return->subscribers)) {
                if (is_array($res->return->subscribers)) {
                    foreach ($res->return->subscribers as $sub) {
                        if ($sub->gsm == $data->gsm && $sub->debitType == $data->debitType) {
                            $res->sub_id = $sub->subscriberId;
                        }
                    }
                } else {
                    $res->sub_id = $res->return->subscribers->subscriberId;
                }
                $resurl = $this->facade->__call('requestActivationUrl', [$res]);

                return redirect($resurl->return->url);
            } else {
                $rand = rand(100,1000);
                return redirect('https://geocell.ge/developer_version/public/ge/private/private-cabinet/direct-debit?status=f&r='.$rand);
            }
        } else {
            $rand = rand(100,1000);
            return redirect('https://geocell.ge/developer_version/public/ge/private/private-cabinet/direct-debit?status=s&r='.$rand);
        }
    }




    public function deactivation(){
        $data = (object) array(
            'accountId' => $this->ddInfo->account->accountId
        );
        $result = $this->facade->__call('deactivateAccount', [$data]);
        return redirect('https://geocell.ge/developer_version/public/ge/private/private-cabinet/direct-debit');
    }


    public function changeCard(){
        $this->ddInfo = $this->facade->__call('getAccountsByGSM', [$this->phone]);
        $makspan = null;
        if(isset($this->ddInfo->return->result->account)){
            $makspan = str_split($this->ddInfo->return->result->account->maskedPan, 4);
            $makspan = implode('-', $makspan);

        }
        return view('site.lightbox.debit_card_change')->with('maskpan', strtoupper($makspan));
    }

    public function activeCard(){
        $this->ddInfo = $this->facade->__call('getAccountsByGSM', [$this->phone]);
        if(isset($this->ddInfo->return->result->account)){
            $res = $this->facade->__call('requestActivationUrl', [$this->ddInfo]);
            if(!isset($res->return->resultCode) || $res->return->resultCode != 1){
                $rand = rand(101,1001);
                if($res->return->resultCode == -1){

                    return redirect('https://geocell.ge/developer_version/public/ge/private/private-cabinet/direct-debit?status=balance&r='.$rand);
                }
                return redirect('https://geocell.ge/developer_version/public/ge/private/private-cabinet/direct-debit?status=f&r='.$rand);
            }
            if(isset($res->return->url)){
                return redirect($res->return->url);
            } else {
                $rand = rand(101,1001);
                return redirect('https://geocell.ge/developer_version/public/ge/private/private-cabinet/direct-debit?status=f&r='.$rand);
            }
        } else {
            $rand = rand(101,1001);
            return redirect('https://geocell.ge/developer_version/public/ge/private/private-cabinet/direct-debit?status=f&r='.$rand.'&e='.$this->ddInfo->return->resultCode);
        }
    }


    public function debitdeactive(){
        return view('site.lightbox.debit_remove');
    }

    public function getHistory(){
        $this->ddInfo = $this->facade->__call('getAccountsByGSM', [$this->phone]);
        if(isset($this->ddInfo->return->result->account->accountId)){
            $res = $this->facade->__call('getTransactionsByAccountId', [$this->ddInfo->return->result->account->accountId]);
            if(isset($res->return->result)){
                if(!is_array($res->return->result)){
                    $res->return->result = array($res->return->result);
                }
                echo json_encode($res->return->result);
            } else {
                echo json_encode('[]');
            }
        } else {
            echo json_encode('[]');
        }
    }

    function fillbalance(){
        $userInfo = $this->facade->__call('portalGetGSMInfo',[$this->phone]);
        $service = (object) array(
            'value' => 1,
            'title' => ''
        );
        return view('site.lightbox.service-fill-balance', compact('service', 'userInfo'));
    }


    function debiterror(){
        $lang = \App::getLocale();
        if($lang == 'en'){
            $msg = 'Technical error.';
        } else {
            $msg = 'ტექნიკური შეცდომა.';
        }

        return view('site.lightbox.msg', compact('msg'));
    }


    public function debitalready(){
        $lang = \App::getLocale();
        if($lang == 'en'){
            $msg = 'The service is already active for this number';
        } else {
            $msg = 'მითითებული ნომერეს უკვე გააქტიურებული აქვს ეს სერვისი.';
        }

        return view('site.lightbox.msg', compact('msg'));
    }

    function debitSucc(){
        $lang = \App::getLocale();
        if($lang == 'en'){
            $msg = 'The service has been activated';
        } else {
            $msg = 'სერვისი წარმატებით გააქტიურდა.';
        }

        return view('site.lightbox.msg', compact('msg'));
    }

    function debitactive(){
        $lang = \App::getLocale();
        if($lang == 'en'){
            $msg = 'Technical error.';
        } else {
            $msg = 'ტექნიკური შეცდომა.';
        }

        return view('site.lightbox.msg', compact('msg'));
    }

    function nolailai(){
        $lang = \App::getLocale();
        if($lang == 'en'){
            $msg = 'This type of automatic refill is available only for LaiLai subscribers.';
        } else {
            $msg = 'ავტომატური შევსების ეს სერვისი ხელმისაწვდომია მხოლოდ ლაილაის აბონენტებისთვის.';
        }

        return view('site.lightbox.msg', compact('msg'));
    }

    public function notAllow(){
        $lang = \App::getLocale();
        if($lang == 'en'){
            $msg = 'This number does not satisfy service activation conditions.';
        } else {
            $msg = 'აღნიშნული ნომერი ვერ აკმაყოფილებს სერვისის აქტივაციის პირობას.';
        }

        return view('site.lightbox.msg', compact('msg'));
    }

    public function limitdeactive(){
        return view('site.lightbox.debit-limit-deactive');
    }

}


