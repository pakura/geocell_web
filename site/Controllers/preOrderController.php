<?php namespace Site\Controllers;


use App\Http\Controllers\Controller;

use Site\Models\Pages;

class preOrderController extends Controller {


    public function __construct()
    {

    }
    /*optimize later */

    public function index(){
        $cities = \DB::table('dictionary_cities')->get();
        $offices = \DB::table('offices')->where('city_id', 1)->get();
        $iphone7['title'] = 'Pre-order official iPhone 7 and purchase before others';
        $iphone7['img'] = 'https://geocell.ge/site/tsr-components/tsr-custom/images/tsr-preorder-apple-iphone-se-02.jpg';
        $iphone7['description'] = 'Pre-ordered iPhone 7 will be available at Geocell shop, #34 Chavchavadze ave.Estimated date: April 25';
        $iphone7['full_slug'] = '/private/online-shop/phones/apple-iphone-se';
        $iphone7 = (object)$iphone7;
        return view('site.products.preorder', compact('iphone7', 'cities', 'offices'));

    }


    public function getOfiices($lang, $city_id){
        $office = \DB::table('offices')->where('city_id', $city_id)->get();
        $offices = array();
        foreach ($office as $key => $val){
            if($lang == 'en'){
                $offices[] = (object) array('address' => $val->address_en, 'id' => $val->id);
            } else {
                $offices[] = (object) array('address' => $val->address_ge, 'id' => $val->id);
            }
        }
        echo json_encode($offices);
    }

    public function terms(){
        return view('site.lightbox.termsncondition');
    }

}
