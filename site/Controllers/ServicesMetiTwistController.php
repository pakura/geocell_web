<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Products;

class ServicesMetiTwistController extends Controller {

    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index($page)
	{
		\Session::put('url',\Request::url());	
		
		$services = Products::collection($page->attached_collection_id)->WithPriceAndAttachment()->orderby('p.id','DESC')->get();
		return view('site.services.meti.list',compact('services'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function show($slug)
	{
		\Session::put('url',\Request::url());	
		$service =  Products::whereSlug($slug)->WithPriceAndAttachment()->first();
		 
		if(!$service){ abort(404); }

		$services = Products::collection($service->collection_id)->where('products.id','!=',$service->product_id)->WithPriceAndAttachment()->orderby('p.id','DESC')->get();
		
		return view('site.services.meti.twistview',compact('service','services'));  
	}

	 

}
