<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use Site\Models\Services;
use Site\Models\GeocellId;
use App\Models\BillingFacadeServices;
use Illuminate\Http\RedirectResponse;

class TestController extends Controller {

    protected $model;
    protected $facade;

    public function __construct(GeocellId $model, BillingFacadeServices $facade)
    {
        $this->model = $model;
        $this->facade = $facade;
    }

    public function index($lang = null, $number = null, $from = null, $to = null){
        $html = file_get_contents("http://91.151.128.70:8680/BillingFacadeVendor/GeocellPortalWS/GeocellPortalWS?wsdl");
        dd($html);
    }


}
