<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Articles;

class FraudRecomController extends Controller {

    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	public function index($page)
	{
		$siblingPages = \Site\Models\Pages::whereParentId($page->parent_id)->withContent()->where('pages.visibility','=',1)->get();

		$parentPages = \Site\Models\Pages::whereParentId(\DB::raw('(select parent_id from pages where id = '.(int)$page->parent_id.')'))->withContent()->where('pages.visibility','=',1)->get();
		
		$recoms = Articles::whereCollectionId($page->attached_collection_id)->withContent()->get();
		
 
		return view('site.fraud.recomendation',compact('recoms','siblingPages','parentPages'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		/*$faq =  Faq::whereSlug($slug)->withContent()->first();
		if(!$faq){ abort(404); }
		return view('site.faq.view',compact('faq'));  */
	}

	 

}
