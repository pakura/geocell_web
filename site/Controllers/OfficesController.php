<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Offices;

class OfficesController extends Controller {

    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	public function index($page)
	{
		$lang = \App::getLocale();
		$subPages = \Site\Models\Pages::whereParentId($page->parent_id)->withContent()->orderby('pages.position')->get();
		
		$offices = Offices::getList($lang);
//		dd($offices);
		$cities = Offices::cities($lang);
		if(\Input::get('list_mode')) {
			$template = 'site.getintouch.list';
		}
		else {
			$template = 'site.getintouch.map';
		}
		return view($template,compact('offices','subPages','cities'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		/*$faq =  Faq::whereSlug($slug)->withContent()->first();
		if(!$faq){ abort(404); }
		return view('site.faq.view',compact('faq'));  */
	}

	 

}
