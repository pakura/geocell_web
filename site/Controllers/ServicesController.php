<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Products;

class ServicesController extends Controller {

    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index($page)
	{
 
		$services = Products::collection($page->attached_collection_id)->WithPriceAndAttachment()->get();
		$children = \Site\models\Pages::Children($page->id)->Attachment()->where('pages.visibility','=',1)->orderby('position')->get();
		header('Content-Type: text/html; charset=utf-8');
		if($page->attached_collection_id == 56){

			$serv[] = $children[0];
			$serv[] = $services[5];
			$serv[] = $services[4];
			$serv[] = $children[1];
			$serv[] = $services[1];
			$serv[] = $services[2];
			$serv[] = $services[3];
			$serv[] = $services[0];
			$services = $serv;
			$children = array();
		}
		return view('site.services.list',compact('services','children'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function tsr_lailai($slug)
	{
	  /*  $methodName ='index';
	    return $this->{$methodName}(56);*/
        \Session::put('url',\Request::url());	 
		$service =  Products::whereSlug($slug)->withContentOnly()->first();
		// dd($service);
		if(!$service){ abort(404); }
		return view('site.services.lailai',compact('service'));  
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function show($slug)
	{
	  /*  $methodName ='index';
	    return $this->{$methodName}(56);*/
        \Session::put('url',\Request::url());	 
		$service =  Products::whereSlug($slug)->WithPriceAndAttachment()->first();
	    //dd($service);
		if(!$service){ abort(404); }
		return view('site.services.view',compact('service'));  
	}

	 

}
