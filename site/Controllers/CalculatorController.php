<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Models\GeocellId;
use Site\Models\Services;
use Site\Models\Products;


class CalculatorController extends Controller {

    public function __construct()
    {

    }


    public function index($page) {
        $maximals = \DB::table('data_calc')->where('device', '=', 1)->lists('max_unit', 'service');
        $data_calc = \DB::table('data_calc')->where('device', '=', 1)->lists('multiplier', 'service');
        $modem_data_calc = \DB::table('data_calc')->where('device', '=', 2)->lists('multiplier', 'service');
        $data_calc_limits = \DB::table('data_calc_limits')->get();
        $modem_data_calc_limit = \DB::table('data_calc_inet_max')->where('service', '=', 3)->get();
        $mobile_data_max = \DB::table('data_calc_inet_max')->where('service', '=', 1)->get();
        $meti_data_max = \DB::table('data_calc_inet_max')->where('service', '=', 2)->get();
        $services = Products::collection($page->attached_collection_id)->forBundles()->get();
//        dd($services);

        return view('site.calculator.view', compact('data_calc', 'data_calc_limits', 'maximals', 'services', 'modem_data_calc', 'mobile_data_max', 'meti_data_max', 'modem_data_calc_limit'));
    }

}
