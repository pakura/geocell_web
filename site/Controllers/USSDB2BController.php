<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\USSD;

class USSDB2BController extends Controller {

    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	public function index($page)
	{
		//$subPages = \Site\Models\Pages::whereParentId($page->parent_id)->withContent()->get();
		$lang = \App::getlocale();
		$ussds = USSD::orderby('position','Desc')->orderby('code')
												 ->select('*','description_'.$lang.' as title','brand_'.$lang.' as brandtitle')
												 ->where('b2b','=',1)->paginate(22);
      
		return view('site.faq.ussd',compact('ussds'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		 
	}

	 

}
