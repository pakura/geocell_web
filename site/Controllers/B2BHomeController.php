<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use  Site\Models\Gallery;
use  Site\Models\Products;
use  Site\Models\Articles;

class B2BHomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {

        $gallery = Gallery::getItem(91);

        $articles = Articles::whereCollectionId(90)->WithContent()->OrderById('DESC')->take(4)->get();
        $blog = Articles::whereCollectionId(105)->WithContent()->OrderById('DESC')->take(4)->get();
        $articlesItemsCount = Articles::where('articles.collection_id','=',90)->count();
        $blogItemsCount = Articles::where('articles.collection_id','=',105)->count();


        return view('site.b2b-home',compact('gallery','articles','articlesItemsCount', 'blog', 'blogItemsCount'));
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function locale($lang)
    {

        return $this->index();
    }

    public function partner($lang){
        return view('site.lightbox.partner');
    }

    public function PartnerSend(){
        $data = (object)\Input::all();

        $message = $data;
        \Mail::send('emails.partner2', ['title' => 'Call me', 'data' => $message], function ($message)
        {
            $message->from('no-reply@geocell.ge', 'Geocell.ge');
            $message->to('request@geocell.ge');
//            $message->to('vajasin@gmail.com');
            $message->subject('Call me');
        });
        \Session::set('mail', true);
        return back()->withInput();
    }
}
