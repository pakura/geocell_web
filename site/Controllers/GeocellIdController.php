<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use Site\Models\Services;
use Site\Models\GeocellId;
use App\Models\BillingFacadeServices;
use Illuminate\Http\RedirectResponse;

class GeocellIdController extends Controller {

    protected $model;
    protected $facade;

    public function __construct(GeocellId $model, BillingFacadeServices $facade)
	{
		 $this->model = $model;
		 $this->facade = $facade;
	}
	
 	public function logInTemplate(){
 		if(\Request::ajax()) {
 			return view('site.lightbox.auth-sign-in');
 		}
 		
 		return view('site.user.auth-sign-in');
 	}

	public function alterNumber(){

		$phone = \Input::get('phone');

		if(!$phone) { return 0;}

		\Session::put('phone', '995'.$phone);
         
		$res  = $this->facade->__call('portalGetSubscriberData',['995'.$phone]);
		$userdata  = $this->facade->__call('portalGetGSMInfo',['995'.$phone]);

		 if((isset($res->return->isPrivate) && $res->return->isPrivate==false &&  (isset($res->return->isStaff) && $res->return->isStaff==false) ) )
		 {
			return view('site.lightbox.msg')->with('msg',trans('site.only_b2c'));
		 }
		 elseif(isset($res->return->resultCode) &&  $res->return->resultCode==-1006)
		 {
			return view('site.lightbox.msg')->with('msg',trans('site.not_geocell'));
		 }
		 elseif(isset($res->return->serviceProviderId) &&  (substr($res->return->serviceProviderId, 0, 3) === 'GCN' ||  substr($res->return->serviceProviderId, 0, 3) === 'MFI') ){
		 	return view('site.lightbox.msg')->with('msg',trans('site.gcn_not_allowed'));
		 }elseif($userdata->return->brand == 2){
             return view('site.lightbox.msg')->with('msg',trans('site.brand1notAllow'));
         }
		// dd($res);
		
		$response = $this->model->_alterNumber($phone,true);
//        dd($response);
        
try {
    if(isset($response->needspassword) && $response->needspassword==1) {
				$inserdata = \DB::table('users_pass_type')->insert(
						['mobile' => '995'.$phone, 'type' => 1]
				);
			return view('site.lightbox.auth-sign-in-type-pass');
		} else {
			$inserdata = \DB::table('users_pass_type')->insert(
					['mobile' => '995'.$phone, 'type' => 0]
			);
			return view('site.lightbox.auth-sign-in-sms');
		}
} catch (\Exception $e) {
    $myfile = file_put_contents('logs.txt', $e->getMessage());
}
		
	}

	public function forgotPass(){
		$phone= \Session::get('phone');

		if(!$phone) { return 0;}

		$response = $this->model->_forgotPassword($phone);

		return view('site.lightbox.auth-forgot-password');		 	 		   

	} 

	public function alterSms($lang){

		$phone= \Session::get('phone');

		if(!$phone) { return 0;}

		$sms = \Input::get('password');

		if(\Input::get('ask_password')) {

			$tpl = view('site.lightbox.auth-set-password')->with('sms',$sms)->render();

        	return \Response::json(['tpl'=>$tpl]);

		}

		$pass = \Input::get('password2');

		$wrongPass = '';
		if(!empty($pass)) {
			$response = $this->model->_setPassword($phone,$sms,$pass);
			$wrongPass = trans('site.wrong_pass');
		}
		else{
			$response = $this->model->_alterSms($phone,$sms);

            $wrongPass = trans('site.wrong_sms_code');
        }

        if($response->success ==0) { return response()->json(['errorMsg' => $wrongPass]);   }

		$user = $this->model->getInfo($response->token);
		if($user->firstname){
			\Session::put('site_user', $user->firstname.' '.$user->lastname);
		}
		else {
			\Session::put('site_user', $phone);
		}
		\Session::put('site_auth_token', $response->token);
		//\Session::forget('phone');

		$resUserInfo = $this->facade->_getUserInfo($phone);
       if(isset($resUserInfo->return->resultCode) && $resUserInfo->return->resultCode==1){
            \Session::put('userInfo',$resUserInfo);
         }
		$login_log = \DB::table('login_log')->insert(
				['mobile' => $phone, 'full_name' => $user->firstname.' '.$user->lastname]
		);
        $a = (\Session::has('_previous.url'))?\Session::get('_previous.url'):false;
         
        if($a) {
        	$thisurl = explode('/', $a);
        	if (in_array('services', $thisurl)){
        		$url = $a.'?next'; 
        	} else {
        		$url = $lang.'/'.\Site\Models\Pages::getSlugById(23);
        	}
        	\Session::forget('url');   	
        } else {
        	$url = $lang.'/'.\Site\Models\Pages::getSlugById(23);
        }
        // die($url);

		return response()->json(['url' => $url]);		 
		 
	} 

	public function logOut($lang){
        \Session::forget('fullauth');
        \Session::forget('accsesDetails');
        \Session::forget('accsesSign');
        $this->model->_logOut(\Session::get('site_auth_token'));
        $this->model->forgetUserSessionData();
		return redirect('/'.$lang);
	}


}
