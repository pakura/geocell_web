<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Models\GeocellId;
use Site\Models\Services;


class EsignatureController extends Controller {

    protected $model;
    protected $geocellId;
    protected $userInfo = [];
    protected $actServices = [];
    protected $phone;
    protected $fullAuth = false;
    protected $documentList = array();
    protected $directdebit;

    public function __construct(BillingFacadeServices $model, geocellid $geocellId)
    {
        $this->geocellId = $geocellId;
        $this->model=$model;
        $this->phone= \Session::get('phone');
        $this->directdebit = $this->model->__call('getAccountsByGSM', [$this->phone]);
    }


    public function index() {
        $lang = \App::getLocale();
        if(!$this->geocellId->isAuthed()) { return view('site.user.auth-sign-in'); }
//        \Session::put('fullauth', true);
//        $test = \Session::has('fullauth');
//        dd($test);

        if(\Session::has('fullauth') == false){
            \Session::put('accsesSign', true);
            return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/seccode');
        }
        \Session::forget('fullauth');
        \Session::forget('accsesSign');
        \Session::put('allowDownload', true);
        $this->documentList = $this->model->__call('portalGetESignedDocumentList',  array('gsm' => $this->phone));
//        dd($this->documentList);
        if(isset($this->documentList->return->items) && !is_array($this->documentList->return->items)){
            $temparr = $this->documentList->return->items;
            $this->documentList->return->items = array();
            $this->documentList->return->items[] = $temparr;
        }
        if(!isset($this->documentList->return)){
            $this->documentList->return = array();
        }
        if (isset($this->directdebit->return->result->account)){
            $this->directdebit = true;
        } else {
            $this->directdebit = false;
        }
        return view('site.user.esignature')->with('lists',$this->documentList->return)->with('directdebit', $this->directdebit);
    }


    public function download($lang, $docId){
        $lang = \App::getLocale();
        if(\Session::get('allowDownload') == false){
            \Session::put('accsesSign', true);
            return redirect('https://geocell.ge/developer_version/public/'.$lang.'/private/seccode');
        }

        $document = $this->model->__call('portalGetESignedDocument', array('docId'=>$docId));
        if(isset($document->return->data)){
            $ourFileName = 'esignatures/'.md5(md5($docId).md5($this->phone)).'.pdf';
            \File::put($ourFileName, $document->return->data);
            $filename = $ourFileName;
            $path = storage_path($filename);
            $res = \DB::table('cron_signature')->insert(['document' => $ourFileName]);
            return \Response::make(file_get_contents($filename), 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$filename.'"'
            ]);

        }
    }


    public function deleteDocs(){
        $docs = \DB::table('cron_signature')->get();
        foreach($docs as $key => $doc){
            unlink($doc->document);
        }
    }

}
