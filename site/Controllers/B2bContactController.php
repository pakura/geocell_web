<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Banners;

class B2bContactController extends Controller {

    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	public function index($page)
	{
		$subPages = \Site\Models\Pages::whereParentId($page->parent_id)->withContent()->orderby('pages.position')->get();
		
		$banners = Banners::whereCollectionId($page->attached_collection_id)->withContent()->get();
		$managers = Banners::whereCollectionId(98)->withContent()->get();
		

		return view('site.getintouch.contact_b2b',compact('banners','subPages','managers'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
		/*$faq =  Faq::whereSlug($slug)->withContent()->first();
		if(!$faq){ abort(404); }
		return view('site.faq.view',compact('faq'));  */
	}

	 

}
