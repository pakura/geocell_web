<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;

use Site\Models\Products;

class B2BFlexibleController extends Controller {

    
    public function __construct()
	{
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function index($page)
	{
 
		$services = Products::collection($page->attached_collection_id)->Withcontent()->get();

		$prices = Products::collection($page->attached_collection_id)->pricesForFlexible()->get()->groupby('product_id'); 

		return view('site.services.flexible',compact('services','prices'));
		 
		 
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function tsr_lailai($slug)
	{
	  /*  $methodName ='index';
	    return $this->{$methodName}(56);*/
        \Session::put('url',\Request::url());	 
		$service =  Products::whereSlug($slug)->withContentOnly()->first();
		// dd($service);
		if(!$service){ abort(404); }
		return view('site.services.lailai',compact('service'));  
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  string  $slug
	 * @return Response
	 */
	public function show($slug)
	{
	  /*  $methodName ='index';
	    return $this->{$methodName}(56);*/
        \Session::put('url',\Request::url());	 
		$service =  Products::whereSlug($slug)->WithPriceAndAttachment()->first();
	    //dd($service);
		if(!$service){ abort(404); }
		return view('site.services.view',compact('service'));  
	}

	 

}
