<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Libs\Curl;

class FillBalanceController extends Controller {
    protected $facade;

    public function __construct(BillingFacadeServices $facade)
    {

         $this->facade = $facade;
    }

	public function fill_balance_enter_number(){
         
        return view('site.lightbox.fill-balance-enter-number');
    }

    public function fill_balance_enter_number_test(){
         
        return view('site.lightbox.fill-balance-enter-number-test');
    }


    public function fill_balance_choose_action(){
        $phone = '995'.\Input::get('phone');
        $res  = $this->facade->__call('portalGetSubscriberData',[$phone]);
        if(isset($res->return->resultCode) &&  $res->return->resultCode==-1006)
         {
            return view('site.lightbox.msg')->with('msg',trans('site.not_geocell'));
         }
         elseif (isset($res->return->resultCode) &&  $res->return->resultCode<=0){
            return view('site.lightbox.msg')->with('msg',trans('site.error_1001'));
         }

        return view('site.lightbox.fill-balance-choose-action',compact('phone'));
    }


    public function fill_balance_enter_amount(){
       
        $phone = \Input::get('phone');

        /*$res  = $this->facade->__call('portalGetSubscriberData',[$phone]);
        if(isset($res->return->resultCode) &&  $res->return->resultCode==-1006)
         {
            return view('site.lightbox.msg')->with('msg',trans('site.not_geocell'));
         }
         elseif (isset($res->return->resultCode) &&  $res->return->resultCode<=0){
            return view('site.lightbox.msg')->with('msg',trans('site.error_1001'));
         }*/
        return view('site.lightbox.fill-balance',compact('phone'));
    }

    public function fill_balance_confirmation(){
        $amount = \Input::get('amount');
        $phone = \Input::get('phone');
        return view('site.lightbox.fill-balance-confirmation',compact('amount','phone'));
    }

    public function fill_balance_buy_meti(){
        $phone = \Input::get('phone');
        $services = \Site\Models\Products::collection(59)->WithPrice()->MetiImage()->orderby('p.value','asc')->get();
        foreach ($services as $key => $value){
            $services[$key]->allowBuy = $this->checkMeti(($value->sku2), ($value->value * 100));
        }
//        dd($services);
        return view('site.lightbox.fill-balance-buy-meti',compact('services','phone'));
    }

    public function checkMeti($bundleid, $bundleprice){
        $paymentid = rand(1000000000,9999999999);
        $id = rand(1000000000,9999999999);
        $phone = \Input::get('phone');
        $phone = substr($phone.'', 3);
        $url = "http://91.151.128.64:7070/bundleAccept/bundleRequest";
        $params = array(
            "source" => "mobility",
            "acquirer" => "mobility",
            "time" => date('y/m/d-H:i:s'),
            "language" => "en",
            "amount" => 300,
            "terminal" => "internet",
            "service" => "verification",
            "phone" => $phone,
            "currency" => 981,
            "paymentid" => $paymentid,
            "id" => $id,
            "pass" => "Gd3mYsn80j"
        );
        $hash = '';
        foreach ($params as $kay => $value){
            if ($hash == ''){
                $url .= "?".$kay."=".$value;
            } else {
                $url .= "&".$kay."=".$value;
            }
            $hash .= $kay."=".$value.";";
        }
        $url .= "&mac=".md5($hash);
        $url .= "&bundleid=$bundleid&bundleprice=$bundleprice";
        $response = file_get_contents($url);
        if (isset($response)){
            $response = explode('&', $response)[0];
            $response = explode('=',$response)[1];
            if ($response == 0){
                return true;
            } else {
                return false;
            }
        }
    }


}


