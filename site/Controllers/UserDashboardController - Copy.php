<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Models\GeocellId;
use Site\Models\Services;


class UserDashboardController extends Controller {
 
    protected $model;
    protected $geocellId;
    protected $userInfo = [];
    protected $actServices = [];
    protected $phone;

    public function __construct(BillingFacadeServices $model,GeocellId $geocellIdModel)
	{
	    
        $this->middleware('site_user');

        $this->geocellId=$geocellIdModel; 
        $this->model=$model; 
        $this->phone= \Session::get('phone');
       
	}
	
 	
	public function index() {

        \Session::put('url',\Request::url());    
        
         if(!$this->geocellId->isAuthed()) { return view('site.user.auth-sign-in'); }
      
      
        // dd(\Session::get('phone'));
		 $resUserInfo = $this->model->__call('portalGetGSMInfo',[$this->phone]);

         if(isset($resUserInfo->return->resultCode) && $resUserInfo->return->resultCode==1){
            \Session::put('userInfo',$resUserInfo);
         }
         $this->userInfo = \Session::has('userInfo')?\Session::get('userInfo'):$resUserInfo;

          //dd($this->userInfo);
        /*get installment status*/
        $installment_status =  $this->model->__call('portalGetPhoneInstallmentStatus',[$this->phone]);
        dd($installment_status);

		 $userServices = $this->model->__call('portalGetActiveServices',[$this->phone]);	
         //dd($userServices);
         /*get services keys*/
         $services_keys = $this->getActiveServicesKey($userServices);
         /* take info from database according keys */
         $actServices = $this->getServicesForList($services_keys);
        // dd($actServices);
         /*merge facade's services info with DB services info */
         $actServices =  $this->getOrderedServiceData($actServices);   
         //dd($actServices);
         $numbers = [];
         if(in_array('S_TRIPLE_ZERO', $services_keys)) {
            $res = $this->model->__call('portalGetFriendList',[$this->phone]);
            
            $numbers= [];  
            if( isset($res->return->list) && is_array($res->return->list)) { $numbers = $res->return->list; } 
            elseif( isset($res->return->list) ){  $numbers[] = $res->return->list;  }
         }
            //dd($services_keys);
         //dd($numbers);
         /*get available services for the number*/
         $available_services= $this->addService();
		 //dd($available_services);
		 return view('site.user.dashboard')->with('userInfo',$this->userInfo)
                                           ->with('actServices',$actServices)      
                                           ->with('numbers',$numbers)      
                                           ->with('installment_status',$installment_status)      
		 								   ->with('available_services',$available_services);  	
	} 

    public function fill_balance(){
         
        //$amount = \Input::get('amount');
        return view('site.lightbox.fill-balance');
    }
    
    public function fill_balance_confirmation(){
        $amount = \Input::get('amount');
        
        return view('site.lightbox.fill-balance-confirmation',compact('amount'));
    }

    public function deactivateService($lang,$service_key){
        $response = $this->model->__call('portalTerminateService',[$this->phone,$service_key]);
       // dd($response);
       
        if(isset($response->return->resultCode) &&  $response->return->resultCode<0)  {
           $msg = "Biling Facade Error: ".$response->return->resultCode;
           
        } 
        else {
             $msg  = "სერვისი დეაქტივირებულია";
        }

        return view('site.lightbox.msg',compact('msg'));
    }

    public function removeService($lang, $service_key){
        $service = Services::whereServiceKey($service_key)->select('*','description_'.$lang.' as description')->first();

    
        return view('site.lightbox.deactivate-service-notification',compact('service'));
       

        // return view('site.lightbox.couldnot-deactivate-service',compact('service'));
    }
    
    public function  activateService($lang,$service_key){
        $response = $this->model->__call('portalActivateService',[$this->phone,'',$service_key]);
        dd($response);
    }
    
    public function getGeocreditInfo(){
        $response = $this->model->__call('portalGetGeoCreditInfo',[$this->phone]);
       // dd($response);
       // dd($this->userInfo);
        return view('site.lightbox.geo-credit-info',compact('response'));
    }
    
    public function removeNumber($lang,$number){
    	$response =  $this->model->__call('portalFriendSlotAction',[$this->phone,$number,2]);
        if($response->return->resultCode==1) {
        	return view('site.lightbox.msg')->with('msg','ნომერი წაიშალა');
        } 
    }

    public function removeNumberConfirmation($lang,$number){
    
        return view('site.lightbox.remove-number-confirmation',compact('number'));
      
    }

    public function addNumber(){
    	 $number = \Request::get('friend_number');
    	 if($number) {
        $number ='995'.$number; 
    	 	$response = $this->model->__call('portalFriendSlotAction',[$this->phone,$number,1]);
        if(isset($response->return->resultCode) && $response->return->resultCode==1) {
           return view('site.lightbox.add-number',compact('number'));
        } 

          
    	 }
    	 
    	 
    }

    public function addService(){

       // $this->userInfo = $this->model->__call('portalGetGSMInfo',[\Session::get('phone')]);

        $services = $this->model->__call('portalGetAvailableServices',[$this->phone]);
       //dd($services);
        $services = $this->getServices($this->getAllAvailableServicesKey($services) );

        return $services;
       // return view('site.lightbox.add-service',compact('services'));
    }

	private function getServices ($keys){

		//$keys = $this->getActiveServicesKey($obj);
         //dd($keys);
    $lang = \App::getLocale();
		$brand = isset($this->userInfo->return->brand) ? $this->userInfo->return->brand : 2;
        $services = Services::join('products as p','p.sku','=','service_key')

                            ->whereBrand($brand)
        			        
                            ->whereIn('service_key',$keys)
                            ->select('p.id as product_id','services_list.*','services_list.description_'.$lang.' as description')
                            ->get(); 
        //dd($services);
        //return $this->getOrderedServiceData($services);	
        return $services;	   
	}
    
    private function getServicesForList ($keys){

        //$keys = $this->getActiveServicesKey($obj);
         //dd($keys);
      $lang = \App::getLocale();
    	$brand = isset($this->userInfo->return->brand) ? $this->userInfo->return->brand : 2;
        $services = Services::whereBrand($brand)                           
                            ->whereIn('service_key',$keys)  
                            ->select('*','description_'.$lang.' as description')                    
                            ->get(); 
        //dd($services);
        //return $this->getOrderedServiceData($services);   
        return $services;      
    }

    private function getOrderedServiceData($services){
    	$data = [];
        //$service - list from DB;
        //$this->actServices - service list from wsdl;

    	foreach( $services as $service) {

            $service['data'] =  $this->actServices[$service->service_key];
    		$data[$service->service_type]['data'][] =$service;
    		 
            /*set amount to array*/
    		if(!array_key_exists('amount',$data[$service->service_type])) { 

                $data[$service->service_type]['amount'] = 0;


    		}

    		$data[$service->service_type]['amount'] = $data[$service->service_type]['amount'] + (isset($this->actServices[$service->service_key]->amount)?$this->actServices[$service->service_key]->amount:0);
    	}

    	 //dd($data);

    	return $data;
    }

	private function getActiveServicesKey($obj){
		$arr = []; 
        if(isset($obj->return->list)) {
		  
           
    		if(is_array($obj->return->list)){
	            foreach ($obj->return->list as $service) { 
	                 
	    		 	/*if(isset($service->expirationDate) && strtotime(str_replace('/', '-',$service->expirationDate) )<strtotime(date('Y-m-d H:i:s')) ) 
	                    { continue; } */
	                  
	                 if(isset($service->key)) {
	    		 	 	$arr[]=$service->key;
	    		 	 	$this->actServices[$service->key] = $service;
	    		 	 }

	    		 }
    		}
    		else {
           // dd($obj->return->list->key);
    			/*if(isset($obj->return->list->expirationDate) && strtotime(str_replace('/', '-',$obj->return->list->expirationDate) )<strtotime(date('Y-m-d H:i:s')) ) 
	                    { continue; } */
	                  
	                 if(isset($obj->return->list->key)) {
	    		 	 	$arr[]=$obj->return->list->key;
	    		 	 	$this->actServices[$obj->return->list->key] = $obj->return->list;
	    		 	 }

    		}


        }
       // dd($arr);
        return $arr;
	}

    private function getAllAvailableServicesKey($obj){
        $arr = []; 
        if(isset($obj->return->list)) {
        
            foreach ($obj->return->list as $service) { 

                    $arr[]=$service->key;                  
             }
        }
       // dd($arr);
        return $arr;
    }


    public function portalGetPhoneInstallmentStatus(){
    	return $status = $this->model->__call('portalGetPhoneInstallmentStatus',[$this->phone]);
    	 
    }
   
}
