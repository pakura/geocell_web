<?php namespace Site\Controllers;

use App\Http\Controllers\Controller;
use App\Models\BillingFacadeServices;
use Site\Models\Services;


class ServicesRoamingGeocellController extends Controller {
 
    protected $model;
    protected $roamingList = [];

    public function __construct(BillingFacadeServices $model)
	{
	    
        $this->model=$model; 
        $this->roamingList = $this->model->__call('getRoamingPartners',[]);

	}
	
 	
	public function index() {

         $key_word = \Request::get('key_word'); 
         $discount = \Request::get('discount'); 
         $mobile = \Request::get('mobile_internet'); 
         //dd($key_word);  
        if($key_word){ return $this->show($key_word); } 
        
        $roamingList = $this->getDistinctLabel($this->roamingList->return->roamingPartners);

        if($discount){ 
            $roamingList = $this->getDistinctLabel($this->withDiscount());
        } 
        elseif ($mobile) {
                $roamingList = $this->getDistinctLabel($this->withMobileInternet());
        }   
        else { 
         $roamingList = $this->getDistinctLabel($this->roamingList->return->roamingPartners);
        } 
         //dd($roamingList);
		 return view('site.services.roaming.list',compact('roamingList'));	
	} 
    
    /* search by country name and get list of roaming operators*/
    public function show($cName){

       /*echo '<pre>';
         var_export($this->roamingList->return->roamingPartners); */
        $roaming = array_filter($this->roamingList->return->roamingPartners, function($el) use ($cName) {
            
            return ( strpos(ucfirst(strtolower($el->countryName)), ucfirst(strtolower($cName))) !== false  && (isset($el->isPostpaid) &&  $el->isPostpaid==true));

        });
        $geo = 1;
        return view('site.services.roaming.view',compact('roaming','cName', 'geo'));
    }
    
    /* search by discount*/
    private function withDiscount(){

        $roaming = array_filter($this->roamingList->return->roamingPartners, function($el) {
            
            return (  $el->isDiscount !== false );

        });
         
        return $roaming;
    }

    /* search by mobile internet */
    private function withMobileInternet(){

        $roaming = array_filter($this->roamingList->return->roamingPartners, function($el) {
            
            return ( $el->isGPRSRoaming !== false );

        });
         
        return $roaming;
    }

	 
    /* Get distinc letters + countries by alphabetical order */
    private function getDistinctLabel($data) {
        $symbols = [];
        $lang = \App::getLocale();
        foreach($data as $dt){
            if(isset($dt->isPostpaid) && $dt->isPostpaid==true){
                if($lang == 'ge'){
                    $dt->countryName = $dt->countryNameGeo;
                }
                $currSymbol = strtoupper(mb_substr($dt->countryName, 0, 1));

                if(isset($symbols[$currSymbol]) && in_array($dt->countryName,$symbols[$currSymbol]) ) {  continue; }

                $symbols[$currSymbol][] =  $dt->countryName;

            }

        }
            
        ksort($symbols); 


        return $symbols;
    }

}
