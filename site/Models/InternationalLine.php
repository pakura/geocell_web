<?php namespace Site\Models;

use Illuminate\Database\Eloquent\Model;
 

class InternationalLine extends Model{

    protected $table = 'international_line';

	public static  function getList($language='en')
	{
		
 	    $key_word = trim(\Request::get('group'));
		if ($key_word != 'discount'){
			return self::where('language', $language)->where('has_discount', '!=', 0)->orderBy('letter')->orderBy('sorting')->orderBy('title')->get();
		} else {
			$res =  self::where('language', $language)->where('has_discount', '!=', 1)->orderBy('letter')->orderBy('sorting')->orderBy('title')->get();
			foreach($res as $key => $val){
				if($val->gel_min_nodiscount != null) $val->gel_min = $val->gel_min_nodiscount; else $val->gel_min = $val->gel_min;
			}
			return $res;
		}
	}
 	
 	public function scopeFilterGroup($query,$kw){

 		$query->where("country_group","like","%$kw%");

 	} 
}


