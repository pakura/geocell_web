<?php namespace Site\Models;

use Illuminate\Database\Eloquent\Model;

class ProductOptions extends Model{

	protected $table='products_options';
	


  public function scopeGetOptions($query,$product_id){

      return $query->whereProductId($product_id)->where('products_options.value','!=','')
                                                 ->join('products_option_list as pol','pol.id','=','products_options.products_option_list_id') 
                                                 ->join('products_option_list_content as c','pol.id','=','c.products_option_list_id')
                                                 ->where('c.language','=',\App::getlocale())       
                                                 ->select('products_options.value','products_options.sku_code','pol.id','pol.color','pol.option_type','c.title','c.description','c.initial')
                                                 /*->orderBy('pol.option_type')   */                                             
                                                /* ->orderBy('products_options.value','asc')*/
//                                                 ->orderBy('pol.position','desc')
                                                 ->get()->groupBy('option_type');

  }
     	 
}
