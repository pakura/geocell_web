<?php namespace Site\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model{

	protected $table='faq';
	
	 
  
 

   public static function getPage($id){
      return \Site\Models\Pages::where('pages.id','=',$id)->where('c.language',config('app.locale'))
                                                           ->join('pages_content as c','pages.id','=','c.page_id')
                                                           ->first();
   }

  public function scopeWithContent($query)
  {
        $query->where('c.language','=',\App::getLocale())
              ->join('faq_content as c','c.faq_id','=','faq.id')
              ->select('faq.*','c.question','c.answer')
              ->groupBy('c.faq_id');
  }

  public function getCreationDateAttribute($creation_date)
  {
      return date("d F Y", strtotime($creation_date));
  
  } 
  	 
}
