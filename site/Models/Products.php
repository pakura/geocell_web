<?php namespace Site\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model{

	protected $table='products';

	 
   public static function getItem(){
 
   }

   public static function getForHomePage(){

        return self::whereHomePage(1)
                    ->whereCollectionId(5)
                    ->WithContent()
                    ->orderBy('products.id', 'desc')
                    ->take(4)
                    ->get();
   }

    public static function getLatest($collection_id){

        return self::collection($collection_id)
            ->withPriceAndAttachment()
            ->where('products.visibility',1)
            ->WithFilter()
            ->orderBy('products.id', 'desc')
            ->paginate(8);
    }

    public static function getAll($collection_id){

        return self::collection($collection_id)
            ->withPriceAndAttachment()
            ->where('products.visibility',1)->groupBy('brand_id');
    }

   public function scopeWithFilter($query){
       if(isset($_GET['brand']) && $_GET['brand'] != 'null') {
           $query->whereIn('brand_id', explode(',',$_GET['brand']));
       }
       if(isset($_GET['price']) && ($_GET['price'] == 'asc' || $_GET['price'] == 'desc')){
          // dd($query);
            $query->orderBy('p.value', $_GET['price']);
       }

   }

  public function scopeWithContent($query)
    {
        $query->join('products_content as c','c.product_id','=','products.id')
              ->join('pages as pg','products.collection_id','=','pg.attached_collection_id')
              ->where('c.language','=',\App::getLocale())
              ->leftjoin('products_attachments as a', function ($join) {

                  $join->on('a.product_id','=','products.id')
                       ->where('a.file_type','=',1);
                })          
              ->select('products.*','pg.full_slug as page_slug','c.*','a.file');
    } 

    public function scopeWithContentOnly($query)
    {
        $query->join('products_content as c','c.product_id','=','products.id')
              ->where('c.language','=',\App::getLocale())          
              ->select('products.*','c.*');
    } 

     public function scopeWithTitle($query)
    {
        $query->join('products_content as c','c.product_id','=','products.id')
              ->where('c.language','=',\App::getLocale())          
              ->select('products.*','c.title');
    } 

   public function scopeWithAttachment($query)
    {
        $query->join('products_attachments as a','a.product_id','=','products.id')
              ->where('a.file_type','=',1)
              ->addSelect('a.file');
    }  

    public function scopeWithPageSlug($query)
    {
        $query->join('pages as pg','products.collection_id','=','pg.attached_collection_id')
              ->addSelect('pg.full_slug as page_slug');
    } 

    public function scopeWithBrand($query)
    {
        $query->join('brands as b','b.id','=','products.brand_id')
              ->addSelect('b.slug as brand_slug');
    } 

    public function scopeForBundles($query)
    {
        $query->join('products_content as c','c.product_id','=','products.id')
              ->join('products_prices as p','p.product_id','=','products.id','left outer')
              
              ->leftjoin('products_options as po', function ($join) {

                  $join->on('po.product_id','=','products.id')
                       ->where('po.products_option_list_id','=',2);
                })
              ->where('c.language','=',\App::getLocale())                           
              ->select('products.*','p.daily','c.*','p.value','po.value as ussd_code');
    }    
   public function scopeWithPrice($query)
    {
        $query->join('products_content as c','c.product_id','=','products.id')
              ->join('pages as pg','products.collection_id','=','pg.attached_collection_id')
              ->join('products_prices as p','p.product_id','=','products.id','left outer')
              
              ->leftjoin('products_options as po', function ($join) {

                  $join->on('po.product_id','=','products.id')
                       ->where('po.products_option_list_id','=',2);
                })
              ->where('c.language','=',\App::getLocale())                           
              ->select('products.*','p.daily','pg.full_slug as page_slug','c.*','p.value','po.value as ussd_code');
    }  
   
    public function scopePricesForFlexible($query)
    {
        $query->join('products_prices as p','p.product_id','=','products.id')                           
              ->select('p.*');
    }  

   public function scopeWithPriceAndAttachment($query)
    {
        $query->join('products_content as c','c.product_id','=','products.id')
              ->leftjoin('products_prices as p', function ($join) {

                  $join->on('p.product_id','=','products.id')
                       ->where('p.type','=',1);
               })

               ->leftjoin('products_attachments as a', function ($join) {

                  $join->on('a.product_id','=','products.id')
                       ->where('a.file_type','=',1);
                })
               ->leftjoin('products_options as po', function ($join) {

                  $join->on('po.product_id','=','products.id')
                       ->where('po.products_option_list_id','=',2);
                })
               ->leftjoin('products_options as rb', function ($join) {

                  $join->on('rb.product_id','=','products.id')
                       ->where('rb.products_option_list_id','=',7);
                }) 
              ->where('c.language','=',\App::getLocale())          
              ->select('products.*','c.*','p.value','p.daily','a.file','po.value as ussd_code','rb.value as ribbon');
    }

  public function scopeMetiImage($query)
    {
        $query->leftjoin('products_attachments as m', function ($join) {

                                  $join->on('m.product_id','=','products.id')
                                       ->where('m.file_type','=',5);
                             })->addSelect('m.file');
    } 

    public function scopeCollection($query,$collection_id)
    {
        $query->where('products.collection_id','=',$collection_id);
    } 

  public static function getAttachedFiles($product_id){

      return \DB::table('products_attachments')->whereProductId($product_id)->where('file_type','!=',1)->get();
  }  

  public static function getAllPrices($product_id){

      return \DB::table('products_prices')->whereProductId($product_id)->get();
  }  


  public static function getOptions($product_id){

      return \DB::table('products_options as po')->whereProductId($product_id)->where('po.value','!=','')
                                                 ->join('products_option_list as pol','pol.id','=','po.products_option_list_id') 
                                                 ->join('products_option_list_content as c','pol.id','=','c.products_option_list_id')
                                                 ->where('c.language','=','en')       
                                                 ->select('po.value','pol.id','pol.color','pol.option_type','c.title')
                                                 ->orderBy('pol.option_type')
                                                 ->get();

  }

  public function scopeVisible($query){
//    if($_SERVER['REMOTE_ADDR']!='91.151.130.78' && $_SERVER['REMOTE_ADDR']!='10.0.6.187'){
//      $query->where('products.visibility','=',1);
//    }
     
  }
     	 
}
