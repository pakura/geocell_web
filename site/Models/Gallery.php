<?php namespace Site\Models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model{

	protected $table='galleries';
	
	 
	 
	public static function getItem($col_id){

		return self::where('collection_id', '=', $col_id)
					->where('c.language','=',config('app.locale'))
					->join('galleries_content as c','c.galleries_id','=','galleries.id')
					->select('galleries.*','c.*')
					->groupBy('galleries_id')
					->orderBy('galleries.position')
					->get();
	}
  	 
    public function scopeOrderByCreationDate($query,$direction){

        $query->orderBy($this->table.'.creation_date', $direction);
    }


    public function scopeWithContent($query)
    {
        $query->join('galleries_content as c','c.galleries_id','=','galleries.id')
              ->where('c.language','=',\App::getLocale())  
              ->select('galleries.*','c.title','c.description','c.file');
    }

	public function scopeYear($query,$year=0){
      if($year) {
        $query->where(\DB::raw('date_format('.$this->table.'.`creation_date`,"%Y")'),'=',$year);
      }
  } 
  public function scopeMonth($query,$month=0){
      if($month) {
        $query->where(\DB::raw('date_format('.$this->table.'.`creation_date`,"%c")'),'=',$month);
      }
  }

  public function getFirstYear(){
       
       $row= $this->select(\DB::raw('min(date_format('.$this->table.'.`creation_date`,"%Y")) as FirstYear'))->first();
       return $row->FirstYear;
      
  }

  public function getCreationDateAttribute($creation_date)
  {
      return date("d F Y", strtotime($creation_date));
  
  } 

}
