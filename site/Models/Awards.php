<?php namespace Site\Models;

use Illuminate\Database\Eloquent\Model;

class Awards extends Model{

	protected $table='awards';
	
    public function scopeWithContent($query)
    {
        $query->join('awards_content as c','c.award_id','=','awards.id')
              ->where('c.language','=',\App::getLocale())  
              ->select('awards.*','c.title','c.description','c.content');
    }
   
}
