<?php namespace Site\Models;

class GeocellId{

	public $base_url = 'https://mobile.geocell.ge/geocellid/ge/'; 
	
  public function _alterNumber($phone,$test_portal=false){
      $str =$test_portal?'&test_portal=1':''; 
    return $this->getJsonData('alterportalnumber?number='.$phone.$str);
  }

   public function _forgotPassword($phone){
    return $this->getJsonData('forgotpassword?number='.$phone);
  }
    
    
	public function _alterSms($phone,$sms){    
    return $this->getJsonData('altersms?number='.$phone.'&sms='.$sms);
  } 

  public function _setPassword($phone,$sms,$pass){    
    return $this->getJsonData('setpassword?number='.$phone.'&sms='.$sms.'&pass='.$pass);
  } 
  public function _getStatus($token){
     return $this->getJsonData('getStatus?token='.$token);
  } 

  public function getInfo($token){
    return $this->getJsonData('getinfo?token='.$token);
  }

  public function isAuthed(){
    
    $auth = $this->_getStatus(\Session::get('site_auth_token'));
    return $auth->success==1?true:false;
  }
   
  public function forgetUserSessionData(){
    \Session::forget('site_user');
    \Session::forget('phone');
    \Session::forget('site_auth_token');
    \Session::forget('userInfo');
    \Session::forget('userServices');
    \Session::forget('availableServices');
    \Session::forget('check4G');
    \Session::forget('fullauth');
    \Session::forget('url');
  }
  public function _requestSaveSms(){
    
  } 

  public function _saveInfo(){
    
  } 

  public function _logOut($token){
     return $this->getJsonData('logout?token='.$token);
  }

  private function getJsonData($url){
    return  json_decode(file_get_contents($this->base_url.$url));
  }

  
     	 
}
