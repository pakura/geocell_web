<?php namespace Site\Models;

use Illuminate\Database\Eloquent\Model;

class Offices extends Model{

	protected $table='offices';
	
	 
    public static function getList($lang){
        return self::leftjoin('dictionary_cities as d','offices.city_id','=','d.id')
                    ->where('b2b','=',0)
                    ->select('*','address_'.$lang.' as address','info_'.$lang.' as info','d.name_'.$lang.' as city', 'offices.note_'.$lang.' as note', 'offices.id as office_id')
                    ->orderby('city')->orderby('address')
                    ->get();
    }

    public static function getListB2b($lang){
    	return self::leftjoin('dictionary_cities as d','offices.city_id','=','d.id')
                    ->where('b2b','=',1)
    				->select('*','address_'.$lang.' as address','info_'.$lang.' as info', 'offices.note_'.$lang.' as note', 'd.name_'.$lang.' as city')
                    ->orderby('city')->orderby('address')
    	            ->get();
    }

    public static function cities($lang){
    	return \DB::table('dictionary_cities')->select('id','name_'.$lang.' as city_name')->orderby('city_name')->get();
    }
 
}
