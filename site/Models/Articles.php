<?php namespace Site\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Articles extends Model{

	protected $table='articles';
	
	 
  public static function getItem(){

     
   }


  public function scopeWithContent($query)
  {
        $query->join('articles_content as c','c.article_id','=','articles.id')
              ->join('pages as pg','articles.collection_id','=','pg.attached_collection_id')
              ->leftJoin('articles_attachments as at','articles.id','=','at.article_id')
              ->where('c.language','=',\App::getLocale())
              ->select('articles.*','pg.full_slug as page_slug','c.title','c.description','c.content', 'at.file');
  }
  
  public function scopeOrderById($query,$direction){

        $query->orderBy($this->table.'.id', $direction);
  }

  public function scopeYear($query,$year=0){
      if($year) {
        $query->where(\DB::raw('date_format('.$this->table.'.`creation_date`,"%Y")'),'=',$year);
      }
  } 
  public function scopeMonth($query,$month=0){
      if($month) {
        $query->where(\DB::raw('date_format('.$this->table.'.`creation_date`,"%c")'),'=',$month);
      }
  }

  public function getFirstYear(){
       
       $row= $this->select(\DB::raw('min(date_format('.$this->table.'.`creation_date`,"%Y")) as FirstYear'))->first();
       return $row->FirstYear;
      
  }

  public function getCreationDateAttribute($creation_date)
  {
      return date("d F Y", strtotime($creation_date));
  
  } 


  	 
}
