<?php namespace Site\Models;

use Illuminate\Database\Eloquent\Model;

class MMS extends Model{

	protected $table='mms_abroad';
	
    
    public function scopeName($query){
           $query->select("*","country_".\App::getlocale()." as country");
    }
  
}
