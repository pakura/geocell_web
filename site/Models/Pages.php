<?php namespace Site\Models;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model{
	
 
    /*get group items*/

    public static function getList($group_id){
        return self::where('group_id','=',$group_id)
                    ->leftjoin('collections as c','pages.attached_collection_id','=','c.id')
                    ->select('pages.*','c.id as cid','c.generic_title as ctitle','c.collection_type as coll_type')
                    ->orderBy('position')->get();
    }
   
    public function scopeWithContent($query)
    {
        $query->join('pages_content as c','c.page_id','=','pages.id')
              ->whereLanguage(\App::getLocale())  
              ->select('pages.*','c.title','c.content','c.description','c.meta_content','c.meta_description');
    }   
	

    public function scopeChildren($query,$parent_id)
    {
        $query->where('parent_id','=',$parent_id);
               
    }
    public function scopeAttachment($query)
    {
        $query->join('pages_content as c','c.page_id','=','pages.id')

              ->leftjoin('pages_attachments as a', function ($join) {

                  $join->on('a.page_id','=','pages.id')
                       ->where('a.file_type','=',1);
                })
              ->where('c.language','=',\App::getLocale())  
              ->select('pages.*','c.title','c.content','c.description','c.meta_content','c.meta_description','a.file');
               
    }
    
    public static function getSlugById($id) {
        $page= self::where('id',$id)->first();
        return $page->full_slug;
    }

    public static function getByGroupID($group_id){
        return self::whereGroupId($group_id)->where('pages.visibility','=',1)->WithContent()->orderby('position')->get();
    }


}
