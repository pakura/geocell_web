<?php namespace Site\Models;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model{

	protected $table='banners';
	
    public function scopeWithContent($query)
    {
        $query->join('banners_content as c','c.banner_id','=','banners.id')
              ->where('c.language','=',\App::getLocale())  
              ->select('banners.*','c.title','c.description');
    }
   
}
