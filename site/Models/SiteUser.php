<?php namespace Site\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SiteUser extends Model{

	protected $table='site_users';
	
	protected $fillable = ["first_name","last_name","birth_date","personal_number","address","address2","city","city2","address2","district2","district","email","phone","birth_date"];

  	 
}
