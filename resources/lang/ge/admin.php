<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| admin panel static text translations
	|
	*/

	"home"              => "Home",
	"logout"            => "Logout",
	"settings"          => "Settings",

	"collections"       => "Collections Management",
	"coll_6"            => "News & Articles",
	"coll_4"            => "Stock",
	"coll_8"            => "Galleries & Banners",
	"coll_12"           => "Faq",
	"coll_13"           => "Guestbook",
	"coll_15"           => "Custom Lists",
	"coll_16"           => "Galleries & Banners",
	"coll_sub_6"        => "News Management",
	"coll_sub_4"        => "Product Categories",
	"coll_sub_8"        => "Gallery Management",
	"coll_sub_12"       => "Faq Management",
	"coll_sub_13"       => "Guestbook Management",
	"coll_sub_15"       => "Custom Lists Management",
	"coll_sub_16"       => "Banners Management",
	"collection"        => "Collection",
	"add_collection"    => "Add Collection",
	"attach"            => "Attach Collection",
	"attached"          => "Attached Collection",

	"question"          => "Question",
	"answer"            => "Answer",

	"news"              => "News Management",
	"addnews"           => "Add",
	"updatenews"        => "Update article",
	"generic"           => "Generic title",
	"title"             => "Title",
	"apages"            => "Atached to pages",
	"atpages"           => "Atach to page",
	"actions"           => "Actions",
	
	"logo"              => "Logo",
	"brand_url"         => "Brand url",

	"file"              => "File",
	"title_file"        => "Title file",
	"title_mobile"      => "Mobile version file",
	"embeded"           => "Embeded link",
	"galleries_type"    => "Galleries type",

	"addproduct"        => "Add product", 
	"options"           => "Options", 
	"updateproduct"     => "Update product", 
	"sku"               => "Stock keeping unit (Sku)", 
	"pinned"            => "Pinned", 
	"home_page"         => "Home page", 
	
	"contact_phone"     => "Contact phone", 
	"contact_email"     => "Contact email", 
	"address"           => "Address", 

	"email"             => "Email", 
	"name"              => "Name", 
    
	"slug"              => "Slug",
	"visibility"        => "Visibility",
	"open_blank"        => "Open in new tab",
	"short_title"       =>"Short title", 
	"description"       =>"Description", 
	"content"           =>"Content", 
	"meta_description"  =>"Meta description", 
	"meta_content"      =>"Meta content", 
	"create"            =>"Publish date", 
	"expire"            =>"Expire date", 
	
	"add"               => "Add",
	"save"              => "Save",
	"edit"              => "Edit",
	"delete"            => "Delete",
	"cancel"            => "Cancel",
	"saveandclose"      => "Save & close",

	"files"             => "Files",
	"photo"             => "Photo",
	"general"           => "General",
	"prices"            => "Prices",
	"price"             => "Price",
	"currency"          => "Currency",
	"type"              => "Type",
	"amount"            => "Amount",
	"mprice"            => "Main price",
	"aprice"            => "Additional price",

	"addphoto"          =>"Default photo has been added",
	"removephoto"       =>"Default photo has been removed",
	"treeupdated"       =>"Items have been updated",
	"updated"           =>"Data has been updated",
    
    "add_group"         =>"Add group",  
    "redirect"          =>"Redirect link",  
    "link"              =>"Link",  
    "page_type"         =>"Page type",  
    "apply"             =>"Apply",  
    
    "initial"          =>"Initial",
    "key"              =>"Key",
    "value"            =>"Value",
    "controller"       =>"Controller",
    "choosecontroller" =>"Choose controller",

    "txt-input"        =>"English input:",
    "txt-output"       =>"Georgian output",
    "convert"          =>"Convert",

    "first_name"       =>"First name",
    "last_name"		   =>"Last name",
    "phone"            =>"Phone",
    "email"            =>"Email",
    "password"         =>"Password",
    "password_confirmation" =>"Password confirmation",
    "add_role"         =>"Add role",
    "role_name"        =>"Role name",
    "roles"            =>"Roles",
    "selectroles"      =>"Attach role",
    "choose_permission"      =>"Choose permissions",
    "permissions"      =>"Permissions",
    "acces_type"       =>"Acces type",
    "allgroups"        =>"All Page groups",
    "allcolls"         =>"All collections",
    "addsub"           =>"Add sub page",
    "preview"          =>"Preview",

    "deletealert"      =>"Are you sure you want to delete the item?",
    "deletealert"      =>"Are you sure you want to delete the item?",
    "terms_and_conditions"      =>"Terms and conditions",
    "services_and_features"      =>"Services and features",

    "service_activation_msg"      =>"Service activation text",
    "service_activation_msg2"      =>"Services deactivation text",

];
