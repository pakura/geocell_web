<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Admin menu list translations
	| 
	*/

	"dashboard"          => "Dashboard",
	"reports"            => "Reports",
	"newsletter"         => "Newsletters",
	"pre-order"         => "Pre-Orders",
    "pre-order"          => "Pre-Orders",
	"international"         => "International line",
	"charts"             => "Charts",

	"sitemap"           => "Sitemap",

	"about"             => "About",
	"awards"            => "Awards",

	"newsarticles"      => "News & Articles",
	"news"              => "News Management",

	"menu"              => "Menu Management",

	"faq"               => "Faq",
	"faqmanagement"     => "Faq Management",

	"guestbook"         => "Guestbook",
	"gbmanagement"      => "Guestbook Management",

	"custom_list"       => "Custom Lists",
	"custom_list_m"     => "Custom Lists Management",

	"gallbanners"       => "Galleries & Banners",
	"gallist"           => "Gallery Management",
	"banners"           => "Banner group list",

	"stock"             => "Stocks",
	"stocklist"         => "Stock list",
	"brands"            => "Brands",
	"prodcats"          => "Product Categories",
	"prodoptions"       => "Product Options",
	"iprodlist"         => "Import Product List",
	"eprodlist"         => "Export Product List",
	 

	"user"              =>"User management", 
	"cmsuser"           =>"CMS User List", 
	"cmsuserroles"      =>"CMS User Roles", 
	"cmsuserperm"       =>"CMS User Permissions", 
	"siteuser"          =>"Site User List", 
	
	"settings"          => "Settings",
	"sitesett"          => "Site Settings",
	"langdata"          => "Language Data",
	"textconv"          => "Text Converter",

	"tandt"            => "Tasks & Tickets",
	"ticket"           => "Ticket List",
	"chat"      	   => "Chat",
 
	"logs"             => "Logs",
	"cmslog"           => "CMS Log",
	"sitelog"      	   => "Site Log",
 

];
