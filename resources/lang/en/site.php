<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| site static text translations
	|
	*/

	"products"             => "Products",
	"viewallproduct"       => "View all products",
	 
	"latestnews"           => "Latest News",
	"viewallnews"          => "View all news",
	"prev"                 => "Previous",
	"next"                 => "Next",
	"ask"                  => "Ask question",
	"choosecat"            => "Choose Category",
	"activate"             => "Activate",
	"purchase"             => "Purchase",
	"buy"             	   => "Buy",
	"from_balance"         => "From Balance",
	"from_bank"            => "From Bank",
	"your_balance"         => "Your balance contains:",
	"wrong_sms_code"       => "Wrong SMS code",
	"wrong_pass"           => "Wrong password",
	
	"error_1001"           => "Unexpected error. Please Try again",	
	"error_1004_2"         => "Service is available only for Geocell subscribers",
	"error_1004_1"         => "Service is available only for Lailai subscribers",
	"error_1005"           => "Service is already activated",
	"error_1006"           => "The number you are activating pack didn't meet the service requirements",
	"error_1007"           => "Balance is not enough",
	"error_1027"           => "Pack activation will be possible from the next day",
	"error_1028"           => "For purchasing a new pack you need to consume existing one",
	"error_1029"           => "Unable to activate pack on specific number",
	"error_1030"           => "The number you are activating pack didn't meet the service requirements",
	"error_1031"           => "The number you are activating pack didn't meet the service requirements",
	"error_1033"           => "Pack deactivation will be possible from the next day",

	"unlimited"            => "Unlimited",
	"minutes"              => "Min",
	"seconds"              => "Sec",
	
	"deactivate_service"   => "Operation completed. Wait for SMS verification to deactivate the service",
	"deactivate_service_postpaid"   => "Operation completed. Service is deactivated",
	"remove_num"           => "Number was removed successfully",
	"not_geocell"          => "Number doesn’t belong to Geocell",
	"error_your_number"    => "Impossible to add your number",
	"only_b2c"             => "Service is available only for B2C users",
    "gcn_not_allowed"      => "Service is available only for B2C users",
    "brand1notAllow"      => "The service is temporarily restricted",


	"hello"                => "Hello",
	"more_info"            => "More information",
	"price_con"            => "Price & Conditions",
	"terms_con"            => "Terms & Condition",

	"month"                => "month",
	"year"                 => "year",
	"validity"             => "Validity:",
	"search"               => "Search",
	
	"south_america"        => "South America, the Pacific, Africa, Asia", 
	"cis_europe"           => "CIS, Europe, Asia, South Africa", 
	"russia_usa"           => "Russia, USA, Canada",
	"satellite_conn"       => "Satellite Connection",
	"no_discount"       => "No discount",
	"installment_alert"       => "Installment status",

	"months"               =>[
								'January', 
								'February', 
								'March', 
								'April', 
								'May', 
								'June', 
								'July', 
								'August', 
								'September', 
								'October', 
								'November', 
								'December'
							] 
];
