<!doctype html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie ie6"> <![endif]-->
<!--[if    IE 7]> <html lang="en" class="no-js ie ie7"> <![endif]-->
<!--[if    IE 8]> <html lang="en" class="no-js ie ie8"> <![endif]-->
<!--[if    IE 9]> <html lang="en" class="no-js    ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
  <head>
    <!-- TITLE -->
    <title><?php
          if(isset($item) && !isset($productPage)){
              echo 'Geocell - '.$item->title;
          } elseif (isset($product)){
              echo 'Geocell - '.$product->title;
          } elseif (isset($service)){
              echo 'Geocell - '.$service->title;
          } else {
              echo 'Geocell';
          }
      ?></title>
    <base href="{{config('app.url')}}" />  
    <!-- META -->
    <meta charset="utf-8">
    <meta name="robots" content="nofollow" />
    <meta name="keywords" content="<?php
    if(isset($item) && !isset($productPage)){
      echo $item->meta_content;
    } elseif (isset($product)){
      echo $product->meta_content;
    } elseif (isset($service)){
      echo $service->meta_content;
    } else {
      echo 'Geocell';
    }
    ?>">
    <meta name="description" content="<?php
    if(isset($item) && !isset($productPage)){
      echo $item->meta_description;
    } elseif (isset($product)){
      echo $product->meta_description;
    } elseif (isset($service)){
      echo $service->meta_description;
    } else {
      echo 'Geocell';
    }
    ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="<?php
    if(isset($item) && !isset($productPage)){
        if(isset($item->file)){
            echo config('app.url').'uploads/pages/'.$item->file;
        } else {
            echo config('app.url').'site/tsr-components/tsr-favicon/favicon-180.png';
        }

    } elseif (isset($product)){
        if(isset($product->file)){
          echo config('app.url').'uploads/products/'.$product->file;
        } else {
          echo config('app.url').'site/tsr-components/tsr-favicon/favicon-180.png';
        }
    } elseif (isset($service)){
        if(isset($service->file)){
            echo config('app.url').'uploads/products/'.$service->file;
        } else {
            echo config('app.url').'site/tsr-components/tsr-favicon/favicon-180.png';
        }
    } else {
      echo config('app.url').'site/tsr-components/tsr-favicon/favicon-180.png';
    }
    ?>">
    <meta property="og:image" content="<?php
    if(isset($item) && !isset($productPage)){
      if(isset($item->file)){
        echo config('app.url').'uploads/pages/'.$item->file;
      } else {
        echo config('app.url').'site/tsr-components/tsr-favicon/favicon-180.png';
      }

    } elseif (isset($product)){
      if(isset($product->file)){
        echo config('app.url').'uploads/products/'.$product->file;
      } else {
        echo config('app.url').'site/tsr-components/tsr-favicon/favicon-180.png';
      }
    } elseif (isset($service)){
      if(isset($service->file)){
        echo config('app.url').'uploads/products/'.$service->file;
      } else {
        echo config('app.url').'site/tsr-components/tsr-favicon/favicon-180.png';
      }
    } else {
      echo config('app.url').'site/tsr-components/tsr-favicon/favicon-180.png';
    }
    ?>" />
    <meta property="og:title" content="<?php
    if(isset($item) && !isset($productPage)){
      echo $item->title;
    } elseif (isset($product)){
      echo $product->title;
    } elseif (isset($service)){
      echo $service->title;
    } else {
      echo 'Geocell';
    }
    ?>" />
    <meta property="og:description" content="<?php
    if(isset($item) && !isset($productPage)){
      echo $item->meta_description;
    } elseif (isset($product)){
      echo $product->meta_description;
    } elseif (isset($service)){
      echo $service->meta_description;
    } else {
      echo 'Geocell';
    }
    ?>"/>
    <meta property="og:url" content="<?php
            if(isset($item) && !isset($productPage)){
                echo config('app.url').$lang.'/'.$item->full_slug.'/';
            } elseif(isset($productPage)) {
                echo config('app.url').$lang.'/'.$item->full_slug.'/';
                echo isset($product)?$product->slug:'';
            } else {
              echo config('app.url');
            }
    ?>" />
    <meta property="og:site_name" content="ჯეოსელი" />
    <meta property="og:type" content="Website" />
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, IE=10">
    <meta http-equiv="Content-Language" content="en">
    <![endif]-->

    <!-- ICONS -->
    <link rel="icon"                                         href="site/tsr-components/tsr-favicon/favicon.png">
    <link rel="shortcut icon"                                href="site/tsr-components/tsr-favicon/favicon.png">
    <link rel="apple-touch-icon"                             href="site/tsr-components/tsr-favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76"   href="site/tsr-components/tsr-favicon/favicon-76.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="site/tsr-components/tsr-favicon/favicon-120.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="site/tsr-components/tsr-favicon/favicon-152.png">
    <link rel="apple-touch-icon-precomposed" sizes="180x180" href="site/tsr-components/tsr-favicon/favicon-180.png">


    <!-- CSS -->

    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="site/tsr-core/tsr-styles/tsr-reset.css">
    <link rel="stylesheet" href="site/tsr-core/tsr-styles/tsr-core.css">
    <link rel="stylesheet" href="site/tsr-core/tsr-styles/tsr-extensions/tsr-extensions.css">
    <link rel="stylesheet" href="site/css/product_info.css">
    <link rel="stylesheet" href="site/css/sumoselect.css">
    @if(!isset($homepage))
    <link rel="stylesheet" href="site/tsr-new-year-promo.css">
    @endif
    <!-- CSS3/JS POLYFILLS -->
    <!--[if lte IE 8]>
    <script src="tsr-core/tsr-scripts/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <script src="site/tsr-core/tsr-scripts/modernizr/modernizr.custom.min.js"></script>
    <script src="site/tsr-core/tsr-scripts/matchmedia/matchMedia.js"></script>
    <script src="site/tsr-core/tsr-scripts/matchmedia/matchMedia.addListener.js"></script>
    <script src="site/tsr-core/tsr-scripts/enquire/enquire.min.js"></script>

      <!--[if lt IE 9]>
    <script src="tsr-core/tsr-scripts/jquery/jquery-legacy.js"></script>
    <![endif]-->
    <!--[if gte IE 9]><!-->
    <script src="site/tsr-core/tsr-scripts/jquery/jquery.js"></script>
    <!--<![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <!-- IE FALLBACK -->
    <!--[if lte IE 7]>
    <link rel="stylesheet" type="text/css" href="tsr-components/tsr-msie-splash/tsr-msie-splash.css" />
    <script src="tsr-components/tsr-msie-splash/tsr-msie-splash.js"></script>
    <![endif]-->
    <script src='https://www.google.com/recaptcha/api.js?hl=<?=$lang=="en"?"en":"ka"?>'></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
     
      ga('create', 'UA-69486669-1', 'auto');
      ga('send', 'pageview');
     
    </script>
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({appId: '159740984475696', status: true, cookie: true,
          xfbml: true});
      };
      (function() {
        var e = document.createElement('script'); e.async = true;
        e.src = document.location.protocol +
                '//connect.facebook.net/en_US/all.js';
        document.getElementById('fb-root').appendChild(e);
      }());
    </script>

    {{--<script>--}}
      {{--!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?--}}
              {{--n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;--}}
        {{--n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;--}}
        {{--t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,--}}
              {{--document,'script','https://connect.facebook.net/en_US/fbevents.js');--}}

      {{--fbq('init', '117412298672089');--}}
      {{--fbq('track', "PageView");--}}
      {{--fbq('track', 'ViewContent');--}}
    {{--</script>--}}

    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=117412298672089&ev=PageView&noscript=1"
      /></noscript>
    <!-- End Facebook Pixel Code -->

  </head>
  <body @if(\Request::segment(2) == 'business') class="tsr-body-b2b" @endif>

    @include('site.header')

    @yield('content')



     @include('site.footer-banners')
     @include('site.footer')


   
     <!-- *************** JAVASCRIPTS *************** -->
    

    <script src="site/tsr-core/tsr-scripts/jquery-easing/jquery.easing.min.js"></script>
    <script src="site/tsr-core/tsr-scripts/jquery-placeholder/jquery.placeholder.js"></script>
    <!--products slder-->
    <script src="site/tsr-core/tsr-scripts/jquery-slideshow/jquery.slideshow.min.js"></script>
    <script src="site/tsr-sections/tsr-slideshow/tsr-slideshow.js"></script>
    <!-- ********** -->
    <script src="site/tsr-core/tsr-scripts/tsr-core.js"></script>
    <script src="site/tsr-sections/tsr-header/tsr-header.js"></script>
    <script src="site/tsr-core/tsr-scripts/tsr-handles.js"></script>

    <script src="site/tsr-components/tsr-forms/tsr-forms.js"></script>
    <script src="site/tsr-modules/tsr-tabular/tsr-tabular.js"></script>

    <script src="site/tsr-sections/tsr-footer/tsr-footer.js"></script>
    <script src="site/tsr-sections/tsr-hero/tsr-hero.js"></script>
    <script src="site/tsr-sections/tsr-ps-listing/tsr-ps-listing.js"></script>
    <script src="site/tsr-sections/tsr-news-listing/tsr-news-listing.js"></script>
    <script src="site/tsr-sections/tsr-refiner/tsr-refiner.js"></script>
    <script src="site/tsr-sections/tsr-support-listing/tsr-support-listing.js"></script>

    <script src="site/tsr-sections/tsr-support-listing/tsr-support-listing.js"></script>

    <script src="site/tsr-core/tsr-scripts/jquery-fancybox/jquery.fancybox.patched.min.js"></script>
     <script src="site/tsr-core/tsr-scripts/jquery-inputmask/jquery.inputmask.min.js"></script>



     <script src="site/tsr-sections/tsr-uboard/tsr-uboard-services.js"></script>

     <script src="site/tsr-core/tsr-scripts/jquery-tooltipster/jquery.tooltipster.patched.min.js"></script>
     <script src="site/tsr-sections/tsr-faq-listing/tsr-faq-listing.js"></script>

     <script src="site/tsr-sections/tsr-attention/tsr-attention.js"></script>
   <script type="text/javascript" src="site/tsr-core/tsr-scripts/jquery-inputmask/extensions/inputmask.regex.extensions.min.js"></script>
    <script src="site/js/jquery.caret.js"></script>
    <script src="site/js/custom.js"></script>
    <script src="site/js/jquery.sumoselect.js"></script>

    <!-- *************** /JAVASCRIPTS *************** -->



    <script type="text/javascript">/*<![CDATA[*/
      $(document).ready(function() {

        /* tooltips */
       /* (function() {
          $('.js-tooltip-text')
          .css({'cursor': 'pointer'})
          .tooltipster({
            'maxWidth': 260,
            'position': 'top'
          });
        })();*/

        /* tooltips */
       (function() {
          var $container = $('.tsr-uboard-myprods');
          $('.tsr-uboard-myprod-serv-ttip', $container).each(function() {
            $('.tsr-uboard-myprod-serv-info', $(this).closest('.tsr-uboard-myprod-serv'))
            .css({'cursor': 'pointer'})
            .attr('title', $(this).html())
            .tooltipster({
              'maxWidth': 260,
              'contentAsHTML': true,
              'position': 'top-right',
              'autoClose': true,
              'interactive': true
            });
          });
        })();

       

        /* avatar */
    
          var fancyboxSpeed  = 150;
          var fancyboxEffect = $('html').hasClass('touch') ? 'none' : 'elastic';
          $('.js-fancybox').fancybox({
            'fitToView'     : false,
            'autoSize'      : false,
            'width'         : '100%',
            'height'        : 'auto',
            'maxWidth'      : 720,
            'scrolling'     : 'visible',
            'openSpeed'     : fancyboxSpeed,
            'closeSpeed'    : fancyboxSpeed,
            'prevSpeed'     : fancyboxSpeed,
            'nextSpeed'     : fancyboxSpeed,
            'openEffect'    : fancyboxEffect,
            'closeEffect'   : fancyboxEffect,
            'prevEffect'    : fancyboxEffect,
            'nextEffect'    : fancyboxEffect,
            'margin'        : 0,
            'padding'       : 0,
            'beforeShow'    : function() {
              window.tsrForms.tsrInit();
            },
            'afterShow'     : function() {
              $('.fancybox-close').attr('title', null);
            },
            'helpers'       : {
              'overlay'       : { 'closeClick': false }
            }
          });
     

        /* promo */
     
       /*   var $check = $('.tsr-uboard-balance-side-check');
          var $layer = $('.tsr-uboard-balance-side-layer');
          $check.on('click', function() {
            if ($layer.is(':visible')) {
              $layer.hide();
            } else {
              $layer.show();
            }
          });*/
         
          /* promo */
        (function() {
          var $check = $('.tsr-uboard-balance-side-check');
          var $layer = $('.tsr-uboard-balance-side-layer');
          var timeoutID = null;
          var showLayer = function() {
            if (timeoutID) {
              window.clearTimeout(timeoutID);
            }
            $layer.stop(true, false).fadeIn(300);
            timeoutID = window.setTimeout(function() {
              $check.trigger('click');
            }, 5000);
          };
          var hideLayer = function() {
            if (timeoutID) {
              window.clearTimeout(timeoutID);
            }
            $layer.stop(true, false).fadeOut(300);
          };
          $check.on('click', function(event) {
            if (!$layer.is(':visible')) {
              showLayer();
            } else {
              hideLayer();
            }
            event || event.preventDefault();
          });
        })();
        

        /* tooltips */
       
          $('.js-tooltip-text')
          .css({'cursor': 'pointer'})
          .tooltipster({
            'maxWidth': 260,
            'position': 'top'
          });
       
      
        /*roaming*/
        var $alpha = $('#js-countries-alpha');
        $('a', $alpha).on('click', function(event) {
          tsrCore.scrollTo(this.hash, 300, -20);
          event.preventDefault();
        });
        
        var $nav = $('#js-countries-nav');
        var $links = $('.js-cgroup');
        $('a', $nav).on('click', function(event) {
          $('a', $nav).removeClass('is-choosen');
          $(this).addClass('is-choosen');
          var cname = 'js-cgroup-' + $(this).attr('data-js-cgroup');
          $links.each(function() {
            var $this = $(this);
            $this.css('opacity', $this.hasClass(cname) ? '' : '0.33');
          });
          event.preventDefault();
        }).first().trigger('click');

          
         
        $('.js-local-tarif-collapse').each(function() {
          var $show = $('.js-local-tarif-collapse-show', this).show();
          var $hide = $('.js-local-tarif-collapse-hide', this).hide();
          var $cont = $('.js-local-tarif-collapse-content', this).hide();
          $show.on('click', function(event) {
            $show.hide();
            $hide.show();
            $cont.slideDown(300, 'easeOutExpo');
            event.preventDefault();
          });
          $hide.on('click', function(event) {
            $show.show();
            $hide.hide();
            $cont.slideUp(300, 'easeOutExpo');
            event.preventDefault();
          });
        }); /*end roaming*/
       


      });
     </script>

   <script src="site/js/site.js"></script>
 </body>
</html>
<?php


?>