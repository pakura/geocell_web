<!doctype html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie ie6"> <![endif]-->
<!--[if    IE 7]> <html lang="en" class="no-js ie ie7"> <![endif]-->
<!--[if    IE 8]> <html lang="en" class="no-js ie ie8"> <![endif]-->
<!--[if    IE 9]> <html lang="en" class="no-js    ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
  <head>
    <!-- TITLE -->
    <title>Geocell</title>

    <!-- META -->
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="tsr-components/tsr-favicon/favicon-144.png">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, IE=10">
    <meta http-equiv="Content-Language" content="en">
    <![endif]-->

    <!-- ICONS -->
    <link rel="icon"                                         href="/site/tsr-components/tsr-favicon/favicon.png">
    <link rel="shortcut icon"                                href="/site/tsr-components/tsr-favicon/favicon.png">
    <link rel="apple-touch-icon"                             href="/site/tsr-components/tsr-favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76"   href="/site/tsr-components/tsr-favicon/favicon-76.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/site/tsr-components/tsr-favicon/favicon-120.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/site/tsr-components/tsr-favicon/favicon-152.png">
    <link rel="apple-touch-icon-precomposed" sizes="180x180" href="/site/tsr-components/tsr-favicon/favicon-180.png">

    <!-- CSS -->
    <link rel="stylesheet" href="/site/tsr-core/tsr-styles/tsr-reset.css">
    <link rel="stylesheet" href="/site/tsr-core/tsr-styles/tsr-core.css">

    <!-- CSS3/JS POLYFILLS -->
    <!--[if lte IE 8]>
    <script src="tsr-core/tsr-scripts/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <script src="/site/tsr-core/tsr-scripts/modernizr/modernizr.custom.min.js"></script>
    <script src="/site/tsr-core/tsr-scripts/matchmedia/matchMedia.js"></script>
    <script src="/site/tsr-core/tsr-scripts/matchmedia/matchMedia.addListener.js"></script>
    <script src="/site/tsr-core/tsr-scripts/enquire/enquire.min.js"></script>


    <!-- IE FALLBACK -->
    <!--[if lte IE 7]>
    <link rel="stylesheet" type="text/css" href="tsr-components/tsr-msie-splash/tsr-msie-splash.css" />
    <script src="tsr-components/tsr-msie-splash/tsr-msie-splash.js"></script>
    <![endif]-->
  </head>
  <body>
 
	@yield('content')



	 @include('site.footer')
	 <!-- *************** JAVASCRIPTS *************** -->
    <!--[if lt IE 9]>
    <script src="tsr-core/tsr-scripts/jquery/jquery-legacy.js"></script>
    <![endif]-->
    <!--[if gte IE 9]><!-->
    <script src="/site/tsr-core/tsr-scripts/jquery/jquery.min.js"></script>
    <!--<![endif]-->
    <script src="/site/tsr-core/tsr-scripts/jquery-easing/jquery.easing.min.js"></script>
    <script src="/site/tsr-core/tsr-scripts/jquery-placeholder/jquery.placeholder.js"></script>
    <script src="/site/tsr-core/tsr-scripts/jquery-flexslider/jquery.flexslider.min.js"></script>
    <!-- ********** -->
    <script src="/site/tsr-core/tsr-scripts/tsr-core.js"></script>
    <script src="/site/tsr-sections/tsr-header/tsr-header.js"></script>
    <script src="/site/tsr-sections/tsr-footer/tsr-footer.js"></script>
    <script src="/site/tsr-sections/tsr-hero/tsr-hero.js"></script>
    <script src="/site/tsr-sections/tsr-ps-listing/tsr-ps-listing.js"></script>
    <script src="/site/tsr-sections/tsr-news-listing/tsr-news-listing.js"></script>
    <script src="/site/tsr-sections/tsr-support-listing/tsr-support-listing.js"></script>
    <!-- *************** /JAVASCRIPTS *************** -->


 </body>
</html>
