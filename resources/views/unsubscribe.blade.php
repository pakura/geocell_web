<!doctype html>
<html lang="en">
<head>
    <!-- TITLE -->
    <title>Geocell</title>

    <!-- META -->
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="tsr-components/tsr-favicon/favicon-144.png">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, IE=10">
    <meta http-equiv="Content-Language" content="en">
    <![endif]-->



    <!-- ICONS -->
    <link rel="icon"                                         href="/site/tsr-components/tsr-favicon/favicon.png">
    <link rel="shortcut icon"                                href="/site/tsr-components/tsr-favicon/favicon.png">
    <link rel="apple-touch-icon"                             href="/site/tsr-components/tsr-favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76"   href="/site/tsr-components/tsr-favicon/favicon-76.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/site/tsr-components/tsr-favicon/favicon-120.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/site/tsr-components/tsr-favicon/favicon-152.png">
    <link rel="apple-touch-icon-precomposed" sizes="180x180" href="/site/tsr-components/tsr-favicon/favicon-180.png">
    <!-- CSS -->
    <link rel="stylesheet" href="/site/tsr-core/tsr-styles/tsr-reset.css">
    <link rel="stylesheet" href="/site/tsr-core/tsr-styles/tsr-core.css">
</head>
<body>





<div class="tsr-section-custom-layout tsr-scl-subscribe">
    <div class="tsr-container">
        <div class="tsr-com-custom-layout-logo"><a href="#"><img alt="Geocell" src="/site/tsr-sections/tsr-custom-errors/logo-en.png"></a></div>
        <div class="tsr-scl-subscribe-content">
            <div class="cs-spb-20"><h1 class="tsr-title">@if($lang == 'en') Unsubscribe @else გამოწერის შეწყვეტა @endif</h1></div>
            <div class="cs-spb-40">
                @if($lang == 'en')
                    Click unsubscribe if you want to stop receiving offers and news from Geocell
                @else
                    გსურთ შეწყვიტოთ შეთავაზებების და სიახლეების მიღება?
                @endif
            </div>
            <div class="cs-spb-20"><!--
          --><a href="/developer_version/public/{{$lang}}/unsub/{{$email}}" class="cs-spb-10 cs-spr-10 tsr-btn tsr-btn-turquoise">
                    @if($lang == 'en')
                        Unsubscribe
                    @else
                        შეწყვეტა
                    @endif
                </a><!--
          --><a href="/" class="cs-spb-10 cs-spr-10 tsr-btn tsr-btn-gray">
                    @if($lang == 'en')
                        Cancel
                    @else
                        გაუქმება
                    @endif</a><!--
        --></div>
        </div>
    </div>
</body>
</html>
