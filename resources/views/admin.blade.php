<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	
	<title>Admin Panel</title>
    <base href="{{config('app.url')}}" /> 
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="admin/css/fonts/linecons/css/linecons.css">
	<link rel="stylesheet" href="admin/css/fonts/fontawesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="admin/css/bootstrap.css">
	<link rel="stylesheet" href="admin/css/xenon-core.css">
	<link rel="stylesheet" href="admin/css/xenon-forms.css">
	<link rel="stylesheet" href="admin/css/xenon-components.css">
	<link rel="stylesheet" href="admin/css/xenon-skins.css">
	<link rel="stylesheet" href="admin/css/fancybox.css" media="screen" />
	<link rel="stylesheet" href="admin/js/dropzone/css/dropzone.css">
	<link rel="stylesheet" href="admin/css/nested.css">
	<link rel="stylesheet" href="admin/css/custom.css">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="//cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css">


	 <script type="text/javascript">
    	var cms_slug = "<?= config('app.cms_slug');?>";
    </script> 

	<script src="admin/js/jquery-1.11.1.min.js"></script>

	<script src="//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
	<script src="//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
	<script src="//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	
</head>
<body class="page-body">

	
 
<div class="page-container">

	<div class="sidebar-menu toggle-others fixed collapsed" style="">
				
		<div class="sidebar-menu-inner ps-container">

			@include('admin/menu')
	    </div>
	</div>
    
	<div class="main-content" style="">
	    @include('admin/topmenu')

 		@yield('content')
    </div>		

</div>

    <div class="modal fade" id="modal-7" data-collid="" data-colltype="">
		<div class="modal-dialog">
			<div class="modal-content">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Pages list</h4>
				</div>
				
				<div class="modal-body">
				
					Content is loading...
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
					<!-- <button type="button" class="btn btn-info">Save changes</button> -->
				</div>
			</div>
		</div>
	</div>

	<!-- Scripts -->
	
	<!-- Bottom Scripts -->
	<script src="admin/js/bootstrap.min.js"></script>
	<script src="admin/js/tagsinput/bootstrap-tagsinput.min.js"></script>
	

	<script src="admin/js/TweenMax.min.js"></script>
	<script src="admin/js/resizeable.js"></script>
	<script src="admin/js/joinable.js"></script>
	<script src="admin/js/xenon-api.js"></script>
	<script src="admin/js/xenon-toggles.js"></script>
    <script src="admin/js/toastr/toastr.min.js"></script>
    <!--input mask-->
    <link rel="stylesheet" href="admin/js/daterangepicker/daterangepicker-bs3.css"> 
    <!--<script src="/admin/js/inputmask/jquery.inputmask.bundle.js"></script> -->
    <script src="admin/js/daterangepicker/daterangepicker.js"></script>
    <script src="admin/js/datepicker/bootstrap-datepicker.js"></script>
    <script src="admin/js/timepicker/bootstrap-timepicker.min.js"></script>

 
    <script type="text/javascript" src="admin/js/jquery.fancybox.js"></script>

    <!--nested items-->
    
     <script src="admin/js/jquery-ui/jquery-ui.min.js"></script> 
     <script src="admin/js/jquery.nestable.js"></script>
    <!--file upload-->
    <script src="admin/js/dropzone/dropzone.min.js"></script>
    <!-- tinymce -->
	<script type="text/javascript" src="admin/js/tinymce/tinymce.min.js"></script>

	<script type="text/javascript">
	tinymce.init({
	    selector: ".textarea",
	     plugins: [
	        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
	        "searchreplace wordcount visualblocks visualchars code fullscreen",
	        "insertdatetime media nonbreaking save table contextmenu directionality",
	        "emoticons template paste textcolor colorpicker textpattern"
	    ],
	    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons",
	    image_advtab: true,
	    templates: [
	        {title: 'Services "Meti"', url: 'admin/templates/services-meti.html'},
	        {title: 'tsr-services-lailai', url: 'admin/templates/tsr-services-lailai.html'},
	        {title: 'tsr-services-inet-bundles', url: 'admin/templates/tsr-services-bundles.html'},
	        {title: 'tsr-services-Blackberry', url: 'admin/templates/tsr-services-blackberry.html'},
	        {title: 'tsr-services-triple-0', url: 'admin/templates/tsr-services-triple-0.html'},
	        {title: 'Modem tariff plans', url: 'admin/templates/product-modem-tariff-plans.html'},
	        {title: 'Group SMS', url: 'admin/templates/tsr-services-group-sms.html'},
	    ],
	    
	    relative_urls:false,
	    remove_script_host:false,
		external_filemanager_path:"{{config('app.url')}}admin/js/ResponsiveFilemanagerMaster/filemanager/",
		filemanager_title:"Filemanager" ,
		external_plugins: { "filemanager" : "{{config('app.url')}}admin/js/ResponsiveFilemanagerMaster/filemanager/plugin.min.js"}
	 });
	</script>
    
    <script type="text/javascript">
     
        // myDropzone.on("error", function(file, message) { alert(message); });
        /*Dropzone.autoDiscover = false;
          myAwesomeDropzone = new Dropzone("div.dropzone", {
			  init: function() {
			    this.on("addedfile", function(file) {
			      console.log("added file");
			    });
			    this.on("success", function(file) {
			      console.log("successfully uploaded file");
			    });
			  }
			}); */

		 $('.iframe-btn').fancybox({	
			'width'		: 900,
			'height'	: 600,
			'type'		: 'iframe',
		    'autoScale'    	: false
		 });
   </script>

	<!-- JavaScripts initializations and stuff -->
	<script src="admin/js/xenon-custom.js"></script>
  

	<script src="admin/js/admin.js"></script>
</body>
</html>
