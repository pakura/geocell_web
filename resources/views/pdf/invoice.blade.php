<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        body { font-family: DejaVu Sans, sans-serif; }
    </style>
    <title>Invoices</title>
</head>
<body>
<div style="width: 100%; text-align: center;">
    <h3>@if($lang == 'en') Indebtedness @else საგადასახადო ანგარიში @endif</h3>
    <h5>@if($lang == 'en') calculation period @else პერიოდი @endif: {{date('01-m-Y', strtotime($invoice->BILL_PERIOD))}} - {{date('t-m-Y', strtotime($invoice->BILL_PERIOD))}}</h5>
</div>
<table>
    <tr>
        <td style="width: 50%">
            <div style="width: 100%; height: 200px; border: 1px solid #000; margin: 3%; float: left; font-size: 12px">
                @if($lang == 'en') Subscriber:
                    {{isset($invoice->listinfo[0]->person_name_eng)?$invoice->listinfo[0]->person_name_eng:''}}
                @else აბონენტი:
                    {{isset($invoice->listinfo[0]->person_name_geo)?$invoice->listinfo[0]->person_name_geo:''}}
                @endif

                <br>
                @if($lang == 'en')
                    Delivery: www.geocell.ge {{isset($invoice->listinfo[0]->person_name_eng)?$invoice->listinfo[0]->person_name_eng:''}}
                @else
                    მიწოდება: www.geocell.ge {{isset($invoice->listinfo[0]->person_name_geo)?$invoice->listinfo[0]->person_name_geo:''}}
                @endif
            </div>
        </td>
        <td style="width: 50%">
            <div style="width: 100%; height: 200px; border: 1px solid #000; margin: 3%; font-size: 14px; float: right; font-size: 12px">
                @if($lang == 'en')
                Bill can be paid at Geocell customer service centers in Tbilisi and Regions, as well as in branch offices of "VTB Bank Georgia".<br>
                "VTB Bank Georgia" Central Branch: ACCount #- GE64VT6600000444467944, Bank Code - UGEBGE22
                @else
                ანგარიშწორება შეგიძლიათ მოახდინოთ თბილისსა და რეგიონებში არსებულ "ჯეოსელის" აბონენტთა მომსახურების ცენტრებში, ასევე "ვი-თი-ბი ბანკი ჯორჯია" -ს ფილიალებში.
                "ვი-თი-ბი ბანკი ჯორჯია" -ს ცენტრალურ ფილიალო: ა/ა GE64VT6600000444467944, ბანკის კოდი: UGEBGE22
                @endif
            </div>
        </td>
    </tr>
    <tr>
        <td style="width: 50%">
            <div style="width: 100%; height: 200px; border: 1px solid #000; margin: 3%; float: left; font-size: 12px">
                @if($lang == 'en') Indebtedness: @else საგადასახადო ანგარიში: @endif {{$invoice->BILL_NO}}
                <br>
                @if($lang == 'en') Indebtedness calcuation date: @else დავალიანების გამოთვლის თარიღი: @endif {{date('d-m-Y', strtotime($invoice->BILL_PERIOD))}}.
                <br>
                @if($lang == 'en') Accounting method: {{$invoice->ACCOUNTING_METHOD}} @else  ანგარიშსწორების მეთოდი: @if($invoice->ACCOUNTING_METHOD == 'Advance') საავანსო @else საკრედიტო @endif @endif
                <br><br>
                <span style="font-size: 20px">@if($lang == 'en') Account number: @else პირადი ანგარიში: @endif {{$invoice->ACCOUNT_NUMBER}}</span>
            </div>
        </td>
        <td style="width: 50%">

            <div style="width: 100%; height: 200px; border: 1px solid #000; margin: 3%; font-size: 14px; float: right; font-size: 12px">
                <table style="width: 100%">
                    <tr>
                        <td>@if($lang == 'en') Deposit: @else დეპოზიტი: @endif</td>
                        <td>{{$invoice->DEPOSIT}}</td>
                    </tr>
                    <tr>
                        <td>@if($lang == 'en') Balance ({{date('01-m-Y', strtotime($invoice->BILL_PERIOD))}}) @else ბალანსი ({{date('01-m-Y', strtotime($invoice->BILL_PERIOD))}}): @endif </td>
                        <td>{{$invoice->BALANCE_FIRST}}</td>
                    </tr>
                    <tr>
                        <td>@if($lang == 'en') Total charged: @else ნასაუბრები პერიოდში: @endif</td>
                        <td style="background-color: darkgrey">{{$invoice->TOTAL_CHARGED}}</td>
                    </tr>
                    <tr>
                        <td>@if($lang == 'en') Correction: @else კორექტირება: @endif</td>
                        <td>{{$invoice->CREDIT_SUM}}</td>
                    </tr>
                    <tr>
                        <td>@if($lang == 'en') Payments: @else გადახდილია: @endif</td>
                        <td>{{$invoice->PAYMENTS}}</td>
                    </tr>
                    <tr>
                        <td>@if($lang == 'en') Deposit coverage @else ჩარიცხული დეპოზიტზე: @endif</td>
                        <td>{{$invoice->DEPOSIT_COVERAGE}}</td>
                    </tr>
                    <tr>
                        <td>@if($lang == 'en') Balance ({{date('t-m-Y', strtotime($invoice->BILL_PERIOD))}}): @else ბალანსი  ({{date('t-m-Y', strtotime($invoice->BILL_PERIOD))}}): @endif</td>
                        <td>{{$invoice->BALANCE_LAST}}</td>
                    </tr>
                    <tr>
                        <td>@if($lang == 'en') The debt forecast: @else პროგნოზი დავალიანებაზე @endif</td>
                        <td>{{$invoice->PROGNOZI_DAVALIANEBZE}}</td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>

</table>




<br>

<div style="width: 100%; text-align: center">
    <h2>@if($lang == 'en') Payable: @else რეკომენდებული გადასახდელი: @endif {{$invoice->PAYABLE}} @if($lang == 'en') Lari @else ლარი @endif</h2>
    <h2 style="font-size: 10px">
        @if($lang == 'en')
            Please be informed: in column "Quantity", calls and consumed internet data is provided in seconds and kilobytes
        @else
            გთხოვთ გაითვალისწინოთ: ველში "რაოდენობა" სასაუბრო დროის და მოხმარებული ინტერნეტის მაჩვენებლები გამოსახულია წამებში და კილობაიტებში

        @endif
    </h2>
</div>

<br>
<div style="width: 80%; margin: auto; border: 1px solid #000">
    <span style="font-size: 15px">@if($lang == 'en')  Mobile: {{$phone}} Total charged: {{$invoice->TOTAL_CHARGED}} Lari @else მობილური: {{$phone}} ნასაუბრები პერიოდში: {{$invoice->TOTAL_CHARGED}} ლარი @endif</span>
    <br>
    @if($lang == 'en') For services @else სატარიფო გეგმა: @endif GEOCELL 01
</div>


@if(isset($invoice->listinfo))
<div style="width: 98%; margin: auto; margin-top: 20px; font-size: 10px">
    <table style="width: 100%">
        <thead>
            <tr>
                <th>@if($lang == 'en') Service @else მომსახურება @endif </th>
                <th>@if($lang == 'en') Quantity @else რაოდენობა @endif</th>
                <th>@if($lang == 'en') Qnty(Lari) @else ოდენობა(ლარი) @endif</th>
                <th>@if($lang == 'en') Excise @else აქციზი @endif</th>
                <th>@if($lang == 'en') Vat(Lari) @else დ.ღ.გ.(ლარი) @endif</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $qbty_lari_total = 0;
        $excise_lari_total = 0;
        $var_lari_total = 0;
        ?>
            @foreach($invoice->listinfo as $key => $val)
                <tr>
                    @if($lang == 'en')
                        <td>{{$val->service_name_eng}}</td>
                    @else
                        <td>{{$val->service_name_geo}}</td>
                    @endif
                    <td>
                        {{$val->q_ty}}
                    </td>
                    <td>{{$val->qbty_lari}}</td>
                    <td>{{$val->excise_lari}}</td>
                    <td>{{$val->var_lari}}</td>
                </tr>
                <?php
                $qbty_lari_total += $val->qbty_lari;
                $excise_lari_total += $val->excise_lari;
                $var_lari_total += $val->var_lari;
                ?>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td>@if($lang == 'en') Total: @else ჯამი: @endif</td>
                <td></td>
                <td>{{$qbty_lari_total}}</td>
                <td>{{$excise_lari_total}}</td>
                <td>{{$var_lari_total}}</td>
            </tr>
        </tfoot>
    </table>
</div>
<div style="width: 100%; text-align: right">
<img src="uploads/stamp.png" alt="Geocell" style="float: right; width: 100px; float: right; margin-top: 10px">
</div>
@endif

<style>
    table {
        border-collapse: collapse;
    }

    table, th, td {
        border: 1px solid black;
    }
</style>
</body>
</html>


<?php
function writeVal($arg, $lang){
    if(strpos(strtoupper($arg->service_name_eng), 'SMS') != false){

        if($lang == 'en')
            return $arg->q_ty.' SMS';
        else
            return $arg->q_ty.' ფაქტი';
    }
    if(strpos(strtoupper($arg->service_name_eng), 'CALL') != false){

        if($unit < 60){
            if($lang == 'en')
                return intval($unit).' second';
            else
                return intval($unit).' წამი';
        } else {
            $min = intval($unit / 60);
            $sec = intval($unit % 60);
            if($lang == 'en')
                return $min.' minutes and '.$sec.' seconds';
            else
                return $min.' წუთი და '.$sec.' წამი';
        }
    }
    if(strpos(strtoupper($arg->service_name_eng), 'INTERNET') > -1){
        $unit = floatval($arg->q_ty);
        $unit = $unit / 1024;
//            return intval($unit);
        if($unit < 1024){
            if($lang == 'en')
                return intval($unit).' kb';
            else
                return intval($unit).' კბ';

        } else {
            $unit = $unit / 1024;
            if($lang == 'en')
                return intval($unit).' mb';
            else
                return intval($unit).' მბ';
        }
    }
    return $arg->q_ty;
}
?>
