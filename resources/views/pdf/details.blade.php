<?php
function unitCorrect($unit, $type, $lang){
    if(strpos($type, 'GPRS') > -1){

        $unit = floatval($unit);
        $unit = $unit / 1024;
//            return intval($unit);
        if($unit < 1024){
            if($lang == 'en')
                return intval($unit).' kb';
            else
                return intval($unit).' კბ';

        } else {
            $unit = $unit / 1024;
            if($lang == 'en')
                return intval($unit).' mb';
            else
                return intval($unit).' მბ';
        }
    } elseif ($type == 'calls' || $type == 'LTE'){
        $unit = floatval($unit);
        if($unit < 60){
            if($lang == 'en')
                return intval($unit).' second';
            else
                return intval($unit).' წამი';
        } else {
            $min = intval($unit / 60);
            $sec = intval($unit % 60);
            if($lang == 'en')
                return $min.' minutes and '.$sec.' seconds';
            else
                return $min.' წუთი და '.$sec.' წამი';
        }
    } elseif(strpos($type, 'sms') > -1) {
        if($lang == 'en')
            return intval($unit).' SMS';
        else
            return intval($unit).' ფაქტი';
    } else {
        return '';
    }
}
?>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        body { font-family: DejaVu Sans, sans-serif; }
    </style>
    <title>Invoices</title>
</head>
<body>
<div style="width: 100%; text-align: center">
    <img src="site/tsr-sections/tsr-header/logo-ge.png" alt="Geocell" style="float: right">
<h3>@if($lang == 'en') calls for period @else ზარები პერიოდში @endif</h3>
@if($lang == 'en')
<h5> From {{date('d-m-Y H:m:s', strtotime($details_req->fromdate))}} To {{date('d-m-Y 23:59:59', strtotime($details_req->todate))}}</h5>
@else
<h5> {{date('d-m-Y H:m:s', strtotime($details_req->fromdate))}} დან {{date('d-m-Y 23:59:59', strtotime($details_req->todate))}} მდე</h5>
@endif
@if($lang == 'en')
    <h3 style="font-size: 10px">Please be informed: in column "Unit value", call data is provided in hh/mm/ss format and internet consumption is provided in kilobytes.</h3>

@else
    <h3 style="font-size: 10px">გთხოვთ გაითვალისწინოთ: ველში "ერთეულის რაოდენობა", სასაუბრო დროის მაჩვენებლები გამოსახულია სთ/წთ/წმ ფორმატში, ინტერნეტის მოხმარება გამოსახულია კილობაიტებში.</h3>

@endif
<h5>{{$phone}}</h5>
</div>

<table style="width: 100%; font-size: 10px">
    <thead>
        <tr>
            <th style="border-bottom: 2px solid #000; height:24px">@if($lang == 'en')Time: @else დრო @endif</th>
            <th style="border-bottom: 2px solid #000; height:24px">@if($lang == 'en')Service type: @else სერვისის ტიპი @endif</th>
            <th style="border-bottom: 2px solid #000; height:24px">@if($lang == 'en')Number: @else ნომერი @endif</th>
            <th style="border-bottom: 2px solid #000; height:24px">@if($lang == 'en')Price: @else ფასი @endif</th>
            <th style="border-bottom: 2px solid #000; height:24px">@if($lang == 'en')Unit value: @else ერთეულის რაოდენობა @endif</th>
            <th style="border-bottom: 2px solid #000; height:24px">@if($lang == 'en')Direction: @else მიმართულება @endif</th>
            <th style="border-bottom: 2px solid #000; height:24px">@if($lang == 'en')Description: @else აღწერა @endif</th>
            <th style="border-bottom: 2px solid #000; height:24px">@if($lang == 'en')Value: @else ტარიფი @endif</th>
        </tr>
    </thead>
    <tbody>
    <?php $total_price = 0; ?>
        @foreach($details as $key => $val)
            <?php $total_price+= $val->price; ?>
        <tr >
            <td style="border-bottom: 1px solid #CCC;">{{$val->call_date}}</td>
            <td style="border-bottom: 1px solid #CCC;">{{$val->type}}</td>
            <td style="border-bottom: 1px solid #CCC;">{{$val->called_phone}}</td>
            <td style="border-bottom: 1px solid #CCC;">{{isset($val->price)?number_format($val->price, 2, '.', '').'':'0.00'}}</td>
            <td style="border-bottom: 1px solid #CCC;">
                {{unitCorrect($val->unitValue, $val->type, $lang)}}
             </td>
            <td style="border-bottom: 1px solid #CCC;">{{isset($val->zone_name)?$val->zone_name:''}}</td>
            <td style="border-bottom: 1px solid #CCC;">{{isset($val->description)?$val->description:''}}</td>
            <td style="border-bottom: 1px solid #CCC;">{{isset($val->tarrifInformation)?$val->tarrifInformation:''}}</td>
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td>@if($lang == 'en') Total: @else ჯამი: @endif</td>
            <td></td>
            <td></td>
            <td>{{number_format($total_price, 2, '.', '')}}</td>
            <td></td>
            <td></td>
        </tr>
    </tfoot>
</table>


</body>
</html>