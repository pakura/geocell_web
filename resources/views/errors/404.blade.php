<!doctype html>
<html lang="en">
  <head>
    <!-- TITLE -->
    <title>Geocell</title>

    <!-- META -->
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="site/tsr-components/tsr-favicon/favicon-144.png">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=8, IE=9, IE=10">
    <meta http-equiv="Content-Language" content="en">
    <![endif]-->

    <!-- ICONS -->
    <link rel="icon"                                         href="tsr-components/tsr-favicon/favicon.png">
    <link rel="shortcut icon"                                href="tsr-components/tsr-favicon/favicon.png">
    <link rel="apple-touch-icon"                             href="tsr-components/tsr-favicon/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="76x76"   href="tsr-components/tsr-favicon/favicon-76.png">
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="tsr-components/tsr-favicon/favicon-120.png">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="tsr-components/tsr-favicon/favicon-152.png">
    <link rel="apple-touch-icon-precomposed" sizes="180x180" href="tsr-components/tsr-favicon/favicon-180.png">

    <!-- CSS -->
    <link rel="stylesheet" href="/site/tsr-core/tsr-styles/tsr-reset.css">
    <link rel="stylesheet" href="/site/tsr-core/tsr-styles/tsr-core.css">
  </head>
  <body>
    <!-- tsr-section-custom-error -->
    <div class="tsr-section-custom-error">
      <div class="tsr-container">
        <div class="tsr-com-custom-error-logo">
          <a href="#"><img alt="Geocell" src="/site/tsr-sections/tsr-custom-errors/logo-en.png"></a>
        </div>
      </div>
      <div class="tsr-com-custom-error-404">
        <div class="tsr-com-custom-error-404-a"></div>
        <div class="tsr-com-custom-error-404-b"></div>
        <div class="tsr-com-custom-error-404-c">
          <div class="tsr-container">
            <div class="tsr-com-custom-error-title">Sorry</div>
            <div class="tsr-com-custom-error-description">
              <div class="cs-spb-04">Category not found!</div>
              <div class="cs-spb-04">Please try one of the following pages: <a href="{{config('app.url')}}">Homepage</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">

       window.setTimeout(function() {
            window.location = "/";
        }, 600);
    </script>
  </body>
</html>
