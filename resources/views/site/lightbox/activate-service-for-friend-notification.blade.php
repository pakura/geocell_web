<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
        <div class="cs-spb-20 cs-tx-40 cs-tx-bold">{{$service->title}}</div>
        <div class="cs-spb-20 cs-tx-bold"> {{$service->title}} {{$translate['activated_on_num']}} {{$number}}</div>
        <div class="cs-spb-20 cs-spt-30">
         <!--  <div class="cs-spb-04"><a href="{{$lang}}/buy/service" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise">Confirm</a></div>
          <div class="cs-spb-04"><a href="#" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel">Cancel</a></div> -->
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');

  $('.js-lightbox-cancel', $box).on('click', function(event) {
    $.fancybox.close();
    event.preventDefault();
  });

/*]]>*/</script>
