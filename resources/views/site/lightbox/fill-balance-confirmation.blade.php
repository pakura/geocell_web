
<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
        
        <div class="cs-spb-20 cs-tx-40 cs-tx-bold">{{$translate['fill_balance']}}</div>
     
        <div class="cs-spb-20 cs-spt-30 cs-mg-center" style="width: 260px;">
        <div class="cs-spb-20 cs-spt-30">
            <div class="tsr-lightbox-layout-a-form-pad">
              <div class="cs-spb-04"><label class="cc-label">{{$translate['number']}}: {{$phone}}</label></div>
         
            </div>
            <div class="tsr-lightbox-layout-a-form-pad">
              <div class="cs-spb-04"><label class="cc-label">{{$translate['tanxa']}}: {{$amount}}</label></div>
         
            </div>
            <div class="cs-spb-04"><a href="https://sb3d.georgiancard.ge/payment/start.wsm?lang=ka&o.phone={{$phone}}&o.amount={{$amount*100}}&page_id=B7F3795AAEA66B1271E323C1AF4A0A1C&merch_id=805F85F4482A04D0B103A4CE83EFFB73&back_url_s={{\Session::get('url')}}&back_url_f={{\Session::get('url')}}" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise">{{$translate['confirm']}}</a></div>
            <div class="cs-spb-04">
              <div class="cs-spb-04"><a href="#" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel">{{$translate['cancel']}}</a></div>
            </div>  
        </div>
        
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');



  $('a#demoaction').on('click', function(event) {

    $('.tsr-lightbox-layout-a-field', $box).addClass('invalid');
    $('.tsr-lightbox-layout-a-field', $box).css({'display': ''});
    $('.tsr-lightbox-layout-a-button', $box).css({'opacity': '0.5'});

    $.fancybox.update();
    
    event.preventDefault();

  });


  $('.js-lightbox-cancel', $box).on('click', function(event) {
    $.fancybox.close();
    event.preventDefault();
    
  });

/*]]>*/</script>
