<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
        <div class="cs-spb-20 cs-tx-20 cs-tx-bold">{{$translate['for_friend']}}</div>
        <div class="cs-spb-20 cs-tx-bold">   </div>
        @if(\Session::has('msg'))
          <div>{{\Session::get('msg')}}</div> 
        @endif
        <div class="cs-spb-20 cs-spt-30">

        <form action="{{$lang}}/buy/service-for-friend/{{$service->product_id}}" method="post" class="tsr-forms" onsubmit="return formAction(this)"  method="post">
          <div class="tsr-lightbox-layout-a-form-pad">
            <div class="cs-spb-04 tsr-lightbox-layout-a-field-combo">
                <div class="tsr-lightbox-layout-a-field-a"><input type="text" name="phone" data-inputmask="'mask': '\\599999999'" class="cs-al-center" /></div>
                <div class="tsr-lightbox-layout-a-field-b" style="visibility: hidden;"><img src="site/tsr-core/tsr-images/tsr-spi-loader-24.gif" alt=""></div>
              </div>
          </div>

          <div class="cs-spb-04"><button class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise">{{$translate['confirm']}}</button></div>
          <div class="cs-spb-04"><a href="#" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel">{{$translate['cancel']}}</a></div>
        </div>

        </form>

      </div>
    </div>
  </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');


  $('input[type="text"]', $box).inputmask();

  $('.js-lightbox-cancel', $box).on('click', function(event) {
    $.fancybox.close();
    event.preventDefault();
  });

/*]]>*/</script>
