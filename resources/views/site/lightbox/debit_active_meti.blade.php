<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
    <div class="tsr-com-collapser-outer">
        <div class="tsr-com-collapser-inner">
            <div class="cs-al-center">
                <div class="cs-spb-20 cs-tx-32 cs-tx-bold">{{$translate['add_new_number']}}</div>
                <div class="cs-spb-20 cs-spt-30">
                    <form action="{{$lang}}/initReq" method="post" class="tsr-forms" onsubmit="return initSendReq(true)">
                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><label class="cc-label">{{$translate['cell_number']}}</label></div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-form-pad">
                                <div class="tsr-lightbox-layout-a-field-combo">
                                    <div class="tsr-lightbox-layout-a-field-a">
                                        <input type="text" data-inputmask="'mask': '\\599999999'" @if(isset($phone)) value="{{substr($phone, 3, 9)}}" readonly style="border: 0px; color: #000;" @endif @if(isset($data->gsm)) value="{{substr($data->gsm, 3, 9)}}" readonly @endif class="cs-al-center" id="phoneN" name="phone" maxlength="9" onkeyup="initSendReq(false)" />
                                    </div>
                                    <div class="tsr-lightbox-layout-a-field-b" ><span class="ts-icon-thick cs-tx-15 cs-cl-turquoise" style="display: none"></span></div>
                                </div>
                            </div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field" ><div class="cc-errorbox" id="gsmerror" style="display: none">@if($lang == 'en') Indicated number does not belong to Geocell @else მითითებული ნომერი არ არის ჯეოსელის ნომერი @endif </div></div>
                        </div>
                        @if(!isset($phone))
                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><label class="cc-label">{{$translate['name']}} ({{$translate['optional']}})</label></div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field">
                                <input type="text" class="cs-al-center" id="servname" name="servname"  @if(isset($data->dsc)) value="{{$data->dsc}}" @endif />
                            </div>
                        </div>
                        @endif
                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><label class="cc-label">{{$translate['choose_bundle']}}</label></div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field">
                                <select id="metibundle" name="metibundle" onchange="initSendReq(false)">
                                    <option value="0">-- select --</option>
                                    <option value="1" @if(isset($data->bonusId) && $data->bonusId == 1) value="{{$data->bonusId}}" @endif >@if($lang == 'en') Meti S @else მეტი S @endif</option>
                                    <option value="2" @if(isset($data->bonusId) && $data->bonusId == 2) value="{{$data->bonusId}}" @endif >@if($lang == 'en') Meti M @else მეტი M @endif</option>
                                    <option value="3" @if(isset($data->bonusId) && $data->bonusId == 3) value="{{$data->bonusId}}" @endif >@if($lang == 'en') Meti unlimited @else მეტი უსასრულო  @endif</option>
                                    <option value="4" @if(isset($data->bonusId) && $data->bonusId == 4) value="{{$data->bonusId}}" @endif >@if($lang == 'en') Meti unlimited + @else მეტი უსასრულო+ @endif</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="debittype" value="2"/>
                        <input type="hidden" name="status" value="1"/>
                        <div class="cs-spb-20">
                            <div style="margin: auto;width: 30px;height: 30px;">
                                <div class="floatingBarsG" id="metiP">
                                    <div class="blockG" id="rotateG_01"></div>
                                    <div class="blockG" id="rotateG_02"></div>
                                    <div class="blockG" id="rotateG_03"></div>
                                    <div class="blockG" id="rotateG_04"></div>
                                    <div class="blockG" id="rotateG_05"></div>
                                    <div class="blockG" id="rotateG_06"></div>
                                    <div class="blockG" id="rotateG_07"></div>
                                    <div class="blockG" id="rotateG_08"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><input type="submit" id="submitbtn" value="{{$translate['confirm']}}" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise" onclick="$('#metiP').fadeIn()"></div>
                            <div class="cs-spb-04"><a href="#" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel">{{$translate['cancel']}}</a></div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

    var $box = $('#unique-lightbox-id');
//    $('input[type="text"]', $box).inputmask();

    function initSendReq(t){

        var phoneN = $('#phoneN').val();
        var servname = $('#servname').val();
        var metibundle = $('#metibundle').val();
        if (parseInt(phoneN) > 100000000){
            if(isGeocell(phoneN) == false){
                $('#phoneN').css({'border':'1px solid red'});
                $('#metiP').fadeOut();
                return false;
            } else {
                $('#phoneN').css({'border':'1px solid #dbdbdb'});
            }
        } else {
            if(t==true){
                $('#phoneN').css({'border':'1px solid red'});
                $('#metiP').fadeOut();
                return false;
            }

        }
        if (metibundle == 0 && t==true) {
            $('#metibundle').css({'border': '1px solid red'});
            $('#metiP').fadeOut();
            $('#gsmerror').fadeOut();
            return false;
        }
        return true;
    }
    $('.js-lightbox-cancel', $box).on('click', function(event) {
        $.fancybox.close();
        event.preventDefault();
    });

    function isGeocell(gsm){
        $.ajax({
                    method: "GET",
                    url: "{{$lang}}/checkgsm",
                    async: false,
                    data: {
                        gsm: gsm
                    }
                })
                .done(function( msg ) {
                    if(msg == 'ok'){
                        $('.cs-cl-turquoise').fadeIn();
                        $('#gsmerror').fadeOut();
                        gsm = true;
                    } else {
                        if(msg == -1006){
                            $('.cs-cl-turquoise').fadeOut();
                            $('#gsmerror').fadeIn();
                            $('.fancybox-inner').css({'height':'590px'});
                        }
                        gsm = false;
                    }
                });
        return gsm;
    }

    /*]]>*/</script>
