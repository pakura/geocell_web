<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">

      <div class="cs-al-center">

        @if(\Session::has('msg'))
          <div>{{\Session::get('msg')}}</div> 
        @endif

        <div class="cs-spb-20 cs-tx-40 cs-tx-bold">Triple 0</div>
        <div class="cs-spb-20 cs-tx-bold">{{$translate['triple_specify_number']}}</div>
        <div class="cs-spb-20 cs-spt-30">
          <form action="{{$lang}}/save-numbers/{{$service->product_id}}" method="POST" class="tsr-forms" onsubmit="formAction(this)">
            <div class="tsr-lightbox-layout-a-form-pad">
              
            @if(is_array($numbers) && count($numbers>0) )
               @foreach($numbers as $numb)
                <div class="cs-spb-10">
                  <div><span class="cs-tx-18">{{$numb}}</span><span class="cs-spl-20 cs-tx-bold cs-tx-13"><!-- <a href="#" onclick="return false;" class="noline">edit</a><span class="cs-sph-04 cs-cl-link">/</span> --><!-- <a href="#" onclick="return false;" class="noline">Remove</a></span> --></div>
                </div>
                @endforeach
            @endif

              @if( count($numbers)<3 )
                <?php (int)$till = 3-count($numbers); ?>
                 @for($i=0;$i<$till;$i++)
                 <div class="cs-spb-04 tsr-lightbox-layout-a-field-combo">
                  <div class="tsr-lightbox-layout-a-field-a"><input type="text" name="friend_phone[]" data-inputmask="'mask': '\\599999999'" class="cs-al-center" /></div>
                  <div class="tsr-lightbox-layout-a-field-b" style="visibility: hidden;"><span class="ts-icon-thick cs-tx-15 cs-cl-turquoise"></span></div>
                </div>
                @endfor
              @endif
            
            </div>

            <div class="cs-spb-20 cs-spt-30">
              <div class="cs-spb-04"><button  class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise">{{$translate['save_and_confirm']}}</button></div>
              <div class="cs-spb-04"><button class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray" >{{$translate['specify_later']}}</button></div>
            </div>

          </form>
        </div>
        
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');

  $('input[type="text"]', $box).inputmask();


/*]]>*/</script>
