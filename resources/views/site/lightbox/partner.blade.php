<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
    <div class="tsr-com-collapser-outer">
        <div class="tsr-com-collapser-inner">
            <div class="cs-al-center">
                <div class="cs-spb-20">
                    <div class="cs-tx-40 cs-tx-bold">Partner 2</div>
                    <div class="cs-cl-blue">Package: 5 Lari</div>
                </div>
                <div class="cs-spb-00">
                    <form action="{{$lang}}/partner/send" method="post" class="tsr-forms">
                        <div class="cs-max-whole-x cs-mg-center" style="width: 260px;">

                            <div class="cs-spb-10 cc-form-field">
                                <div class="cs-spb-04"><label class="cc-label">@if($lang == 'en') Company name @else კომპანიის სახელი @endif *</label></div>
                                <div class="cs-spb-00"><input name="company_name" id="companyname" type="text" class="cs-al-center" /></div>
                            </div>

                            <div class="cs-spb-10 cc-form-field">
                                <div class="cs-spb-04"><label class="cc-label">@if($lang == 'en') ID Code @else ID კოდი @endif*</label></div>
                                <div class="cs-spb-00"><input name="id_code" id="idcode" type="text" class="cs-al-center" /></div>
                            </div>

                            <div class="cs-spb-10 cc-form-field">
                                <div class="cs-spb-04"><label class="cc-label">@if($lang == 'en') Number of employees @else თანამშრომლების რაოდენობა @endif *</label></div>
                                <div class="cs-spb-00">
                                    <select name="employ" >
                                        <option value="1-10">1-10</option>
                                        <option value="11-50">11-50</option>
                                        <option value="51-100">51-100</option>
                                        <option value="100+">more than 100</option>
                                    </select>
                                </div>
                            </div>

                            <div class="cs-spb-10 cc-form-field">
                                <div class="cs-spb-04"><label class="cc-label">@if($lang == 'en') Contact person @else საკონტაქტო პირი @endif *</label></div>
                                <div class="cs-spb-00"><input name="contact_person" id="person" type="text" class="cs-al-center" /></div>
                            </div>

                            <div class="cs-spb-10 cc-form-field">
                                <div class="cs-spb-04"><label class="cc-label">@if($lang == 'en') Phone number @else ტელეფონის ნომერი @endif *</label></div>
                                <div class="cs-spb-00"><input name="cell_number" id="phone" type="text" class="cs-al-center" /></div>
                            </div>

                            <div class="cs-spb-10 cc-form-field">
                                <div class="cs-spb-04"><label class="cc-label">@if($lang == 'en') Email @else ელ.ფოსტა @endif</label></div>
                                <div class="cs-spb-00"><input name="email" type="text" class="cs-al-center" /></div>
                            </div>

                            <div class="cs-spb-10 cc-form-field">
                                <div class="cs-spb-00 cs-clearfix"><label class="cc-label-cbox cs-al-left"><span class="cs-fl-left"><input type="checkbox"></span><span class="cs-dp-block cs-spl-30 cs-cl-gray cs-tx-normal">I wish to receive information regarding other products</span></label></div>
                            </div>

                            <div class="cs-spt-20 cc-form-field">
                                <div class="cs-spb-04"><input type="submit" class="tsr-btn tsr-btn-form tsr-btn-100 tsr-btn-turquoise" value="@if($lang == 'en') Submit @else გაგზავნა @endif " /></div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

    var $box = $('#unique-lightbox-id');
    var $form = $('form', $box);

    $form.on('submit', function(event) {
        if($('#companyname').val() == ''){
            $('#companyname').parent().parent().addClass('invalid');
            $.fancybox.update();
            event.preventDefault();
        }
        if($('#idcode').val() == ''){
            $('#idcode').parent().parent().addClass('invalid');
            $.fancybox.update();
            event.preventDefault();
        }
        if($('#person').val() == ''){
            $('#person').parent().parent().addClass('invalid');
            $.fancybox.update();
            event.preventDefault();
        }
        if($('#phone').val() == ''){
            $('#phone').parent().parent().addClass('invalid');
            $.fancybox.update();
            event.preventDefault();
        }


    });

    /*]]>*/</script>
