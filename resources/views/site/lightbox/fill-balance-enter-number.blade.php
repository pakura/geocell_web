<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
        <div class="cs-spb-20 cs-tx-20 cs-tx-bold">{{$translate['fill_balance']}}</div>
        <div class="cs-spb-20 cs-tx-bold">{{$translate['fill_balance_enter_number']}}</div>
        @if(\Session::has('msg'))
          <div>{{\Session::get('msg')}}</div> 
        @endif
        <div class="cs-spb-04 cs-spt-30">

        <form action="{{$lang}}/fill-balance/choose-action" method="post" class="tsr-forms" onsubmit="return formAction(this)"  method="post">
          <div class="tsr-lightbox-layout-a-form-pad">
            <div class="cs-spb-04 tsr-lightbox-layout-a-field-combo">
                <div class="tsr-lightbox-layout-a-field-a"><input type="text" name="phone" data-inputmask="'mask': '\\599999999'" class="cs-al-center" /></div>
                <div class="tsr-lightbox-layout-a-field-b" style="visibility: hidden;"><img src="site/tsr-core/tsr-images/tsr-spi-loader-24.gif" alt=""></div>
              </div>
          </div>
           <div class="cs-spb-10 tsr-lightbox-layout-a-field msisdn-error" style="display: none;"><div class="cc-errorbox">{{$translate['wrong_msisdn']}}</div></div>

          <div class="cs-spb-04"><button class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise" id="next">{{$translate['confirm']}}</button></div>
          <div class="cs-spb-04"><a href="#" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel">{{$translate['cancel']}}</a></div>
        </div>

        </form>

      </div>
    </div>
  </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');


//  $('input[type="text"]', $box).inputmask();

   /*$('#next').on('click', function(event) {
     event.preventDefault();
    var number =$('input[name=phone]','.tsr-forms').val();
     if(!number || number<9){
      $('.msisdn-error').show();
      $(".fancybox-inner").height('auto');
    }
 });*/


  $('.js-lightbox-cancel', $box).on('click', function(event) {
    $.fancybox.close();
    event.preventDefault();
  });

/*]]>*/</script>
