<style>
  #floatingBarsG{
    position:relative;
    width:23px;
    height:29px;
    margin:auto;
  }

  .blockG{
    position:absolute;
    background-color:rgb(255,255,255);
    width:4px;
    height:9px;
    border-radius:3px 3px 0 0;
    -o-border-radius:3px 3px 0 0;
    -ms-border-radius:3px 3px 0 0;
    -webkit-border-radius:3px 3px 0 0;
    -moz-border-radius:3px 3px 0 0;
    transform:scale(0.4);
    -o-transform:scale(0.4);
    -ms-transform:scale(0.4);
    -webkit-transform:scale(0.4);
    -moz-transform:scale(0.4);
    animation-name:fadeG;
    -o-animation-name:fadeG;
    -ms-animation-name:fadeG;
    -webkit-animation-name:fadeG;
    -moz-animation-name:fadeG;
    animation-duration:1.2s;
    -o-animation-duration:1.2s;
    -ms-animation-duration:1.2s;
    -webkit-animation-duration:1.2s;
    -moz-animation-duration:1.2s;
    animation-iteration-count:infinite;
    -o-animation-iteration-count:infinite;
    -ms-animation-iteration-count:infinite;
    -webkit-animation-iteration-count:infinite;
    -moz-animation-iteration-count:infinite;
    animation-direction:normal;
    -o-animation-direction:normal;
    -ms-animation-direction:normal;
    -webkit-animation-direction:normal;
    -moz-animation-direction:normal;
  }

  #rotateG_01{
    left:0;
    top:10px;
    animation-delay:0.45s;
    -o-animation-delay:0.45s;
    -ms-animation-delay:0.45s;
    -webkit-animation-delay:0.45s;
    -moz-animation-delay:0.45s;
    transform:rotate(-90deg);
    -o-transform:rotate(-90deg);
    -ms-transform:rotate(-90deg);
    -webkit-transform:rotate(-90deg);
    -moz-transform:rotate(-90deg);
  }

  #rotateG_02{
    left:3px;
    top:4px;
    animation-delay:0.6s;
    -o-animation-delay:0.6s;
    -ms-animation-delay:0.6s;
    -webkit-animation-delay:0.6s;
    -moz-animation-delay:0.6s;
    transform:rotate(-45deg);
    -o-transform:rotate(-45deg);
    -ms-transform:rotate(-45deg);
    -webkit-transform:rotate(-45deg);
    -moz-transform:rotate(-45deg);
  }

  #rotateG_03{
    left:10px;
    top:1px;
    animation-delay:0.75s;
    -o-animation-delay:0.75s;
    -ms-animation-delay:0.75s;
    -webkit-animation-delay:0.75s;
    -moz-animation-delay:0.75s;
    transform:rotate(0deg);
    -o-transform:rotate(0deg);
    -ms-transform:rotate(0deg);
    -webkit-transform:rotate(0deg);
    -moz-transform:rotate(0deg);
  }

  #rotateG_04{
    right:3px;
    top:4px;
    animation-delay:0.9s;
    -o-animation-delay:0.9s;
    -ms-animation-delay:0.9s;
    -webkit-animation-delay:0.9s;
    -moz-animation-delay:0.9s;
    transform:rotate(45deg);
    -o-transform:rotate(45deg);
    -ms-transform:rotate(45deg);
    -webkit-transform:rotate(45deg);
    -moz-transform:rotate(45deg);
  }

  #rotateG_05{
    right:0;
    top:10px;
    animation-delay:1.05s;
    -o-animation-delay:1.05s;
    -ms-animation-delay:1.05s;
    -webkit-animation-delay:1.05s;
    -moz-animation-delay:1.05s;
    transform:rotate(90deg);
    -o-transform:rotate(90deg);
    -ms-transform:rotate(90deg);
    -webkit-transform:rotate(90deg);
    -moz-transform:rotate(90deg);
  }

  #rotateG_06{
    right:3px;
    bottom:3px;
    animation-delay:1.2s;
    -o-animation-delay:1.2s;
    -ms-animation-delay:1.2s;
    -webkit-animation-delay:1.2s;
    -moz-animation-delay:1.2s;
    transform:rotate(135deg);
    -o-transform:rotate(135deg);
    -ms-transform:rotate(135deg);
    -webkit-transform:rotate(135deg);
    -moz-transform:rotate(135deg);
  }

  #rotateG_07{
    bottom:0;
    left:10px;
    animation-delay:1.35s;
    -o-animation-delay:1.35s;
    -ms-animation-delay:1.35s;
    -webkit-animation-delay:1.35s;
    -moz-animation-delay:1.35s;
    transform:rotate(180deg);
    -o-transform:rotate(180deg);
    -ms-transform:rotate(180deg);
    -webkit-transform:rotate(180deg);
    -moz-transform:rotate(180deg);
  }

  #rotateG_08{
    left:3px;
    bottom:3px;
    animation-delay:1.5s;
    -o-animation-delay:1.5s;
    -ms-animation-delay:1.5s;
    -webkit-animation-delay:1.5s;
    -moz-animation-delay:1.5s;
    transform:rotate(-135deg);
    -o-transform:rotate(-135deg);
    -ms-transform:rotate(-135deg);
    -webkit-transform:rotate(-135deg);
    -moz-transform:rotate(-135deg);
  }



  @keyframes fadeG{
    0%{
      background-color:rgb(0,0,0);
    }

    100%{
      background-color:rgb(255,255,255);
    }
  }

  @-o-keyframes fadeG{
    0%{
      background-color:rgb(0,0,0);
    }

    100%{
      background-color:rgb(255,255,255);
    }
  }

  @-ms-keyframes fadeG{
  0%{
    background-color:rgb(0,0,0);
  }

  100%{
    background-color:rgb(255,255,255);
  }
  }

  @-webkit-keyframes fadeG{
    0%{
      background-color:rgb(0,0,0);
    }

    100%{
      background-color:rgb(255,255,255);
    }
  }

  @-moz-keyframes fadeG{
    0%{
      background-color:rgb(0,0,0);
    }

    100%{
      background-color:rgb(255,255,255);
    }
  }
</style>
<div class="tsr-lightbox-layout-a" id="unique-lightbox-id"  style="padding:0">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
       
        <div class="cs-spb-20"><a href="ge/login" data-fancybox-type="ajax" class="composite js-fancybox fancybox.ajax"><img src="site/tsr-core/tsr-images/tsr-arrow-text-left.png" alt=""><span class="cs-spl-04 cs-cl-text cs-tx-noline">{{$translate['back']}}</span></a></div>
        <div class="cs-spb-20 cs-tx-40 cs-tx-bold">{{$translate['type_pass']}}</div>
        <div class="cs-spb-20"><!-- Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text --> <!-- <a href="#" id="demoaction" class="">DEMO</a>. --></div>
        <div class="cs-spb-20 cs-spt-30 cs-mg-center" style="width: 260px;">
        <div class="cs-spb-20 cs-spt-30">
          <form action="{{$lang}}/altersms" method="post" class="tsr-forms"  onsubmit="return alterSms(this)">
            <div class="tsr-lightbox-layout-a-form-pad">
              <div class="cs-spb-04"><label class="cc-label">{{$translate['password']}}</label></div>
              <div class="cs-spb-04 tsr-lightbox-layout-a-field"><input type="password" id="passin" class="cs-al-center" name="password"  id="password"/></div>
              <div class="cs-spb-10 tsr-lightbox-layout-a-field" style="display: none;"><div class="cc-errorbox">{{$translate['wrong_pass']}}</div></div>
           <!--    <div class="cs-spb-30 cs-spt-10"><label class="cc-label-cbox"><input type="checkbox"><span> Remember me</span></label></div> -->
            </div>
         	<div class="cs-spb-20"><button id="passloginbtn" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise" onclick="$('#floatingBarsG').fadeIn(); $('.cs-spb-10').fadeOut();">{{$translate['signin']}}</button></div>
          </form>
        </div>
          <div id="floatingBarsG" style="display: none">
            <div class="blockG" id="rotateG_01"></div>
            <div class="blockG" id="rotateG_02"></div>
            <div class="blockG" id="rotateG_03"></div>
            <div class="blockG" id="rotateG_04"></div>
            <div class="blockG" id="rotateG_05"></div>
            <div class="blockG" id="rotateG_06"></div>
            <div class="blockG" id="rotateG_07"></div>
            <div class="blockG" id="rotateG_08"></div>
          </div>
        <div class="cs-spb-20 cs-spt-30"><a href="{{$lang}}/forgot-password"  class="js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['forgot_pass']}}</a></div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');
  $(function(){
    setTimeout(function () {
      $( "#passin" ).focus();
    },400);

  });
/*  $('a#demoaction').on('click', function(event) {

    $('.tsr-lightbox-layout-a-field', $box).addClass('invalid');
    $('.tsr-lightbox-layout-a-field', $box).css({'display': ''});
    $('.tsr-lightbox-layout-a-button', $box).css({'opacity': '0.5'});

    $.fancybox.update();
    
    event.preventDefault();

  });*/

/*]]>*/</script>
