<div class="tsr-lightbox-layout-a" id="remove-service-lightbox">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
   
        <div class="cs-spb-20 cs-tx-bold">Service <span id="service-title">{{$service->description}}</span> is not removable! </div>
        <div class="cs-spb-20 cs-spt-30">
        
      
          <div class="cs-spb-04 cancel-button"><a href="#" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise js-lightbox-cancel">OK</a></div>

        </div>

      </div>
    </div>
  </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#remove-service-lightbox');

  $('.js-lightbox-cancel', $box).on('click', function(event) {
    $.fancybox.close();
    event.preventDefault();
  });

/*]]>*/</script>
