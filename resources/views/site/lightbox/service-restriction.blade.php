<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
        <div class="cs-spb-20"><span class="ts-icon-alert cs-tx-32 cs-cl-orange"></span></div>
        <div class="cs-spb-20 cs-tx-40 cs-tx-bold">{{$service->title}}</div>
        <div class="cs-spb-20">{{$removable?$translate['service_restriction']:$translate['service_meti_restriction']}} </div>
       <!--  <div class="cs-spb-20 cs-tx-bold">თქვენ ნომერზე აქტიურია სერვისები:</div> -->
        <div class="cs-spb-20 cs-cl-red" id="errors"></div>
        <div class="cs-spb-20 cs-spt-30">

          <div class="tsr-uboard-services">
          @foreach($restrictions as $restriction)
            <div class="tsr-uboard-service restriction">
              <div class="tsr-uboard-service-modal"></div>
              <div class="tsr-uboard-service-head">
                <div class="tsr-uboard-service-head-col-01">
                  <div class="tsr-clearfix">
                    <div class="tsr-uboard-service-span tsr-clearfix">
                      <figure class="tsr-uboard-service-image">
                        <img alt="" src="site/tsr-temp/tsr-images/service-01.png">
                      </figure>
                      <div class="tsr-uboard-service-desc">
                        <div class="tsr-uboard-service-title">{{@$restriction->title}}</div>
<!--                         <div class="tsr-uboard-service-intro">Till September 10, 2015</div>
 -->                      </div>
                    </div>
                  </div>
                </div>
                @if($removable)
                <div class="tsr-uboard-service-head-col-02">
                  <div class="composite"><!--
                    --><div class="cs-spr-10"><div class="tsr-uboard-service-stats">{{$translate['service_on']}}  <img class="loading_wait" width="16" src="site/tsr-core/tsr-images/tsr-spi-loader-16.gif" style="display:none"></div></div><!--
                    --><div class="cs-spl-10"><div class="tsr-btn tsr-btn-xsmall tsr-btn-gray deactivate-service" data-url="{{$lang}}/dashboard/deactivate-service/{{$restriction->sku}}?jsn=1" data-clicked="0">{{$translate['deactivate']}}</div>  </div><!--
                  --></div>
                </div>
                @endif

              </div>

            </div>
            @endforeach
           
          </div>

        </div>
        <div class="cs-spb-20 cs-spt-30">
            @if(!$removable)
            <!-- <a href="javascript:;" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray"   style="min-width: 200px;">{{$translate['continue']}} </a> -->
            @else
              <a href="{{$lang}}/buy/service/{{$service->product_id}}?periodic={{$periodic}}" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise js-fancybox fancybox.ajax" data-fancybox-type="ajax" id="continue" style="min-width: 200px;">{{$translate['continue']}} </a>
            @endif
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');

  var $services = $('.tsr-uboard-service', $box);


    $('.deactivate-service').on('click', function() {
            var ths = $(this);

            var clicked = ths.data('clicked');
            /*var conf = confirm("Are you sure?");*/
            if(!clicked) { 

                $('.loading_wait').show(); 

                var url = $(this).data('url');
                console.log(url);
                $.ajax({
                    type : 'GET',
                    url : url,
                    dataType: "json",
                    success: function(response){
                             if(response.success){

                                setTimeout(function () {
                                    ths.closest('.restriction').remove();

                                    $('#continue').trigger("click");
                                }, 3000);
                                
                             }
                             if(response.error) { $('#unique-lightbox-id').find('#errors').html(response.error);  $(".fancybox-inner").height('auto');/*console.log(response.error);*/}
                              
                        }
                }); 
            }

            ths.data('clicked',1);
             
          });

 /* $services.each(function() {

    var $service = $(this);
    var $switch  = $('.tsr-com-switch', $service);
    var $status  = $('.tsr-uboard-service-stats', $service);

    $switch.on('click', function(event) {
      $switch.toggleClass('is-choosen');

      $status.html($switch.hasClass('is-choosen') ? 'Service on' : 'Service off');

      //$service.addClass('is-loading');
      $services.not($service).addClass('is-hidden');
      if(!$(this).hasClass('is-choosen')) {  

                var url = $(this).data('url');
                $.ajax({
                    type : 'GET',
                    url : url,
                    success: function(response){
                            console.log(response);

                        }
                }); 
            }

      event.preventDefault();
    });

  });*/


/*]]>*/</script>
