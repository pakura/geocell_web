<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
    <div class="tsr-com-collapser-outer">
        <div class="tsr-com-collapser-inner">
            <div class="cs-al-center">
                <div class="cs-spb-20">
                    <div class="cs-tx-40 cs-tx-bold">@if($lang == 'en') Partner 2 @else პარტნიორი 2 @endif</div>
                    <div class="cs-cl-blue">
                        @if($lang == 'en')
                        Package: {{$pack->price}} Lari
                        @else
                        პაკეტი: {{$pack->price}} ლარი
                        @endif
                    </div>
                </div>
                <div class="cs-spb-00">
                    <form action="{{$lang}}/partner2/order" method="post" class="tsr-forms">
                        <input type="hidden" name="pack" value="{{$pack->price}}">
                        <div class="cs-max-whole-x cs-mg-center" style="width: 260px;">

                            <div class="cs-spb-10 cc-form-field" id="company_name_wp">
                                <div class="cs-spb-04"><label class="cc-label">@if($lang == 'en') Company name * @else კომპანიის სახელი * @endif</label></div>
                                <div class="cs-spb-00"><input type="text" class="cs-al-center" name="company_name" id="company_name"/></div>
                            </div>

                            <div class="cs-spb-10 cc-form-field" id="id_code_wp">
                                <div class="cs-spb-04"><label class="cc-label">ID @if($lang == 'en') Code @else კოდი @endif*</label></div>
                                <div class="cs-spb-00"><input type="text" class="cs-al-center" name="id_code" id="id_code" /></div>
                            </div>

                            <div class="cs-spb-10 cc-form-field">
                                <div class="cs-spb-04"><label class="cc-label">@if($lang == 'en') Number of employees * @else დასაქმებულების რაოდენობა * @endif</label></div>
                                <div class="cs-spb-00">
                                    <select name="employ">
                                        @for($i = 1; $i<=100; $i += 10)
                                        <option value="{{$i.'-'.($i+9)}}">{{$i.'-'.($i+9)}}</option>
                                        @endfor
                                        <option value="more than 100">more than 100</option>
                                    </select>
                                </div>
                            </div>

                            <div class="cs-spb-10 cc-form-field" id="contact_person_wp">
                                <div class="cs-spb-04"><label class="cc-label">@if($lang == 'en') Contact person * @else საკონტაქტო პირი @endif</label></div>
                                <div class="cs-spb-00"><input type="text" class="cs-al-center" name="contact_person" id="contact_person" /></div>
                            </div>

                            <div class="cs-spb-10 cc-form-field" id="cell_number_wp">
                                <div class="cs-spb-04"><label class="cc-label">{{$translate['cell_number']}}*</label></div>
                                <div class="cs-spb-00"><input type="text" class="cs-al-center" name="cell_number" id="cell_number" minlength="9" maxlength="9" /></div>
                            </div>

                            <div class="cs-spb-10 cc-form-field" id="email_wp">
                                <div class="cs-spb-04"><label class="cc-label">@if($lang == 'en') Email @else ელ.ფოსტა @endif</label></div>
                                <div class="cs-spb-00"><input type="text" class="cs-al-center" name="email"  id="email"/></div>
                            </div>

                            {{--<div class="cs-spb-10 cc-form-field">--}}
                                {{--<div class="cs-spb-00 cs-clearfix"><label class="cc-label-cbox cs-al-left"><span class="cs-fl-left"><input type="checkbox"></span><span class="cs-dp-block cs-spl-30 cs-cl-gray cs-tx-normal">{{$translate['emailsub']}}</span></label></div>--}}
                            {{--</div>--}}

                            <div class="cs-spt-20 cc-form-field">
                                <div class="cs-spb-04"><input type="submit" class="tsr-btn tsr-btn-form tsr-btn-100 tsr-btn-turquoise" value="{{$translate['contact_submit']}}" /></div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">/*<![CDATA[*/

    var $box = $('#unique-lightbox-id');
    var $form = $('form', $box);

    $form.on('submit', function(event) {
//        $('.cc-form-field', $form).addClass('invalid');
        if($('#company_name').val().length < 2){
            $('#company_name_wp', $form).addClass('invalid');
            return false;
        }

        if($('#id_code').val().length < 5){
            $('#id_code_wp', $form).addClass('invalid');
            return false;
        }

        if($('#contact_person').val().length < 5){
            $('#contact_person_wp', $form).addClass('invalid');
            return false;
        }

        if($('#cell_number').val().length != 9){
            $('#cell_number_wp', $form).addClass('invalid');
            return false;
        }

        if($('#email').val().length != 0){
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(!re.test($('#email').val())){
                $('#email_wp', $form).addClass('invalid');
                return false;
            }
        }

        return true;
        $.fancybox.update();
        event.preventDefault();
    });
    $('#cell_number', $form).inputmask('Regex', { regex: "([0-9]{1,8})?((\.)([0-9]){0,2})" });
    $( "input" ).change(function() {
        $(this).parent().parent().removeClass('invalid');
    });




    /*]]>*/</script>
