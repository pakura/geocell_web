<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
       
        <div class="cs-spb-20"><a href="ge/login" data-fancybox-type="ajax" class="composite js-fancybox fancybox.ajax"><img src="site/tsr-core/tsr-images/tsr-arrow-text-left.png" alt=""><span class="cs-spl-04 cs-cl-text cs-tx-noline">{{$translate['back']}}</span></a></div>
        <div class="cs-spb-20 cs-tx-40 cs-tx-bold">{{$translate['set_new_password']}}</div>
        <!-- <div class="cs-spb-20">{{$translate['one_time_sms_code']}}</div> -->
        <div class="cs-spb-20 cs-spt-30 cs-mg-center" style="width: 260px;">
        <div class="cs-spb-20 cs-spt-30">
          <form action="{{$lang}}/altersms" method="post" class="tsr-forms"  id="set_password"  onsubmit="return alterSms(this)" autocomplete="off">
            <input type="password" style="display:none"/>
            <div class="tsr-lightbox-layout-a-form-pad cs-spb-10">
              <div class="cs-spb-04"><label class="cc-label">{{$translate['sms_code']}}</label></div>
              <div class="cs-spb-04 tsr-lightbox-layout-a-field"><input type="text" name="password" data-inputmask="'mask': '999999'" class="cs-al-center"   autocomplete="off" /></div>

              <div class="cs-spb-10 tsr-lightbox-layout-a-field" style="display: none;"><div class="cc-errorbox">{{$translate['wrong_sms_code']}}</div></div>

              <div class="cs-spb-10 cs-spt-20"><a href="#" id="resend_sms">{{$translate['resend_sms']}}</a></div>
              <div class="cs-spb-10 tsr-lightbox-layout-a-field" style="display: none;" id="resend_sms_text"><div class="cc-errorbox">{{$translate['resend_sms_text']}}</div></div>

              <div class="set_pass">
                  <div class="cs-spb-04"><label class="cc-label">{{$translate['password']}}</label></div>
                 <div class="cs-spb-04 tsr-lightbox-layout-a-field"><input type="password" class="cs-al-center" name="password2"  id="password2" value=""   autocomplete="off"/></div>
              </div>
              <div class="cs-spb-10 tsr-lightbox-layout-a-field password_length_error error" style="display: none;" ><div class="cc-errorbox">{{$translate['password_length']}}</div></div>

              <div class="set_pass">
                  <div class="cs-spb-04"><label class="cc-label">{{$translate['repeat_password']}}</label></div>
                 <div class="cs-spb-04 tsr-lightbox-layout-a-field"><input type="password" class="cs-al-center" name="repeat_password2" id="repeat_password2" value=""   autocomplete="off"/></div>
              </div>

              <div class="cs-spb-10 tsr-lightbox-layout-a-field password_dont_mutch error" style="display: none;" ><div class="cc-errorbox ">{{$translate['passwords_dont_mutch']}}</div></div>

            </div>
            <div class="cs-spb-20"><button class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise" id="set_password_btn">{{$translate['set_and_sign_in']}}</button></div>

          </form>
        </div>
        
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');

  $('input[type="text"]', $box).inputmask();

/*
  $('a#demoaction').on('click', function(event) {

    $('.tsr-lightbox-layout-a-field', $box).addClass('invalid');
    $('.tsr-lightbox-layout-a-field', $box).css({'display': ''});
    $('.tsr-lightbox-layout-a-button', $box).css({'opacity': '0.5'});

    $.fancybox.update();
    
    event.preventDefault();

  });
*/
 $('#set_password_btn').on('click', function(event) {

      event.preventDefault();
      var pass = $("#password2").val();
      var repeat_pass = $("#repeat_password2").val();
      if(pass.length<6){
        $(".password_length_error").show();
         $(".fancybox-inner").height('auto');
        return false;
      }
      else if(pass!=repeat_pass){
        $(".password_length_error").hide();
        $(".password_dont_mutch").show();
         $(".fancybox-inner").height('auto');
           return false;
      }
      else{
        $(".error").hide();

         return alterSms($("#set_password"));
      }
      $(".fancybox-inner").height('auto');
    

  });

  $('a#resend_sms').on('click', function(event) {

    $("#resend_sms_text").show(); 
      $(".fancybox-inner").height('auto');
    event.preventDefault();

  });

/*]]>*/</script>
