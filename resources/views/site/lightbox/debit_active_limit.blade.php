<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
    <div class="tsr-com-collapser-outer">
        <div class="tsr-com-collapser-inner">
            <div class="cs-al-center">
                <div class="cs-spb-20 cs-tx-32 cs-tx-bold">{{$translate['add_new_number']}}</div>
                <div class="cs-spb-20 cs-spt-30">
                    <form action="{{$lang}}/initReq" method="post" class="tsr-forms"  onsubmit="return initSendReq(true)">
                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><label class="cc-label">{{$translate['cell_number']}}</label></div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-form-pad">
                                <div class="tsr-lightbox-layout-a-field-combo">
                                    <div class="tsr-lightbox-layout-a-field-a"><input type="text" data-inputmask="'mask': '\\599999999'" @if(isset($phone)) value="{{substr($phone, 3, 9)}}" readonly style="border: 0px; color: #000;" @endif  @if(isset($data->gsm)) value="{{substr($data->gsm, 3, 9)}}" readonly @endif  class="cs-al-center" onkeyup="initSendReq(false)" id="phoneN" name="phone"  maxlength="9"/></div>
                                    <div class="tsr-lightbox-layout-a-field-b"><span class="ts-icon-thick cs-tx-15 cs-cl-turquoise" style="display: none;"></span></div>
                                </div>
                            </div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field"><div class="cc-errorbox" id="gsmerror" style="display: none">@if($lang == 'en') Indicated number does not belong to Geocell @else მითითებული ნომერი არ არის ჯეოსელის ნომერი @endif </div></div>
                        </div>
                        @if(!isset($phone))
                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><label class="cc-label">{{$translate['name']}} ({{$translate['optional']}})</label></div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field"><input type="text" class="cs-al-center"  id="servname" name="servname"  @if(isset($data->dsc)) value="{{$data->dsc}}" @endif /></div>
                        </div>
                        @endif
                        <div class="cs-spb-20">
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field">
                                <div class="tsr-clearfix">
                                    <div class="cs-fl-left"  style="width: 98px;">
                                        <div class="cs-spb-04"><label class="cc-label">{{$translate['limit_low']}}</label></div>
                                        <div class="cs-spb-04"><input type="text" class="cs-al-center" id="low" name="low" onkeyup="initSendReq(false)"  @if(isset($data->minimalBalance)) value="{{$data->minimalBalance}}" @endif /></div>
                                    </div>

                                    <div class="cs-fl-right" style="width: 98px;">
                                        <div class="cs-spb-04"><label class="cc-label">{{$translate['limit_top']}}</label></div>
                                        <div class="cs-spb-04"><input type="text" class="cs-al-center" id="top" name="top" onkeyup="initSendReq(false)"  @if(isset($data->targetBalance)) value="{{$data->targetBalance}}" @endif /></div>
                                    </div>
                                </div>
                            </div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field"><div class="cc-errorbox" id="priceErrorlow" style="display: none">@if($lang == 'en') Amount for lower threshold should be fixed at minimum 1 GEL @else ქვედა ლიმიტის თანხა უნდა შეადგენდეს მინიმუმ 1 ლარს @endif </div></div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field"><div class="cc-errorbox" id="priceErrortop" style="display: none">@if($lang == 'en') Amount for upper threshold should be fixed at maximum 500 GEL @else ზედა ლიმიტის თანხა უნდა შეადგენდეს მაქსიმუმ  500 ლარს @endif </div></div>
                        </div>
                        <input type="hidden" name="debittype" value="0"/>
                        <input type="hidden" name="status" value="1"/>
                        <div class="cs-spb-20">
                            <div style="margin: auto;width: 30px;height: 30px;">
                                <div class="floatingBarsG" id="limitP">
                                    <div class="blockG" id="rotateG_01"></div>
                                    <div class="blockG" id="rotateG_02"></div>
                                    <div class="blockG" id="rotateG_03"></div>
                                    <div class="blockG" id="rotateG_04"></div>
                                    <div class="blockG" id="rotateG_05"></div>
                                    <div class="blockG" id="rotateG_06"></div>
                                    <div class="blockG" id="rotateG_07"></div>
                                    <div class="blockG" id="rotateG_08"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><input type="submit" id="submitbtn" value="{{$translate['confirm']}}"  class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise" onclick="$('#limitP').fadeIn()"></div>
                            <div class="cs-spb-04"><a class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel">{{$translate['cancel']}}</a></div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">/*<![CDATA[*/
    $("#low").keypress(function (e) {
        if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
    });
    $("#top").keypress(function (e) {
        if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
    });
    var $box = $('#unique-lightbox-id');
//    $('input[type="text"]', $box).inputmask();
    function initSendReq(t){
        var phoneN = $('#phoneN').val();
        if (parseInt(phoneN) > 100000000){
            if(isGeocell(phoneN) == false){
                $('#phoneN').css({'border':'1px solid red'});
                $('#limitP').fadeOut();
                return false;
            } else {
                $('#phoneN').css({'border':'1px solid #dbdbdb'});
            }
        } else {
            if(t == true){
                $('#phoneN').css({'border':'1px solid red'});
                $('#limitP').fadeOut();
                return false;
            }
        }
        var servname = $('#servname').val();
        if(($('#low').val() < 1 || $('#low').val() == '' || $('#low').val() > 500) && t==true){
            $('#low').css({'border':'1px solid red'});
            $('#limitP').fadeOut();
            $('#priceErrorlow').fadeIn();
            $('#gsmerror').fadeOut();
            $('.fancybox-inner').css({'height':'615px'});
            return false;
        }
        if(($('#top').val() < 2 || $('#top').val() == '' || $('#top').val() > 500) && t==true){
            $('#top').css({'border':'1px solid red'});
            $('#limitP').fadeOut();
            $('#priceErrortop').fadeIn();
            $('#gsmerror').fadeOut();
            $('.fancybox-inner').css({'height':'615px'});
            return false;
        }
        var low = parseFloat($('#low').val());
        var top = parseFloat($('#top').val());
        return true;
    }
    $('.js-lightbox-cancel', $box).on('click', function(event) {
        $.fancybox.close();
        event.preventDefault();
    });


    function isGeocell(gsm){
        $.ajax({
                    method: "GET",
                    url: "{{$lang}}/checkgsm",
                    async: false,
                    data: {
                        gsm: gsm
                    }
                })
                .done(function( msg ) {
                    if(msg == 'ok'){
                        $('.cs-cl-turquoise').fadeIn();
                        $('#gsmerror').fadeOut();
                        gsm = true;
                    } else {
                        if(msg == -1006){
                            $('.cs-cl-turquoise').fadeOut();
                            $('#gsmerror').fadeIn();
                            $('.fancybox-inner').css({'height':'600px'});
                        }
                        gsm = false;
                    }
                });
        return gsm;
    }
    /*]]>*/</script>
