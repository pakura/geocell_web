<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
    <div class="tsr-com-collapser-outer">
        <div class="tsr-com-collapser-inner">
            <div class="cs-al-center">
                <div class="cs-spb-20 cs-tx-32 cs-tx-bold">Change your card number</div>
                <div class="cs-spb-20 cs-spt-30">
                    <div class="tsr-forms">

                        <div class="cs-spb-20">
                            {{isset($maskpan)?$maskpan:'XXXX-XXXX-XXXX-XXXX'}}
                            <br><br>
                            ნამდვილა გსურთ ბარათის შეცვლა?
                        </div>

                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><a href="{{$lang}}/activecard"><input type="submit" value="{{$translate['change']}}" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise"></a></div>
                            <div class="cs-spb-04"><a onclick="$('.fancybox-close').click()" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray">{{$translate['cancel']}}</a></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

    var $box = $('#unique-lightbox-id');
    $('input[type="text"]', $box).inputmask();

    /*]]>*/</script>
