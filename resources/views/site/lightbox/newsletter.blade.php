    <div class="tsr-container" style="padding: 25px; padding-left: 0px">
        <div class="tsr-section-generic-pad-h">
            <div class="cs-spb-10"><h1 class="cs-al-center">{{$translate['subscribe_newsletter']}}</h1></div>
            <div class="cs-spb-00">
                <div class="tsr-forms">
                    <a style="display: none" href="{{$lang}}/oknewsletter" data-fancybox-type="ajax" class="js-fancybox fancybox.ajax tsr-avatar-link" onclick="return false;" id="oknewsletter"></a>
                    <a style="display: none" href="{{$lang}}/alreadynewsletter" data-fancybox-type="ajax" class="js-fancybox fancybox.ajax tsr-avatar-link" onclick="return false;" id="alreadynewsletter"></a>
                    <a style="display: none" href="{{$lang}}/disablenewsletter" data-fancybox-type="ajax" class="js-fancybox fancybox.ajax tsr-avatar-link" onclick="return false;" id="disablenewsletter"></a>
                    <div class="cs-al-center"><!--
                            --><div class="cs-dp-iblock cs-al-top" style="width: 280px; max-width: 100%;"><div class="cs-spx-05"><input type="text" name="email" id="sub_email" class="placeholder" placeholder="@if($lang == 'en') Please enter your E-mail @else მიუთითეთ თქვენი ელ-ფოსტა @endif " title=""></div></div><!--
                            --><div class="cs-dp-iblock cs-al-top"><div class="cs-spx-05"><button class="tsr-btn tsr-btn-form tsr-btn-turquoise" onclick="sendSubscribeReq()">{{$translate['subscribe']}}</button></div></div><!--
                                --><div class="cs-spb-10 tsr-lightbox-layout-a-field msisdn-error" style="width: 275px;margin: auto; display: none;" id="suberror"><div class="cc-errorbox" style="left: -67px;">@if($lang == 'en') The E-mail is not valid @else მითითებული ელ-ფოსტა არასწორია @endif</div></div><!--
                          --></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function checksub(){
            if($('#sub_email').val() == ''){
                $('#sub_email').css({'border' : '1px solid red'});
                return false;
            }
            var email = $('#sub_email').val();
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(!regex.test(email) && email != ''){
                $('#sub_email').css({'border' : '1px solid red'});
                return false;
            }
            return true;
        }
        function sendSubscribeReq(){
            var email = $('#sub_email').val();
            $('#suberror').fadeOut();
            var req = checksub();
            if(req == false){
                $('#suberror').fadeIn();
                $('.fancybox-inner').css({'height':'166px'});
                return
            }
            $.ajax({
                method: "POST",
                url: "{{$lang}}/emailsubscriber",
                data: {email: email}
            })
            .done(function( msg ) {
                $('#sub_email').val('');
                $('#sub_email').css({'border' : '1px solid #dbdbdb'});
                if(msg == 'ok'){
                    $('#oknewsletter').click();
                } else {
                    if(msg == 'already'){
                        $('#alreadynewsletter').click();
                    } else {
                        if(msg == 'disabled'){
                            $('#disablenewsletter').click();
                        } else {
                            $('#sub_email').css({'border' : '1px solid red'});
                        }
                    }
                }
            });
        }
    </script>