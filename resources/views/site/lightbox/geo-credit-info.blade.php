<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
          @if(isset($response->return->creditAmount) && $response->return->creditAmount>0)
              
          	<div class="cs-spb-20 cs-tx-bold">
          	    @if($lang=='ge')
	              <p> თქვენ სარგებლობთ სერვისით ჯეოკრედიტი.</p>
                <p>კრედიტის და სერვისის ღირებულება ჯამურად შეადგენს {{number_format($response->return->creditAmount/100,2)}} ლარს. </p>
	            @else 
                <p> You are using Geocredit service.</p>
	              <p>Credit amount and service fee is {{number_format($response->return->creditAmount/100,2)}} Gel.</p> 
	            @endif 
          		
          	</div>
            @else 

            <div class="cs-spb-20 cs-tx-bold">
                @if($lang=='ge')
                <p> თქვენ არ სარგებლობთ სერვისით ჯეოკრედიტი.</p>
               
              @else 
                <p> You are not using Geocredit service.</p>
                
              @endif 
              
            </div>
          @endif
      </div>
    </div>
  </div>
</div>
 