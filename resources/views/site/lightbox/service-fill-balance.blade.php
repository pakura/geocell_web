<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
        <div class="cs-spb-20"><span class="ts-icon-alert cs-tx-32 cs-cl-orange"></span></div>
        <div class="cs-spb-20 cs-tx-40 cs-tx-bold">{{$service->title}}</div>
        <div class="cs-spb-20">
          <div class="cs-spb-04 cs-tx-bold">{{$translate['your_balance']}}</div>
          <div class="cs-spb-00 cs-tx-24">{{round(@$userInfo->return->balance,2)}}&nbsp;<img src="site/tsr-core/tsr-images/tsr-currency-gel.png" height="20" alt="gel" class="cs-al-baseline" /></div>
        </div>
        <div class="cs-spb-20 cs-spt-30">
          <div class="cs-spb-04">{{$translate['fill_balance_msg']}}</div>
          <div class="cs-spb-04">{{$translate['recomended_amount']}} {{round($service->value,2)}}
            &nbsp;<img src="site/tsr-core/tsr-images/tsr-currency-gel.png" height="14" alt="gel" class="cs-al-baseline" /></div>
        </div>
        <div class="cs-spb-20 cs-spt-30">
          <div class="cs-spb-04"><a href="https://sb3d.georgiancard.ge/payment/start.wsm?lang=ka&o.phone={{\Session::get('phone')}}&o.amount={{$service->value*100}}&page_id=B7F3795AAEA66B1271E323C1AF4A0A1C&merch_id=805F85F4482A04D0B103A4CE83EFFB73&back_url_s={{\Session::get('url')}}&back_url_f={{\Session::get('url')}}" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise">{{$translate['fill_balance']}}</a></div>
          <div class="cs-spb-04"><a href="#" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel">{{$translate['cancel']}}</a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');
  
  $('.js-lightbox-cancel', $box).on('click', function(event) {
    $.fancybox.close();
    event.preventDefault();
  });

/*]]>*/</script>
