<div class="tsr-lightbox-layout-a" id="unique-lightbox-id" >
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
        
        <div class="cs-spb-20 cs-tx-40 cs-tx-bold">{{$translate['fill_balance']}}</div>
     
        <div class="cs-spb-20 cs-spt-30 cs-mg-center" style="width: 260px;">
        <div class="cs-spb-04 cs-spt-30">
          <form action="{{$lang}}/fill-balance/confirmation" method="post" class="tsr-forms" onsubmit="return formAction(this);">
            <div class="tsr-lightbox-layout-a-form-pad">
              <div class="cs-spb-04"><label class="cc-label">{{$translate['amount']}}</label></div>
              <div class="cs-spb-04 tsr-lightbox-layout-a-field"><input type="text" name="amount" class="cs-al-center" /></div>
              <input type="hidden" value="{{$phone}}" name="phone"/>
              <div class="cs-spb-10 tsr-lightbox-layout-a-field msisdn-error" style="display: none;"><div class="cc-errorbox">{{$translate['amount']}}</div></div>

            </div>
            <div class=""><button class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise" id="next">{{$translate['next']}}</button></div>
          </form>
        </div>
         <div class="cs-spb-04"><button class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel">{{$translate['cancel']}}</button></div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');


  $('input[type="text"]', $box).inputmask('Regex', { regex: "([0-9]{1,8})?((\.)([0-9]){0,2})" });
   

 $('#next').on('click', function(event) {
    var number =$('input[name=amount]','.tsr-forms').val();
     if(!number){
      $('.msisdn-error').show();
      $(".fancybox-inner").height('auto');
      event.preventDefault();
      
    }
 });

 /* $('a#demoaction').on('click', function(event) {

    $('.tsr-lightbox-layout-a-field', $box).addClass('invalid');
    $('.tsr-lightbox-layout-a-field', $box).css({'display': ''});
    $('.tsr-lightbox-layout-a-button', $box).css({'opacity': '0.5'});

    $.fancybox.update();
    
    event.preventDefault();

  });
  */
  
  $('.js-lightbox-cancel', $box).on('click', function(event) {
    $.fancybox.close();
    event.preventDefault();
  });
 
/*]]>*/</script>
