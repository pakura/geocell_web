<?php

     $moreInfoUrl = $lang.'/'.$service->page_slug.'/'.$service->slug;
  
?>
<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
         @if(\Session::has('msg'))<div class="cs-spb-20"><span class="ts-icon-alert cs-tx-32 cs-cl-orange"></span></div>@endif
        <div class="cs-spb-20 cs-tx-40 cs-tx-bold">{{$service->title}}</div>
        <div class="cs-spb-20">
          <div class="cs-spb-04 cs-tx-bold">{{trans('site.your_balance')}}</div>
          <div class="cs-spb-00 cs-tx-24">{{round(@$userInfo->return->balance,2)}} &nbsp;<img src="site/tsr-core/tsr-images/tsr-currency-gel.png" height="20" alt="gel" class="cs-al-baseline" /></div>
        </div>
        <div class="cs-spb-20 cs-spt-30">{{$translate['service_price'].' '.$service->value}}&nbsp;<img src="site/tsr-core/tsr-images/tsr-currency-gel.png" height="14" alt="gel" class="cs-al-baseline" /></div>

        @if(\Session::has('msg'))
          <div>{{\Session::get('msg')}}</div> 
        @endif

        <div class="cs-spb-20">
          <div class="tsr-lightbox-layout-a-cols-a tsr-clearfix">
            <div class="tsr-lightbox-layout-a-col">
              <div class="cs-spb-10"><img src="site/tsr-core/tsr-images/pay_ico_1.png" alt="" class="cs-max-whole-x"></div>
              <div class="cs-spb-10 cs-tx-24 cs-cl-purple">{{$translate['from_balance']}}</div>
              <div class="cs-spb-00"><a href="{{$lang}}/buy/service-meti/{{$service->product_id}}" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['purchase']}}</a></div>
            </div>
            <div class="tsr-lightbox-layout-a-col">
              <div class="cs-spb-10"><img src="site/tsr-core/tsr-images/pay_ico_2.png" alt="" class="cs-max-whole-x"></div>
              <div class="cs-spb-10 cs-tx-24 cs-cl-purple">{{$translate['from_bank']}}</div>
              @if($allowBuy == true)
                <div class="cs-spb-00"><a href="https://sb3d.georgiancard.ge/payment/start.wsm?lang=ka&o.phone={{\Session::get('phone')}}&o.amount={{$service->value*100}}&o.bundlePrice={{$service->value*100}}&page_id=137393DFB876D5BFBB3211C0C32EF30B&merch_id=7D7CA98E5AF0F3C57575E6334C9EDC7B&o.bundleId={{$service->sku2}}&back_url_s={{\Session::get('url')}}&back_url_f={{\Session::get('url')}}" class="tsr-btn tsr-btn-turquoise">{{$translate['purchase']}}</a></div>
              @else
                <div class="cs-spb-00"><a class="tsr-btn tsr-btn-gray tooltip" title="
                  @if($lang == 'en')
                    To activate selected package, first deactivate the one in use, then activate the new package.
                  @else
                    არჩეული პაკეტის ჩასართავად, ჯერ გამორთეთ პაკეტი, რომლითაც უკვე სარგებლობთ და შემდეგ გაააქტიურეთ ახალი.
                  @endif">{{$translate['purchase']}}</a></div>
              @endif
            </div>
          </div>
        </div>

        <div class="cs-spb-20 cs-spt-30">{{$translate['for_more_info_click']}} <a href="{{$moreInfoUrl}}" class="">{{$translate['here']}}</a></div>

      </div>
    </div>
  </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');
  $('.tooltip').tooltipster();

/*]]>*/</script>
