<div class="tsr-lightbox-layout-a" id="unique-lightbox-id" >
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
        
        <div class="cs-spb-20 cs-tx-40 cs-tx-bold">{{$translate['buy_meti']}}</div>

        <div class="cs-spb-20">
          <div class="tsr-lightbox-layout-a-cols-a tsr-clearfix cs-spb-20">
            
             <div class="tsr-uboard-services tsr-uboard-services-scenario-a">  
              <div class="tsr-dashboard-block-wrap-body tsr-dashboard-block-wrap-head" style="border:0;">
                <!--  <div class="tsr-uboard-service-head" style="cursor:pointer">
                    <div class="tsr-uboard-service-title">{{$translate['bucket_meti']}}</div>
                 </div> -->

                  <div class="tsr-uboard-service-body" id="group-meti" style="display:block">
                    <div class="tsr-uboard-service-body-pad">
                     @foreach($services as $service)
                        <div class="tsr-uboard-service">
                            <div class="tsr-uboard-service-modal"></div>
                            <div class="tsr-uboard-service-head">
                              <div class="tsr-uboard-service-head-col-01">
                                <div class="tsr-clearfix">
                                  <div class="tsr-uboard-service-link tsr-clearfix">
                                    <figure class="tsr-uboard-service-image">
                                      <img alt="" src="uploads/products/thumbs/{{$service->file}}">
                                    </figure>
                                    <div class="tsr-uboard-service-desc">
                                        <div class="tsr-uboard-service-title">{{$service->title}}</div>
                                      
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="tsr-uboard-service-head-col-02">
                                <div class="composite"><!--
                                  --><div class="cs-spr-10"><div class="tsr-uboard-service-price"></div></div><!--
                                  --><div class="cs-spl-10">
                                @if($service->allowBuy == true)
                                    <a href="https://sb3d.georgiancard.ge/payment/start.wsm?lang=ka&o.phone={{$phone}}&o.amount={{$service->value*100}}&o.bundlePrice={{$service->value*100}}&page_id=137393DFB876D5BFBB3211C0C32EF30B&merch_id=7D7CA98E5AF0F3C57575E6334C9EDC7B&o.bundleId={{$service->sku2}}&back_url_s={{config('app.url')}}&back_url_f={{config('app.url')}}" class="tsr-btn tsr-btn-turquoise">{{$translate['purchase']}}</a>
                                @else
                                    <a class="tsr-btn tsr-btn-gray tooltip" title="
                                        @if($lang == 'en')
                                            To activate selected package, first deactivate the one in use, then activate the new package.
                                        @else
                                            არჩეული პაკეტის ჩასართავად, ჯერ გამორთეთ პაკეტი, რომლითაც უკვე სარგებლობთ და შემდეგ გაააქტიურეთ ახალი.
                                        @endif
                                    ">{{$translate['purchase']}}</a>
                                @endif

                                  </div>

                                  </div>

                              </div>
                            </div>
                         
                          </div>
                     @endforeach
                    </div>
                  </div>         
              </div>   
            </div>  

          </div>

        </div>
 
    </div>
  </div>
</div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');


  $('input[type="text"]', $box).inputmask('Regex', { regex: "([0-9]{1,8})?((\.)([0-9]){0,2})" });

  $('.tooltip').tooltipster();
  
  $('.js-lightbox-cancel', $box).on('click', function(event) {
    $.fancybox.close();
    event.preventDefault();
  });
 
/*]]>*/</script>
