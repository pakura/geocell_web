<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
    <div class="tsr-com-collapser-outer">
        <div class="tsr-com-collapser-inner">
            <div class="cs-al-center">
                <div class="cs-spb-20 cs-tx-32 cs-tx-bold">{{$translate['add_new_number']}}</div>
                <div class="cs-spb-20 cs-spt-30">
                    <form action="{{$lang}}/initReq" method="post" class="tsr-forms"  onsubmit="return initSendReq(true)">
                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><label class="cc-label">{{$translate['cell_number']}}</label></div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-form-pad">
                                <div class="tsr-lightbox-layout-a-field-combo">
                                    <div class="tsr-lightbox-layout-a-field-a"><input type="text" onkeyup="initSendReq(false)" data-inputmask="'mask': '\\599999999'" @if(isset($phone)) value="{{substr($phone, 3, 9)}}" readonly style="border: 0px; color: #000;" @endif @if(isset($data->gsm)) value="{{substr($data->gsm, 3, 9)}}" readonly @endif class="cs-al-center" id="phoneN" name="phone"  maxlength="9"/></div>
                                    <div class="tsr-lightbox-layout-a-field-b"><span class="ts-icon-thick cs-tx-15 cs-cl-turquoise" style="display: none"></span></div>
                                </div>
                            </div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field" ><div class="cc-errorbox" id="gsmerror"  style="display: none">@if($lang == 'en') Indicated number does not belong to Geocell @else მითითებული ნომერი არ არის ჯეოსელის ნომერი @endif </div></div>
                        </div>
                        @if(!isset($phone))
                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><label class="cc-label">{{$translate['name']}} ({{$translate['optional']}})</label></div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field">
                                <input type="text" class="cs-al-center" id="servname" name="servname"  @if(isset($data->dsc)) value="{{$data->dsc}}" @endif />
                            </div>
                        </div>
                        @endif
                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><label class="cc-label">{{$translate['tanxa']}}</label></div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field">
                                <input type="text" class="cs-al-center" id="amount" name="amount" @if(isset($data->fixedAmount)) value="{{$data->fixedAmount}}" @endif  onkeyup="initSendReq(false)"/>
                            </div>
                        </div>
                        <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field"><div class="cc-errorbox" id="priceErrorlow" style="display: none">@if($lang == 'en') Minimum amount for lower threshold should be 1 GEL @else თანხა უნდა შეადგენდეს  მინიმუმ 1 ლარს @endif </div></div>
                        <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field"><div class="cc-errorbox" id="priceErrortop" style="display: none">@if($lang == 'en') Maximum amount for upper threshold should be 500 GEL @else თანხა უნდა შეადგენდეს მაქსიმუმ  500 ლარს @endif </div></div>
                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><label class="cc-label">{{$translate['repeat_every_month_at']}}</label></div>
                            <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field">
                                <select name="day">
                                    <option value="1" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 1) selected @endif>01</option>
                                    <option value="2" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 2) selected @endif>02</option>
                                    <option value="3" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 3) selected @endif>03</option>
                                    <option value="4" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 4) selected @endif>04</option>
                                    <option value="5" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 5) selected @endif>05</option>
                                    <option value="6" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 6) selected @endif>06</option>
                                    <option value="7" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 7) selected @endif>07</option>
                                    <option value="8" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 8) selected @endif>08</option>
                                    <option value="9" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 9) selected @endif>09</option>
                                    <option value="10" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 10) selected @endif>10</option>
                                    <option value="11" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 11) selected @endif>11</option>
                                    <option value="12" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 12) selected @endif>12</option>
                                    <option value="13" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 13) selected @endif>13</option>
                                    <option value="14" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 14) selected @endif>14</option>
                                    <option value="15" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 15) selected @endif>15</option>
                                    <option value="16" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 16) selected @endif>16</option>
                                    <option value="17" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 17) selected @endif>17</option>
                                    <option value="18" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 18) selected @endif>18</option>
                                    <option value="19" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 19) selected @endif>19</option>
                                    <option value="20" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 20) selected @endif>20</option>
                                    <option value="21" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 21) selected @endif>21</option>
                                    <option value="22" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 22) selected @endif>22</option>
                                    <option value="23" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 23) selected @endif>23</option>
                                    <option value="24" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 24) selected @endif>24</option>
                                    <option value="25" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 25) selected @endif>25</option>
                                    <option value="26" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 26) selected @endif>26</option>
                                    <option value="27" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 27) selected @endif>27</option>
                                    <option value="28" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 28) selected @endif>28</option>
                                    <option value="29" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 29) selected @endif>29</option>
                                    <option value="30" @if(isset($data->dayOfMonth) && $data->dayOfMonth == 30) selected @endif>30</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="debittype" value="1"/>
                        <input type="hidden" name="status" value="1"/>
                        <div class="cs-spb-20">
                            <div style="margin: auto;width: 30px;height: 30px;">
                                <div class="floatingBarsG" id="fixedP">
                                    <div class="blockG" id="rotateG_01"></div>
                                    <div class="blockG" id="rotateG_02"></div>
                                    <div class="blockG" id="rotateG_03"></div>
                                    <div class="blockG" id="rotateG_04"></div>
                                    <div class="blockG" id="rotateG_05"></div>
                                    <div class="blockG" id="rotateG_06"></div>
                                    <div class="blockG" id="rotateG_07"></div>
                                    <div class="blockG" id="rotateG_08"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><input type="submit" id="submitbtn" value="{{$translate['confirm']}}" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise" onclick="$('#fixedP').fadeIn()"></div>
                            <div class="cs-spb-04"><a class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel">{{$translate['cancel']}}</a></div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

    var $box = $('#unique-lightbox-id');
//    $('input[type="text"]', $box).inputmask();
    $("#amount").keypress(function (e) {
        if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
    });






    function initSendReq(t){
        var phoneN = $('#phoneN').val();
        var servname = $('#servname').val();
        var amount = parseFloat($('#amount').val());
        var amount = $('#amount').val();
        if (parseInt(phoneN) > 100000000){
            if(isGeocell(phoneN) == false){
                $('#phoneN').css({'border':'1px solid red'});
                $('#fixedP').fadeOut();
                return false;
            } else {
                $('#phoneN').css({'border':'1px solid #dbdbdb'});
            }
        } else {
            if(t == true){
                $('#fixedP').fadeOut();
                return false;
                $('#phoneN').css({'border':'1px solid red'});
            }
        }
        if ((amount < 1 || amount > 500) && t==true){
            $('#amount').css({'border':'1px solid red'});
            $('#fixedP').fadeOut();
            if(amount < 1){
                $('#priceErrorlow').fadeIn();
            } else {
                $('#priceErrortop').fadeIn();
            }
            $('#gsmerror').fadeOut();
            $('.fancybox-inner').css({'height':'615px'})
            return false;
        }
        return true;
    }
    $('.js-lightbox-cancel', $box).on('click', function(event) {
        $.fancybox.close();
        event.preventDefault();
    });


    function isGeocell(gsm){
        $.ajax({
                    method: "GET",
                    url: "{{$lang}}/checkgsm",
                    async: false,
                    data: {
                       gsm: gsm
                    }
                })
                .done(function( msg ) {
                    if(msg == 'ok'){
                        $('.cs-cl-turquoise').fadeIn();
                        $('#gsmerror').fadeOut();
                        gsm = true;
                    } else {
                        if(msg == -1006){
                            $('.cs-cl-turquoise').fadeOut();
                            $('#gsmerror').fadeIn();
                            $('.fancybox-inner').css({'height':'666px'});
                        }
                        gsm = false;
                    }
                });
        return gsm;
    }

    /*]]>*/</script>
