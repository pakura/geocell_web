<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
    <div class="tsr-com-collapser-outer">
        <div class="tsr-com-collapser-inner">
            <div class="cs-al-center">
                <div class="cs-spb-20 cs-tx-40 cs-tx-bold">
                    @if($lang == 'en')
                        General condition
                    @else
                        ზოგადი პირობები
                    @endif
                </div>
                <div class="cs-spb-20">
                    @if($lang == 'en')
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                    @else
                        სერვისის საშუალებით თქვენ შეძლებთ ბალანსის ავტომატურ შევსებას ბარათიდან.
                        <br><br>
                        1. აირჩიეთ ავტომატური შევსების ტიპი<br>
                        2. დაამატეთ ნომერი<br>
                        3. მიუთითოთ ბარათის მონაცემები
                        <br><br>

                        სერვისის ღირებულება 1 ლარი ჩამოგეჭრებათ თვეში ერთხელ თქვენი მობილური ნომრის ანგარიშიდან.
                    @endif
                </div>

                <div class="cs-spb-20 cs-spt-30"><a href="{{$lang}}/private/private-cabinet/direct-debit" class="tsr-btn tsr-btn-turquoise ">{{$translate['continue']}}</a></div>
            </div>
        </div>
    </div>
</div>
