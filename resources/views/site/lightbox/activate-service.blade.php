<?php
  
  if((!isset($isBundle) || $isBundle==0) && $service->collection_id!=68){
     $moreInfoUrl = $lang.'/'.$service->page_slug.'/'.$service->slug;
  }
  else{
     $moreInfoUrl = $lang.'/'.$service->page_slug;
  }
$userInfo = \Session::get('userInfo');
?>
<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
        <div class="cs-spb-20 cs-tx-40 cs-tx-bold">{{$service->title}}</div>
        <div class="cs-spb-20 cs-tx-bold">
          @if($service->service_activation_msg)
            {{strip_tags($service->service_activation_msg)}}
          @else
            @if($lang=='en')
              You're activating {{$service->title}}. Price {{round($service->value,2)}} GEL, Validity 30 days. 
            @else
              თქვენ გაგიაქტიურდებათ {{$service->title}}. ტარიფის ფასი {{round($service->value,2)}} ლარი, ვადა  30 დღე. 
            @endif
          @endif
        </div>
        @if(\Session::has('msg'))
          <div>{{\Session::get('msg')}}</div> 
        @endif

        @if((isset($isBundle) && $isBundle) && (isset($userInfo->return->brand) && $userInfo->return->brand==2) && $periodic!=1)
          <div class="cs-spb-20 cs-spt-30"><a href="{{$lang}}/activate/service-for-friend/{{$service->product_id}}" class="js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['activate_for_friend']}}</a></div>
        @endif

        <div class="cs-spb-20 cs-spt-30">
          <div class="cs-spb-04"><a href="{{$lang}}/buy/service/{{$service->product_id}}?periodic={{$periodic}}" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['confirm']}}</a></div>
          <div class="cs-spb-04"><a href="#" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel">{{$translate['cancel']}}</a></div>
        </div>

        <div class="cs-spb-20 cs-spt-30">{{$translate['for_more_info_click']}} <a href="{{$moreInfoUrl}}" class="">{{$translate['here']}}</a></div>

       
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');

  $('.js-lightbox-cancel', $box).on('click', function(event) {
    $.fancybox.close();
    event.preventDefault();
  });

/*]]>*/</script>
