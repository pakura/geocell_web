<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
    <div class="tsr-com-collapser-outer">
        <div class="tsr-com-collapser-inner">
            <div class="cs-al-center">
                <div class="cs-spb-20 cs-tx-32 cs-tx-bold"> @if($lang == 'en') iPhone SE pre-order terms and conditions @else iPhone SE დაჯავშნის პირობები @endif </div>
                <div class="cs-spb-20 cs-spt-30">
                    <div class="tsr-forms">

                        <div class="cs-spb-20" style="text-align: left">
                            @if($lang == 'en')
                                Pre-order gives possibility to the customer to purchase iPhone SE before in-store sales.
                                <br><br>
                                In case of installment, pre-ordered iPhone SE can be received only if installment form is approved.
                                <br><br>
                                If customer doesn’t come to get order in agreed period of time , company is eligible to cancel pre-order. You’ll be informed about specified time on your number.
                                <br><br>
                                While getting pre-ordered iPhone SE priority has the customer that comes earlier than other.
                            @else
                                დაჯავშნა საშუალებას აძლევს მომხმარებელს შეიძინოს iPhone SE მაღაზიებში გაყიდვის დაწყებამდე.
                                <br><br>
                                განვადებით შეძენისას, დაჯავშნილი iPhone SE -ის გატანა შესაძლებელი იქნება მხოლოდ განვადების განაცხადის დადასტურების შემთხვევაში.
                                <br><br>
                                თუ მომხმარებელი დაჯავშნილ ტელეფონს არ მოაკითხავს შეთანხმებულ ვადაში, კომპანია უფლებას იტოვებს გააუქმოს ჯავშანი (ვადას შეიტყობთ ტელეფონის ჩამოსვლასთან ერთად).
                                <br><br>
                                დაჯავშნილი ტელეფონის გატანისას პრიორიტეტი მიენიჭება მომხმარებელს, რომელიც უფრო ადრე მოაკითხავს ტელეფონს.
                            @endif
                        </div>

                        <div class="cs-spb-20">
                            <div class="cs-spb-04"><a onclick="$('.fancybox-close').click()" class="tsr-lightbox-layout-a-button tsr-btn">{{$translate['close']}}</a></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">/*<![CDATA[*/

    var $box = $('#unique-lightbox-id');
    $('input[type="text"]', $box).inputmask();

    /*]]>*/</script>
