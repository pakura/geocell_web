<div class="tsr-dashboard-block-wrap-body">

    <?php 

        $return = array();

        foreach($available_services as $data) {
           $icon = $data->service_type_group==4?$data->meti_file:$data->file;
          $src = ($data->file)?'uploads/products/thumbs/'.$icon:'site/tsr-temp/tsr-images/service-01.png';
            $return[$data['service_type_group']][] = 

            '<div class="tsr-uboard-service">
              <div class="tsr-uboard-service-modal"></div>
              <div class="tsr-uboard-service-head">
                <div class="tsr-uboard-service-head-col-01">
                  <div class="tsr-clearfix">
                    <div class="tsr-uboard-service-link tsr-clearfix">
                      <figure class="tsr-uboard-service-image">
                        <img alt="" src="'.$src.'">
                      </figure>
                      <div class="tsr-uboard-service-desc">
                          <div class="tsr-uboard-service-title">'.$data->description.'</div>
                        
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tsr-uboard-service-head-col-02">
                  <div class="composite"><!--
                    --><div class="cs-spr-10"><div class="tsr-uboard-service-price"></div></div><!--
                    --><div class="cs-spl-10">
                    <a href="'.$lang.'/activate/'.($data->parent==119?"service-meti":"service").'/'.$data->product_id.($data->is_bundle?"?is_bundle=1":"").'" class="tsr-btn tsr-btn-xsmall js-fancybox fancybox.ajax" data-fancybox-type="ajax" >'.$translate['activate'].'</a>
                    

                    </div>

                    </div>

                </div>
              </div>
           
            </div>';
         

            
        }
           
       
    ?>

      @if(isset($return[4]))
      <div class="tsr-uboard-services tsr-uboard-services-scenario-a">  
        <div class="tsr-dashboard-block-wrap-body tsr-dashboard-block-wrap-head">
           <div class="tsr-uboard-service-head" onclick="$('#group-meti').slideToggle();" style="cursor:pointer">
              <div class="tsr-uboard-service-title">{{$translate['bucket_meti']}}</div>
           </div>

            <div class="tsr-uboard-service-body" id="group-meti">
              <div class="tsr-uboard-service-body-pad">
           @foreach($return[4] as $service)
              {!! $service !!} 
           @endforeach
              </div>
            </div>         
        </div>   
      </div>  
      @endif

       @if(isset($return[1]))
        <div class="tsr-uboard-services tsr-uboard-services-scenario-a">  
          <div class="tsr-dashboard-block-wrap-body tsr-dashboard-block-wrap-head">
             <div class="tsr-uboard-service-head" onclick="$('#group-internet').slideToggle();" style="cursor:pointer">
                <div class="tsr-uboard-service-title">{{$translate['internet']}}</div>
             </div>

              <div class="tsr-uboard-service-body" id="group-internet">
                <div class="tsr-uboard-service-body-pad">
             @foreach($return[1] as $service)
                {!! $service !!} 
             @endforeach
                </div>
              </div>         
          </div>   
        </div>  
      @endif

      @if(isset($return[2]))
      <div class="tsr-uboard-services tsr-uboard-services-scenario-a">  
        <div class="tsr-dashboard-block-wrap-body tsr-dashboard-block-wrap-head">
           <div class="tsr-uboard-service-head" onclick="$('#group-sms').slideToggle();" style="cursor:pointer">
              <div class="tsr-uboard-service-title">{{$translate['sms']}}</div>
           </div>

            <div class="tsr-uboard-service-body" id="group-sms">
              <div class="tsr-uboard-service-body-pad">
           @foreach($return[2] as $service)
              {!! $service !!} 
           @endforeach
              </div>
            </div>         
        </div> 
      </div>    
      @endif

      @if(isset($return[3]))
      <div class="tsr-uboard-services tsr-uboard-services-scenario-a">  
        <div class="tsr-dashboard-block-wrap-body tsr-dashboard-block-wrap-head">
           <div class="tsr-uboard-service-head" onclick="$('#group-roaming').slideToggle();" style="cursor:pointer">
              <div class="tsr-uboard-service-title">{{$translate['roaming']}}</div>
           </div>

            <div class="tsr-uboard-service-body" id="group-roaming">
              <div class="tsr-uboard-service-body-pad">
           @foreach($return[3] as $service)
              {!! $service !!} 
           @endforeach
              </div>
            </div>         
        </div> 
      </div>    
      @endif

      @if(isset($return[0]))
      <div class="tsr-uboard-services tsr-uboard-services-scenario-a">  
        <div class="tsr-dashboard-block-wrap-body tsr-dashboard-block-wrap-head">
           <div class="tsr-uboard-service-head" onclick="$('#group-other').slideToggle();" style="cursor:pointer">
              <div class="tsr-uboard-service-title">{{$translate['tariffs']}}</div>
           </div>

            <div class="tsr-uboard-service-body" id="group-other">
              <div class="tsr-uboard-service-body-pad">
           @foreach($return[0] as $service)
              {!! $service !!} 
           @endforeach
              </div>
            </div>         
        </div> 
      </div>    
      @endif

  
    </div>

    