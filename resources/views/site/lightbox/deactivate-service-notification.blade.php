<div class="tsr-lightbox-layout-a" id="remove-service-lightbox">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
        
        <div class="cs-spb-20 cs-tx-bold">
            @if($lang=='ge')
              გსურთ სერვისის "<span id="service-title">{{$service->description}}</span>" დეაქტივაცია? 
            @else 
              Do you want to remove service "<span id="service-title">{{$service->description}}</span>"?
            @endif    
        </div>
        <div class="cs-spb-20 cs-spt-30">



          <div class="cs-spb-04"><a href="{{$lang}}/dashboard/deactivate-service/{{$service->service_key}}" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise js-fancybox fancybox.ajax" data-fancybox-type="ajax"
                                    onclick="setTimeout(function(e) {$(this).removeAttr('href'); e.preventDefault();   },300);">{{$translate['ok']}}</a></div>

{{--setTimeout(function() {$(this).removeAttr('href');},300);--}}
          <div class="cs-spb-04 cancel-button"><a href="#" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel">{{$translate['cancel']}}</a></div>

        </div>

      </div>
    </div>
  </div>
</div>

{{--setTimeout(function(e) {$(this).removeAttr('href'); e.preventDefault();   },300);--}}
<script type="text/javascript">/*<![CDATA[*/
/*
  function myFunction(){
    console.log(55);
    return false;

  }*/
  var $box = $('#remove-service-lightbox');

  $('.js-lightbox-cancel', $box).on('click', function(event) {
    $.fancybox.close();
    event.preventDefault();
  });

/*]]>*/</script>
