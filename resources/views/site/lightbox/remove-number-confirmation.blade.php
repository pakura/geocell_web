<div class="tsr-lightbox-layout-a" id="unique-lightbox-id" >
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
        
        <div class="cs-spb-20 cs-tx-20">{{$number}} {{$translate['number_remove']}}</div>
     
        <div class="cs-spb-20 cs-spt-30 cs-mg-center" style="width: 260px;">
        <div class="cs-spb-20 cs-spt-30">
   
            <div class="cs-spb-20"><a href="{{$lang}}/remove-number/{{$number}}" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise js-fancybox fancybox.ajax" data-fancybox-type="ajax" >{{$translate['next']}}</a></div>
       
    
         <div class="cs-spb-04"><button class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel">{{$translate['cancel']}}</button></div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">/*<![CDATA[*/

  var $box = $('#unique-lightbox-id');

  
  $('.js-lightbox-cancel', $box).on('click', function(event) {
    $.fancybox.close();
    event.preventDefault();
  });
 
/*]]>*/</script>
