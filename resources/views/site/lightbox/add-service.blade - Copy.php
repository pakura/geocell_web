<div class="tsr-dashboard-block-wrap-body">
      <div class="tsr-uboard-services tsr-uboard-services-scenario-a">  
    
    <?php 

        $return = array();

        foreach($available_services as $data) {
            $return[$data['service_type_group']][] = 

            '<div class="tsr-uboard-service">
              <div class="tsr-uboard-service-modal"></div>
              <div class="tsr-uboard-service-head">
                <div class="tsr-uboard-service-head-col-01">
                  <div class="tsr-clearfix">
                    <div class="tsr-uboard-service-link tsr-clearfix">
                      <figure class="tsr-uboard-service-image">
                        <img alt="" src="site/tsr-temp/tsr-images/service-01.png">
                      </figure>
                      <div class="tsr-uboard-service-desc">
                          <div class="tsr-uboard-service-title">'.$data->description.'</div>
                        
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tsr-uboard-service-head-col-02">
                  <div class="composite"><!--
                    --><div class="cs-spr-10"><div class="tsr-uboard-service-price"></div></div><!--
                    --><div class="cs-spl-10">
                    <a href="'.$lang.'/activate/'.($data->parent==119?"service-meti":"service").'/'.$data->product_id.($data->is_bundle?"?is_bundle=1":"").'" class="tsr-btn tsr-btn-xsmall js-fancybox fancybox.ajax" data-fancybox-type="ajax" >'.$translate['activate'].'</a>
                    

                    </div>

                    </div>

                </div>
              </div>
           
            </div>';
         

            
        }
           
       
    ?>

      @if(isset($return[4]))
           @foreach($return[4] as $service)
              {{$service}} 
           @endforeach
      @endif

     @foreach($available_services as $data)
         
        <div class="tsr-uboard-service">
          <div class="tsr-uboard-service-modal"></div>
          <div class="tsr-uboard-service-head">
            <div class="tsr-uboard-service-head-col-01">
              <div class="tsr-clearfix">
                <div class="tsr-uboard-service-link tsr-clearfix">
                  <figure class="tsr-uboard-service-image">
                    <img alt="" src="site/tsr-temp/tsr-images/service-01.png">
                  </figure>
                  <div class="tsr-uboard-service-desc">
                      <div class="tsr-uboard-service-title">{{$data->description}}</div>
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="tsr-uboard-service-head-col-02">
              <div class="composite"><!--
                --><div class="cs-spr-10"><div class="tsr-uboard-service-price"></div></div><!--
                --><div class="cs-spl-10">
                <a href="{{$lang}}/activate/{{$data->parent==119?'service-meti':'service'}}/{{$data->product_id}}{{$data->is_bundle?'?is_bundle=1':''}}" class="tsr-btn tsr-btn-xsmall js-fancybox fancybox.ajax" data-fancybox-type="ajax" >{{$translate['activate']}}</a>
                

                </div>

                </div>

            </div>
          </div>
       
        </div>
         
      @endforeach

      </div>
    </div>

    