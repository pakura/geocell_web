@extends('site')

@section('content')

    <div class="tsr-section-generic cs-bg-shade cs-smt-02">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h cs-pdb-40">
                <h1 class="tsr-title cs-cl-blue">{{$item->title}}</h1>
                <div>{!! $item->description !!}</div>
            </div>
        </div>

        <div class="tsr-promo-splash-b" style="background-image: url('site/tsr-temp/tsr-images/promo-splash-b.png');"></div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->


    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h">
                <div class="cs-spb-20">{!! $item->content !!}</div>
                <div class="cs-spb-20">
                    <div class="tsr-module-ipacksbx">
                        <div class="tsr-boxgrid tsr-boxgrid-0{{count($packs)}} tsr-boxgrid-fit tsr-clearfix">
                            @foreach($packs as $k => $pack)
                                <div class="tsr-boxcol-outer">
                                    <div class="tsr-boxcol-inner">
                                        <a href="#" class="tsr-module-ipacksbx-item">
                                            <div class="cs-spb-20 cs-tx-bold cs-tx-26 tsr-module-ipacksbx-title">{{$pack->price}} @if($lang=='en') Lari @else ლარი @endif</div>
                                            <div class="cs-spb-00">
                                                <table class="tsr-module-ipacksbx-table">

                                                    <tr><td>@if($lang == 'en') Local calls @else ადგილობრივი ზარები @endif</td><td>{{$pack->local_calls}} @if($lang == 'en') Min @else წუთი @endif</td></tr>
                                                    <tr><td>@if($lang == 'en') Internet @else interneti @endif</td><td>{{$pack->internet}} @if($lang == 'en') MB @else მბ @endif</td></tr>
                                                    <tr><td>SMS</td><td>{{$pack->sms}}</td></tr>

                                                </table>
                                            </div>
                                            <div class="cs-spt-20">
                                                @if($lang == 'en')
                                                Validity: {{$pack->validity}} days
                                                @else
                                                ვადა: {{$pack->validity}} დღე
                                                @endif
                                            </div>
                                        </a>

                                        <a href="{{$lang}}/partner2/{{$pack->id}}" data-fancybox-type="ajax" class="tsr-module-ipacksbx-btn tsr-btn tsr-btn-xsmall js-fancybox fancybox.ajax">@if($lang == 'en') Purchase @else შეკვეთა @endif</a>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
                <div class="cs-spb-00">
                    <a class="cs-dp-block noline cs-clearfix">
                        <div class="cs-fl-left cs-tx-32"><span class="ts-icon-support"></span></div>
                        <div class="cs-spl-40 cs-tx-13 cs-tx-bold">დაგვიტოვეთ ტელეფონი,<br/>მენეჯერი დაკივაშირდებათ დეტალებზე</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- /tsr-section-generic -->


    <!-- tsr-section-divider -->
    <div class="tsr-section-popup-sticker" data-handle="popup-sticker-bot">
        <div class="tsr-popup-sticker-element" data-sticker-role="sticker" data-sticker-pos="bot">
            <div class="tsr-popup-sticker-theme-a">
                <div class="tsr-popup-sticker-close" data-sticker-role="close"><span class="ts-icon-delete"></span></div>
                <div class="tsr-container">
                    <div class="cs-al-center">
                        <div class="cs-mg-center cs-max-whole-x" style="width: 360px;">
                            <div class="cs-spb-20 cs-tx-bold">ასევე გთავაზობთ</div>
                            <div class="cs-spb-00">
                                <div class="cs-dp-table cs-whole-x">
                                    <div class="cs-dp-table-cell cs-al-top cs-al-left" style="width: 40%;">
                                        <a href="#" class="cs-dp-block"><img src="site/tsr-temp/tsr-images/sticker-promo-01.png" alt="" class="cs-max-whole-x" /></a>
                                    </div>
                                    <div class="cs-dp-table-cell cs-al-top cs-al-left cs-pdl-20">
                                        <div class="cs-spb-00 cs-tx-24"><a href="http://hronline.ge/" target="_blank" class="">HROnline.ge</a></div>
                                        <div class="cs-spb-00 cs-tx-13">თანამშრომლების მართვის  ვებ აპლიკაცია</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /tsr-section-divider -->


    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-color-purple">
        <div class="tsr-container">
            <h2>More Information</h2>
        </div>
    </div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
        <div class="tsr-module-tabular-a">
            <div class="tsr-module-tabular-header">
                <div class="tsr-container">
                    <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab1">Terms & Condition</a>
                    <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab2">Additional Information</a>
                </div>
            </div>
            <div class="tsr-module-tabular-footer">
                <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
                    <div class="tsr-container">
                        <div class="tsr-section-generic-pad">
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.<br><br></p>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.<br><br></p>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.<br><br></p>
                        </div>
                    </div>
                </div>
                <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
                    <div class="tsr-container">
                        <div class="tsr-section-generic-pad">
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<br><br></p>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<br><br></p>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<br><br></p>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<br><br></p>
                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).<br><br></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->
    <a href="{{$lang}}/partner2/done" data-fancybox-type="ajax" class="js-fancybox fancybox.ajax" style="display: none;" id="showOK"></a>
    <script>
        @if(\Session::has('mail'))
        $(function(){setTimeout(function () {
            $('#showOK').click();
        });});
        @endif
    </script>



@endsection