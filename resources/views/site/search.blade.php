@extends('site')

@section('content')
	
	{{--<div class="tsr-section-generic">--}}
        {{--<div class="tsr-container">--}}
            {{--<div style="margin-top: 32px; width: 100%; height: 100px">--}}
                {{--<form action="/developer_version/public/{{$lang}}/search" method="get">--}}
                    {{--<input type="search" id="searchInput" maxlength="60" placeholder="{{$translate['search']}}..." name="q" value="{{isset($q)?$q:''}}">--}}
                    {{--<input type="submit" value="{{$translate['search']}}" id="searchButton">--}}
                {{--</form>--}}
                {{--<br>--}}
                {{--<span>results: {{$displaytotal.'/'.$totalnum}} </span>--}}
            {{--</div>--}}

            {{--<div style="width: 100%; height: auto; min-height: 100px" id="searchContent">--}}
                {{--@foreach($output_data as $res)--}}
                    {{--<a href="{{$res['url']}}">--}}
                        {{--<div class="searchTitle">--}}
                            {{--{!! $res['title'] !!}--}}
                        {{--</div>--}}
                        {{--<div class="searchDesc">--}}
                            {{--{!! $res['text'] !!}}--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--@endforeach--}}
            {{--</div>--}}
            {{--<div style="margin-top: 20px; width: 100%; height: 100px">--}}

            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}


    <div class="tsr-section-generic">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h">
                <div class="cs-spb-10"><h1 class="tsr-title">{{$translate['search_results']}}</h1></div>
                <div class="cs-spb-20">
                    <form action="/developer_version/public/{{$lang}}/search" method="get">
                        <div class="tsr-search-form tsr-forms">
                            <div class="tsr-search-form-a"><input type="text" name="q" placeholder="{{$translate['search']}}..." value="{{isset($q)?$q:''}}"/></div>
                            <div class="tsr-search-form-b"><input type="submit"value="{{$translate['search']}}" class="tsr-btn tsr-btn-form tsr-btn-purple"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h">
                <div class="tsr-com-collapser-outer">
                    <div class="tsr-com-collapser-inner">
                        <div class="cs-spb-40">
                            <div>{{$translate['found']}} <b>{{$totalnum}}</b> {{$translate['results_for']}} <b>"{{isset($q)?$q:''}}"</b> {{$translate['tvis']}}.</div>
                        </div>
                        @if(isset($output_data) && sizeof($output_data)>0)
                            @foreach(@$output_data as $res)
                                <div class="cs-spb-40">
                                    <div><a href="{{$res['url']}}" class="cs-tx-22 noline">{!! $res['title'] !!}</a></div>
                                    <div>{!! $res['text'] !!}</div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /tsr-section-generic -->






















    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad">
                <div class="tsr-pagination tsr-pagination-left">
                    <div class="tsr-center">
                        @if($start > 9) <a href="{{$lang}}/search?q={{$_GET['q']}}&s=0" class="tsr-dir-first">First</a> @endif
                        @if($start > 9) <a href="{{$lang}}/search?q={{$_GET['q']}}&s={{$start-10}}" class="tsr-dir-previous">Previus</a> @endif

                            @if($start - 30 >= 0)<a href="{{$lang}}/search?q={{$_GET['q']}}&s={{$start-30}}">{{($start-20)/10}}</a> @endif
                            @if($start - 20 >= 0)<a href="{{$lang}}/search?q={{$_GET['q']}}&s={{$start-20}}">{{($start-10)/10}}</a> @endif
                            @if($start - 10 >= 0)<a href="{{$lang}}/search?q={{$_GET['q']}}&s={{$start-10}}">{{($start)/10}}</a> @endif
                            <a class="tsr-active">{{$start/10 + 1}}</a>
                            @if($start + 10 < $totalnum) <a href="{{$lang}}/search?q={{$_GET['q']}}&s={{$start+10}}">{{$start/10 + 2}}</a> @endif
                            @if($start + 20 < $totalnum) <a href="{{$lang}}/search?q={{$_GET['q']}}&s={{$start+20}}">{{$start/10 + 3}}</a> @endif
                            @if($start + 30 < $totalnum) <a href="{{$lang}}/search?q={{$_GET['q']}}&s={{$start+30}}">{{$start/10 + 4}}</a> @endif

                        @if($totalnum > 9) <a href="{{$lang}}/search?q={{$_GET['q']}}&s={{$start+10}}" class="tsr-dir-next">Next</a> @endif
                        @if($totalnum > 9) <a href="{{$lang}}/search?q={{$_GET['q']}}&s={{intval($totalnum / 10)*10}}" class="tsr-dir-last">Last</a> @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->


    <style>
        #searchInput{
            float: left;
            width: 37%;
            height: 36px;
            border: 1px solid #BBB;
            outline: none;
            border-radius: 5px;
            background: none repeat scroll 0% 0% white !important;
            padding-left: 6px;
        }
        #searchButton{
            margin-left: 12px;
            padding: 9px 18px;
            background: #642887;
            border: 1px solid #431F5D;
            color: #FFF;
            border-radius: 4px
        }
        #searchButton:hover{
            background: #7a2fa3;
        }
        .searchTitle{
            width: 100%;
            height: auto;
            font-size: 20px;
            color: #642887;
        }
        .searchDesc{
            width: 100%;
            height: auto;
            color: #666666;
            margin-bottom: 20px;
        }
        .searchDesc a:link{
            text-decoration: none;
        }
        .searchDesc a:hover{
            text-decoration: none;
        }
        .searchDesc a:focus{
            text-decoration: none;
        }
        .searchDesc a:visited{
            text-decoration: none;
        }
    </style>

@endsection
