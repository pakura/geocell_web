@extends('site')

@section('content')
<?php
$ids = array('500mb', '1gb', '2gb', '4gb', '6gb', '15gb');
$cntid = 0;
?>
<div class="tsr-section-generic tsr-section-generic-bd-bot cs-bg-shade cs-smt-02">
    <div class="tsr-container">
        <div class="tsr-section-generic-pad-h cs-pdb-40">
            <h1 class="tsr-title cs-cl-blue">{{$item->title}}</h1>
            <div>{!!  $item->description !!}</div>
        </div>
    </div>
</div>
<!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-empty"></div>
<!-- /tsr-section-divider -->

<!-- tsr-section-generic -->

<div class="tsr-section-generic">
    <div class="tsr-container">
        <div class="tsr-section-generic-pad-h" style="text-align: center">
            <div class="cs-spt-30 cs-spb-30">
                <div class="tsr-com-linklist" style="text-align: center">
                    <h4 class="tsr-title">{{$translate['choose_device_type']}}</h4>

                    <br>
                    <br>

                    <div class="tsr-com-linkmenu-a">
                    <ul class="cs-tx-13 cs-tx-bold"><!--
                --><li>

                            <a  onclick="device = 1; changeDevice(1);
                document.getElementById('resultdata').style.display = 'none';
                     document.getElementById('resulttable').style.display = 'none';
                      $('#metibtn').removeClass('is-choosen')" id="dev1">
                                <div class="tsr-image"><img src="/developer_version/public/site/tsr-temp/tsr-images/data-calc-x-01.png" alt="" height="60"></div>
                                <div class="tsr-title"><span>{{$translate['smartphon-data-calc']}}</span></div>
                            </a>

                        </li><!--
                --><li>

                            <a onclick="device = 2; changeDevice(2);
                     document.getElementById('resulttable').style.display = 'none';
                     document.getElementById('resultdata').style.display = 'block';
                  document.getElementById('resulttable').style.display = 'none'; $('#metibtn').removeClass('is-choosen')" id="dev2">
                                <div class="tsr-image"><img src="/developer_version/public/site/tsr-temp/tsr-images/data-calc-x-02.png" alt="" height="60"></div>
                                <div class="tsr-title"><span>{{$translate['geo_connect']}}</span></div>
                            </a>

                        </li><!--
              --></ul>
                        </div>
                </div>
            </div>


            <div style="display:none;" id="bundle">
                <div class="cs-spt-30 cs-spb-10 modem_hide" style="text-align: center">
                    <h4 class="tsr-title">{{$translate['recom_data_plane']}}</h4>
                </div>
                <div class="cs-spt-10 cs-spb-30 modem_hide" >
                    <div class="tsr-com-linkmenu-a">
                        <ul class="cs-tx-13 cs-tx-bold cs-al-center"><!--
                --><li onclick="service = 2;changeService(1);
                     document.getElementById('resultdata').style.display = 'block';
                     document.getElementById('resulttable').style.display = 'none';">
                                <a id="metibtn">
                                    <div class="tsr-image"><img src="/developer_version/public/site/tsr-temp/tsr-images/data-calc-01.png" alt="" height="40" class="cs-bd-round-04"></div>
                                    <div class="tsr-title">{{$translate['bucket_meti']}}</div>
                                </a>
                            </li><!--
                --><li onclick="service = 1;changeService(2);
                    document.getElementById('resultdata').style.display = 'block';
                     document.getElementById('resulttable').style.display = 'none';">
                                <a id="mobilenetbtn">
                                    <div class="tsr-image"><img src="/developer_version/public/site/tsr-temp/tsr-images/data-calc-02.png" alt="" height="40" class="cs-bd-round-04"></div>
                                    <div class="tsr-title">{{$translate['mob_internet_pack']}}</div>
                                </a>
                            </li><!--
              --></ul>
                    </div>

                </div>

            </div>

            <div style="display: none" id="resultdata">


                <div class="cs-spt-30 cs-spb-10" style="text-align: center">
                    <br><br>
                    <h4 class="tsr-title">{{$translate['enteramount']}}</h4>
                </div>

                <div class="cs-spt-40 cs-spb-40">
                    <div class="tsr-module-dtcalc">
                        <div class="tsr-module-dtcalc-grid tsr-clearfix">








                            <!--
                    --><div class="tsr-module-dtcalc-cell">
                                <div class="tsr-module-dtcalc-ln-a tsr-clearfix">
                                    <div class="tsr-module-dtcalc-name"><!--
                      --><span class="tsr-module-dtcalc-icon" data-icon="&#xe603"></span><!--
                      --><span class="tsr-module-dtcalc-text">{{$translate['email_sentrecived']}}</span><!--
                    --></div>
                                    <div class="tsr-module-dtcalc-xval"><b><span class="count" id="email">0</span> {{$translate['units']}}</b></div>
                                </div>
                                <div class="tsr-module-dtcalc-ln-b"><div class="js-dtcalc-slider" id="email_slide"></div></div>
                                <div class="tsr-module-dtcalc-ln-c tsr-clearfix">
                                    <div class="tsr-module-dtcalc-xmin">0</div>
                                    <div class="tsr-module-dtcalc-xmax">{{$maximals['email']}}</div>
                                </div>
                            </div><!--
                --><div class="tsr-module-dtcalc-cell">
                                <div class="tsr-module-dtcalc-ln-a tsr-clearfix">
                                    <div class="tsr-module-dtcalc-name"><!--
                      --><span class="tsr-module-dtcalc-icon" data-icon="&#xe607"></span><!--
                      --><span class="tsr-module-dtcalc-text">{{$translate['video_hour']}}</span><!--
                    --></div>
                                    <div class="tsr-module-dtcalc-xval"><b><span class="count" id="video">0</span> {{$translate['min']}}</b></div>
                                </div>
                                <div class="tsr-module-dtcalc-ln-b"><div class="js-dtcalc-slider" id="video_slide"></div></div>
                                <div class="tsr-module-dtcalc-ln-c tsr-clearfix">
                                    <div class="tsr-module-dtcalc-xmin">0</div>
                                    <div class="tsr-module-dtcalc-xmax">{{$maximals['video']}}</div>
                                </div>
                            </div><!--
                --><div class="tsr-module-dtcalc-cell">
                                <div class="tsr-module-dtcalc-ln-a tsr-clearfix">
                                    <div class="tsr-module-dtcalc-name"><!--
                      --><span class="tsr-module-dtcalc-icon" data-icon="&#xe635"></span><!--
                      --><span class="tsr-module-dtcalc-text">{{$translate['music_stream']}}</span><!--
                    --></div>
                                    <div class="tsr-module-dtcalc-xval"><b><span class="count" id="music">0</span> {{$translate['min']}}</b></div>
                                </div>
                                <div class="tsr-module-dtcalc-ln-b"><div class="js-dtcalc-slider" id="music_slide"></div></div>
                                <div class="tsr-module-dtcalc-ln-c tsr-clearfix">
                                    <div class="tsr-module-dtcalc-xmin">0</div>
                                    <div class="tsr-module-dtcalc-xmax" id="max_music">{{$maximals['music']}}</div>
                                </div>
                            </div><!--
                --><div class="tsr-module-dtcalc-cell">
                                <div class="tsr-module-dtcalc-ln-a tsr-clearfix">
                                    <div class="tsr-module-dtcalc-name"><!--
                      --><span class="tsr-module-dtcalc-icon" data-icon="&#xe64d"></span><!--
                      --><span class="tsr-module-dtcalc-text">{{$translate['app_game_mus_down']}}</span><!--
                    --></div>
                                    <div class="tsr-module-dtcalc-xval"><b><span class="count" id="apps">0</span> {{$translate['units']}}</b></div>
                                </div>
                                <div class="tsr-module-dtcalc-ln-b"><div class="js-dtcalc-slider" id="apps_slide"></div></div>
                                <div class="tsr-module-dtcalc-ln-c tsr-clearfix">
                                    <div class="tsr-module-dtcalc-xmin">0</div>
                                    <div class="tsr-module-dtcalc-xmax">{{$maximals['apps']}}</div>
                                </div>
                            </div><!--
                --><div class="tsr-module-dtcalc-cell">
                                <div class="tsr-module-dtcalc-ln-a tsr-clearfix">
                                    <div class="tsr-module-dtcalc-name"><!--
                      --><span class="tsr-module-dtcalc-icon" data-icon="&#xe63f"></span><!--
                      --><span class="tsr-module-dtcalc-text">{{$translate['web_hour']}}</span><!--
                    --></div>
                                    <div class="tsr-module-dtcalc-xval"><b><span class="count" id="web">0</span> {{$translate['min']}}</b></div>
                                </div>
                                <div class="tsr-module-dtcalc-ln-b"><div class="js-dtcalc-slider" id="web_slide"></div></div>
                                <div class="tsr-module-dtcalc-ln-c tsr-clearfix">
                                    <div class="tsr-module-dtcalc-xmin">0</div>
                                    <div class="tsr-module-dtcalc-xmax">{{$maximals['web']}}</div>
                                </div>
                            </div><!--
                --><div class="tsr-module-dtcalc-cell">
                                <div class="tsr-module-dtcalc-ln-a tsr-clearfix">
                                    <div class="tsr-module-dtcalc-name"><!--
                      --><span class="tsr-module-dtcalc-icon" data-icon="&#xe63e"></span><!--
                      --><span class="tsr-module-dtcalc-text">{{$translate['social_media_posts']}}</span><!--
                    --></div>
                                    <div class="tsr-module-dtcalc-xval"><b><span class="count" id="social">0</span> {{$translate['units']}}</b></div>
                                </div>
                                <div class="tsr-module-dtcalc-ln-b"><div class="js-dtcalc-slider" id="social_slide"></div></div>
                                <div class="tsr-module-dtcalc-ln-c tsr-clearfix">
                                    <div class="tsr-module-dtcalc-xmin">0</div>
                                    <div class="tsr-module-dtcalc-xmax">{{$maximals['social']}}</div>
                                </div>
                            </div><!--
                --><div class="tsr-module-dtcalc-cell modem_show" style="display: none">
                                <div class="tsr-module-dtcalc-ln-a tsr-clearfix">
                                    <div class="tsr-module-dtcalc-name"><!--
                      --><span class="tsr-module-dtcalc-icon" data-icon="&#xe67b"></span><!--
                      --><span class="tsr-module-dtcalc-text">{{$translate['video_call_hour']}}</span><!--
                    --></div>
                                    <div class="tsr-module-dtcalc-xval"><b><span class="count" id="video_call">0</span> {{$translate['min']}}</b></div>
                                </div>
                                <div class="tsr-module-dtcalc-ln-b"><div class="js-dtcalc-slider" id="video_call_slide" ></div></div>
                                <div class="tsr-module-dtcalc-ln-c tsr-clearfix">
                                    <div class="tsr-module-dtcalc-xmin">0</div>
                                    <div class="tsr-module-dtcalc-xmax">{{$maximals['videocall']}}</div>
                                </div>
                            </div><!--
                --><div class="tsr-module-dtcalc-cell modem_show" style="display: none">
                                <div class="tsr-module-dtcalc-ln-a tsr-clearfix">
                                    <div class="tsr-module-dtcalc-name"><!--
                      --><span class="tsr-module-dtcalc-icon" data-icon="&#xe643"></span><!--
                      --><span class="tsr-module-dtcalc-text">{{$translate['online_gaming']}}</span><!--
                    --></div>
                                    <div class="tsr-module-dtcalc-xval"><b><span class="count" id="games">0</span> {{$translate['min']}}</b></div>
                                </div>
                                <div class="tsr-module-dtcalc-ln-b"><div class="js-dtcalc-slider" id="games_slide"></div></div>
                                <div class="tsr-module-dtcalc-ln-c tsr-clearfix">
                                    <div class="tsr-module-dtcalc-xmin">0</div>
                                    <div class="tsr-module-dtcalc-xmax">{{$maximals['games']}}</div>
                                </div>
                            </div>
                        </div>



                        <!--
              --></div>
                </div>
            </div>




            <div id="resulttable" style="display: none">
                <div class="cs-spt-40 cs-spb-20">
                    <div class="cs-tx-20">{{$translate['total_mon_data_usage']}}: <span class="cs-tx-bold" id="usegb">0 GB</span></div>
                </div>
                <div class="cs-spt-10 cs-spb-30" id="modem_section" style="display: none">
                    <div class="tsr-module-ipacksdc tsr-module-ipacksdc-04">
                        <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">
                            <div class="tsr-boxcol-outer" id="mod1" style="width: 50%">
                                <div class="tsr-boxcol-inner" style="height: 130px" onclick="fillMaxModem(modem_max[0])">
                                    <div class="tsr-module-ipacksdc-item" style="height: 130px">
                                        <div class="cs-spt-00 cs-tx-24">6 {{$translate['gb']}}/{{$translate['month']}}</div>
                                        <div class="cs-spt-00 cs-tx-15 cs-tx-bold cs-cl-blue">{{$translate['price']}} 15 &#x20be; <span class="ts-icon-infoball cs-tx-normal cs-cl-gray js-ipacksdc-tooltip" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry."></span></div>
                                        <div class="cs-spt-10">*136*1# OK</div>
                                        <div class="cs-spt-04 cs-tx-13">&nbsp;</div>
                                    </div>
                                </div>
                            </div>

                            <div class="tsr-boxcol-outer" id="mod2" style=" width: 50%">
                                <div class="tsr-boxcol-inner" style="height: 130px;">
                                    <div class="tsr-module-ipacksdc-item" style="height: 130px" onclick="fillMaxModem(modem_max[1])">
                                        <div class="cs-spt-00 cs-tx-24">15 {{$translate['gb']}}/{{$translate['month']}}</div>
                                        <div class="cs-spt-00 cs-tx-15 cs-tx-bold cs-cl-blue">{{$translate['price']}} 30 &#x20be; <span class="ts-icon-infoball cs-tx-normal cs-cl-gray js-ipacksdc-tooltip" title="Lorem Ipsum is simply dummy text of the printing and typesetting industry."></span></div>
                                        <div class="cs-spt-10">*136*2# OK</div>
                                        <div class="cs-spt-04 cs-tx-13" id="modem_extra_wp" style="display: none;float: right;">
                                            <a href="/{{$lang}}/private/internet/for-modems/internet-packs-for-modems"> + {{$translate['internet']}} <span id="modem_extra">0 გბ</span></a>
                                        </div>
                                        <div class="cs-spt-04 cs-tx-13">&nbsp;</div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tsr-module-ipacksdc-xinfo"><a href="#">View all packages</a></div>
                    </div>
                </div>




                <div class="cs-spt-10 cs-spb-30" style="display: none" id="mobile_section">
                    <div class="tsr-module-ipacksdc tsr-module-ipacksdc-06">
                        <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">
                            @foreach($services as $service)
                                <div class="tsr-boxcol-outer" style="height: 130px" id="{{$ids[$cntid]}}" onclick="fillMax(maxLimit[{{$cntid}}])">
                                    <div class="tsr-boxcol-inner" style="height: 130px">
                                        <div class="tsr-module-ipacksdc-item" style="height: 130px">
                                            <div class="cs-spt-00 cs-tx-24">{{$service->title}}</div>
                                            <div class="cs-spt-00 cs-tx-15 cs-tx-bold cs-cl-blue">{{$translate['price']}} {{$service->value}} &#x20be;</div>
                                            <div class="cs-spt-10"><a href="{{$lang}}/activate/service/{{$service->product_id}}" class="tsr-module-ipacksbx-btn tsr-btn tsr-btn-xsmall js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['activate']}}</a>
                                            </div>
                                            @if($service->value == 30)
                                                <div class="cs-spt-04 cs-tx-13" id="mobile_extra_wp" style="display: none"><a href="/developer_version/public/{{$lang}}/private/services/internet/mobile-internet-bundles" class="cs-tx-underline">+ {{$translate['internet']}} <span id="mobile_extra"></span></a></div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <?php $cntid++; ?>
                            @endforeach


                        </div>

                    </div>
                </div>





                <div class="cs-spt-10 cs-spb-30" id="meti_section" style="display:none;">
                    <div class="tsr-module-ipacksdc tsr-module-ipacksdc-04">
                        <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">
                            <div class="tsr-boxcol-outer" id="metis"  onclick="fillMaxMeti(metiMaxLimit[0])">
                                <div class="tsr-boxcol-inner">
                                    <div class="tsr-module-ipacksdc-item" style="height: 130px">
                                        <div class="cs-spt-00 cs-tx-24">100 {{$translate['mb']}}/{{$translate['month']}}</div>
                                        <div class="cs-spt-00 cs-tx-15 cs-tx-bold cs-cl-blue">{{$translate['meti_s']}} - 5 &#x20be; <span class="ts-icon-infoball cs-tx-normal cs-cl-gray tooltipstered"
                                        title='
                                        @if($lang == 'en')
                                                <table>
                                                    <tr>
                                                        <td>Local calls&nbsp;&nbsp;&nbsp;</td>
                                                        <td>120 min</td>
                                                    </td>
                                                    <tr>
                                                        <td>Data</td>
                                                        <td>100 MB</td>
                                                    </td>
                                                    <tr>
                                                        <td>SMS</td>
                                                        <td>100</td>
                                                    </td>
                                                </table>
                                                <div class="cs-spb-00">Validity: 30 days</div>

                                                @else
                                                <table>
                                                    <tr>
                                                        <td>ადგილობრივ ქსელებზე&nbsp;&nbsp;&nbsp;</td>
                                                        <td>120&nbsp;წუთი</td>
                                                    </td>
                                                    <tr>
                                                        <td>ინტერნეტი</td>
                                                        <td>100 MB</td>
                                                    </td>
                                                    <tr>
                                                        <td>SMS</td>
                                                        <td>100</td>
                                                    </td>
                                                </table>
                                                <div class="cs-spb-00">სარგებლობის ვადა&nbsp;: 30&nbsp;დღე</div>
                                        @endif
                                        '></span></div>
                                        <div class="cs-spt-10"><a href="ge/activate/service-meti/11" class="tsr-module-metipack-btn tsr-btn tsr-btn-xsmall  js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['activate']}}</a></div>
                                        <div class="cs-spt-04 cs-tx-13">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tsr-boxcol-outer" id="metim" onclick="fillMaxMeti(metiMaxLimit[1])">
                                <div class="tsr-boxcol-inner">
                                    <div class="tsr-module-ipacksdc-item" style="height: 130px">
                                        <div class="cs-spt-00 cs-tx-24">300 {{$translate['mb']}}/{{$translate['month']}}</div>
                                        <div class="cs-spt-00 cs-tx-15 cs-tx-bold cs-cl-blue">{{$translate['meti_m']}} - 10 &#x20be; <span class="ts-icon-infoball cs-tx-normal cs-cl-gray tooltipstered" title='
                                        @if($lang == 'en')


                                                    <table>
                                                    <tr>
                                                        <td>On-net calls&nbsp;&nbsp;&nbsp;</td>
                                                        <td>Unlimited</td>
                                                    </td>
                                                    <tr>
                                                        <td>Data</td>
                                                        <td>300 MB</td>
                                                    </td>
                                                    <tr>
                                                        <td>SMS</td>
                                                        <td>1000</td>
                                                    </td>
                                                </table>
                                                <div class="cs-spb-00">Validity: 30 days</div>

                                                @else
                                                    <table>
                                                        <tr>
                                                            <td>შიდა &nbsp; ქსელში&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                            <td>უსასრულო</td>
                                                        </td>
                                                        <tr>
                                                            <td>ინტერნეტი</td>
                                                            <td>300 MB</td>
                                                        </td>
                                                        <tr>
                                                            <td>SMS</td>
                                                            <td>1000</td>
                                                        </td>
                                                    </table>
                                                    <div class="cs-spb-00">სარგებლობის ვადა&nbsp;: 30&nbsp;დღე</div>
                                        @endif'></span></div>
                                        <div class="cs-spt-10"><a href="ge/activate/service-meti/12" class="tsr-module-metipack-btn tsr-btn tsr-btn-xsmall  js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['activate']}}</a></div>
                                        <div class="cs-spt-04 cs-tx-13">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tsr-boxcol-outer" id="metiu" onclick="fillMaxMeti(metiMaxLimit[2])">
                                <div class="tsr-boxcol-inner">
                                    <div class="tsr-module-ipacksdc-item" style="height: 130px">
                                        <div class="cs-spt-00 cs-tx-24">1.0 {{$translate['gb']}}/{{$translate['month']}}</div>
                                        <div class="cs-spt-00 cs-tx-15 cs-tx-bold cs-cl-blue">{{$translate['meti_u']}} - 25 &#x20be; <span class="ts-icon-infoball cs-tx-normal cs-cl-gray tooltipstered" title='
                                         @if($lang == 'en')

                                                    <table>
                                                    <tr>
                                                        <td>Local calls&nbsp;&nbsp;&nbsp;</td>
                                                        <td>Unlimited</td>
                                                    </td>
                                                    <tr>
                                                        <td>Data</td>
                                                        <td>1 GB</td>
                                                    </td>
                                                    <tr>
                                                        <td>SMS</td>
                                                        <td>Unlimited</td>
                                                    </td>
                                                </table>
                                                <div class="cs-spb-00">Validity: 30 days</div>

                                                @else
                                                    <table>
                                                        <tr>
                                                            <td>ადგილობრივ ქსელებზე&nbsp;&nbsp;&nbsp;</td>
                                                            <td>უსასრულო</td>
                                                        </td>
                                                        <tr>
                                                            <td>ინტერნეტი</td>
                                                            <td>1 გბ</td>
                                                        </td>
                                                        <tr>
                                                            <td>SMS</td>
                                                            <td>უსასრულო</td>
                                                        </td>
                                                    </table>
                                                    <div class="cs-spb-00">სარგებლობის ვადა&nbsp;: 30&nbsp;დღე</div>

                                        @endif
                                        '></span></div>
                                        <div class="cs-spt-10"><a href="ge/activate/service-meti/13" class="tsr-module-metipack-btn tsr-btn tsr-btn-xsmall  js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['activate']}}</a></div>
                                        <div class="cs-spt-04 cs-tx-13">&nbsp;</div>
                                    </div>
                                </div>
                            </div>
                            <div class="tsr-boxcol-outer" id="metiup" onclick="fillMaxMeti(metiMaxLimit[3])">
                                <div class="tsr-boxcol-inner">
                                    <div class="tsr-module-ipacksdc-item" style="height: 130px">
                                        <div class="cs-spt-00 cs-tx-24">5.0 {{$translate['gb']}}/{{$translate['month']}}</div>
                                        <div class="cs-spt-00 cs-tx-15 cs-tx-bold cs-cl-blue">{{$translate['meti_u_p']}} - 50 &#x20be; <span class="ts-icon-infoball cs-tx-normal cs-cl-gray tooltipstered" title='
                                        @if($lang == 'en')
                                                    <table>
                                                      <tr>
                                                          <td>Local calls&nbsp;&nbsp;&nbsp;</td>
                                                          <td>Unlimited</td>
                                                      </td>
                                                      <tr>
                                                          <td>Data</td>
                                                          <td>5 GB</td>
                                                      </td>
                                                      <tr>
                                                          <td>SMS</td>
                                                          <td>Unlimited</td>
                                                      </td>
                                                  </table>
                                                  <div class="cs-spb-00">Validity: 30 days</div>

                                                  @else
                                                    <table>
                                                        <tr>
                                                            <td>ადგილობრივ ქსელებზე&nbsp;&nbsp;&nbsp;</td>
                                                            <td>უსასრულო</td>
                                                        </td>
                                                        <tr>
                                                            <td>ინტერნეტი</td>
                                                            <td>5 გბ</td>
                                                        </td>
                                                        <tr>
                                                            <td>SMS</td>
                                                            <td>უსასრულო</td>
                                                        </td>
                                                    </table>
                                                    <div class="cs-spb-00">სარგებლობის ვადა&nbsp;: 30&nbsp;დღე</div>
                                        @endif'></span></div>
                                        <div class="cs-spt-10"><a href="ge/activate/service-meti/15" class="tsr-module-metipack-btn tsr-btn tsr-btn-xsmall  js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['activate']}}</a></div>
                                        <div class="cs-spt-04 cs-tx-13" id="extra_wp" style="display:none;"><a href="/developer_version/public/{{$lang}}/private/services/internet/mobile-internet-bundles" class="cs-tx-underline">+ {{$translate['internet']}} <span id="extra"></span> </a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tsr-module-ipacksdc-xinfo"><a href="#">View all packages</a></div>
                    </div>

                </div>



        </div>
            <div class="cs-spt-40 cs-spb-20 cs-tx-13 cs-cl-gray">{!! $item->content !!}</div>
    </div>
</div>



<!-- *************** JAVASCRIPTS *************** -->
<!--[if lt IE 9]>
<script src="site/tsr-core/tsr-scripts/jquery/jquery-legacy.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="site/tsr-core/tsr-scripts/jquery/jquery.min.js"></script>
<!--<![endif]-->
<script src="site/tsr-core/tsr-scripts/jquery-easing/jquery.easing.min.js"></script>
<script src="site/tsr-core/tsr-scripts/jquery-placeholder/jquery.placeholder.js"></script>
<script src="site/tsr-core/tsr-scripts/jquery-fancybox/jquery.fancybox.patched.min.js"></script>
<script src="site/tsr-core/tsr-scripts/jquery-slideshow/jquery.slideshow.min.js"></script>
<script src="site/tsr-core/tsr-scripts/jquery-tooltipster/jquery.tooltipster.patched.min.js"></script>

<script src="site/tsr-core/tsr-scripts/jquery-ui/jquery-ui.min.js"></script>

<!--
<script src="tsr-core/tsr-scripts/tsr-core.js"></script>
<script src="tsr-components/tsr-forms/tsr-forms.js"></script>
<script src="tsr-sections/tsr-header/tsr-header.js"></script>
<script src="tsr-sections/tsr-footer/tsr-footer.js"></script>
<script src="tsr-sections/tsr-refiner/tsr-refiner.js"></script>
<script src="tsr-sections/tsr-support-listing/tsr-support-listing.js"></script>
 -->
<script src="site/tsr-modules/tsr-ipacksdc/tsr-ipacksdc.js"></script>

<!-- *************** /JAVASCRIPTS *************** -->

<script type="text/javascript">/*<![CDATA[*/
    var interval;
    var service = 2; //1=inter; 2=meti
    var device = 1; //1=smarphone; 2=modem;

    var calcData = JSON.parse('<?= json_encode($data_calc) ?>');
    var calclimits = JSON.parse('<?= json_encode($data_calc_limits) ?>');
    var modem_calclimits = JSON.parse('<?= json_encode($modem_data_calc) ?>');
    var maxLimit = JSON.parse('<?= json_encode($mobile_data_max) ?>');
    var metiMaxLimit = JSON.parse('<?= json_encode($meti_data_max) ?>');
    var modem_max = JSON.parse('<?= json_encode($modem_data_calc_limit) ?>');
    console.log('data: ');
    console.log(maxLimit);
    console.log('meti: ');
    console.log(metiMaxLimit);
    console.log('modem: ');
    console.log(modem_max);
    $(document).ready(function() {
        setTimeout(function(){
            collectData();
        },1200);
        /* tooltips */
        (function() {
            $( ".tooltipstered" ).each(function( index ) {
                $(this).tooltipster({
                    'maxWidth': 260,
                    'content': $(this).attr('title'),
                    'contentAsHTML': true,
                    'position': 'top',
                    'autoClose': true,
                    'interactive': true
                });
            });

        })();
        /* JUST DEMO (YOU SHOULDN'T USE THIS CODE) */
        (function() {
            $('.tsr-module-dtcalc-cell').each(function() {
                var $xval = $('.count', this);
                var update = function(val) {
                    $xval.html(val);
                };
                var max = $('.tsr-module-dtcalc-xmax', this).html();

                var ival = 0;

                $('.js-dtcalc-slider', this).slider({
                    range: 'min',
                    min: 0,
                    max: max,
                    value: ival,
                    slide: function(event, ui) {
                        update(ui.value);
                        clearTimeout(interval);
                        interval = setTimeout(function () {
                            collectData();
                        }, 100);
                    },
                    change: function(event, ui) {
                        if (event.originalEvent) {
                            document.getElementById('resulttable').style.display = 'block';
                        } else {
                            //programmatic change
                        }
                    }
                });

                update(ival);

            });
        })();


    });


    function collectData(){
        units = {
            email: parseInt($('#email').html()),
            music: parseInt($('#music').html()),
            video: parseInt($('#video').html()),
            web: parseInt($('#web').html()),
            apps: parseInt($('#apps').html()),
            social: parseInt($('#social').html()),
            videocall: parseInt($('#video_call').html()),
            games: parseInt($('#games').html())
        };
        initData(units);
    }





    function changeService(argument){

        if (argument == 1){
            $('#metibtn').addClass('is-choosen');
            $('#mobilenetbtn').removeClass('is-choosen');
            document.getElementById('mobile_section').style.display = 'none';
            document.getElementById('meti_section').style.display = 'block';
        } else {
            $('#mobilenetbtn').addClass('is-choosen');
            $('#metibtn').removeClass('is-choosen');
            document.getElementById('meti_section').style.display = 'none';
            document.getElementById('mobile_section').style.display = 'block';
        }

        collectData();
    }
    function initData(units){
        var sumServices = 0;
        for (var ii in units){
            if (device == 1){
                units[ii] *= calcData[ii];
            } else {
                units[ii] *= modem_calclimits[ii];
            }
            sumServices += units[ii];
        }

        $('#usegb').html(parseFloat(sumServices/ 1000).toFixed(1)+' {{$translate['gb']}}');
        initPack(sumServices, false);
    }

    function initPack(total, recur){
        if (device == 1) {
            if (service == 2 && recur == false) {
                var res = countMeti(total);
                if (typeof res == 'string') {
                    showRecomended(res, 0);
                } else {
                    initPack(res, true);
                }
            } else {
                if(service == 1 && recur == true){
                    showRecomended('15gb', countMobileData(total, false));
                } else {
                    if (recur == true) {
                        showRecomended('metiup', countMobileData(total, false));
                    } else {
                        var res = countMobileData(total, true);
                        if (typeof res == 'string') {
                            showRecomended(res, 0);
                        } else {
                            initPack(res, true);
                        }
                    }
                }
            }
        } else {

            if(recur == true){
                showRecomended('mod2', countModemData(total, false), 0);
            } else {
                var res = countModemData(total, false);
                if (typeof res == 'string') {
                    showRecomended(res, 0);
                } else {
                    initPack(res, true);
                }
            }
        }
    }


    function countMeti(total){
        var k = 99999999;
        var thispack = 'metiup';
        for (var ii in calclimits) {
            if (calclimits[ii].service == 2) {
                if (calclimits[ii].limit - total > 0 && k > calclimits[ii].limit - total) {
                    k = calclimits[ii].limit - total;
                    thispack = calclimits[ii].pack;
                }
            }
        }
        if (k == 99999999){
            return total-5000;
        } else {
            return thispack;
        }
    }


    function countMobileData(total , t){
        var k = 999999999;
        var thispack = '15gb';
        for (var ii in calclimits) {
            if (calclimits[ii].service == 1) {
                if (calclimits[ii].limit - total > 0 && k > calclimits[ii].limit - total) {
                    if(t == false){
                        k = calclimits[ii].limit - total;
                        thispack = calclimits[ii].pack;
                    } else {
                        if(calclimits[ii].limit != 21000 && calclimits[ii].limit != 30000 ){
                            k = calclimits[ii].limit - total;
                            thispack = calclimits[ii].pack;
                            service = 1;
                        }
                    }
                }
            }
        }
        if (k == 999999999){
            return total-15003;
        } else {
            return thispack;
        }
    }


    function countModemData(total, t){
        var k = 999999999;
        var thispack = 'mod2';
        for (var ii in calclimits) {
            if (calclimits[ii].service == 3) {
                if (calclimits[ii].limit - total > 0 && k > calclimits[ii].limit - total) {
                    if(t == false){
                        k = calclimits[ii].limit - total;
                        thispack = calclimits[ii].pack;
                    } else {
                        if(calclimits[ii].limit != 21000 && calclimits[ii].limit != 30000 ){
                            k = calclimits[ii].limit - total;
                            thispack = calclimits[ii].pack;
                            service = 1;
                        }
                    }

                }
            }
        }
        if (k == 999999999){
            return total-15003;
        } else {
            return thispack;
        }
    }


    function showRecomended(argument, extra){
        $('.tsr-module-ipacksdc-item').css({'height': '130px'});
        $('.tsr-boxcol-outer').removeClass('is-choosen');
        $('#'+argument.replace('1.2gb','1gb')).addClass('is-choosen');
        if (extra != 0){

            if(device == 2){
                $('#modem_extra_wp').fadeIn();
                $('#modem_extra').html(extra.replace('mod1', '6 {{$translate['gb']}}').replace('mod2', '15 {{$translate['gb']}}'));
            } else {
                if(service == 2){
                    $('#extra_wp').fadeIn();
                    $('#extra').html(extra.replace('gb', ' {{$translate['gb']}}').replace('mb', ' {{$translate['mb']}}'));
                }
                if(service == 1){
                    $('#mobile_extra_wp').fadeIn();
                    $('#mobile_extra').html(extra.replace('gb', ' {{$translate['gb']}}').replace('mb', ' {{$translate['mb']}}'));
                }
            }

        } else {

            $('#extra_wp').fadeOut();

            $('#mobile_extra_wp').fadeOut();

            $('#modem_extra_wp').fadeOut();
        }
    }

    function changeDevice(argument){
        if (argument == 1){


            document.getElementById('bundle').style.display = 'block';
            device = 1;
            $('#dev1').addClass('is-choosen');
            $('#dev2').removeClass('is-choosen');
            document.getElementById('modem_section').style.display = 'none';
            $('.modem_hide').fadeIn();
            $('.modem_show').fadeOut();
            $( "#music_slide" ).slider({
                max: 250
            });
            $('#max_music').html('250');
            changeService(1);
        } else {
            device = 2;
            $('#dev1').removeClass('is-choosen');
            $('#dev2').addClass('is-choosen');
            document.getElementById('mobile_section').style.display = 'none';
            document.getElementById('meti_section').style.display = 'none';
            document.getElementById('modem_section').style.display = 'block';
            $('.modem_hide').fadeOut();
            $('.modem_show').fadeIn();
            $( "#music_slide" ).slider({
                max: 150
            });
            $('#max_music').html('150');
            if (parseInt($('#music').html()) > 150){
                $('#music').html('150');
            }
            collectData();
        }
    }

    function fillMax(arg){
        for (var ii in arg){
            $( "#"+ii+"_slide" ).slider({
                value: arg[ii]
            });
            $('#'+ii).html(arg[ii]);
        }
        collectData();
    }


    function fillMaxMeti(arg){
        for (var ii in arg){
            $( "#"+ii+"_slide" ).slider({
                value: arg[ii]
            });
            $('#'+ii).html(arg[ii]);
        }
        collectData();
    }

    function fillMaxModem(arg){
        console.log(arg);
        for (var ii in arg){
            $( "#"+ii+"_slide" ).slider({
                value: arg[ii]
            });
            $('#'+ii).html(arg[ii]);
        }
        collectData();
    }


    /*]]>*/</script>





@endsection