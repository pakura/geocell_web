@if(count($gallery)>0) 

 <!-- tsr-section-hero -->
    <div class="tsr-section-hero">
      <div class="tsr-hero">
        <div class="tsr-hero-stage">
          <div class="tsr-slides">
          @foreach($gallery as $gal)
            <a href="{{$gal->link?$gal->link:'#'}}" class="tsr-slide">
              <div class="tsr-hero-showical" data-bgi-v1="{{$gal->file}}" data-bgi-v2="{{$gal->title_file}}"></div>
              <div class="tsr-hero-tactical">
                @if($gal->description)
                <figure class="tsr-tactical-soarPanel tsr-tactical-speach-bubble tsr-color-pink" style="top: 30px; right: 38px;">
                  <span class="tsr-header">{!!$gal->description!!}</span>
                  <!-- <span class="tsr-text">Jumps over the lazy dog, jump<br>the lazy dog lorem ipsum</span> -->
                </figure>
                @endif
              </div>

              @if($gal->title || $gal->short_title)
	          <div class="tsr-hero-promocal">
	            <div class="tsr-container">
	              <figure class="tsr-tactical-textPanel">
	                 <span class="tsr-header">{{$gal->title}}</span>
	              	 <span class="tsr-text">{{$gal->short_title}}</span>
	              </figure>
	            </div>
	          </div>
	          @endif

            </a>
            @endforeach

          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-hero -->



@endif