@extends('site')

@section('content')
    
    @include('site.textcontent_default')


    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-magazine-listing -->
    <div class="tsr-section-magazine-listing">
      <div class="tsr-container">
        <div class="cs-spb-40">
          
           @if($startYear)
          
              <form action="{{Request::url()}}?page=2" method="get" >
                <div class="tsr-forms">
                  <div class="tsr-clearfix">

                    <div class="cs-fl-left cs-spb-10 cs-spr-10" style="min-width: 120px; max-width: 260px;">

                        <select onchange="this.form.submit()" name="year">    
                          <option value=''>{{trans('site.year')}}</option>
                          @for($i=$startYear;$i<=date('Y');$i++)
                          <option value= "{{$i}}" {{ (@$_GET['year']==$i)?'selected':'' }}>{{$i}}</option>
                          @endfor                         
                        </select>

                    </div>
                    <div class="cs-fl-left cs-spb-10" style="min-width: 180px; max-width: 260px;">
                        <select onchange="this.form.submit()" name="month">
                          <option value=''>{{trans('site.month') }}</option>
                          @foreach (trans('site.months') as $key=>$month)
                            <?php $val = $key+1; ?>  
                            <option value="{{$val}}" {{ (@$_GET['month']==$val)?'selected':'' }}>{{$month}}</option> 
                          @endforeach  
                        </select>
                    </div>

                  </div>
                </div>
              </form>
             
             @endif

        </div>
        <div class="tsr-grid tsr-grid-collapse tsr-clearfix">
          @foreach($magazines as $magazine)
          <div class="tsr-col-03">
            <a href="javascript:;" class="tsr-module-magazine">
              <figure class="tsr-module-magazine-image">
                <img src="{{$magazine->file}}" alt="" />
              </figure>
              <header class="tsr-module-magazine-title">{{$magazine->title}}</header>
              <span class="tsr-module-magazine-date">{{$magazine->creation_date}}</span>
            </a>
          </div>
         @endforeach 
        </div>
      </div>
    </div>
    <!-- /tsr-section-magazine-listing -->



    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->


    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad">
          <div class="tsr-pagination tsr-pagination-left">
            <div class="tsr-center">
              {!! $magazines->render() !!}	
               
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty tsr-color-white"></div>
    <!-- /tsr-section-divider -->
       

    @endsection