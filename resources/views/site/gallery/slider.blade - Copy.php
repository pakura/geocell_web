@if(count($gallery)>0) 
<!-- tsr-section-hero -->
<div class="tsr-section-hero">
  <div class="tsr-slides">
  		@foreach($gallery as $item)
	        <a href="{{$item->link}}" class="tsr-slide">
	          <div class="tsr-hero-showical" style="background-image: url({{$item->file}});"></div>
	          @if($item->description)
	          <div class="tsr-hero-tactical">
	            <figure class="tsr-tactical-soarPanel tsr-tactical-speach-bubble tsr-color-pink" style="top: 30px; right: 38px;">
	              <span class="tsr-header">{!!$item->description!!}</span>
	             <!--  <span class="tsr-text">The quick brown fox jumps over the lazy dog</span> -->
	            </figure>
	          </div>
	          @endif

	          @if($item->title || $item->short_title)
	          <div class="tsr-hero-promocal">
	            <div class="tsr-container">
	              <figure class="tsr-tactical-textPanel">
	                 <span class="tsr-header">{{$item->title}}</span>
	              	 <span class="tsr-text">{{$item->short_title}}</span>
	              </figure>
	            </div>
	          </div>
	          @endif
	        </a>
  		@endforeach
  </div>
</div>
<!-- /tsr-section-hero -->
@endif