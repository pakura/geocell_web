@extends('site')

@section('content')
<div class="tsr-section-divider">
    <div class="tsr-container">
        <p>
            @if($lang == 'en')
                “Partner 2” – special offer for Growing Businesses including local calls, SMS and internet in one unified package. From three different packages you can choose the one that suits your business.
                Together with the offer “Partner” you can buy unlimited talks and SMS among your company employees for 1 GEL a month.
            @else
                „პარტნიორი 2“ - სპეციალური შეთავაზება მზარდი ბიზნესებისთვის, რომელშიც გაერთიანებულია სასაუბრო დრო, sms-ები და მობილური ინტერნეტი.  სამი შესაძლო ვარიანტიდან შეგიძლიათ აირჩიოთ თქვენს ბიზნესზე მორგებული პაკეტი ფასის და პირობების მიხედვით.
                შეთავაზება „პარტნიორთან“ ერთად შეგიძლიათ შეიძინოთ თანამშრომლებთან ულიმიტო საუბრის და sms-ების სერვისი თვეში მხოლოდ 1 ლარად.
            @endif


            
        </p>

        <p><br /><br /><br /></p>
        @if($lang == 'en')
        <div class="tsr-module-metipacks tsr-module-metipacks-processed">
            <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">
                <div class="tsr-boxcol-outer">
                    <div class="tsr-boxcol-inner" style="width: 90%; padding: 5%;">
                        <figure class="tsr-module-metipack-image tsr-module-metipack-image-40" style="background-image: url('https://geocell.ge/developer_version/public/cms/pages/edit/uploads/products/service-sprite-meti-m.png');"></figure>
                        <div class="cs-spb-10 cs-tx-bold cs-tx-26">5 Gel</div>
                        <div class="cs-spt-20 cs-spb-10">
                            <table class="tsr-module-metipack-table" style="width: 100%;">
                                <tbody>
                                <tr>
                                    <td><span style="color: #0083be;">Local calls</span>&nbsp;&nbsp;</td>
                                    <td>2&nbsp;hour</td>
                                </tr>
                                <tr>
                                    <td><span style="color: #0083be;">Data</span></td>
                                    <td>200&nbsp;MB</td>
                                </tr>
                                <tr>
                                    <td><span style="color: #0083be;">SMS</span></td>
                                    <td>200</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="cs-spb-00">Validity: 30 days</div>
                    </div>
                </div>
                <div class="tsr-boxcol-outer">
                    <div class="tsr-boxcol-inner" style="width: 90%; padding: 5%;">
                        <figure class="tsr-module-metipack-image tsr-module-metipack-image-40" style="background-image: url('https://geocell.ge/developer_version/public/cms/pages/edit/uploads/products/service-sprite-meti-m.png');"></figure>
                        <div class="cs-spb-10 cs-tx-bold cs-tx-26">8&nbsp;Gel</div>
                        <div class="cs-spt-20 cs-spb-10">
                            <table class="tsr-module-metipack-table" style="width: 100%;">
                                <tbody>
                                <tr>
                                    <td><span style="color: #0083be;">Local calls&nbsp;&nbsp;</span></td>
                                    <td>3&nbsp;hour</td>
                                </tr>
                                <tr>
                                    <td><span style="color: #0083be;">Data</span></td>
                                    <td>1&nbsp;GB</td>
                                </tr>
                                <tr>
                                    <td><span style="color: #0083be;">SMS</span></td>
                                    <td>300</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="cs-spb-00">Validity: 30 days</div>
                    </div>
                </div>
                <div class="tsr-boxcol-outer">
                    <div class="tsr-boxcol-inner" style="width: 90%; padding: 5%;">
                        <figure class="tsr-module-metipack-image tsr-module-metipack-image-40" style="background-image: url('https://geocell.ge/developer_version/public/cms/pages/edit/uploads/products/service-sprite-meti-u.png');"></figure>
                        <div class="cs-spb-10 cs-tx-bold cs-tx-26">12&nbsp;Gel</div>
                        <div class="cs-spt-20 cs-spb-10">
                            <table class="tsr-module-metipack-table" style="width: 100%;">
                                <tbody>
                                <tr>
                                    <td><span style="color: #0083be;">Local calls</span></td>
                                    <td>5&nbsp;hour</td>
                                </tr>
                                <tr>
                                    <td><span style="color: #0083be;">Data</span></td>
                                    <td>1.5 GB</td>
                                </tr>
                                <tr>
                                    <td><span style="color: #0083be;">SMS</span></td>
                                    <td>500</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="cs-spb-00">Validity: 30 days</div>
                    </div>
                </div>
            </div>
        </div>
            @else
            <div class="tsr-module-metipacks tsr-module-metipacks-processed">
                <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">
                    <div class="tsr-boxcol-outer">
                        <div class="tsr-boxcol-inner" style="width: 90%; padding: 5%;">
                            <figure class="tsr-module-metipack-image tsr-module-metipack-image-40" style="background-image: url('https://geocell.ge/developer_version/public/cms/pages/edit/uploads/products/service-sprite-meti-m.png');"></figure>
                            <div class="cs-spb-10 cs-tx-bold cs-tx-26">5 ლარი</div>
                            <div class="cs-spt-20 cs-spb-10">
                                <table class="tsr-module-metipack-table" style="width: 100%;">
                                    <tbody>
                                    <tr>
                                        <td><span style="color: #0083be;">ადგილობრივ ქსელებზე&nbsp;</span></td>
                                        <td>2&nbsp;საათი</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p><span style="color: #0083be;">ინტერნეტი&nbsp;</span></p>
                                        </td>
                                        <td>200&nbsp;მბ</td>
                                    </tr>
                                    <tr>
                                        <td><span style="color: #0083be;">SMS</span></td>
                                        <td>200</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="cs-spb-00">სარგებლობის ვადა&nbsp;: 30&nbsp;დღე</div>
                        </div>
                    </div>
                    <div class="tsr-boxcol-outer">
                        <div class="tsr-boxcol-inner" style="width: 90%; padding: 5%;">
                            <figure class="tsr-module-metipack-image tsr-module-metipack-image-40" style="background-image: url('https://geocell.ge/developer_version/public/cms/pages/edit/uploads/products/service-sprite-meti-m.png');"></figure>
                            <div class="cs-spb-10 cs-tx-bold cs-tx-26">8 ლარი</div>
                            <div class="cs-spt-20 cs-spb-10">
                                <table class="tsr-module-metipack-table" style="width: 100%;">
                                    <tbody>
                                    <tr>
                                        <td><span style="color: #0083be;">ადგილობრივ ქსელებზე&nbsp;</span></td>
                                        <td>3 საათი</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p><span style="color: #0083be;">ინტერნეტი&nbsp;</span></p>
                                        </td>
                                        <td>1 გბ</td>
                                    </tr>
                                    <tr>
                                        <td><span style="color: #0083be;">SMS</span></td>
                                        <td>300</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="cs-spb-00">სარგებლობის ვადა&nbsp;: 30&nbsp;დღე</div>
                        </div>
                    </div>
                    <div class="tsr-boxcol-outer">
                        <div class="tsr-boxcol-inner" style="width: 90%; padding: 5%;">
                            <figure class="tsr-module-metipack-image tsr-module-metipack-image-40" style="background-image: url('https://geocell.ge/developer_version/public/cms/pages/edit/uploads/products/service-sprite-meti-u.png');"></figure>
                            <div class="cs-spb-10 cs-tx-bold cs-tx-26">12&nbsp;ლარი</div>
                            <div class="cs-spt-20 cs-spb-10">
                                <table class="tsr-module-metipack-table" style="width: 100%;">
                                    <tbody>
                                    <tr>
                                        <td><span style="color: #0083be;">ადგილობრივ ქსელებზე&nbsp;</span></td>
                                        <td>5 საათი</td>
                                    </tr>
                                    <tr>
                                        <td><span style="color: #0083be;">ინტერნეტი&nbsp;</span></td>
                                        <td>1.5 GB</td>
                                    </tr>
                                    <tr>
                                        <td><span style="color: #0083be;">SMS</span></td>
                                        <td>500</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="cs-spb-00">სარგებლობის ვადა&nbsp;: 30&nbsp;დღე</div>
                        </div>
                    </div>
                    <div class="tsr-boxcol-outer">&nbsp;</div>
                </div>
            </div>

        @endif
    </div>
</div>




<div class="tsr-section-divider tsr-color-purple">
    <div class="tsr-container">
        <h2>
            @if($lang == 'en')
                More information
            @else
                მეტი ინფორმაცია
            @endif</h2>
    </div>
</div>



<div class="tsr-section-divider">
    <div class="tsr-container">
        <div class="tsr-section-generic">
            <div class="tsr-module-tabular-a tsr-module-tabular-a-processed tsr-count-2">
                <div class="tsr-module-tabular-header">
                    <div class="tsr-container">
                        <a onclick="termsncontd(1)" id="tab1" class="tsr-module-tabular-but is-selected" style="height: 59px;" data-tabular-exp-target="tab1">
                            @if($lang == 'en')
                                Price &amp; Conditions
                            @else
                                პირობები
                            @endif
                        </a>
                        <a onclick="termsncontd(2)" id="tab2" class="tsr-module-tabular-but" style="height: 59px;" data-tabular-exp-target="tab2">
                            @if($lang == 'en')
                                Terms &amp; Conditions
                            @else
                                დამატებითი ინფორმაცია
                            @endif
                        </a></div>
                    </div>



























                <div class="tsr-module-tabular-footer" >
                    <div id="tab2cont" class="tsr-module-tabular-tab is-expanded" data-tabular-exp-anchor="tab2">
                        <div class="tsr-container">
                            <div class="tsr-section-generic-pad">
                              {!! $item->description  !!}
                            </div>
                        </div>
                    </div>

                    <div id="tab1cont" class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
                        <div class="tsr-container">
                            <div class="tsr-section-generic-pad">
                                {!! $item->content  !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        function termsncontd(arg){
            if(arg == 1){
                $('#tab1').addClass('is-selected');
                $('#tab2').removeClass('is-selected');
                $('#tab2cont').addClass('is-expanded');
                $('#tab1cont').removeClass('is-expanded');
            } else {
                $('#tab2').addClass('is-selected');
                $('#tab1').removeClass('is-selected');
                $('#tab1cont').addClass('is-expanded');
                $('#tab2cont').removeClass('is-expanded');
            }
        }
    </script>






@endsection