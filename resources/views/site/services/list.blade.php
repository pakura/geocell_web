@extends('site')

@section('content')
    
    @include('site.textcontent_default')


     <!-- tsr-section-ps-listing -->
    <div class="tsr-section-ps-listing">
      <div class="tsr-container">
        <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">
          
          @foreach($services as $service)
              @if($service->product_id==66 && \Request::segment(2)=='business') <?php continue; ?>  @endif 
          <div class="tsr-boxcol-outer">
            <div class="tsr-boxcol-inner">
              <a href="{{ $lang.'/'.$item->full_slug.'/'.$service->slug }}" class="tsr-module-service">
                <div class="tsr-clearfix">
                  <figure class="tsr-module-service-image">
                    <img src="{{$service->file?'uploads/products/thumbs/'.$service->file:'site/tsr-temp/tsr-images/service-01.png'}}" alt="" />
                    <!-- <figure class="tsr-tactical-ribbon"><span>New</span></figure>
                    <figure class="tsr-tactical-ribbon-usp">4G<span></span></figure> -->
                  </figure>
                  <div class="tsr-module-service-desc">
                    <header class="tsr-module-service-title">{{$service->title}}</header>
<!--                     <span class="tsr-module-service-intro">{!!$service->description!!}</span>
 -->                   
                     <!--     @if( $service->value)
                          <span class="tsr-module-service-price">
                            {{$translate['price']}} {{ round($service->value,2) }} 
                            {{$service->daily?$translate['gel_day']:$translate['gel_month']}}
                          </span>
                        @endif -->

                        
                  </div>
                </div>
              </a>
            </div>
          </div>
          @endforeach

          @foreach($children as $child)


          <div class="tsr-boxcol-outer">
            <div class="tsr-boxcol-inner">
              <a href="{{ $lang.'/'.$child->full_slug }}" class="tsr-module-service">
                <div class="tsr-clearfix">
                  <figure class="tsr-module-service-image">
                    <img src="{{$child->file?'uploads/pages/thumbs/'.$child->file:'site/tsr-temp/tsr-images/service-01.png'}}" alt="" />
                   <!--  <figure class="tsr-tactical-ribbon"><span>New</span></figure>
                    <figure class="tsr-tactical-ribbon-usp">4G<span></span></figure> -->
                  </figure>
                  <div class="tsr-module-service-desc">
                    <header class="tsr-module-service-title">{{$child->title}}</header>
                    <!-- <span class="tsr-module-service-intro">{!!$child->description!!}</span> -->
                   <!--  <span class="tsr-module-service-price">Price fr. 12 $/month</span> -->
                  </div>
                </div>
              </a>
            </div>
          </div>
          @endforeach

        </div>
      </div>
    </div>
    <!-- /tsr-section-ps-listing -->

    @endsection