
@extends('site')

@section('content')

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <h1 class="tsr-title">{{$item->title }}</h1>
          <div class="cs-spb-20"> {!! $item->description !!} </div>

          <div class="tsr-module-ipacksbx">
            <div class="tsr-boxgrid {{count($services)>4?'tsr-boxgrid-06':''}} tsr-boxgrid-fit tsr-clearfix">
              <?php
                Session::forget('url');
                $btncnt = 0;
              ?>

              @foreach($services as $service)
              <?php $isBundle=$service->collection_id!=68?'?is_bundle=1':''; ?>
              <div class="tsr-boxcol-outer">
                <div class="tsr-boxcol-inner">
                  <div class="tsr-module-ipacksbx-item">
                    <div class="cs-spb-00 cs-tx-bold cs-tx-26 tsr-module-ipacksbx-title">{{$service->title  }}</div>
                    <div class="cs-spb-20 cs-tx-bold cs-tx-15">{{$translate['price']}} {{$service->value}} {{$translate['gel']}}</div>
                    @if(isset($service->ussd_code)  && $service->ussd_code!='' && !$service->sku)
                    <div class="cs-spb-02">{{$service->ussd_code.' OK' }} </div>
                    @endif
                    <div class="cs-spb-00">{{$service->daily?$translate['validity_daily']:$translate['validity']}}</div>
                  </div>
                  @if($service->sku)
                  <a href="{{$lang}}/activate/service/{{$service->product_id.$isBundle}}" class="tsr-module-ipacksbx-btn tsr-btn tsr-btn-xsmall js-fancybox fancybox.ajax" id="service_btn_{{$btncnt++}}" data-fancybox-type="ajax" onclick="saveAction(this)">{{$translate['purchase']}}</a>

                  @endif
                </div>
              </div>
              @endforeach

              </div>
          </div>
            <!----asdashdkjaskdaskjdhkja-->

            <div class="cs-spt-40 cs-spb-20">
                {!! $item->content !!}
            </div>


            <br><br>
            <div style="float:right;"><a href="/developer_version/public/{{$lang}}/private/help-support/useful-information/data-calculator">@if($lang == 'en') Data calculator @else ინტერნეტ კალკულატორი @endif</a> </div>


          <div class="cs-spt-40 cs-spb-20"><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.geocell.ge%2F&amp;locale=en_US&amp;layout=button_count&amp;show_faces=false&amp;action=like&amp;font=arial&amp;colorscheme=light&amp;width=200&amp;height=40" frameborder="0" scrolling="no" allowtransparency="true" style="background: transparent; border: 0; width: 100%; height: 40px;"></iframe></div>

        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

 @if(isset($service)&& ($service->services_and_features || $service->terms_and_conditions) )
    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-color-purple">
      <div class="tsr-container">
        <h2>{{$translate['more_info']}}</h2>
      </div>
    </div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-module-tabular-a">
        <div class="tsr-module-tabular-header">
          <div class="tsr-container">
             <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab1">{{$translate['price_con']}}</a>
            <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab2">{{$translate['terms_con']}}</a>
          </div>
        </div>
        <div class="tsr-module-tabular-footer">
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad" id="plane_table">
                {!! $service->services_and_features !!}

              </div>
            </div>
          </div>
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad">
                {!! $service->terms_and_conditions !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-ge -->
<script type="text/javascript">

  $(function(){
      var url = window.location.href;
      url = url.split('?')[1];
      if (url == 'next'){

          if (localStorage){
            var obj_btn = localStorage.lastaction;
            setTimeout(function(){
              $('#'+obj_btn).click()
            },1000);
          }
      } else {
          localStorage.removeItem("lastaction");
      }
  });

  function saveAction(obj){
    if (typeof localStorage.lastaction != 'undefined'){
       localStorage.removeItem("lastaction");
    } else {
        localStorage.setItem("lastaction", obj.id);
    }

  }

</script>

    @endif

    @endsection  

