@extends('site')

@section('content')
    
    @include('site.textcontent_default')


     <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <div class="cs-spb-30">
            <div class="tsr-com-linklist" >
              <ul class="cs-tx-13 cs-tx-bold"  ><!--





                --><li><a href="{{$lang}}/{{$item->full_slug}}" data-js-cgroup="0">{{$translate['all']}}</a></li><!--
                --><li><a href="{{$lang}}/{{$item->full_slug}}?discount=1" data-js-cgroup="1" class="{{@$_GET['discount']==1? 'is-choosen' :''}}">{{$translate['with_discounts']}}</a></li><!--
                --><li><a href="{{$lang}}/{{$item->full_slug}}?mobile_internet=1" data-js-cgroup="2" class="{{@$_GET['mobile_internet']==1? 'is-choosen' :''}}">{{$translate['with_mobile_internet']}}</a></li><!--
              --></ul>
            </div>
          </div>
          <div class="cs-spb-30">
            <div class="tsr-com-alphalist" id="js-countries-alpha">
              <ul class="cs-tx-bold">
                @foreach($roamingList as $symbol=>$countries)
                  <li><a href="#js-letter-code-{{$symbol}}">{{$symbol}}</a></li> 
                @endforeach 
              </ul>
            </div>
          </div>
          <div class="cs-spb-40">

          @foreach($roamingList as $symbol=>$countries)
            <div class="cs-spb-20" id="js-letter-code-{{$symbol}}">
              <div class="cs-spb-10 cs-tx-40 cs-tx-bold">{{$symbol}}</div>
              <div class="tsr-grid tsr-grid-separate tsr-clearfix">
                @foreach($countries as $name)
                 <div class="tsr-col-box tsr-col-03 cs-spv-02"><a href="{{$lang}}/{{$item->full_slug}}?key_word={{$name}}" class="js-cgroup js-cgroup-0 js-cgroup-1 js-cgroup-2">{{$name}}</a></div>
                @endforeach  
              </div>
            </div>

            <hr class="tsr-com-hr cs-spv-30" />
          @endforeach  
        </div>

    </div>
  </div>
</div>        

    @endsection