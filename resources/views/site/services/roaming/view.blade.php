@extends('site')

@section('content')
    
    @include('site.textcontent_default')
 
   <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <div class="cs-spb-20">
            <a href="javascript:window.history.back();" class="composite"><img src="site/tsr-core/tsr-images/tsr-arrow-link-left.png" alt="" class="cs-spr-04"><span>{{$translate['back_to_list']}}</span></a>
          </div>
          <div class="cs-spb-20"><h1 class="tsr-title">{{ $cName }}</h1></div>
          <div class="cs-spb-40">
          @foreach($roaming as $roam)

            @if(isset($roam->tariffCurrency))
              <div class="cs-spb-30">
                <div class="cs-spb-10 cs-tx-22">{{$roam->operatorName}}</div>
                <table class="tsr-com-table-nulled" style="min-width: 270px; width: 40%;">
                  <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['operator_type']}}</td><td class="cs-pdv-02 cs-cl-blue">{{isset($roam->operatorType)?$roam->operatorType :''}}</td></tr>
                  <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['handset_display']}}</td><td class="cs-pdv-02 cs-cl-blue">{{isset($roam->handsetDisplay)?$roam->handsetDisplay :'' }}</td></tr>
                  <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['discount']}}</td><td class="cs-pdv-02 cs-cl-blue"> {{ (isset($roam->isDiscount)&& $roam->isDiscount==1)?'Yes':'No'}}</td></tr>
                </table>
              </div>
              <div class="cs-spb-30">
                <div class="js-local-tarif-collapse">
                  <div class="cs-dp-none js-local-tarif-collapse-content">
                    <div class="cs-pdb-30">

                      <div class="cs-spb-10 cs-tx-22">{{$translate['tariffs']}}</div>
                      <table class="tsr-com-table-nulled" style="min-width: 270px; width: 40%;">
                        @if ($roam->isPostpaid == true)
                        <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['call_set_up']}}</td><td class="cs-pdv-02 cs-cl-blue">{{ isset($roam->callSetup)?number_format(floatval($roam->callSetupPeak), 2, '.', ''):'' }}</td></tr>
                        @endif
                        <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['call_to_georgia']}}</td><td class="cs-pdv-02 cs-cl-blue">{{ isset($roam->mosGeorgiaPeak)?number_format(floatval($roam->mosGeorgiaPeak ), 2, '.', ''):'' }} </td></tr>
                        @if ($roam->isPostpaid == false)
                        <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['international_call']}}</td><td class="cs-pdv-02 cs-cl-blue">{{ isset($roam->mocOperator)?number_format(floatval($roam->mocOperatorPeak), 2, '.', '') :'' }}</td></tr>
                        @else
                            <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['call_within_operator']}}</td><td class="cs-pdv-02 cs-cl-blue">{{ isset($roam->mocOperator)?number_format(floatval($roam->mocOperatorPeak), 2, '.', '') :'' }}</td></tr>
                        @endif
                        <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['call_within_country']}}</td><td class="cs-pdv-02 cs-cl-blue">{{ isset($roam->mocLocal)?number_format(floatval($roam->mocLocalPeak  ), 2, '.', ''):'' }}</td></tr>

                        <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['incomming_call']}}</td><td class="cs-pdv-02 cs-cl-blue">{{ isset($roam->mtc)?$roam->mtc :'' }} </td></tr>

                        <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['outgoing_sms']}}</td><td class="cs-pdv-02 cs-cl-blue">{{ isset($roam->mocSMS)?number_format(floatval($roam->mocSMS), 2, '.', '') :'' }} </td></tr>

                        <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['mobile_internet_and_mms']}}</td><td class="cs-pdv-02 cs-cl-blue"> {{ isset($roam->mosGPRS)?$roam->mosGPRSPeak :'' }} </td></tr>
                        <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">&nbsp;</td><td class="cs-pdv-02 cs-cl-blue">&nbsp;</td></tr>
                        <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['currency']}}</td><td class="cs-pdv-02 cs-cl-blue"> {{strtoupper(isset($roam->tariffCurrency)?$roam->tariffCurrency :'')}} </td></tr>
                        @if($roam->countryName == 'United States')
                        <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">
                          @if($geo == 1)
                            @if ($lang == 'en')
                              <br><br>
                              * 0% - 48% state based taxes are  added to the  rates on calls and SMS
                              Rounding - 60 sec
                            @else
                              <br><br>
                              * ტარიფებს ზარსა და SMS-ზე ემატება დღგ (0%- დან 48%-მდე) შტატების მიხედვით
                              დამრგვალება - 60 წამი
                            @endif
                          @endif
                        </td></tr>
                        @endif
                      </table>
                    </div>
                  </div>
                  <div>
                    <span class="tsr-link-tria tsr-link-tria-bottom js-local-tarif-collapse-show">{{$translate['show_tariffs']}}</span>
                    <span class="tsr-link-tria tsr-link-tria-top    js-local-tarif-collapse-hide cs-dp-none">{{$translate['hide_tariffs']}}</span>
                  </div>
                </div>
              </div>
               <hr class="tsr-com-hr cs-spv-30" />
              @endif
            @endforeach
           
           

          </div>
        </div>
      </div>
    </div>
   

@endsection
