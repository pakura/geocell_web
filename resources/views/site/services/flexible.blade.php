@extends('site')

@section('content')
    

    <!-- tsr-section-flexible-myprods -->
    <div class="tsr-section-flexible-myprods">
      <div class="tsr-container">
        
        <h1 class="tsr-title">{{$item->title}}</h1>
        <div class="cs-spb-20">{!! $item->content !!}</div>

          <div class="cs-spb-20">
             <div class="tsr-flexible-myprods tsr-grid tsr-grid-separate tsr-clearfix">
                 @foreach($services as $service)
                  <!--
                  -->
                    <div class="tsr-flexible-myprod tsr-col-box tsr-col-00" style="margin-right:0">
                        
                        <div class="tsr-flexible-myprod-head">
                          <div class="tsr-flexible-myprod-head-desc">
                            <div class="tsr-flexible-myprod-head-icon"><img src="{{config('folders.products')}}{{$service->file}}" alt=""></div>
                            <div class="tsr-flexible-myprod-head-name"><div class="cs-tx-18">{{$service->title}}</div></div>
                          </div>
                        </div>

                        <div class="tsr-flexible-myprod-body">
                         
                         <div class="tsr-flexible-myprod-serv tsr-selactable "  role="button"  data-label="{{$service->title}}"  data-title="" data-price="0">
                            <div class="tsr-flexible-myprod-serv-desc">
                              <div class="tsr-flexible-myprod-serv-name"><b></b></div>
                              <div class="tsr-flexible-myprod-serv-info"><span class="price_val" style="display:none">0 </span> {{$translate['choose_pack']}}</div>
                            </div>
                          </div>

                         @if(isset($prices[$service->product_id]))

                           @foreach($prices[$service->product_id] as $price)
                            <?php $title = ($lang=='ge'?$price->title_ge:$price->title_en);  ?>
                           <div class="tsr-flexible-myprod-serv tsr-selactable" role="button" data-label="{{$service->title}}" data-title="{{$title}}" data-price="{{number_format($price->value,2)}}">
                              <div class="tsr-flexible-myprod-serv-desc">
                                <div class="tsr-flexible-myprod-serv-name"><b>{{$title}}</b></div>
                                <div class="tsr-flexible-myprod-serv-info"><span class="price_val">{{number_format($price->value,2)}}</span> {{$translate['gel']}}</div>
                              </div>
                            </div>
                            @endforeach
                          @endif

                        </div>
                       
                      <div class="cs-spt-10 cs-cl-gray">{!!$service->description!!}</div>

                      </div>

                 @endforeach
              </div>

          </div> 


           <div class="cs-spb-20">
          <div class="tsr-flexible-results">
             <div id="service_info"  class="cs-spt-10 cs-cl-gray">{{$translate['min_two_packs']}}</div>
            <div class="cs-cl-gray cs-spt-10" id="text_sum" style="font-size:18px"></div>
            <div class="cs-cl-blue cs-tx-bold cs-tx-24">{{$translate['total']}} <span id="total_amt">0</span> {{$translate['gel']}}</div>
          </div>
        </div>
        <div class="cs-spt-40"><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.geocell.ge%2F&amp;locale=en_US&amp;layout=button_count&amp;show_faces=false&amp;action=like&amp;font=arial&amp;colorscheme=light&amp;width=200&amp;height=40" frameborder="0" scrolling="no" allowtransparency="true" style="background: transparent; border: 0; width: 100%; height: 40px;"></iframe></div>

      </div>
    </div>
    <!-- /tsr-section-ps-listing -->



    @if(isset($service)&& ($service->services_and_features || $service->terms_and_conditions) )
    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-color-purple">
      <div class="tsr-container">
        <h2>{{$translate['more_info']}}</h2>
      </div>
    </div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-module-tabular-a">
        <div class="tsr-module-tabular-header">
          <div class="tsr-container">
             <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab1">{{$translate['price_con']}}</a>
            <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab2">{{$translate['terms_con']}}</a>
          </div>
        </div>
        <div class="tsr-module-tabular-footer">
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad" id="plane_table">
                {!! $service->services_and_features !!}
                
              </div>
            </div>
          </div>
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad">
                {!! $service->terms_and_conditions !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->
    @endif

 
  <script type="text/javascript">/*<![CDATA[*/
      $(document).ready(function() {

        /* prod switch */
        (function() {
          var price = 0;
          var txt = '';
          var count = 0;

          $('.tsr-flexible-myprod').each(function() {
            
            
            var $servs = $('.tsr-flexible-myprod-serv', this);

            $servs.on('click', function(event) {
              
              
              $servs.removeClass('is-selected');
              
              $(this).addClass('is-selected');

              $('.tsr-flexible-myprod .is-selected').each(function() {

                  var label = $(this).data('label');
                  var title = $(this).data('title');
                  var newTitle =  title.split("-");

                  var val= $(this).data('price');

                  if(val>0) {
                    txt += label;
                    txt +=': ';
                    txt +=newTitle[0].trim() || 0;
                    txt +='; ';
                    count++;
                  }
                  

                  if(count>=2) { $("#service_info").hide();}
                  else { 
                    $("#service_info").show(); 
                    $("#total_amt").html(0);
                    $("#text_sum").html('');
                  }

                  price= price + parseFloat(val);
              });            
              
              if(count>=2) {  
               $("#total_amt").html(price);
               $("#text_sum").html(txt);
             }
               price = 0;
               count = 0;
               txt = '';
              event && event.preventDefault();
            }).first().trigger('click');
          });
        })();

      });
     /*]]>*/</script>


    @endsection