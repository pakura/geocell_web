@extends('site')

@section('content')


    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->


    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h">
                <div class="cs-spb-20"><h1 class="tsr-title">{{$item->title}}</h1></div>

                <div class="cs-spb-40">
                    <div class="cs-spb-00">
                        <form class="tsr-forms" action="{{$lang}}/international-line/search-ajax" onsubmit="return internationalLine(this)">
                            <label class="cc-label">{{$translate['search_country']}}</label>
                            <div class="tsr-clearfix">
                                <div class="cs-fl-left cs-spr-10 cs-spb-10" style="width: 260px;"><input type="text" name="key_word"></div>
                                <div class="cs-fl-left cs-spr-10 cs-spb-10"><input type="submit" value="{{$translate['search']}}" class="tsr-btn tsr-btn-form tsr-btn-purple"></div>
                            </div>
                        </form>
                    </div>
                    <!-- search results from ajax -->
                    <div class="cs-spb-10" id="int-line-search-result">

                    </div>
                    <!-- /search results from ajax -->
                </div>

                <div class="cs-spb-30">
                    <div class="tsr-com-linklist">
                        <ul class="cs-tx-13 cs-tx-bold"><!--
                --><li><a href="{{$lang}}/{{$item->full_slug}}" class="is-choosen">{{$translate['all']}}</a></li><!--
                {{----><li><a href="{{$lang}}/{{$item->full_slug.'?group='.trans('site.south_america')}}">{{trans('site.south_america')}}</a></li><!----}}
                        {{----><li><a href="{{$lang}}/{{$item->full_slug.'?group='.trans('site.cis_europe')}}">{{trans('site.cis_europe')}}</a></li><!----}}
                        {{----><li><a href="{{$lang}}/{{$item->full_slug.'?group='.trans('site.russia_usa')}}">{{trans('site.russia_usa')}}</a></li><!----}}
                        {{----><li><a href="{{$lang}}/{{$item->full_slug.'?group='.trans('site.satellite_conn')}}">{{trans('site.satellite_conn')}}</a></li><!----}}
                                --><li><a href="{{$lang}}/{{$item->full_slug}}?group=discount">{{$translate['no_discount']}}</a></li><!--
              --></ul>
                    </div>
                </div>


                <div class="cs-spb-40">
                    @foreach($symbols as $symbol)
                        <div class="cs-spb-20">
                            <div class="tsr-scrollpane">
                                <div class="tsr-scrollpane-content">

                                    <div class="cs-spb-10 cs-tx-40 cs-tx-bold">{{$symbol}}</div>

                                    <table  class="tsr-com-table-pricelist">
                                        <tr>

                                            <th class="tsr-out-pname">
                                                <div class="tsr-inn-pname"></div>
                                            </th>
                                            <th class="tsr-out-price"><div class="tsr-inn-price">{{$translate['price']}} ({{$translate['gel']}})</div></th>
                                            <th class="tsr-out-pname">
                                                {{$translate['country_code']}}
                                            </th>
                                        </tr>

                                        @foreach($services as $service)
                                            @if($service->letter!=$symbol) <?php continue; ?> @endif
                                            <tr>
                                                <td class="tsr-out-pname cs-cl-blue"><div class="tsr-inn-pname">{{str_replace('*', '', $service->title)}}</div></td>
                                                <td class="tsr-out-price"><div class="tsr-inn-price">{{$service->gel_min}}</div></td>
                                                <td class="tsr-out-pname">{{isset($service->country_code)?'+'.$service->country_code:''}}</td>
                                            </tr>
                                        @endforeach
                                    </table>

                                </div>
                            </div>
                        </div>
                        <hr class="tsr-com-hr cs-spv-30" />
                    @endforeach

                </div>
            </div>
        </div>
    </div>

    <script src="site/tsr-core/tsr-scripts/jquery-scrollbar/jquery.scrollbar.min.js"></script>

@endsection