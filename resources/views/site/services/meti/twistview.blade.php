@extends('site')

@section('content')
      
     <!-- tsr-section-service-inside -->
    <div class="tsr-section-service-present">
      <div class="tsr-container">
        <div class="tsr-section-flows">
          <div class="tsr-section-flow-b-ct">
            <div class="cs-spb-20"><h1 class="tsr-title">{{$service->title}}</h1></div>
            <div class="cs-spb-20">{!!$item->content !!}</div>
            <div class="cs-spb-20 tsr-section-flow-b-lt">
              <div href="#" class="tsr-module-metipack-present">
              <!--   <figure class="tsr-tactical-ribbon tsr-color-pink"><span>New</span></figure> -->
                @if($service->file)
                  <figure class="tsr-module-metipack-image tsr-module-metipack-image-40" style="background-image: url({{config('folders.products')}}{{$service->file}});"></figure>
                @endif
                {!! $service->description !!}

              </div>
<!--              <div class="cs-spt-10"><span class="tsr-link-tria tsr-link-tria-right" id="local-buckets-section-switcher">{{$translate['view_other_buckets']}}</span></div>-->
            </div>
            <div class="cs-spb-20 tsr-section-flow-b-gt">
<!--               <div><img src="site/tsr-temp/tsr-images/promo-25-lari.png" alt=""></div>
 -->            </div>
          <!--   <div class="cs-spb-20 cs-tx-20 cs-tx-bold cs-cl-blue">{{round($service->value,2)}} {{($lang=='en')?' GEL / Month':'ლარი / თვე'}}</div> -->
          
           @if($service->sku) 
             <div class="cs-spb-20"><a href="{{$lang}}/activate/service-meti/{{$service->product_id}}" class="tsr-btn js-fancybox fancybox.ajax" data-fancybox-type="ajax" >{{$translate['activate']}}</a></div>
             @endif

            <div class="cs-spt-40 cs-spb-20"><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.geocell.ge%2F&amp;locale=en_US&amp;layout=button_count&amp;show_faces=false&amp;action=like&amp;font=arial&amp;colorscheme=light&amp;width=200&amp;height=40" frameborder="0" scrolling="no" allowtransparency="true" style="background: transparent; border: 0; width: 100%; height: 40px;"></iframe></div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-service-inside -->



    <!-- tsr-section-generic -->
    <div class="tsr-section-generic" id="local-buckets-section">
      <div class="cs-pdb-60">
        <div class="tsr-container">
          <div class="tsr-section-generic-pad-h">
            <div class="tsr-module-metipacks">
              <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">

                @foreach($services as $serv)  
                <div class="tsr-boxcol-outer">
                  <div class="tsr-boxcol-inner">
                    <a href="{{$lang}}/{{$item->full_slug.'/'.$serv->slug}}" class="tsr-module-metipack">
                   <!--    <figure class="tsr-tactical-ribbon tsr-color-pink"><span>New</span></figure> -->
                       @if($serv->file)

                       <figure class="tsr-module-metipack-image tsr-module-metipack-image-40" style="background-image: url({{config('folders.products')}}{{$serv->file}});"></figure>

                       @endif

                      <div class="cs-spb-10 cs-tx-bold cs-tx-26">{{$serv->value}} {{$translate['gel']}}</div>
                      {!!$serv->description!!}
                    </a>

                    @if($serv->sku) 
                    <a href="{{$lang}}/activate/service-meti/{{$serv->product_id}}" class="tsr-module-metipack-btn tsr-btn tsr-btn-xsmall   js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['purchase']}}</a>
                    @endif

                  </div>
                </div>
                @endforeach

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

      <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-color-purple">
      <div class="tsr-container">
        <h2>{{trans('site.more_info')}}</h2>
      </div>
    </div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-module-tabular-a">
        <div class="tsr-module-tabular-header">
          <div class="tsr-container">
            <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab1">{{$translate['price_con']}}</a>
            <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab2">{{$translate['terms_con']}}</a>
          </div>
        </div>
        <div class="tsr-module-tabular-footer">
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad">
                {!! $service->services_and_features !!}
                
              </div>
            </div>
          </div>
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad">
                {!! $service->terms_and_conditions !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->
            <script src="site/tsr-modules/tsr-metipacks/tsr-metipacks.js"></script>


     <script type="text/javascript">/*<![CDATA[*/

      $(document).ready(function() {
        var $section  = $('#local-buckets-section');
        var $switcher = $('#local-buckets-section-switcher');
        $section.hide();
        $switcher.on('click', function(event) {
          $section.show();
          $(window).trigger('tsr-update');
          tsrCore.scrollTo($section, 300, -20);
          event.preventDefault();
        });
      });

    /*]]>*/</script>




@endsection