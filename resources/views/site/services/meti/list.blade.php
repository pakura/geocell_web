@extends('site')

@section('content')
    
    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <h1 class="tsr-title">{{$item->title }}</h1>
          <div class="cs-spb-20"> {!!$item->content !!} </div>

          <div class="tsr-module-metipacks">
            <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">
             <?php $btncnt = 0; ?>
              @foreach($services as $service)
              <div class="tsr-boxcol-outer">
                <div class="tsr-boxcol-inner">
                  <a href="{{$lang}}/{{$item->full_slug.'/'.$service->slug}}" class="tsr-module-metipack">
                   @if(@$service->file)
                      <figure class="tsr-module-metipack-image tsr-module-metipack-image-40" style="background-image: url({{config('folders.products')}}{{$service->file}});"></figure>
                    @else 
                     <div class="cs-spb-10 cs-tx-bold cs-tx-26">{{$service->title}}</div>  
                    @endif

                    @if(@$service->value)
                      <div class="cs-spb-10 cs-tx-bold cs-tx-26">{{$service->value}} {{$translate['gel']}}</div>
                     @endif
                    {!!$service->description!!}

                  </a>

                  @if(@$service->sku)
                    <a href="{{$lang}}/activate/service-meti/{{$service->product_id}}" class="tsr-module-metipack-btn tsr-btn tsr-btn-xsmall  js-fancybox fancybox.ajax" id="service_btn_{{$btncnt++}}" data-fancybox-type="ajax" onclick="saveAction(this)">{{$translate['purchase']}}</a>
                  @endif  
                </div>
              </div>
              @endforeach

            </div>
          </div>

          <div class="cs-spb-20"> {!!$item->description !!} </div>

          <div class="cs-spt-40 cs-spb-20"><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.geocell.ge%2F&amp;locale=en_US&amp;layout=button_count&amp;show_faces=false&amp;action=like&amp;font=arial&amp;colorscheme=light&amp;width=200&amp;height=40" frameborder="0" scrolling="no" allowtransparency="true" style="background: transparent; border: 0; width: 100%; height: 40px;"></iframe></div>

        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->
    

     @if(isset($service)&& ($service->services_and_features || $service->terms_and_conditions) )
    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-color-purple">
      <div class="tsr-container">
        <h2>{{$translate['more_info']}}</h2>
      </div>
    </div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-module-tabular-a">
        <div class="tsr-module-tabular-header">
          <div class="tsr-container">
             <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab1">{{$translate['price_con']}}</a>
             <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab2">{{$translate['terms_con']}}</a>
          </div>
        </div>
        <div class="tsr-module-tabular-footer">
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad" id="plane_table"> 
                {!! $service->services_and_features !!}
                
              </div>
            </div>
          </div>
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad">
                {!! $service->terms_and_conditions !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->


<script type="text/javascript">
  
  $(function(){
      var url = window.location.href;
      url = url.split('?')[1];
      if (url == 'next'){
          
          if (localStorage){
            var obj_btn = localStorage.lastaction;
            setTimeout(function(){
              $('#'+obj_btn).click()
            },1000);
          }
      } else {
          localStorage.removeItem("lastaction");
      }
  });

  function saveAction(obj){
    if (typeof localStorage.lastaction != 'undefined'){
       localStorage.removeItem("lastaction");
    } else {
        localStorage.setItem("lastaction", obj.id);
    }
    
  }

</script>

    @endif
    
    <script src="site/tsr-modules/tsr-metipacks/tsr-metipacks.js"></script>
    @endsection