
@extends('site')

@section('content')
  
    <!-- tsr-section-service-inside -->
    <div class="tsr-section-service-present">
      <div class="tsr-container">
        <div class="tsr-section-flows">
          <div class="tsr-section-flow-a-ct">
            <h1 class="tsr-title cs-spb-20">{{$service->title}}</h1>
            <div class="cs-spb-20">{!! html_entity_decode($service->description, 0, 'UTF-8') !!}</div>
            <div class="cs-spb-20 tsr-section-flow-a-lt">
              <figure class="tsr-section-image">
                <img src="{{$service->file?'uploads/products/'.$service->file:'site/tsr-temp/tsr-images/service-01.png'}}" alt="" />
                <!-- <figure class="tsr-tactical-ribbon"><span>New</span></figure>-->
                @if($service->ribbon!='')
                <figure class="tsr-tactical-ribbon-usp">{{$service->ribbon}}<span></span></figure> 
                @endif
              </figure>
            </div>
            
            @if(isset($service->ussd_code)  && $service->ussd_code!='' && !$service->sku)
            <!-- <div class="cs-spb-20 tsr-section-flow-a-gt">
              <figure class="tsr-section-bubble">
                <figure class="tsr-tactical-speach-bubble tsr-contour tsr-color-blue">
                  <div class="cs-tx-15">Activation by phone:</div>
                  <div class="cs-tx-20 cs-tx-bold cs-cl-blue">{{$service->ussd_code}} OK</div>
                </figure>
              </figure>
            </div> -->

             <div class="cs-spb-20">
              <div class="cs-spb-10"><figure class="tsr-tactical-speach-bubble tsr-contour tsr-color-blue"><div class="cs-tx-15"> {{$translate['activate_by_phone']}}</div><div class="cs-tx-20 cs-tx-bold cs-cl-blue">{{$service->ussd_code}} OK</div></figure></div>
            </div>
            
            @endif

             @if(isset($service->value)  && $service->value!='') 
              <div class="cs-spb-20 cs-tx-20 cs-tx-bold cs-cl-blue">{{round($service->value,2)}}  
                      {{$service->daily?$translate['gel_day']:$translate['gel_month']}}</div>
              @endif

             @if($service->sku) 
              <div class="cs-spb-20"><a href="{{$lang}}/activate/service/{{$service->product_id}}" class="tsr-btn js-fancybox fancybox.ajax" data-fancybox-type="ajax" >{{$translate['activate']}}</a></div>
            @endif
            <div class="cs-spt-40 cs-spb-20">
                <div class="fb-like" data-href="{{config('app.url').$lang.'/'.$item->full_slug.'/'.$service->slug}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
            </div>
          </div>
        </div>
        <div class="tsr-section-flows">
            {!! $service->content !!}
        </div>
      </div>
    </div>
    <!-- /tsr-section-service-inside -->

     @if($service->services_and_features || $service->terms_and_conditions)

     <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-color-purple">
      <div class="tsr-container">
        <h2>{{$translate['more_info']}}</h2>
      </div>
    </div>
    <!-- /tsr-section-divider -->
    
   
    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-module-tabular-a">
        <div class="tsr-module-tabular-header">
          <div class="tsr-container">
             <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab1">{{$translate['price_con']}}</a>
            <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab2">{{$translate['terms_con']}}</a>
          </div>
        </div>
        <div class="tsr-module-tabular-footer">
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad">
                {!! $service->services_and_features !!}
                
              </div>
            </div>
          </div>
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad">
                {!! $service->terms_and_conditions !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->
    @endif

@endsection