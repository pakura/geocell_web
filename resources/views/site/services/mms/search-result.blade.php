<div class="cs-spb-04 cs-tx-bold">Search results for "{{$key_word}}"</div>
  <table class="tsr-com-table-nulled" style="min-width: 280px;">
    @foreach($lines as $line)
      <tr><td class="cs-pdv-02 cs-pdr-20 cs-cl-blue">{{$line->title}}</td><td class="cs-pdv-02">{{$line->gel_min}} Gel</td></tr>
    @endforeach
  </table>