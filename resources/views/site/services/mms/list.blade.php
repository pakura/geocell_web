@extends('site')

@section('content')
    
      <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->
 
     <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">

        <h1 class="tsr-title">{{$item->title}}</h1>

          <div class="cs-spb-20">{!! $item->content !!}</div>

         <!--  <div class="cs-spb-30">
            <div class="tsr-com-linklist" >
              <ul class="cs-tx-13 cs-tx-bold"  > 
                 <li><a href="{{$lang}}/{{$item->full_slug}}" data-js-cgroup="0">All</a></li> 

                 <li><a href="{{$lang}}/{{$item->full_slug}}?price=6" data-js-cgroup="1" class="{{@$_GET['price']==6? 'is-choosen' :''}}">6 Tetri</a></li> 

                 <li><a href="{{$lang}}/{{$item->full_slug}}?price=12" data-js-cgroup="2" class="{{@$_GET['price']==12? 'is-choosen' :''}}">12 Tetri</a></li> 

                  <li><a href="{{$lang}}/{{$item->full_slug}}?price=21" data-js-cgroup="3" class="{{@$_GET['price']==21? 'is-choosen' :''}}">21 Tetri</a></li> 
              </ul>
            </div>
          </div> -->
 

          <div class="cs-spb-30">
            <div class="tsr-com-alphalist" id="js-countries-alpha">
              <ul class="cs-tx-bold">
                @foreach($roamingList as $symbol=>$countries)
                  <li><a href="#js-letter-code-{{$symbol}}">{{$symbol}}</a></li> 
                @endforeach 
              </ul>
            </div>
          </div>
          <div class="cs-spb-40">

          @foreach($roamingList as $symbol=>$countries)

            <div class="cs-spb-20">

              <div class="tsr-scrollpane">
                <div class="tsr-scrollpane-content">

                  <div class="cs-spb-10 cs-tx-40 cs-tx-bold" id="js-letter-code-{{$symbol}}">{{$symbol}}</div>
                  
                  <table class="tsr-com-table-pricelist">
                    <tr>
                       <th class="tsr-out-pname">           <div class="tsr-inn-pname"></div></th>   
                       <th class="tsr-out-price"><div class="tsr-inn-price">{{$translate['price']}} ({{$translate['gel']}})</div></th>   
                    </tr>       

                    @foreach($countries as $country=>$operators)
                     <tr><td  class="tsr-out-pname cs-cl-blue"><div class="tsr-inn-pname">{{$country}}</div></td></tr>

                      @foreach ($operators as $operator)

                         <tr><td class="tsr-out-pname cs-pdl-10" ><div class="tsr-inn-pname">{{$operator["operator"]}}</div></td>
                          <td class="tsr-out-price"><div class="tsr-inn-price"> {{number_format($operator["price"],2)}}</div></td>  </tr>
                      @endforeach

                    @endforeach  
                  
                </table>

                </div>
              </div>  

            </div>
            <hr class="tsr-com-hr cs-spv-30" />
          @endforeach  
        </div>

    </div>
  </div>
</div>        

 <script src="site/tsr-core/tsr-scripts/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    @endsection