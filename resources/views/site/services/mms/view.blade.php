@extends('site')

@section('content')
    
    @include('site.textcontent_default')

   <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <div class="cs-spb-20">
            <a href="javascript:window.history.back();" class="composite"><img src="site/tsr-core/tsr-images/tsr-arrow-link-left.png" alt="" class="cs-spr-04"><span>Back to list</span></a>
          </div>
          <div class="cs-spb-20"><h1 class="tsr-title">{{ $cName }}</h1></div>
          <div class="cs-spb-40">

          @foreach($roaming as $roam)  
            <div class="cs-spb-30">
              <div class="cs-spb-10 cs-tx-22">{{$roam->operatorName}}</div>
              <table class="tsr-com-table-nulled" style="min-width: 270px; width: 40%;">
                <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">Operator type</td><td class="cs-pdv-02 cs-cl-blue">{{isset($roam->operatorType)?$roam->operatorType :''}}</td></tr>
                <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">Handset display</td><td class="cs-pdv-02 cs-cl-blue">{{isset($roam->handsetDisplay)?$roam->handsetDisplay :'' }}</td></tr>
                <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">Discount</td><td class="cs-pdv-02 cs-cl-blue"> {{ isset($roam->isDiscount)?$roam->isDiscount :''}}</td></tr>
              </table>
            </div>
            <div class="cs-spb-30">
              <div class="js-local-tarif-collapse">
                <div class="cs-dp-none js-local-tarif-collapse-content">
                  <div class="cs-pdb-30">
                    <div class="cs-spb-10 cs-tx-22">Tariffs</div>
                    <table class="tsr-com-table-nulled" style="min-width: 270px; width: 40%;">
                      <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">Call set-up</td><td class="cs-pdv-02 cs-cl-blue">0.0 gel</td></tr>
                      <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">Call to Georgia</td><td class="cs-pdv-02 cs-cl-blue">{{ isset($roam->mocGeorgia)?$roam->mocGeorgia :'' }} Gel</td></tr>
                      <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">Call within the operator</td><td class="cs-pdv-02 cs-cl-blue">{{ isset($roam->mocOperator)?$roam->mocOperator :'' }} Gel</td></tr>
                      <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">Call within the country</td><td class="cs-pdv-02 cs-cl-blue">{{ isset($roam->mocLocal)?$roam->mocLocal :'' }} Gel</td></tr>
                      <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">Incomming call</td><td class="cs-pdv-02 cs-cl-blue">{{ isset($roam->mtc)?$roam->mtc :'' }}  Gel</td></tr>
                      <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">Outgoing SMS</td><td class="cs-pdv-02 cs-cl-blue">{{ isset($roam->mocSMS)?$roam->mocSMS :'' }} Gel</td></tr>
                      <tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">Mobile internet and MMS</td><td class="cs-pdv-02 cs-cl-blue"> {{ isset($roam->mosGPRS)?$roam->mosGPRS :'' }} </td></tr>
                    </table>
                  </div>
                </div>
                <div>
                  <span class="tsr-link-tria tsr-link-tria-bottom js-local-tarif-collapse-show">Show tariffs</span>
                  <span class="tsr-link-tria tsr-link-tria-top    js-local-tarif-collapse-hide cs-dp-none">Hide tariffs</span>
                </div>
              </div>
            </div>
             <hr class="tsr-com-hr cs-spv-30" />
            @endforeach
           
           

          </div>
        </div>
      </div>
    </div>
   

@endsection
