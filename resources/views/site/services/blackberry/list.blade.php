@extends('site')

@section('content')
    
    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <h1 class="tsr-title">{{$item->title }}</h1>
          <div class="cs-spt-10 cs-spb-20">
            <div class="tsr-com-linklist">
              <ul class="cs-tx-13 cs-tx-bold">
                @foreach($subPages as $subPage)
                <li><a href="{{$lang.'/'.$subPage->full_slug}}" class="@if($subPage->slug==$item->slug) is-choosen @endif">{{$subPage->title}}</a></li>
                 @endforeach
              </ul>
            </div>
          </div>

          <div class="cs-spt-30 tsr-section-generic-box">
            <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">
             @foreach($services as $service)
              <div class="tsr-boxcol-outer">
                <div class="tsr-boxcol-inner tsr-section-generic-fix-0">
                  <div class="cs-pdx-20">
                    <div class="cs-spb-20 cs-tx-24 cs-tx-bold cs-cl-blue tsr-section-generic-fix-1"> {{$service->title  }}</div>
                     {!! $service->description !!}
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>

          <div class="cs-spt-40 cs-spb-20"><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.geocell.ge%2F&amp;locale=en_US&amp;layout=button_count&amp;show_faces=false&amp;action=like&amp;font=arial&amp;colorscheme=light&amp;width=200&amp;height=40" frameborder="0" scrolling="no" allowtransparency="true" style="background: transparent; border: 0; width: 100%; height: 40px;"></iframe></div>

        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->


      <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-color-purple">
      <div class="tsr-container">
        <h2>{{$translate['more_info']}}</h2>
      </div>
    </div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-module-tabular-a">
        <div class="tsr-module-tabular-header">
          <div class="tsr-container">
            <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab1">{{$translate['price_con']}}</a>
            <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab2">{{$translate['terms_con']}}</a>
          </div>
        </div>
        <div class="tsr-module-tabular-footer">
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad">
                {!! $service->services_and_features !!}
                
              </div>
            </div>
          </div>
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad">
                {!! $service->terms_and_conditions !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->



    @endsection


    