@extends('site')

@section('content')
   @include('site.textcontent_default')
      
   <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <h1 class="tsr-title">{{$service->title}}</h1>
           
           {!! $service->description !!}
         
          <div class="cs-spt-40 cs-spb-20"><iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.geocell.ge%2F&amp;locale=en_US&amp;layout=button_count&amp;show_faces=false&amp;action=like&amp;font=arial&amp;colorscheme=light&amp;width=200&amp;height=40" frameborder="0" scrolling="no" allowtransparency="true" style="background: transparent; border: 0; width: 100%; height: 40px;"></iframe></div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

     @if($service->services_and_features || $service->terms_and_conditions)
     <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-color-purple">
      <div class="tsr-container">
        <h2>{{$translate['more_info']}}</h2>
      </div>
    </div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-module-tabular-a">
        <div class="tsr-module-tabular-header">
          <div class="tsr-container">
            <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab1">{{$translate['price_con']}}</a>
            <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab2">{{$translate['terms_con']}}</a>
          </div>
        </div>
        <div class="tsr-module-tabular-footer">
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad">
                {!! $service->services_and_features !!}
                
              </div>
            </div>
          </div>
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad">
                {!! $service->terms_and_conditions !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->
     @endif


@endsection