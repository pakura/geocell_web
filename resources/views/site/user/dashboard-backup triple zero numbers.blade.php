@extends('site')

@section('content')
    @include('site.user.usercabinet-head')
    <!-- tsr-section-uboard-have -->
    <div class="tsr-section-uboard-myprods">
      <div class="tsr-container">
        <h1 class="tsr-title">{{$translate['you_have']}}</h1>
        <div class="tsr-uboard-myprods tsr-grid tsr-grid-separate tsr-clearfix"><!--
          --><div class="tsr-uboard-myprod tsr-col-box tsr-col-00">
            <div class="tsr-uboard-myprod-head">
              <div class="composite"><!--
                --><div class="cs-spr-10"><img src="site/tsr-core/tsr-images/tsr-icon-48-white-phone.png" alt=""></div><!--
                --><div class="cs-spr-00"><div>{{$translate['total_minutes']}}</div><div class="cs-tx-22">

                {{@$actServices[3]['amount']?convertMinutes(@$actServices[3]['amount']):0}}

                </div></div><!--
              --></div>
            </div>
            <div class="tsr-uboard-myprod-body">
            @if(isset($actServices[3]))  
              @foreach(@$actServices[3]['data'] as $service)
              @if(@$service->data->amount>0 || @$service->data->amount==-1)
              <div class="tsr-uboard-myprod-serv">
                <div class="tsr-uboard-myprod-serv-desc">
                  <div class="tsr-uboard-myprod-serv-name">{{ $service->description }} <!-- <span class="tsr-inline-btn tsr-btn-pink">auto</span> --></div>
                  <div class="tsr-uboard-myprod-serv-info">{{ convertMinutes($service->data->amount)}}<span class="tsr-com-sup cs-tx-bold cs-cl-link">?</span></div>

                   @if(@$service->data->expirationDate)
                  <div class="tsr-uboard-myprod-serv-ttip"><div class="tsr-clearfix"><div class="cs-al-center cs-spb-10"><span class="cs-tx-bold">{{$translate['expires_on']}}</span><br>{{$userInfo->return->brand==1? convertDateTime(@$service->data->expirationDate):convertDate(@$service->data->expirationDate)}}</div><!-- <div class="cs-al-center"><a href="#" class="tsr-btn tsr-btn-xsmall">Prolong now</a></div> --></div></div>
                  @endif

                </div>
                <div class="tsr-uboard-myprod-serv-pbar"><i style="width:@if(@$service->data->defaultAmount>0 ){{($service->data->amount/@$service->data->defaultAmount)*100}}@elseif($service->data->amount==-1){{100}}@else{{0}}@endif%"></i></div>
              </div>
              @endif
              @endforeach
            @endif
              
            </div>
          </div><!--
          --><div class="tsr-uboard-myprod tsr-col-box tsr-col-00">
            <div class="tsr-uboard-myprod-head">
              <div class="composite"><!--
                --><div class="cs-spr-10"><img src="site/tsr-core/tsr-images/tsr-icon-48-white-message.png" alt=""></div><!--
                --><div class="cs-spr-00"><div>{{$translate['total_msg']}}</div><div class="cs-tx-22">
                @if(@$actServices[2]['amount'] && @$actServices[2]['amount']!=0)
                  {{@$actServices[2]['amount']<0?trans('site.unlimited'):@$actServices[2]['amount']}}
                @else 
                   0  
                @endif
                </div></div><!--
              --></div>
            </div>
            <div class="tsr-uboard-myprod-body">
            @if(isset($actServices[2]))  
              @foreach(@$actServices[2]['data'] as $service)
              @if(@$service->data->amount>0  || @$service->data->amount==-1)
              <div class="tsr-uboard-myprod-serv">
                <div class="tsr-uboard-myprod-serv-desc">
                  <div class="tsr-uboard-myprod-serv-name">{{ $service->description }} <!-- <span class="tsr-inline-btn tsr-btn-pink">auto</span> --></div>
                  <div class="tsr-uboard-myprod-serv-info">{{ $service->data->amount==-1?trans('site.unlimited'):$service->data->amount}} SMS <span class="tsr-com-sup cs-tx-bold cs-cl-link">?</span></div>
                 
                  @if(@$service->data->expirationDate)
                  <div class="tsr-uboard-myprod-serv-ttip"><div class="tsr-clearfix"><div class="cs-al-center cs-spb-10"><span class="cs-tx-bold">{{$translate['expires_on']}} </span><br>{{$userInfo->return->brand==1? convertDateTime(@$service->data->expirationDate):convertDate(@$service->data->expirationDate)}}</div><!-- <div class="cs-al-center"><a href="#" class="tsr-btn tsr-btn-xsmall">Prolong now</a></div> --></div></div>
                  @endif
                </div>
                <div class="tsr-uboard-myprod-serv-pbar"><i style="width:@if(@$service->data->defaultAmount>0 ){{($service->data->amount/@$service->data->defaultAmount)*100}}@elseif($service->data->amount==-1){{100}}@else{{0}}@endif%"></i></div>
              </div>
              @endif
              @endforeach
            @endif  
            </div>
          </div><!--
          --><div class="tsr-uboard-myprod tsr-col-box tsr-col-00">
            <div class="tsr-uboard-myprod-head">
              <div class="composite"><!--
                --><div class="cs-spr-10"><img src="site/tsr-core/tsr-images/tsr-icon-48-white-traffic.png" alt=""></div><!--
                --><div class="cs-spr-00"><div>{{$translate['total_traffic']}}</div><div class="cs-tx-22">{{convertBalance(isset($actServices[1]['amount'])?$actServices[1]['amount']:0 )}} </div></div><!--
              --></div>
            </div>
            <div class="tsr-uboard-myprod-body">
              
           @if(isset($actServices[1]) && isset($actServices[1]['data']))  
            
              @foreach(@$actServices[1]['data'] as $service)
              @if(@$service->data->amount>0)
              <div class="tsr-uboard-myprod-serv">
                <div class="tsr-uboard-myprod-serv-desc">
                  <div class="tsr-uboard-myprod-serv-name">{{ $service->description }} <!-- <span class="tsr-inline-btn tsr-btn-pink">auto</span> --></div>
                  <div class="tsr-uboard-myprod-serv-info">{{ convertBalance($service->data->amount)}}<span class="tsr-com-sup cs-tx-bold cs-cl-link">?</span></div>
                  
                  @if(@$service->data->expirationDate)
                  <div class="tsr-uboard-myprod-serv-ttip"><div class="tsr-clearfix"><div class="cs-al-center cs-spb-10"><span class="cs-tx-bold">{{$translate['expires_on']}}</span><br>{{$userInfo->return->brand==1? convertDateTime(@$service->data->expirationDate):convertDate(@$service->data->expirationDate)}}</div><!-- <div class="cs-al-center"><a href="#" class="tsr-btn tsr-btn-xsmall">Prolong now</a></div> --></div></div>
                  @endif
               
                </div>
                <div class="tsr-uboard-myprod-serv-pbar"><i style="width:  {{@$service->data->defaultAmount>0?($service->data->amount/@$service->data->defaultAmount)*100 : 0}}%"></i></div>
              </div>
              @endif
              @endforeach
            @endif   
            </div>
          </div>


        <!--   @if(isset($actServices[4]))  
          <div class="tsr-uboard-myprod tsr-col-box tsr-col-00">
            <div class="tsr-uboard-myprod-head">
              <div class="composite">
                <div class="cs-spr-10"><img src="site/tsr-core/tsr-images/tsr-icon-48-white-message.png" alt=""></div>
                <div class="cs-spr-00"><div>{{$translate['roaming_mins']}}</div><div class="cs-tx-22">{{@$actServices[4]['amount']?@$actServices[4]['amount'] :0}}</div></div>
             </div>
            </div>

            <div class="tsr-uboard-myprod-body">
            
                @foreach(@$actServices[4]['data'] as $service)
                  <div class="tsr-uboard-myprod-serv">
                    <div class="tsr-uboard-myprod-serv-desc">
                      <div class="tsr-uboard-myprod-serv-name">{{ $service->description_en }} </div>
                      <div class="tsr-uboard-myprod-serv-info">{{ convertMinutes(@$service->data->amount)}}<span class="tsr-com-sup cs-tx-bold cs-cl-link">?</span></div>
                      <div class="tsr-uboard-myprod-serv-ttip"><div class="tsr-clearfix"><div class="cs-al-center cs-spb-10"><span class="cs-tx-bold">Expires on:</span><br>{{$userInfo->return->brand==1? convertDateTime(@$service->data->expirationDate):convertDate(@$service->data->expirationDate)}}</div>  </div></div>
                  </div>
                    <div class="tsr-uboard-myprod-serv-pbar"><i style="width:  {{$service->total_amount>0?($service->data->amount/$service->total_amount)*100 : 0}}%"></i></div>
                  </div>
                  @endforeach
            
            </div>
          </div> 
           @endif      -->
           <!-- roaming is ended -->

		</div>
      </div>
    </div>
    <!-- /tsr-section-uboard-have -->

    <!-- tsr-section-uboard-dashboard -->
    <div class="tsr-section-uboard-dashboard">
      <div class="tsr-container">
        <div class="tsr-dashboard-grid tsr-grid tsr-grid-separate tsr-clearfix">
          <div class="tsr-col-12">
            <div class="tsr-dashboard-block-wrap">
              <div class="tsr-dashboard-block-wrap-head">
                <div class="tsr-dashboard-block-wrap-head-tb">
                  <div class="tsr-dashboard-block-wrap-head-tb-lt"><h3 class="tsr-title">{{$translate['my_services']}}</h3></div>
                  <div class="tsr-dashboard-block-wrap-head-tb-gt"><a href="javascript:$('#available_services').slideToggle();" class="composite"  ><span class="ts-icon-add cs-dp-inline-block cs-tx-22 cs-spr-04"></span><span>{{$translate['add_service']}}</span></a></div>
                </div>
              </div>

              <div id="available_services" style="display:none;">
                @include('site.lightbox.add-service')
              </div>


              <div class="tsr-dashboard-block-wrap-body">
                <div class="tsr-uboard-services tsr-uboard-services-scenario-a">  
              

              

              @if( isset($actServices[99]) )  
                @foreach($actServices[99]['data'] as $data)
                  <?php
                    $icon = $data->service_type_group==4?$data->meti_file:$data->file;
     
                     $src = ($data->file)?'uploads/products/thumbs/'.$icon:'site/tsr-temp/tsr-images/service-01.png'; 

                   ?> 
                  <div class="tsr-uboard-service">
                    <div class="tsr-uboard-service-modal"></div>
                    <div class="tsr-uboard-service-head">
                      <div class="tsr-uboard-service-head-col-01">
                        <div class="tsr-clearfix">
                          <div class="tsr-uboard-service-link tsr-clearfix">
                            <figure class="tsr-uboard-service-image">
                              <img alt="" src="{{$src}}">
                            </figure>
                            <div class="tsr-uboard-service-desc">
                                <div class="tsr-uboard-service-title">{{$data->description}}</div>
                                @if($data->daily)
                                  <div class="tsr-uboard-service-intro">{{$translate['validity_daily']}}</div>
                                @elseif(isset($data->data->status) && $data->data->status==2)  
                                  <div class="tsr-uboard-service-intro">{{$translate['susp_service']}}</div>
                                @elseif (isset($data->data->expirationDate) && !$data->hideDate)
                                  <div class="tsr-uboard-service-intro">{{trans('site.validity')}} {{convertDate($data->data->expirationDate)}} </div> 
                                @endif
                            </div>
                          </div>
                        </div>
                      </div>

                      @if(isset($data->data->removable) && $data->data->removable==true)
                      <div class="tsr-uboard-service-head-col-02">
                        <div class="composite"> 
                         <div class="cs-spr-10"><div class="tsr-uboard-service-price"></div></div> 
                           <div class="cs-spl-10"><a href="{{$lang}}/dashboard/remove-service/{{$data->data->key}}" class="tsr-btn tsr-btn-xsmall tsr-btn-gray js-fancybox fancybox.ajax" data-fancybox-type="ajax" >{{$translate['deactivate']}}</a></div> 
                         </div>
                      </div>
                      @endif
                    </div>

                    @if($data->data->key=='S_TRIPLE_ZERO')
                    <div class="tsr-uboard-service-body">
                      <div class="tsr-uboard-service-body-pad">

                        <div class="tsr-uboard-service-body-desc">
                            
                           <div class="cs-spb-10 cs-tx-bold">{{$translate['triple_zero_add_num']}}</div>
                            
                            <div class="cs-spb-40 tsr-forms js-ph-editor">
                            
                            <div id="numbers">
	                            @if(is_array($numbers) && count($numbers>0) )
	                              @foreach(@$numbers as $numb)
	                              <div class="cs-spb-10">
	                                <div><span class="cs-tx-18">{{$numb}}</span><span class="cs-spl-20 cs-tx-bold cs-tx-13"><!-- <a href="#" onclick="return false;" class="noline">edit</a><span class="cs-sph-04 cs-cl-link">/</span> -->

                                  <!-- <a href="{{$lang}}/remove-number-conf/{{$numb}}"  class="noline remove-number js-fancybox fancybox.ajax" data-fancybox-type="ajax" >{{$translate['remove']}}</a> -->

                                  </span></div>
	                              </div>
	                              @endforeach 
	                             @endif
                             </div>
                              
                              @if( count($numbers)<3 )

                              <!-- <div id="msg" style="padding:5px 0;"></div> -->
                             <!-- <div class="cs-spb-10" style="display:none;" id="addNumber">
                                <div>
                                  
                                  <div class="tsr-clearfix">
                                    <form action="{{$lang}}/dashboard/add-number" method="POST" onsubmit="tripleZeroNumbers(this,'numbers')">
                                      <div class="cs-fl-left cs-spr-04" style="width: 120px;">
                                          <input type="text" value="" name="friend_number" data-inputmask="'mask': '\\599999999'" >
                                      </div>
                                      <div class="cs-fl-left">
                                          <input type="submit" value="{{$translate['add']}}" class="tsr-btn tsr-btn-form">
                                      </div>
                                    </form>  
                                  </div>
                                </div>
                              </div> -->

                              <script src="site/tsr-core/tsr-scripts/jquery-inputmask/jquery.inputmask.min.js"></script>


                              <script type="text/javascript">/*<![CDATA[*/

                                var $box = $('#addNumber');


                                $('input[type="text"]', $box).inputmask();
                                  </script>
                             <!--  <div class="cs-spt-20">
                                <div><a href="javascript:$('#addNumber').slideToggle();"   class="composite"><span class="tsr-ico-btn cs-spr-04"><span class="ts-icon-add cs-tx-16 cs-pdx-04"></span></span><span class="cs-tx-13 cs-tx-bold">{{$translate['add_number']}}</span></a></div>
                              </div> -->
                              @endif
                            </div>

                        </div>

                        <div class="tsr-uboard-service-body-close"><span class="tsr-uboard-service-collapse">{{$translate['collapse']}}</span></div>
                      </div>
                    </div>  
                    @endif
                  </div>
                   
                @endforeach
              @endif 
              
                </div>
              </div>
            </div>
          </div>
        


          <div class="tsr-clear"></div>

          <!-- <div class="tsr-col-06 tsr-spacetop">
            <div class="tsr-dashboard-block-wrap">
              <div class="tsr-dashboard-block-wrap-head">
                <h3 class="tsr-title">{{$translate['my_orders']}}</h3>
              </div>
              <div class="tsr-dashboard-block-wrap-body">
                <div class="cs-spb-20">
                  <div><a href="#" class="cs-tx-bold noline">Apple IPhone 6 (Order #14761)</a></div>
                  <div class="cs-tx-13 cs-cl-gray"><span class="cs-tx-bold">Purchase date:</span> June 20, 2015</div>
                  <div class="cs-tx-13 cs-cl-gray"><span class="cs-tx-bold">Status:</span> Delivered <span class="ts-icon-circle cs-tx-11 cs-cl-turquoise"></span></div>
                </div>
                <div class="cs-spb-20">
                  <div><a href="#" class="cs-tx-bold noline">Apple IPhone 6 (Order #14761)</a></div>
                  <div class="cs-tx-13 cs-cl-gray"><span class="cs-tx-bold">Purchase date:</span> June 20, 2015</div>
                  <div class="cs-tx-13 cs-cl-gray"><span class="cs-tx-bold">Status:</span> In progress <span class="ts-icon-circle cs-tx-11 cs-cl-yellow"></div>
                </div>
                <div class="cs-spb-20">
                  <div><a href="#" class="cs-tx-bold noline">Apple IPhone 6 (Order #14761)</a></div>
                  <div class="cs-tx-13 cs-cl-gray"><span class="cs-tx-bold">Purchase date:</span> June 20, 2015</div>
                  <div class="cs-tx-13 cs-cl-gray"><span class="cs-tx-bold">Status:</span> Canceled <span class="ts-icon-circle cs-tx-11 cs-cl-red"></div>
                </div>
                <div class="cs-al-right"><a href="#" class="cs-tx-bold">{{$translate['view_full_history']}}</a></div>
              </div>
            </div>
          </div> -->
          
          @if(isset($installment->hasInstalment) && $installment->hasInstalment==true)
          <div class="tsr-col-06 tsr-spacetop">
            <div class="tsr-dashboard-block-wrap">
              <div class="tsr-dashboard-block-wrap-head">
                <h3 class="tsr-title">{{$translate['installment']}}</h3>
              </div>
              <div class="tsr-dashboard-block-wrap-body">
                <div class="tsr-dashboard-instl">
                  <div class="tsr-dashboard-instl-date">{{instalmentDate(isset($installment->planedPayments[0]->date)?$installment->planedPayments[0]->date:0 )}}</div>
                  <div class="cs-spb-20 cs-tx-17 cs-cl-pink">
                      
                      {{$translate['you_should_pay'].' '.$installment->planedPayments[0]->amount.' '.$translate['gel']}} 
                  </div>
                  <!-- <div><a href="#" class="cs-tx-bold">Full shedule</a></div> -->
                  <hr class="tsr-dashboard-instl-hr" />
                  <div class="cs-spb-04 cs-tx-17 cs-cl-pink">Your monthly bonuses</div>
                  <div class="cs-tx-13">Unlimited minutes for local calls</div>
                  <div class="cs-tx-13">Unlimited SMS-es</div>
                  <div class="cs-tx-13">1 GB mobile internet</div>
                </div>
              </div>
            </div>
          </div>

          <div class="tsr-clear"></div>
          @endif


        </div>
      </div>
    </div>
    <!-- /tsr-section-uboard-dashboard -->


 
   

@endsection