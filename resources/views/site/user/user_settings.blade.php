@extends('site')

@section('content')
     @include('site.user.usercabinet-head')
    

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">

          <h1 class="tsr-title cs-spb-10">{{$item->title}}</h1>

          <div class="cs-spb-40">
            <div class="tsr-com-linklist">
              <ul class="cs-tx-13 cs-tx-bold"><!--
                --><!-- <li><a href="#" class="is-choosen">Personal info</a></li> --><!--
                --><!-- <li><a href="#">Notifications</a></li> --><!--
                --><!-- <li><a href="#">Security settings</a></li> --><!--
              --></ul>
            </div>
          </div>

 
          {!!  Form::model($user,['url'=>$lang.'/user/save-profile','class'=>'tsr-forms','id'=>'local-profile-form', 'onsubmit'=>'return chekfield()']) !!}
            <div class="tsr-grid tsr-clearfix">

              <div class="tsr-col-03 cs-spb-20">
                <div><label class="cc-label">{{$translate['cell_number']}}</label></div>
                <div><input type="text" data-inputmask="'mask': '\\599 999999'"  value="{{\Session::get('phone')}}" disabled></div>
              </div>
              <div class="tsr-col-03 cs-spb-20">
                <div><label class="cc-label" for="personal_number">{{$translate['personal_id']}}</label></div>
                <div><input type="text" value="{{$user->personal_number}}" name="personal_number" ></div>
              </div>

              <div class="tsr-clear"></div>

              <div class="tsr-col-03 cs-spb-20">
                <div><label class="cc-label" for="first_name">{{$translate['first_name']}}</label></div>
                <div><input type="text" value="{{$user->first_name}}"  name="first_name" ></div>
              </div>
              <div class="tsr-col-03 cs-spb-20" >
                <div><label class="cc-label" for="last_name">{{$translate['last_name']}}</label></div>
                <div><input type="text" value="{{$user->last_name}}"  name="last_name"></div>
              </div>

              <div class="tsr-clear"></div>

              <div class="tsr-col-12 cs-spb-20">
                <div><label class="cc-label cc-no-margin">{{$translate['birth_date']}}</label></div>
                <div class="tsr-clearfix">
                  <div class="cs-fl-left cs-spt-04 cs-spr-04" style="width: 100px;">

                    <select name="day">
                      <option>{{$translate['day']}}</option>
                     @for($i=1;$i<=31;$i++)
                      <option value="{{$i}}">{{$i}}</option>
                     @endfor 
                    </select>

                    </div>
                  <div class="cs-fl-left cs-spt-04 cs-spr-04" style="width: 160px;">

                    <select name="month">

                      <option>{{$translate['month']}}</option>
                      @foreach(trans('site.months') as $key=>$name)
                        <option value="{{$key+1}}">{{$name}}</option>
                      @endforeach  
                    </select>
                  </div>

                  <div class="cs-fl-left cs-spt-04 cs-spr-04" style="width: 100px;">

                    <select name="year">
                      <option>{{$translate['year']}}</option>
                      <?php $year = date('Y');  $startY= $year-80;?>
                      @for($i=$startY;$i<=$year;$i++)
                        <option name="{{$i}}">{{$i}}</option>
                      @endfor
                    </select>
                  </div>
                </div>
              </div>

              <div class="tsr-clear"></div>

              <div class="tsr-col-03 cs-spb-20">
                <div><label class="cc-label">{{$translate['email']}}</label></div>
                <div><input type="text" value="{{$user->email}}" name="email" id="email" onkeyup="$('#email').css({'border' : '1px solid #dbdbdb'});$('#emailError').fadeOut();"></div>
                <div class="cs-spb-04 cs-mg-center tsr-lightbox-layout-a-field"><div class="cc-errorbox" id="emailError" style="display: none; margin-top: 5px"> @if($lang == 'en') The E-mail is not valid @else მითითებული ელ-ფოსტა არასწორია @endif </div></div>
                <br><br>
                <div><input type="checkbox" value="1" @if($unsub === false) checked @endif name="subscribe" id="subscribe"/>
                  <label for="subscribe" class="cc-label" style="float: right;margin-right: 66px;" onclick="togglesub()">{{$translate['sub_for_newsletter']}}</label></div>
              </div>

              <div class="tsr-clear"></div>

              <div class="tsr-col-12 cs-spb-20">
                <div><label class="cc-label cc-no-margin">{{$translate['legal_addr']}}</label></div>
                <div class="tsr-grid tsr-clearfix">
                  <div class="tsr-col-03 cs-spt-04">
                    <div>

                    <select name="city"><option>{{$translate['city']}}</option>
                         @foreach($cities as $city)
                          <option value="{{$city->id}}" {{$user->city==$city->id?'selected':''}}>{{$city->city_name}}</option>
                        @endforeach 

                    </select>

                    </div>
                  </div>
                  <div class="tsr-col-03 cs-spt-04">

                      <div><input type="text" value="{{$user->district}}" class="placeholder" name="district" title="{{$translate['district']}}"></div>
                  </div>

                  <div class="tsr-col-03 cs-spt-04">
                    <div><input type="text" class="placeholder" value="{{$user->address}}" name="address" title="{{$translate['address']}}"></div>
                  </div>
                  
                </div>
              </div>

              <div class="tsr-col-12 cs-spb-20">
                <div><label class="cc-label cc-no-margin">{{$translate['physical_addr']}}</label></div>
                <div class="tsr-grid tsr-clearfix">
                  <div class="tsr-col-03 cs-spt-04">
                    <div>
                    <select name="city2">
                      <option>{{$translate['city']}}</option>
                       @foreach($cities as $city)
                          <option value="{{$city->id}}" {{$user->city2==$city->id?'selected':''}}>{{$city->city_name}}</option>
                        @endforeach 
                        </select> 
                      </div> 
                  </div>
                  <div class="tsr-col-03 cs-spt-04">

                      <div><input type="text" name="district2" value="{{$user->district2}}"  title="{{$translate['district']}}"></div>
                  </div>

                  <div class="tsr-col-03 cs-spt-04">
                    <div><input type="text" name="address2" value="{{$user->address2}}" class="placeholder" title="{{$translate['address']}}"></div>
                  </div>
                  
                </div>
              </div>

              <div class="tsr-col-12 cs-spb-20">
                <div class="tsr-clearfix">
                  <div class="cs-fl-left cs-spt-04 cs-spr-04"><input type="submit" value="{{$translate['cancel']}}" class="tsr-btn tsr-btn-gray tsr-btn-form"></div>
                  <div class="cs-fl-left cs-spt-04 cs-spr-04"><input type="submit" value="{{$translate['save']}}" class="tsr-btn tsr-btn-turquoise tsr-btn-form"></div>
                </div>
              </div>

            </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->


<script>
//    function togglesub(){
//        if($('#subscribe').is(":checked")){
//             $('#subscribe').prop( "checked", false );
//             $('.cc-state-checked').addClass('cc-state');
//             $('.cc-state-checked').removeClass('cc-state-checked');
//        } else {
//            $('#subscribe').prop( "checked", true );
//            $('.cc-state-checked').addClass('cc-state-checked');
//            $('.cc-state-checked').removeClass('cc-state');
//        }
//    }


$(function(){
  $(window).scrollTop($('.tsr-section-refiner').offset().top);
    setTimeout(function () {
        $(window).scrollTop($('.tsr-section-refiner').offset().top);

    }, 500);
});

  function chekfield(){
    $('#email').css({'border' : '1px solid #dbdbdb'});
    $('#emailError').fadeOut();
    var email = $('#email').val();
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email) && email != ''){
      $('#email').css({'border' : '1px solid red'});
      $('#emailError').fadeIn();

      return false;
    }
    return true;
  }
</script>

@endsection