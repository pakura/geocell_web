<?php   //if($lang=='ge'){ setlocale(LC_ALL, 'ka-GE.UTF-8'); }  ?>
        <!-- Google Code for Registration Mygeocell Conversion Page --> <script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 975642127;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "lFOCCN-hkmgQj7yc0QM"; var google_remarketing_only = false;
    /* ]]> */
</script>
<script type="text/javascript"
        src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt=""
             src="//www.googleadservices.com/pagead/conversion/975642127/?label=lFOCCN-hkmgQj7yc0QM&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>
    <!-- tsr-section-uboard-balance -->
    <div class="tsr-section-uboard-balance">
      <div class="tsr-container">
        <div class="tsr-clearfix">
          <div class="tsr-uboard-balance-main">
            <div class="tsr-clearfix">
             <!--  <div class="tsr-uboard-balance-main-avatar">
                <img src="site/tsr-temp/tsr-images/avatar-01.png" alt="" class="tsr-avatar-img" />
                <span class="tsr-avatar-layer">change<br>photo</span>
                <a href="tsr-lightbox-auth-avatar.html" data-fancybox-type="ajax" class="js-fancybox fancybox.ajax tsr-avatar-link" onclick="return false;"></a>
              </div> 
              tsr-uboard-balance-main-account
              -->
              <div class="">
 
                 <div class="tsr-uboard-balance-main-title">
                      <span class="js-tooltip-text"  @if(isset($userInfo->return->brand) && $userInfo->return->brand==2) title="{{$translate['expire_date']}} {{convertDate(@$userInfo->return->expireDate)}}" @endif>

                        {{\Session::get('phone')}}

                      @if(isset($userInfo->return->blockingStatus))  
                        @if($userInfo->return->blockingStatus==0)
                          <span class="tsr-uboard-balance-main-ico ts-icon-info tsr-color-green"></span>
                        @elseif($userInfo->return->blockingStatus==3 || $userInfo->return->blockingStatus==16) 
                          <span class="tsr-uboard-balance-main-ico ts-icon-info tsr-color-yellow"></span>
                        @else
                         <span class="tsr-uboard-balance-main-ico ts-icon-info tsr-color-red"></span>
                        @endif
                      @endif  
                     
                        
                      </span>
                  </div>
                  @if(isset($userInfo->return->blockingStatus))
                    <div class="tsr-uboard-balance-main-message">
                      <?php
                        switch($userInfo->return->blockingStatus){
                          case 0:
                            echo $translate['number_is_active'];
                            break;
                          case 3:
                          case 16:
                            echo $translate['partial_active'];
                            break;  
                          case 5:
                          case 2:
                            echo $translate['forced'];
                            break;  
                          case 10:  
                             echo $translate['final'];
                             break;
                        }

                      ?>
                    </div>  
                  @endif
              </div> 
            </div>  
            <div class="cs-spt-20">
              <div class="tsr-uboard-balance-main-balance">
                <span class="tsr-uboard-balance-main-balance-a">

                  <span class="cs-pdr-10">

                  @if(isset($userInfo->return->brand) && $userInfo->return->brand==1 && $userInfo->return->accountMethod==1)
                    {{ @$userInfo->return->balance>0?$translate['available_balance']:$translate['excided_balance_limit'] }}
                  @else 
                   {{$translate['balance']}}
                  @endif

                  </span>
                  <span class="cs-pdr-04">{{number_format(@$userInfo->return->balance,2)}} ₾</span>

                </span>
                @if($userInfo->return->brand==1 && $userInfo->return->accountMethod==1)
                  <!-- <span class="tsr-uboard-balance-main-balance-b">/</span> -->
                    <div class="tsr-clear"></div>
                  <span class="tsr-uboard-balance-main-balance-a"><span class="cs-pdr-10">{{$translate['previous_debt']}}:</span><span class="cs-pdr-04">{{number_format(@$userInfo->return->previousMonthDebt,2)}} ₾</span></span>
                 <!--  <span class="tsr-uboard-balance-main-balance-b">/</span> -->
                   <div class="tsr-clear"></div>
                  <span class="tsr-uboard-balance-main-balance-a"><span class="cs-pdr-10">{{$translate['limit']}}:</span><span class="cs-pdr-04">{{number_format(@$userInfo->return->creditLimitDouble,2)}} ₾</span></span>

                  @if(isset($userInfo->return->isStaff) && $userInfo->return->isStaff==true && isset($userInfo->return->roamingBalance))
                  <div class="tsr-clear"></div>
                  <span class="tsr-uboard-balance-main-balance-a"><span class="cs-pdr-10">{{$translate['staff_balance_for_roaming']}}:</span><span class="cs-pdr-04">{{number_format(@$userInfo->return->roamingBalance,2)}} ₾</span></span> 
                  @endif


                @endif

              </div>
            </div>

          <div class="cs-spt-20">
              <div class="cs-smx-05"><!--
                --><span class="cs-dp-iblock cs-spx-05"><a href="{{$lang}}/fill-balance" class="tsr-btn tsr-btn-small tsr-btn-green js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['fill_balance']}}</a></span><!--

            @if(isset($directdebit))
                  @if($directdebit === true)
                          --><span class="cs-dp-iblock cs-spx-05"><a href="{{$lang}}/private/private-cabinet/direct-debit" class="tsr-btn tsr-btn-small tsr-btn-green">{{$translate['automatic_fill']}}<span class="cs-spl-10 tsr-btn-icon" data-btn-icon=""></span></a></span><!--
                @else
                          --><span class="cs-dp-iblock cs-spx-05"><a href="{{$lang}}/private/private-cabinet/direct-debit" class="tsr-btn tsr-btn-small tsr-btn-green">{{$translate['automatic_fill']}}</a></span><!--
                @endif
            @else
                          --><span class="cs-dp-iblock cs-spx-05"><a href="{{$lang}}/private/private-cabinet/direct-debit" class="tsr-btn tsr-btn-small tsr-btn-green ">{{$translate['automatic_fill']}}</a></span><!--
            @endif
              --></div>
          </div>
            <div class="cs-spt-20">
              <div class="tsr-com-linklist">
                <ul class="cs-tx-13 cs-tx-bold"> 
                	@if($userInfo->return->brand==2)
                    <li><a href="{{$lang}}/dashboard/get-geocredit-info" class="js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['geocredit']}}</a></li>
                  @endif  
                  <!--
                	<li><a href="#">Direct debit</a></li> 
                    <li><a href="#">Money transfer</a></li> 
                	-->
                </ul>
              </div>
            </div>
          </div>
           
           @if(isset($check4G->return->resultCode) && $check4G->return->resultCode==1) 
          <div class="tsr-uboard-balance-side">
            <div class="tsr-uboard-balance-side-promo">
              <div class="tsr-uboard-balance-side-image">
                <img src="site/tsr-core/tsr-images/default_mobile.jpg" alt="" height="200">
                @if($check4G->return->isPhone4GCompatibility) 
                <figure class="tsr-tactical-flash tsr-flash-usp-1 tsr-color-purple"><span>4G</span></figure>
                @endif
                <!-- <figure class="tsr-tactical-flash tsr-flash-usp-2 tsr-color-blue"><span>HD<small>Voice</small></span></figure> -->
                <div class="tsr-uboard-balance-side-layer">
                  <div class="cs-dp-table cs-whole">
                    <div class="cs-dp-table-cell">
                      <div class="cs-spb-06">{{$translate['sim_card_comat']}} <span class="{{$check4G->return->isSIM4GCompatibility?'cs-tx-12 cs-cl-turquoise ts-icon-thick ':'cs-tx-20 cs-cl-pink ts-icon-delete'}}"></span></div>
                      <div class="cs-spb-06">{{$translate['sim_device_comat']}} <span class="{{$check4G->return->isPhone4GCompatibility?'cs-tx-12 cs-cl-turquoise ts-icon-thick ':'cs-tx-20 cs-cl-pink ts-icon-delete'}}"></span></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tsr-uboard-balance-side-title">{{ $check4G->return->model }}</div>
              <div class="tsr-uboard-balance-side-check"><a href="#" onclick="return false;">{{$translate['check_4g']}}</a></div>
            </div>
          </div>
          @endif

        </div>
      </div>
    </div>
    <!-- /tsr-section-uboard-balance -->


     <!-- tsr-section-attention -->
   <!--  <div class="tsr-section-attention tsr-color-pink">
      <div class="tsr-container">
        <div class="tsr-module-attention">
          <figure class="tsr-module-attention-icon ts-icon-alert"></figure>
          <div class="tsr-module-attention-text"><strong>Alert:</strong> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
          <a href="#" class="tsr-module-attention-close ts-icon-delete" onclick="return false;"></a>
        </div>
      </div>
    </div> -->
    <!-- /tsr-section-attention -->


    <!-- tsr-section-refiner -->

    <div class="tsr-section-refiner">
    <?php
    if (isset($installment->planedPayments) && $installment->hasInstalment == true){
        $thipaydate = array('firstdate' => $installment->planedPayments[0]->date, 'amount' => $installment->planedPayments[0]->amount);
        $lastdate = explode(' ', $thipaydate['firstdate'])[0];
        $lastdate = implode('-', array_reverse(explode('/', $lastdate)));
            //asdasfajkshfkjhakj
        //$lastdate = '2016-02-15 00:23:00';
        $subtime = date('z',strtotime($lastdate))- date('z', time());
        $leftday = -1;
        if ($subtime <= 0){
            $leftday = 0;
        } else {
            if ($subtime <= 5){
                if ($subtime <= 2){
                    if (!isset($installmentNotification->last_close)){
                        $leftday = 2;
                    } else {
                        if (date('z', time($lastdate)) - date('z', strtotime($installmentNotification->last_close)) >= 2){
                            $leftday = 2;
                        }
                    }
                } else {
                    if (!isset($installmentNotification->last_close)){
                        $leftday = 5;
                    } else {
                        if (date('z', time($lastdate)) - date('z', strtotime($installmentNotification->last_close)) >= 5){
                            $leftday = 5;
                        }
                    }
                }
            } else {
                $leftday = -1;
            }
        }
    }
        ?>

        @if(@isset($installment->planedPayments) && $leftday>=0)
            <div class="tsr-section-attention tsr-section-attention-processed" style="@if($leftday>2){{'background-color: #ff6319;'}} @elseif($leftday>0 && $leftday<3){{'background-color: #ff6319'}}@else{{'background-color: #c41b79'}}@endif">
                <div class="tsr-container">
                    <div class="tsr-module-attention">
                        <figure class="@if($leftday > 2) tsr-module-attention-icon ts-icon-info @elseif($leftday>0) tsr-module-attention-icon ts-icon-time @else tsr-module-attention-icon ts-icon-alert @endif"></figure>
                        <div class="tsr-module-attention-text"><a href="private/private-cabinet/installment"><b>{{$translate['installment_alert']}}:</b> {{$translate['you_should_pay_till']}} {{instalmentDate(isset($thipaydate['firstdate'])?$thipaydate['firstdate']:0 ) }}</a></div>
                        @if($leftday != -1)<div class="tsr-module-attention-close ts-icon-delete" style="cursor: pointer" onclick="closeInstallMent()"></div>@endif
                    </div>
                </div>
            </div>
        @endif

      <div class="tsr-refiner-header tsr-clearfix">
        <div class="tsr-container">
          <div class="tsr-refiner-links">
            <ul>
             @if(isset($menuItems))
                @foreach($menuItems as $pg)

                    <?php if($pg->id==207 && (!isset($installment->hasInstalment) || $installment->hasInstalment==false) ){continue;}  ?>
                    @if($pg->id == 223)
                        @if(isset($directdebit))
                            @if($directdebit === true)
                                <li><a href="{{$lang.'/'.$pg->full_slug}}" class="{{ Request::is($lang.'/'.$pg->full_slug) ? 'is-choosen' : '' }}" >{{$pg->title}}</a></li>
                             @else
                                <li><a href="{{$lang.'/'.$pg->full_slug}}" class="{{ Request::is($lang.'/'.$pg->full_slug) ? 'is-choosen' : '' }}" >{{$pg->title}}</a></li>
                            @endif
                        @else
                                <li><a href="{{$lang.'/'.$pg->full_slug}}" class="{{ Request::is($lang.'/'.$pg->full_slug) ? 'is-choosen' : '' }}" >{{$pg->title}}</a></li>
                        @endif
                    @else
                        <li><a href="{{$lang.'/'.$pg->full_slug}}" class="{{ Request::is($lang.'/'.$pg->full_slug) ? 'is-choosen' : '' }}" >{{$pg->title}}</a></li>
                    @endif


                @endforeach

             @endif 
            </ul>
          </div>
        </div>
      </div>
    </div>
<a style="display: none" href="{{$lang}}/newsletterpopup" data-fancybox-type="ajax" class="js-fancybox fancybox.ajax tsr-avatar-link" onclick="return false;" id="showNewsletter"></a>
<script>

    function closeInstallMent(){
        $.ajax({
            method: "GET",
            url: "dashboard/closeinstallment",
            data: { close: "install" }
        }).done(function( msg ) {
            $('.tsr-section-attention').fadeOut();
        });
    }

    (function() {
        var $check = $('.tsr-uboard-balance-side-check');
        var timeoutID = null;
        $check.on('click', function(event) {
            clearTimeout(timeoutID);
            $('tsr-uboard-balance-side-layer').fadeToggle();
            timeoutID = setTimeout(function () {
                $('tsr-uboard-balance-side-layer').fadeOut();
            }, 5000);
            event || event.preventDefault();
        });
        setTimeout(function(){
            showNewsletterPopup();
        }, 20000);
    })();

    function showNewsletterPopup(){
        if(localStorage.getItem("newsletterpopup") == 'yes'){
            return
        }
        localStorage.setItem("newsletterpopup", "yes");
        $('#showNewsletter').click();
    }

    $(function(){
        //tsr-section-refiner-processed

    });
</script>