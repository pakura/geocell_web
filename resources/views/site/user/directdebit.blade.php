@extends('site')

@section('content')
    @include('site.user.usercabinet-head')
<?php
    $metiMyNum = false;
    $limitMyNum = false;
    $fixedMyNum = false;
?>
        <!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-empty"></div>
<!-- /tsr-section-divider -->

<!-- tsr-section-generic -->
<div class="tsr-section-generic">
    <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
            <h1 class="tsr-title cs-spb-20">{{$translate['automatic_fill']}}</h1>

            <div class="cs-spb-40 cs-bg-shade" style="padding: 20px">

                @if($lang == 'en')
                    The service enables you to automatically top up your mobile account via bankcard
                    <br><br>
                    1. Select the type of automatic refill
                    <br>
                    2. Add the number
                    <br>
                    3. Insert card data
                    <br><br>
                    The service fee 1 Gel will be deducted from your mobile account on monthly basis.
                @else
                    სერვისის საშუალებით თქვენ შეძლებთ ბალანსის ავტომატურ შევსებას ბარათიდან. 
                    <br><br>
                    1. აირჩიეთ ავტომატური შევსების ტიპი
                    <br>
                    2. დაამატეთ ნომერი
                    <br>
                    3. მიუთითეთ ბარათის მონაცემები 
                    <br> <br>
                    სერვისის ღირებულება 1 ლარი ჩამოგეჭრებათ თვეში ერთხელ თქვენი მობილური

                    ნომრის ანგარიშიდან.
                @endif
            </div>



            <div class="cs-spb-40">
                <div class="tsr-com-linklist">
                    <ul class="cs-tx-13 cs-tx-bold"><!--
                --><li><a onclick="changeScreen(1)" class="is-choosen switchDebit">{{$translate['general']}}</a></li><!--

                -->@if(isset($ddInfo->account)) <li><a onclick="getHistory()" class="switchDebit">{{$translate['trans_hist']}}</a></li> @endif<!--

                --><li><a onclick="changeScreen(3)" class="switchDebit">{{$translate['information']}}</a></li><!--
              --></ul>
                </div>
            </div>




            <!-- tsr-autorefill -->
            <div class="cs-spb-40">
                <div class="tsr-uboard-arfill tsr-clearfix">
                @if(isset($ddInfo->account->active) && $ddInfo->account->active == true)
                   <div class="tsr-uboard-arfill-side">
                        <div class="tsr-uboard-arfill-fixator">
                            <div class="tsr-uboard-arfill-sticker">
                                <div class="cs-spb-10"><!--
                      --><span class="cs-al-middle cs-dp-iblock cs-spr-10 cs-cl-purple"><span class="cs-tx-40" data-icon="&#xe67d;"></span></span><!--
                      --><span class="cs-al-middle cs-tx-bold cs-tx-20">{{$translate['payment_method']}}</span><!--
                    --></div>
                                <div class="cs-spb-10">
                                    <div class="cs-spb-04 cs-tx-20">{{isset($ddInfo->account->maskedPan)?$ddInfo->account->maskedPan:'XXXX-XXXX-XXXX-XXXX'}}</div>
                                </div>
                                <div class="cs-spt-20">
                                    <div class="tsr-com-linklist">
                                        <ul class="cs-tx-13 cs-tx-bold"><!--
                          --><li><a href="{{$lang}}/changecard"  class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax" id="xx">{{$translate['change']}}</a></li><!--
                          --><li><a href="{{$lang}}/debitdeactive" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax" id="yy">{{$translate['Remove']}}</a></li><!--
                        --></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    @if(isset($ddInfo->account))
                        <div class="tsr-uboard-arfill-side">
                            <div class="tsr-uboard-arfill-fixator">
                                <div class="tsr-uboard-arfill-sticker">
                                    <div class="cs-spb-10"><!--
                      --><span class="cs-al-middle cs-dp-iblock cs-spr-10 cs-cl-purple"><span class="cs-tx-40" data-icon="&#xe67d;" style="color: #8c8c8c"></span></span><!--
                      --><span class="cs-al-middle cs-tx-bold cs-tx-20">{{$translate['payment_method']}}</span><!--
                    --></div>
                                    <div class="cs-spt-20">
                                        <div class="tsr-com-linklist">
                                            <ul class="cs-tx-13 cs-tx-bold"><!--
                          --><li><a href="{{$lang}}/activecard" id="xx">{{$translate['add_card']}}</a></li><!--
                          --><!--
                        --></ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif
                    <div class="tsr-uboard-arfill-main">
                        <!-- tsr-service -->
                        <div class="cs-spb-40" id="mainDebitWP">
                            <div class="tsr-module-droplist">
                                <div class="tsr-module-droplist-item">
                                    <div class="tsr-module-droplist-header is-choosen" tabindex="0">{{$translate['meti']}}</div>
                                    <div class="tsr-module-droplist-footer">
                                        <div class="tsr-module-droplist-pad">
                                            <div class="cs-spb-20">
                                                @if($lang == 'en')
                                                    Add the number and activate pack “Meti” with automatic prolongation. You can also activate
                                                    the service for your family members and friends. The quantity of numbers is not limited.
                                                @else
                                                    დაამატეთ ნომერი და გაააქტიურეთ პაკეტი მეტი ავტომატური განახლებით.
                                                    ჩაურთეთ პაკეტი მეტი ოჯახის წევრებს და მეგობრებს. დასამატებელი
                                                    ნომრების რაოდენობა შეზღუდული არ არის.
                                                @endif
                                            </div>

                                        @if(isset($ddInfo->subscribers))
                                            <?php
                                            $data = $ddInfo->subscribers;
                                            $metiMyNum = false;
                                            ?>
                                            @foreach($data as $serv)
                                                @if($serv->debitType == 2  && $serv->status != 0)
                                                    <?php
                                                        if($serv->mygsm == true){
                                                            $metiMyNum = true;
                                                        }
                                                    ?>
                                                    <div class="cs-spb-20">
                                                        <div class="tsr-clearfix" @if($serv->retry > 0) style="background: #a4a5a6;padding: 10px;opacity: 0.8;" @endif>
                                                            {{--<div class="cs-spb-20 cs-spl-20 cs-fl-right">--}}
                                                                {{--<div class="tsr-uboard-lr-switch">--}}
                                                                    {{--<div class="tsr-uboard-lr-switch-col-l"><div class="tsr-uboard-lr-switch-txt cs-wspace-nowrap"> @if($serv->status == 1) off @else {!! $translate['suspended_click'] !!} @endif </div></div>--}}
                                                                    {{--<div class="tsr-uboard-lr-switch-col-m">--}}
                                                                        {{--<div class="tsr-com-switch @if($serv->status == 1) is-choosen @endif" onclick="changeStatus(2, {{substr($serv->gsm, 3, 9)}}, {{$serv->subscriberId}}, this, {{$serv->bonusId}}, null, null, null, null, '{{isset($serv->dsc)?$serv->dsc:''}}', 1)"></div>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            <div style="float: right; margin-left: 10px">
                                                                <div class="floatingBarsG" id="meti{{substr($serv->gsm, 3, 9)}}">
                                                                    <div class="blockG" id="rotateG_01"></div>
                                                                    <div class="blockG" id="rotateG_02"></div>
                                                                    <div class="blockG" id="rotateG_03"></div>
                                                                    <div class="blockG" id="rotateG_04"></div>
                                                                    <div class="blockG" id="rotateG_05"></div>
                                                                    <div class="blockG" id="rotateG_06"></div>
                                                                    <div class="blockG" id="rotateG_07"></div>
                                                                    <div class="blockG" id="rotateG_08"></div>
                                                                </div>
                                                            </div>
                                                            <div class="cs-spb-20 cs-spr-20 cs-fl-left">
                                                                <div class="cs-spb-20 @if($serv->status == 2) {{'tsr-uboard-semitran'}} @endif">
                                                                    <div class="cs-tx-24">{{substr($serv->gsm, 3, 9)}} <span class="cs-cl-gray">{{isset($serv->dsc)?'('.$serv->dsc.')':''}}</span> @if($serv->mygsm == true) <span class="cs-al-top cs-tx-16 cs-cl-pink"  data-icon="&#xe646;"></span> @endif </div>
                                                                    <div class="cs-tx-15">{{$translate['tanxa']}}: {{$serv->bonusPrice}} &#x20be;, {{isset($serv->bonusId)?bonusId2text($serv->bonusId):''}}


@if(isset($serv->retry))
    @if($serv->retry == 0)
        @if(isset($serv->nextRequestDate))
            , {{$translate['nextrequestdate']}}: {{$serv->nextRequestDate}}
        @endif
    @else
        @if(isset($serv->nextRequestDate))
            @if($lang == 'en')
                , Pack is not active yet. Next activation attempt {{$serv->nextRequestDate}}
            @else
                , პაკეტი ჯერ არ გააქტიურებულა, შემდეგი აქტივაციის მცდელობა {{$serv->nextRequestDate}}
            @endif
        @endif
    @endif
@else
    @if(isset($serv->nextRequestDate))
    , {{$translate['nextrequestdate']}}: {{$serv->nextRequestDate}}
    @endif
@endif

                                                                    </div>
                                                                </div>
                                                                <div class="cs-spb-00">
                                                                    <div class="tsr-com-linklist">
                                                                        <ul class="cs-tx-13 cs-tx-bold"><!--
                                              --><li><a href="{{$lang}}/initmeti/edit/{{$serv->gsm}}" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['edit']}}</a></li><!--
                                              --><li><a onclick="changeStatus(2, {{substr($serv->gsm, 3, 9)}}, {{$serv->subscriberId}}, this, {{$serv->bonusId}}, null, null, null, null, '{{isset($serv->dsc)?$serv->dsc:''}}', 0)">{{$translate['Remove']}}</a></li><!--
                                            --></ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr class="tsr-com-hr" />
                                                    </div>
                                                @endif
                                            @endforeach
                                    @endif


                                            <div class="cs-spb-20">
                                                @if($metiMyNum == false)
                                                <a href="{{$lang}}/initmeti" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax"><span class="tsr-ico-btn cs-spr-10"><span class="ts-icon-add cs-tx-16 cs-pdx-04"></span></span><span class="cs-tx-13 cs-tx-bold">{{$translate['add_my_number']}}</span></a>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                @endif
                                                <a href="{{$lang}}/initmeti/other" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax"><span class="tsr-ico-btn cs-spr-10"><span class="ts-icon-add cs-tx-16 cs-pdx-04"></span></span><span class="cs-tx-13 cs-tx-bold">{{$translate['add_other_number']}}</span></a>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="tsr-module-droplist-item">
                                    <div class="tsr-module-droplist-header is-choosen" tabindex="0">{{$translate['fixed_date']}}</div>
                                    <div class="tsr-module-droplist-footer">
                                        <div class="tsr-module-droplist-pad">
                                            <div class="cs-spb-20">
                                                @if($lang == 'en')
                                                    Add the number and top up your mobile account with fixed amount on a fixed date on
                                                    monthly basis. The quantity of numbers is not limited.
                                                @else
                                                    დაამატეთ ნომერი და შეივსეთ ბალანსი ფიქსირებული თანხით ყოველთვიურად.

                                                    ავტომატური განახლება მოხდება თქვეში ერთხელ, თქვენ მიერ არჩეულ თარიღში.

                                                    დასამატებელი ნომრების რაოდენობა შეზღუდული არ არის.
                                                @endif
                                            </div>
                                        @if(isset($ddInfo->subscribers))
                                            <?php
                                                $data = $ddInfo->subscribers;
                                                $fixedMyNum = false;
                                            ?>
                                            @foreach($data as $serv)
                                                @if($serv->debitType == 1  && $serv->status != 0)
                                                        <?php
                                                        if($serv->mygsm == true){
                                                            $fixedMyNum = true;
                                                        }
                                                        ?>
                                                    <div class="cs-spb-20">
                                                        <div class="tsr-clearfix" @if($serv->retry > 0) style="background: #a4a5a6;padding: 10px;opacity: 0.8;" @endif>
                                                            {{--<div class="cs-spb-20 cs-spl-20 cs-fl-right">--}}
                                                                {{--<div class="tsr-uboard-lr-switch">--}}
                                                                    {{--<div class="tsr-uboard-lr-switch-col-l"><div class="tsr-uboard-lr-switch-txt cs-wspace-nowrap"> @if($serv->status == 1) off @else {!! $translate['suspended_click'] !!} @endif </div></div>--}}
                                                                    {{--<div class="tsr-uboard-lr-switch-col-m">--}}
                                                                        {{--<div class="tsr-com-switch @if($serv->status == 1) is-choosen @endif" onclick="changeStatus(1, {{substr($serv->gsm, 3, 9)}}, {{$serv->subscriberId}}, this, null, {{$serv->fixedAmount}}, {{$serv->dayOfMonth}}, null, null, '{{isset($serv->dsc)?$serv->dsc:''}}', 1)"></div>--}}

                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            {{--</div>--}}
                                                            <div style="float: right; margin-left: 10px">
                                                                <div class="floatingBarsG" id="fixed{{substr($serv->gsm, 3, 9)}}">
                                                                    <div class="blockG" id="rotateG_01"></div>
                                                                    <div class="blockG" id="rotateG_02"></div>
                                                                    <div class="blockG" id="rotateG_03"></div>
                                                                    <div class="blockG" id="rotateG_04"></div>
                                                                    <div class="blockG" id="rotateG_05"></div>
                                                                    <div class="blockG" id="rotateG_06"></div>
                                                                    <div class="blockG" id="rotateG_07"></div>
                                                                    <div class="blockG" id="rotateG_08"></div>
                                                                </div>
                                                            </div>
                                                            <div class="cs-spb-20 cs-spr-20 cs-fl-left">
                                                                <div class="cs-spb-20 @if($serv->status == 2) {{'tsr-uboard-semitran'}} @endif">
                                                                    <div class="cs-tx-24">{{substr($serv->gsm, 3, 9)}} <span class="cs-cl-gray">{{isset($serv->dsc)?'('.$serv->dsc.')':''}}</span> @if($serv->mygsm == true) <span class="cs-al-top cs-tx-16 cs-cl-pink"  data-icon="&#xe646;"></span> @endif</div>
                                                                    <div class="cs-tx-15">{{$translate['tanxa']}}: {{$serv->fixedAmount}} &#x20be;,

@if(isset($serv->retry))
    @if($serv->retry == 0)
        @if($lang == 'en')
            auto-refill on {{$serv->dayOfMonth}} of every month. @else შევსება თვის {{$serv->dayOfMonth}} რიცხვში
        @endif
    @else
        @if($lang == 'en')
            The account has not refilled yet. Next refill attempt {{$serv->nextRequestDate}}
        @else
            ბალანსი ჯერ არ შევსებულა. შემდეგი მცდელობა {{$serv->nextRequestDate}}
        @endif
    @endif
@else
    @if(isset($serv->dayOfMonth))
        @if($lang == 'en')
            auto-refill on {{$serv->dayOfMonth}} of every month. @else შევსება თვის {{$serv->dayOfMonth}} რიცხვში
        @endif
    @endif
@endif


                                                                    {{--@if(isset($serv->nextRequestDate)), {{$translate['nextrequestdate']}}: {{$serv->nextRequestDate}}@endif--}}
                                                            </div>
                                                                </div>
                                                                <div class="cs-spb-00">
                                                                    <div class="tsr-com-linklist">
                                                                        <ul class="cs-tx-13 cs-tx-bold"><!--
                                             --><li> <a href="{{$lang}}/initfixed/edit/{{$serv->gsm}}" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['edit']}}</a></li><!--
                                              --><li><a onclick="changeStatus(1, '{{substr($serv->gsm, 3, 9)}}', '{{$serv->subscriberId}}', this, null, '{{$serv->fixedAmount}}', '{{$serv->dayOfMonth}}', null, null, '{{isset($serv->dsc)?$serv->dsc:''}}', 0)"> {{$translate['Remove']}} </a></li><!--

                                            --></ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr class="tsr-com-hr" />
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                            <div class="cs-spb-20">
                                                @if($fixedMyNum == false)
                                                    <a href="{{$lang}}/initfixed" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax"><span class="tsr-ico-btn cs-spr-10"><span class="ts-icon-add cs-tx-16 cs-pdx-04"></span></span><span class="cs-tx-13 cs-tx-bold">{{$translate['add_my_number']}}</span></a>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                                @endif
                                                <a href="{{$lang}}/initfixed/other" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax"><span class="tsr-ico-btn cs-spr-10"><span class="ts-icon-add cs-tx-16 cs-pdx-04"></span></span><span class="cs-tx-13 cs-tx-bold">{{$translate['add_other_number']}}</span></a>

                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="tsr-module-droplist-item">
                                    <div class="tsr-module-droplist-header is-choosen" tabindex="0">{{$translate['top_low_limit']}}</div>
                                    <div class="tsr-module-droplist-footer">
                                        <div class="tsr-module-droplist-pad">
                                            <div class="cs-spb-20">
                                                @if($lang == 'en')
                                                    Add the number and top up mobile account immediately as it goes down the low threshold.
                                                    The quantity of numbers is not limited.
                                                @else
                                                    დაამატეთ ნომერი და შეივსეთ ბალანსი სასურველ ლიმიტამდე. შევსება მოხდება
                                                    ავტომატურად, როგორც კი ბალანსი ჩამოცდება მითითებულ ლიმიტს.
                                                    დასამატებელი ნომრების რაოდენობა შეზღუდული არ არის.
                                                @endif
                                            </div>
                                            @if(isset($ddInfo->subscribers))
                                                <?php
                                                     $data = $ddInfo->subscribers;
                                                     $limitMyNum = false;
                                                ?>
                                                @foreach($data as $serv)
                                                @if($serv->debitType == 0 && $serv->status != 0)
                                                    <?php
                                                        if($serv->mygsm == true){
                                                            $limitMyNum = true;
                                                        }
                                                    ?>
                                                        <div class="cs-spb-20">
                                                            <div class="tsr-clearfix" @if($serv->retry > 0) style="background: #a4a5a6;padding: 10px;opacity: 0.8;" @endif>

                                                                <div style="float: right; margin-left: 10px">
                                                                    <div class="floatingBarsG" id="limit{{substr($serv->gsm, 3, 9)}}">
                                                                        <div class="blockG" id="rotateG_01"></div>
                                                                        <div class="blockG" id="rotateG_02"></div>
                                                                        <div class="blockG" id="rotateG_03"></div>
                                                                        <div class="blockG" id="rotateG_04"></div>
                                                                        <div class="blockG" id="rotateG_05"></div>
                                                                        <div class="blockG" id="rotateG_06"></div>
                                                                        <div class="blockG" id="rotateG_07"></div>
                                                                        <div class="blockG" id="rotateG_08"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="cs-spb-20 cs-spr-20 cs-fl-left">
                                                                    <div class="cs-spb-20 @if($serv->status == 2) {{'tsr-uboard-semitran'}} @endif">
                                                                        <div class="cs-tx-24">{{substr($serv->gsm, 3, 9)}} <span class="cs-cl-gray">{{isset($serv->dsc)?'('.$serv->dsc.')':''}}</span> @if($serv->mygsm == true) <span class="cs-al-top cs-tx-16 cs-cl-pink"  data-icon="&#xe646;"></span> @endif</div>
                                                                        <div class="cs-tx-15">@if($lang == 'ge') ზედა ლიმიტი - {{$serv->minimalBalance}} &#x20be;, ქვედა ლიმიტი - {{$serv->targetBalance}} @else  Upper limit - {{$serv->minimalBalance}} &#x20be; GEL, lower limit – {{$serv->targetBalance}} GEL @endif  &#x20be;


@if(isset($serv->retry))
    @if($serv->retry == 0)
        @if($lang == 'en')
            auto-refill on {{$serv->dayOfMonth}} of every month. @else შევსება თვის {{$serv->dayOfMonth}} რიცხვში
        @endif
    @else
        @if($lang == 'en')
            The account has not refilled yet. Next refill attempt {{$serv->nextRequestDate}}
        @else
            ბალანსი ჯერ არ შევსებულა. შემდეგი მცდელობა {{$serv->nextRequestDate}}
        @endif
    @endif
@else
    @if(isset($serv->dayOfMonth))
        @if($lang == 'en')
            auto-refill on {{$serv->dayOfMonth}} of every month. @else შევსება თვის {{$serv->dayOfMonth}} რიცხვში
        @endif
    @endif
@endif






                                                                </div>
                                                            </div>
                                                            <div class="cs-spb-00">
                                                                <div class="tsr-com-linklist">
                                                                    <ul class="cs-tx-13 cs-tx-bold"><!--
                                                      --><li><a href="{{$lang}}/initlimit/edit/{{$serv->gsm}}" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['edit']}}</a></li><!--
                                                      --><li><a onclick="changeStatus(0, {{substr($serv->gsm, 3, 9)}}, {{$serv->subscriberId}}, this,  null, null, null, {{$serv->minimalBalance}}, {{$serv->targetBalance}}, '{{isset($serv->dsc)?$serv->dsc:''}}', 0)">{{$translate['Remove']}}</a></li><!--
                                                    --></ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                        </div>
                                        <hr class="tsr-com-hr" />
                                    </div>
                                    @endif
                                    @endforeach
                                    @endif
                                    <div class="cs-smv-02">
                                    @if(!isset($limitMyNum) || $limitMyNum == false)
                                    <a href="{{$lang}}/initlimit" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax"><span class="tsr-ico-btn cs-spr-10"><span class="ts-icon-add cs-tx-16 cs-pdx-04"></span></span><span class="cs-tx-13 cs-tx-bold">{{$translate['add_my_number']}}</span></a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    @endif
                                    <a href="{{$lang}}/initlimit/other" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax"><span class="tsr-ico-btn cs-spr-10"><span class="ts-icon-add cs-tx-16 cs-pdx-04"></span></span><span class="cs-tx-13 cs-tx-bold">{{$translate['add_other_number']}}</span></a>
                                    </div>

                                </div>
                            </div>
                        </div>



                            </div>
                        </div>

                        <div id="debitHistoryWP" style="display: none">
                            <div class="scroll-wrapper tsr-scrollpane" style="position: relative;"><div class="tsr-scrollpane scroll-content" style="height: auto; margin-bottom: 0px; margin-right: 0px; max-height: 470px;">
                                    <div class="tsr-scrollpane-content">
                                        <table class="tsr-com-table-standard" style="width: 100%">
                                            <thead>
                                            <tr style="width: 100%">
                                                <th style="width: 33%">{{$translate['datetime']}}</th>
                                                <th style="width: 33%">{{$translate['serv_name']}}</th>
                                                <th style="width: 33%">{{$translate['tanxa']}}</th>
                                            </tr>
                                            </thead>
                                            <tbody id="historyTable">

                                            </tbody>
                                        </table>
                                    </div>
                                </div><div class="scroll-element scroll-x"><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar" style="width: 100px;"></div></div></div><div class="scroll-element scroll-y"><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar" style="height: 100px;"></div></div></div></div>
                            <div class="tsr-section-divider tsr-divider-empty"></div>
                        </div>
                        <div id="debitInfoWP" style="display: none">
                            <div class="scroll-wrapper tsr-scrollpane" style="position: relative;"><div class="tsr-scrollpane scroll-content" style="height: auto; margin-bottom: 0px; margin-right: 0px; max-height: 470px;">
                                    <div class="tsr-scrollpane-content">

                                        <div class="cs-spb-40">
                                            <div class="cs-pdx-20 cs-bg-shade">
                                                {!! isset($item->description)?$item->description:'' !!}
                                            </div>
                                        </div>
                                    </div>
                                </div><div class="scroll-element scroll-x"><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar" style="width: 100px;"></div></div></div><div class="scroll-element scroll-y"><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar" style="height: 100px;"></div></div></div></div>
                            <div class="tsr-section-divider tsr-divider-empty"></div>
                        </div>
                        @if(isset($ddInfo->account))
                        <div class="cs-spb-40" id="deactivate_wp">
                            <div class="cs-pdx-20">
                               @if($lang == 'en')
                                    <div class="cs-spb-00"><a href="{{$lang}}/debitdeactive" style="padding: 19px 17px;" class="tsr-btn tsr-btn-gray composite js-fancybox fancybox.ajax" data-fancybox-type="ajax">Deactivate</a></div>
                               @else
                                    <div class="cs-spb-00"><a href="{{$lang}}/debitdeactive" style="padding: 19px 17px;" class="tsr-btn tsr-btn-gray composite js-fancybox fancybox.ajax" data-fancybox-type="ajax" >დეაქტივაცია</a></div>
                               @endif
                            </div>
                        </div>
                        @endif
                        <!-- /tsr-service -->

                    </div>
                </div>
            </div>
            <!-- /tsr-autorefill -->
        </div>
    </div>
</div>
<a href="{{$lang}}/debitbalance" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax" style="display: none" id="fillbalance"></a>
<a href="{{$lang}}/debiterror" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax" style="display: none" id="debiterror"></a>
<a href="{{$lang}}/debitalready" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax" style="display: none" id="debitalready"></a>
<a href="{{$lang}}/debitsucc" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax" style="display: none" id="debitsucc"></a>
<a href="{{$lang}}/notallow" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax" style="display: none" id="notallow"></a>
<a href="{{$lang}}/limitdeactive" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax" style="display: none" id="limitdeactive"></a>
<a href="{{$lang}}/nolailai" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax" style="display: none" id="nolailai"></a>



<script>/*<![CDATA[*/

    var actionStatus = true;
    function changeStatus(debitType, gsm, subId, obj, bonusid, amount, fixday, low, top, desc, status){

        if(actionStatus === false){
            return
        }
        $('#floatingBarsG').fadeIn();
        actionStatus = false;
        if(status == 1){
            if($(obj).hasClass('is-choosen')){
                status = 2;
            } else {
                status = 1;
            }
        } else {
            status = 0;
        }
        if(debitType == 0){
            document.getElementById('limit'+gsm).style.display = 'block';
        }
        if(debitType == 1){
            document.getElementById('fixed'+gsm).style.display = 'block';
        }
        if(debitType == 2){
            document.getElementById('meti'+gsm).style.display = 'block';
        }



















        $.ajax({
                    method: "POST",
                    url: "{{$lang}}/initReq",
                    async: false,
                    data: {
                        status: status,
                        debittype: debitType,
                        phone: gsm,
                        subid: subId,
                        metibundle: bonusid,
                        amount: amount,
                        day: fixday,
                        low: low,
                        top: top,
                        servname: desc,
                        ajax: 1
                    }
                })
                .done(function( msg ) {
                    if(msg == 'limitdeactive'){
                        var rand = parseInt(Math.random()*10000);
                        location.href = '{{$lang}}/private/private-cabinet/direct-debit?status=limitdeactive&r='+rand;
                    } else {
                        location.reload();
                    }
                    actionStatus = true;
                    $('#floatingBarsG').fadeOut();
                });
    }





    $(document).ready(function() {
        $(window).scrollTop($('.tsr-section-refiner').offset().top);
        setTimeout(function () {
            $(window).scrollTop($('.tsr-section-refiner').offset().top);

        }, 500);
        var token = '{{isset($_GET['r'])?$_GET['r']:''}}';
        @if(isset($_GET['status']) && $_GET['status'] == 'balance')
        setTimeout(function () {
            if(localStorage.getItem("debitStatus") != token){
                $('#fillbalance').click();
                localStorage.debitStatus = token;
            }

        },1000);
        @endif

        @if(isset($_GET['status']) && $_GET['status'] == 'limitdeactive')
        setTimeout(function () {
            if(localStorage.getItem("debitStatus") != token){
                $('#limitdeactive').click();
                localStorage.debitStatus = token;
            }

        },1000);
        @endif


        @if(isset($_GET['status']) && $_GET['status'] == 'notallow')
        setTimeout(function () {
            if(localStorage.getItem("debitStatus") != token){
                $('#notallow').click();
                localStorage.debitStatus = token;
            }

        },1000);
        @endif

        @if(isset($_GET['status']) && $_GET['status'] == 'already')
        setTimeout(function () {
            if(localStorage.getItem("debitStatus") != token){
                $('#debitalready').click();
                localStorage.debitStatus = token;
            }

        },1000);
        @endif

        @if(isset($_GET['status']) && $_GET['status'] == 'f')
        setTimeout(function () {
            if(localStorage.getItem("debitStatus") != token){
                $('#debiterror').click();
                localStorage.debitStatus = token;
            }
        },1000);
        @endif

        @if(isset($_GET['status']) && $_GET['status'] == 'nolailai')
        setTimeout(function () {
            if(localStorage.getItem("debitStatus") != token){
                $('#nolailai').click();
                localStorage.debitStatus = token;
            }
        },1000);
        @endif

        @if(isset($_GET['status']) && $_GET['status'] == 's')
        setTimeout(function () {
            if(localStorage.getItem("debitStatus") != token){
                $('#debitsucc').click();
                localStorage.debitStatus = token;
            }
        },1000);
        @endif
        /* tooltips */
        (function() {
            $('.js-tooltip-text')
                    .css({'cursor': 'pointer'})
                    .tooltipster({
                        'maxWidth': 260,
                        'position': 'top'
                    });
        })();

        /* period button */
        (function() {
            $('.js-btn-period-button').on('click', function() {
                $('.js-btn-period-container').slideToggle(300, 'easeOutExpo');
            });
        })();



        /* promo */
        (function() {

            var $check = $('.tsr-uboard-balance-side-check');
            var $layer = $('.tsr-uboard-balance-side-layer');
            var timeoutID = null;
            var showLayer = function() {
                if (timeoutID) {
                    window.clearTimeout(timeoutID);
                }
                $layer.stop(true, false).fadeIn(300);
                timeoutID = window.setTimeout(function() {
                    $check.trigger('click');
                }, 5000);
            };
            var hideLayer = function() {
                if (timeoutID) {
                    window.clearTimeout(timeoutID);
                }
                $layer.stop(true, false).fadeOut(300);
            };
            $check.on('click', function(event) {
                if (!$layer.is(':visible')) {
                    showLayer();
                } else {
                    hideLayer();
                }
                event || event.preventDefault();
            });
        })();

    });

    function changeScreen(screen){
        document.getElementById('mainDebitWP').style.display = 'none';
        document.getElementById('debitHistoryWP').style.display = 'none';
        document.getElementById('debitInfoWP').style.display = 'none';
        $('.switchDebit').removeClass('is-choosen');

        if(screen == 1){
            document.getElementById('mainDebitWP').style.display = 'block';
            document.getElementsByClassName("switchDebit")[0].classList.add("is-choosen");
            $('#deactivate_wp').fadeIn();
        } else {
            if(screen == 2){
                $('#deactivate_wp').fadeOut();
                document.getElementById('debitHistoryWP').style.display = 'block';
                document.getElementsByClassName("switchDebit")[1].classList.add("is-choosen");
            } else {
                $('#deactivate_wp').fadeOut();
                document.getElementById('debitInfoWP').style.display = 'block';
                document.getElementsByClassName("switchDebit")[2].classList.add("is-choosen");
            }
        }

    }

    function getHistory(){
        $.ajax({
                    method: "POST",
                    url: "{{$lang}}/gethistory",
                    data: {}
                })
                .done(function( msg ) {
                    console.log(JSON.parse(msg));
                    writeHistory(JSON.parse(msg));
                    changeScreen(2);
                });
    }


    function writeHistory(hist){
        if(hist == '[]'){
            return;
        }
        var html = '';
        for(var ii in hist){
            html += '<tr>\
                    <td>'+hist[ii].transactionDate+'</td>\
                    <td>'+setvidtoname(hist[ii].transactionType)+'</td>\
                    <td>'+parseFloat(hist[ii].amount).toFixed(2)+' ₾</td>\
                    </tr>';
        }
        $('#historyTable').html(html);
    }

    function setvidtoname(arg){
        arg = parseInt(arg);
        switch (arg){
            case 0:
                return '{{$translate['deposit']}}';
                break;
            case 1:
                return '{{$translate['bucket_meti']}}';
                break;
            default:
                return '{{$translate['deposit']}}';
                break;
        }
    }

    /*]]>*/</script>



<!-- /tsr-section-generic -->
<script src="site/tsr-modules/tsr-droplist/tsr-droplist.js"></script>
<script src="site/tsr-core/tsr-scripts/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script src="site/tsr-sections/tsr-uboard/tsr-uboard-autorefill.js"></script>
<!-- *************** /JAVASCRIPTS *************** -->



<?php

        function bonusId2text($id){
            switch ($id){
                case 1:
                    return 'METI S';
                    break;
                case 2:
                    return 'METI M';
                    break;
                case 3:
                    return 'METI U';
                    break;
                case 4:
                    return 'METI U+';
                    break;
                default:
                    return 'METI';
                    break;
            }
        }

?>


<style>
    .floatingBarsG{
        position:relative;
        width:15px;
        height:19px;
        margin:auto;
        display: none;
        float: right;
    }

    .blockG{
        position:absolute;
        background-color:rgb(255,255,255);
        width:2px;
        height:6px;
        border-radius:2px 2px 0 0;
        -o-border-radius:2px 2px 0 0;
        -ms-border-radius:2px 2px 0 0;
        -webkit-border-radius:2px 2px 0 0;
        -moz-border-radius:2px 2px 0 0;
        transform:scale(0.4);
        -o-transform:scale(0.4);
        -ms-transform:scale(0.4);
        -webkit-transform:scale(0.4);
        -moz-transform:scale(0.4);
        animation-name:fadeG;
        -o-animation-name:fadeG;
        -ms-animation-name:fadeG;
        -webkit-animation-name:fadeG;
        -moz-animation-name:fadeG;
        animation-duration:1.2s;
        -o-animation-duration:1.2s;
        -ms-animation-duration:1.2s;
        -webkit-animation-duration:1.2s;
        -moz-animation-duration:1.2s;
        animation-iteration-count:infinite;
        -o-animation-iteration-count:infinite;
        -ms-animation-iteration-count:infinite;
        -webkit-animation-iteration-count:infinite;
        -moz-animation-iteration-count:infinite;
        animation-direction:normal;
        -o-animation-direction:normal;
        -ms-animation-direction:normal;
        -webkit-animation-direction:normal;
        -moz-animation-direction:normal;
    }

    #rotateG_01{
        left:0;
        top:7px;
        animation-delay:0.45s;
        -o-animation-delay:0.45s;
        -ms-animation-delay:0.45s;
        -webkit-animation-delay:0.45s;
        -moz-animation-delay:0.45s;
        transform:rotate(-90deg);
        -o-transform:rotate(-90deg);
        -ms-transform:rotate(-90deg);
        -webkit-transform:rotate(-90deg);
        -moz-transform:rotate(-90deg);
    }

    #rotateG_02{
        left:2px;
        top:2px;
        animation-delay:0.6s;
        -o-animation-delay:0.6s;
        -ms-animation-delay:0.6s;
        -webkit-animation-delay:0.6s;
        -moz-animation-delay:0.6s;
        transform:rotate(-45deg);
        -o-transform:rotate(-45deg);
        -ms-transform:rotate(-45deg);
        -webkit-transform:rotate(-45deg);
        -moz-transform:rotate(-45deg);
    }

    #rotateG_03{
        left:6px;
        top:1px;
        animation-delay:0.75s;
        -o-animation-delay:0.75s;
        -ms-animation-delay:0.75s;
        -webkit-animation-delay:0.75s;
        -moz-animation-delay:0.75s;
        transform:rotate(0deg);
        -o-transform:rotate(0deg);
        -ms-transform:rotate(0deg);
        -webkit-transform:rotate(0deg);
        -moz-transform:rotate(0deg);
    }

    #rotateG_04{
        right:2px;
        top:2px;
        animation-delay:0.9s;
        -o-animation-delay:0.9s;
        -ms-animation-delay:0.9s;
        -webkit-animation-delay:0.9s;
        -moz-animation-delay:0.9s;
        transform:rotate(45deg);
        -o-transform:rotate(45deg);
        -ms-transform:rotate(45deg);
        -webkit-transform:rotate(45deg);
        -moz-transform:rotate(45deg);
    }

    #rotateG_05{
        right:0;
        top:7px;
        animation-delay:1.05s;
        -o-animation-delay:1.05s;
        -ms-animation-delay:1.05s;
        -webkit-animation-delay:1.05s;
        -moz-animation-delay:1.05s;
        transform:rotate(90deg);
        -o-transform:rotate(90deg);
        -ms-transform:rotate(90deg);
        -webkit-transform:rotate(90deg);
        -moz-transform:rotate(90deg);
    }

    #rotateG_06{
        right:2px;
        bottom:2px;
        animation-delay:1.2s;
        -o-animation-delay:1.2s;
        -ms-animation-delay:1.2s;
        -webkit-animation-delay:1.2s;
        -moz-animation-delay:1.2s;
        transform:rotate(135deg);
        -o-transform:rotate(135deg);
        -ms-transform:rotate(135deg);
        -webkit-transform:rotate(135deg);
        -moz-transform:rotate(135deg);
    }

    #rotateG_07{
        bottom:0;
        left:6px;
        animation-delay:1.35s;
        -o-animation-delay:1.35s;
        -ms-animation-delay:1.35s;
        -webkit-animation-delay:1.35s;
        -moz-animation-delay:1.35s;
        transform:rotate(180deg);
        -o-transform:rotate(180deg);
        -ms-transform:rotate(180deg);
        -webkit-transform:rotate(180deg);
        -moz-transform:rotate(180deg);
    }

    #rotateG_08{
        left:2px;
        bottom:2px;
        animation-delay:1.5s;
        -o-animation-delay:1.5s;
        -ms-animation-delay:1.5s;
        -webkit-animation-delay:1.5s;
        -moz-animation-delay:1.5s;
        transform:rotate(-135deg);
        -o-transform:rotate(-135deg);
        -ms-transform:rotate(-135deg);
        -webkit-transform:rotate(-135deg);
        -moz-transform:rotate(-135deg);
    }



    @keyframes  fadeG{
        0%{
            background-color:rgb(0,0,0);
        }

        100%{
            background-color:rgb(255,255,255);
        }
    }

    @-o-keyframes fadeG{
        0%{
            background-color:rgb(0,0,0);
        }

        100%{
            background-color:rgb(255,255,255);
        }
    }

    @-ms-keyframes fadeG{
    0%{
        background-color:rgb(0,0,0);
    }

    100%{
        background-color:rgb(255,255,255);
    }
    }

    @-webkit-keyframes fadeG{
        0%{
            background-color:rgb(0,0,0);
        }

        100%{
            background-color:rgb(255,255,255);
        }
    }

    @-moz-keyframes fadeG{
        0%{
            background-color:rgb(0,0,0);
        }

        100%{
            background-color:rgb(255,255,255);
        }
    }
</style>

@endsection