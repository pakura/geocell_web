
@extends('site')

@section('content')
@include('site.user.usercabinet-head')
        <!-- tsr-section-uboard-have -->
<div class="tsr-section-uboard-myprods">
    <div class="tsr-container">
        <h1 class="tsr-title">{{$translate['you_have']}}</h1>
        <div class="tsr-uboard-myprods tsr-grid tsr-grid-separate tsr-clearfix"><!--
          --><div class="tsr-uboard-myprod tsr-col-box tsr-col-00">
                <div class="tsr-uboard-myprod-head">
                    <div class="composite"><!--
                --><div class="cs-spr-10"><img src="site/tsr-core/tsr-images/tsr-icon-48-white-phone.png" alt=""></div><!--
                --><div class="cs-spr-00"><div>{{$translate['total_minutes']}}</div><div class="cs-tx-22">

                                {{@$actServices[3]['amount']?convertMinutes(@$actServices[3]['amount']):0}}

                            </div></div><!--
              --></div>
                </div>
                <div class="tsr-uboard-myprod-body">
                    @if(isset($actServices[3]))
                        @foreach(@$actServices[3]['data'] as $service)
                            @if(@$service->data->amount>0 || @$service->data->amount==-1)
                                <div class="tsr-uboard-myprod-serv">
                                    <div class="tsr-uboard-myprod-serv-desc">
                                        <div class="tsr-uboard-myprod-serv-name">{{ $service->description }} <!-- <span class="tsr-inline-btn tsr-btn-pink">auto</span> --></div>
                                        <div class="tsr-uboard-myprod-serv-info">{{ convertMinutes($service->data->amount)}}<span class="tsr-com-sup cs-tx-bold cs-cl-link">?</span></div>

                                        @if(@$service->data->expirationDate)
                                            <div class="tsr-uboard-myprod-serv-ttip"><div class="tsr-clearfix"><div class="cs-al-center cs-spb-10"><span class="cs-tx-bold">{{$translate['expires_on']}}</span><br>{{$userInfo->return->brand==1? convertDateTime(@$service->data->expirationDate):convertDate(@$service->data->expirationDate)}}</div><!-- <div class="cs-al-center"><a href="#" class="tsr-btn tsr-btn-xsmall">Prolong now</a></div> --></div></div>
                                        @endif

                                    </div>
                                    <div class="tsr-uboard-myprod-serv-pbar"><i style="width:@if(@$service->data->defaultAmount>0 ){{($service->data->amount/@$service->data->defaultAmount)*100}}@elseif($service->data->amount==-1){{100}}@else{{0}}@endif%"></i></div>
                                </div>
                            @endif
                        @endforeach
                    @endif

                </div>
            </div><!--
          --><div class="tsr-uboard-myprod tsr-col-box tsr-col-00">
                <div class="tsr-uboard-myprod-head">
                    <div class="composite"><!--
                --><div class="cs-spr-10"><img src="site/tsr-core/tsr-images/tsr-icon-48-white-message.png" alt=""></div><!--
                --><div class="cs-spr-00"><div>{{$translate['total_msg']}}</div><div class="cs-tx-22">
                                @if(@$actServices[2]['amount'] && @$actServices[2]['amount']!=0)
                                    {{@$actServices[2]['amount']<0?trans('site.unlimited'):@$actServices[2]['amount']}}
                                @else
                                    0
                                @endif
                            </div></div><!--
              --></div>
                </div>
                <div class="tsr-uboard-myprod-body">
                    @if(isset($actServices[2]))
                        @foreach(@$actServices[2]['data'] as $service)
                            @if(@$service->data->amount>0  || @$service->data->amount==-1)
                                <div class="tsr-uboard-myprod-serv">
                                    <div class="tsr-uboard-myprod-serv-desc">
                                        <div class="tsr-uboard-myprod-serv-name">{{ $service->description }} <!-- <span class="tsr-inline-btn tsr-btn-pink">auto</span> --></div>
                                        <div class="tsr-uboard-myprod-serv-info">{{ $service->data->amount==-1?trans('site.unlimited'):$service->data->amount}} SMS <span class="tsr-com-sup cs-tx-bold cs-cl-link">?</span></div>

                                        @if(@$service->data->expirationDate)
                                            <div class="tsr-uboard-myprod-serv-ttip"><div class="tsr-clearfix"><div class="cs-al-center cs-spb-10"><span class="cs-tx-bold">{{$translate['expires_on']}} </span><br>{{$userInfo->return->brand==1? convertDateTime(@$service->data->expirationDate):convertDate(@$service->data->expirationDate)}}</div><!-- <div class="cs-al-center"><a href="#" class="tsr-btn tsr-btn-xsmall">Prolong now</a></div> --></div></div>
                                        @endif
                                    </div>
                                    <div class="tsr-uboard-myprod-serv-pbar"><i style="width:@if(@$service->data->defaultAmount>0 ){{($service->data->amount/@$service->data->defaultAmount)*100}}@elseif($service->data->amount==-1){{100}}@else{{0}}@endif%"></i></div>
                                </div>
                            @endif
                        @endforeach
                    @endif
                </div>
            </div><!--
          --><div class="tsr-uboard-myprod tsr-col-box tsr-col-00">
                <div class="tsr-uboard-myprod-head">
                    <div class="composite"><!--
                --><div class="cs-spr-10"><img src="site/tsr-core/tsr-images/tsr-icon-48-white-traffic.png" alt=""></div><!--
                --><div class="cs-spr-00"><div>{{$translate['total_traffic']}}</div><div class="cs-tx-22">{{convertBalance(isset($actServices[1]['amount'])?$actServices[1]['amount']:0 )}} </div></div><!--
              --></div>
                </div>
                <div class="tsr-uboard-myprod-body">

                    @if(isset($actServices[1]) && isset($actServices[1]['data']))

                        @foreach(@$actServices[1]['data'] as $service)
                            @if(@$service->data->amount>0)
                                <div class="tsr-uboard-myprod-serv">
                                    <div class="tsr-uboard-myprod-serv-desc">
                                        <div class="tsr-uboard-myprod-serv-name">{{ $service->description }} <!-- <span class="tsr-inline-btn tsr-btn-pink">auto</span> --></div>
                                        <div class="tsr-uboard-myprod-serv-info">{{ convertBalance($service->data->amount)}}<span class="tsr-com-sup cs-tx-bold cs-cl-link">?</span></div>

                                        @if(@$service->data->expirationDate)
                                            <div class="tsr-uboard-myprod-serv-ttip"><div class="tsr-clearfix"><div class="cs-al-center cs-spb-10"><span class="cs-tx-bold">{{$translate['expires_on']}}</span><br>{{($userInfo->return->brand==1 && $service->hide_only_time!=1)? convertDateTime(@$service->data->expirationDate):convertDate(@$service->data->expirationDate)}}</div><!-- <div class="cs-al-center"><a href="#" class="tsr-btn tsr-btn-xsmall">Prolong now</a></div> --></div></div>
                                        @endif

                                    </div>
                                    <div class="tsr-uboard-myprod-serv-pbar"><i style="width:  {{@$service->data->defaultAmount>0?($service->data->amount/@$service->data->defaultAmount)*100 : 0}}%"></i></div>
                                </div>
                            @endif
                        @endforeach
                    @endif

                    @if(isset($actServices[100]) && isset($actServices[100]['data']))
                        @foreach(@$actServices[100]['data'] as $service)
                            @if(@$service->data->amount>0)
                                <div class="tsr-uboard-myprod-serv">
                                    <div class="tsr-uboard-myprod-serv-desc">
                                        <div class="tsr-uboard-myprod-serv-name">{{ $service->description }} <!-- <span class="tsr-inline-btn tsr-btn-pink">auto</span> --></div>
                                        <div class="tsr-uboard-myprod-serv-info">{{ convertBalance($service->data->amount)}}<span class="tsr-com-sup cs-tx-bold cs-cl-link">?</span></div>

                                        @if(@$service->data->expirationDate)
                                            <div class="tsr-uboard-myprod-serv-ttip"><div class="tsr-clearfix"><div class="cs-al-center cs-spb-10"><span class="cs-tx-bold">{{$translate['expires_on']}}</span><br>{{($userInfo->return->brand==1 && $service->hide_only_time!=1)? convertDateTime(@$service->data->expirationDate):convertDate(@$service->data->expirationDate)}}</div><!-- <div class="cs-al-center"><a href="#" class="tsr-btn tsr-btn-xsmall">Prolong now</a></div> --></div></div>
                                        @endif

                                    </div>
                                    <div class="tsr-uboard-myprod-serv-pbar"><i style="width:  {{@$service->data->defaultAmount>0?($service->data->amount/@$service->data->defaultAmount)*100 : 0}}%"></i></div>
                                </div>
                            @endif
                        @endforeach
                    @endif


                </div>
            </div>


            <!--   @if(isset($actServices[4]))
                    <div class="tsr-uboard-myprod tsr-col-box tsr-col-00">
                      <div class="tsr-uboard-myprod-head">
                        <div class="composite">
                          <div class="cs-spr-10"><img src="site/tsr-core/tsr-images/tsr-icon-48-white-message.png" alt=""></div>
                          <div class="cs-spr-00"><div>{{$translate['roaming_mins']}}</div><div class="cs-tx-22">{{@$actServices[4]['amount']?@$actServices[4]['amount'] :0}}</div></div>
             </div>
            </div>

            <div class="tsr-uboard-myprod-body">

                @foreach(@$actServices[4]['data'] as $service)
                    <div class="tsr-uboard-myprod-serv">
                      <div class="tsr-uboard-myprod-serv-desc">
                        <div class="tsr-uboard-myprod-serv-name">{{ $service->description_en }} </div>
                      <div class="tsr-uboard-myprod-serv-info">{{ convertMinutes(@$service->data->amount)}}<span class="tsr-com-sup cs-tx-bold cs-cl-link">?</span></div>
                      <div class="tsr-uboard-myprod-serv-ttip"><div class="tsr-clearfix"><div class="cs-al-center cs-spb-10"><span class="cs-tx-bold">Expires on:</span><br>{{$userInfo->return->brand==1? convertDateTime(@$service->data->expirationDate):convertDate(@$service->data->expirationDate)}}</div>  </div></div>
                  </div>
                    <div class="tsr-uboard-myprod-serv-pbar"><i style="width:  {{$service->total_amount>0?($service->data->amount/$service->total_amount)*100 : 0}}%"></i></div>
                  </div>
                  @endforeach

                    </div>
                  </div>
                   @endif      -->
            <!-- roaming is ended -->

        </div>
    </div>
</div>
<!-- /tsr-section-uboard-have -->

<!-- tsr-section-uboard-dashboard -->
<div class="tsr-section-uboard-dashboard">
    <div class="tsr-container">
        <div class="tsr-dashboard-grid tsr-grid tsr-grid-separate tsr-clearfix">
            <div class="tsr-col-12">
                <div class="tsr-dashboard-block-wrap">
                    <div class="tsr-dashboard-block-wrap-head">
                        <div class="tsr-dashboard-block-wrap-head-tb">
                            <div class="tsr-dashboard-block-wrap-head-tb-lt"><h3 class="tsr-title">{{$translate['my_services']}}</h3></div>
                            <div class="tsr-dashboard-block-wrap-head-tb-gt"><a href="javascript: $('#available_services').slideToggle();void(0);" class="composite"  ><span class="ts-icon-add cs-dp-inline-block cs-tx-22 cs-spr-04"></span><span>{{$translate['add_service']}}</span></a></div>
                        </div>
                    </div>

                    <div id="available_services" style="display:none;">
                        @include('site.lightbox.add-service')
                    </div>


                    <div class="tsr-dashboard-block-wrap-body">
                        <div class="tsr-uboard-services tsr-uboard-services-scenario-a">

                            @if( isset($actServices[99]) )
                                @foreach($actServices[99]['data'] as $data)
                                    <?php
                                    $icon = $data->service_type_group==4?$data->meti_file:$data->file;

                                    $src = ($data->file)?'uploads/products/thumbs/'.$icon:'site/tsr-temp/tsr-images/service-01.png';

                                    ?>
                                    <div class="tsr-uboard-service">
                                        <div class="tsr-uboard-service-modal"></div>
                                        <div class="tsr-uboard-service-head">
                                            <div class="tsr-uboard-service-head-col-01">
                                                <div class="tsr-clearfix">
                                                    <div class="tsr-uboard-service-link tsr-clearfix">
                                                        <figure class="tsr-uboard-service-image">
                                                            <img alt="" src="{{$src}}">
                                                        </figure>
                                                        <div class="tsr-uboard-service-desc">
                                                            <div class="tsr-uboard-service-title">{{$data->description}}</div>

                                                            @if(isset($data->data->status) && $data->data->status==2)
                                                                <div class="tsr-uboard-service-intro">{{$translate['susp_service']}}</div>
                                                            @elseif($data->daily)
                                                                <div class="tsr-uboard-service-intro">{{$translate['validity_daily']}}</div>

                                                            @elseif (isset($data->data->expirationDate) && !$data->hideDate)
                                                                <div class="tsr-uboard-service-intro">{{trans('site.validity')}} {{convertDate($data->data->expirationDate)}} </div>
                                                            @elseif($data->hideDate)
                                                                <div class="tsr-uboard-service-intro">{{$translate['uvado']}}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            @if(isset($data->data->removable) && $data->data->removable==true)
                                                <div class="tsr-uboard-service-head-col-02">
                                                    <div class="composite">
                                                        <div class="cs-spr-10"><div class="tsr-uboard-service-price"></div></div>
                                                        <div class="cs-spl-10"><a href="{{$lang}}/dashboard/remove-service/{{$data->data->key}}" class="tsr-btn tsr-btn-xsmall tsr-btn-gray js-fancybox fancybox.ajax" data-fancybox-type="ajax" >{{$translate['deactivate']}}</a></div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>

                                    @if($data->data->key=='S_TRIPLE_ZERO')
                                        <!--  <div class="tsr-uboard-service-body">
                      <div class="tsr-uboard-service-body-pad">

                        <div class="tsr-uboard-service-body-desc">

                           <div class="cs-spb-10 cs-tx-bold">{{$translate['triple_zero_add_num']}}</div>

                            <div class="cs-spb-40 tsr-forms js-ph-editor">

                            <div id="numbers">
	                            @if(is_array($numbers) && count($numbers>0) )
                                            @foreach(@$numbers as $numb)
                                                <div class="cs-spb-10">
                                                  <div><span class="cs-tx-18">{{$numb}}</span><span class="cs-spl-20 cs-tx-bold cs-tx-13">

                                  <a href="{{$lang}}/remove-number-conf/{{$numb}}"  class="noline remove-number js-fancybox fancybox.ajax" data-fancybox-type="ajax" >{{$translate['remove']}}</a>

                                  </span></div>
	                              </div>
	                              @endforeach
                                        @endif
                                                </div>

                                                 @if( count($numbers)<3 )

                                            <div id="msg" style="padding:5px 0;"></div>
                                           <div class="cs-spb-10" style="display:none;" id="addNumber">
                                              <div>

                                                <div class="tsr-clearfix">
                                                  <form action="{{$lang}}/dashboard/add-number" method="POST" onsubmit="tripleZeroNumbers(this,'numbers')">
                                      <div class="cs-fl-left cs-spr-04" style="width: 120px;">
                                          <input type="text" value="" name="friend_number" data-inputmask="'mask': '\\599999999'" >
                                      </div>
                                      <div class="cs-fl-left">
                                          <input type="submit" value="{{$translate['add']}}" class="tsr-btn tsr-btn-form">
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <script src="site/tsr-core/tsr-scripts/jquery-inputmask/jquery.inputmask.min.js"></script>


                              <script type="text/javascript">/*<![CDATA[*/

                                var $box = $('#addNumber');


                                $('input[type="text"]', $box).inputmask();
                                  </script>
                              <div class="cs-spt-20">
                                <div><a href="javascript:$('#addNumber').slideToggle();"   class="composite"><span class="tsr-ico-btn cs-spr-04"><span class="ts-icon-add cs-tx-16 cs-pdx-04"></span></span><span class="cs-tx-13 cs-tx-bold">{{$translate['add_number']}}</span></a></div>
                              </div>
                              @endif
                                                </div>

                                            </div>

                                            <div class="tsr-uboard-service-body-close"><span class="tsr-uboard-service-collapse">{{$translate['collapse']}}</span></div>
                      </div>
                    </div>   -->
                                        @endif
                                    </div>

                                @endforeach
                            @endif





                                @if( isset($actServices[100]) )
                                    @foreach($actServices[100]['data'] as $data)
                                        <?php
                                        $icon = $data->service_type_group==4?$data->meti_file:$data->file;

                                        $src = ($data->file)?'uploads/products/thumbs/'.$icon:'site/tsr-temp/tsr-images/service-01.png';

                                        ?>
                                        <div class="tsr-uboard-service">
                                            <div class="tsr-uboard-service-modal"></div>
                                            <div class="tsr-uboard-service-head">
                                                <div class="tsr-uboard-service-head-col-01">
                                                    <div class="tsr-clearfix">
                                                        <div class="tsr-uboard-service-link tsr-clearfix">
                                                            <figure class="tsr-uboard-service-image">
                                                                <img alt="" src="{{$src}}">
                                                            </figure>
                                                            <div class="tsr-uboard-service-desc">
                                                                <div class="tsr-uboard-service-title">{{$data->description}}</div>

                                                                @if(isset($data->data->status) && $data->data->status==2)
                                                                    <div class="tsr-uboard-service-intro">{{$translate['susp_service']}}</div>
                                                                @elseif($data->daily)
                                                                    <div class="tsr-uboard-service-intro">{{$translate['validity_daily']}}</div>

                                                                @elseif (isset($data->data->expirationDate) && !$data->hideDate)
                                                                    <div class="tsr-uboard-service-intro">{{trans('site.validity')}} {{convertDate($data->data->expirationDate)}} </div>
                                                                @elseif($data->hideDate)
                                                                    <div class="tsr-uboard-service-intro">{{$translate['uvado']}}</div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                @if(isset($data->data->removable) && $data->data->removable==true)
                                                    <div class="tsr-uboard-service-head-col-02">
                                                        <div class="composite">
                                                            <div class="cs-spr-10"><div class="tsr-uboard-service-price"></div></div>
                                                            <div class="cs-spl-10"><a href="{{$lang}}/dashboard/remove-service/{{$data->data->key}}" class="tsr-btn tsr-btn-xsmall tsr-btn-gray js-fancybox fancybox.ajax" data-fancybox-type="ajax" >{{$translate['deactivate']}}</a></div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>

                                        @if($data->data->key=='S_TRIPLE_ZERO')
                                            <!--  <div class="tsr-uboard-service-body">
                      <div class="tsr-uboard-service-body-pad">

                        <div class="tsr-uboard-service-body-desc">

                           <div class="cs-spb-10 cs-tx-bold">{{$translate['triple_zero_add_num']}}</div>

                            <div class="cs-spb-40 tsr-forms js-ph-editor">

                            <div id="numbers">
	                            @if(is_array($numbers) && count($numbers>0) )
                                                @foreach(@$numbers as $numb)
                                                    <div class="cs-spb-10">
                                                      <div><span class="cs-tx-18">{{$numb}}</span><span class="cs-spl-20 cs-tx-bold cs-tx-13">

                                  <a href="{{$lang}}/remove-number-conf/{{$numb}}"  class="noline remove-number js-fancybox fancybox.ajax" data-fancybox-type="ajax" >{{$translate['remove']}}</a>

                                  </span></div>
	                              </div>
	                              @endforeach
                                            @endif
                                                    </div>

                                                     @if( count($numbers)<3 )

                                                <div id="msg" style="padding:5px 0;"></div>
                                               <div class="cs-spb-10" style="display:none;" id="addNumber">
                                                  <div>

                                                    <div class="tsr-clearfix">
                                                      <form action="{{$lang}}/dashboard/add-number" method="POST" onsubmit="tripleZeroNumbers(this,'numbers')">
                                      <div class="cs-fl-left cs-spr-04" style="width: 120px;">
                                          <input type="text" value="" name="friend_number" data-inputmask="'mask': '\\599999999'" >
                                      </div>
                                      <div class="cs-fl-left">
                                          <input type="submit" value="{{$translate['add']}}" class="tsr-btn tsr-btn-form">
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                              <script src="site/tsr-core/tsr-scripts/jquery-inputmask/jquery.inputmask.min.js"></script>


                              <script type="text/javascript">/*<![CDATA[*/

                                var $box = $('#addNumber');


                                $('input[type="text"]', $box).inputmask();
                                  </script>
                              <div class="cs-spt-20">
                                <div><a href="javascript:$('#addNumber').slideToggle();"   class="composite"><span class="tsr-ico-btn cs-spr-04"><span class="ts-icon-add cs-tx-16 cs-pdx-04"></span></span><span class="cs-tx-13 cs-tx-bold">{{$translate['add_number']}}</span></a></div>
                              </div>
                              @endif
                                                    </div>

                                                </div>

                                                <div class="tsr-uboard-service-body-close"><span class="tsr-uboard-service-collapse">{{$translate['collapse']}}</span></div>
                      </div>
                    </div>   -->
                                            @endif
                                        </div>

                                    @endforeach
                                @endif








                        </div>
                    </div>
                </div>
            </div>



            <div class="tsr-clear"></div>

            <!-- <div class="tsr-col-06 tsr-spacetop">
            <div class="tsr-dashboard-block-wrap">
              <div class="tsr-dashboard-block-wrap-head">
                <h3 class="tsr-title">{{$translate['my_orders']}}</h3>
              </div>
              <div class="tsr-dashboard-block-wrap-body">
                <div class="cs-spb-20">
                  <div><a href="#" class="cs-tx-bold noline">Apple IPhone 6 (Order #14761)</a></div>
                  <div class="cs-tx-13 cs-cl-gray"><span class="cs-tx-bold">Purchase date:</span> June 20, 2015</div>
                  <div class="cs-tx-13 cs-cl-gray"><span class="cs-tx-bold">Status:</span> Delivered <span class="ts-icon-circle cs-tx-11 cs-cl-turquoise"></span></div>
                </div>
                <div class="cs-spb-20">
                  <div><a href="#" class="cs-tx-bold noline">Apple IPhone 6 (Order #14761)</a></div>
                  <div class="cs-tx-13 cs-cl-gray"><span class="cs-tx-bold">Purchase date:</span> June 20, 2015</div>
                  <div class="cs-tx-13 cs-cl-gray"><span class="cs-tx-bold">Status:</span> In progress <span class="ts-icon-circle cs-tx-11 cs-cl-yellow"></div>
                </div>
                <div class="cs-spb-20">
                  <div><a href="#" class="cs-tx-bold noline">Apple IPhone 6 (Order #14761)</a></div>
                  <div class="cs-tx-13 cs-cl-gray"><span class="cs-tx-bold">Purchase date:</span> June 20, 2015</div>
                  <div class="cs-tx-13 cs-cl-gray"><span class="cs-tx-bold">Status:</span> Canceled <span class="ts-icon-circle cs-tx-11 cs-cl-red"></div>
                </div>
                <div class="cs-al-right"><a href="#" class="cs-tx-bold">{{$translate['view_full_history']}}</a></div>
              </div>
            </div>
          </div> -->


            @if(isset($installment->hasInstalment) && $installment->hasInstalment==true )

                <?php
                if(!is_array($installment->planedPayments)) {  $first_pay=$installment->planedPayments; }
                else {  $first_pay = $installment->planedPayments[0]; }
                ?>
                <div class="tsr-col-06 tsr-spacetop" style="height: 527px">
                    <div class="tsr-dashboard-block-wrap" style="height: 100%">
                        <div class="tsr-dashboard-block-wrap-head" >
                            <h3 class="tsr-title">{{$translate['installment']}}</h3>
                        </div>

                        <div class="tsr-dashboard-block-wrap-body">
                            {{--<div class="arr left_arr disabled" style="margin-top: 120px;" onclick="changeDateInstall(-1)" ></div>--}}
                            <div class="tsr-dashboard-instl" style="float: left; width: 80%; margin-top: 60px" >
                                <div class="tsr-dashboard-instl-date" id="changeDate" style="margin-top: 45px; width: 100%; text-align: center;margin: auto; font-size: 60px">{!! instalmentDate(isset($first_pay->date)?$first_pay->date:0, true) !!}</div>
                                <div class="cs-spb-20 cs-tx-17 cs-cl-pink" id="changeamount" style="line-height: 70px; font-size: 21px; ">
                                    {{$translate['you_should_pay']}} <span id="changeValue">{{$first_pay->amount}}</span> {{strtoupper($translate['gel'])}}
                                </div>
                                <div class="cs-tx-13"><a href="{{$lang}}/private/private-cabinet/installment" style="font-size: 16px">{{$translate['full_schedule']}}</a></div>

                                {{--<div class="cs-spb-04 cs-tx-17 cs-cl-pink">{{$translate['monthly_bonuses']}}</div>--}}
                                {{--<div class="cs-tx-13">{{$translate['un_local_mins']}}</div>--}}
                                {{--<div class="cs-tx-13">{{$translate['unl_sms']}}</div>--}}
                                {{--<div class="cs-tx-13">{{$translate['gb_internet']}}</div>--}}
                            </div>
                            {{--<div class="arr right_arr" style="margin-top: 120px;" onclick="changeDateInstall(1)"></div>--}}
                        </div>
                    </div>
                </div>
            @endif









            <div class="tsr-col-06 tsr-spacetop">
                <div class="tsr-dashboard-block-wrap">
                    <div class="tsr-dashboard-block-wrap-head">
                        <h3 class="tsr-title">{{$translate['additional_services']}}</h3>
                    </div>
                    <div class="tsr-dashboard-block-wrap-body">
                        @foreach($available_services as $serv)
                            @if($serv->service_type == 4)
                                <div class="cs-spb-20">
                                    <div class="tsr-dashboard-hlr">
                                        <div class="tsr-dashboard-hlr-col-01">
                                            <div class="cs-cl-primary cs-tx-bold">{{$serv->description}}</div>
                                        </div>
                                        <div class="tsr-dashboard-hlr-col-02">
                                            <div class="composite">
                                                <div class="cs-spr-10"><span class="cs-tx-13">@if($serv->price != 0) {{$serv->price}}&nbsp; ₾@if($serv->service_key != 'SO_CLIP' && $serv->service_key != 'S_CLIR' && $serv->service_key != 'S_CLIP')/{{$translate['month']}}@endif @endif</span></div>
                                                <div class="cs-spl-10">
                                                    <?php
                                                    $skuif = false;
                                                        if(!isset($actServices[4]['data'])){
                                                            $actServices[4]['data'] = array();
                                                        }
                                                    foreach ($actServices[4]['data'] as $myhlr){
                                                        if ($myhlr['service_key'] == $serv->service_key){
                                                            $skuif = true;
                                                        }
                                                    }
                                                    ?>

                                                    <div class="tsr-com-switch @if($skuif) {{'is-choosen'}} @if($serv->removable == false){{'noremovebale'}} @endif @endif" onclick="changeStatus('{{$serv->service_key}}', this, '{{$serv->price}}', '{{$serv->description}}')"></div>

                                                </div>
                                                <div class="cs-spl-10" style="width: 30px;">
                                                    <div class="floatingBarsG" id="{{$serv->service_key}}">
                                                        <div class="blockG" id="rotateG_01"></div>
                                                        <div class="blockG" id="rotateG_02"></div>
                                                        <div class="blockG" id="rotateG_03"></div>
                                                        <div class="blockG" id="rotateG_04"></div>
                                                        <div class="blockG" id="rotateG_05"></div>
                                                        <div class="blockG" id="rotateG_06"></div>
                                                        <div class="blockG" id="rotateG_07"></div>
                                                        <div class="blockG" id="rotateG_08"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach

                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
<!-- /tsr-section-uboard-dashboard -->

<!-- installment notification -->



<script>
            <?php
                if (isset($installment->planedPayments)){
                    foreach ($installment->planedPayments as $key => $value){
                        $installment->planedPayments[$key]->date = instalmentDate(isset($value->date)?$value->date:0, true);
                    }
                }
            ?>
            var installPointer = 0;
    var planedPayments = JSON.parse('<?= isset($installment->planedPayments)?json_encode($installment->planedPayments):"" ?>');
</script>
<script type="text/javascript">/*<![CDATA[*/
    var lastobj = '';
    var allochange = true;
    $(document).ready(function() {

        /* tooltips */
        (function() {
            $('.js-tooltip-text')
                    .css({'cursor': 'pointer'})
                    .tooltipster({
                        'maxWidth': 260,
                        'position': 'top'
                    });
        })();

        /* switches demo */


        /* avatar */



        /* promo */


    });
    /*]]>*/

    function changeStatus(sku, obj, price, title){
        if (lastobj == obj && allochange == false){
            return;
        }
        lastobj = obj;
        allochange = false;
        if ($(lastobj).hasClass( "is-choosen" )){
            var active = true;
            if ($(lastobj).hasClass( "noremovebale" )){
                return;
            }
            var url = 'hlroff/'+sku;
        } else {
            var active = false;
            var url = 'hlron/'+sku;
        }
        price = parseFloat(price);
        if (price > 0 && active == false){
            changeStatusPrice(url, sku, title, price);
            return;
        }

        $('#'+sku).fadeIn();
        sendReq(url, sku, price);
    }

    function sendReq(url, sku, price){
             $.ajax({
                    method: "get",
                    url: url
             })
            .done(function( msg ) {
                $('#'+sku).fadeOut();
                setTimeout(function(){
                    allochange = true;
                }, 5000);
                if (msg == 'ok'){
                    $(lastobj).toggleClass("is-choosen");
                } else {
                    if (msg == 'ბალანსი არ არის საკმარისი'){
                        var cont = '<div class="fancybox-overlay fancybox-overlay-fixed flaxcustbox" style="display: block; width: auto; height: auto;" id="flaxcustbox"></div>\
                        <div class="fancybox-wrap fancybox-desktop fancybox-type-ajax fancybox-opened" tabindex="-1" style="width: 720px; height: auto; position: fixed; top: calc(50% - 300px); left: calc(50% - 310px); opacity: 1; overflow: visible;"><div class="fancybox-skin" style="padding: 0px; width: auto; height: auto;"><div class="fancybox-outer"><div class="fancybox-inner" style="overflow: visible; width: 720px; height: auto;"><div class="tsr-lightbox-layout-a" id="unique-lightbox-id">\
                                <div class="tsr-com-collapser-outer">\
                                <div class="tsr-com-collapser-inner">\
                                <div class="cs-al-center">\
                                <div class="cs-spb-20"><span class="ts-icon-alert cs-tx-32 cs-cl-orange"></span></div>\
                        <div class="cs-spb-20">\
                                <div class="cs-spb-04 cs-tx-bold">თქვენი ბალანსია:</div>\
                        <div class="cs-spb-00 cs-tx-24">19.73&nbsp;<img src="site/tsr-core/tsr-images/tsr-currency-gel.png" height="20" alt="gel" class="cs-al-baseline"></div>\
                                </div>\
                                <div class="cs-spb-20 cs-spt-30">\
                                <div class="cs-spb-04">თქვენ არ გაქვთ საკმარისი თანხა სერვისის გასააქტიურებლად. გთხოვთ შეავსოთ ბალანსი.</div>\
                        <div class="cs-spb-04">რეკომენდებული თანხა: '+price+' &nbsp;<img src="site/tsr-core/tsr-images/tsr-currency-gel.png" height="14" alt="gel" class="cs-al-baseline"></div>\
                                </div>\
                                <div class="cs-spb-20 cs-spt-30">\
                                <div class="cs-spb-04"><a href="https://sb3d.georgiancard.ge/payment/start.wsm?lang=ka&amp;o.phone={{\Session::get('phone')}}&amp;o.amount='+(price*100)+'&amp;page_id=B7F3795AAEA66B1271E323C1AF4A0A1C&amp;merch_id=805F85F4482A04D0B103A4CE83EFFB73&amp;back_url_s=&amp;back_url_f=" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise">ბალანსის შევსება</a></div>\
                        <div class="cs-spb-04"><a href="#" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel">გაუქმება</a></div>\
                                </div>\
                                </div>\
                                </div>\
                                </div>\
                                </div>\
</div></div><a class="fancybox-item fancybox-close" onclick="$(\'.fancybox-wrap\').fadeOut(); $(\'.fancybox-overlay\').fadeOut(function(){ $(\'.flaxcustbox\').remove(); })"></a></div></div>';
                    } else {
                        var cont = '<div class="fancybox-overlay fancybox-overlay-fixed flaxcustbox" style="display: block; width: auto; height: auto;" id="flaxcustbox"></div>\
                                    <div class="fancybox-wrap fancybox-desktop fancybox-type-ajax fancybox-opened flaxcustbox" tabindex="-1" style="width: 720px; height: auto; position: fixed; top: calc(50% - 300px); left: calc(50% - 310px); opacity: 1; overflow: visible;"><div class="fancybox-skin" style="padding: 0px; width: auto; height: auto;"><div class="fancybox-outer"><div class="fancybox-inner" style="overflow: visible; width: 720px; height: auto;"><div class="tsr-lightbox-layout-a" id="remove-service-lightbox">\
                                        <div class="tsr-com-collapser-outer">\
                                            <div class="tsr-com-collapser-inner">\
                                                <div class="cs-al-center">\
                                                    <div class="cs-spb-20 cs-tx-bold">\
                                                         '+msg+'\
                                                     </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>\
                                 </div></div><a class="fancybox-item fancybox-close" onclick="$(\'.fancybox-wrap\').fadeOut(); $(\'.fancybox-overlay\').fadeOut(function(){ $(\'.flaxcustbox\').remove(); })"></a></div></div>';
                    }

                    $('body').append(cont);
                }
            });
    }


    function changeStatusPrice(url, sku, title, price){
        var cont = '<div class="fancybox-overlay fancybox-overlay-fixed flaxcustbox" style="display: block; width: auto; height: auto;" ></div>\
                        <div class="fancybox-wrap fancybox-desktop fancybox-type-ajax fancybox-opened flaxcustbox" tabindex="-1" style="width: 720px; height: auto; position: fixed; top: calc(50% - 120px); left: calc(50% - 360px); opacity: 1; overflow: visible;"><div class="fancybox-skin" style="padding: 0px; width: auto; height: auto;"><div class="fancybox-outer"><div class="fancybox-inner" style="overflow: visible; width: 720px; height: auto;"><div class="tsr-lightbox-layout-a" id="remove-service-lightbox">\
                            <div class="tsr-com-collapser-outer">\
                                <div class="tsr-com-collapser-inner">\
                                    <div class="cs-al-center">\
                                        <div class="cs-spb-20 cs-tx-bold">\
                                        @if($lang=='en')
                                            You\'re activating '+title+'. Price '+price+' GEL.\
                                        @else
                                            თქვენ გაგიაქტიურდებათ '+title+'. ტარიფის ფასი '+price+' ლარი.\
                                        @endif
                                     </div>\
                                     <div class="cs-spb-04">\
                                         <div class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise" onclick="$(\'.fancybox-wrap\').fadeOut(); $(\'.fancybox-overlay\').fadeOut(function(){ $(\'.flaxcustbox\').remove(); }); sendReq(\''+url+'\', \''+sku+'\', '+price+')">დადასტურება</div>\
                                     </div>\
                                     <div class="cs-spb-04">\
                                        <div class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-gray js-lightbox-cancel" onclick="$(\'.fancybox-wrap\').fadeOut(); $(\'.fancybox-overlay\').fadeOut(function(){ $(\'.flaxcustbox\').remove(); }); $(\'#'+sku+'\').fadeOut();">გაუქმება</div>\
                                     </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                     </div></div><a class="fancybox-item fancybox-close" onclick="$(\'.fancybox-wrap\').fadeOut(); $(\'.fancybox-overlay\').fadeOut(function(){ $(\'.flaxcustbox\').remove(); });  $(\'#'+sku+'\').fadeOut();"></a></div></div>';
        $('body').append(cont);
    }
    function changeDateInstall(action){
        if ( installPointer + parseInt(action) < 0 || installPointer + parseInt(action) >= planedPayments.length -1){
            return;
        }
        installPointer += parseInt(action);
        $('#changeDate').fadeOut(function(){
            $('#changeDate').html(planedPayments[installPointer].date);
        });
        $('#changeamount').fadeOut(function(){
            $('#changeValue').html(planedPayments[installPointer].amount);
        });
        $('#changeDate').fadeIn();
        $('#changeamount').fadeIn();

        if (installPointer + parseInt(action) >= planedPayments.length -1){
            $('.right_arr').addClass('disabled');
        } else {
            $('.right_arr').removeClass('disabled');
        }
        if (installPointer + parseInt(action) < 0){
            $('.left_arr').addClass('disabled');
        } else {
            $('.left_arr').removeClass('disabled');
        }
    }
</script>

<style>
    div.is-choosen.noremovebale{
        opacity: 0.6;
    }
    .floatingBarsG{
        position:relative;
        width:15px;
        height:19px;
        margin:auto;
        display: none;
    }

    .blockG{
        position:absolute;
        background-color:rgb(255,255,255);
        width:2px;
        height:6px;
        border-radius:2px 2px 0 0;
        -o-border-radius:2px 2px 0 0;
        -ms-border-radius:2px 2px 0 0;
        -webkit-border-radius:2px 2px 0 0;
        -moz-border-radius:2px 2px 0 0;
        transform:scale(0.4);
        -o-transform:scale(0.4);
        -ms-transform:scale(0.4);
        -webkit-transform:scale(0.4);
        -moz-transform:scale(0.4);
        animation-name:fadeG;
        -o-animation-name:fadeG;
        -ms-animation-name:fadeG;
        -webkit-animation-name:fadeG;
        -moz-animation-name:fadeG;
        animation-duration:1.2s;
        -o-animation-duration:1.2s;
        -ms-animation-duration:1.2s;
        -webkit-animation-duration:1.2s;
        -moz-animation-duration:1.2s;
        animation-iteration-count:infinite;
        -o-animation-iteration-count:infinite;
        -ms-animation-iteration-count:infinite;
        -webkit-animation-iteration-count:infinite;
        -moz-animation-iteration-count:infinite;
        animation-direction:normal;
        -o-animation-direction:normal;
        -ms-animation-direction:normal;
        -webkit-animation-direction:normal;
        -moz-animation-direction:normal;
    }
    .arr{
        width: 10%;
        height: 100%;
        background: red;
        margin-top: 120px!important;
    }
    .left_arr{
        float: left;
        background: #000;
    }
    .right_arr{
        float: right;
        background: #000;
    }

    #rotateG_01{
        left:0;
        top:7px;
        animation-delay:0.45s;
        -o-animation-delay:0.45s;
        -ms-animation-delay:0.45s;
        -webkit-animation-delay:0.45s;
        -moz-animation-delay:0.45s;
        transform:rotate(-90deg);
        -o-transform:rotate(-90deg);
        -ms-transform:rotate(-90deg);
        -webkit-transform:rotate(-90deg);
        -moz-transform:rotate(-90deg);
    }

    #rotateG_02{
        left:2px;
        top:2px;
        animation-delay:0.6s;
        -o-animation-delay:0.6s;
        -ms-animation-delay:0.6s;
        -webkit-animation-delay:0.6s;
        -moz-animation-delay:0.6s;
        transform:rotate(-45deg);
        -o-transform:rotate(-45deg);
        -ms-transform:rotate(-45deg);
        -webkit-transform:rotate(-45deg);
        -moz-transform:rotate(-45deg);
    }

    #rotateG_03{
        left:6px;
        top:1px;
        animation-delay:0.75s;
        -o-animation-delay:0.75s;
        -ms-animation-delay:0.75s;
        -webkit-animation-delay:0.75s;
        -moz-animation-delay:0.75s;
        transform:rotate(0deg);
        -o-transform:rotate(0deg);
        -ms-transform:rotate(0deg);
        -webkit-transform:rotate(0deg);
        -moz-transform:rotate(0deg);
    }

    #rotateG_04{
        right:2px;
        top:2px;
        animation-delay:0.9s;
        -o-animation-delay:0.9s;
        -ms-animation-delay:0.9s;
        -webkit-animation-delay:0.9s;
        -moz-animation-delay:0.9s;
        transform:rotate(45deg);
        -o-transform:rotate(45deg);
        -ms-transform:rotate(45deg);
        -webkit-transform:rotate(45deg);
        -moz-transform:rotate(45deg);
    }

    #rotateG_05{
        right:0;
        top:7px;
        animation-delay:1.05s;
        -o-animation-delay:1.05s;
        -ms-animation-delay:1.05s;
        -webkit-animation-delay:1.05s;
        -moz-animation-delay:1.05s;
        transform:rotate(90deg);
        -o-transform:rotate(90deg);
        -ms-transform:rotate(90deg);
        -webkit-transform:rotate(90deg);
        -moz-transform:rotate(90deg);
    }

    #rotateG_06{
        right:2px;
        bottom:2px;
        animation-delay:1.2s;
        -o-animation-delay:1.2s;
        -ms-animation-delay:1.2s;
        -webkit-animation-delay:1.2s;
        -moz-animation-delay:1.2s;
        transform:rotate(135deg);
        -o-transform:rotate(135deg);
        -ms-transform:rotate(135deg);
        -webkit-transform:rotate(135deg);
        -moz-transform:rotate(135deg);
    }

    #rotateG_07{
        bottom:0;
        left:6px;
        animation-delay:1.35s;
        -o-animation-delay:1.35s;
        -ms-animation-delay:1.35s;
        -webkit-animation-delay:1.35s;
        -moz-animation-delay:1.35s;
        transform:rotate(180deg);
        -o-transform:rotate(180deg);
        -ms-transform:rotate(180deg);
        -webkit-transform:rotate(180deg);
        -moz-transform:rotate(180deg);
    }

    #rotateG_08{
        left:2px;
        bottom:2px;
        animation-delay:1.5s;
        -o-animation-delay:1.5s;
        -ms-animation-delay:1.5s;
        -webkit-animation-delay:1.5s;
        -moz-animation-delay:1.5s;
        transform:rotate(-135deg);
        -o-transform:rotate(-135deg);
        -ms-transform:rotate(-135deg);
        -webkit-transform:rotate(-135deg);
        -moz-transform:rotate(-135deg);
    }



    @keyframes fadeG{
        0%{
            background-color:rgb(0,0,0);
        }

        100%{
            background-color:rgb(255,255,255);
        }
    }

    @-o-keyframes fadeG{
        0%{
            background-color:rgb(0,0,0);
        }

        100%{
            background-color:rgb(255,255,255);
        }
    }

    @-ms-keyframes fadeG{
    0%{
        background-color:rgb(0,0,0);
    }

    100%{
        background-color:rgb(255,255,255);
    }
    }

    @-webkit-keyframes fadeG{
        0%{
            background-color:rgb(0,0,0);
        }

        100%{
            background-color:rgb(255,255,255);
        }
    }

    @-moz-keyframes fadeG{
        0%{
            background-color:rgb(0,0,0);
        }

        100%{
            background-color:rgb(255,255,255);
        }
    }
</style>
@endsection