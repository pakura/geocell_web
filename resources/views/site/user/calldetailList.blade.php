@extends('site')

@section('content')
@include('site.user.usercabinet-head')
<?php

function unitCorrect($unit){
    global $lang;
    if(strpos($unit, 'INTERNET_BYTE') > -1){

        $unit = floatval($unit);
        $unit = $unit / 1024;
//            return intval($unit);
        if($unit < 1024){
            if($lang == 'en')
                return intval($unit).' kb';
            else
                return intval($unit).' კბ';

        } else {
            $unit = $unit / 1024;
            if($lang == 'en')
                return intval($unit).' mb';
            else
                return intval($unit).' მბ';
        }
    } elseif (strpos($unit, 'TIME_SECONDS') > -1){
        $unit = floatval($unit);
        if($unit < 60){
            if($lang == 'en')
                return intval($unit).' second';
            else
                return intval($unit).' წამი';
        } else {
            $min = intval($unit / 60);
            $sec = intval($unit % 60);
            if($lang == 'en')
                return $min.' minutes and '.$sec.' seconds';
            else
                return $min.' წუთი და '.$sec.' წამი';
        }
    } elseif(strpos($unit, 'SMS') > -1) {
        if($lang == 'en')
            return intval($unit).' SMS';
        else
            return intval($unit).' ფაქტი';
    } else {
        return '';
    }
}

?>
        <!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-empty"></div>
<!-- /tsr-section-divider -->

<!-- tsr-section-generic -->
<div class="tsr-section-generic">
    <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">

            <h1 class="tsr-title cs-spb-10">{{$item->title}}</h1>
            @if(!isset($details_req))
                {!! $item->content !!}

            @endif
            <br>
            <br>
            @if($lang == 'en')
                <h3 style="font-size: 14px">Please be informed: in column "Unit value", call data is provided in hh/mm/ss format and internet consumption is provided in kilobytes.</h3>
            @else
                <h3 style="font-size: 14px">გთხოვთ გაითვალისწინოთ: ველში "ერთეულის რაოდენობა", სასაუბრო დროის მაჩვენებლები გამოსახულია სთ/წთ/წმ ფორმატში, ინტერნეტის მოხმარება გამოსახულია კილობაიტებში.</h3>
            @endif
            <br>
            <br>

            <div class="cs-spb-10 tsr-clearfix">
                <div class="cs-fl-left cs-spt-20 cs-spr-10">
                    <div class="tsr-com-linklist">
                        <ul class="cs-tx-13 cs-tx-bold">
                            @if(isset($details_req))
                                <li>{{$translate['shown']}}
                                    @if($lang == 'en')
                                        from {{$details_req->fromdate}} to {{$details_req->todate}}
                                    @else
                                        {{$details_req->fromdate}} დან {{$details_req->todate}} ჩათვლით
                                    @endif
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
                <div class="cs-fl-right cs-spt-20 cs-spl-10 cs-tx-13 cs-tx-bold" >
                    <span style="margin-right: 10px">
                    @if($lang == 'en')
                        For the full information download the file
                    @else
                        სრული ინფორმაციის სანახავად, ჩამოტვირთე ფაილი
                    @endif
                    </span>
                    @if(isset($details))
                        <div id="circularG" style="float: left; display: none">
                            <div id="circularG_1" class="circularG"></div>
                            <div id="circularG_2" class="circularG"></div>
                            <div id="circularG_3" class="circularG"></div>
                            <div id="circularG_4" class="circularG"></div>
                            <div id="circularG_5" class="circularG"></div>
                            <div id="circularG_6" class="circularG"></div>
                            <div id="circularG_7" class="circularG"></div>
                            <div id="circularG_8" class="circularG"></div>
                        </div>
                        <div class="tsr-com-lineblock" style="float: right">
                            <a class="composite" id="cmd" href="{{$lang}}/download-as-pdf" download onclick="$('#circularG').fadeIn();
                                setTimeout(function(){
                                    $('#circularG').fadeOut();
                                    $('#cmd').css({'opacity': '1'});
                                },7000);
                                $(this).css({'opacity': '0.4'});
                            ">
                                <span class="ts-icon-download cs-tx-18 cs-spr-04"></span>
                                <span class="cs-tx-13 cs-tx-bold">{{$translate['download_pdf']}}</span>
                            </a>
                        </div>
                    @endif
                </div>
            </div>


@if(isset($details))
            <div class="tsr-scrollpane" style="position: relative; overflow: auto"><div class="tsr-scrollpane" style="height: auto; margin-bottom: 0px; margin-right: 0px; max-height: 353px;">
                    <div class="t" id="detailstable">
                        <table class="tsr-com-table-standard" style="width: 100%">
                            <thead>
                                <tr style="font-weight: bold; font-family: Arial, Helvetica, sans-serif">
                                    @if($lang == 'en')
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">Date/time</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">Service Type</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">Price</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">Number</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">Unit value</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">Direction</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">Description</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">Tariff</th>
                                    @else
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">დღე/დრო</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">სერვისის ტიპი</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">ფასი</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">ნომერი</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">ერთეულის რაოდენობა</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">მიმართულება</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">აღწერა</th>
                                        <th style="vertical-align: top;border: 1px solid #dbdbdb;padding: 10px 15px;">ტარიფი</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($details as $key => $detail)
                                <tr>
                                    <td>{{$detail->call_date}}</td>
                                    <td>{{isset($detail->type)?$detail->type:''}}</td>
                                    <td>{{isset($detail->price)?number_format($detail->price, 2, '.', '').'₾':'0.00 ₾'}}</td>
                                    <td>
                                    @if($resUserInfo->brand == 1)
                                        {{isset($detail->b_number)?substr($detail->b_number,-9):''}}
                                    @else
                                        {{isset($detail->called_phone)?substr($detail->called_phone,-9):''}}
                                    @endif
                                    </td>
                                    <td>{{isset($detail->unitValue)?unitCorrect($detail->unitValue):''}}</td>
                                    <td>{{isset($detail->zone_name)?$detail->zone_name:''}}</td>
                                    <td>{{isset($detail->description)?$detail->description:''}}</td>
                                    <td>{{isset($detail->tarrifInformation)?$detail->tarrifInformation:''}}</td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                        @if(!isset($details[0]))
                           <table style="width: 100%"> <tr><td style="text-align: center;
    padding: 26px;"> @if($lang == 'en') No data available @else მოთხოვნილ თარიღში ჩანაწერი არ ფიქსირდება @endif </td></tr></table>
                        @endif
                    </div>
                </div>
             </div>
@endif
    </div>
</div>
<div id="editor"></div>
<!-- /tsr-section-generic -->

<!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-empty"></div>
<!-- /tsr-section-divider -->

<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="site/tsr-core/tsr-scripts/jquery-dtpicker/jquery.dtpicker.patched.min.js"></script>
<script src="site/js/jspdf.min.js"></script>
<script type="text/javascript">/*<![CDATA[*/
    var update;
    $(document).ready(function() {

        $('#table_0').DataTable();
        /* tooltips */


        /* details */
        (function() {
            $(window).scrollTop($('.tsr-section-refiner').offset().top);
            setTimeout(function () {
                $(window).scrollTop($('.tsr-section-refiner').offset().top);

            }, 500);
            var $blocks = $('.js-local-details');
            $blocks.each(function() {
                var $block     = $(this);
                var $radio     = $('.js-local-details-switcher', this);
                var $locker    = $('.js-local-details-locker',   this);
//            var $container = $('.js-local-details-content',  this);
                $block.on('tsr-details-on', function() {
                    $blocks.trigger('tsr-details-off');
                    $radio.prop('checked', true).trigger('cc-update');
                    $locker.removeClass('is-hidden');
//              $container.show();
                });
                $block.on('tsr-details-off', function() {
                    $radio.prop('checked', false).trigger('cc-update');
                    $locker.addClass('is-hidden');
//              $container.hide();
                });
                $radio.on('click', function() {
                    $block.trigger('tsr-details-on');
                });
            }).trigger('tsr-details-off');
        })();

        /* date-picker */
        (function() {
            $('.js-date-picker').appendDtpicker({
                'locale': $('html').attr('lang'),
                'dateFormat': 'YYYY-MM-DD',
                'dateOnly': true,
                'disableInit': true,
                'animation': false,
                'todayButton': false,
                'firstDayOfWeek': 1
            });
        })();



    });



    // All units are in the set measurement for the document
    // This can be changed to "pt" (points), "mm" (Default), "cm", "in"

    function downloadPDF() {
        var pdf = new jsPDF('p', 'pt', 'letter');
        // source can be HTML-formatted string, or a reference
        // to an actual DOM element from which the text will be scraped.
        source = $('#detailstable')[0];
        var t=$('#detailstable').clone();
        var t2=$("<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'/></head><body></body></html>").find("body").append(t);
        source=t2[0];
        console.log(sources);

        // we support special element handlers. Register them with jQuery-style
        // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
        // There is no support for any other type of selectors
        // (class, of compound) at this time.
        specialElementHandlers = {
            // element with id of "bypass" - jQuery style selector
            '#bypassme': function (element, renderer) {
                // true = "handled elsewhere, bypass text extraction"
                return true
            }
        };
        margins = {
            top: 80,
            bottom: 40,
            left: 80,
            width: 600
        };
        // all coords and widths are in jsPDF instance's declared units
        // 'inches' in this case
        pdf.fromHTML(
                source, // HTML string or DOM elem ref.
                margins.left, // x coord
                margins.top, { // y coord
                    'width': margins.width, // max width of content on PDF
                    'elementHandlers': specialElementHandlers
                },

                function (dispose) {
                    // dispose: object with X, Y of the last line add to the PDF
                    //          this allow the insertion of new lines after html
                    pdf.save('Call Details.pdf');
                }, margins);
    }



    /*]]>*/</script>

<style>
    #circularG{
        position:relative;
        width:25px;
        height:25px;
        margin: auto;
    }

    .circularG{
        position:absolute;
        background-color:rgb(0,0,0);
        width:6px;
        height:6px;
        border-radius:4px;
        -o-border-radius:4px;
        -ms-border-radius:4px;
        -webkit-border-radius:4px;
        -moz-border-radius:4px;
        animation-name:bounce_circularG;
        -o-animation-name:bounce_circularG;
        -ms-animation-name:bounce_circularG;
        -webkit-animation-name:bounce_circularG;
        -moz-animation-name:bounce_circularG;
        animation-duration:1.1s;
        -o-animation-duration:1.1s;
        -ms-animation-duration:1.1s;
        -webkit-animation-duration:1.1s;
        -moz-animation-duration:1.1s;
        animation-iteration-count:infinite;
        -o-animation-iteration-count:infinite;
        -ms-animation-iteration-count:infinite;
        -webkit-animation-iteration-count:infinite;
        -moz-animation-iteration-count:infinite;
        animation-direction:normal;
        -o-animation-direction:normal;
        -ms-animation-direction:normal;
        -webkit-animation-direction:normal;
        -moz-animation-direction:normal;
    }

    #circularG_1{
        left:0;
        top:10px;
        animation-delay:0.41s;
        -o-animation-delay:0.41s;
        -ms-animation-delay:0.41s;
        -webkit-animation-delay:0.41s;
        -moz-animation-delay:0.41s;
    }

    #circularG_2{
        left:3px;
        top:3px;
        animation-delay:0.55s;
        -o-animation-delay:0.55s;
        -ms-animation-delay:0.55s;
        -webkit-animation-delay:0.55s;
        -moz-animation-delay:0.55s;
    }

    #circularG_3{
        top:0;
        left:10px;
        animation-delay:0.69s;
        -o-animation-delay:0.69s;
        -ms-animation-delay:0.69s;
        -webkit-animation-delay:0.69s;
        -moz-animation-delay:0.69s;
    }

    #circularG_4{
        right:3px;
        top:3px;
        animation-delay:0.83s;
        -o-animation-delay:0.83s;
        -ms-animation-delay:0.83s;
        -webkit-animation-delay:0.83s;
        -moz-animation-delay:0.83s;
    }

    #circularG_5{
        right:0;
        top:10px;
        animation-delay:0.97s;
        -o-animation-delay:0.97s;
        -ms-animation-delay:0.97s;
        -webkit-animation-delay:0.97s;
        -moz-animation-delay:0.97s;
    }

    #circularG_6{
        right:3px;
        bottom:3px;
        animation-delay:1.1s;
        -o-animation-delay:1.1s;
        -ms-animation-delay:1.1s;
        -webkit-animation-delay:1.1s;
        -moz-animation-delay:1.1s;
    }

    #circularG_7{
        left:10px;
        bottom:0;
        animation-delay:1.24s;
        -o-animation-delay:1.24s;
        -ms-animation-delay:1.24s;
        -webkit-animation-delay:1.24s;
        -moz-animation-delay:1.24s;
    }

    #circularG_8{
        left:3px;
        bottom:3px;
        animation-delay:1.38s;
        -o-animation-delay:1.38s;
        -ms-animation-delay:1.38s;
        -webkit-animation-delay:1.38s;
        -moz-animation-delay:1.38s;
    }



    @keyframes bounce_circularG{
        0%{
            transform:scale(1);
        }

        100%{
            transform:scale(.3);
        }
    }

    @-o-keyframes bounce_circularG{
        0%{
            -o-transform:scale(1);
        }

        100%{
            -o-transform:scale(.3);
        }
    }

    @-ms-keyframes bounce_circularG{
    0%{
        -ms-transform:scale(1);
    }

    100%{
        -ms-transform:scale(.3);
    }
    }

    @-webkit-keyframes bounce_circularG{
        0%{
            -webkit-transform:scale(1);
        }

        100%{
            -webkit-transform:scale(.3);
        }
    }

    @-moz-keyframes bounce_circularG{
        0%{
            -moz-transform:scale(1);
        }

        100%{
            -moz-transform:scale(.3);
        }
    }
</style>
@endsection