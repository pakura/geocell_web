@extends('site')

@section('content')
     @include('site.user.usercabinet-head')
    

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">

          <h1 class="tsr-title cs-spb-10">{{$item->title}}</h1>

          

          @if(isset($installment->hasInstalment) && $installment->hasInstalment==true)
          <div class="tsr-col-06 tsr-spacetop">
            <div class="tsr-dashboard-block-wrap">
        
              <div class="tsr-dashboard-block-wrap-body">
                
              @foreach($installment->planedPayments as $planned)
                <div class="tsr-dashboard-instl" style="text-align:left">
                  <div class=" cs-tx-30" >{{instalmentDate(isset($planned->date)?$planned->date:0 )}}</div>
                  <div class="cs-spb-20 cs-tx-17 cs-cl-pink">
                      
                      {{$translate['you_should_pay'].' '.$planned->amount.' '.$translate['gel']}} 
                  </div>
                  <!-- <div><a href="#" class="cs-tx-bold">Full shedule</a></div> -->
                  <hr class="tsr-dashboard-instl-hr" style="margin:15px 0"/>
                 
                </div>
                @endforeach


              </div>
            </div>
          </div>

          <div class="tsr-clear"></div>

          @if(isset($installment->previousPayments))
          <div class="tsr-col-06 tsr-spacetop">
            <div class="tsr-dashboard-block-wrap">
              
              <div class="tsr-dashboard-block-wrap-head">
                <h3 class="tsr-title">{{$translate['payment_history']}}</h3>
              </div>

              <div class="tsr-dashboard-block-wrap-body">
                
              @foreach($installment->previousPayments as $prev)
                <div class="tsr-dashboard-instl" style="text-align:left">
                  <div class=" cs-tx-30" >{{instalmentDate(isset($prev->date)?$prev->date:0 )}}</div>
                  <div class="cs-spb-20 cs-tx-17 cs-cl-green-t">
                      
                      {{$translate['installment_payed'].' '.$prev->amount.' '.$translate['gel']}} 
                  </div>
                  <!-- <div><a href="#" class="cs-tx-bold">Full shedule</a></div> -->
                  <hr class="tsr-dashboard-instl-hr" style="margin:15px 0"/>
                 
                </div>
                @endforeach


              </div>
            </div>
          </div>
          @endif



          @endif

        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->


@endsection