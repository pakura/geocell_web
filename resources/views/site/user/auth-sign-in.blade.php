@extends('site')

@section('content')


    <div class="tsr-lightbox-layout-a unique-lightbox-id" id="unique-lightbox-id-1" style="margin-top:-20px">
        <div class="tsr-com-collapser-outer">
            <div class="tsr-com-collapser-inner">
                <div class="cs-al-center">
                    <div class="cs-spb-20 cs-tx-40 cs-tx-bold">{{$translate['signin']}}</div>
                    <div class="cs-spb-20">{{$translate['sign_in_text']}}  </div>
                    <div class="cs-spb-20 cs-spt-30 cs-mg-center" style="width: 260px;">
                        <div class="cs-spb-04 cs-spt-30">
                            <form action="{{$lang}}/alternumber" method="post" class="tsr-forms" onsubmit="return alterNumber(this)"  method="post">
                                <div class="tsr-lightbox-layout-a-form-pad">
                                    <div class="cs-spb-04"><label class="cc-label">{{$translate['cell_number']}}</label></div>
                                    <div class="cs-spb-04 tsr-lightbox-layout-a-field-combo">
                                        <div class="tsr-lightbox-layout-a-field-a"><input type="text" name="phone" class="cs-al-center"  maxlength="9"/></div>
                                        <div class="tsr-lightbox-layout-a-field-b" style="visibility: hidden;"><img src="site/tsr-core/tsr-images/tsr-spi-loader-24.gif" alt=""></div>
                                    </div>
                                    <div class="cs-spb-10 tsr-lightbox-layout-a-field msisdn-error" style="display: none;"><div class="cc-errorbox">{{$translate['wrong_msisdn']}}</div></div>
                                </div>

                                <div class="cs-spb-20"><input type="submit" class="tsr-btn-xsmall  tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise" value="{{$translate['next']}}"/></div>

                            </form>
                        </div>

                        <!--  <div class="cs-spb-20 cs-spt-30"><a href="#">Registration</a></div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="site/tsr-core/tsr-scripts/jquery-inputmask/jquery.inputmask.min.js"></script>
    <script type="text/javascript">/*<![CDATA[*/
        var $box = $('#unique-lightbox-id-1');

        // $('input[type="text"]', $box).inputmask();
        $('input[type="text"]', $box).keypress(function (e) {
            if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) return false;
        });
        $(document).on("click", "a.js-fancybox", function(){
            event.preventDefault();
            event.stopPropagation();
        });
        /*]]>*/

    </script>

@endsection
