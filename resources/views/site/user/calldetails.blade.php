<?php
$monthka = array('01' => 'იანვარი', '02' => 'თებერვალი', '03' => 'მარტი',
        '04' => 'აპრილი', '05' => 'მაისი', '06' => 'ივნისი',
        '07' => 'ივლისი', '08' => 'აგვისტო', '09' => 'სექტემბერი',
        '10' => 'ოქტომბერი', '11' => 'ნოემბერი', '12' => 'დეკემბერი');
$monthen = array('01' => 'January', '02' => 'February', '03' => 'March',
        '04' => 'April', '05' => 'May', '06' => 'June',
        '07' => 'July', '08' => 'August', '09' => 'September',
        '10' => 'October', '11' => 'November', '12' => 'December');

?>
@extends('site')

@section('content')
     @include('site.user.usercabinet-head')
     <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">

          <h1 class="tsr-title cs-spb-10">@if($lang == 'en') Call details / Invoices @else დეტალური ამონაწერი / ინვოისი @endif</h1>

          <div class="cs-spt-20 cs-spb-40 tsr-forms js-local-details">
              @if($userinfo->return->brand == 1)
                  <div class="cs-pdb-10"><label class="cc-label-cbox"><input type="radio" name="local-details" class="js-local-details-switcher js-local-details-default"><span class="cs-spl-04 cs-tx-18">@if($lang == 'en') Call detail @else დეტალური ამონაწერი @endif</span></label></div>
              @endif
            <div class="tsr-com-lockbox js-local-details-locker">
              <div class="tsr-com-lockbox-layer tsr-semiwhite"></div>
              <div class="cs-pdb-10 cs-cl-blue">
                  {{$translate['price_for_1_day']}}: 0.5 {{$translate['gel']}}
              </div>
                <div class="cs-spb-10 cs-tx-18 cs-tx-bold cs-cl-blue" style="display: none" id="valueWP">@if($lang == 'en') Total: @else სულ: @endif <span id="value">0</span> {{$translate['gel']}}</div>
              <div class="cs-pdb-10">
                {{--<form action="/developer_version/public/ge/test-calldetails" method="post">--}}
                  <div class="cs-spb-10"><!--
                    --><div class="cs-dp-inline-block cs-al-bottom cs-pdt-10 cs-spr-10" style="width: 200px;">
                      <div class="cs-spb-04 cs-tx-bold">{{$translate['from']}}:</div>
                      <div><input type="text" class="js-date-picker" id="fromdate"  onchange="$(this).removeClass('errorDate');selectDate(1)" /></div>
                    </div><!--
                    --><div class="cs-dp-inline-block cs-al-bottom cs-pdt-10 cs-spr-10" style="width: 200px;">
                      <div class="cs-spb-04 cs-tx-bold">{{$translate['chatvlit']}}:</div>
                      <div><input type="text" class="js-date-picker" id="todate" onchange="$(this).removeClass('errorDate');selectDate(0)" /></div>
                    </div>
                      <div class="cs-spb-10 tsr-lightbox-layout-a-field" id="errorInputDate" style="
    width: 200px;
    display: none;
    margin-top: 6px;
"><div class="cc-errorbox">მაქსიმუმ 3 თვის</div></div><!--

                    --><div class="cs-dp-inline-block cs-al-bottom cs-pdt-10 cs-spr-10">
                      <button class="tsr-btn tsr-btn-form" id="submitdetailbtn" onclick="initCallDetails()" @if($query->status == 1 || $query->status == 2) disabled @endif
>@if($lang == 'en') Submit @else შესრულება @endif</button>
                    </div><!--
                  --></div>
                {{--</form>--}}
              </div>
            </div>
              <div class="js-local-details-content" style="background-color: #D8DFDC;color: orangered;padding: 8px 13px;width: 99%;margin-bottom: 20px; display: none" id="errorWP">

              </div>
              <div class="js-local-details-content" style="@if($query->status == 1 || $query->status == 2) display: block @else display:none @endif;" id="viewDetails">
                <div class="cs-spb-10 cs-tx-18 cs-tx-bold cs-cl-blue">{{$translate['request_of_details']}}</div>
                  <div class="cs-spv-30" style="max-width: 480px;">
                      <div class="cs-spb-04" style="text-align: center"><span><span id="percent">@if($query->status == 1 || $query->status == 2) {{$query->percent}} @endif</span>%</span></div>
                      <div class="cs-spb-00"><div class="tsr-progressline"><i style="@if($query->status == 1 || $query->status == 2) width:{{$query->percent}}% @else width: 0%; @endif" class="progress"></i></div></div>
                  </div>
                <div style="margin-top: 25px; width: 99%;padding: 1%; background-color: #d8dfdc; width: 455px">
                  <b>{{$translate['date']}}:</b>
                    @if($lang == 'en')
                        From: <span id="labelfromdate">
                        @if($query->status == 1 || $query->status == 2) {{$query->fromdate}} @endif
                    </span>
                        Including:
                        <span id="labeltodate">
                        @if($query->status == 1 || $query->status == 2) {{$query->todate}} @endif
                    </span>

                    @else
                    <span id="labelfromdate">
                        @if($query->status == 1 || $query->status == 2) {{$query->fromdate}} @endif
                    </span>
                    დან
                    <span id="labeltodate">
                        @if($query->status == 1 || $query->status == 2) {{$query->todate}} @endif
                    </span>
                    ჩათვლით
                    @endif
                  <br><br>
                  {{$translate['waiting_time']}}: <b><span id="timeleft">@if($query->status == 1 || $query->status == 2) {{($query->timeleft == 0)?1:$query->timeleft}} @else 5 @endif</span> {{$translate['min']}}</b>
                </div>
              </div>
                <div style="display:none; margin-top: 20px;" id="resultdonebtn">
                    <div class="cs-spb-10 cs-tx-18 cs-tx-bold cs-cl-blue">{{$translate['request_details_done']}}:</div>
                    <a href="/developer_version/public/{{$lang}}/private/private-cabinet/calldetails">
                        <button class="tsr-btn tsr-btn-form tsr-btn-green" >@if($lang == 'en') View  @else ნახვა @endif </button>
                    </a>
                </div>
                  <div style="@if($query->status == 4 || $query->status == 3) display:block; @else display:none; @endif margin-top: 20px;" id="resultdonebtnviewed">
                      <a href="/developer_version/public/{{$lang}}/private/private-cabinet/calldetails">
                          <button class="tsr-btn tsr-btn-form tsr-btn-green" >@if($lang == 'en') Archive @else არქივი @endif</button>
                      </a>
                      <a href="/developer_version/public/{{$lang}}/removedetails">
                          <button class="tsr-btn tsr-btn-form tsr-btn-green" >@if($lang == 'en') Delete Archive @else არქივის წაშლა @endif</button>
                      </a>
                  </div>

            </div>
          </div>
          @if($userinfo->return->brand == 1)
          <div class="cs-spt-20 cs-spb-40 tsr-forms js-local-details">
            <div class="cs-pdb-10"><label class="cc-label-cbox"><input type="radio" name="local-details" class="js-local-details-switcher"><span class="cs-spl-04 cs-tx-18">@if($lang == 'en') Invoice @else ინვოისი @endif</span></label></div>
            <div class="tsr-com-lockbox js-local-details-locker">
              <div class="tsr-com-lockbox-layer tsr-semiwhite"></div>
              <div class="cs-pdb-10">
                <form action="#">
                  <div class="cs-spb-10"><!--
                    --><div class="cs-dp-inline-block cs-al-bottom cs-pdt-10 cs-spr-10" style="width: 200px;">
                      <div class="cs-spb-04 cs-tx-bold">{{$translate['choose']}}:</div>
                      <div>
                          <select id="monthSelect" onchange="$('.cc-select').removeClass('errorDate');">
                          @if($lang == 'en')
                              <option value="0">Month</option>
                              @foreach($invoiceDate as $key => $val)
                                      <option value="{{date('m', strtotime($val))}}">{{$monthen[date('m', strtotime($val))]}}</option>
                              @endforeach
                          @else
                              <option value="0">თვე</option>
                              @foreach($invoiceDate as $key => $val)
                                  <option value="{{date('m', strtotime($val))}}">{{$monthka[date('m', strtotime($val))]}}</option>
                              @endforeach

                          @endif
                          </select>

                      </div>
                    </div>
                      <div class="cs-dp-inline-block cs-al-bottom cs-pdt-10 cs-spr-10" style="width: 100px;">
                          <div><a class="tsr-btn tsr-btn-form" onclick="getInvoice(); $('#invoiceArchive').fadeOut()">{{$translate['submit_invoice']}}</a></div>
                    </div>
                    </div>
                </form>
              </div>
              <div class="js-local-details-content">
                <div><a href="{{$lang}}/download-invoice" download id="downloadInvoiceBtn" class="tsr-btn" style="display:none;">{{$translate['download_pdf']}}</a></div>
                <div><span id="noInvoice" style="display: none">ინვოისი ვერ მოიძებნა...</span></div>
              </div>
                <div style="@if(isset($invoiceArchove)) display:block; @else display:none; @endif margin-top: 20px;" id="invoiceArchive">
                    <a download="" href="/developer_version/public/{{$lang}}/getinvoices">
                        <button class="tsr-btn tsr-btn-form tsr-btn-green" >@if($lang == 'en') Archive @else არქივი @endif</button>
                    </a>
                    <a href="/developer_version/public/{{$lang}}/removeinvoice">
                        <button class="tsr-btn tsr-btn-form tsr-btn-green" >@if($lang == 'en') Delete Archive @else არქივის წაშლა @endif</button>
                    </a>
                </div>
            </div>
          </div>
            @endif

        </div>
      </div>
    </div>
<a href="{{$lang}}/detailfillbalance/1" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax" style="display: none" id="fillbalancebtn"></a>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="site/tsr-core/tsr-scripts/jquery-dtpicker/jquery.dtpicker.patched.min.js"></script>
     <script type="text/javascript">/*<![CDATA[*/
       var update;
       $(document).ready(function() {
           $('.cc-label-cbox').on('click', 'a', function(e){
               e.preventDefault();
           });
         $('#table_0').DataTable();
        /* tooltips */

       @if($userinfo->return->brand == 1)
                   /* details */
        (function() {
          var $blocks = $('.js-local-details');
          $blocks.each(function() {
            var $block     = $(this);
            var $radio     = $('.js-local-details-switcher', this);
            var $locker    = $('.js-local-details-locker',   this);
//            var $container = $('.js-local-details-content',  this);
            $block.on('tsr-details-on', function() {
              $blocks.trigger('tsr-details-off');
              $radio.prop('checked', true).trigger('cc-update');
              $locker.removeClass('is-hidden');
//              $container.show();
            });
            $block.on('tsr-details-off', function() {
              $radio.prop('checked', false).trigger('cc-update');
              $locker.addClass('is-hidden');
//              $container.hide();
            });
            $radio.on('click', function() {
              $block.trigger('tsr-details-on');
            });
          }).trigger('tsr-details-off');
            $('.js-local-details-default').trigger('click');
        })();

           @endif
                   /* date-picker */
        (function() {
          $('.js-date-picker').appendDtpicker({
            'locale': $('html').attr('lang'),
            'dateFormat': 'YYYY-MM-DD',
            'dateOnly': true,
            'disableInit': true,
            'animation': false,
            'todayButton': false,
            'firstDayOfWeek': 1,
            'minDate': '{{$mindate}}',
            'maxDate': '{{date("Y-m-d")}}'
          });
        })();

        

      });



       function initCallDetails(){
         var fromDate = $('#fromdate').val();
         var toDate = $('#todate').val();
         if(dateFilter(fromDate, toDate) == false){
           $('#fromdate').addClass('errorDate');
           $('#todate').addClass('errorDate');
            return false;
         }
         $('#fromdate').removeClass('errorDate');
         $('#todate').removeClass('errorDate');

         var res = sendReq(fromDate, toDate);

       }


       function sendReq(fromDate, toDate){
           $('#resultdonebtn').fadeOut();
           $('#resultdonebtnviewed').fadeOut();
            var returndata = '';

            $.ajax({
                method: "POST",
                url: "/developer_version/public/{{$lang}}/getdetails",
                data: { from_date: fromDate, to_date: toDate }
            })
            .done(function( msg ) {
              var res = JSON.parse(msg);
              if(res.result == true){
                showDetails(res);
              } else {
                $('#fromdate').addClass('errorDate');
                $('#toDate').addClass('errorDate');
              }
            });
           showview = true;
       }


       function showDetails(data){
           if(parseInt(data.error) < 0){
               if(data.error == -101){
                   console.log('yes: balance');
                   $('#fillbalancebtn').attr("href", "{{$lang}}/detailfillbalance/"+data.status);
                   $('#fillbalancebtn').click();
                   return 0;
               } else {
                   $('#errorWP').fadeIn();
                   $('#errorWP').html('შეცდომააა');
               }
           } else {
               $('#errorWP').fadeOut();
           }
           $('.progress').animate({
               'width':0+'%'
           }, 100);
            $('#viewDetails').fadeIn();
            $('#submitdetailbtn').prop('disabled', true);
            $('#timeleft').html((data.timeleft == 0)?1:data.timeleft);
            $('#labelfromdate').html(data.fromdate);
            $('#labeltodate').html(data.todate);
            $('#percent').html(data.percent);
           showview = true;
            update = setInterval(function(){
               checkUpdate();
            },15000);
       }


        $(function () {
            update = setInterval(function(){
                checkUpdate();
            },5000);
            $(window).scrollTop($('.tsr-section-refiner').offset().top);
            setTimeout(function () {
                $(window).scrollTop($('.tsr-section-refiner').offset().top);

            }, 500);
        });


       function checkUpdate(){
            $.ajax({
               method: "POST",
               url: "/developer_version/public/{{$lang}}/checkupdate"
            }).done(function( msg ) {
              writeUpdate(JSON.parse(msg));
            });
       }
var percentPointer = '';
var showview = @if($query->status == 1 || $query->status == 2) true @else false @endif;
var lastpercent = 0;
         var firstinit = 0;
       function writeUpdate(data){
            if(data.timeleft == 0 && data.percent == 0){
                data.timeleft = 1;
            }
           if(parseInt(data.error) < 0){
               if(data.error == -101){
                   console.log('yes: balance');
                   $('#fillbalancebtn').attr("href", "{{$lang}}/detailfillbalance/"+data.status);
                   $('#fillbalancebtn').click();
                   return 0;
               } else {
                   $('#errorWP').fadeIn();
                   $('#errorWP').html('შეცდომააა');
               }
           } else {
               $('#errorWP').fadeOut();
           }
            if(firstinit <= 3){
                percentPointer = setInterval(function(){
                    var x = parseInt(document.getElementsByClassName('progress')[0].style.width);
                    $('#percent').html(x);
                },1000);
            }

            firstinit++;
        if(data.percent != lastpercent){

            $( ".progress" ).stop(true,false);
            $('.progress').animate({
                'width':data.percent+'%'
            }, 1000);
            lastpercent = data.percent;
        } else {

            $('.progress').animate({
                'width':'100%'
            }, data.timeleft*1000*60*3);
        }

           if(data.percent >= 100){
               $('#timeleft').html('0');
               clearInterval(percentPointer);
               $('.progress').css({'background-color':'#00b48c'});
           }
         $('#timeleft').html((data.timeleft==0)?1:data.timeleft);
         $('#percent').html(data.percent);
           if(data.status == '3' && showview === true){
               $('#resultdonebtn').fadeIn();
           }

       }





       function dateFilter(fromDate, toDate){
         if(fromDate == '' || toDate == ''){
           return false;
         }

         fromDate = fromDate.split('-');
         toDate = toDate.split('-');
         if(fromDate.length != 3 || toDate.length != 3) {
           return false;
         }
         if(!isNumber(fromDate[2]) || !isNumber(fromDate[1]) || !isNumber(fromDate[0]) || !isNumber(toDate[2])
                 || !isNumber(toDate[1]) || !isNumber(toDate[0])){
           return false;
         }
         fromDate = fromDate[0]+'-'+fromDate[1]+'-'+fromDate[2];
         toDate = toDate[0]+'-'+toDate[1]+'-'+toDate[2];
         try {
           fromDate = new Date(fromDate);

         }
         catch(err) {
           return false;
         }


         try {
           toDate = new Date(toDate);
         }
         catch(err) {
           return false;
         }

         if(toDate < fromDate){
           return false;
         }

         return true;
       }

       function isNumber(n) {
         return !isNaN(parseFloat(n)) && isFinite(n);
       }

         function selectDate(arg){
             $('#errorInputDate').fadeOut();
             $('#valueWP').fadeOut();
             var fromDate = $('#fromdate').val();
             var toDate = $('#todate').val();
            if(arg == 1 && toDate == ''){
                $('#todate').val(fromDate);
            }
             if(fromDate == '' || toDate == ''){
                 return;
             }
             try {
                 fromDate = new Date(fromDate).getTime();

             }
             catch(err) {
                 return false;
             }

             try {
                 toDate = new Date(toDate).getTime();
             }
             catch(err) {
                 return false;
             }

             if(!checkdays(fromDate,toDate)){
//                $('#errorInputDate').fadeIn();
                 return 0;
             }

             checkDeysprice(fromDate,toDate);

         }

         function checkdays(fromDate,toDate){

             var today = new Date().getTime();
             var sub = today-fromDate;
             sub = parseInt(sub/86400000);
             if(sub>90){
                return false;
             }
             return true;
         }

         function checkDeysprice(fromDate,toDate){
             var sub = toDate-fromDate;
             sub = parseInt(sub/86400000)+1;
             if(sub>0){
                 $('#value').html(sub*0.5);
                 $('#valueWP').fadeIn();
             }
         }


         function getInvoice(){
             var month = $('#monthSelect').val();
             if (month == 0){
                 $('#monthSelect').addClass('errorDate');
                 $('.cc-select').addClass('errorDate');
                 return 0;
             }
             $.ajax({
                 method: "POST",
                 url: "/developer_version/public/{{$lang}}/getinvoices",
                 data: { month: month }
             })
             .done(function( msg ) {
                 if(msg == 1){
                     $('#noInvoice').fadeOut();
                     $('#downloadInvoiceBtn').fadeIn();
                 } else {
                     $('#noInvoice').fadeIn();
                     $('#downloadInvoiceBtn').fadeOut();
                 }
             });
         }


     /*]]>*/</script>

<style>
  .pregressbar{
    float: left;
    width: 100%;
    height: 16px;
    background-color: #642887;
    border: 1px solid #481d60;
    padding-left: 3px;
    border-radius: 12px;
  }
  .pregressbar .progress{
    float: left;
    width: 40%;
    background-color: #00B48C;
    height: 10px;
    margin-top: 3px;
    border-radius: 8px;
  }
  .errorDate{
    border:1px solid #da552a!important;
  }
</style>
@endsection