@extends('site')

@section('content')
     @include('site.user.usercabinet-head')


    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">

          <h1 class="tsr-title cs-spb-30">{{$item->title}}</h1>

          @if(isset($installment->hasInstalment) && $installment->hasInstalment==true)

          <?php
              if(!is_array($installment->planedPayments)) { (array)$installment_plan[]=$installment->planedPayments; }
              else { (array)$installment_plan = $installment->planedPayments; }
          ?>
            <?php $totalPlanned =0;?>
            <?php $totalPayed = 0; ?>
            <?php
            if(!is_array($installment->previousPayments)) {
                (array)$installment_payed[]=$installment->previousPayments;
            } else {
                (array)$installment_payed = $installment->previousPayments;
            }
            if(!is_array($installment->planedPayments)) {
                (array)$installment_Planned[]=$installment->planedPayments;
            } else {
                (array)$installment_Planned = $installment->planedPayments;
            }
            $cntrow = 0;

            foreach($installment_payed as $key => $planned){
                $totalPayed+=$planned->amount;
            }
            $planedcnt = 0;
            foreach($installment_Planned as $key => $planned){
                  $totalPlanned+=$planned->amount;
                $planedcnt++;
            }


            ?>


              <div class="cs-spb-30">
                  <div class="tsr-installment-progwrapp">
                      <div class="tsr-installment-progblock">
                          <div class="tsr-installment-progblock-lt">
                              <div class="cs-tx-30">{{instalmentDate(isset($installment_plan[0]->date)?$installment_plan[0]->date:0 )}}</div>
                              <div class="cs-tx-20 cs-cl-pink">{{$translate['you_should_pay'].' '.$installment_plan[0]->amount.' '.strtoupper($translate['gel'])}}</div>
                          </div>
                          <div class="tsr-installment-progblock-gt">
                              <div class="cs-tx-20">{{$translate['total'].' '.($totalPlanned + $totalPayed).' '.strtoupper($translate['gel'])}}</div>
                          </div>
                      </div>


                      <div class="tsr-installment-progress">
                          <div class="tsr-installment-progbar">
                              <div class="tsr-installment-progline" style="width:{{($totalPayed/($totalPlanned + $totalPayed))*100}}%;">
                                  <div class="tsr-installment-progptr">
                                      <div class="tsr-installment-progdesc-a">
                                          <div class="cs-tx-20">{{$totalPayed}} {{strtoupper($translate['gel'])}}</div>
                                          <div class="cs-tx-15">{{$translate['installment_payed']}}</div>
                                      </div>
                                      <div class="tsr-installment-progdesc-b">
                                          <div class="cs-tx-20">{{$totalPlanned}} {{strtoupper($translate['gel'])}}</div>
                                          <div class="cs-tx-15" style="width: 150px">@if($lang == 'en' ) To be paid @else {{$translate['total_planned_payment']}} @endif</div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="tsr-installment-progaltdesc">
                          <span class="cs-dp-iblock">{{$translate['installment_payed']}}: {{$totalPayed}} {{strtoupper($translate['gel'])}}</span><span class="cs-dp-iblock">&nbsp;/&nbsp;</span><span class="cs-dp-iblock cs-cl-pink">{{$translate['total_planned_payment']}}: {{$totalPlanned}} {{strtoupper($translate['gel'])}}</span>
                      </div>
                  </div>
              </div>

              <hr class="tsr-com-hr cs-spv-30">

              <div class="cs-spb-10">
                  <div class="cs-tx-20">{{$translate['full_schedule']}} (@if($lang == 'en') {{$planedcnt}} {{$translate['payments_left']}} @else დარჩენილია {{$planedcnt}} გადახდა @endif</div>
              </div>

              <div class="cs-spb-30">
                  <div class="scroll-wrapper tsr-scrollpane" style="position: relative;"><div class="tsr-scrollpane scroll-content" style="height: auto; margin-bottom: 0px; margin-right: 0px;">
                          <div class="tsr-scrollpane-content">
                              <table class="tsr-com-table-standard cs-whole-x">
                                  <tbody><tr>
                                      <th class="cs-wspace-nowrap">{{$translate['date']}}</th>
                                      <th class="cs-wspace-nowrap">{{$translate['tanxa']}} ({{strtoupper($translate['gel'])}})</th>
                                      <th class="cs-wspace-nowrap">{{$translate['status']}}</th>
                                  </tr>
                                  @foreach($installment_payed as $key => $planned)
                                      <tr>
                                          <td class="cs-wspace-nowrap">{{instalmentDate(isset($planned->date)?$planned->date:0 )}}</td>
                                          <td class="cs-wspace-nowrap">{{$planned->amount.' '.strtoupper($translate['gel'])}}</td>
                                          <td class="cs-wspace-nowrap"><span class="ts-icon-circle cs-tx-11 cs-cl-turquoise"></span> {{$translate['paid']}}</td>
                                      </tr>
                                  @endforeach
                                  @foreach($installment_Planned as $key => $planned)
                                      <tr>
                                          <td class="cs-wspace-nowrap">{{instalmentDate(isset($planned->date)?$planned->date:0 )}}</td>
                                          <td class="cs-wspace-nowrap">{{$planned->amount.' '.strtoupper($translate['gel'])}}</td>
                                          <td class="cs-wspace-nowrap">@if($key == 0) <span class="ts-icon-circle cs-tx-11 cs-cl-yellow"></span> {{$translate['pay_this_month']}} @else <span class="ts-icon-circle cs-tx-11 cs-cl-lgray"></span> @endif</td>
                                      </tr>
                                  @endforeach

                                  </tbody></table>
                          </div>
                      </div><div class="scroll-element scroll-x"><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar" style="width: 100px;"></div></div></div><div class="scroll-element scroll-y"><div class="scroll-element_outer"><div class="scroll-element_size"></div><div class="scroll-element_track"></div><div class="scroll-bar" style="height: 100px;"></div></div></div></div>
              </div>



          <div class="tsr-clear"></div>



          @endif

        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->




@endsection