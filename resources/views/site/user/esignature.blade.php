@extends('site')

@section('content')
    @include('site.user.usercabinet-head')

        <!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-empty"></div>
<!-- /tsr-section-divider -->

<!-- tsr-section-generic -->
<div class="tsr-section-generic">
    <div class="tsr-container">
        @if(isset($lists->items) && is_array($lists->items))


            <table style="width: 50%;">
                <thead>
                    <tr style="height: 38px">
                        <th style="background-color: #e2e2e2;"></th>
                        <th style="background-color: #e2e2e2;">{{$translate['date']}}</th>
                        <th style="background-color: #e2e2e2;">{{$translate['documents']}}</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($lists->items as $key => $val)


                    <tr style="border-bottom: 1px solid #CCC;">
                            <td style="background-color: #FFF;padding: 8px 5px;">{{$key+1}}</td>
                            <td style="background-color: #FFF;;padding: 8px 5px; text-align: center;">{{isset($val->completeDate)?$val->completeDate:''}}</td>
                            <td style="text-align: center;background: #FFF;padding: 8px 5px;"><a href="/developer_version/public/{{$lang}}/getdocumentbyid/{{$val->docId}}" target="_blank">{{$val->subjectGeo}}</a></td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <div style="width: 100%; height: auto; padding: 10px 6px; background-color: #CCCCCC;">
                @if($lang == 'en')
                    You don’t have any E-Document signed.
                @else
                    თქვენ არ გაქვთ ელექტრონულად ხელმოწერილი დოკუმენტები.
                @endif
            </div>
        @endif
    </div>
</div>

<script>/*<![CDATA[*/






    $(document).ready(function() {
        $(window).scrollTop($('.tsr-section-refiner').offset().top);
        setTimeout(function () {
            $(window).scrollTop($('.tsr-section-refiner').offset().top);

        }, 500);
        /* period button */
        (function() {
            $('.js-btn-period-button').on('click', function() {
                $('.js-btn-period-container').slideToggle(300, 'easeOutExpo');
            });
        })();




    });

</script>

<div class="tsr-section-divider tsr-divider-empty"></div>

@endsection