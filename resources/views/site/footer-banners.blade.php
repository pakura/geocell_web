 <!-- tsr-section-support-listing -->
    <div class="tsr-section-support-listing">
      <div class="tsr-container">
        <div class="tsr-boxgrid-wrapper">
          <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">
          <?php 

              $indx = (\Request::segment(2)=='business')?99:78;

           ?> 
          @if(isset($banners[$indx]) && count($banners[$indx]))
            @foreach($banners[$indx] as $banner)
            <div class="tsr-boxcol-outer">
              <div class="tsr-boxcol-inner">
                <a href="{{$banner->url?$lang.'/'.$banner->url:'javascript:;'}}" class="tsr-module-support">
                  <figure class="tsr-module-support-icon ts-icon-{{$banner->photo}}"></figure>
                  <header class="tsr-module-support-title">{{$banner->title}}</header>
                  <span class="tsr-module-support-intro">{{strip_tags($banner->description)}}</span>
                </a>
              </div>
            </div>
            @endforeach
           @endif 

          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-support-listing -->