<!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-space-top">
  <div class="tsr-container">
    <h1>{{trans('site.latestnews')}}</h1>
  </div>
</div>
<!-- /tsr-section-divider -->

<!-- tsr-news-listing -->
<div class="tsr-section-news-listing tsr-color-shade">
  <div class="tsr-container">
    <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">

      @foreach($articles as $item)
        <div class="tsr-boxcol-outer">
          <div class="tsr-boxcol-inner">
            <a href="{{$lang.'/'.$item->page_slug.'/'.$item->slug}}" class="tsr-module-news">
              <div class="tsr-clearfix">
                <div class="tsr-module-news-desc">
                  <header class="tsr-module-news-title">{{$item->title}}</header>
                  <span class="tsr-module-news-date">{{convertDate($item->creation_date)}}</span>
                  <span class="tsr-module-news-intro">{!! $item->description !!}</span>
                </div>
              </div>
            </a>
          </div>
        </div>
      @endforeach

    </div>
  </div>
  <a href="{{$lang.'/'.$item->page_slug}}" class="tsr-btn-view-all"><span>{{$translate['viewallnews']}}<i>({{$articlesItemsCount}})</i></span></a>
</div>

@if(\Request::segment(2) == 'business')

<!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-space-top">
  <div class="tsr-container">
    <h1>{{trans('site.latestnews')}}</h1>
  </div>
</div>
<!-- /tsr-section-divider -->



<!-- tsr-news-listing -->


<div class="tsr-section-generic">
  <div class="tsr-container">
    <div class="tsr-section-generic-pad-h">
      <div data-grid-type="blog-listing" data-grid-gap="2" data-handle="equalh" data-equalh="blog-listing-title">
        @foreach($blog as $item)
        <a href="{{$lang.'/'.$item->page_slug.'/'.$item->slug}}" class="tsr-bg-hovered-block tsr-color-white cs-al-center" data-grid-cell="">
          @if(isset($item->file))
          <div class="cs-spb-20"><img src="/geocell/public/uploads/articles/{{$item->file}}" alt="" class="cs-max-whole-x"></div>
          @endif
          <div class="cs-spb-10" data-equalh="blog-listing-title" style="min-height: 22px;"><h4 class="tsr-title cs-cl-primary">{{$item->title}}</h4></div>
          <div class="cs-spb-20"><span class="cs-tx-bold cs-cl-blue">{{convertDate($item->creation_date)}}</span></div>
        </a>
        @endforeach
      </div>
    </div>
  </div>
  <a href="{{$lang.'/'.$item->page_slug}}" class="tsr-btn-view-all"><span>{{$translate['viewallnews']}}<i>({{$blogItemsCount}})</i></span></a>
</div>




@endif