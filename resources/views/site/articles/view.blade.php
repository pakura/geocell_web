@extends('site')

@section('content')
      
  <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad">
          <div class="cs-spb-10"><h1 class="tsr-title">{{$article->title}}</h1></div>
          <div class="cs-spb-20">
              {!! html_entity_decode($article->content) !!}
          </div>
          <div class="cs-tx-13 cs-spt-20 cs-cl-gray">{{convertDate($article->creation_date)}}</div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

@endsection