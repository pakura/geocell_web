@extends('site')

@section('content')
    
    @include('site.textcontent_default')


    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <div class="tsr-clearfix">
            
            @if($startYear)
            <div class="cs-fl-left cs-spr-20">
              <form action="{{Request::url()}}?page=2" method="get" >
                <div class="tsr-forms">
                  <div class="tsr-clearfix">

                    <div class="cs-fl-left cs-spb-10 cs-spr-10" style="min-width: 120px; max-width: 260px;">

                        <select onchange="this.form.submit()" name="year">    
                          <option value=''>{{trans('site.year')}}</option>
                          @for($i=2012;$i<=date('Y');$i++)
                          <option value= "{{$i}}" {{ (@$_GET['year']==$i)?'selected':'' }}>{{$i}}</option>
                          @endfor                         
                        </select>

                    </div>
                    <div class="cs-fl-left cs-spb-10" style="min-width: 180px; max-width: 260px;">
                        <select onchange="this.form.submit()" name="month">
                          <option value=''>{{trans('site.month') }}</option>
                          @foreach (trans('site.months') as $key=>$month)
                            <?php $val = $key+1; ?>  
                            <option value="{{$val}}" {{ (@$_GET['month']==$val)?'selected':'' }}>{{$month}}</option> 
                          @endforeach  
                        </select>
                    </div>

                  </div>
                </div>
              </form>
            </div>
             @endif
             
            <!-- <div class="cs-fl-right cs-pdt-20">
              <div class="cs-spb-10 cs-smt-08">
                <div class="tsr-com-linklist">
                  <ul class="cs-tx-13 cs-tx-bold"> 
                     <li><a href="#" class="is-choosen">All</a></li> 
                     <li><a href="#">Service news</a></li> 
                     <li><a href="#">Company news</a></li> 
                   </ul>
                </div>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <div class="tsr-com-collapser">
           @foreach($newsList as $news)
            <div class="cs-spb-40">
              <div><a href="{{ $lang.'/'.$item->full_slug.'/'.$news->slug }}" class="cs-tx-22 noline">{{$news->title}}</a></div>
              <div>{!! $news->description !!}</div>
              <div class="cs-tx-13 cs-spt-10 cs-cl-gray">{{convertDate($news->creation_date)}}</div>
            </div>
            @endforeach 
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

      <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad">
          <div class="tsr-pagination tsr-pagination-left">
            <div class="tsr-center">
              {!! str_replace('/?', '?',$newsList->render()) !!} 
               
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->  

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty tsr-color-white"></div>
    <!-- /tsr-section-divider -->


    @endsection