@extends('site')

@section('content')

<div class="tsr-lightbox-layout-a" id="unique-lightbox-id">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
        
        <div class="cs-spb-20 cs-tx-bold">{{$msg}}</div>
       
      </div>
    </div>
  </div>
</div>


@endsection