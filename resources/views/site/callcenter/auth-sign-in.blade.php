@extends('site')

@section('content')
 

<div class="tsr-lightbox-layout-a" id="unique-lightbox-id" style="margin-top:-20px">
  <div class="tsr-com-collapser-outer">
    <div class="tsr-com-collapser-inner">
      <div class="cs-al-center">
        <div class="cs-spb-20 cs-tx-40 cs-tx-bold">{{$translate['signin']}}</div>
        <div class="cs-spb-20">{{$translate['sign_in_text']}}  </div>
        <div class="cs-spb-20 cs-spt-30 cs-mg-center" style="width: 260px;">
        <div class="cs-spb-04 cs-spt-30">
          <form action="{{$lang}}/alternumber" method="post" class="tsr-forms" onsubmit="alterNumber(this)"  method="post">
            <div class="tsr-lightbox-layout-a-form-pad">
              <div class="cs-spb-04"><label class="cc-label">{{$translate['cell_number']}}</label></div>
              <div class="cs-spb-04 tsr-lightbox-layout-a-field-combo">
                <div class="tsr-lightbox-layout-a-field-a"><input type="text" name="phone" data-inputmask="'mask': '\\599999999'" class="cs-al-center" /></div>
                <div class="tsr-lightbox-layout-a-field-b" style="visibility: hidden;"><img src="site/tsr-core/tsr-images/tsr-spi-loader-24.gif" alt=""></div>
              </div>
              <div class="cs-spb-10 tsr-lightbox-layout-a-field msisdn-error" style="display: none;"><div class="cc-errorbox">Wrong MSISDN</div></div>
            </div>

            <div class="cs-spb-20"><button class="tsr-btn-xsmall  tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise">{{$translate['next']}}</button></div>

          </form>
        </div>
        
       <!--  <div class="cs-spb-20 cs-spt-30"><a href="#">Registration</a></div> -->
      </div>
    </div>
  </div>
</div>
</div>
<script src="site/tsr-core/tsr-scripts/jquery-inputmask/jquery.inputmask.min.js"></script>
<script type="text/javascript">/*<![CDATA[*/
 var $box = $('#unique-lightbox-id');

  $('input[type="text"]', $box).inputmask();

 
/*]]>*/

</script>

@endsection
