@extends('site')

@section('content')
     @include('site.user.usercabinet-head')
    

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">

          <h1 class="tsr-title cs-spb-10">Profile Settings</h1>

          <div class="cs-spb-40">
            <div class="tsr-com-linklist">
              <ul class="cs-tx-13 cs-tx-bold"><!--
                --><li><a href="#" class="is-choosen">Personal info</a></li><!--
                --><li><a href="#">Notifications</a></li><!--
                --><li><a href="#">Security settings</a></li><!--
              --></ul>
            </div>
          </div>

          <form action="#" method="post" class="tsr-forms" id="local-profile-form">
            <div class="tsr-grid tsr-clearfix">

              <div class="tsr-col-03 cs-spb-20">
                <div><label class="cc-label">Cell number</label></div>
                <div><input type="text" data-inputmask="'mask': '\\599 999999'"></div>
              </div>
              <div class="tsr-col-03 cs-spb-20">
                <div><label class="cc-label">Personal ID number</label></div>
                <div><input type="text" value="10000000000" disabled=""></div>
              </div>

              <div class="tsr-clear"></div>

              <div class="tsr-col-03 cs-spb-20">
                <div><label class="cc-label">First name</label></div>
                <div><input type="text" value="John" disabled=""></div>
              </div>
              <div class="tsr-col-03 cs-spb-20">
                <div><label class="cc-label">Last name</label></div>
                <div><input type="text" value="Doe" disabled=""></div>
              </div>

              <div class="tsr-clear"></div>

              <div class="tsr-col-12 cs-spb-20">
                <div><label class="cc-label cc-no-margin">Birth date</label></div>
                <div class="tsr-clearfix">
                  <div class="cs-fl-left cs-spt-04 cs-spr-04" style="width: 100px;"><select><option>Day</option><option>1</option><option>2</option></select></div>
                  <div class="cs-fl-left cs-spt-04 cs-spr-04" style="width: 160px;"><select><option>Month</option><option>January</option><option>February</option></select></div>
                  <div class="cs-fl-left cs-spt-04 cs-spr-04" style="width: 100px;"><select><option>Year</option><option>2000</option><option>2001</option></select></div>
                </div>
              </div>

              <div class="tsr-clear"></div>

              <div class="tsr-col-03 cs-spb-20">
                <div><label class="cc-label">E-mail</label></div>
                <div><input type="text"></div>
              </div>

              <div class="tsr-clear"></div>

              <div class="tsr-col-12 cs-spb-20">
                <div><label class="cc-label cc-no-margin">Legal address</label></div>
                <div class="tsr-grid tsr-clearfix">
                  <div class="tsr-col-03 cs-spt-04">
                    <div><select><option>City</option><option>Tbilisi</option><option>Batumi</option></select></div>
                  </div>
                  <div class="tsr-col-03 cs-spt-04">
                    <div><select><option>District</option><option>Saburtalo</option><option>Vake</option></select></div>
                  </div>
                  <div class="tsr-col-03 cs-spt-04">
                    <div><input type="text" class="placeholder" title="Street"></div>
                  </div>
                  <div class="tsr-col-03 cs-spt-04">
                    <div><input type="text" class="placeholder" title="#"></div>
                  </div>
                </div>
              </div>

              <div class="tsr-col-12 cs-spb-20">
                <div><label class="cc-label cc-no-margin">Physical address</label></div>
                <div class="tsr-grid tsr-clearfix">
                  <div class="tsr-col-03 cs-spt-04">
                    <div><select><option>City</option><option>Tbilisi</option><option>Batumi</option></select></div>
                  </div>
                  <div class="tsr-col-03 cs-spt-04">
                    <div><select><option>District</option><option>Saburtalo</option><option>Vake</option></select></div>
                  </div>
                  <div class="tsr-col-03 cs-spt-04">
                    <div><input type="text" class="placeholder" title="Street"></div>
                  </div>
                  <div class="tsr-col-03 cs-spt-04">
                    <div><input type="text" class="placeholder" title="#"></div>
                  </div>
                </div>
              </div>

              <div class="tsr-col-12 cs-spb-20">
                <div class="tsr-clearfix">
                  <div class="cs-fl-left cs-spt-04 cs-spr-04"><input type="submit" value="Reset" class="tsr-btn tsr-btn-gray tsr-btn-form"></div>
                  <div class="cs-fl-left cs-spt-04 cs-spr-04"><input type="submit" value="Save changes" class="tsr-btn tsr-btn-turquoise tsr-btn-form"></div>
                </div>
              </div>

            </div>
          </form>

        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->


@endsection