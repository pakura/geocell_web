@extends('site')

@section('content')
     @include('site.user.usercabinet-head')
     <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">

          <h1 class="tsr-title cs-spb-10">Call Details</h1>

          <div class="cs-spt-20 cs-spb-40 tsr-forms js-local-details">
            <div class="cs-pdb-10"><label class="cc-label-cbox"><input type="radio" name="local-details" class="js-local-details-switcher"><span class="cs-spl-04 cs-tx-18">Call details</span></label></div>
            <div class="tsr-com-lockbox js-local-details-locker">
              <div class="tsr-com-lockbox-layer tsr-semiwhite"></div>
              <div class="cs-pdb-10 cs-cl-blue">Price for 1 day: 0.3 gel</div>
              <div class="cs-pdb-10">
                <form action="#">
                  <div class="cs-spb-10"><!--
                    --><div class="cs-dp-inline-block cs-al-bottom cs-pdt-10 cs-spr-10" style="width: 200px;">
                      <div class="cs-spb-04 cs-tx-bold">From:</div>
                      <div><input type="text" class="js-date-picker" /></div>
                    </div><!--
                    --><div class="cs-dp-inline-block cs-al-bottom cs-pdt-10 cs-spr-10" style="width: 200px;">
                      <div class="cs-spb-04 cs-tx-bold">To:</div>
                      <div><input type="text" class="js-date-picker" /></div>
                    </div><!--
                    --><div class="cs-dp-inline-block cs-al-bottom cs-pdt-10 cs-spr-10">
                      <input type="submit" value="Submit" class="tsr-btn tsr-btn-form">
                    </div><!--
                  --></div>
                </form>
              </div>
              <div class="js-local-details-content">
                <div class="cs-spb-10 cs-tx-18 cs-tx-bold cs-cl-blue">Total: 13.85 gel</div>
                <div><a href="#" class="tsr-btn">Download PDF</a></div>
              </div>
            </div>
          </div>

          <div class="cs-spt-20 cs-spb-40 tsr-forms js-local-details">
            <div class="cs-pdb-10"><label class="cc-label-cbox"><input type="radio" name="local-details" class="js-local-details-switcher"><span class="cs-spl-04 cs-tx-18">Invoices</span></label></div>
            <div class="tsr-com-lockbox js-local-details-locker">
              <div class="tsr-com-lockbox-layer tsr-semiwhite"></div>
              <div class="cs-pdb-10">
                <form action="#">
                  <div class="cs-spb-10"><!--
                    --><div class="cs-dp-inline-block cs-al-bottom cs-pdt-10 cs-spr-10" style="width: 200px;">
                      <div class="cs-spb-04 cs-tx-bold">Choose:</div>
                      <div><select><option>Month</option><option>January</option><option>February</option></select></div>
                    </div><!--
                    --><div class="cs-dp-inline-block cs-al-bottom cs-pdt-10 cs-spr-10" style="width: 100px;">
                      <div><select><option>Year</option><option>2000</option><option>2001</option></select></div>
                    </div><!--
                  --></div>
                </form>
              </div>
              <div class="js-local-details-content">
                <div class="cs-spb-10 cs-tx-18 cs-tx-bold cs-cl-blue">Total: 13.85 gel</div>
                <div><a href="#" class="tsr-btn">Download PDF</a></div>
              </div>
            </div>
          </div>

          <div class="cs-spt-20 cs-spb-40 tsr-forms js-local-details">
            <div class="cs-pdb-10"><label class="cc-label-cbox"><input type="radio" name="local-details" class="js-local-details-switcher"><span class="cs-spl-04 cs-tx-18">Other invoices</span></label></div>
            <div class="tsr-com-lockbox js-local-details-locker">
              <div class="tsr-com-lockbox-layer tsr-semiwhite"></div>
              <div class="cs-pdb-10">
                <form action="#">
                  <div class="cs-spb-10"><!--
                    --><div class="cs-dp-inline-block cs-al-bottom cs-pdt-10 cs-spr-10" style="width: 200px;">
                      <div class="cs-spb-04 cs-tx-bold">Choose:</div>
                      <div><select><option>Month</option><option>January</option><option>February</option></select></div>
                    </div><!--
                    --><div class="cs-dp-inline-block cs-al-bottom cs-pdt-10 cs-spr-10" style="width: 100px;">
                      <div><select><option>Year</option><option>2000</option><option>2001</option></select></div>
                    </div><!--
                  --></div>
                </form>
              </div>
              <div class="js-local-details-content">
                <div class="cs-spb-10 cs-tx-18 cs-tx-bold cs-cl-blue">Total: 13.85 gel</div>
                <div><a href="#" class="tsr-btn">Download PDF</a></div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->
   

    <script src="site/tsr-core/tsr-scripts/jquery-dtpicker/jquery.dtpicker.patched.min.js"></script>
     <script type="text/javascript">/*<![CDATA[*/
      $(document).ready(function() {

        /* tooltips */


        /* details */
        (function() {
          var $blocks = $('.js-local-details');
          $blocks.each(function() {
            var $block     = $(this);
            var $radio     = $('.js-local-details-switcher', this);
            var $locker    = $('.js-local-details-locker',   this);
            var $container = $('.js-local-details-content',  this);
            $block.on('tsr-details-on', function() {
              $blocks.trigger('tsr-details-off');
              $radio.prop('checked', true).trigger('cc-update');
              $locker.removeClass('is-hidden');
              $container.show();
            });
            $block.on('tsr-details-off', function() {
              $radio.prop('checked', false).trigger('cc-update');
              $locker.addClass('is-hidden');
              $container.hide();
            });
            $radio.on('click', function() {
              $block.trigger('tsr-details-on');
            });
          }).trigger('tsr-details-off');
        })();

        /* date-picker */
        (function() {
          $('.js-date-picker').appendDtpicker({
            'locale': $('html').attr('lang'),
            'dateFormat': 'YYYY-MM-DD',
            'dateOnly': true,
            'disableInit': true,
            'animation': false,
            'todayButton': false,
            'firstDayOfWeek': 1
          });
        })();

        

      });
     /*]]>*/</script>


@endsection