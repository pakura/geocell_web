<?php   //if($lang=='ge'){ setlocale(LC_ALL, 'ka-GE.UTF-8'); }  ?>
    <!-- tsr-section-uboard-balance -->
    <div class="tsr-section-uboard-balance">
      <div class="tsr-container">
        <div class="tsr-clearfix">
          <div class="tsr-uboard-balance-main">
            <div class="tsr-clearfix">
             <!--  <div class="tsr-uboard-balance-main-avatar">
                <img src="site/tsr-temp/tsr-images/avatar-01.png" alt="" class="tsr-avatar-img" />
                <span class="tsr-avatar-layer">change<br>photo</span>
                <a href="tsr-lightbox-auth-avatar.html" data-fancybox-type="ajax" class="js-fancybox fancybox.ajax tsr-avatar-link" onclick="return false;"></a>
              </div> 
              tsr-uboard-balance-main-account
              -->
              <div class="">
                 <div class="tsr-uboard-balance-main-title">
                      <span class="js-tooltip-text"  @if($userInfo->return->brand==2) title="{{$translate['expire_date']}} {{convertDate(@$userInfo->return->expireDate)}}" @endif style="margin-top:10px; float:left;display:block">
					<input type="text" name="phone" id="phone" value="{{\Session::get('phone')}}" style="height: 48px;width: 300px;font-size: 24px;line-height:36px;font-weight: bold;" />

                      @if(isset($userInfo->return->blockingStatus))  
                        @if($userInfo->return->blockingStatus==0)
                          <span class="tsr-uboard-balance-main-ico ts-icon-info tsr-color-green"></span>
                        @elseif($userInfo->return->blockingStatus==3) 
                          <span class="tsr-uboard-balance-main-ico ts-icon-info tsr-color-yellow"></span>
                        @else 
                         <span class="tsr-uboard-balance-main-ico ts-icon-info tsr-color-red"></span>
                        @endif  
                      @endif  
                      <select name="year" id="year" style="height:48px;width:120px;font-size:24px;line-height:36px;">
@for($i=date("Y"); $i>2014; $i--)
                      	  <option value="{{ $i }}" {{ ((isset($_GET["year"]) && $i==$_GET["year"]) || (!isset($_GET["year"]) && $i==date("Y"))) ? "selected":"" }}>{{ $i }}</option>
@endfor                      	 
                      </select>
                      <select name="month" id="month" style="height:48px;width:240px;font-size:24px;line-height:36px;">
                      	  <option value="01" {{ ((isset($_GET["month"]) && '01'==$_GET["month"]) || (!isset($_GET["month"]) && '01'==date("m"))) ? "selected":"" }}>იანვარი</option>
                      	  <option value="02" {{ ((isset($_GET["month"]) && '02'==$_GET["month"]) || (!isset($_GET["month"]) && '02'==date("m"))) ? "selected":"" }}>თებერვალი</option>
                      	  <option value="03" {{ ((isset($_GET["month"]) && '03'==$_GET["month"]) || (!isset($_GET["month"]) && '03'==date("m"))) ? "selected":"" }}>მარტი</option>
                      	  <option value="04" {{ ((isset($_GET["month"]) && '04'==$_GET["month"]) || (!isset($_GET["month"]) && '04'==date("m"))) ? "selected":"" }}>აპრილი</option>
                      	  <option value="05" {{ ((isset($_GET["month"]) && '05'==$_GET["month"]) || (!isset($_GET["month"]) && '05'==date("m"))) ? "selected":"" }}>მაისი</option>
                      	  <option value="06" {{ ((isset($_GET["month"]) && '06'==$_GET["month"]) || (!isset($_GET["month"]) && '06'==date("m"))) ? "selected":"" }}>ივნისი</option>
                      	  <option value="07" {{ ((isset($_GET["month"]) && '07'==$_GET["month"]) || (!isset($_GET["month"]) && '07'==date("m"))) ? "selected":"" }}>ივლისი</option>
                      	  <option value="08" {{ ((isset($_GET["month"]) && '08'==$_GET["month"]) || (!isset($_GET["month"]) && '08'==date("m"))) ? "selected":"" }}>აგვისტო</option>
                      	  <option value="09" {{ ((isset($_GET["month"]) && '09'==$_GET["month"]) || (!isset($_GET["month"]) && '09'==date("m"))) ? "selected":"" }}>სექტემბერი</option>
                      	  <option value="10" {{ ((isset($_GET["month"]) && '10'==$_GET["month"]) || (!isset($_GET["month"]) && '10'==date("m"))) ? "selected":"" }}>ოქტომბერი</option>
                      	  <option value="11" {{ ((isset($_GET["month"]) && '11'==$_GET["month"]) || (!isset($_GET["month"]) && '11'==date("m"))) ? "selected":"" }}>ნოემბერი</option>
                      	  <option value="12" {{ ((isset($_GET["month"]) && '12'==$_GET["month"]) || (!isset($_GET["month"]) && '12'==date("m"))) ? "selected":"" }}>დეკემბერი</option>
                      </select>
                     
                        
                      </span>
						<div style="margin-top:-10px;">&nbsp;<a href="javascript:checkNumber();" class="tsr-btn tsr-btn-green" style="margin-top:18px;">Check</a></div>
                  </div>
                 <div class="cs-spt-20" style="clear:both;"></div>                   	  
                  		  
                  		  
                  @if(isset($userInfo->return->blockingStatus))
                    <div class="tsr-uboard-balance-main-message">
                      <?php
                        switch($userInfo->return->blockingStatus){
                          case 0:
                            echo $translate['number_is_active'];
                            break;
                          case 3:
                            echo $translate['partial_active'];
                            break;  
                          case 5:
                            echo $translate['forced'];
                            break;  
                          case 10:  
                             echo $translate['final'];
                             break;
                        }

                      ?>
                    </div>  
                  @endif
              </div> 
              			  
            
              			  
            </div>  
 
            <div class="cs-spt-20">
              <div class="tsr-uboard-balance-main-balance">
                <span class="tsr-uboard-balance-main-balance-a"><span class="cs-pdr-10">{{$translate['balance']}}</span><span class="cs-pdr-04">{{number_format(@$userInfo->return->balance,2)}} ₾</span></span>

                @if($userInfo->return->brand==1 && $userInfo->return->accountMethod==1)
                  <!-- <span class="tsr-uboard-balance-main-balance-b">/</span> -->
                    <div class="tsr-clear"></div>
                  <span class="tsr-uboard-balance-main-balance-a"><span class="cs-pdr-10">{{$translate['previous_debt']}}:</span><span class="cs-pdr-04">{{number_format(@$userInfo->return->previousMonthDebt,2)}} ₾</span></span>
                 <!--  <span class="tsr-uboard-balance-main-balance-b">/</span> -->
                   <div class="tsr-clear"></div>
                  <span class="tsr-uboard-balance-main-balance-a"><span class="cs-pdr-10">{{$translate['limit']}}:</span><span class="cs-pdr-04">{{number_format(@$userInfo->return->creditLimit,2)}} ₾</span></span> 
                @endif

              </div>
            </div>

              
<script>
	function checkNumber() {
		location.href="http://geocell.ge/{{$lang}}/callcenter?phone=" + $("#phone").val() + "&year=" + $("#year").val() + "&month=" + $("#month").val();
		
		
	}
</script>
         
            <div class="cs-spt-20">
              <div class="tsr-com-linklist">
                <ul class="cs-tx-13 cs-tx-bold"> 
                	@if($userInfo->return->brand==2)
                    <!-- <li><a href="{{$lang}}/dashboard/get-geocredit-info" class="js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['geocredit']}}</a></li>-->
                  @endif  
                  <!--
                	<li><a href="#">Direct debit</a></li> 
                    <li><a href="#">Money transfer</a></li> 
                	-->
                </ul>
              </div>
            </div>
          </div>
           
           @if(isset($check4G->return->resultCode) && $check4G->return->resultCode==1) 
          <div class="tsr-uboard-balance-side">
            <div class="tsr-uboard-balance-side-promo">
              <div class="tsr-uboard-balance-side-image">
                <img src="site/tsr-temp/tsr-images/product-01.png" alt="" height="200">
                @if($check4G->return->isPhone4GCompatibility) 
                <figure class="tsr-tactical-flash tsr-flash-usp-1 tsr-color-purple"><span>4G</span></figure>
                @endif
                <!-- <figure class="tsr-tactical-flash tsr-flash-usp-2 tsr-color-blue"><span>HD<small>Voice</small></span></figure> -->
                <div class="tsr-uboard-balance-side-layer">
                  <div class="cs-dp-table cs-whole">
                    <div class="cs-dp-table-cell">
                      <div class="cs-spb-06">{{$translate['sim_card_comat']}} <span class="cs-tx-12 {{$check4G->return->isSIM4GCompatibility?'cs-cl-turquoise ts-icon-thick ':'cs-cl-pink ts-icon-delete'}}"></span></div>
                      <div class="cs-spb-06">{{$translate['sim_device_comat']}} <span class="cs-tx-20 {{$check4G->return->isPhone4GCompatibility?'cs-cl-turquoise ts-icon-thick ':'cs-cl-pink ts-icon-delete'}}"></span></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tsr-uboard-balance-side-title">{{ $check4G->return->model }}</div>
              <div class="tsr-uboard-balance-side-check"><a href="#" onclick="return false;">{{$translate['check_4g']}}</a></div>
            </div>
          </div>
          @endif

        </div>
      </div>
    </div>
    <!-- /tsr-section-uboard-balance -->


     <!-- tsr-section-attention -->
   <!--  <div class="tsr-section-attention tsr-color-pink">
      <div class="tsr-container">
        <div class="tsr-module-attention">
          <figure class="tsr-module-attention-icon ts-icon-alert"></figure>
          <div class="tsr-module-attention-text"><strong>Alert:</strong> Lorem Ipsum is simply dummy text of the printing and typesetting industry.</div>
          <a href="#" class="tsr-module-attention-close ts-icon-delete" onclick="return false;"></a>
        </div>
      </div>
    </div> -->
    <!-- /tsr-section-attention -->


    <!-- tsr-section-refiner -->
    <div class="tsr-section-refiner">
      <div class="tsr-refiner-header tsr-clearfix">
        <div class="tsr-container">
          <div class="tsr-refiner-links">
            <ul>
             @if(isset($menuItems))
                @foreach($menuItems as $pg)
                    <li><a href="{{$lang.'/'.$pg->full_slug}}" class="{{ Request::is($lang.'/'.$pg->full_slug) ? 'is-choosen' : '' }}">{{$pg->title}}</a></li>
                @endforeach

             @endif 
            </ul>
          </div>
        </div>
      </div>
    </div>

