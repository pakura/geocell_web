@extends('site')

@section('content')
<style>
    .tsr-forms input[type='date']{
        display: block;
        font-size: 15px;
        line-height: 21px;
        padding: 9px;
        width: 100%;
        height: 41px;
        -webkit-border-radius: 3px;
        -ms-border-radius: 3px;
        border-radius: 3px;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        color: #888888;
        background-color: #ffffff;
        border: 1px solid #dbdbdb;
    }
    .tsr-forms input[type='date']:focus{
        outline: none;
        color: #000000;
        background-color: #ffffff;
        border: 1px solid #642887;
    }
</style>

        <!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-empty"></div>
<!-- /tsr-section-divider -->

<!-- tsr-section-generic -->
<div class="tsr-section-generic">
    <div class="tsr-container">
        <h2>{{$translate['installment']}} - Iphone 6s</h2>
        <br><br>
        <form action="{{$lang}}/private/online-shop/phones/installment-form/check" method="post" class="tsr-forms">
            <input type="hidden" name="sku" value="CELL0050">
            <table style="width: 100%">
                <tr>
                    <td>
                        <label for="firstname">სახელი:</label>
                        <input type="text" name="firstname" required placeholder="სახელი" style="width: 95%"/>
                    </td><td>
                        <label for="firstname">გვარი:</label>
                        <input type="text" name="lastname" required placeholder="გვარი" style="width: 98%"/>
                    </td>
                </tr>
            </table>

            <br><br>

            <table style="width: 100%">
                <tr>
                    <td>
                        <label for="pid">პირადი ნომერი (11 ციფრიანი ნომერი):</label>
                        <input type="text" name="pid" required placeholder="პირადი ნომერი" style="width: 95%"/>
                    </td><td>
                        <label for="pidr">მოწმობის გამცემი ორგანო:</label>
                        <input type="text" name="pidr" required placeholder="მოწმობის გამცემი ორგანო" value="იუტიციის სამინისტრო" style="width: 98%"/>
                    </td>
                </tr>
            </table>

            <br><br>

            <table style="width: 100%">
                <tr>
                    <td>
                        <label for="birthday">დაბადების თარიღი:</label>
                        <input type="date" name="birthday" required style="width: 95%"/>
                    </td><td style="line-height: 25px">
                        <label>სქესი:</label><br>
                        <input type="radio" name="gender" checked value="2" id="male"> <label for="male">მამრობითი</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" name="gender" value="1" id="female"> <label for="female">მდედრობითი</label>
                    </td>
                </tr>
            </table>

            <br><br>

            <table style="width: 100%">
                <tr>
                    <td>
                        <label for="hphone">ტელეფონი (მაგ: 32XXXXXX):</label>
                        <input type="text" name="hphone" required placeholder="32xxxxxx" style="width: 95%"/>
                    </td><td>
                        <label for="mhphone">მობილური (მაგ: 577XXXXXX):</label>
                        <input type="text" name="mhphone" required placeholder="577XXXXXX" style="width: 98%"/>
                    </td>
                </tr>
            </table>

            <br><br>

            <table style="width: 100%">
                <tr>
                    <td>
                        <label for="address1">ფაქტობრივი მისამართი:</label>
                        <input type="text" name="address1" required placeholder="ფაქტობრივი მისამართი" style="width: 95%"/>
                    </td><td>
                        <label for="address2">რეგისტრაციის მისამართი:</label>
                        <input type="text" name="address2" required placeholder="რეგისტრაციის მისამართი" style="width: 98%"/>
                    </td>
                </tr>
            </table>

            <br><br>

            <table style="width: 100%">
                <tr>
                    <td>
                        <label for="company">ორგანიზაციის დასახელება:</label>
                        <input type="text" name="company" required placeholder="ფაქტობრივი მისამართი" style="width: 95%"/>
                    </td><td>
                        <label for="position">თანამდებობა:</label>
                        <input type="text" name="position" required placeholder="რეგისტრაციის მისამართი" style="width: 98%"/>
                    </td>
                </tr>
            </table>

            <br><br>

            <table style="width: 100%">
                <tr>
                    <td>
                        <label for="companyphone">სამსახურის ტელეფონი (მაგ: 32XXXXXX):</label>
                        <input type="text" name="companyphone" required placeholder="სამსახურის ტელეფონი" style="width: 95%"/>
                    </td><td>
                        <label for="salary">ყოველთვიური ხელფასი ლარებში
                            (შეიყვანეთ მხოლოდ ციფრები):</label>
                        <input type="text" name="salary" required placeholder="ყოველთვიური ხელფასი ლარებში" style="width: 98%"/>
                    </td>
                </tr>
            </table>
            <br><br>
            <hr>
            <br><br>
            <table style="width: 100%">
                <tr>
                    <td>
                        <label for="item">შესაძენი ნივთი:</label>
                        <input type="text" name="item" required placeholder="შესაძენი ნივთი" style="width: 95%"/>
                    </td>
                </tr>
            </table>

            <br><br>
            <table style="width: 100%">
                <tr>
                    <td>
                        <label for="value">ღირებულობა (ლარი):</label>
                        <input type="text" name="value" readonly value="2300" style="width: 95%"/>
                    </td>
                    <td>
                        <label for="loanvalue">სესხის თანხა (ლარი):</label>
                        <input type="text" name="loanvalue" readonly value="2250" style="width: 95%"/>
                    </td>
                    <td>
                        <label for="monthlyvalue">გადასახადი ყოველთვიურად (ლარი):</label>
                        <input type="text" name="monthlyvalue" readonly value="230" style="width: 95%"/>
                    </td>
                    <td>
                        <label for="loantime">სესხის ვადა (თვე):</label>
                        <input type="text" name="loantime" readonly value="12" style="width: 95%"/>
                    </td>
                </tr>
            </table>
            <br><br>
            <hr>
            <br><br>
            <h3>საკონტაქტო პირები</h3>
            <br>
            <h4>ოჯახის წევრი</h4>
            <br>
            <table style="width: 100%">
                <tr>
                    <td>
                        <label for="fname">სახელი:</label>
                        <input type="text" name="fname" required placeholder="სახელი" style="width: 95%"/>
                    </td>
                    <td>
                        <label for="flastname">გვარი:</label>
                        <input type="text" name="flastname" required placeholder="გვარი" style="width: 95%"/>
                    </td>
                    <td>
                        <label for="fphone">ტელეფონი (მაგ: 577XXXXXX)</label>
                        <input type="text" name="fphone" required placeholder="ტელეფონი" style="width: 95%"/>
                    </td>
                </tr>
            </table>

            <br>
            <h4>თანამშრომელი</h4>
            <br>
            <table style="width: 100%">
                <tr>
                    <td>
                        <label for="wname">სახელი:</label>
                        <input type="text" name="wname" required placeholder="სახელი" style="width: 95%"/>
                    </td>
                    <td>
                        <label for="wlastname">გვარი:</label>
                        <input type="text" name="wlastname" required placeholder="გვარი" style="width: 95%"/>
                    </td>
                    <td>
                        <label for="wphone">ტელეფონი (მაგ: 577XXXXXX)</label>
                        <input type="text" name="wphone" required placeholder="ტელეფონი" style="width: 95%"/>
                    </td>
                </tr>
            </table>

            <br><br>
            <hr>
            <br>
            <div class="g-recaptcha" data-sitekey="6Lc8JhkTAAAAAFXWF-Xj739aJihUGCvYK2E7Lg_8"></div>
            <br>


            <input type="submit" value="განაცხადის გაგზავნა" class="tsr-lightbox-layout-a-button tsr-btn tsr-btn-turquoise" style="float: right">
            <br><br>
            <br><br>
        </form>
    </div>
</div>



<!-- *************** JAVASCRIPTS *************** -->
<!--[if lt IE 9]>
<script src="site/tsr-core/tsr-scripts/jquery/jquery-legacy.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script src="site/tsr-core/tsr-scripts/jquery/jquery.min.js"></script>
<!--<![endif]-->
<script src="site/tsr-core/tsr-scripts/jquery-easing/jquery.easing.min.js"></script>
<script src="site/tsr-core/tsr-scripts/jquery-placeholder/jquery.placeholder.js"></script>
<script src="site/tsr-core/tsr-scripts/jquery-fancybox/jquery.fancybox.patched.min.js"></script>
<script src="site/tsr-core/tsr-scripts/jquery-slideshow/jquery.slideshow.min.js"></script>
<script src="site/tsr-core/tsr-scripts/jquery-tooltipster/jquery.tooltipster.patched.min.js"></script>

<script src="site/tsr-core/tsr-scripts/jquery-ui/jquery-ui.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>






@endsection