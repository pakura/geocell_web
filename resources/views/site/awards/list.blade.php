@extends('site')

@section('content')
   
   
    <!-- tsr-section-generic -->
    <div class="tsr-section-generic tsr-section-generic-bd-bot cs-bg-shade cs-smt-02">
      <div class="tsr-module-bcombo-b">
        <div class="tsr-module-bcombo-back"><div class="tsr-module-bcombo-back-01"></div></div>
        <div class="tsr-module-bcombo-fore">
          <div class="tsr-container">
            <div class="tsr-section-generic-pad-h cs-pdb-40">
              <div class="tsr-module-bcombo-fore-01">
                <h1 class="tsr-title cs-cl-blue">{{$item->title}}</h1>
                <div>{!! $item->content !!}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

   <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <div class="tsr-com-collapser-outer">
            <div class="tsr-com-collapser-inner">

              <div class="tsr-com-label-list">
                <ul>
                 @foreach ($awards as $award)
                  <li>
                    <div class="tsr-com-label-list-title">{{$award->title}}</div>
                    <div>
                      {!! $award->content !!}
                    </div>
                  </li>
                  @endforeach
                </ul>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->
  


@endsection