<!--tsr-section-footer -->
    <div class="tsr-section-footer">

      <!-- tsr-footer-links-opener -->
      <div class="tsr-footer-nav-trigger ts-icon-menu"></div>
      <!-- /tsr-footer-links-opener -->

      <!-- tsr-footer-links -->
      <div class="tsr-footer-nav">
        <div class="tsr-container">
          <div class="tsr-nav-level-01">
            <ul>

              @if(isset($pages[@$parentid]))
                @foreach ($pages[$parentid] as $subPage)
                  @if($subPage->id==22) <?php continue;?> @endif
                  <li>
                    <a href="{{($subPage->id==22 || !array_key_exists($subPage->id,$pages))?$lang.'/'.$subPage->full_slug:'javascript:'}}" @if (\Request::segment(3)==$subPage->slug) class="is-choosen" @endif >
                      {{$subPage->title}}
                    </a>

                    @if(array_key_exists($subPage->id,$pages) && $subPage->id!=22)
                      <div class="tsr-nav-level-02">
                        <ul>
                            @foreach ($pages[$subPage->id] as $child)
                              @if($child->title)
                              <li>
                                <a href="{{$lang.'/'.$child->full_slug}}">{{$child->title}}</a>
                                
                           
                              </li>
                              @endif
                            @endforeach
                        </ul>
                         
                      </div>  
                    @endif
                  </li>

                @endforeach
              @endif

            
            </ul>
          </div>
        </div>
      </div>
      <!-- /tsr-footer-links -->

      <!-- tsr-footer-global -->
      <div class="tsr-footer-global">
        <div class="tsr-container tsr-clearfix">
          <div class="tsr-footer-social tsr-social-icons">
            <span>{{$translate['join_us']}}:</span>

            @if(isset($settings['facebook']) )<a href="{{$settings['facebook'][0]['value']}}" class="tsr-footer-icon-facebook" target="_blank"></a>@endif
            @if(isset($settings['youtube']) )<a href="{{$settings['youtube'][0]['value']}}" class="tsr-footer-icon-youtube"  target="_blank"></a>@endif
            @if(isset($settings['twitter']) )<a href="{{$settings['twitter'][0]['value']}}" class="tsr-footer-icon-twitter"  target="_blank"></a>@endif
            @if(isset($settings['google.plus']) )<a href="{{$settings['google.plus'][0]['value']}}" class="tsr-footer-icon-google" target="_blank"></a>@endif
            @if(isset($settings['instagram']) )<a href="{{$settings['instagram'][0]['value']}}" class="tsr-footer-icon-instagram" target="_blank"></a>@endif
            @if(isset($settings['linkedin']) )<a href="{{$settings['linkedin'][0]['value']}}" class="tsr-footer-icon-linkedin" target="_blank"></a>@endif
          </div>
          <div class="tsr-footer-copyright">
          	{{$translate['copyright1']}}
            <!--Copyright &copy; 2014 «Geocell&nbsp;LLC». All&nbsp;rights&nbsp;reserved.-->
          </div>
        </div>
      </div>
      <!-- /tsr-footer-copyright -->

      <a href="#to-top" class="tsr-btn-toTop">Back to top</a>

    </div>
    <!-- tsr-section-footer