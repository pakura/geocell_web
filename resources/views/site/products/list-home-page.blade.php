 @if(count($products)>0) 

   <!-- tsr-section-divider -->
  <div class="tsr-section-divider">
    <div class="tsr-container">
      <!--<h1>{{trans('site.products')}}  </h1>-->
       
       <h1>{{$translate['products']}}</h1>
    </div>
  </div>
  <!-- /tsr-section-divider -->
 <div class="tsr-section-ps-listing tsr-color-shade">
      <div class="tsr-container">
        <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">
          
        @foreach($products as $product)	
          <div class="tsr-boxcol-outer">
            <div class="tsr-boxcol-inner">
              <a href="{{$lang.'/'.$product->page_slug.'/'.$product->slug}}" class="tsr-module-product">
                <div class="tsr-clearfix">
                 <!--  <figure class="tsr-tactical-ribbon"><span>New</span></figure> -->
                  <figure class="tsr-module-product-image">
                    <img src="uploads/products/{{$product->file}}" alt="" />
                   
                     @if(isset($options[$product->product_id][1]))
                        @foreach($options[$product->product_id][1] as $index=>$option)
                          <?php  $title = explode(' ',$option->title);   ?> 
                          <figure class="tsr-tactical-flash tsr-flash-usp-{{$index+1}} tsr-color-{{$option->color}}">
                              <span>{{isset($title[0])?$title[0]:''}}<small>{{isset($title[1])?$title[1]:''}}</small></span>
                          </figure>
                        @endforeach  
                      <!-- <figure class="tsr-tactical-flash tsr-flash-usp-2 tsr-color-blue"><span>HD<small>Voice</small></span></figure>
                      <figure class="tsr-tactical-flash tsr-flash-usp-3 tsr-color-pink"><span>A6<small>Chip</small></span></figure> -->
                      @endif
                  </figure>
                  <div class="tsr-module-product-desc">
                    <header class="tsr-module-product-title">{{$product->title}}</header>
                    <ul class="tsr-module-product-colors">
                       @if(isset($options[$product->product_id][2]))
                          @foreach($options[$product->product_id][2] as $opt)
                            <li style="background:{{$opt->color}}; border: 1px solid #DBDBDB"  title="{{$opt->title}}"></li>
                          @endforeach  

                        @endif  
                    </ul>
                   <!--  <span class="tsr-module-product-intro">Lorem ipsum dolor amet sit dolor ipsum sit</span>
                    <span class="tsr-module-product-price">Price fr. 12 $/month</span>
                    <span class="tsr-module-product-print">Total cost 299 $</span> -->
                  </div>
                </div>
              </a>
            </div>
          </div>
        @endforeach

      </div>
  </div>
  <a href="{{$lang.'/'.$product->page_slug}}" 
  		  
  		  class="tsr-btn-view-all"><span>
			<!--{{trans('site.viewallproduct')}}-->
  		  	  {{$translate['viewallproduct']}}
  		  <i>({{$productItemsCount}})</i></span></a>
</div>
<!-- /tsr-section-ps-listing -->
@endif