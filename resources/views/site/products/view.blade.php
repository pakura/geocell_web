@extends('site')

@section('content')

    <!-- tsr-section-product-inside -->
    <div class="tsr-section-product-present tsr-forms">
        <div class="tsr-container">
            <div class="tsr-section-flows">
                <div class="tsr-section-flow-a-ct">
                    <h1 class="tsr-title cs-spb-20">{{$product->title}}</h1>
                    <div class="cs-spb-20"> {!! $product->description !!} </div>
                    <div class="cs-spb-20 tsr-section-flow-a-lt">
                        <figure class="tsr-section-image">
                            <img src="uploads/products/{{$product->file}}" alt="" id="poster"/>

                            @if(isset($options[1]))
                                @foreach($options[1] as $index=>$option)
                                    <?php  $title = explode(' ',$option->title);   ?>
                                    <figure style="right:30px;" class="tsr-tactical-flash tsr-flash-usp-{{$index+1}} tsr-color-{{$option->color}}">
                                        <span>{{isset($title[0])?$title[0]:''}}<small>{{isset($title[1])?$title[1]:''}}</small></span>
                                    </figure>
                                @endforeach
                            <!-- <figure class="tsr-tactical-flash tsr-flash-usp-2 tsr-color-blue"><span>HD<small>Voice</small></span></figure>
                <figure class="tsr-tactical-flash tsr-flash-usp-3 tsr-color-pink"><span>A6<small>Chip</small></span></figure> -->
                            @endif
                        </figure>

                        @if(count($photos))<div class="tsr-section-image-link"><a href="#product-more-pictures" class="js-scroll-to">{{$translate['more_pictures']}}</a></div>@endif

                    </div>
                    @if(isset($options[3]))
                        <div class="cs-spb-20">
                            <div class="cs-spb-04 cs-tx-bold">{{$translate['choose_storage']}}:</div>
                            <div style="max-width: 200px;">
                                <select name="storage" id="storageS">
                                    @foreach($options[3] as $option)
                                        <option value="{{(int)$option->title}}">{{$option->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif

                    @if(isset($options[2]))
                        <div class="cs-spb-20">
                            <div class="cs-spb-04 cs-tx-bold">{{$translate['choose_color']}}:</div>
                            <div class="cs-spb-10">
                                <div class="tsr-color-select tsr-color-select-processed">
                                    <input type="hidden" value="">
                                    <ul>

                                        @foreach($options[2] as $index=>$option)
                                            <li style="background:{{$option->color}}; border: 1px solid #DBDBDB" class="colorS" data-color="color{{$index+1}}" title="{{$option->title}}" onclick="changeImgColor('{{$option->color}}', {{$product->product_id}}, '{{$option->title}}', this)"></li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="cs-spb-10 tsr-lightbox-layout-a-field color-error" style="width: 200px; display: none"><div class="cc-errorbox">@if($lang == 'en') Please choose a color @else გთხოვთ აირჩიოთ ფერი @endif</div></div>
                            </div>
                        </div>
                    @endif

                    @if($product->value && $product->slug !='iphone-7-test-link' && $product->slug != 'apple-iphone-7' && $product->slug != 'apple-iphone-7-plus')
                    <!-- <div class="cs-spb-20 cs-spt-40 cs-tx-20 cs-tx-bold cs-cl-blue">{{number_format($product->value,2)}} {{$translate['gel']}}</div> -->
                        @foreach($prices as $price)
                            <div data-id="price_for_{{$price->title_en}}" class="total_price cs-spb-20 cs-spt-40 cs-tx-20 cs-tx-bold cs-cl-blue" style="{{$price->type!=1?'display:none':''}}">{{number_format($price->value,2)}} {{$translate['gel']}}</div>
                        @endforeach
                    @endif


                <!--  -->
                    @if($product->collection_id==5 && isset($options[4]))

                        <div class="cs-spb-20 tsr-section-flow-b-gt">

                            <div class="tsr-module-tabular-b">
                                <div class="tsr-module-tabular-header">
                                    <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab1">{{$translate['with_inst']}}</a>
                                    @if($product->value)<a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab2">{{$translate['regular']}}</a>@endif
                                </div>
                                <div class="tsr-module-tabular-footer tsr-has-border">
                                    <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
                                        <div class="tsr-module-tabular-tab-pad">


                                            <div class="cs-spb-40">
                                                <div class="cs-spb-04 cs-tx-bold">{{$translate['inst_period']}}</div>
                                                <?php $month_periods = []; ?>
                                                @foreach($options[4] as $index=>$inst)
                                                    <?php

                                                    $titles = explode('|', $inst->initial);
                                                    if(isset($titles[1])){ $month_periods[] = (int)$titles[1]; }
                                                    ?>
                                                @endforeach
                                                <?php
                                                $month_periods = array_unique($month_periods);
                                                if(isset($month_periods[1])){
                                                    $month1 = $month_periods[0];
                                                    $month2 = $month_periods[1];
                                                    if($month1 > $month2){
                                                        $month_periods = array();
                                                        $month_periods[] = $month2;
                                                        $month_periods[] = $month1;

                                                    }
                                                }
                                                ?>

                                                @foreach($month_periods as $idx => $month)
                                                    <div class="cs-spb-04" id="month_{{$month}}"><label class="cc-label-cbox monthS" for="my-instalment-radio-{{$idx+1}}" ><input id="my-instalment-radio-{{$idx+1}}" type="radio" name="my-instalment-radio" {{$idx==0?'checked':''}}  data-month="{{$month}}" value="{{$month}}"><span class="cs-spl-04 cs-tx-normal" >{{$month}} {{$translate['month']}}</span></label></div>
                                                @endforeach

                                            </div>

                                            <div class="cs-spb-40">
                                                <div class="cs-spb-04 cs-tx-bold">{{$translate['desirable_offer']}}</div>

                                                @foreach($options[4] as $index=>$inst)
                                                    <?php
                                                    //dd($inst->initial);

                                                    $titles = explode('|', $inst->initial);
                                                    (int)$storage = isset($titles[0])?(int)$titles[0]:0;
                                                    (int)$month = isset($titles[1])?(int)$titles[1]:0;
                                                    ?>

                                                    <div  class="cs-spb-04 inst-offer" style="{{($month!=12 || (isset($options[3]) && $storage!=(int)$options[3][0]->title) )?'display:none;':''}}"  data-month="{{$month}}" data-storage="{{$storage}}" data-sku="{{$inst->sku_code}}">
                                                        <label class="cc-label-cbox" for="my-offer-radio-{{$index+1}}">
                                                            <input id="my-offer-radio-{{$index+1}}" type="radio" name="my-offer-radio" {{$index==0?'checked':''}} data-text="{{$inst->value}} {{$translate['gel']}} / {{$translate['month']}}" data-bonus="+ {{$inst->title}}">
                                                            <span class="cs-spl-04 cs-tx-normal">{{$inst->title}}  </span>
                                                        </label>
                                                    </div>

                                                @endforeach

                                                @foreach($options[4] as $index=>$inst)
                                                    @if($inst->description)
                                                        <div class="cs-dp-none">
                                                            <div data-tooltip-for="my-offer-radio-{{$index+1}}">
                                                                {!! $inst->description !!}
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach



                                            </div>
                                            <div class="cs-spb-20">
                                                <div class="cs-tx-20 cs-tx-bold cs-cl-blue" id="price_txt">{{$options[4][0]->value}} {{$translate['gel']}} / {{$translate['month']}}</div>
                                                <div class="cs-tx-bold" id="bonus_txt" >+{{$options[4][0]->title}}</div>
                                            </div>
                                            @if($preorder == false)
                                                <div><a href="#" id="fill_the_form" data-sku="{{$options[4][0]->sku_code}}" style="{{(!$options[4][0]->sku_code)?'display:none':''}}" data-month="12" class="tsr-btn">{{$translate['fill_the_form']}}</a></div>
                                            @else
                                                <div style="text-align: center"><a onclick="preOrder('{{$product->slug}}', 0)" class="tsr-btn">{{$translate['order']}}</a></div>
                                            @endif

                                        </div>

                                    </div>



                                    @if($product->value)
                                        <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
                                            <div class="tsr-module-tabular-tab-pad">
                                                <div class="cs-spb-40">
                                                    <div>{{$translate['reg_price_text']}}</div>
                                                </div>
                                                <div class="cs-spb-20">

                                                    @foreach($prices as $price)
                                                        <div data-id="price_for_{{$price->title_en}}" class="total_price cs-tx-20 cs-tx-bold cs-cl-blue" style="{{$price->type!=1?'display:none':''}}">{{number_format($price->value,2)}} {{$translate['gel']}}</div>
                                                    @endforeach
                                                </div>
                                            <!--  @if($preorder == false)
                                                <div><a href="#"  data-sku="{{$options[4][0]->sku_code}}" style="{{(!$options[4][0]->sku_code)?'display:none':''}}" data-month="12" class="tsr-btn">{{$translate['fill_the_form']}}</a></div>
                                                    @else
                                                <div style="text-align: center"><a onclick="preOrder('{{$product->slug}}', 1)" class="tsr-btn">{{$translate['order']}}</a></div>
                                                @endif -->
                                                <!--  <div><a href="#" class="tsr-btn">Add to cart</a></div> -->
                                            </div>
                                        </div>
                                    @endif

                                </div>
                            </div>

                        </div>




                    @endif
                    <div class="cs-spt-40 cs-spb-20">
                        <div class="fb-like" data-href="{{config('app.url').$lang.'/'.$item->full_slug.'/'.$product->slug}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /tsr-section-product-inside -->


    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    @if($product->services_and_features || $product->terms_and_conditions)
        <!-- tsr-section-divider -->
        <div class="tsr-section-divider tsr-color-purple">
            <div class="tsr-container">
                <h2>{{$translate['more_info']}}</h2>
            </div>
        </div>
        <!-- /tsr-section-divider -->

        <!-- tsr-section-generic -->
        <div class="tsr-section-generic">
            <div class="tsr-module-tabular-a">
                <div class="tsr-module-tabular-header">
                    <div class="tsr-container">
                        @if($product->brand_id==1 && $product->content!='')
                            <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab3">{{$translate['about_product']}}</a>
                        @endif
                        <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab1">{{$translate['technical_info']}}</a>
                        <a href="#" class="tsr-module-tabular-but is-selected" data-tabular-exp-target="tab2" id="tab2">{{$translate['product_features']}}</a>
                    </div>
                </div>
                <div class="tsr-module-tabular-footer">
                    @if($product->brand_id==1 && $product->content!='')
                        <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab3">
                            <div class="tsr-container">
                                <div class="tsr-section-generic-pad">
                                    {!! $product->content !!}
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
                        <div class="tsr-container">
                            <div class="tsr-section-generic-pad" id="table">
                                {!! $product->terms_and_conditions !!}
                            </div>
                        </div>
                    </div>
                    <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
                        <div class="tsr-container">
                            <div class="tsr-section-generic-pad">
                                {!! $product->services_and_features !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /tsr-section-generic -->

    @endif

    @if(count($photos))
        <!-- tsr-section-divider -->
        <div class="tsr-section-divider tsr-color-purple" id="product-more-pictures">
            <div class="tsr-container" id="photosss">
                <h2>{{$translate['more_pictures']}}</h2>
            </div>
        </div>
        <!-- /tsr-section-divider -->

        <!-- tsr-section-slideshow -->
        <div class="tsr-section-slideshow">
            <div class="tsr-containero">
                <div class="tsr-slideshow tsr-slideshow-30x10 js-slideshow-type-usual js-slideshow-has-nav">
                    <div class="tsr-slideshow-stage">
                        <ul>
                            @foreach($photos as $photo)
                                <li><div class="tsr-slideshow-image"><img src="uploads/products/{{$photo->file}}" alt="" /></div></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- /tsr-section-slideshow -->
    @endif
    <script src="site/tsr-core/tsr-scripts/tsr-core.js"></script>
    <script src="site/tsr-components/tsr-forms/tsr-forms.js"></script>
    <script src="site/tsr-sections/tsr-header/tsr-header.js"></script>
    <script src="site/tsr-sections/tsr-footer/tsr-footer.js"></script>
    <script src="site/tsr-sections/tsr-refiner/tsr-refiner.js"></script>
    <script src="site/tsr-sections/tsr-support-listing/tsr-support-listing.js"></script>

    <script src="site/tsr-components/tsr-color-select/tsr-color-select.js"></script>
    <script src="site/tsr-core/tsr-scripts/jquery-tooltipster/jquery.tooltipster.patched.min.js"></script>
    <script type="text/javascript">/*<![CDATA[*/
        $(document).ready(function() {
            console.log(<?= $product->id ?>);
            var actStorage;
            $("input[name='my-instalment-radio']").change(function(){
                var month = $(this).data('month');
                actStorage = parseInt($("select[name='storage']").val());
                var isAct=false;
                $('.inst-offer').each(function() {
                    if($(this).data('month')!=month || $(this).data('storage')!=actStorage) {
                        $(this).hide();
                    } else {
                        $(this).show();
                        //console.log(isAct);
                        if(isAct==false) { $('input',this).trigger('click'); }
                        isAct = true;
                    }
                })

            });


            $("input[name='my-offer-radio']").change(function(){
                var price_txt = $(this).data('text');
                var bonus = $(this).data('bonus');
                var sku = $(this).closest('.inst-offer').data('sku');
                var month = $(this).closest('.inst-offer').data('month');

                $("#price_txt").html(price_txt);
                $("#bonus_txt").html(bonus);

                if(sku && month){
                    $("#fill_the_form").attr('data-sku',sku);
                    $("#fill_the_form").attr('data-month',month);
                    $("#fill_the_form").show();
                }
                else{
                    $("#fill_the_form").hide();
                }

            });


            $("select[name='storage']").change(function(){
                var storage = $(this).val();
                var month = parseInt($("input[name='my-instalment-radio']").val());
                var isAct=false;
                var cnt = false;
                $('.inst-offer').each(function() {
                    if($(this).data('storage')!=storage || $(this).data('month')!=month) {
                        $(this).hide();
                    } else {
                        cnt = true;
                        $(this).show();
                        //console.log(isAct);
                        if(isAct==false) {
                            $('input#my-instalment-radio-1').trigger('click');
                            $('input',this).trigger('click');
                        }

                        isAct = true;
                    }


//                    if(cnt == false){
//                        $('#month_'+month).fadeOut();
//                    } else {
//                        $('#month_'+month).fadeIn();
//                    }
                });
                var active24 = false;
                $("input[name='my-instalment-radio']").each(function () {
                    if($(this).data('storage') == actStorage && $(this).data('month') == 24){
                        active24 = true;
                    }
                });

                if(active24 == false){
                    $('#month_24').fadeOut();
                } else {
                    $('#month_24').fadeIn();

                }


                $('.total_price').hide();
                $('.total_price[data-id="price_for_'+storage+'"]').show();

            });

            $('#fill_the_form').click(function(event){
                event.preventDefault();
                var sku = $(this).data('sku');
                var month = $(this).data('month');

                if(sku && month) {

                    var url ="https://old.geocell.ge/?language=<?=$lang;?>&section=private&module=phones3&brand=<?=$product->brand_slug?>&period="+month+"&form=1&id="+sku;
                    // var url = "<?=$lang;?>/private/online-shop/phones/installment-form?month="+month+"&sku="+sku;
                    //console.log(url);
                    window.location = url;
                }

            });


            /* tooltips only METI checkboxes */
            (function() {
                var $checks = $('#my-offer-radio-1, #my-offer-radio-2, #my-offer-radio-3, #my-offer-radio-4');
                function toggleTooltipster(selector, content) {
                    var $check = $(selector);
                    var $label = $check.closest('label');
                    var fblock = function() { return false; };
                    if (content && content.length) {
                        if ($check.attr('data-has-tooltipster') != 'on') {
                            $check.attr('data-has-tooltipster', 'on').on('mouseout', fblock);
                            $label.tooltipster({
                                'theme': 'tooltipster-lighter',
                                'minWidth': 200,
                                'maxWidth': 260,
                                'position': 'left',
                                'content': content,
                                'contentAsHTML': true
                            })
                        }
                    } else {
                        if ($check.attr('data-has-tooltipster') == 'on') {
                            $check.attr('data-has-tooltipster', null).off('mouseout', fblock);
                            $label.tooltipster('destroy').attr('title', null);
                        }
                    }
                };
                function enableTooltips() {
                    $checks.each(function() {
                        toggleTooltipster(this, $('[data-tooltip-for="' + $(this).prop('id') + '"]').html());
                    });
                };
                function disableTooltips() {
                    $checks.each(function() {
                        toggleTooltipster(this, null);
                    });
                };
                enquire.register("screen and (max-width: 800px)", {
                    setup   : function() { enableTooltips();  },
                    match   : function() { disableTooltips(); },
                    unmatch : function() { enableTooltips();  }
                });
            })();
            @if($product->id == 226 || $product->id == 225)
              setTimeout(function () {
                $('#tab2').click();
            },1500);
            @endif

        });


        var selectedColor = '';
        var iphonecolor = '';
        function changeImgColor(color, id, name, obj){
            selectedColor = name;
            $('.color-error').fadeOut();
            $('.colorS').css({'box-shadow': 'none'});
            $(obj).css({'box-shadow': '0 0px 12px -1px #000'})
            color = color.replace('#', '');
            iphonecolor = color;
            $.ajax({
                method: "get",
                url: "/ge/products/"+id+"/"+color

            })
                    .done(function( msg ) {
                        if (msg != ''){
                            $('#poster').attr( "src", "uploads/products/"+msg );
                        }

                    });
        }


        function preOrder(iphone, orderType){
            var storage = $('#storageS').val();
            if($('#my-instalment-radio-1').is(':checked')){
                var month = 12;
            } else {
                var month = 24;
            }
            if($('#my-offer-radio-6').is(':checked') || $('#my-offer-radio-5').is(':checked') || $('#my-offer-radio-7').is(':checked') || $('#my-offer-radio-9').is(':checked') || $('#my-offer-radio-8').is(':checked')){
                var bonus = 'unlimited';
            } else {
                var bonus = '500mb';
            }
            if(selectedColor == ''){
                $('.color-error').fadeIn();
                return false;
            }


            var url = '/{{$lang}}/private/online-shop/phones/pre-order?device='+iphone+'&storage='+storage+'&iphone-color='+selectedColor+'&month='+month+'&bonus='+bonus+'&order='+orderType+'&color-code='+iphonecolor;
            window.location = url;
        }





        /*]]>*/</script>
    <script type="text/javascript">/*<![CDATA[*/
        $(document).ready(function() {

            @if($product->slug == 'iphone-7-test-link' || $product->slug == 'apple-iphone-7' || $product->slug == 'apple-iphone-7-plus')
      document.getElementsByClassName('colorS')[4].style.display = 'none';
            $( "#storageS" ).change(function() {
                if($( "#storageS" ).val() == 32){
                    document.getElementsByClassName('colorS')[4].style.display = 'none';
                } else {
                    document.getElementsByClassName('colorS')[4].style.display = 'inline-block';
                }
            });
            @endif
            /* tooltips only METI checkboxes */
            (function() {
                var $checks = $('#my-metibonus-radio-1, #my-metibonus-radio-2, #my-metibonus-radio-3, #my-metibonus-radio-4');
                function toggleTooltipster(selector, content) {
                    var $check = $(selector);
                    var $label = $check.closest('label');
                    var fblock = function() { return false; };
                    if (content && content.length) {
                        if ($check.attr('data-has-tooltipster') != 'on') {
                            $check.attr('data-has-tooltipster', 'on').on('mouseout', fblock);
                            $label.tooltipster({
                                'theme': 'tooltipster-lighter',
                                'minWidth': 200,
                                'maxWidth': 260,
                                'position': 'left',
                                'content': content,
                                'contentAsHTML': true
                            })
                        }
                    } else {
                        if ($check.attr('data-has-tooltipster') == 'on') {
                            $check.attr('data-has-tooltipster', null).off('mouseout', fblock);
                            $label.tooltipster('destroy').attr('title', null);
                        }
                    }
                };
                function enableTooltips() {
                    $checks.each(function() {
                        toggleTooltipster(this, $('[data-tooltip-for="' + $(this).prop('id') + '"]').html());
                    });
                };
                function disableTooltips() {
                    $checks.each(function() {
                        toggleTooltipster(this, null);
                    });
                };
                enquire.register("screen and (max-width: 800px)", {
                    setup   : function() { enableTooltips();  },
                    match   : function() { disableTooltips(); },
                    unmatch : function() { enableTooltips();  }
                });
            })();


            /* payment-type-switcher */
            (function() {
                var attrAnchor = 'data-payment-type-anchor';
                var attrTarget = 'data-payment-type-target';
                var $anchors = $('[' + attrAnchor + ']');
                var $targets = $('[' + attrTarget + ']');
                var switchTargetOn = function(name) {
                    $targets.each(function() {
                        if ($(this).attr(attrTarget) == name) {
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
                    });
                };
                $anchors.on('change', function(event) {
                    if ($(this).is(':checked')) {
                        switchTargetOn($(this).attr(attrAnchor));
                    }
                });
                switchTargetOn($anchors.filter(':checked').first().attr(attrAnchor));
            })();
            $( "#scrolltopiphone" ).click(function() {
                $('body').animate({ scrollTop: 0 }, 'slow');
            });
        });

        /*]]>*/</script>
    <style>
        #scrolltopiphone{
            cursor:pointer;
            font-size: 18px!important;
            color: #999999!important;
            margin-bottom: 0;
        }

    </style>

@endsection

