 @extends('site')

 @section('content')
<?php
    $brands = array('', 'apple', 'samsung', 'huawei', 'htc', 'coloud', 'marshall', 'urbanears');
?>

     <!-- tsr-section-product-inside -->
    <div class="tsr-section-product-present tsr-forms">
      <div class="tsr-container">
        <div class="tsr-section-flows">
          <div class="tsr-section-flow-b-ct">
            <h1 class="tsr-title cs-spb-20">{{$product->title}}</h1>
            <div class="cs-spb-20">{!! $product->content !!}</div>
            <div class="cs-spb-20 tsr-section-flow-b-lt">
              <figure class="tsr-section-image">
                <img src="uploads/products/{{$product->file}}" alt="" />
              <!--   <figure class="tsr-tactical-flash tsr-flash-usp-1 tsr-color-purple"><span>4G</span></figure>
                <figure class="tsr-tactical-flash tsr-flash-usp-2 tsr-color-blue"><span>HD<small>Voice</small></span></figure>
                <figure class="tsr-tactical-flash tsr-flash-usp-3 tsr-color-pink"><span>A6<small>Chip</small></span></figure> -->
              </figure>
              @if(count($photos))<div class="tsr-section-image-link"><a href="#product-more-pictures" class="js-scroll-to">{{$translate['more_pictures']}}</a></div>@endif
            </div>
            <div class="cs-spb-20 cs-spt-40 cs-tx-20 cs-tx-bold cs-cl-blue">{{$translate['price']}} {{$product->value}} {{$translate['gel']}} <span class="cs-cl-pink">+ bonus 6 GB</span></div>




              @if($product->collection_id==35 && isset($options[4]))

                  <div class="cs-spb-20 tsr-section-flow-b-gt">

                      <div class="tsr-module-tabular-b">
                          <div class="tsr-module-tabular-header">
                              <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab1">{{$translate['with_inst']}}</a>
                              @if($product->value)<a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab2">{{$translate['regular']}}</a>@endif
                          </div>
                          <div class="tsr-module-tabular-footer tsr-has-border">
                              <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
                                  <div class="tsr-module-tabular-tab-pad">


                                      <div class="cs-spb-40">
                                          <div class="cs-spb-04 cs-tx-bold">{{$translate['inst_period']}}</div>
                                          <?php $month_periods = []; ?>
                                          @foreach($options[4] as $index=>$inst)
                                              <?php

                                              $titles = explode('|', $inst->initial);
                                              if(isset($titles[1])){ $month_periods[] = (int)$titles[1]; }
                                              ?>
                                          @endforeach
                                          <?php $month_periods = array_unique($month_periods); ?>

                                          @foreach($month_periods as $idx => $month)
                                              <div class="cs-spb-04" id="month_{{$month}}"><label class="cc-label-cbox monthS" for="my-instalment-radio-{{$idx+1}}" ><input id="my-instalment-radio-{{$idx+1}}" type="radio" name="my-instalment-radio" {{$idx==0?'checked':''}}  data-month="{{$month}}" value="{{$month}}"><span class="cs-spl-04 cs-tx-normal" >{{$month}} {{$translate['month']}}</span></label></div>
                                          @endforeach

                                      </div>


                                      <div class="cs-spb-20">
                                          <div class="cs-tx-20 cs-tx-bold cs-cl-blue" id="price_txt">{{$options[4][0]->value}} {{$translate['gel']}} </div>
                                      </div>

                                      <div><a href="#" id="fill_the_form" data-sku="{{$options[4][0]->sku_code}}" style="{{(!$options[4][0]->sku_code)?'display:none':''}}" data-month="12" class="tsr-btn">{{$translate['fill_the_form']}}</a></div>

                                      @if($product->id == 264)
                                          <div><a onclick="preOrder(1)" class="tsr-btn">{{$translate['order']}}</a></div>
                                      @endif
                                  </div>

                              </div>


                              @if($product->value)
                                  <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
                                      <div class="tsr-module-tabular-tab-pad">
                                          <div class="cs-spb-40">
                                              <div>{{$translate['reg_price_text']}}</div>
                                          </div>
                                          <div class="cs-spb-20">

                                              @foreach($prices as $price)
                                                  <div data-id="price_for_{{$price->title_en}}" class="total_price cs-tx-20 cs-tx-bold cs-cl-blue" style="{{$price->type!=1?'display:none':''}}">{{number_format($price->value,2)}} {{$translate['gel']}}</div>
                                              @endforeach
                                          </div>
                                          @if($product->id == 264)
                                              <div><a onclick="preOrder(2)" class="tsr-btn">{{$translate['order']}}</a></div>
                                              @endif
                                                      <!--  <div><a href="#" class="tsr-btn">Add to cart</a></div> -->
                                      </div>
                                  </div>
                              @endif

                          </div>
                      </div>

                  </div>

              @else

            {!! $product->description !!}
            @endif



            <div class="cs-spt-40 cs-spb-20">
                <div class="fb-like" data-href="{{config('app.url').$lang.'/'.$item->full_slug.'/'.$product->slug}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-product-inside -->



    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-color-purple">
      <div class="tsr-container">
        <h2>{{$translate['more_info']}}</h2>
      </div>
    </div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-module-tabular-a">
        <div class="tsr-module-tabular-header">
          <div class="tsr-container">
            <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab1">{{$translate['technical_info']}}</a>
            <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab2">{{$translate['product_features']}}</a>
          </div>
        </div>
        <div class="tsr-module-tabular-footer">
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad"  id="table">
                {!! $product->terms_and_conditions !!}

              </div>
            </div>
          </div>
          <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
            <div class="tsr-container">
              <div class="tsr-section-generic-pad" >
                 {!! $product->services_and_features !!}

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

  @if(count($photos))
    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-color-purple" id="product-more-pictures">
      <div class="tsr-container">
        <h2>{{$translate['more_pictures']}}</h2>
      </div>
    </div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-slideshow -->
    <div class="tsr-section-slideshow">
      <div class="tsr-containero">
        <div class="tsr-slideshow tsr-slideshow-30x10 js-slideshow-type-usual js-slideshow-has-nav">
          <div class="tsr-slideshow-stage">
            <ul>
              @foreach($photos as $photo)
              <li><div class="tsr-slideshow-image"><img src="uploads/products/{{$photo->file}}" alt="" /></div></li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-slideshow -->
 @endif



<script src="site/tsr-components/tsr-color-select/tsr-color-select.js"></script>
<script src="site/tsr-core/tsr-scripts/jquery-tooltipster/jquery.tooltipster.patched.min.js"></script>
<script type="text/javascript">/*<![CDATA[*/
    $(document).ready(function() {
        console.log(<?= $product->id ?>);
        $("input[name='my-instalment-radio']").change(function(){
            var month = $(this).data('month');
            var actStorage = parseInt($("select[name='storage']").val());
            var isAct=false;
            $('.inst-offer').each(function() {
                if($(this).data('month')!=month || $(this).data('storage')!=actStorage) { $(this).hide(); }
                else {
                    $(this).show();
                    //console.log(isAct);
                    if(isAct==false) { $('input',this).trigger('click'); }
                    isAct = true;
                }
            })

        });

        $("input[name='my-offer-radio']").change(function(){
            var price_txt = $(this).data('text');
            var bonus = $(this).data('bonus');
            var sku = $(this).closest('.inst-offer').data('sku');
            var month = $(this).closest('.inst-offer').data('month');

            $("#price_txt").html(price_txt);
            $("#bonus_txt").html(bonus);

            if(sku && month){
                $("#fill_the_form").attr('data-sku',sku);
                $("#fill_the_form").attr('data-month',month);
                $("#fill_the_form").show();
            }
            else{
                $("#fill_the_form").hide();
            }

        });


        $("select[name='storage']").change(function(){
            var storage = $(this).val();
            var month = parseInt($("input[name='my-instalment-radio']").val());
            var isAct=false;
            $('.inst-offer').each(function() {
                if($(this).data('storage')!=storage || $(this).data('month')!=month) {
                    $(this).hide();
                } else {

                    $(this).show();
                    //console.log(isAct);
                    if(isAct==false) { $('input#my-instalment-radio-1').trigger('click'); $('input',this).trigger('click');}
                    isAct = true;
                }
            })
            @if($product->id == 264)
                if(storage == 64){
                $('#month_24').fadeOut();
            } else {
                $('#month_24').fadeIn();
            }
            @endif
            $('.total_price').hide();
            $('.total_price[data-id="price_for_'+storage+'"]').show();

        });

        $('#fill_the_form').click(function(event){
            event.preventDefault();
            var sku = $(this).data('sku');
            var month = $(this).data('month');

            if(sku && month) {

                var url ="https://old.geocell.ge/?language=<?=$lang;?>&section=private&module=phones3&brand=<?=$brands[$product->brand_id]?>&period="+month+"&form=1&id="+sku;
                // var url = "<?=$lang;?>/private/online-shop/phones/installment-form?month="+month+"&sku="+sku;
                //console.log(url);
                window.location = url;
            }

        });


        /* tooltips only METI checkboxes */
        (function() {
            var $checks = $('#my-offer-radio-1, #my-offer-radio-2, #my-offer-radio-3, #my-offer-radio-4');
            function toggleTooltipster(selector, content) {
                var $check = $(selector);
                var $label = $check.closest('label');
                var fblock = function() { return false; };
                if (content && content.length) {
                    if ($check.attr('data-has-tooltipster') != 'on') {
                        $check.attr('data-has-tooltipster', 'on').on('mouseout', fblock);
                        $label.tooltipster({
                            'theme': 'tooltipster-lighter',
                            'minWidth': 200,
                            'maxWidth': 260,
                            'position': 'left',
                            'content': content,
                            'contentAsHTML': true
                        })
                    }
                } else {
                    if ($check.attr('data-has-tooltipster') == 'on') {
                        $check.attr('data-has-tooltipster', null).off('mouseout', fblock);
                        $label.tooltipster('destroy').attr('title', null);
                    }
                }
            };
            function enableTooltips() {
                $checks.each(function() {
                    toggleTooltipster(this, $('[data-tooltip-for="' + $(this).prop('id') + '"]').html());
                });
            };
            function disableTooltips() {
                $checks.each(function() {
                    toggleTooltipster(this, null);
                });
            };
            enquire.register("screen and (max-width: 800px)", {
                setup   : function() { enableTooltips();  },
                match   : function() { disableTooltips(); },
                unmatch : function() { enableTooltips();  }
            });
        })();
        @if($product->id == 226 || $product->id == 225)
          setTimeout(function () {
            $('#tab2').click();
        },1500);
        @endif

        });



    function changeImgColor(color, id){
        color = color.replace('#', '');
        $.ajax({
                    method: "get",
                    url: "/developer_version/public/ge/products/"+id+"/"+color

                })
                .done(function( msg ) {
                    if (msg != ''){
                        $('#poster').attr( "src", "uploads/products/"+msg );
                    }

                });
    }


    function preOrder(option){
//              ge/private/online-shop/phones/pre-order
        var storage = $('#storageS').val();
        var color = 'none';
        for(var i=0; i<document.getElementsByClassName('colorS').length; i++){
            var ele = document.getElementsByClassName('colorS')[i];
            if($(ele).hasClass('is-choosen')){
                var color = $(ele).attr('title');
            }
        }
        if(option == 2){
            var month = 'none';
            var bonus = 'none';
        } else {
            if($('#my-instalment-radio-1').is(':checked')){
                var month = 12;
            } else {
                var month = 24;
            }
            if($('#my-offer-radio-2').is(':checked')){
                var bonus = 'meti';
            } else {
                var bonus = '500MB';
            }
        }
        var url = '{{$lang}}/private/online-shop/phones/pre-order?storage='+storage+'&color='+color+'&option='+option+'&month='+month+'&bonus='+bonus;
        window.location = url;
    }


    /*]]>*/</script>

@endsection

