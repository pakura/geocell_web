 @extends('site')
 
 @section('content')

 @include('site.textcontent_default')

 @if(count($products)>0)
     <div class="tsr-section-ps-listing" style="border: 0;border-top: 1px solid #dbdbdb;">
         <div class="tsr-container" style="padding: 5px 10px">
             {{$translate['filter']}}:
             <select id="priceSort" class="filterSelect">
                 <option value="-1">{{$translate['price']}}</option>
                 <option value="desc" @if(isset($_GET['price']) && $_GET['price'] == 'desc') selected @endif>@if($lang == 'en')  High to Low @else კლების @endif</option>
                 <option value="asc" @if(isset($_GET['price']) && $_GET['price'] == 'asc') selected @endif>@if($lang == 'en') Low to High @else ზრდის @endif</option>
             </select>
             <select id="brandSort" class="filterSelect" multiple>
                 <option value="-1" @if(!isset($_GET['brand']) || $_GET['brand'] == 'null') selected @endif> @if($lang == 'en') All Brands @else ყველა ბრენდი @endif</option>
                 @foreach($brands as $key => $brand)
                     <option value="{{$brand->id}}" @if(isset($_GET['brand']) && (in_array($brand->id, explode(',', $_GET['brand'] )) || $brand->id == $_GET['brand'] )) selected @endif>{{$brand->generic_title}}</option>
                 @endforeach
             </select>


         </div>
     </div>
 <div class="tsr-section-ps-listing tsr-color-shade">
      <div class="tsr-container">
        <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">
      
        @foreach($products as $product)	
          <div class="tsr-boxcol-outer">
            <div class="tsr-boxcol-inner">
              <a href="{{ $item->full_slug.'/'.$product->slug }}" class="tsr-module-product">
                <div class="tsr-clearfix">
<!--                   <figure class="tsr-tactical-ribbon"><span>New</span></figure>
 -->                  <figure class="tsr-module-product-image">
                    <img src="uploads/products/{{$product->file}}" alt="" />
                        @if($product->ribbon == 1)
                            <figure class="tsr-tactical-flash tsr-flash-usp-1 tsr-color-purple"><span>4G</span></figure>
                        @endif
                   <!--
                    <figure class="tsr-tactical-flash tsr-flash-usp-2 tsr-color-blue"><span>HD<small>Voice</small></span></figure>
                    <figure class="tsr-tactical-flash tsr-flash-usp-3 tsr-color-pink"><span>A6<small>Chip</small></span></figure> -->
                  </figure>
                  <div class="tsr-module-product-desc">
                    <header class="tsr-module-product-title">{{$product->title}}</header>
                   <!--  <ul class="tsr-module-product-colors">
                      <li style="background:#e2e2e2"></li>
                      <li style="background:#d77b72"></li>
                      <li style="background:#f8f07b"></li>
                      <li style="background:#73b5e7"></li>
                      <li style="background:#a5c76c"></li>
                    </ul> -->
<!--                     <span class="tsr-module-product-intro">Lorem ipsum dolor amet sit dolor ipsum sit</span>
 -->                    <span class="tsr-module-product-price">{{$product->value}} {{$translate['gel']}}</span>
<!--                     <span class="tsr-module-product-print">Total cost 299 $</span>
 -->                  </div>
                </div>
              </a>
            </div>
          </div>
        @endforeach

      </div>
  </div>
  
</div>
<!-- /tsr-section-ps-listing -->

 <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad">
          <div class="tsr-pagination tsr-pagination-left">
            <div class="tsr-center">
               {!! str_replace('/?', '?',$products->render()) !!} 
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

@endif
 <script>
     var stopredirect = false;
     $(function () {
         $('#brandSort').SumoSelect();
         $('#priceSort').SumoSelect();
         $('.SumoSelect > .optWrapper.multiple > .options li').first('.opt').find('i').hide();
         $('.SumoSelect > .optWrapper.multiple > .options li').first('.opt').click(function () {
             var url = window.location.href.split('?');
             stopredirect = true;
             window.location.href = url[0]+'<?php if(isset($_GET['price'])) echo '?price='.$_GET['price']; ?>';
         })
     })
 </script>
@endsection

