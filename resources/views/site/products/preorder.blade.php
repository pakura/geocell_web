@extends('site')

@section('content')

    <div class="tsr-section-generic cs-bg-shade cs-smt-02">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h cs-pdb-40">
                <h1 class="tsr-title cs-cl-blue">
                    {{$translate['order']}}
                </h1>
                <div>

                </div>
            </div>
        </div>
    </div>
    <div class="tsr-section-divider tsr-divider-empty"></div>


    <div class="tsr-section-generic">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h">

                <div class="cs-spb-40">
                    <div class="tsr-shopsteps">
                        <div class="tsr-shopsteps-mobile">
                            {{--<div class="cs-spb-04 cs-tx-24">--}}
                                {{--@if($lang == 'en')--}}
                                    {{--Step 2 of 3--}}
                                {{--@else--}}
                                    {{--ნაბიჯი 2 / 3 დან--}}
                                {{--@endif--}}
                            {{--</div>--}}
                            {{--<div class="cs-tx-bold">Provide contact information</div>--}}
                        </div>
                        <div class="tsr-shopsteps-desktop">
                            <div class="tsr-shopsteps-steps tsr-shopsteps-steps-03">
                                <div class="tsr-shopsteps-step tsr-passed">
                                    <div class="tsr-shopsteps-text">{{$translate['choose_device']}}</div>
                                    <div class="tsr-shopsteps-line"></div>
                                </div>
                                <div class="tsr-shopsteps-step tsr-active">
                                    <div class="tsr-shopsteps-text">{{$translate['contact_info']}}</div>
                                    <div class="tsr-shopsteps-line"></div>
                                </div>
                                <div class="tsr-shopsteps-step">
                                    <div class="tsr-shopsteps-text">{{$translate['approval']}}</div>
                                    <div class="tsr-shopsteps-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="cs-spb-40">
                    <div class="cs-al-center cs-mg-center cs-max-whole-x" style="width: 800px;">

                            {!! $item->content !!}

                        </div>

                        <?php

                            function errorMSGEn($arg){
                                switch ($arg){
                                    case 'email':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">This e-mail is already registered</div>';
                                        break;
                                    case 'pid':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">This Personal ID is already registered</div>';
                                        break;
                                    case 'ip':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">Your IP address has reached the limit of registrations</div>';
                                        break;
                                    case 'storage':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">This storage option is not avaiable at the moment</div>';
                                        break;
                                    case 'color':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">This  color option is not avaiable at the moment</div>';
                                        break;
                                    case 'phone':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">This Phone Number is already registered</div>';
                                        break;
                                    case 'captcha':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">An unexpected error has occurred
Please try again</div>';
                                        break;
                                    default:
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">An unexpected error has occurred
Please try again</div>';
                                }
                            }
                            function errorMSGGe($arg){
                                switch ($arg){
                                    case 'email':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">მითითებული ელ-ფოსტა უკვე რეგისტრირებულია</div>';
                                        break;
                                    case 'pid':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">მითითებული პირადი ნომერი უკვე რეგისტრირებულია</div>';
                                        break;
                                    case 'ip':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">თქვენი IP მისამართით რეგისტრაცია უკვე გავლილია</div>';
                                        break;
                                    case 'storage':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">მითითებული მეხსიერების iPhoone აღარ არის</div>';
                                        break;
                                    case 'color':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">მითითებული ფერის მოდელი ამ მომენტისთვის ამოწურულია </div>';
                                        break;
                                    case 'captcha':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">დაფიქსირდა შეცდომა გთხოვთ, სცადეთ თავიდან</div>';
                                        break;
                                    case 'phone':
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">მითითებული ტელეფონის ნომერი უკვე რეგისტრირებულია</div>';
                                        break;
                                    default:
                                        return '<div class="cs-spb-10 cs-al-center" style="background-color: #C76D6D;border-radius: 6px;color: #FFF;padding: 9px;margin-top:10px">დაფიქსირდა შეცდომა გთხოვთ, სცადეთ თავიდან</div>';
                                }
                            }

                            
                            if(isset($_GET['f']) && !empty($_GET['f'])){
                                if($lang == 'en'){
                                    echo errorMSGEn($_GET['f']);
                                } else {
                                    echo errorMSGGe($_GET['f']);
                                }

                            }
                        ?>
                    </div>
                </div>

                <div class="cs-spb-40">
                    <div class="tsr-custom-preorder tsr-clearfix">
                        <div class="tsr-custom-preorder-lt">
                            <img src="site/tsr-components/tsr-custom/images/tsr-preorder-apple-iphone-se-01.jpg" alt="" class="cs-whole-x">
                        </div>
                        <div class="tsr-custom-preorder-rt" id="form">
                            <form action="https://geocell.ge/developer_version/public/{{$lang}}/private/online-shop/phones/pre-order-done" method="get" class="tsr-forms" onsubmit="return chekfield()" >
                                <input type="hidden" name="storage" value="{{isset($_GET['storage'])?$_GET['storage']:''}}">
                                <input type="hidden" name="iphone-color" value="{{isset($_GET['iphone-color'])?$_GET['iphone-color']:''}}">
                                <input type="hidden" name="device" value="{{isset($_GET['device'])?$_GET['device']:''}}">
                                <input type="hidden" name="month" value="{{isset($_GET['month'])?$_GET['month']:''}}">
                                <input type="hidden" name="bonus" value="{{isset($_GET['bonus'])?$_GET['bonus']:''}}">
                                <input type="hidden" name="order" value="{{isset($_GET['order'])?$_GET['order']:''}}">
                                <input type="hidden" name="color-code" value="{{isset($_GET['color-code'])?$_GET['color-code']:''}}">
                                <div class="cs-spb-20">
                                    <div><label class="cc-label">{{$translate['first_name']}}</label></div>
                                    <div id="nameWp"><input type="text" name="name" id="name" class="og-symbols" @if(isset($_GET['name'])) value="{{$_GET['name']}}" @endif ></div>
                                    <div class="cs-spb-10 tsr-lightbox-layout-a-field msisdn-error" id="nameerr" style="margin-top: 4px; display:none"><div class="cc-errorbox">@if($lang == 'en') Please enter a valid format @else გთხოვთ მიუთითოთ სწორი ფორმატით @endif</div></div>
                                </div>
                                <div class="cs-spb-20">
                                    <div><label class="cc-label">{{$translate['last_name']}}</label></div>
                                    <div id="lastnameWp"><input type="text" name="lastname" id="lastname" class="og-symbols"  @if(isset($_GET['lastname'])) value="{{$_GET['lastname']}}" @endif ></div>
                                    <div class="cs-spb-10 tsr-lightbox-layout-a-field msisdn-error" id="lastnameerr" style="margin-top: 4px; display:none"><div class="cc-errorbox">@if($lang == 'en') Please enter a valid format @else გთხოვთ მიუთითოთ სწორი ფორმატით @endif</div></div>
                                </div>
                                <div class="cs-spb-20">
                                    <div><label class="cc-label">{{$translate['phone_number']}}</label></div>
                                    <div id="lastnameWp" @if(isset($_GET['f']) && $_GET['f'] == 'phone') class="invalid" @endif><input type="text" data-inputmask="'mask': '\\599999999'"@if(isset($_GET['phone'])) value="{{$_GET['phone']}}" @endif name="phone" id="phone" ></div>
                                    <div class="cs-spb-10 tsr-lightbox-layout-a-field msisdn-error" id="phoneerr" style="margin-top: 4px; display:none"><div class="cc-errorbox">@if($lang == 'en') Please enter a valid format @else გთხოვთ მიუთითოთ სწორი ფორმატით @endif</div></div>
                                </div>
                                <div class="cs-spb-20">
                                    <div><label class="cc-label">{{$translate['email']}}</label></div>
                                    <div id="emailWp"  @if(isset($_GET['f']) && $_GET['f'] == 'email') class="invalid" @endif><input type="text" name="email" id="email" @if(isset($_GET['email'])) value="{{$_GET['email']}}" @endif ></div>
                                    <div class="cs-spb-10 tsr-lightbox-layout-a-field msisdn-error" id="emailerr" style="margin-top: 4px; display:none"><div class="cc-errorbox">@if($lang == 'en') Please enter a valid format @else გთხოვთ მიუთითოთ სწორი ფორმატით @endif</div></div>
                                </div>
                                <div class="cs-spb-20">
                                    <div><label class="cc-label">{{$translate['personal_id']}}</label></div>
                                    <div id="pidWp" @if(isset($_GET['f']) && $_GET['f'] == 'pid') class="invalid" @endif><input type="text" name="pid" id="pid" @if(isset($_GET['pid'])) value="{{$_GET['pid']}}" @endif  maxlength="11"></div>
                                    <div class="cs-spb-10 tsr-lightbox-layout-a-field msisdn-error" id="piderr" style="margin-top: 4px; display:none"><div class="cc-errorbox">@if($lang == 'en') Please enter a valid format @else გთხოვთ მიუთითოთ სწორი ფორმატით @endif</div></div>
                                </div>
                                <div class="cs-spb-20">
                                    <div><label class="cc-label">{{$translate['city']}}</label></div>
                                    <div id="cityWp" @if(isset($_GET['f']) && $_GET['f'] == 'city') class="invalid" @endif>
                                        <select id="city" name="city" class="no-render" style="width: 100%;height: 41px;border: 1px solid #dbdbdb;border-radius: 3px;" onchange="getffices()" >
                                            @foreach($cities as $key => $city)
                                                <option value="{{$city->id}}">
                                                    @if($lang == 'en')
                                                        {{$city->name_en}}
                                                    @else
                                                        {{$city->name_ge}}
                                                    @endif
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="cs-spb-20">
                                    <div><label class="cc-label">{{$translate['office']}}</label></div>
                                    <div id="officeWP" @if(isset($_GET['f']) && $_GET['f'] == 'office') class="invalid" @endif>
                                        <select id="office" class="no-render" style="width: 100%;height: 41px;border: 1px solid #dbdbdb;border-radius: 3px;" name="office">
                                            @foreach($offices as $key => $office)
                                                <option value="{{$office->id}}">
                                                    @if($lang == 'en')
                                                        {{$office->address_en}}
                                                    @else
                                                        {{$office->address_ge}}
                                                    @endif
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="cs-spb-20">
                                    <div class="g-recaptcha" data-sitekey="6Lc8JhkTAAAAAFXWF-Xj739aJihUGCvYK2E7Lg_8"></div>
                                </div>
                                <div class="cs-spb-40">
                                    <div>
                                        <label class="cc-label-cbox" for="check"></label>
                                            <input type="checkbox" class="cc-processed" name="check" id="check" onclick="checkinput()" checked style="width:19px; height: 19px;">
                                        @if($lang == 'en')
                                            <span class="cs-spl-04">I agree to the
                                                <a href="{{$lang}}/termsncondition" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax">Terms and Conditions</a>
                                            </span>
                                        @else
                                            <span class="cs-spl-04">ვეთანხმები
                                                <a href="{{$lang}}/termsncondition" class="composite js-fancybox fancybox.ajax" data-fancybox-type="ajax" >წესებს და შეთანხმებას</a>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="cs-spb-00">
                                    <div> <input type="submit" class="tsr-btn tsr-btn-turquose tsr-btn-form" id="submit" value="{{$translate['confirm']}}"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
        $(function(){
            var myInput = document.getElementById('name');
            myInput.onpaste = function(e) {
                e.preventDefault();
            }
            var myInput = document.getElementById('lastname');
            myInput.onpaste = function(e) {
                e.preventDefault();
            }
            $("#pid").keypress(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });

            var symbols = {
                a : 'ა',
                A : 'ა',
                b : 'ბ',
                B : 'ბ',
                g : 'გ',
                G : 'გ',
                d : 'დ',
                D : 'დ',
                e : 'ე',
                E : 'ე',
                v : 'ვ',
                V : 'ვ',
                z : 'ზ',
                T : 'თ',
                i : 'ი',
                I : 'ი',
                k : 'კ',
                K : 'კ',
                l : 'ლ',
                L : 'ლ',
                m : 'მ',
                M : 'მ',
                n : 'ნ',
                N : 'ნ',
                o : 'ო',
                O : 'ო',
                p : 'პ',
                P : 'პ',
                J : 'ჟ',
                r : 'რ',
                s : 'ს',
                S : 'ს',
                t : 'ტ',
                u : 'უ',
                U : 'უ',
                f : 'ფ',
                F : 'ფ',
                q : 'ქ',
                Q : 'ქ',
                R : 'ღ',
                y : 'ყ',
                Y : 'ყ',
                S : 'შ',
                C : 'ჩ',
                c : 'ც',
                Z : 'ძ',
                w : 'წ',
                W : 'ჭ',
                x : 'ხ',
                X : 'ხ',
                j : 'ჯ',
                h : 'ჰ',
                H : 'ჰ'
            };

            $(".og-symbols").bind("keypress", function(e){
                if(e.which >= 4300 && e.which <= 4335){
                    return true;
                }
                var s = String.fromCharCode(e.which);
                var cv = $(this).val();
                var ss = symbols[s];
                var n = doGetCaretPosition(this);
                if(ss != undefined && e.ctrlKey == false){

                    cv = cv.substring(0, n) + ss + cv.substring(n);
                    $(this).val(cv);
                    setCaretPosition(this, n+1);

                    return false;

                } else {
                    return false;
                }
            });


        });
        function chekfield(){
            $('.msisdn-error').fadeOut();
            var name = $('#name').val();
            var lastname = $('#lastname').val();
            var phonen = $('#phone').val();
            var email = $('#email').val();
            var pid = $('#pid').val();
            $('#email').css({'border' : '0px'});
            $('#name').css({'border' : '0px'});
            $('#lastname').css({'border' : '0px'});
            $('#pid').css({'border' : '0px'});
            $('#phone').css({'border' : '0px'});
            var returns = true;
            if(name.length < 2){
                $('#name').css({'border' : '1px solid red'});
                $('#nameerr').fadeIn();
                returns = false;
            }
            if(lastname.length < 3){
                $('#lastname').css({'border' : '1px solid red'});
                $('#lastnameerr').fadeIn();
                returns = false;
            }
            if(parseInt(phonen) < 100000000 || phonen == ''){
                $('#phone').css({'border' : '1px solid red'});
                $('#phoneerr').fadeIn();
                returns = false;
            }
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(!regex.test(email)){
                $('#email').css({'border' : '1px solid red'});
                $('#emailerr').fadeIn();
                returns = false;
            }
            if(pid.length < 11){
                $('#pid').css({'border' : '1px solid red'});
                $('#piderr').fadeIn();
                returns = false;
            }


            if(!$( '#check' ).prop( "checked" )){
                $('#submit').prop('disabled', true);
                returns = false;
            }

            return returns;
        }

        function checkinput(){
            if($( '#check' ).prop( "checked" )){
                $('#submit').prop('disabled', false);
            } else {
                $('#submit').prop('disabled', true);
            }
        }

        function getffices() {
            var city = $('#city').val();
            $.ajax({
                method: "GET",
                url: "https://geocell.ge/developer_version/public/ge/getoffices/"+city,
            })
                    .done(function( msg ) {
                        writeffices(JSON.parse(msg));
                    });
        }

        function writeffices(arg) {
            var html = ''
            for(var ii in arg){
                html += '<option value="'+arg[ii].id+'">'+arg[ii].address+'</option>';
            }
            $('#office').html(html);
        }

    </script>

@endsection

