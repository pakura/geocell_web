@extends('site')

@section('content')

    <div class="tsr-section-generic cs-bg-shade cs-smt-02">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h cs-pdb-40">
                <h1 class="tsr-title cs-cl-blue">
                    {{$translate['order']}}
                </h1>

            </div>
        </div>
    </div>
    <div class="tsr-section-divider tsr-divider-empty"></div>






    <div class="tsr-section-generic">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h">

                <div class="cs-spb-40">
                    <div class="tsr-shopsteps">
                        <div class="tsr-shopsteps-mobile">
                            {{--<div class="cs-spb-04 cs-tx-24">--}}
                                {{--@if($lang == 'en')--}}
                                    {{--Step 3 of 3--}}
                                {{--@else--}}
                                    {{--ნაბიჯი 3 / 3 დან--}}
                                {{--@endif--}}
                            {{--</div>--}}
                            {{--<div class="cs-tx-bold">Provide contact information</div>--}}
                        </div>
                        <div class="tsr-shopsteps-desktop">
                            <div class="tsr-shopsteps-steps tsr-shopsteps-steps-03">
                                <div class="tsr-shopsteps-step tsr-passed">
                                    <div class="tsr-shopsteps-text">{{$translate['choose_device']}}</div>
                                    <div class="tsr-shopsteps-line"></div>
                                </div>
                                <div class="tsr-shopsteps-step tsr-passed">
                                    <div class="tsr-shopsteps-text">{{$translate['contact_info']}}</div>
                                    <div class="tsr-shopsteps-line"></div>
                                </div>
                                <div class="tsr-shopsteps-step tsr-active">
                                    <div class="tsr-shopsteps-text">{{$translate['approval']}}</div>
                                    <div class="tsr-shopsteps-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="cs-spb-40">
                    <div class="cs-al-center cs-mg-center cs-max-whole-x" style="width: 800px;">
                        {!! $item->content !!}
                    </div>
                </div>

                <div class="cs-spb-40">
                    <div class="cs-al-center">
                        {{--<a href="http://www.facebook.com/sharer.php?u=https://geocell.ge/developer_version/public/ge/private/online-shop/phones/apple-iphone-se" target="_blank">--}}
                            <img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" id="fbBtn" />
                        {{--</a>--}}
                        {{--<div class="fb-share-button" data-href="https://geocell.ge/developer_version/public/ge/private/online-shop/phones/apple-iphone-se" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fgeocell.ge%2Fdeveloper_version%2Fpublic%2Fge%2Fprivate%2Fonline-shop%2Fphones%2Fapple-iphone-se&amp;src=sdkpreparse">Share</a></div>--}}
                        {{--<div class="fb-share-button" data-href="https://geocell.ge/developer_version/public/ge/private/online-shop/phones/pre-order-done" data-layout="button" data-mobile-iframe="true"></div>--}}
                        {{--<a href="#" class="tsr-com-soc-btn tsr-com-soc-btn-fb"><span class="tsr-com-soc-btn-icon cs-spr-10" data-btn-icon="&#xe002;"></span>Be the first, share on facebook</a>--}}
                    </div>
                </div>

                <div class="cs-spt-60">
                    <div class="cs-al-center"><img src="site/tsr-components/tsr-custom/images/tsr-preorder-apple-iphone-se-02.jpg" alt="" class="cs-max-whole-x" /></div>
                </div>

            </div>
        </div>
    </div>



    <script type="text/javascript">
        $(document).ready(function(){
            $('#fbBtn').click(function(e){
                e.preventDefault();
                FB.ui(
                        {
                            method: 'feed',
                            name: 'My pre-order is approved',
                            link: ' https://geocell.ge/{{$lang}}/private/online-shop/phones/iphone7',
                            picture: 'https://geocell.ge/site/tsr-components/tsr-custom/images/tsr-preorder-apple-iphone-se-02.jpg',
                            caption: 'My pre-order is approved',
                            description: 'Pre-order official iPhone 7 and purchase before others',
                            message: ''
                        });
            });
        });
    </script>


@endsection

