    <!------------------ Only Jeffry Way can judge me :/ ---------------->

    <!-- tsr-section-header -->
    <div class="tsr-section-header">

      <!-- tsr-header-global -->
      <div class="tsr-header-global">
        <div class="tsr-container tsr-clearfix">
          <div class="tsr-global-left">
            <ul>
             <?php $actSlug = \Request::segment(2)?\Request::segment(2) :'private';?>
              @foreach ($pages[0] as $page)
                <?php

                    $blank = '';
                    $href = $lang.'/'.$page->full_slug;
                    $url = parse_url($page->redirect_link);
            
                    if(isset($url["scheme"]) && ($url["scheme"]=="http" || $url["scheme"]=="https")) {
                      $blank = 'target="_blank"'; 
                      $href = $page->redirect_link;
                    }
                ?>
                <li>
                  <a href="{{$href}}" {{$blank}} @if ($actSlug==$page->slug) class="is-choosen" @endif >
                    {{$page->title}}
                  </a>
                </li>

              @endforeach
              
            </ul>
          </div>

          <div class="tsr-global-right">
            <ul>
            @if(\Request::segment(2) != 'business')
              <li class="tsr-header-cell-link"><a href="{{$lang}}/fill-balance/enter-number" data-fancybox-type="ajax" class="js-fancybox fancybox.ajax">{{$translate['instant_payment']}}</a></li>
              <li class="tsr-header-cell-div"></li>
              <!-- <li class="tsr-header-cell-cart"><a href="#">{{$translate['cart']}}<i class="tsr-tactical">3</i></a></li> -->
              @if(!\Session::has('site_auth_token'))
              <li class="tsr-header-cell-login"><a href="{{$lang}}/login" data-fancybox-type="ajax" class="tsr-btn tsr-btn-xsmall js-fancybox fancybox.ajax" >{{$translate['login']}}</a></li>
              @else
              <li class="tsr-header-cell-logout"><a href="#" data-header-exp-target="logout">{{\Session::get('site_user')}}</a></li>
              @endif
            @endif
              <li class="tsr-header-cell-lang"><a href="{!!$lang=='en'?'ge':'en'; !!}{{getUrlForSwitchLang()}}"><img src="site/tsr-sections/tsr-header/flag-{!!$lang=='en'?'ge':'en'; !!}.png" alt="" /></a></li>
            </ul>
          </div>

          @if(isset($banners[80]) && count($banners[80]))
          <div class="tsr-floatmenu">
            <ul class="tsr-floatmenu-gray">
            @foreach ($banners[80] as $flMenu)
              <li><a href="{{$flMenu->url?$flMenu->url : 'javascript:;'}}"><img src="{{$flMenu->photo}}" alt="" /><span>{{$flMenu->title}}</span></a></li>
            @endforeach  
              
            </ul>
              @if(\Request::segment(2) == 'business')
            <ul class="tsr-floatmenu-pink">
                <li>
                    <a href="{{$lang}}/partner/flow" data-handle="fancybox" data-fancybox-width="720" data-fancybox-type="ajax"><img src="https://geocell.ge/developer_version/public/site/tsr-sections/tsr-header/tsr-floatmenu-callme.png" alt=""><span>Call Center</span></a>
                </li>
            </ul>
              @endif
          </div>
          @endif

        </div>
      </div>
      <!-- /tsr-header-global -->

      <!-- tsr-header-main -->
      <div class="tsr-header-main">
        <div class="tsr-container tsr-clearfix">

          <!-- logo -->
        @if(\Request::segment(2) != 'business')
          <div class="tsr-header-logo"><a href="{{config('app.url')}}{{$lang!='ge'?$lang:""}}"><img src="site/tsr-sections/tsr-header/logo-{{$lang}}.png" alt="Geocell" /></a></div>
        @else
          <div class="tsr-header-logo"><a href="{{config('app.url')}}{{$lang!='ge'?$lang:""}}business"><img src="site/tsr-sections/tsr-header/logo-{{$lang}}.png" alt="Geocell" /></a></div>
        @endif
          <!-- top mobile & search -->
          <div class="tsr-header-sidenav">
            <nav>
              <ul>
                <li class="tsr-sidenav-apps"><a href="#" data-header-exp-target="apps"><span>{{$translate['mobile_apps']}}</span></a></li>
                <li class="tsr-sidenav-login"><a href="{{$lang}}/login" class="js-fancybox fancybox.ajax" data-fancybox-type="ajax" data-header-exp-target="login"></a></li>
                <li class="tsr-sidenav-search"><a href="#" data-header-exp-target="search"></a></li>
                <li class="tsr-sidenav-menu"><a href="#" data-header-exp-target="nav"></a></li>
              </ul>
            </nav>
          </div>
          <!-- /top mobile & search -->

          <!-- navigation -->
          <div class="tsr-header-nav">
            <div class="tsr-nav-level-01">
              <ul>
              @if(isset($pages[@$parentid]))
                @foreach ($pages[$parentid] as $subPage)
                  <li>
                    @if($subPage->id==22 && !\Session::get('site_user')) 
                      <a href="{{$lang}}/login" data-fancybox-type="ajax" class="js-fancybox fancybox.ajax" >
                        {{$subPage->title}}
                      </a>
                    @else
                    <a href="{{($subPage->id==22 || !array_key_exists($subPage->id,$pages))?$lang.'/'.$subPage->full_slug:'#'}}" @if (\Request::segment(3)==$subPage->slug) class="is-choosen " @endif >
                      {{$subPage->title}}
                        @if($subPage->id == 69)
                            <img src="/developer_version/public/site/tsr-sections/tsr-header/mm-icon-01.png" alt="" class="tsr-nav-level-01-icon">
                        @endif
                    </a>
                    @endif
                    @if(array_key_exists($subPage->id,$pages) && $subPage->id!=22)
                      <div class="tsr-nav-level-02" id="{{$subPage->id}}">
                        <ul>
                            @foreach ($pages[$subPage->id] as $child)
                              @if($child->title)
{{--26--}}
                                @if($child->id == 70)

                                        <li class="has-landing">
                                            <a href="http://movla.ge/" class="is-landing">
                                                <span>
                                                    @if($lang == 'en')
                                                        #Movla
                                                    @else
                                                        #მოვლა
                                                    @endif
                                                </span>
                                                <div class="tsr-landing">
                                                    <div class="tsr-landing-image">
                                                        <img src="site/tsr-core/tsr-images/menu-landing-{{$lang}}.png" alt="">
                                                    </div>
                                                    <div class="tsr-landing-caption">

                                                        @if($lang == 'en')
                                                            Take care of the Country with the help of Mobile Internet
                                                        @else
                                                            ქვეყნის #მოვლა მობილური ინტერნეტით
                                                        @endif

                                                    </div>
                                                    <div class="tsr-landing-description">
                                                        @if($lang == 'en')
                                                            Useful for People and Environment
                                                        @else
                                                            სასარგებლო ადამიანისთვის და ბუნებისთვის
                                                        @endif
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                @endif

                                  <li id="{{$child->id}}">
                                    <a href="{{$lang.'/'.$child->full_slug}}"> {{$child->title}}  </a>

                                      <div class="tsr-nav-level-03">
                                        <ul>
                                         <?php  $itemCounter = 0;  $totalitems= 0; ?>

                                        @if(array_key_exists($child->id,$pages))
                                                @if($child->id == 26)
                                                     <?php  $itemCounter = 4; ?>
                                                     <li>
                                                         <a href="{{$lang}}/private/services/tariffs/tariff-meti">
                                                             @if($lang == 'en')
                                                                 Bucket METI
                                                             @else
                                                                 პაკეტი „მეტი“
                                                             @endif
                                                         </a>
                                                     </li>
                                                     <li>
                                                         <a href="{{$lang}}/private/services/tariffs/nulomani">
                                                             @if($lang == 'en')
                                                                 Tariff “Nulomani”
                                                             @else
                                                                 ტარიფი „ნულომანი“
                                                             @endif
                                                         </a>
                                                     </li>
                                                     <li>
                                                         <a href="{{$lang}}/private/services/tariffs/tariff-globusi">
                                                             @if($lang == 'en')
                                                                 Tariff Globusi
                                                             @else
                                                                 ტარიფი გლობუსი
                                                             @endif
                                                         </a>
                                                     </li>
                                                     <li>
                                                         <a href="{{$lang}}/private/services/tariffs/twist">
                                                             @if($lang == 'en')
                                                                 Twist - Geocell and Silknet joint offer
                                                             @else
                                                                 ჯეოსელის და სილქნეტის „ტვისტი“
                                                             @endif
                                                         </a>
                                                     </li>
                                                @else
                                                    <?php $totalitems = count($pages[$child->id]); ?>

                                                    @foreach ($pages[$child->id] as $chld)
                                                      <li> <a href="{{$lang.'/'.$chld->full_slug}}">{{$chld->title}}</a></li>
                                                    <?php $itemCounter ++; ?>
                                                    @if($itemCounter==4) <?php break; ?> @endif

                                                    @endforeach
                                                @endif
                                        @endif

                                        @if(isset($products[$child->attached_collection_id]) && $child->attached_collection_id!=94)

                                            <?php

                                                $product = $products[$child->attached_collection_id];

                                                $prodCount = count($product);

                                                $totalitems +=  $prodCount;

                                                $leftNum = 4-$itemCounter;
                                                 $prodCount =   $prodCount < $leftNum ?  $prodCount : $leftNum;
                                              ?>



                                            @for($j=0;$j< $prodCount;$j++)


                                                <li> <a href="{{$lang.'/'.$child->full_slug.'/'.$product[$j]->slug}}">{{$product[$j]->title}} </a></li>

                                            @endfor
                                        @endif

                                            @if($totalitems>4)
                                               <li> <a href="{{$lang.'/'.$child->full_slug}}" class="tsr-nav-etc"> {{$translate['more']}} ... </a></li>

                                            @endif
                                         </ul>
                                        </div><!--level3-->

                                  </li> <!--level2-->
                                @endif
                            @endforeach
                        </ul>
                        <div class="tsr-btn-close"></div>
                      </div>  
                    @endif
                  </li>

                @endforeach
              @endif
              </ul>
            </div>
            <div class="tsr-btn-close"></div>
          </div>
          <!-- /navigation -->

          <!-- apps -->
         <!-- apps -->
          <div class="tsr-header-sub tsr-header-sub-apps" data-header-exp-anchor="apps">
            <div class="tsr-header-apps">
              <div class="tsr-grid">

              @if(isset($banners[84]) && count($banners[84]))
                 @foreach($banners[84] as $app)
                  <div class="tsr-col-00 tsr-col-box cs-spb-20">
                    <div class="tsr-header-app tsr-clearfix">
                      <figure class="tsr-header-app-image"><img src="{{$app->photo}}" alt="" /></figure>
                      <div class="tsr-header-app-desc">
                        <div class="tsr-header-app-name">{{$app->title}}</div>
                         {!!$app->description!!}
                      </div>
                    </div>
                  </div>
                  @endforeach
                @endif

                 </div>
            </div>
            <div class="tsr-btn-close" data-header-exp-target="apps"></div>
          </div>
          <!-- /apps -->
          <!-- /apps -->

          <!-- search -->
          <div class="tsr-header-sub tsr-header-sub-search" data-header-exp-anchor="search">
            <div class="tsr-header-sub-form tsr-forms tsr-clearfix">
              <form action="{{$lang}}/search" method="get">

                <input type="text" value="" name="q" title="{{$translate['search_placeholder_text']}}" class="placeholder">
                <input type="submit" value="{{$translate['search']}}" class="tsr-btn tsr-btn-form">
              </form>
            </div>
            <div class="tsr-btn-close" data-header-exp-target="search"></div>
          </div>
          <!-- /search -->

          @if(\Session::get('site_user'))
           <!-- logout -->
          <div class="tsr-header-sub tsr-header-sub-logout" data-header-exp-anchor="logout">
            <div class="tsr-header-sub-form tsr-forms tsr-clearfix">
              <div class="cs-spb-20 cs-al-center cs-tx-bold">{{$translate['hello']}} {{\Session::get('site_user')}}</div>
              <div class="tsr-header-sub-logout-buttons">
                <div class="cs-spb-10"><a href="{{$lang}}/{{\Site\Models\Pages::getSlugById(23)}}" class="tsr-btn tsr-btn-100 tsr-btn-green">{{$translate['dashboard']}}</a></div>
                <div class="cs-spb-00"><a href="{{$lang}}/logout" class="tsr-btn tsr-btn-100 tsr-btn-gray">{{$translate['logout']}}</a></div>
              </div>
            </div>
            <div class="tsr-btn-close" data-header-exp-target="logout"></div>
          </div>
          <!-- /logout -->
          @endif

        </div>
      </div>
      <!-- tsr-header-main -->

    </div>
    <!-- /tsr-section-header -->
    
    @if(array_key_exists(@$subParentId,$pages) && @$subParentId!=22)
    <!-- tsr-section-submenu -->
    <div class="tsr-section-submenu">
      <div class="tsr-container">
        <nav>
          <ul>
            @foreach ($pages[$subParentId] as $subPage)

              <li>
                <a href="{{$lang.'/'.$subPage->full_slug}}" @if (\Request::segment(4)==$subPage->slug) class="is-choosen" @endif >
                  {{$subPage->title}}
                </a>
              </li>

            @endforeach
          
          </ul>
        </nav>
      </div>
    </div>
    <!-- /tsr-section-submenu -->
    @endif

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-pixup"></div>
    <!-- /tsr-section-divider -->
   
    @if(isset($breadcrumbs) && count($breadcrumbs)>0 && !\Request::is($lang.'/business'))
     <!-- tsr-section-breadcrumbs -->
     
    <div class="tsr-section-breadcrumbs">
      <div class="tsr-container">
        <nav>
          <ul>
            <li><a href="{{config('app.url')}}">{{$translate['home']}}</a></li>
           @foreach($breadcrumbs as $slug=>$title)
             <li><a href="{{$lang}}/{{$slug}}">{{$title}}</a></li>
            @endforeach 
            @if(isset($item) && $item->title!=$title)
            <li><a href="{{$lang}}/{{$item->full_slug}}">{{$item->title}}</a></li>
            @endif
          </ul>
        </nav>
      </div>
    </div>

    <!-- /tsr-section-breadcrumbs -->
    @endif
 
  