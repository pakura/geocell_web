@extends('site')

@section('content')

  @include('site.fraud.head')

  
               <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <div class="cs-spb-10"><h1 class="tsr-title">{{$item->title}}</h1></div>
          <div class="cs-spb-20">{{$item->content}}</div> 

            <div class="cs-spb-20">
   
              <form action="{{$lang}}/fraud-contact/send" method="post" class="tsr-forms" onsubmit="return complexFormAction(this)">
                <div class="errors tsr-clearfix"></div>
                <div class="tsr-grid tsr-clearfix">
                  <div class="tsr-col-06 cs-spb-20">
                    <div><label class="cc-label">{{$translate['cell_number']}}</label></div>
                    <div><input type="text" name="department" value=""></div>
                  </div>
                  <div class="tsr-col-06 cs-spb-20">
                    <div><label class="cc-label">{{$translate['email']}}*</label></div>
                    <div><input type="text" name="email" value=""></div>
                  </div>
                  <div class="tsr-col-12 cs-spb-20">
                    <div><label class="cc-label">{{$translate['issue']}}*</label></div>
                    <div><textarea rows="6" name="comment"></textarea></div>
                  </div>
                  <div class="tsr-col-12 cs-spb-20">
                   <!--  <div><label class="cc-label">Security code*</label></div> -->
                 
                    <div><div class="g-recaptcha" data-sitekey="6LeO3AsTAAAAAP1mbagBkBXXzJjl9ofId_msIzpF"></div></div>
                  </div>
                  <div class="tsr-col-12 cs-spb-20">
                    <div><input type="submit" value="{{$translate['contact_submit']}}" class="tsr-btn tsr-btn-form"></div>
                  </div>
                </div>
              </form>


            </div>

          </div>
        </div>
      </div>
    
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->
 
 

@endsection
