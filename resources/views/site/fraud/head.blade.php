  <!-- tsr-section-generic -->
    <div class="tsr-section-generic cs-bg-shade cs-smt-02">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h cs-pdb-40">
          <h1 class="tsr-title cs-cl-blue">{{$parentPage->title}}</h1>
          <div>{!! $parentPage->content !!}</div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic  -->

  <!-- tsr-section-refiner -->
    <div class="tsr-section-refiner">
      <div class="tsr-refiner-header tsr-clearfix">
        <div class="tsr-container">
          <div class="tsr-refiner-links">
           <ul>
              @foreach($parentPages as $parentpg)
                <li><a href="{{$lang}}/{{$parentpg->full_slug}}" class="{{ Request::is($lang.'/'.$parentpg->full_slug.'*') ? 'is-choosen' : '' }}">{{$parentpg->title}}</a></li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <div class="tsr-com-linklist">
            <ul class="cs-tx-13 cs-tx-bold"> 
               @foreach($siblingPages as $subpg)
                <li><a href="{{$lang}}/{{$subpg->full_slug}}" class="{{ Request::is($lang.'/'.$subpg->full_slug) ? 'is-choosen' : '' }}">{{$subpg->title}}</a></li>
              @endforeach
             </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->



