@extends('site')

@section('content')
    
   @include('site.fraud.head')

     <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <div class="cs-spb-10"><h1 class="tsr-title">{{$item->title}}</h1></div> 
          <div class="tsr-com-collapser-outer">
            <div class="tsr-com-collapser-inner">
            

        @foreach($newsList as $news)
           
            <div class="cs-spb-40">
              <div class="cs-tx-22 cs-cl-primary">{{$news->title}}</div>
              <div class="cs-tx-13 cs-spt-10 cs-cl-gray">{{convertDate($news->creation_date)}}</div>
              <div>{!! html_entity_decode($news->content)  !!}</div>
            </div>

         @endforeach

          </div>
        </div>
      </div>
    </div>
  </div>
    <!-- /tsr-contactus-listing -->
 <!-- tsr-section-generic -->
  
      <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad">
          <div class="tsr-pagination tsr-pagination-left">
            <div class="tsr-center">
              {!! str_replace('/?', '?',$newsList->render()) !!} 
               
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->  

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty tsr-color-white"></div>
    <!-- /tsr-section-divider -->

@endsection
