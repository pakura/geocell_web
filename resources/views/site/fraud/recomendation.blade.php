@extends('site')

@section('content')

     @include('site.fraud.head')

     <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <div class="cs-spb-10"><h1 class="tsr-title">{{$item->title}}</h1></div>
          <div class="tsr-com-collapser-outer">
            <div class="tsr-com-collapser-inner">
            
             @foreach($recoms as $recom)  

              <div class="cs-spb-20">
                <div class="cs-tx-22">{{$recom->title}}</div>
                <div>{!! $recom->content !!}</div>
              </div>
              <hr class="tsr-com-hr cs-spv-30" />

             @endforeach
              
            </div>
          </div>
        </div>
      </div>
    </div>
  
@endsection
