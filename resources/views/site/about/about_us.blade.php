@extends('site')

@section('content')
<!-- tsr-section-generic -->
    <div class="tsr-section-generic tsr-section-generic-bd-bot cs-bg-shade cs-smt-02">

      <div class="tsr-module-bcombo-a">
        <div class="tsr-module-bcombo-back">
          <div class="tsr-module-bcombo-back-01"></div>
        </div>
        <div class="tsr-module-bcombo-fore">
          <div class="tsr-container">
            <div class="tsr-section-generic-pad-h cs-pdb-40">
              <h1 class="tsr-title cs-cl-blue cs-spb-10">{{$item->title}}</h1>

                 {!! $item->content !!}

            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          
          @foreach($aboutPages as $about)
           
                    {!! $about->content !!}
               
          @endforeach 


        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->
@endsection