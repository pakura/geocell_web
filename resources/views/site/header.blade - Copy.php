    <!-- tsr-section-header -->
    <div class="tsr-section-header">

      <!-- tsr-header-global -->
      <div class="tsr-header-global">
        <div class="tsr-container tsr-clearfix">

          <div class="tsr-global-left">
            <ul>
              @foreach ($pages[0] as $page)

                <li>
                  <a href="{{$page->full_slug}}" @if (\Request::segment(2)==$page->slug) class="is-choosen" @endif >
                    {{$page->generic_title}}
                  </a>
                </li>

              @endforeach
              
            </ul>
          </div>

          <div class="tsr-global-right">
            <ul>
              <li class="tsr-header-cell-link"><a href="#">Instant Payment</a></li>
              <li class="tsr-header-cell-cart"><a href="#">Cart<i class="tsr-tactical">3</i></a></li>
              <li class="tsr-header-cell-login"><a href="#" class="tsr-btn tsr-btn-xsmall" data-header-exp-target="login">Login</a></li>
              <li class="tsr-header-cell-lang"><a href="#"><img src="site/tsr-sections/tsr-header/flag-ka.png" alt="" /></a></li>
            </ul>
          </div>

        </div>
      </div>
      <!-- /tsr-header-global -->

      <!-- tsr-header-main -->
      <div class="tsr-header-main">
        <div class="tsr-container tsr-clearfix">

          <!-- logo -->
          <div class="tsr-header-logo"><a href="{{config('app.url')}}"><img src="site/tsr-sections/tsr-header/logo-en.png" alt="Geocell" /></a></div>

          <!-- top mobile & search -->
          <div class="tsr-header-sidenav">
            <nav>
              <ul>
                <li class="tsr-sidenav-apps"><a href="#" data-header-exp-target="apps"><span>Mobile Apps</span></a></li>
                <li class="tsr-sidenav-login"><a href="#" data-header-exp-target="login"></a></li>
                <li class="tsr-sidenav-search"><a href="#" data-header-exp-target="search"></a></li>
                <li class="tsr-sidenav-menu"><a href="#" data-header-exp-target="nav"></a></li>
              </ul>
            </nav>
          </div>
          <!-- /top mobile & search -->

          <!-- navigation -->
          <div class="tsr-header-nav">
            <div class="tsr-nav-level-01">
              <ul>
                @foreach ($pages[$parentid] as $subPage)

                  <li>
                    <a href="{{$subPage->full_slug}}" @if (\Request::segment(3)==$subPage->slug) class="is-choosen" @endif >
                      {{$subPage->generic_title}}
                    </a>
                  </li>

                @endforeach
              
              </ul>
            </div>
            <div class="tsr-btn-close"></div>
          </div>
          <!-- /navigation -->

          <!-- apps -->
          <div class="tsr-header-sub tsr-header-sub-apps" data-header-exp-anchor="apps">
            <div>Not implemented...</div>
            <div class="tsr-btn-close" data-header-exp-target="apps"></div>
          </div>
          <!-- /apps -->

          <!-- search -->
          <div class="tsr-header-sub tsr-header-sub-search" data-header-exp-anchor="search">
            <div class="tsr-header-sub-form tsr-forms tsr-clearfix">
              <form action="#">
                <input type="text" value="" title="Type here your keywords..." class="placeholder">
                <input type="button" value="Search" class="tsr-btn tsr-btn-form">
              </form>
            </div>
            <div class="tsr-btn-close" data-header-exp-target="search"></div>
          </div>
          <!-- /search -->

          <!-- login -->
          <div class="tsr-header-sub tsr-header-sub-login" data-header-exp-anchor="login">
            <div class="tsr-header-sub-form tsr-forms tsr-clearfix">
              <form action="#">
                <div class="tsr-header-sub-form-field">
                  <div class="tsr-header-sub-form-label">Username</div>
                  <div class="tsr-header-sub-form-input"><input type="text"></div>
                </div>
                <div class="tsr-header-sub-form-field">
                  <div class="tsr-header-sub-form-label">Password</div>
                  <div class="tsr-header-sub-form-input"><input type="password"></div>
                </div>
                <div class="tsr-header-sub-form-submit">
                  <input type="button" name="form-button" value="Login" class="tsr-btn tsr-btn-form">
                </div>
              </form>
            </div>
            <div class="tsr-header-sub-links">
              <a href="#">Registration</a><span>|</span><a href="#">Password Recovery</a>
            </div>
            <div class="tsr-btn-close" data-header-exp-target="login"></div>
          </div>
          <!-- /login -->

        </div>
      </div>
      <!-- tsr-header-main -->

    </div>
    <!-- /tsr-section-header -->
    
    @if($pages[$subParentId])
    <!-- tsr-section-submenu -->
    <div class="tsr-section-submenu">
      <div class="tsr-container">
        <nav>
          <ul>
            @foreach ($pages[$subParentId] as $subPage)

              <li>
                <a href="{{$subPage->full_slug}}" @if (\Request::segment(4)==$subPage->slug) class="is-choosen" @endif >
                  {{$subPage->generic_title}}
                </a>
              </li>

            @endforeach
          
          </ul>
        </nav>
      </div>
    </div>
    <!-- /tsr-section-submenu -->
    @endif

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-pixup"></div>
    <!-- /tsr-section-divider -->
 
    @if($breadcrumbs)
    <!-- tsr-section-breadcrumbs -->
    <div class="tsr-section-breadcrumbs">
      <div class="tsr-container">
        <nav>
          <ul>
            <li><a href="#">Home</a></li>
            @foreach($breadcrumbs as $slug=>$title)
             <li><a href="{{$slug}}">{{$title}}</a></li>
            @endforeach
            
          </ul>
        </nav>
      </div>
    </div>
    <!-- /tsr-section-breadcrumbs -->
    @endif