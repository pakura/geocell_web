@extends('site')

@section('content')
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->
    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h">
                <div class="cs-spb-20">
                    <div class="cs-al-center">
                        <h1 class="tsr-title">{{$item['title']}}</h1>
                    </div>
                </div>
                <div class="cs-spb-30">
                    <div class="cs-al-center cs-mg-center cs-max-whole-x" style="width: 800px;">


                        @if(isset($seccode->indexes) && $seccode->resultCode == 1)
                            {!! $item['description'] !!}
                        @elseif($seccode->resultCode == -105)
                            @if($lang == 'en')
                                Service is blocked for 1 hour.
                            @else
                                სერვისი დაბლოკილია 1 საათით.
                            @endif
                        @elseif($seccode->resultCode == -104 || $seccode->resultCode == -103)
                            @if($lang == 'en')
                                Service is blocked, Please visit Geocell offices to activate the service.
                            @else
                                სერვისი დაბლოკილია, გასააქტიურებლად მიმართეთ ჯეოსელის ოფისებს.
                            @endif
                        @elseif($seccode->resultCode == -101 || $seccode->resultCode == -102 || $seccode->resultCode == -1003)
                            {!! $item['content'] !!}
                        @else
                            @if($lang == 'en')
                                Unknown error
                            @else
                                უცნობი შეცდომა
                            @endif
                        @endif
                    </div>
                </div>
            @if(isset($seccode->resultCode) && $seccode->resultCode == 1)
                @if(isset($seccode->indexes))
                <div class="cs-spb-30">
                    <div class="cs-al-center">
                        <form class="tsr-forms">
                            <div class="cs-spb-10"><label class="cc-label">
                                    @if($lang == 'en')
                                        Please enter <b>{{digit2text($seccode->indexes[0], $lang)}}</b>, <b>{{digit2text($seccode->indexes[1], $lang)}}</b> and <b>{{digit2text($seccode->indexes[2], $lang)}}</b> digits:
                                    @else
                                        გთხოვთ მიუთითეთ <b>{{digit2text($seccode->indexes[0], $lang)}}</b>, <b>{{digit2text($seccode->indexes[1], $lang)}}</b> და <b>{{digit2text($seccode->indexes[2], $lang)}}</b> ციფრი:
                                    @endif
                                </label></div>
                            <input type="hidden" name="key" id="key" value="{{$seccode->key}}">

                            <div class="cs-spb-10"><!--
                  --><div class="cs-dp-iblock cs-al-middle" style="width: 60px;"><div class="cs-spx-05"><input type="text" maxlength="1" name="num1" id="num1" autofocus maxlength="1" class="js-secret-code"/></div></div><!--
                  --><div class="cs-dp-iblock cs-al-middle" style="width: 20px;">-</div><!--
                  --><div class="cs-dp-iblock cs-al-middle" style="width: 60px;"><div class="cs-spx-05"><input type="text" maxlength="1" class="js-secret-code" name="num2" id="num2" max="1"/></div></div><!--
                  --><div class="cs-dp-iblock cs-al-middle" style="width: 20px;">-</div><!--
                  --><div class="cs-dp-iblock cs-al-middle" style="width: 60px;"><div class="cs-spx-05"><input type="text" maxlength="1" class="js-secret-code" name="num3" id="num3" max="1"/></div></div><!--
                --></div>
                            <div class="cs-spb-10"><!--
                  --><div class="cs-dp-iblock cs-al-top" id="errorMSG" style="width: 260px; margin:auto; <?php
                                            if(isset($_GET['err']) && $_GET['err'] == 'f')
                                                echo 'display: block';
                                            else
                                                echo 'display: none';
                                            ?>">
                                    <div class="cc-errorbox"> @if($lang == 'en' ) Wrong Secret Word @else კოდური სიტყვა არასწორია @endif </div></div><!--
                --></div>
                        </form>
                        <div class="cs-spb-10 cs-spt-30"><button class="tsr-btn tsr-btn-form tsr-btn-turquoise" id="sendcode" onclick="sendCode()">{{$translate['confirm']}}</button></div>
                    </div>
                </div>
                @endif
            @endif
            </div>
        </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <?php
        function digit2text($arg, $lang){
            $arg = intval($arg);
            if ($lang == 'en'){
                switch ($arg){
                    case 1:
                        return 'first';
                        break;
                    case 2:
                        return 'second';
                        break;
                    case 3:
                        return 'third';
                        break;
                    case 4:
                        return 'fourth';
                        break;
                    case 5:
                        return 'fifth';
                        break;
                    case 6:
                        return 'sixth';
                        break;
                    case 7:
                        return 'seventh';
                        break;
                    case 8:
                        return 'eighth';
                        break;
                }
            } else {
                switch ($arg){
                    case 1:
                        return 'პირველი';
                        break;
                    case 2:
                        return 'მეორე';
                        break;
                    case 3:
                        return 'მესამე';
                        break;
                    case 4:
                        return 'მეოთხე';
                        break;
                    case 5:
                        return 'მეხუთე';
                        break;
                    case 6:
                        return 'მეექვსე';
                        break;
                    case 7:
                        return 'მეშვიდე';
                        break;
                    case 8:
                        return 'მერვე';
                        break;
                }
            }
        }
    ?>

    <script>

        $(document).ready(function() {

            (function() {
                var $inputs = $('input');

                var $secret_inputs = $inputs.filter('.js-secret-code');

                var isAllowed = function(code, shift, ctrl) {
                    return $.inArray(code, [8, 9, 13, 27, 46]) !== -1 ||
                            (code == 65 && ctrl === true) || (code == 67 && ctrl === true) || (code == 88 && ctrl === true) ||
                            (code >= 33 && code <= 40);
                };

                var isDigit = function(code, shift, ctrl) {
                    return (code >= 48 && code <= 57);
                };

                $secret_inputs.on('keypress', function(e) {
                    var code = e.keyCode || e.which, shift = !!e.shiftKey, ctrl = !!e.ctrlKey;
                    if(e.keyCode == 13){
                        sendCode();
                    }
                    if (!isAllowed(code, shift, ctrl) && !isDigit(code, shift, ctrl)) {
                        e.preventDefault();
                    }

                }).on('input', function (e) {
                    var len = window.parseInt($(this).attr('maxlength')) || 0;
                    if (len && $(this).val().length == len) {
                        var pos = $inputs.index(this);
                        if (pos + 1 < $inputs.length) {
                            $inputs.eq(pos + 1).trigger('focus');
                        }
                    }
                });

            })();

        });




        function sendCode(){
            $('#errorMSG').fadeOut();
            var num1 = $('#num1').val();
            var num2 = $('#num2').val();
            var num3 = $('#num3').val();
            var key = $('#key').val();

            if (num1 == '' || num2 == '' || num3 == ''){
                $('#errorMSG').fadeIn();
                return;
            }
            $('#sendcode').prop('disabled', true);
            $.ajax({
                method: "get",
                url: "private/secsend/"+num1+"/"+num2+"/"+num3+"/"+key
            })
            .done(function( msg ) {
                if(msg == 'signature'){

                    window.location.href = "https://geocell.ge/developer_version/public/{{$lang}}/private/private-cabinet/esignature";

                } else {
                    if(msg == 'details'){
                        window.location.href = "https://geocell.ge/developer_version/public/{{$lang}}/private/private-cabinet/detailed";
                    } else {
                        if (msg == 'ok'){
                            alert('ok');
                        } else {
                            window.location.href = "/developer_version/public/{{$lang}}/private/seccode?err=f";
                        }
                    }

                }
            });
        }


    </script>
    <style>
        .redfield{
            border: 1px solid indianred;
        }
    </style>
@endsection