@extends('site')

@section('content')

@include('site.textcontent_default')
        <!-- tsr-section-refiner -->
<div class="tsr-section-refiner">
    <div class="tsr-refiner-header tsr-clearfix">
        <div class="tsr-container">
            <div class="tsr-refiner-links">
                <ul>
                    @foreach($subPages as $subpage)
                        <li><a href="{{$lang}}/{{$subpage->full_slug}}" class="{{ Request::is($lang.'/'.$subpage->full_slug) ? 'is-choosen' : '' }}">{{$subpage->title }}</a></li>
                        @endforeach
                                <!--  <li><a href="#" class="is-choosen">Our offices</a></li>
              <li><a href="#">Contact us</a></li> -->
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-empty"></div>
<!-- /tsr-section-divider -->

<!-- tsr-section-generic -->
<div class="tsr-section-generic">
    <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">

            <div class="cs-fl-right cs-spl-20"><a href="{{$lang.'/'.$item->full_slug}}" class="composite cs-tx-13 cs-dp-block"><span class="cs-tx-27 ts-icon-map cs-spr-04"></span><span class="cs-tx-bold">{{$translate['map_mode']}}</span></a></div>

            <div><span class="tsr-module-sdrop-btn cs-cl-purple" role="button"><span class="cs-al-middle cs-tx-28">{{$translate['head_office']}}</span></span></div>

            <br>
            <div class="cs-spb-20 tsr-twocol">
                <div class="tsr-twocol-grid">
                    <div class="tsr-twocol-cell cs-spb-30">
                        <div class="cs-pdr-20">
                            <div class="cs-tx-bold">
                                @if($lang == 'en')
                                    #3 Gotua Str., Right Riverside of Mtkvari
                                    Tbilisi, Georgia 0160

                                @else
                                    გოთუას ქ. 3., მტკვრის მარჯვენა სანაპირო
                                    თბილისი, საქართველო 0160
                                @endif
                            </div>
                            <div class="cs-cl-dgray">
                                @if($lang == 'en') Tel: @else ტელ: @endif +995 32 2770100 <br>
                                @if($lang == 'en') Fax: @else ფაქსი: @endif +995 32 2770101 <br>
                                @if($lang == 'en') P.O. Box: @else საფოსტო ყუთი: @endif  48-0102</div>
                            <!--  <div class="cs-spt-04 cs-tx-13"><a href="#">see on map</a></div> -->
                        </div>
                    </div>

                </div>
            </div>


            <div><span class="tsr-module-sdrop-btn cs-cl-purple" role="button"><span class="cs-al-middle cs-tx-28">{{$translate['corporate_clients']}}</span></span></div>

            <br>
            <div class="cs-spb-20 tsr-twocol">
                <div class="tsr-twocol-grid">
                    <div class="tsr-twocol-cell cs-spb-30">
                        <div class="cs-pdr-20">
                            <div class="cs-tx-bold">
                                @if($lang == 'en')
                                    #9 Tsintsadze str.
                                @else
                                    ცინცაძის ქ. #9
                                @endif
                            </div>
                            <div class="cs-cl-dgray">
                                @if($lang == 'en')
                                    Working hours: Mon-Fri 09:00-18:00,<br>
                                    Sat 10:00-14:00, without break.<br>
                                    Sun: Closed<br>
                                    Tel: +995 (32) 2770122<br>
                                @else
                                    Fax:+995 (32) 2770119
                                    სამუშაო საათები: ორშ-პარ 09:00-18:00,<br>
                                    ცინცაძის ოფისი შაბათს მუშაობს 10:00 – 14:00 სთ.(შესვენების გარეშე) კვირა: დასვენების დღე<br>
                                    ტელ: +995 (32) 2770122<br>
                                    ფაქსი: +995 (32) 2770119
                                @endif
                            </div>
                            <!--  <div class="cs-spt-04 cs-tx-13"><a href="#">see on map</a></div> -->
                        </div>
                    </div>

                </div>
            </div>


            <div class="cs-spb-10">
                <div class="tsr-module-sdrop tsr-style-01 is-expanded">
                    <div><span class="tsr-module-sdrop-btn cs-cl-purple" role="button"><span class="cs-al-middle cs-tx-28">{{$translate['tbilisi']}}</span></span></div>
                    <div class="tsr-module-sdrop-cnt">
                        <div class="cs-pdv-10">

                            <!-- <div class="cs-spb-10 cs-tx-22">Shops</div> -->
                            <div class="cs-spb-20 tsr-twocol">
                                <div class="tsr-twocol-grid">


                                    @foreach($offices as $key=>$office)
                                        @if($office->city_id == 1 && $office->shop == 1)
                                            <div class="tsr-twocol-cell cs-spb-30">
                                                <div class="cs-pdr-20">
                                                    <div class="cs-tx-bold">{{$office->address}} </div>
                                                    <div class="cs-cl-dgray">{{$office->info}}</div>
                                                    <div class="cs-cl-dgray"><b>{{$office->note}}</b></div>

                                                    <!--  <div class="cs-spt-04 cs-tx-13"><a href="#">see on map</a></div> -->
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach

                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>


            <hr class="tsr-com-hr cs-spv-10">

            <div class="cs-spb-10">
                <div class="tsr-module-sdrop tsr-style-01">
                    <div><span class="tsr-module-sdrop-btn cs-cl-purple" role="button"><span class="cs-al-middle cs-tx-28">{{$translate['metro_station']}}</span></span></div>
                    <div class="tsr-module-sdrop-cnt">
                        <div class="cs-pdv-10">

                            <!--                   <div class="cs-spb-10 cs-tx-22">Shops</div>
                             -->
                            <div class="cs-spb-20 tsr-twocol">
                                <div class="tsr-twocol-grid">
                                    @foreach($offices as $office)
                                        @if($office->city_id == 1 && $office->shop == 0 && $office->office_id != 69)
                                            <div class="tsr-twocol-cell cs-spb-30">
                                                <div class="cs-pdr-20">
                                                    <div class="cs-tx-bold">{{$office->city}} , {{$office->address}} </div>
                                                    <div class="cs-cl-dgray">{{$office->info}}</div>
                                                    <!--  <div class="cs-spt-04 cs-tx-13"><a href="#">see on map</a></div> -->
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <hr class="tsr-com-hr cs-spv-10">

            <div class="cs-spb-10">
                <div class="tsr-module-sdrop tsr-style-01">
                    <div><span class="tsr-module-sdrop-btn cs-cl-purple" role="button"><span class="cs-al-middle cs-tx-28">{{$translate['regions']}}</span></span></div>
                    <div class="tsr-module-sdrop-cnt">
                        <div class="cs-pdv-10">

                            <!--                   <div class="cs-spb-10 cs-tx-22">Shops</div>
                             -->
                            <div class="cs-spb-20 tsr-twocol">
                                <div class="tsr-twocol-grid">

                                    @foreach($offices as $office)
                                        @if($office->city_id != 1)
                                            <div class="tsr-twocol-cell cs-spb-30">
                                                <div class="cs-pdr-20">
                                                    <div class="cs-tx-bold">{{$office->city}} , {{$office->address}} </div>
                                                    <div class="cs-cl-dgray">{{$office->info}}</div>
                                                    <!--  <div class="cs-spt-04 cs-tx-13"><a href="#">see on map</a></div> -->
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /tsr-section-generic -->

<!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-empty"></div>
<!-- /tsr-section-divider -->

<script src="site/tsr-modules/tsr-sdrop/tsr-sdrop.js"></script>

@endsection
