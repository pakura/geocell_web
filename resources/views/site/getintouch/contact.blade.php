@extends('site')

@section('content')
  @include('site.textcontent_default')
  <!-- tsr-section-refiner -->
    <div class="tsr-section-refiner">
      <div class="tsr-refiner-header tsr-clearfix">
        <div class="tsr-container">
          <div class="tsr-refiner-links">
            <ul>
               @foreach($subPages as $subpage)
                <li><a href="{{$lang}}/{{$subpage->full_slug}}" class="{{ Request::is($lang.'/'.$subpage->full_slug) ? 'is-choosen' : '' }}">{{$subpage->title}}</a></li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-contactus-listing -->
    <div class="tsr-section-contactus-listing">
      <div class="tsr-container">
        <div class="tsr-boxgrid tsr-boxgrid-fit tsr-clearfix">

        @foreach($banners as $banner)
          <div class="tsr-boxcol-outer">
            <div class="tsr-boxcol-inner">
              <a href="{{$banner->url?$banner->url:'javascript:;'}}" {{$banner->open_blank?'target="_blank"':''}} id="{{$banner->url}}"  class="tsr-module-contactus">
                <div class="tsr-clearfix">
                  <figure class="tsr-module-contactus-image"><img alt="" src="{{$banner->photo}}"></figure>
                  <div class="tsr-module-contactus-desc">
                    <header class="tsr-module-contactus-title">{{$banner->title}}</header>
                    <span class="tsr-module-contactus-intro">{!! $banner->description !!}</span>
                  </div>
                </div>
              </a>
            </div>
          </div>
         @endforeach


        </div>
      </div>
    </div>
    <!-- /tsr-contactus-listing -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic" id="local-write-us-section">
      <div class="tsr-container">
        <div class="cs-pdt-40">
          <div class="tsr-section-generic-pad-h">
            <div class="cs-spb-20">{{$translate['write_us_text']}}</div>
            <div class="cs-spb-20">

              <form action="{{$lang}}/contact/send" method="post" class="tsr-forms" onsubmit="return complexFormAction(this)">
                <div class="errors tsr-clearfix"></div>
                <div class="tsr-grid tsr-clearfix">
                  <div class="tsr-col-06 cs-spb-20">
                    <div><label class="cc-label">{{$translate['choose_department']}}</label></div>
                    <div><input type="text" name="department" value=""></div>
                  </div>
                  <div class="tsr-col-06 cs-spb-20">
                    <div><label class="cc-label">{{$translate['email']}}*</label></div>
                    <div><input type="text" name="email" value=""></div>
                  </div>
                  <div class="tsr-col-12 cs-spb-20">
                    <div><label class="cc-label">{{$translate['issue']}}*</label></div>
                    <div><textarea rows="6" name="comment"></textarea></div>
                  </div>
                  <div class="tsr-col-12 cs-spb-20">
                   <!--  <div><label class="cc-label">Security code*</label></div> -->
                 
                    <div><div class="g-recaptcha" data-sitekey="6LeO3AsTAAAAAP1mbagBkBXXzJjl9ofId_msIzpF"></div></div>
                  </div>
                  <div class="tsr-col-12 cs-spb-20">
                    <div><input type="submit" value="{{$translate['contact_submit']}}" class="tsr-btn tsr-btn-form"></div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->
 
 <script src="site/tsr-sections/tsr-contactus-listing/tsr-contactus-listing.js"></script>
 <script type="text/javascript">/*<![CDATA[*/

      $(document).ready(function() {
        var $button   = $('#local-write-us-button');
        var $section  = $('#local-write-us-section');
        $section.hide();
        $button.on('click', function(event) {
          if (!$section.is(':visible')) {
            $section.show();
            tsrCore.scrollTo($section, 300, 0);
          } else {
            $section.hide();
          }
          event.preventDefault();
        });
      });

    /*]]>*/</script>

@endsection
