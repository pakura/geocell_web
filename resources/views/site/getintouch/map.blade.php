@extends('site')


@section('content')

 @include('site.textcontent_default')
 
 <script src="https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false&amp;language=en" type="text/javascript"></script>


<!-- tsr-section-refiner -->
    <div class="tsr-section-refiner">
      <div class="tsr-refiner-header tsr-clearfix">
        <div class="tsr-container">
          <div class="tsr-refiner-links">
            <ul>
              @foreach($subPages as $subpage)
              	<li><a href="{{$lang}}/{{$subpage->full_slug}}" class="{{ Request::is($lang.'/'.$subpage->full_slug) ? 'is-choosen' : '' }}">{{$subpage->title }}</a></li>
              @endforeach
             <!--  <li><a href="#" class="is-choosen">Our offices</a></li>
              <li><a href="#">Contact us</a></li> -->
            </ul>
          </div>
        </div>
      </div>
    </div>

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
          <div class="cs-spb-04 cs-tx-bold">{{$translate['choose_city_reg']}}</div>
          <div class="tsr-clearfix">
            <div class="cs-fl-left cs-spr-20">
              <form action="#">
                <div class="tsr-forms">
                  <div class="tsr-clearfix">
                    <div class="cs-fl-left cs-spb-10" style="min-width: 200px; max-width: 260px;">

                    	<select  onchange="filterMarkers(this.value);">
                          <option value="999">{{$translate['all']}}</option>
                    		@foreach($cities as $city)
                    			<option value="{{$city->id}}">{{$city->city_name}}</option>
                    	    @endforeach                  		 
                    	</select>

                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="cs-fl-left cs-pdt-20">
              <div class="cs-spb-10 cs-smt-08">
                <div class="tsr-com-linklist">
                  <ul class="cs-tx-13 cs-tx-bold"><!--
                    --><!-- <li class="filter"><a href="javascript:;" onclick="filterMarkersShop(3,this)" class="is-choosen">{{$translate['all']}}</a></li> --><!--
                    --><li class="filter"><a href="javascript:;"  onclick="filterMarkersShop(1,this)" >{{$translate['only_shops']}}</a></li><!--
                    --><li class="filter"><a href="javascript:;"  onclick="filterMarkersShop(0,this)" >{{$translate['offices_in_metro']}}</a></li><!--
                  --></ul>
                </div>
              </div>
            </div>
            <div class="cs-fl-right cs-spl-20 cs-pdt-10">
              <div class="cs-spb-10 cs-smt-02"><a href="{{$lang.'/'.$item->full_slug}}?list_mode=1" class="composite cs-tx-13 cs-dp-block"><span class="cs-tx-27 ts-icon-menu cs-spr-04"></span><span class="cs-tx-bold">{{$translate['view_list_mode']}}</span></a></div></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

    <!-- tsr-section-gmap -->
    <div class="tsr-section-gmap-offices" id="google-map-offices"></div>
    <!-- /tsr-section-gmap -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->


<script type="text/javascript">

var gmarkers1 = [];
var markers1 = [];
var infowindow = new google.maps.InfoWindow({
    content: ''
});

 // icon path
        var iconpath = '{{config('app.url')}}site/tsr-temp/tsr-images/';

// Our markers
/*markers1 = [ 
    ['0', '<div class="tsr-gmap-infowindow"><div class="cs-tx-22">#31 Chavchavadze Ave<\/div><div>Working hours: Mon-Sat 10:00-22:00, Sun 10:00-18:00, without break.<\/div><\/div>', 41.710238, 44.726629, '2','google-marker.png'],
    ['1', '<div class="tsr-gmap-infowindow"><div class="cs-tx-22">#31 Chavchavadze Ave<\/div><div>Working hours: Mon-Sat 10:00-22:00, Sun 10:00-18:00, without break.<\/div><\/div>', 41.710494, 44.728453, '3','google-marker.png'],
    ['2', '<div class="tsr-gmap-infowindow"><div class="cs-tx-22">#31 Chavchavadze Ave<\/div><div>Working hours: Mon-Sat 10:00-22:00, Sun 10:00-18:00, without break.<\/div><\/div>', 52.4555687, 5.039231599999994, '2','google-marker.png'],
    ['3', '<div class="tsr-gmap-infowindow"><div class="cs-tx-22">#31 Chavchavadze Ave<\/div><div>Working hours: Mon-Sat 10:00-22:00, Sun 10:00-18:00, without break.<\/div><\/div>', 52.4555687, 5.029231599999994, '3','google-marker.png']
];

*/

<?php

	for($i=0;$i<count($offices);$i++) {
		if(!$offices[$i]->coordinates) { continue;}
		$coord = explode(',', $offices[$i]->coordinates);
		//dd($coord);
		$o = [$i,'<div class="tsr-gmap-infowindow"><div class="cs-tx-22">'.$offices[$i]->address.'</div><div>'.$offices[$i]->info.'</div></div>',(float)$coord[0],(float)$coord[1],$offices[$i]->city_id,'google-marker.png',$offices[$i]->shop];
		?>

          markers1.push(<?php echo json_encode($o); ?>);
 
		<?php
	}

?>
/**
 * Function to init map
 */

function initialize() {
    var center = new google.maps.LatLng(41.717502,44.780229);
    var mapOptions = {
        zoom: 14,
        center: center,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById('google-map-offices'), mapOptions);
    for (i = 0; i < markers1.length; i++) {
        addMarker(markers1[i]);
    }
}

/**
 * Function to add marker to map
 */

function addMarker(marker) {
    var category = marker[4];
    var title = marker[1];
    var pos = new google.maps.LatLng(marker[2], marker[3]);
    var content = marker[1];
    var pinicon = iconpath + marker[5];
    var shop =  marker[6];

    marker1 = new google.maps.Marker({
        title: '',
        position: pos,
        icon: {
              url: pinicon,
              
            },
        category: category,
        shop: shop,
        map: map
    });

    gmarkers1.push(marker1);

    // Marker click listener
    google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
        return function () {
            console.log('Gmarker 1 gets pushed');
            infowindow.setContent(content);
            infowindow.open(map, marker1);
            map.panTo(this.getPosition());
            map.setZoom(15);
        }
    })(marker1, content));
}

/**
 * Function to filter markers by category
 */

filterMarkers = function (category) {
   var pos;
   if(category==1 || category==999) { $(".filter").show();}
   else { $(".filter").hide();}

    for (i = 0; i < markers1.length; i++) {
        marker = gmarkers1[i];
        // If is same category or category not picked
        if (marker.category == category || category.length === 0 || category==999) {
            marker.setVisible(true);

            pos = marker.position;
        }
        // Categories don't match 
        else {
            marker.setVisible(false);
        }
    }

    pos = category==999?new google.maps.LatLng(41.717502,44.780229):pos;
    map.setCenter(pos);

}

filterMarkersShop = function (shop,elem) {
   var pos = new google.maps.LatLng(41.717502,44.780229);

    for (i = 0; i < markers1.length; i++) {
        marker = gmarkers1[i];
        // If is same category or category not picked
        if (marker.shop == shop || shop==3) {
            marker.setVisible(true);

           // pos = marker.position;
        }
        // Categories don't match 
        else {
            marker.setVisible(false);
        }
    }

    map.setCenter(pos);
    
    $(elem).parent().siblings().find('a.is-choosen').removeClass('is-choosen');
    $(elem).addClass('is-choosen');
    
}

// Init map
initialize();
 	 </script>

@endsection
