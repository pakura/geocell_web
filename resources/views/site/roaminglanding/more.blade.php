@extends('site')

@section('content')


        <!-- tsr-section-generic -->
<div class="tsr-section-generic cs-bg-shade cs-smt-02">
    <div class="tsr-container">
        <div class="tsr-section-generic-pad-h cs-pdb-40">
            <h1 class="tsr-title cs-cl-blue">{{$item->title}}</h1>


        </div>
    </div>
</div>

<!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-empty"></div>
<!-- /tsr-section-divider -->

        <!-- tsr-section-generic -->
<div class="tsr-section-generic">

    <div class="cs-spt-20">
        <div class="tsr-module-tabular-a">
            <div class="tsr-module-tabular-header">
                <div class="tsr-container">
                    <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab1">{{$translate['price_n_condition']}}</a>
                    <a href="#" class="tsr-module-tabular-but" data-tabular-exp-target="tab2">{{$translate['terms_n_conditions']}}</a>
                </div>
            </div>
            <div class="tsr-module-tabular-footer">
                <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1">
                    <div class="tsr-container">
                        <div class="tsr-section-generic-pad">
                            {!! $item->content !!}
                        </div>
                    </div>
                </div>
                <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
                    <div class="tsr-container">
                        <div class="tsr-section-generic-pad">
                            {!! $item->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /tsr-section-generic -->

@endsection
