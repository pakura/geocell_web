@extends('site')

@section('content')

    <div class="tsr-section-generic cs-bg-shade cs-smt-02">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h cs-pdb-40">
                <h1 class="tsr-title cs-cl-blue">@if($lang == 'en') Cheap Roaming @else იაფი როუმინგი @endif</h1>
                <div></div>
            </div>
        </div>
    </div>

<!-- tsr-section-generic -->
<div class="tsr-section-generic">
    <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
            <div class="cs-spb-20"><img src="site/tsr-temp/tsr-images/promo-image-04.jpg" width="100%" alt="" class="cs-max-whole-x"></div>
        </div>
    </div>
</div>
<!-- /tsr-section-generic -->

<!-- QUIZ STEP 1 -->
<div class="tsr-section-generic">
    <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
            <div class="cs-spb-20 cs-al-center"><h2 class="tsr-title">{{$translate['where_going']}}</h2></div>
            <div class="cs-spb-20 cs-al-center"><input id="countryInput" type="text" class="tsr-search-input cs-al-top" placeholder="{{$translate['type_country_name']}}" style="padding:0px;width: 320px;text-align: center;"
           @if(isset($_GET['name'])) value="{{$_GET['name']}}" @endif /></div>
            <div class="cs-spb-20 tsr-lightbox-layout-a-field msisdn-error tsr-forms" style="display: none;width: 275px; margin: auto;" id="suberror"><div class="cc-errorbox" >@if($lang == 'en') Country name is not correct or there is no roaming service in this country. Please, try to enter correct country name. @else ქვეყნის სახელი არასწორია ან ამ ქვეყანაში როუმინგ სერვისი მიუწვდომელია. გთხოვთ, სწორად შეიყვანეთ ქვეყნის სახელი. @endif</div></div>
            <div class="cs-spb-20 cs-spacer"></div>
        </div>
    </div>
</div>
<!-- /QUIZ STEP 1 -->



<!-- QUIZ LOADER -->
<div class="tsr-section-generic" style="display:none;">
    <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
            <div class="cs-spb-20 cs-al-center"><img src="site/tsr-core/tsr-images/tsr-spi-loader-32.gif" alt="Loading..." /></div>
            <div class="cs-spb-20 cs-spacer"></div>
        </div>
    </div>
</div>
<!-- /QUIZ LOADER -->


<!-- QUIZ STEP 2 -->
<div class="tsr-section-generic" style="@if(!isset($_GET['carrier'])) display:none; @endif" id="chooseGSM">
    <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
            <div class="cs-spb-20 cs-al-center"><h2 class="tsr-title">{{$translate['what_number_travel']}}</h2></div>
            <div class="cs-spb-20 cs-al-center"><!--
            --><div class="cs-dp-iblock cs-al-top cs-spx-02"><span data-icon-b="&#xe005;" class="tsr-chbtn tsr-chbtn-icon-a gsmbutton
                    @if(isset($_GET['carrier'])) @if($_GET['carrier'] == 1) is-selected @endif @endif
                " onclick="chooseGSM(true, this)">Geocell</span></div><!--
            --><div class="cs-dp-iblock cs-al-top cs-spx-02"><span data-icon-b="&#xe005;" class="tsr-chbtn tsr-chbtn-icon-a gsmbutton

                @if(isset($_GET['carrier'])) @if($_GET['carrier'] == 2) is-selected @endif @endif
                    " onclick="chooseGSM(false, this)">Lai-Lai</span></div><!--
          --></div>
            <div class="cs-spb-20 cs-spacer"></div>
        </div>
    </div>
</div>
<!-- /QUIZ STEP 2 -->

<!-- QUIZ STEP 3 -->
<!-- tsr-section-generic -->
<div class="tsr-section-generic" style="display: none" id="roamingResult">
    <div class="cs-spb-20">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h">
                <div class="cs-al-center"><h2 class="tsr-title">{{$translate['cheap _roaming']}}</h2></div>
            </div>
        </div>
    </div>
    <div class="cs-spb-20">
        <div class="tsr-module-tabular-a" data-tabular-collapsed="true">
            <div class="tsr-module-tabular-header">
                <div class="tsr-container">
                    <div class="cs-al-center cs-cl-link cs-tx-18" id="onlycall" style="height: 63px;line-height: 63px; display: none"><span class="cs-spr-10 cs-tx-20 cs-al-middle ts-icon-phone"></span><span class="cs-al-middle">@if ($lang=='en') Call only for 0.35&nbsp;₾ @else ზარები მხოლოდ 0.35&nbsp;₾ დან @endif </span></div>
                    <div class="cs-al-center cs-cl-link cs-tx-18" id="onlygprs"  onclick="urlgsmtype = 3" style="height: 63px;line-height: 63px; display: none"><span class="cs-spr-10 cs-tx-20 cs-al-middle ts-icon-internet"></span><span class="cs-al-middle">{{$translate['internet']}} 2.50&nbsp;&#x20be;/{{$translate['day']}}</span></div>
                    <div style="display: none" id="both">
                        <a href="#" class="tsr-module-tabular-but is-selected" id="tab1" style="height: 63px!important; " data-tabular-exp-target="tab1"><span class="cs-spr-10 cs-tx-20 cs-al-middle ts-icon-phone"></span><span class="cs-al-middle cs-tx-18">@if ($lang=='en') Call only for 0.35&nbsp;&#x20be; @else ზარები მხოლოდ 0.35&nbsp;&#x20be; დან @endif </span></a>
                        
                        <a href="#" class="tsr-module-tabular-but" id="tab2" style="height: 63px!important;" data-tabular-exp-target="tab2" onclick="urlgsmtype = 3"><span class="cs-spr-10 cs-tx-20 cs-al-middle ts-icon-internet"></span><span class="cs-al-middle cs-tx-18">{{$translate['internet']}} 2.50&nbsp;&#x20be;/{{$translate['day']}}</span></a>
                    </div>
                </div>
            </div>
            <div class="tsr-module-tabular-footer">
                <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab1" >
                    <div class="tsr-container">
                        <div class="tsr-section-generic-pad">
                            <div class="cs-spb-10">
                                <table class="tsr-com-table-nulled border-inner-h">
                                    <tr>
                                        <td class="cs-pdv-05 cs-pdr-20 cs-cl-blue cs-wspace-nowrap">35 {{$translate['tetri']}}</td>
                                        <td class="cs-pdv-05">
                                            @if($lang == 'en')
                                                Outgoing calls to Georgia;
                                                works only  in case you dial an addressee’s number in the following format:*123*995xxxxxxxxx# OK
                                            @else
                                                ნებისმიერი გამავალი ზარი საქართველოში
                                                ადრესატის ნომერი აკრიფეთ  ფორმატში:  *123*995xxxxxxxxx# OK.
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="cs-pdv-05 cs-pdr-20 cs-cl-blue cs-wspace-nowrap">35 {{$translate['tetri']}}</td>
                                        <td class="cs-pdv-05">
                                            @if($lang == 'en')
                                                Incoming call
                                            @else
                                                ნებისმიერი შემავალი ზარი
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="cs-pdv-05 cs-pdr-20 cs-cl-blue cs-wspace-nowrap"></td>
                                        <td class="cs-pdv-05">
                                            @if($lang == 'en')
                                                Call set up fee - 10 tetri.
                                            @else
                                                ზარის წამოწყების საფასური - 10 თეთრი.
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="cs-spb-20">
                            <p class="cs-spb-04 cs-tx-22 cs-tx-bold">{{$translate['service_folowing_operators']}}</p>
                            <p id="operatorList"></p>
                        </div>
                    </div>
                </div>
                <div class="tsr-module-tabular-tab" data-tabular-exp-anchor="tab2">
                    <div class="tsr-container">
                        <div class="tsr-section-generic-pad">

                            <div class="cs-spb-10">
                                <div class="tsr-module-ipacksbx">
                                    <div class="tsr-boxgrid tsr-boxgrid-06 tsr-boxgrid-fit tsr-clearfix">
                                        <div class="tsr-boxcol-outer">
                                            <div class="tsr-boxcol-inner">
                                                <a href="#" class="tsr-module-ipacksbx-item">
                                                    <div class="cs-spb-00 cs-tx-bold cs-tx-26 tsr-module-ipacksbx-title">10 {{$translate['mb']}}</div>
                                                    <div class="cs-spb-20 cs-tx-bold cs-tx-15">{{$translate['price']}} 2.5 {{$translate['gel']}}</div>
                                                    <div class="cs-spb-02"></div>
                                                    <div class="cs-spb-00">{{$translate['vada']}} 1 {{$translate['days']}}</div>
                                                </a>
                                                <a href="{{$lang}}/activate/service/55" class="tsr-module-ipacksbx-btn tsr-btn tsr-btn-xsmall js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['purchase']}}</a>
                                            </div>
                                        </div>
                                        <div class="tsr-boxcol-outer">
                                            <div class="tsr-boxcol-inner">
                                                <a href="#" class="tsr-module-ipacksbx-item">
                                                    <div class="cs-spb-00 cs-tx-bold cs-tx-26 tsr-module-ipacksbx-title">50 {{$translate['mb']}}</div>
                                                    <div class="cs-spb-20 cs-tx-bold cs-tx-15">{{$translate['price']}} 10 {{$translate['gel']}}</div>
                                                    <div class="cs-spb-02"></div>
                                                    <div class="cs-spb-00">{{$translate['vada']}} 30 {{$translate['days']}}</div>
                                                </a>
                                                <a href="{{$lang}}/activate/service/56" class="tsr-module-ipacksbx-btn tsr-btn tsr-btn-xsmall js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['purchase']}}</a>
                                            </div>
                                        </div>
                                        <div class="tsr-boxcol-outer">
                                            <div class="tsr-boxcol-inner">
                                                <a href="#" class="tsr-module-ipacksbx-item">
                                                    <div class="cs-spb-00 cs-tx-bold cs-tx-26 tsr-module-ipacksbx-title">150 {{$translate['mb']}}</div>
                                                    <div class="cs-spb-20 cs-tx-bold cs-tx-15">{{$translate['price']}} 25 {{$translate['gel']}}</div>
                                                    <div class="cs-spb-02"></div>
                                                    <div class="cs-spb-00">{{$translate['vada']}}: 30 {{$translate['days']}}</div>
                                                </a>
                                                <a href="{{$lang}}/activate/service/57" class="tsr-module-ipacksbx-btn tsr-btn tsr-btn-xsmall js-fancybox fancybox.ajax" data-fancybox-type="ajax">{{$translate['purchase']}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cs-spb-20">
                                <p class="cs-spb-04 cs-tx-22 cs-tx-bold">{{$translate['service_folowing_operators']}}</p>
                                <p id="gprsoperatorList"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="tsr-section-generic" style="display: none" id="inforesult">
    <div class="cs-spb-20">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h">
                <div class="cs-spb-00">
                    <div class="cs-spb-04"><a onclick="openUrl()" target="_blank" class="tsr-link-composite"><span class="cs-dp-inline-block cs-tx-16 cs-spr-04 ts-icon-info"></span><span>{{$translate['more_details']}}</span></a></div>
                    <div class="cs-spb-04"><a class="tsr-link-composite" onclick="$('#standartResult').fadeToggle()"><span class="cs-dp-inline-block cs-tx-16 cs-spr-04 ts-icon-info"></span><span>{{$translate['standard_tarifs_b']}}</span></a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /QUIZ STEP 3 -->

<div class="tsr-section-generic" style="display: none" id="standartResult">

    <div class="cs-spb-20">
        <div class="tsr-container">
            <hr class="tsr-com-hr cs-spv-30">
            <div class="tsr-section-generic-pad-h">
                <div class="cs-al-center">
                    <h2 id="no_standart_stand" class="tsr-title">{{$translate['standard_tarifs_b']}}</h2>
                    <h2 id="no_standart" style="display: none" class="tsr-title">{{$translate['standard_tarifs']}}</h2>
                    <h2 id="no_standart_lai" style="display: none" class="tsr-title"> @if($lang == 'en') Unfortunately, Lai Lai roaming is not available in this country. @else სამწუხაროდ, ამ ქვეყანაში ლაი ლაი როუმინგი არ არის ხელმისაწვდომი @endif</h2>
                    <h2 id="no_standart_geo" style="display: none" class="tsr-title"> @if($lang == 'en') Unfortunately, Geocell roaming is not available in this country. @else სამწუხაროდ, ამ ქვეყანაში ჯეოსელი როუმინგი არ არის ხელმისაწვდომი @endif </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="cs-spb-20">
        <div class="tsr-container">
            <div class="tsr-section-generic-pad-h" id="standartTarifs">

            </div>
        </div>
    </div>
</div>

<!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-empty"></div>
<!-- /tsr-section-divider -->

<!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-color-purple">
    <div class="tsr-container">
        <h2>{{$translate['usefull_information_for_roaming']}}</h2>
    </div>
</div>
<!-- /tsr-section-divider -->

<!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-empty"></div>
<!-- /tsr-section-divider -->

<!-- tsr-section-generic -->
<div class="tsr-section-generic">
    <div class="tsr-container">
        <div class="tsr-section-generic-pad-h">
            <div class="cs-spb-10"><h3 class="tsr-title">{{$translate['how_to_subscribe_roaming_service']}}</h3></div>
            <div class="cs-spb-10 cs-tx-bold">{{$translate['dial']}}:</div>
            <div class="cs-spb-10">
                <div class="tsr-phone-nums">
                    <span class="tsr-phone-num">*</span>
                    <span class="tsr-phone-num">1</span>
                    <span class="tsr-phone-num">1</span>
                    <span class="tsr-phone-num">7</span>
                    <span class="tsr-phone-num">#</span>
                    <span class="tsr-phone-num tsr-phone-num-ok tsr-color-turquoise">ok</span>
                </div>
            </div>
            <div class="cs-spb-00">* {{$translate['activation_roaming_is_free_to_charge']}}.</div>
        </div>
    </div>
</div>
<!-- /tsr-section-generic -->

<!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-empty"></div>
<!-- /tsr-section-divider -->

<!-- tsr-section-faq-listing -->
<div class="tsr-section-faq-listing">
    <div class="tsr-container">
        <div class="tsr-com-collapser-outer">
            <div class="tsr-com-collapser-inner">
                <div class="cs-spb-20"><h3 class="tsr-title">{{$translate['faq']}}</h3></div>
                @foreach($faqs as $faq)
                    <div class="tsr-module-faq">
                        <a href="#" class="tsr-module-faq-question" onclick="return false;"><span class="tsr-module-faq-pad">{{$faq->question}} </span></a>
                        <div class="tsr-module-faq-answer"><div class="tsr-module-faq-pad">{!! $faq->answer !!}</div></div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<!-- /tsr-section-faq-listing -->



<!-- tsr-section-divider -->
<div class="tsr-section-divider tsr-divider-empty"></div>
<!-- /tsr-section-divider -->


<script>
    var selectedCoutry = '';
    $(function() {

        var availableTags = [
            @for($i = 0; $i<sizeof($countryList); $i++)
                 "{{ str_replace('&', 'and', $countryList[$i])}}",
            @endfor
        ];

        $("#countryInput").autocomplete({
            source: function(request, response) {
                var results = $.ui.autocomplete.filter(availableTags, request.term);
                response(results.slice(0, 3));
            },
            select: function (a, b) {
                getCountry(b.item.value);
            }
        }).keyup(function (e) {
            if(e.which === 13) {
                $(".ui-menu-item").hide();
                getCountry($('#countryInput').val());
            }
        });
        @if(isset($_GET['name']) && isset($_GET['carrier']))
            chooseGSM(@if($_GET['carrier'] == 1) {{'true'}} @else {{'false'}} @endif);
        @endif
    });


    function getCountry(name){
        $('#inforesult').fadeOut();
        $('#standartResult').fadeOut();
        $.ajax({
            method: "POST",
            url: "/developer_version/public/{{$lang}}/getroamingitem",
            data: { name: name }
        })
        .done(function( msg ) {
            if(msg == 1){
                selectedCoutry = name.value;
                $('#chooseGSM').fadeIn();
                $('#roamingResult').fadeOut();
                $('.msisdn-error').fadeOut();
            } else {
                $('.msisdn-error').fadeIn();
                $('#chooseGSM').fadeOut();
                $('#roamingResult').fadeOut();
            }
            $('.is-selected').removeClass('is-selected');
        });

    }

    function chooseGSM(gsmtype, btn){
        if(gsmtype == true){
            urlgsmtype = 1
        } else {
            urlgsmtype = 2;
        }
        document.getElementById('standartResult').style.display = 'none';
        document.getElementById('inforesult').style.display = 'none';
        if(typeof btn != 'undefined'){
            $('.gsmbutton').removeClass('is-selected');
            $(btn).addClass('is-selected');
        }
        if($('#countryInput').val() != ''){
            name = $('#countryInput').val();
            $.ajax({
                method: "POST",
                url: "/developer_version/public/{{$lang}}/getroamingitems",
                data: { name: name }
            })
            .done(function( msg ) {
                if(msg == false){
                    return;
                }
                showCountry(JSON.parse(msg), gsmtype);
            });
        }
    }

    function showCountry(operators, gsmtype){
        var operatorcnt = 0;
        $('#standartTarifs').html('');
        $('#operatorList').html('');
        document.getElementById('standartResult').style.display = 'none';
        for(var ii in operators){
            if(operators[ii].isPostpaid == gsmtype) {
                if ((operators[ii].mtc.indexOf('+') < 0 && operators[ii].limitedList == true) && typeof operators[ii].tariffCurrency != 'undefined'){
                    if (operatorcnt == 0) {
                        $('#operatorList').html(operators[ii].operatorName);
                    } else {
                        $('#operatorList').html($('#operatorList').html() + ', ' + operators[ii].operatorName);
                    }
                    operatorcnt++;
                }
            } else {
                if(operators[ii].isPostpaid == gsmtype && gsmtype == false  && typeof operators[ii].tariffCurrency != 'undefined'){
                    if(operatorcnt == 0){
                        $('#operatorList').html(operators[ii].operatorName);
                    } else {
                        $('#operatorList').html($('#operatorList').html()+', '+operators[ii].operatorName);
                    }
                    operatorcnt++;
                }
            }
        }
        for(var ii in operators){






            if(operators[ii].isPostpaid == gsmtype) {
                var html = '<div class="cs-spb-30">\
                        <div class="cs-spb-10 cs-tx-22">'+operators[ii].operatorName+'</div>\
                        <div class="scroll-wrapper tsr-scrollpane" style="position: relative;"><div class="tsr-scrollpane scroll-content" style="height: auto;margin-bottom: 0px; margin-right: 0px; ">\
                        <div class="tsr-scrollpane-content">\
                        <table class="tsr-com-table-pricelist"><tbody>';
                if(gsmtype == true){
                    html +='<tr><td class="tsr-out-pname"><div class="tsr-inn-pname">{{$translate['call_set_up']}}</div></td>                                    <td class="tsr-out-pdesc cs-cl-blue"><div class="tsr-inn-pdesc">'+parseFloat(operators[ii].callSetup).toFixed(2)+'</div></td></tr>';
                }
                html +='<tr><td class="tsr-out-pname"><div class="tsr-inn-pname">{{$translate['call_to_georgia']}}</div></td>                                    <td class="tsr-out-pdesc cs-cl-blue"><div class="tsr-inn-pdesc">'+parseFloat(operators[ii].mosGeorgiaPeak).toFixed(2)+'</div></td></tr>';
                if (gsmtype == false) {
                    html += '<tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['international_call']}}</td>                          <td class="tsr-out-pdesc cs-cl-blue"><div class="tsr-inn-pdesc">'+parseFloat(operators[ii].mocOperator).toFixed(2)+'</div></td></tr>';
                } else {
                    html += '<tr><td class="cs-pdv-02 cs-pdr-20" style="width: 60%;">{{$translate['call_within_operator']}}</td>                        <td class="tsr-out-pdesc cs-cl-blue"><div class="tsr-inn-pdesc">'+parseFloat(operators[ii].mocOperator).toFixed(2)+'</div></td></tr>';
                }
                html += '<tr><td class="tsr-out-pname"><div class="tsr-inn-pname">{{$translate['call_within_country']}}</div></td>                               <td class="tsr-out-pdesc cs-cl-blue"><div class="tsr-inn-pdesc">'+parseFloat(operators[ii].mocLocal).toFixed(2)+'</div></td></tr>\
                    <tr><td class="tsr-out-pname"><div class="tsr-inn-pname">{{$translate['incomming_call']}}</div></td>                          <td class="tsr-out-pdesc cs-cl-blue"><div class="tsr-inn-pdesc">'+operators[ii].mtc+'</div></td></tr>\
                    <tr><td class="tsr-out-pname"><div class="tsr-inn-pname">{{$translate['outgoing_sms']}}</div></td>                                <td class="tsr-out-pdesc cs-cl-blue"><div class="tsr-inn-pdesc">'+parseFloat(operators[ii].mocSMS).toFixed(2)+'</div></td></tr>';

                if(operators[ii].mosGPRS !== false)
                    html += '<tr><td class="tsr-out-pname"><div class="tsr-inn-pname">{{$translate['mobile_internet_and_mms']}}</div></td>                                  <td class="tsr-out-pdesc cs-cl-blue"><div class="tsr-inn-pdesc">'+operators[ii].mosGPRS+'</div></td></tr>';

                html += '<tr><td class="tsr-out-pname">&nbsp;</td><td class="tsr-out-pdesc cs-cl-blue">&nbsp;</td></tr>\
                    <tr><td class="tsr-out-pname"><div class="tsr-inn-pname">{{$translate['currency']}}</td>\       <td class="tsr-out-pdesc cs-cl-blue"><div class="tsr-inn-pdesc">'+operators[ii].tariffCurrency+'</div></td></tr></tbody></table>\
                    </div>\
                    </div><hr class="tsr-com-hr cs-spv-30">\
                    </div>';

                $('#standartTarifs').append(html);
            }
        }
        var gprscnt = 0;
        for(var ii in operators){
            if(operators[ii].isPostpaid == gsmtype && operators[ii].dataBundle == true){
                if(gprscnt == 0){
                    $('#gprsoperatorList').html(operators[ii].operatorName);
                } else {
                    $('#gprsoperatorList').html($('#gprsoperatorList').html()+', '+operators[ii].operatorName);
                }
                gprscnt++;
            }
        }
        document.getElementById('no_standart').style.display = 'none';
        document.getElementById('no_standart_geo').style.display = 'none';
        document.getElementById('no_standart_lai').style.display = 'none';
        if(operatorcnt != 0 || gprscnt != 0){
            if(operatorcnt != 0 && gprscnt != 0){
                $('#tab1').click();
                $('#both').fadeIn();
                document.getElementById('no_standart_stand').style.display = 'block';
                document.getElementById('no_standart_geo').style.display = 'none';
                document.getElementById('no_standart_lai').style.display = 'none';
                document.getElementById('no_standart').style.display = 'none';
                document.getElementById('onlygprs').style.display = 'none';
                document.getElementById('onlycall').style.display = 'none';
                document.getElementById('both').style.display = 'block';
            } else {
                if(operatorcnt > 0){
                    document.getElementById('no_standart_stand').style.display = 'block';
                    document.getElementById('no_standart_geo').style.display = 'none';
                    document.getElementById('no_standart_lai').style.display = 'none';
                    document.getElementById('no_standart').style.display = 'none';
                    document.getElementById('onlycall').style.display = 'block';
                    document.getElementById('both').style.display = 'none';
                    document.getElementById('onlygprs').style.display = 'none';
                    $('#tab1').click();
                    document.getElementById('both').style.display = 'none';
                } else {
                    document.getElementById('no_standart_stand').style.display = 'none';
                    document.getElementById('no_standart_geo').style.display = 'none';
                    document.getElementById('no_standart_lai').style.display = 'none';
                    document.getElementById('no_standart').style.display = 'block';
                    if(gprscnt >0 ){
                        $('#tab2').click();
                        urlgsmtype = 3;
                        document.getElementById('onlycall').style.display = 'none';
                        document.getElementById('both').style.display = 'none';
                        document.getElementById('onlygprs').style.display = 'block';
                    } else {
                        document.getElementById('onlycall').style.display = 'none';
                        document.getElementById('both').style.display = 'none';
                        document.getElementById('onlygprs').style.display = 'none';
                        document.getElementById('standartResult').style.display = 'block';
                    }
                }
            }

            $('.tsr-module-tabular-but').css({'height':'63px'});
            document.getElementById('roamingResult').style.display = 'block';
            document.getElementById('inforesult').style.display = 'block';
        } else {
            var operatorcnt = 0;
            for(var ii in operators){
                if(operators[ii].isPostpaid == gsmtype){
                    operatorcnt++;
                }
            }

            if(operatorcnt > 0){
                document.getElementById('roamingResult').style.display = 'none';
                document.getElementById('standartResult').style.display = 'block';
                document.getElementById('no_standart_stand').style.display = 'none';
                document.getElementById('no_standart_geo').style.display = 'none';
                document.getElementById('no_standart_lai').style.display = 'none';
                document.getElementById('no_standart').style.display = 'block';
            } else {
                document.getElementById('roamingResult').style.display = 'none';
                document.getElementById('standartResult').style.display = 'block';
                document.getElementById('no_standart_stand').style.display = 'none';
                document.getElementById('no_standart_geo').style.display = 'none';
                document.getElementById('no_standart_lai').style.display = 'none';
                document.getElementById('no_standart').style.display = 'none';

                if(gsmtype == true){
                    document.getElementById('no_standart_geo').style.display = 'block';
                } else {
                    document.getElementById('no_standart_lai').style.display = 'block';
                }
            }



        }

    }
    var urlgsmtype = 1;
    function openUrl(){
        if(urlgsmtype == 1){
            var url = "/developer_version/public/{{$lang}}/private/roaming/roaming-partners/rouming-more-information-geocell";
        } else {
            if(urlgsmtype == 2){
                var url = "/developer_version/public/{{$lang}}/private/roaming/roaming-partners/rouming-more-information-lailai";
            } else {
                var url = "/developer_version/public/{{$lang}}/private/roaming/roaming-partners/rouming-more-data";
            }
        }
        var win = window.open(url, '_blank');
        win.focus();
    }
</script>

@endsection
