<!-- tsr-section-generic -->
    <div class="tsr-section-generic cs-bg-shade cs-smt-02">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h cs-pdb-40">
          <h1 class="tsr-title cs-cl-blue">{{$item->title}}</h1>
          <div>{!! $item->content !!}</div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic   @if(!$item->content) tsr-section-generic-bd-bot @endif  -->

