@extends('site')

@section('content')
	
	@include('site.gallery.slider')


    @include('site.articles.homepage')
    
    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->
@endsection
