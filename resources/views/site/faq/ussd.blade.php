@extends('site')

@section('content')
    
    @include('site.textcontent_default')

 
    <!-- tsr-section-ussd -->
    <div class="tsr-section-ussd">
      <div class="tsr-container">
       
       @foreach ($ussds as $ussd)

        <div class="tsr-section-ussd-item">
          <div class="tsr-module-ussd">
            <div class="tsr-module-ussd-col-01">
              <div class="cs-tx-22">{{$ussd->title}} {{ ($ussd->brand !="") ? "/":"" }} {{$ussd->brandtitle}}</div>
             <!--  <div><a href="#">More about this service</a></div> -->
            </div>
            <div class="tsr-module-ussd-col-02">
              <div class="tsr-phone-nums">
                <?php $code = str_split($ussd->code);?>

                @foreach($code as $symbol)
                    <span class="tsr-phone-num">{{$symbol}}</span>
                @endforeach
                <span class="tsr-phone-num tsr-phone-num-ok tsr-color-turquoise">ok</span>
              </div>
            </div>

          </div>
        </div>
        @endforeach
       
      </div>
    </div>
    <!-- /tsr-section-ussd -->
    
     <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad">
          <div class="tsr-pagination tsr-pagination-left">
            <div class="tsr-center">
              {!! str_replace('/?', '?',$ussds->render()) !!} 
               
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->  

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->
@endsection