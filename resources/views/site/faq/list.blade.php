@extends('site')

@section('content')
    
    @include('site.textcontent_default')

    
     <!-- tsr-section-generic -->
    <div class="tsr-section-generic">
      <div class="tsr-container">
        
        <div class="tsr-section-generic-pad-h">
          @if(count($subPages)>1) <div class="cs-spb-04 cs-tx-bold">{{trans('site.choosecat')}}:</div>@endif
          
          <div class="tsr-clearfix">
            
            <div class="cs-fl-left">
              <form action="#">
                <div class="tsr-forms">
                  <div class="tsr-clearfix">
                    <div class="cs-fl-left cs-spb-10" style="min-width: 200px; max-width: 260px;">
                    @if(count($subPages)>1)
                      <select  onchange="faqRedirect(this.value);">
                         @foreach($subPages as $faqCat)
                            <option value="{{$lang.'/'.$faqCat->full_slug}}" @if($item->attached_collection_id==$faqCat->attached_collection_id) selected @endif>{{$faqCat->title}}</option>
                         @endforeach
                      </select>
                    @endif  
                    </div>
                  </div>
                </div>
              </form>
            </div>

            @if(isset($item) && $item->id!=79)
              <div class="cs-fl-right cs-spl-20 cs-pdt-10">
                <div class="cs-spb-10 cs-smt-03"><a href="http://geocell.ge/chat" class="composite cs-tx-13 cs-dp-block"><span class="cs-tx-27 cs-tx-bold cs-spr-04">?</span><span class="cs-tx-bold">{{trans('site.ask')}}</span></a>
                </div>
              </div>
             @endif 
            </div>

          </div>

        </div>
      </div>
    </div>
    <!-- /tsr-section-generic -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->

  <!-- tsr-section-faq-listing -->
    <div class="tsr-section-faq-listing">
      <div class="tsr-container">
        <div class="tsr-com-collapser-outer">
          <div class="tsr-com-collapser-inner">
       
          @foreach($faqs as $faq)
       
          <div class="tsr-module-faq">
              <a href="#" class="tsr-module-faq-question" onclick="return false;"><span class="tsr-module-faq-pad">{{$faq->question}} </span></a>
              <div class="tsr-module-faq-answer"><div class="tsr-module-faq-pad">{!! $faq->answer !!}</div></div>
            </div>
          @endforeach  


        </div>
      </div>
    </div>
    </div>
    <!-- /tsr-section-faq -->

    <!-- tsr-section-divider -->
    <div class="tsr-section-divider tsr-divider-empty"></div>
    <!-- /tsr-section-divider -->


@endsection