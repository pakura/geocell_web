@extends('site')

@section('content')
	
	<!-- tsr-section-generic -->
    <div class="tsr-section-generic cs-bg-shade cs-smt-02">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h cs-pdb-40">
          <h1 class="tsr-title cs-cl-blue">{{$item->title}}</h1>
         
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic   @if(!$item->content) tsr-section-generic-bd-bot @endif  -->

	@if(isset($subPages))
	<!-- tsr-section-refiner -->
    <div class="tsr-section-refiner">
      <div class="tsr-refiner-header tsr-clearfix">
        <div class="tsr-container">
          <div class="tsr-refiner-links">
            <ul>
               @foreach($subPages as $subpage)
                <li><a href="{{$lang}}/{{$subpage->full_slug}}" class="{{ Request::is($lang.'/'.$subpage->full_slug) ? 'is-choosen' : '' }}">{{$subpage->title}}</a></li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>

    <!-- tsr-section-generic -->
    <div class="tsr-section-generic ">
      <div class="tsr-container">
        <div class="tsr-section-generic-pad-h cs-pdb-40">
          <div>{!! $item->content !!}</div>
        </div>
      </div>
    </div>
    <!-- /tsr-section-generic   @if(!$item->content) tsr-section-generic-bd-bot @endif  -->

    @endif

@endsection
