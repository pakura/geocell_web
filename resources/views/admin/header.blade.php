<div class="panel panel-default nobottompadding">
	<div class="panel-heading nobottompadding">

		<div class="breadcrumb-env">
		
			<ol class="breadcrumb bc-1">
				<li>
					<a href="{{config('app.cms_slug')}}"><i class="fa-home"></i></a>
				</li>
				@for ($i = 0; $i < count($title); $i++)
				   <li>
				      <a href="@if ($title[$i]['url']){{config('app.cms_slug').'/'.$title[$i]['url']}}@else javascript:; @endif">
				   		{{$title[$i]['title']}}
				   	  </a>	
				   </li>	 
				@endfor
			</ol>
						
		</div>	
		 
	
 		<div class="panel-options">
			 @if($url)
			 <a href="{{config('app.cms_slug')}}/{{$url}}" class="btn btn-success" > 
				 {{ Lang::get('admin.add') }} 
				 <i class="fa-plus"></i>
			 </a> 
			@endif

			<a href="javascript:;" title="settings">
				<i class="fa-arrow-up"></i>
			</a>
			<a href="javascript:;" title="settings">
				<i class="fa-arrow-down"></i>
			</a>

			<a href="javascript:;" title="settings">
				<i class="fa-cog"></i>
			</a>
			
			<a href="javascript:;" data-toggle="reload" title="reload">
				<i class="fa-rotate-right"></i>
			</a>
			
			<a href="javascript:;" title="help">
				<i class="fa-info"></i>
			</a>
		</div>
	</div>
 
</div>


 
					