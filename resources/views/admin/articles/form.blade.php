 <div class="row">
	<div class="col-md-12">

  		<?php 
  			$langs = config('app.langs'); 
  			$disabled = (@$article)?'':'disabled';
  			$toggle = (@$article)?'tab':'';
  		?>

		 	
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>
			
			@foreach ($langs as $key=>$val)
				<li class="{{$disabled}}">
					<a href="#profile-{{$key}}" data-toggle="{{$toggle}}">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">{{$val}}</span>
					</a>
				</li>
			@endforeach	 
			<li class="{{$disabled}} showGallery"  >
				<a href="#files" data-toggle="{{$toggle}}">
					<span class="visible-xs"><i class="fa-cog"></i></span>
					<span class="hidden-xs">Files</span>
				</a>
			</li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="general">
				
				<div class="form-group">
					{!! Form::label('generic_title',Lang::get('admin.generic')) !!}
					{!! Form::text('generic_title', null, ['class'=>'form-control']) !!}					 
				</div>	
				
				<div class="form-group">
					{!! Form::label('slug',Lang::get('admin.slug')) !!}
					{!! Form::text('slug', null, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('collection_id',Lang::get('admin.collection')) !!}
					{!! Form::select('collection_id', $collections, @$collection->id, ['class'=>'form-control']) !!}					 
				</div>
				
				<div class="row"> 
	                <div class="form-group ov_hidden col-md-3">
						{!! Form::label('creation_date',Lang::get('admin.create')) !!}        
						
						<div class="date-and-time">
							{!! Form::text('creation_date', null, ['class'=>'form-control datepicker', 'data-format'=>'dd-mm-yyyy']) !!}	

							<div style="display:none;">
								{!! Form::label('creation_date_time',Lang::get('admin.expire')) !!}
							</div>

							{!! Form::text('creation_date_time', null, ['class'=>'form-control timepicker', 'data-template'=>'dropdown']) !!}	
				 
	                    </div>
					</div>

					<div class="form-group ov_hidden col-md-3">
						{!! Form::label('expire_date',Lang::get('admin.expire')) !!}

						<div class="date-and-time">
							{!! Form::text('expire_date', null , ['class'=>'form-control datepicker', 'data-format'=>'dd-mm-yyyy']) !!}	

							<div style="display:none;">
								{!! Form::label('expire_date_time',Lang::get('admin.expire')) !!}
							</div>

							{!! Form::text('expire_date_time', null, ['class'=>'form-control timepicker', 'data-template'=>'dropdown']) !!}	
				 
	                    </div>					 
					</div>
                </div>

				<div class="form-group">
					{!! Form::label('visibility',Lang::get('admin.visibility')) !!}
					{!! Form::checkbox('visibility',1, 1, ['class'=>'cbr']) !!}					 
				</div>

			</div>
			
		    @foreach ($langs as $key=>$val)
			   
				<div class="tab-pane" id="profile-{{$key}}">
				
					<div class="form-group">
						{!! Form::label("{$key}[title]",Lang::get("admin.title")) !!}
						{!! Form::text("{$key}[title]",  @$article["data"][$key]["title"], ["class"=>"form-control"]) !!}					 
					</div>	

					<div class="form-group">
						{!! Form::label("{$key}[short_title]",Lang::get("admin.short_title")) !!}
						{!! Form::text("{$key}[short_title]", @$article["data"][$key]["short_title"], ["class"=>"form-control"]) !!}					 
					</div>

					<div class="form-group">
						{!! Form::label("{$key}[meta_content]",Lang::get("admin.meta_content")) !!}
						{!! Form::text("{$key}[meta_content]", @$article["data"][$key]["meta_content"], ["class"=>"form-control"]) !!}					 
					</div>
					<div class="form-group">
						{!! Form::label("{$key}[meta_description]",Lang::get("admin.meta_description")) !!}
						{!! Form::text("{$key}[meta_description]",  @$article["data"][$key]["meta_description"], ["class"=>"form-control"]) !!}					 
					</div>

					<div class="form-group collapsed-desc">
						{!! Form::label("{$key}[description]",Lang::get("admin.description")) !!} 
						
						<i class="fa-plus expand-desc-icon"></i>
						<i class="fa-minus collapse-desc-icon"></i>

					    <div class="hide_description">
					    	{!! Form::textarea("{$key}[description]",  @$article["data"][$key]["description"], ["class"=>"textarea hidden"]) !!}	
					    </div>					 
					</div>
					
					<div class="form-group">
						{!! Form::label("{$key}[content]",Lang::get("admin.content")) !!}
						{!! Form::textarea("{$key}[content]",   @$article["data"][$key]["content"], ["class"=>"textarea"]) !!}					 
					</div>
						
				</div>
			@endforeach	
			
			<div class="tab-pane" id="files">
					
				 <div action="{{config('app.cms_slug')}}/articles/uploadfiles/{{@$article->collection_id}}/{{@$article->id}}" class="dropzone dz-clickable">
				 	
				 
				 	<div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                  
				 </div>

			</div>
		</div>
		
		
	</div>				
</div>
<div class="form-group">

	<input type="submit" class="btn btn-success" value="{{Lang::get('admin.save')}}"/>
		
		
	 
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/articles/get/{{@$collection->id?@$collection->id:@$article->collection_id}}" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>
					
			