@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		<?php $titles = [
	    					0=> ['title'=>Lang::get('admin_menu.stock'),'url'=>''], 
	    					1=> ['title'=>Lang::get('admin_menu.stocklist'),'url'=>'stocks/'],
	    					2=> ['title'=>$stock->generic_title,'url'=>''],

		    			]; 
		?>
			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
            @include('errors.admin')

				{!!  Form::model($stock,['url'=>config('app.cms_slug').'/stocks/update/'.$stock->id]) !!}
					
					@include('admin.stocks.form')
					
				{!! Form::close() !!}
			
		</div>


	</div>
</div>
@endsection
