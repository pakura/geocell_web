 <div class="row">
	<div class="col-md-12">

  		<?php 
  			$langs = config('app.langs'); 
  			$disabled = (@$stock)?'':'disabled';
  			$toggle = (@$stock)?'tab':'';
  		?>

		 	
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>
			
			@foreach ($langs as $key=>$val)
				<li class="{{$disabled}}">
					<a href="#profile-{{$key}}" data-toggle="{{$toggle}}">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">{{$val}}</span>
					</a>
				</li>
			@endforeach	 
			 
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="general">
				
				<div class="form-group">
					{!! Form::label('generic_title',Lang::get('admin.generic')) !!}
					{!! Form::text('generic_title', null, ['class'=>'form-control']) !!}					 
				</div>	
				
				<div class="form-group">
					{!! Form::label('contact_phone',Lang::get('admin.contact_phone')) !!}
					{!! Form::text('contact_phone', null, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('contact_email',Lang::get('admin.contact_email')) !!}
					{!! Form::text('contact_email',null, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('visibility',Lang::get('admin.visibility')) !!}
					{!! Form::checkbox('visibility',1, 1, ['class'=>'cbr']) !!}					 
				</div>

			</div>
			
		    @foreach ($langs as $key=>$val)
			   
				<div class="tab-pane" id="profile-{{$key}}">
				
					<div class="form-group">
						{!! Form::label("{$key}[title]",Lang::get("admin.title")) !!}
						{!! Form::text("{$key}[title]",  @$stock["data"][$key]["title"], ["class"=>"form-control"]) !!}					 
					</div>	

					<div class="form-group collapsed-desc">
						{!! Form::label("{$key}[description]",Lang::get("admin.description")) !!} 
						
						<i class="fa-plus expand-desc-icon"></i>
						<i class="fa-minus collapse-desc-icon"></i>

					    <div class="hide_description">
					    	{!! Form::textarea("{$key}[description]",  @$stock["data"][$key]["description"], ["class"=>"textarea hidden"]) !!}	
					    </div>					 
					</div>

					<div class="form-group">
						{!! Form::label("{$key}[address]",Lang::get('admin.address')) !!}
						{!! Form::text("{$key}[address]", @$stock["data"][$key]["address"], ['class'=>'form-control']) !!}						 
					</div>
						
				</div>
			@endforeach	
			
			 
		</div>
		
		
	</div>				
</div>
<div class="form-group">

	<input type="submit" class="btn btn-success" value="{{Lang::get('admin.save')}}"/>
		
		
	 
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/stocks" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>
					
			