@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<!--heading gasatania calke failshi-->
			
			 <?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.stock'),'url'=>''], 
		    					1=> ['title'=>Lang::get('admin_menu.stocklist'),'url'=>''],

			    			];

			  ?>

			@include('admin.header',['title'=>$titles,'url'=>'stocks/create/']) 

			<div class="panel panel-default">
			<table class="table responsive">
					 <thead>
						<tr>
					    	<th class="col-md-5">{{Lang::get('admin.title')}}</th>
					    	
					    	<th class="col-md-5"></th>
					    	<th class="col-md-2">{{Lang::get('admin.actions')}}</th>
					    </tr>
					</thead>  
					 
					<tbody>
					@foreach ($stocks as $stock)
					    <tr>	
					    	<td>
					    		<a href="{{config('app.cms_slug')}}/stocks/edit/{{$stock->id}}">{{ $stock->generic_title }}</a> 
					    	</td>
					    	<td>
					    		
					    	</td>	
					    	<td>
					    	    <a href="{{config('app.cms_slug')}}/stocks/edit/{{$stock->id}}" class="btn btn-warning" title="{{Lang::get('admin.edit')}}"> <i class="fa-pencil"></i></a>

					    		<a href="{{config('app.cms_slug')}}/stocks/delete/{{$stock->id}}" class="btn btn-danger" title="{{Lang::get('admin.delete')}}" onclick="return confirm('Are you sure?')"> <i class="fa-remove"></i></a> 

					    	</td>
					    </tr>	
					@endforeach
					 
					</tbody>
				</table>

				
				
			</div>

		</div>
	</div>
</div>
@endsection
