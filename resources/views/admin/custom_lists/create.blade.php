@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.newsarticles'),'url'=>''], 
		    					1=> ['title'=>$collection->generic_title,'url'=>'custom_lists/get/'.$collection->id],
		    					2=> ['title'=>Lang::get('admin.addnews'),'url'=>''],

			    			];
		     ?>

			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::open(['url'=>config('app.cms_slug').'/custom_lists/save/'.$collection->id]) !!}

					@include('admin.custom_lists.form')
					
				{!! Form::close() !!}
			
		</div>
	</div>
</div>
@endsection
