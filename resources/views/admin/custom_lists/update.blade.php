@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		<?php $titles = [
	    					0=> ['title'=>Lang::get('admin_menu.newsarticles'),'url'=>''], 
	    					1=> ['title'=>$article->ctitle,'url'=>'custom_lists/get/'.$article->cid],
	    					2=> ['title'=>$article->generic_title,'url'=>''],

		    			]; 
		?>
			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::model($article,['url'=>config('app.cms_slug').'/custom_lists/update/'.$article->cid.'/'.$article->id]) !!}
					
					@include('admin.custom_lists.form')
					
				{!! Form::close() !!}
			
		</div>

		<div class="col-md-12 col-md-offset-0 gallery-env">
			<div class="album-images row">
				@if(count(@$article->images)) 
				   @foreach (@$article->images as $image)	
						<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" data-action="parent">
							<div class="album-image">
								<a href="#" class="thumb" data-action="edit">
									<img src="{{config('folders.articles')}}thumbs/{{$image->file}}" class="img-responsive">
								</a>
								
								<a href="#" class="name">
									<span>{{$image->file}}</span>
									<em> </em>
								</a>
								
								<div class="image-options">
									<a href="#" data-action="edit"  data-url="{{config('app.cms_slug')}}/custom_lists/edit-file/{{$article->cid}}/{{$image->id}}"><i class="fa-pencil"></i></a>
									<a href="#" data-action="trash" data-url="{{config('app.cms_slug')}}/custom_lists/delete-file/{{$article->cid}}/{{$image->id}}"><i class="fa-trash"></i></a>
								</div>
								
							    <div class="form-group imagetitle">
									{!! Form::label('title',Lang::get('admin.title')) !!}
									{!! Form::text('title', @$image->title, ['class'=>'form-control','data-url'=>config('app.cms_slug').'/custom_lists/edit-file/'.$article->cid.'/'.$image->id]) !!}					 
								</div>
								<div class="image-checkbox">
									<input type="checkbox" class="cbr default_foto" name="default_foto" {{@$image->file_type==1?"checked":"" }} data-url="{{config('app.cms_slug').'/custom_lists/update-default-foto/'.$article->cid.'/'.$image->id}}" data-relid="{{@$image->article_id}}">
								</div>
							</div>
						</div>
			        @endforeach
				@endif
			</div>	
		</div> 
	</div>
</div>
@endsection
