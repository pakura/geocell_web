@extends('admin')

@section('content')
    <div class="container_">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <a href="cms/workinghour/add" style="float: right" class="btn btn-success">
                            Add
                            <i class="fa-plus"></i>
                        </a>
                        <h1>Offices - Working hours</h1>
                        <hr>
                        <br>
                        <div id="tabledata">
                            <table style="width: 100%; color: #666" id="tab_0">
                                <thead>
                                <tr>
                                    <th style="width: 110px">City</th>
                                    <th style="width: 160px">Address</th>
                                    <th style="width: 230px">Info</th>
                                    <th>Status</th>
                                    <th>Shop</th>
                                    <th>B2B</th>
                                    <th>Coordinates</th>
                                    <th style="width: 100px">Note</th>
                                    <th style="width: 100px">Edit/Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($lists as $key => $office)
                                        <tr>
                                            <td style="width: 110px">{{$office->name_en}}</td>
                                            <td style="width: 160px">{{$office->address_en}}</td>
                                            <td style="width: 230px">{{$office->info_en}}</td>
                                            <td>@if($office->status == 1) Visible @else Hidden @endif</td>
                                            <td>@if($office->shop == 1) Yes @else No @endif</td>
                                            <td>@if($office->b2b == 1) Yes @else No @endif</td>
                                            <td>{{$office->coordinates}}</td>
                                            <td style="width: 100px">{{$office->note_ge}}</td>
                                            <td style="width: 100px">
                                                <a href="cms/workinghour/edit/{{$office->id}}" class="btn btn-warning" title="Edit">
                                                    <i class="fa-pencil"></i>
                                                </a>
                                                <a href="cms/workinghour/delete/{{$office->id}}" class="btn btn-danger" onclick="return confirm('Are you sure?')" title="Delete">
                                                    <i class="fa-remove"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        $(document).ready(function() {
            $('#tab_0').DataTable({
                "iDisplayLength": 25,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        } );

        function filter(){

            var from = $('#from').val();
            var to = $('#to').val();
            var url = '/developer_version/public/cms/newsletters/'+from+'/'+to;
            window.location.href = url;

        }

        function writeData(arg){
            $('#tabledata').html('<table style="width: 100%; color: #666" id="tab_0">\
            <thead>\
            <tr>\
            <th>N</th>\
            <th>Name</th>\
            <th>Quantity</th>\
            </tr>\
            </thead>\
            <tbody>');
            var cnt = 0;
            for (var ii in arg){
                cnt++;
                $('#tabledata').append('<tr>\
                        <td>'+(cnt++)+'</td>\
                        <td style="background-color: #7722ff; color: #FFF;">'+sku2text(ii)+'</td>\
                <td></td>\
                </tr>');
                for (var jj in arg[ii]){

                }
            }

            $('#tabledata').append('</tbody>\
                    </table>');
        }



    </script>
@endsection
