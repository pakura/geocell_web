@extends('admin')

@section('content')
    <div class="container_">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>Offices - Working hours</h1>
                        <br>
                        <br>
                        <form action="/developer_version/public/cms/workinghour/update/{{$office->id}}" method="post">
                            <label for="city_id">City:</label><br>
                            <select name="city_id">
                                @foreach($cities as $key => $city)
                                    <option value="{{$city->id}}" @if($city->id == $office->city_id) selected @endif>{{$city->name_en}}</option>
                                @endforeach
                            </select>
                            <br><br>
                            <label for="address_en">Address (en):</label><br>
                            <textarea id="address_en" name="address_en">{{isset($office->address_en)?$office->address_en:''}}</textarea>
                            <br>
                            <label for="address_ge">Address (ge):</label><br>
                            <textarea id="address_ge" name="address_ge">{{isset($office->address_ge)?$office->address_ge:''}}</textarea>
                            <br><br>
                            <label for="info_en">Info (en):</label><br>
                            <textarea id="info_en" name="info_en">{{isset($office->info_en)?$office->info_en:''}}</textarea>
                            <br>
                            <label for="info_ge">Info (ge):</label><br>
                            <textarea id="info_ge" name="info_ge">{{isset($office->info_ge)?$office->info_ge:''}}</textarea>
                            <br><br>
                            <label for="status">Status:</label><br>
                            <select name="status">
                                <option value="1">Visible</option>
                                <option value="0" @if($office->status == 0) selected @endif>Invisible</option>
                            </select>

                            <br><br>
                            <label for="shop">Shop:</label><br>
                            <select name="shop">
                                <option value="1">Yes</option>
                                <option value="0" @if($office->shop == 0) selected @endif>No</option>
                            </select>

                            <br><br>
                            <label for="b2b">B2B:</label><br>
                            <select name="b2b">
                                <option value="1">Yes</option>
                                <option value="0" @if($office->b2b == 0) selected @endif>No</option>
                            </select>
                            <br><br>
                            <label for="coordinates">Coordinates:</label><br>
                            <input type="text" name="coordinates" value="{{isset($office->coordinates)?$office->coordinates:''}}">

                            <br><br>
                            <label for="note_en">Note (en):</label><br>
                            <textarea id="note_en" name="note_en">{{isset($office->note_en)?$office->note_en:''}}</textarea>
                            <br>
                            <label for="note_ge">Note (ge):</label><br>
                            <textarea id="note_ge" name="note_ge">{{isset($office->note_ge)?$office->note_ge:''}}</textarea>

                            <br><br><br><br>
                            <input type="submit" value="Save" class="btn btn-success"/>
                            <a href="/workinghour"><button class="btn btn-warning">Cancel</button></a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
