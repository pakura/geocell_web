@foreach (@$product_stocks as $stock) 
<div class="row" data-action="parent">
	<div class="form-group col-md-2">
		 {{$stock->generic_title}}
	</div>	
	<div class="form-group col-md-2">
		 {{$stock->amount_in_stock}}
	</div>	
 	

	<div class="form-group col-md-1 col-md-offset-6">
		 <a href="#" class="btn btn-danger remove-from-stock" data-stockid="{{$stock->stock_id}}"  data-url="{{config('app.cms_slug')}}/products/remove-from-stock/{{$stock->id}}">
			<i class="fa-remove"></i>
		</a> 
		 <a href="#"   class="btn btn-success show-prices" data-stockid="{{$stock->stock_id}}">
			 {{Lang::get('admin.prices')}}
		</a> 
	</div>	

</div>		
@endforeach
