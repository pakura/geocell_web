 <div class="row">
	<div class="col-md-12">

  		<?php 
  			$langs = config('app.langs'); 
  			$disabled = (@$product)?'':'disabled';
  			$toggle = (@$product)?'tab':'';
  		?>

		 	
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">{{Lang::get('admin.general')}}</span>
				</a>
			</li>
			
			@foreach ($langs as $key=>$val)
				<li class="{{$disabled}}">
					<a href="#profile-{{$key}}" data-toggle="{{$toggle}}">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">{{$val}}</span>
					</a>
				</li>
			@endforeach	 

			<li class="{{$disabled}} ">
				<a href="#prices" data-toggle="{{$toggle}}">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">{{Lang::get('admin.prices')}}</span>
				</a>
			</li>

			<li class="{{$disabled}} ">
				<a href="#stocks" data-toggle="{{$toggle}}">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">{{Lang::get('admin_menu.stock')}}</span>
				</a>
			</li>

			<li class="{{$disabled}} ">
				<a href="#options" data-toggle="{{$toggle}}">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">{{Lang::get('admin.options')}}</span>
				</a>
			</li>

			<li class="{{$disabled}} showGallery">
				<a href="#files" data-toggle="{{$toggle}}">
					<span class="visible-xs"><i class="fa-cog"></i></span>
					<span class="hidden-xs">{{Lang::get('admin.files')}}</span>
				</a>
			</li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="general">
				
				<div class="form-group">
					{!! Form::label('generic_title',Lang::get('admin.generic')) !!}
					{!! Form::text('generic_title', null, ['class'=>'form-control']) !!}					 
				</div>	
				
				<div class="form-group">
					{!! Form::label('slug',Lang::get('admin.slug')) !!}
					{!! Form::text('slug', null, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('sku',Lang::get('admin.sku')) !!}
					{!! Form::text('sku', null, ['class'=>'form-control']) !!}					 
				</div>
				<div class="form-group">
					{!! Form::label('sku2',Lang::get('admin.sku')) !!}
					{!! Form::text('sku2', null, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('collection_id',Lang::get('admin.collection')) !!}
					{!! Form::select('collection_id', $collections, @$collection->id, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('brand_id',Lang::get('admin_menu.brands')) !!}
					{!! Form::select('brand_id', $brands, @$product->brand_id, ['class'=>'form-control']) !!}					 
				</div>

				
				<div class="row"> 
	                <div class="form-group ov_hidden col-md-3">
						{!! Form::label('creation_date',Lang::get('admin.create')) !!}        
						
						<div class="date-and-time">
							{!! Form::text('creation_date', null, ['class'=>'form-control datepicker', 'data-format'=>'dd-mm-yyyy']) !!}	
							{!! Form::text('creation_date_time', '10:00 AM', ['class'=>'form-control timepicker', 'data-template'=>'dropdown']) !!}	
				 
	                    </div>
					</div>

					<div class="form-group ov_hidden col-md-3">
						{!! Form::label('expire_date',Lang::get('admin.expire')) !!}

						<div class="date-and-time">
							{!! Form::text('expire_date', null , ['class'=>'form-control datepicker', 'data-format'=>'dd-mm-yyyy']) !!}	
							{!! Form::text('expire_date_time', '08:00 PM', ['class'=>'form-control timepicker', 'data-template'=>'dropdown']) !!}	
				 
	                    </div>					 
					</div>
                </div>

                <div class="form-group">
					{!! Form::label('pinned',Lang::get('admin.pinned')) !!}
					{!! Form::checkbox('pinned',null, isset($product)?$product->visibility:false, ['class'=>'cbr']) !!}					 
				</div>
				<div class="form-group">
					{!! Form::label('home_page',Lang::get('admin.home_page')) !!}
					{!! Form::checkbox('home_page',null, isset($product)?$product->visibility:false, ['class'=>'cbr']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('visibility',Lang::get('admin.visibility')) !!}
					{!! Form::checkbox('visibility',1, isset($product)?$product->visibility:true, ['class'=>'cbr']) !!}					 
				</div>

			</div> <!--General-->

		   <!--Languages-->
			
		    @foreach ($langs as $key=>$val)
			   
				<div class="tab-pane" id="profile-{{$key}}">
				
					<div class="form-group">
						{!! Form::label("{$key}[title]",Lang::get("admin.title")) !!}
						{!! Form::text("{$key}[title]",  @$product["data"][$key]["title"], ["class"=>"form-control"]) !!}					 
					</div>	

					<div class="form-group">
						{!! Form::label("{$key}[short_title]",Lang::get("admin.short_title")) !!}
						{!! Form::text("{$key}[short_title]", @$product["data"][$key]["short_title"], ["class"=>"form-control"]) !!}					 
					</div>

					<div class="form-group">
						{!! Form::label("{$key}[meta_content]",Lang::get("admin.meta_content")) !!}
						{!! Form::text("{$key}[meta_content]", @$product["data"][$key]["meta_content"], ["class"=>"form-control"]) !!}					 
					</div>
					<div class="form-group">
						{!! Form::label("{$key}[meta_description]",Lang::get("admin.meta_description")) !!}
						{!! Form::text("{$key}[meta_description]",  @$product["data"][$key]["meta_description"], ["class"=>"form-control"]) !!}					 
					</div>

					<div class="form-group collapsed-desc">
						{!! Form::label("{$key}[description]",Lang::get("admin.description")) !!} 
						
						<i class="fa-plus expand-desc-icon"></i>
						<i class="fa-minus collapse-desc-icon"></i>

					    <div class="hide_description">
					    	{!! Form::textarea("{$key}[description]",  @$product["data"][$key]["description"], ["class"=>"textarea hidden"]) !!}	
					    </div>					 
					</div>
					
					<div class="form-group">
						{!! Form::label("{$key}[content]",Lang::get("admin.content")) !!}
						{!! Form::textarea("{$key}[content]",   @$product["data"][$key]["content"], ["class"=>"textarea"]) !!}					 
					</div>

					<div class="form-group collapsed-desc">
						{!! Form::label("{$key}[services_and_features]",Lang::get('admin.services_and_features')) !!} 
						
						<i class="fa-plus expand-desc-icon"></i>
						<i class="fa-minus collapse-desc-icon"></i>

					    <div class="hide_description">
					    	{!! Form::textarea("{$key}[services_and_features]",  @$product["data"][$key]["services_and_features"], ['class'=>'textarea hidden']) !!}	
					    </div>					 
					</div>

					<div class="form-group collapsed-desc">
						{!! Form::label("{$key}[terms_and_conditions]",Lang::get('admin.terms_and_conditions')) !!} 
						
						<i class="fa-plus expand-desc-icon"></i>
						<i class="fa-minus collapse-desc-icon"></i>

					    <div class="hide_description">
					    	{!! Form::textarea("{$key}[terms_and_conditions]", @$product["data"][$key]["terms_and_conditions"], ['class'=>'textarea hidden']) !!}	
					    </div>					 
					</div>


					<div class="form-group collapsed-desc">
						{!! Form::label("{$key}[service_activation_msg]",Lang::get('admin.service_activation_msg')) !!} 
						
						<i class="fa-plus expand-desc-icon"></i>
						<i class="fa-minus collapse-desc-icon"></i>

					    <div class="hide_description">
					    	{!! Form::textarea("{$key}[service_activation_msg]", @$product["data"][$key]["service_activation_msg"], ['class'=>'textarea hidden']) !!}	
					    </div>					 
					</div>

					<div class="form-group collapsed-desc">
						{!! Form::label("{$key}[service_activation_msg2]",Lang::get('admin.service_activation_msg2')) !!} 
						
						<i class="fa-plus expand-desc-icon"></i>
						<i class="fa-minus collapse-desc-icon"></i>

					    <div class="hide_description">
					    	{!! Form::textarea("{$key}[service_activation_msg2]", @$product["data"][$key]["service_activation_msg2"], ['class'=>'textarea hidden']) !!}	
					    </div>					 
					</div>
						
				</div>
			@endforeach	 <!--langs-->
            
            <!--prices -->
			<div class="tab-pane" id="prices">
			   @if (@$product) 
					<div class="row list-head">
						<div class="col-md-2"><strong>{{Lang::get('admin.title')}} Geo</strong></div>
						<div class="col-md-2"><strong>Additional {{Lang::get('admin.title')}}</strong></div>
						<div class="col-md-2"><strong>{{Lang::get('admin.type')}}</strong></div>
						<div class="col-md-2"><strong>{{Lang::get('admin.price')}}</strong></div>
						<div class="col-md-2"><strong>{{Lang::get('admin.currency')}}</strong></div>
						<div class="col-md-1 "><strong>{{Lang::get('admin.actions')}}</strong></div>
					</div>

					<div class="list-content" id="price_list">
					@if (count(@$product->prices))
					
						@include('admin.products.prices',['prices'=>$product->prices])
					
					@endif
					</div>

					<div class="row">
						<div class="form-group col-md-2">
							<input type="text" class="form-control" value="" name="price_title" id="price_title">
						</div>	
						<div class="form-group col-md-2">
							<input type="text" class="form-control" value="" name="price_title_en" id="price_title_en">
						</div>	

						<div class="form-group col-md-2">
							<select class="form-control"   name="price_type" id="price_type">
								<option value="1">{{Lang::get('admin.mprice')}}</option>
								<option value="2">{{Lang::get('admin.aprice')}}</option>
							</select>
						</div>	

						<div class="form-group col-md-2">
							<input type="text" class="form-control" value="" name="price_value" id="price_value">
						</div>	
						<div class="form-group col-md-2">
							<input type="text" class="form-control" value="" name="currency" id="currency">
						</div>	
						
						<div class="col-md-1 ">
							<a href="javascript:;" class="btn btn-success add-price" data-url= "{{config('app.cms_slug')}}/products/add-price">
								<i class="fa-plus"></i>
							</a>
						</div> 
					</div>
					<div class="row">
						<div class="form-group col-md-2">
							   
							{!! Form::select('product_stock_id',  array('default' => 'Choose stock') + @$product->stocks_ids, null, ['class'=>'form-control','id'=>'product_stock_id']) !!}	
							
						</div>
					</div>
				@endif
			</div> <!--prices-->
              
            <!--stocks-->  
			<div class="tab-pane" id="stocks">
			    @if (@$product)
					<div class="row list-head">
						<div class="col-md-2">{{Lang::get('admin.title')}}</div>
						<div class="col-md-2">{{Lang::get('admin.amount')}}</div>

						<div class="col-md-1 col-md-offset-6">{{Lang::get('admin.actions')}}</div>
						
					</div>
					<div  id="stocklist" class="list-content">	
						@if (count(@$product->stocks))
						 					 
							@include('admin.products.stocks',['product_stocks'=>@$product->stocks])
						 
						@endif
					</div>	

					<div class="row">
						<div class="form-group col-md-2">
							{!! Form::label('stock_id',Lang::get('admin_menu.stock')) !!}
							<select name="stock_id" id="stock_id" class="form-control" required>
								<option>Select stock</option>
								@foreach (@$stocks as $stockid=>$stock_title)
								    {!! $disabledopt =  (array_key_exists(@$stockid, @$product->stocks_ids) ? ' disabled="disabled"' : '') !!}
									<option value="{{$stockid}}" {{$disabledopt}}> {{$stock_title}} </option>
								    
								@endforeach
							</select>
							 				 
						</div>

						<div class="form-group col-md-2">
							{!! Form::label('amount',Lang::get('admin.amount')) !!}
							{!! Form::text('amount', null,  ['class'=>'form-control']) !!}	
						</div>	

						<div class="form-group col-md-1">

							<a href="#" class="btn btn-success add-in-stock" data-url= "{{config('app.cms_slug')}}/products/add-in-stock">
								<i class="fa-plus"></i>
							</a>
						</div>	
						<div class="form-group col-md-1">
							 
						</div>	
						 
					</div>
				@endif
			</div> <!--stocks-->
			
			<!--stocks-->  
			<div class="tab-pane" id="options">
			    @if (@$product)
					<div class="row list-head">
						<div class="col-md-2">{{Lang::get('admin.title')}}</div>
						<div class="col-md-2">{{Lang::get('admin.value')}}</div>
						<div class="col-md-2">{{Lang::get('admin.sku')}}</div>

						<div class="col-md-1 col-md-offset-6">{{Lang::get('admin.actions')}}</div>
						
					</div>
					<div  id="optionslist" class="list-content">	
						@if (count(@$options))
						 					 
							@include('admin.products.options',['product_options'=>@$options])
						 
						@endif
					</div>	

				 
				@endif
			</div> <!--options-->

			<div class="tab-pane" id="files">
					
				 <div action="{{config('app.cms_slug')}}/products/uploadfiles/{{@$product->id}}" class="dropzone dz-clickable">
				 	
				 
				 	<div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                  
				 </div>

			</div>
		</div>
		
		
	</div>				
</div>
<div class="form-group">

	<input type="submit" class="btn btn-success" value="{{Lang::get('admin.save')}}"/>
		
		
	 
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/products/get/{{@$collection->id?@$collection->id:@$product->collection_id}}" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>
					
			