@foreach (@$product_options as $option) 
	<div class="row parent-{{$option->id}}" data-action="parent" >
		<div class="form-group col-md-2">
			 {{$option->generic_title}}
		</div>	
		<div class="form-group col-md-2">
			{!! Form::text('value', @$option->value,  ['class'=>'form-control option_value']) !!}	
		</div>	
		<div class="form-group col-md-2">
			{!! Form::text('sku_code', @$option->sku_code,  ['class'=>'form-control sku_code']) !!}	
		</div>	
		<div class="form-group col-md-2">
			  
		</div>

		<div class="form-group col-md-1 col-md-offset-2">
			 <a href="javascript:;" class="btn btn-success save-option" data-url= "{{config('app.cms_slug')}}/products/save-option/{{$option->id}}">
				<i class="fa-plus"></i>
			</a>
		</div>	
	</div>	
@endforeach
