@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<!--heading gasatania calke failshi-->
			
			 <?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.stock'),'url'=>''], 
		    					1=> ['title'=>Lang::get('admin_menu.prodcats'),'url'=>'collections/get/4'], 
		    					2=> ['title'=>$collection->generic_title,'url'=>'products/get/'.$collection->id],

			    			];

			  ?>

			@include('admin.header',['title'=>$titles,'url'=>'products/create/'.$collection->id]) 

			<div class="panel panel-default">
			<table class="table responsive">
					 <thead>
						<tr>
					    	<th class="col-md-5">{{Lang::get('admin.title')}}</th>
					    	
					    	<th class="col-md-4"></th>
					    	<th class="col-md-1">Id</th>
					    	<th class="col-md-2">{{Lang::get('admin.actions')}}</th>
					    </tr>
					</thead>  
					 
					<tbody>
					@foreach ($products as $product)
					    <tr>	
					    	<td>
					    		<a href="{{config('app.cms_slug')}}/products/edit/{{$product->collection_id}}/{{$product->id}}">{{ $product->generic_title }}</a> 
					    	</td>
					    	<td>
					    		
					    	</td>
					    	<td>
					    		{{$product->id}}
					    	</td>	
					    	<td>
					    		<a href="{{config('app.cms_slug')}}/products/edit/{{$product->collection_id}}/{{$product->id}}/#files" class="btn btn-default" title="{{Lang::get('admin.files')}}" >
					    	    	<i class="fa-paperclip"></i>
					    	    </a>
					    	    <a href="{{config('app.cms_slug')}}/products/edit/{{$product->collection_id}}/{{$product->id}}" class="btn btn-warning" title="{{Lang::get('admin.edit')}}"> <i class="fa-pencil"></i></a>

					    		<a href="{{config('app.cms_slug')}}/products/delete/{{$product->collection_id}}/{{$product->id}}" class="btn btn-danger" title="{{Lang::get('admin.delete')}}" onclick="return confirm('Are you sure?')"> <i class="fa-remove"></i></a> 

					    	</td>
					    </tr>	
					@endforeach
					 
					</tbody>
				</table>

				
				
			</div>

		</div>
	</div>
</div>
@endsection
