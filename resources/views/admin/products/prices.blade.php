@foreach (@$prices as $price) 
	<div class="row parent-{{$price->stock_id}}" data-action="parent" >
		<div class="form-group col-md-2">
			 {{$price->title_ge}}
		</div>	
		<div class="form-group col-md-2">
			 {{$price->title_en}}
		</div>	
		<div class="form-group col-md-2">
			 {{$price->type}}
		</div>	
		<div class="form-group col-md-2">
			 {{$price->value}}
		</div>	
		<div class="form-group col-md-2">
			 {{$price->currency}}
		</div>

		<div class="form-group col-md-1 ">
			 <a href="#" class="btn btn-danger remove-price"  data-stockid="{{$price->stock_id}}"  data-url="{{config('app.cms_slug')}}/products/remove-price/{{$price->id}}">
				<i class="fa-remove"></i>
			</a> 
		</div>	
	</div>	
@endforeach
