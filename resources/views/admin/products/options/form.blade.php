 <div class="row">
	<div class="col-md-12">

  		<?php 
  			$langs = config('app.langs'); 
  			$disabled = (@$option)?'':'disabled';
  			$toggle = (@$option)?'tab':'';
  		?>

		 	
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>
			
			@foreach ($langs as $key=>$val)
				<li class="{{$disabled}}">
					<a href="#profile-{{$key}}" data-toggle="{{$toggle}}">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">{{$val}}</span>
					</a>
				</li>
			@endforeach	 
		 
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="general">
				
				<div class="form-group">
					{!! Form::label('generic_title',Lang::get('admin.generic')) !!}
					{!! Form::text('generic_title', null, ['class'=>'form-control']) !!}					 
				</div>	

				<div class="form-group">
					{!! Form::label('collection_id',Lang::get('admin.collection')) !!}
					{!! Form::select('collection_id[]', $collections, @$collections_ids, ['class'=>'form-control','multiple','style'=>'min-height:300px;']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('color','Color') !!}
					{!! Form::text('color', null, ['class'=>'form-control']) !!}					 
				</div>	

				<div class="form-group">
					{!! Form::label('option_type','Option Type') !!}
					{!! Form::select('option_type', config('app.option_type'), null, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('type',Lang::get('admin.type')) !!}
					{!! Form::select('type', config('app.settings_type'), null, ['class'=>'form-control']) !!}					 
				</div>

			</div>
			
		    @foreach ($langs as $key=>$val)
			   
				<div class="tab-pane" id="profile-{{$key}}">
				
					<div class="form-group">
						{!! Form::label("{$key}[title]",Lang::get("admin.title")) !!}
						{!! Form::text("{$key}[title]",  @$option["data"][$key]["title"], ["class"=>"form-control"]) !!}					 
					</div>	

					<div class="form-group collapsed-desc">
						{!! Form::label("{$key}[description]",Lang::get("admin.description")) !!} 
						
						<i class="fa-plus expand-desc-icon"></i>
						<i class="fa-minus collapse-desc-icon"></i>

					    <div class="hide_description">
					    	{!! Form::textarea("{$key}[description]",  @$option["data"][$key]["description"], ["class"=>"textarea hidden"]) !!}	
					    </div>					 
					</div>

					<div class="form-group">
						{!! Form::label("{$key}[initial]",Lang::get('admin.initial')) !!}
						{!! Form::text("{$key}[initial]", @$option["data"][$key]["initial"], ['class'=>'form-control']) !!}					 
					</div>

						
				</div>
			@endforeach	
 
		</div>
		
		
	</div>				
</div>
<div class="form-group">

	<input type="submit" class="btn btn-success" value="{{Lang::get('admin.save')}}"/>
		
		
	 
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/product-options" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>
					
			