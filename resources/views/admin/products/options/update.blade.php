@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		<?php $titles = [
	    					0=> ['title'=>Lang::get('admin_menu.stock'),'url'=>''], 
	    					1=> ['title'=>Lang::get('admin_menu.prodoptions'),'url'=>'product-options'], 
	    					2=> ['title'=>$option->generic_title,'url'=>''],

		    			]; 
		?>
			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::model($option,['url'=>config('app.cms_slug').'/product-options/update/'.$option->id]) !!}
					
					@include('admin.products.options.form')
					
				{!! Form::close() !!}
			
		</div>

	 
	</div>
</div>
@endsection
