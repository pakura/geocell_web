@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.stock'),'url'=>''], 
		    					1=> ['title'=>Lang::get('admin_menu.prodcats'),'url'=>'collections/get/4'], 
		    					2=> ['title'=>$collection->generic_title,'url'=>'products/get/'.$collection->id],
		    					3=> ['title'=>Lang::get('admin.addproduct'),'url'=>''],

			    			];
		     ?>

			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::open(['url'=>config('app.cms_slug').'/products/save/'.$collection->id]) !!}

					@include('admin.products.form')
					
				{!! Form::close() !!}
			
		</div>
	</div>
</div>
@endsection
