@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		<?php $titles = [
	    					0=> ['title'=>Lang::get('admin_menu.stock'),'url'=>''], 
	    					1=> ['title'=>Lang::get('admin_menu.prodcats'),'url'=>'collections/get/4'], 
	    					2=> ['title'=>$product->ctitle,'url'=>'products/get/'.$product->cid],
	    					3=> ['title'=>$product->generic_title,'url'=>''],

		    			]; 
		?>
			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::model($product,['url'=>config('app.cms_slug').'/products/update/'.$product->cid.'/'.$product->id]) !!}
					
					{!! Form::hidden('product_id',$product->id, ['id'=>'product_id']) !!}

					@include('admin.products.form')
					
				{!! Form::close() !!}
			
		</div>

		<div class="col-md-12 col-md-offset-0 gallery-env">
			<div class="album-images row">

				@if(count(@$product->images)) 
				   @foreach (@$product->images as $image)	
						<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" data-action="parent">
							<div class="album-image">
								<div class="colorSelect_wp">
									<div class="color_btn" onclick="$('#colorFor{{$image->id}}').fadeToggle()"></div>
									<div class="all_color" id="colorFor{{$image->id}}">
										@foreach($colors as $value)
											<div class="thiscolor" onclick="changeImgColor({{$image->id}}, '{{$value->color}}')" style="background-color: {{$value->color}};" title="{{$value->generic_title}}"></div>

										@endforeach
									</div>
								</div>
								<a href="#" class="thumb" data-action="edit">
									<img src="{{config('folders.products')}}thumbs/{{$image->file}}" class="img-responsive">
								</a>
								
								<a href="#" class="name">
									<span>{{$image->file}}</span>
									<em> </em>
								</a>
								
								<div class="image-options">
									<a href="#" data-action="edit"  data-url="{{config('app.cms_slug')}}/products/edit-file/{{$image->id}}"><i class="fa-pencil"></i></a>
									<a href="#" data-action="trash" data-url="{{config('app.cms_slug')}}/products/delete-file/{{$image->id}}"><i class="fa-trash"></i></a>
								</div>
								
							    <div class="form-group imagetitle">
									{!! Form::label('title',Lang::get('admin.title')) !!}
									{!! Form::text('title', @$image->title, ['class'=>'form-control','data-url'=>config('app.cms_slug').'/products/edit-file/'.$image->id]) !!}					 
								</div>
								<div class="image-checkbox">
									<input type="checkbox" class="cbr default_foto" name="default_foto" {{@$image->file_type==1?"checked":"" }} data-url="{{config('app.cms_slug').'/products/update-default-foto/'.$image->id}}" data-relid="{{@$image->product_id}}">
								</div>
							</div>
						</div>
			        @endforeach
				@endif
			</div>	
		</div> 
	</div>
</div>

	<script>
		function changeImgColor(id, color){
			color = color.replace('#', '');
			$.ajax({
				method: "GET",
				url: "/developer_version/public/cms/products/changecolor/"+id+"/"+color
			})
			.done(function( msg ) {
				$('#colorFor'+id).fadeOut();
			});
		}
	</script>
	<style>

		.colorSelect_wp{
			float: left;
			min-width: 20px;
			min-height: 28px;
			background-color: rgba(156, 156, 156, 0.3);
			border-radius: 5px;
			cursor: pointer;
			position: absolute;
			padding: 4px;
			z-index: 9;
		}

		.colorSelect_wp .color_btn{
			float: left;
			width: 20px;
			height: 20px;
			background-image: url('https://geocell.ge/developer_version/public/admin/images/colorchange.png');
			background-repeat: no-repeat;
			background-size: 80% 80%;
			background-position: center center;
		}
		.colorSelect_wp .color_btn:hover{
			opacity: 0.7;
		}
		.colorSelect_wp .all_color{
			float: left;
			min-height: 20px;
			display: none;
		}
		.colorSelect_wp .all_color .thiscolor{
			float: left;
			width: 16px;
			margin-left: 4px;
			height: 16px;
			margin-top: 2px;
			border-radius: 10px;
			border: 1px solid #000;
		}
	</style>
@endsection
