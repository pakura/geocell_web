@extends('admin')

@section('content')
    <div class="container_">
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                <!--heading gasatania calke failshi-->

                <?php $titles = [
                        0=> ['title'=>Lang::get('admin_menu.international'),'url'=>''],


                ];

                ?>

                @include('admin.header',['title'=>$titles,'url'=>'internationalline/add'])

                <ul class="nav nav-tabs">
                    {{--  $key==config('app.locale')? 'active':''--}}
                    <li class="">
                        <a href="{{config('app.cms_slug')}}/internationalline/en" >
                            <span class="visible-xs"><i class="fa-user"></i></span>
                            <span class="hidden-xs">English</span>
                        </a>
                    </li>

                    <li class="">
                        <a href="{{config('app.cms_slug')}}/internationalline/ge" >
                            <span class="visible-xs"><i class="fa-user"></i></span>
                            <span class="hidden-xs">Georgian</span>
                        </a>
                    </li>


                </ul>
                <div class="panel panel-default">


                    <table class="table responsive">
                        <thead>
                        <tr>
                            <th class="col-md-6">country</th>
                            <th class="col-md-2">Price</th>
                            <th class="col-md-2">Discount</th>
                            <th class="col-md-2">Country code</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($lists as $list)
                            <tr>
                                <td>
                                    {{ $list->title }}
                                </td>
                                <td>
                                    {{ $list->gel_min }} @if( $list->gel_min_nodiscount != ''){{'- '.$list->gel_min_nodiscount}} @endif
                                </td>
                                <td>
                                    @if($list->has_discount == 0) No @elseif($list->has_discount == 1) Yes @else Both @endif
                                </td>
                                <td>
                                    {{$list->country_code}}
                                </td>
                                <td style="min-width:120px!important;">
                                    <a href="{{config('app.cms_slug')}}/internationalline/edit/{{$list->id}}" class="btn btn-warning" title="{{Lang::get('admin.edit')}}">
                                        <i class="fa-pencil"></i>
                                    </a>
                                    <a href="{{config('app.cms_slug')}}/internationalline/delete/{{$list->id}}" class="btn btn-danger" title="Delete" onclick="return confirm('Are you sure?')">
                                        <i class="fa-remove"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>



                </div>

            </div>
        </div>
    </div>
@endsection
