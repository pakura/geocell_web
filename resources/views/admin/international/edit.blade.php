@extends('admin')

@section('content')
    <div class="container_">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>{{Lang::get('admin_menu.international')}}</h1>
                        <br>
                        <br>
                        <form action="/developer_version/public/cms/internationalline/update/{{$id}}" method="post">
                            <label for="country">Country Name:</label><br>
                            <input type="text" id="country" name="country" required @if(isset($list->title)) value="{{$list->title}}" @endif>
                            <br><br>
                            <label for="price_discount">Price (all):</label><br>
                            <input type="text" id="price_discount" name="price_discount" required @if(isset($list->gel_min)) value="{{$list->gel_min}}" @endif>
                            <br><br>
                            <label for="price_nodiscount">Price (no discount):</label><br>
                            <input type="text" id="price_nodiscount" name="price_nodiscount" @if(isset($list->gel_min_nodiscount)) value="{{$list->gel_min_nodiscount}}" @endif>
                            <br><br>
                            <label for="group">Country group:</label><br>
                            <select name="group">
                                @if(isset($list->country_group))
                                    <option value="Russia, USA, Canada"  @if($list->country_group == 'Russia, USA, Canada') selected @endif >Russia, USA, Canada</option>
                                    <option value="South America, the Pacific, Africa, Asia" @if($list->country_group == 'South America, the Pacific, Africa, Asia') selected @endif >South America, the Pacific, Africa, Asia</option>
                                    <option value="CIS, Europe, Asia, South Africa" @if($list->country_group == 'CIS, Europe, Asia, South Africa') selected @endif >CIS, Europe, Asia, South Africa</option>
                                    <option value="Satellite Connection" @if($list->country_group == 'Satellite Connection') selected @endif >Satellite Connection</option>
                                    <option value="რუსეთი, აშშ, კანადა" @if($list->country_group == 'რუსეთი, აშშ, კანადა') selected @endif >რუსეთი, აშშ, კანადა</option>
                                    <option value="სამხრეთ ამერიკა, წყნარი ოკეანის ქვეყნები, აფრიკა," @if($list->country_group == 'სამხრეთ ამერიკა, წყნარი ოკეანის ქვეყნები, აფრიკა,') selected @endif >სამხრეთ ამერიკა, წყნარი ოკეანის ქვეყნები, აფრიკა,</option>
                                    <option value="დსთ, ევროპა, აზია, სამხრეთ აფრიკა" @if($list->country_group == 'დსთ, ევროპა, აზია, სამხრეთ აფრიკა') selected @endif >დსთ, ევროპა, აზია, სამხრეთ აფრიკა</option>
                                    <option value="სატელიტური კავშირი" @if($list->country_group == 'სატელიტური კავშირი') selected @endif >სატელიტური კავშირი</option>
                                @else
                                    <option value="Russia, USA, Canada">Russia, USA, Canada</option>
                                    <option value="South America, the Pacific, Africa, Asia">South America, the Pacific, Africa, Asia</option>
                                    <option value="CIS, Europe, Asia, South Africa">CIS, Europe, Asia, South Africa</option>
                                    <option value="Satellite Connection">Satellite Connection</option>
                                    <option value="რუსეთი, აშშ, კანადა">რუსეთი, აშშ, კანადა</option>
                                    <option value="სამხრეთ ამერიკა, წყნარი ოკეანის ქვეყნები, აფრიკა">სამხრეთ ამერიკა, წყნარი ოკეანის ქვეყნები, აფრიკა,</option>
                                    <option value="დსთ, ევროპა, აზია, სამხრეთ აფრიკა">დსთ, ევროპა, აზია, სამხრეთ აფრიკა</option>
                                    <option value="სატელიტური კავშირი">სატელიტური კავშირი</option>
                                @endif
                            </select>
                            <br><br>
                            <label for="discount">Discount:</label><br>
                            <select name="discount">
                                <option value="0" @if($list->has_discount == '0') selected @endif>No</option>
                                <option value="1" @if($list->has_discount == '1') selected @endif>Yes</option>
                                <option value="2" @if($list->has_discount == '2') selected @endif>Both</option>
                            </select>
                            <br><br>
                            <label for="code">Country code:</label><br>
                            <input type="text" id="code" name="code" required  @if(isset($list->country_code)) value="{{$list->country_code}}" @endif>
                            <br><br>
                            <input type="submit" value="Save" class="btn btn-success"/>
                            <a href="/developer_version/public/cms/internationalline/en"><button class="btn btn-warning">Cancel</button></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
