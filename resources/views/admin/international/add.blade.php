@extends('admin')

@section('content')
    <div class="container_">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>{{Lang::get('admin_menu.international')}}</h1>
                        <br>
                        <br>

                        <form action="/developer_version/public/cms/internationalline/save" method="post">
                            <label for="country">Country Name:</label><br>
                            <input type="text" id="country" name="country">
                            <br><br>
                            <label for="price_discount">Price (all):</label><br>
                            <input type="text" id="price_discount" name="price_discount" >
                            <br><br>
                            <label for="price_nodiscount">Price (no dicount):</label><br>
                            <input type="text" id="price_nodiscount" name="price_nodiscount">
                            <br><br>
                            <br><br>
                            <label for="group">Country group:</label><br>
                            <select name="group">

                                    <option value="Russia, USA, Canada">Russia, USA, Canada</option>
                                    <option value="South America, the Pacific, Africa, Asia">South America, the Pacific, Africa, Asia</option>
                                    <option value="CIS, Europe, Asia, South Africa">CIS, Europe, Asia, South Africa</option>
                                    <option value="Satellite Connection">Satellite Connection</option>
                                    <option value="რუსეთი, აშშ, კანადა">რუსეთი, აშშ, კანადა</option>
                                    <option value="სამხრეთ ამერიკა, წყნარი ოკეანის ქვეყნები, აფრიკა">სამხრეთ ამერიკა, წყნარი ოკეანის ქვეყნები, აფრიკა,</option>
                                    <option value="დსთ, ევროპა, აზია, სამხრეთ აფრიკა">დსთ, ევროპა, აზია, სამხრეთ აფრიკა</option>
                                    <option value="სატელიტური კავშირი">სატელიტური კავშირი</option>

                            </select>
                            <br><br>
                            <label for="language">language:</label><br>
                            <select name="language" required>

                                <option value="en" >English</option>
                                <option value="ge" >Georgian</option>
                            </select>
                            <br><br>
                            <label for="discount">Discount:</label><br>
                            <select name="discount">
                                <option value="0">No</option>
                                <option value="1">yes</option>
                                <option value="2">Both</option>
                            </select>
                            <br><br>
                            <label for="code">Country code:</label><br>
                            <input type="text" id="code" name="code" required>
                            <br><br>
                            <input type="submit" value="Save" class="btn btn-success"/>
                            <a href="/developer_version/public/cms/internationalline/en"><button class="btn btn-warning">Cancel</button></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
