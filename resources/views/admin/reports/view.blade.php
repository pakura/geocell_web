@extends('admin')

@section('content')
    <div class="container_">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Reports</div>
                    <div class="panel-body">
                        <?php
                        $cnt = 0;
                        $suc = 0;
                        $rej = 0;
                        $uns = 0;
                        foreach ($activation_status as $val){

                            if ($val->result_code == 1){
                                $suc += $val->cnt;
                            } elseif ($val->result_code == -1001) {
                                $uns += $val->cnt;
                            } else {
                                $rej += $val->cnt;
                            }
                        }
                            $orderId = 11;
                        $serviceName = array(
                                'S_METI_S' => (object) array('name' => 'Bucket METI S', 'order' => $orderId++),
                                'S_METI_M' => (object) array('name' => 'Bucket METI M', 'order' => $orderId++),
                                'S_METI_L' => (object) array('name' => 'Bucket METI Unlimited', 'order' => $orderId++),
                                'S_METI_U' => (object) array('name' => 'Bucket METI Unlimited+', 'order' => $orderId++),
                                'S_METI_S_PERIODIC' => (object) array('name' => 'Bucket METI S (RC)', 'order' => $orderId++),
                                'S_METI_M_PERIODIC' => (object) array('name' => 'Bucket METI M (RC)', 'order' => $orderId++),
                                'S_METI_L_PERIODIC' => (object) array('name' => 'Bucket METI Unlimited (RC)', 'order' => $orderId++),
                                'S_METI_U_PERIODIC' => (object) array('name' => 'Bucket METI Unlimited+ (RC)', 'order' => $orderId++),
                                'S_INTERNET_35' => (object) array('name' => 'Internet Pack 500 MB', 'order' => $orderId++),
                                'S_INTERNET_6' => (object) array('name' => 'Internet Pack 1.2 GB', 'order' => $orderId++),
                                'S_INTERNET_19' => (object) array('name' => 'Internet Pack 2 GB', 'order' => $orderId++),
                                'S_INTERNET_26' => (object) array('name' => 'Internet Pack 4 GB', 'order' => $orderId++),
                                'S_INTERNET_40' => (object) array('name' => 'Internet Pack 6 GB', 'order' => $orderId++),
                                'S_INTERNET_15' => (object) array('name' => 'Internet pack 15 GB', 'order' => $orderId++),
                                'S_INTERNET_35_PERIODIC' => (object) array('name' => 'Internet Pack 500 MB (RC)', 'order' => $orderId++),
                                'S_INTERNET_6_PERIODIC' => (object) array('name' => 'Internet Pack 1.2 GB (RC)', 'order' => $orderId++),
                                'S_INTERNET_19_PERIODIC' => (object) array('name' => 'Internet Pack 2 GB (RC)', 'order' => $orderId++),
                                'S_INTERNET_26_PERIODIC' => (object) array('name' => 'Internet Pack 4 GB (RC)', 'order' => $orderId++),
                                'S_INTERNET_40_PERIODIC' => (object) array('name' => 'Internet Pack 6 GB (RC)', 'order' => $orderId++),
                                'S_INTERNET_15_PERIODIC' => (object) array('name' => 'Internet pack 15 GB (RC)', 'order' => $orderId++),
                                'S_INTERNET_44' => (object) array('name' => '1 MB Mobile internet from  0.5  Tetri', 'order' => $orderId++),
                                'S_INTERNET_45' => (object) array('name' => '100 MB Daily - 1.5 GEL', 'order' => $orderId++),
                                'S_INTERNET_15_TWICE' => (object) array('name' => '30 GB - 60 GEL', 'order' => $orderId++),
                                'S_SMARTPHONE_PACKAGE' => (object) array('name' => 'Smartphone Pack', 'order' => $orderId++),
                                'S_SOCIAL_NETWORK_PACKAGE' => (object) array('name' => 'Social networks pack', 'order' => $orderId++),
                                'S_UNLIMITED_OPERA_MINI' => (object) array('name' => 'Unlimited Opera mini', 'order' => $orderId++),
                                'S_UNLIMITED_ODNOKLASNIKI' => (object) array('name' => 'Unlimited Odnoklassniki', 'order' => $orderId++),
                                'S_UNLIMITED_FACEBOOK' => (object) array('name' => 'Unlimited facebook', 'order' => $orderId++),
                                'S_SMS_1' => (object) array('name' => '150 SMS', 'order' => $orderId++),
                                'S_SMS_2' => (object) array('name' => '500 SMS', 'order' => $orderId++),
                                'S_UNLIMITED_SMS' => (object) array('name' => 'Unlimited SMS', 'order' => $orderId++),
                                'S_SMS_DAYNIGHT' => (object) array('name' => 'SMSIZATSIA', 'order' => $orderId++),
                                'S_ROAMING_10MB' => (object) array('name' => 'Roaming 10 MB', 'order' => $orderId++),
                                'S_ROAMING_50MB' => (object) array('name' => 'Roaming 50 MB', 'order' => $orderId++),
                                'S_ROAMING_150MB' => (object) array('name' => 'Roaming 150MB', 'order' => $orderId++),
                                'S_TARIFF_120' => (object) array('name' => 'Tariff 12 tetri', 'order' => $orderId++),
                                'S_TRIPLE_ZERO' => (object) array('name' => 'Tariff  Nulomani', 'order' => $orderId++),
                                'S_TARIFF_GLOBUS' => (object) array('name' => 'Tariff Globusi', 'order' => $orderId++),
                                'S_SERVICE_110' => (object) array('name' => 'Tariff 1-10', 'order' => $orderId++),
                                'S_TARIFF_120' => (object) array('name' => 'Tariff 1-12', 'order' => $orderId++),
                                'S_INTERNATIONAL_DISCOUNT' => (object) array('name' => 'International discount', 'order' => $orderId++),
                                'S_ROAMING_SERVICE' => (object) array('name' => 'Roaming', 'order' => $orderId++),
                                'S_MMS' => (object) array('name' => 'MMS', 'order' => $orderId++),
                                'S_MISSED_CALL' => (object) array('name' => 'Missed call', 'order' => $orderId++),
                                'S_BEEP' => (object) array('name' => 'Call request service', 'order' => $orderId++),
                                'S_CLIR' => (object) array('name' => 'Calling Number Permanent Restriction', 'order' => $orderId++),
                                'S_BALANCE_ON_OFF' => (object) array('name' => 'Geomritskhveli', 'order' => $orderId++),
                                'S_CLIP' => (object) array('name' => 'Calling Number Restriction', 'order' => $orderId++),
                                'SO_CLIP' => (object) array('name' => 'Calling Number Identification', 'order' => $orderId++),
                                'S_BEEP' => (object) array('name' => 'Call request service', 'order' => $orderId++)
                        );
                        ?>
                        <h1>Reports</h1>
                        <br>
                        <br>
                        <div style="background-color: #CCC; border-radius: 6px; width: 100%; padding: 12px; height: 90px">
                            <div class="form-group ov_hidden col-md-3">
                                <label for="creation_date">Filter From: </label>

                                <div class="date-and-time">
                                    <input class="form-control datepicker" data-format="yyyy-mm-dd" name="creation_date" type="text" id="from" value="<?= isset($from)?$from:date('Y-m').'-01' ?>">
                                </div>
                            </div>
                            <div class="form-group ov_hidden col-md-3">
                                <label for="creation_date">To: </label>

                                <div class="date-and-time">
                                    <input class="form-control datepicker" data-format="yyyy-mm-dd" name="creation_date" type="text" id="to" value="<?= isset($to)?$to:date('Y-m-d') ?>">
                                </div>
                            </div>

                            <button style="float: right; margin-top: 18px; width: 200px" class="btn btn-success" onclick="filter()">Filter</button>
                        </div>
                        <br>
                        <hr>
                        <br>

                        <div id="tabledata">
                            <table style="width: 100%; color: #666" id="tab_0">
                                <thead>
                                <tr>
                                    <th>N</th>
                                    <th>Name</th>
                                    <th>Quantity</th>
                                </tr>
                                </thead>
                                <tbody>


                                <tr>
                                    <td>1</td>
                                    <td style="background-color: #7722ff; color: #FFF;">Private Cabinet Stats</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Unique Users</td>
                                    <td><?= sizeof($logins) ?></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Number of entries</td>
                                    <td><?php $sum = 0; foreach ($logins as $val){$sum += $val->cnt;} echo $sum; ?></td>
                                </tr>



                                <tr>
                                    <td>4</td>
                                    <td style="background-color: #7722ff; color: #FFF;">Password Status</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>SMS</td>
                                    <td><?= @$pasType['sms']; ?></td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>Password</td>
                                    <td><?= @$pasType['pass'] ?></td>
                                </tr>




                                <tr>
                                    <td>7</td>
                                    <td style="background-color: #7722ff; color: #FFF;">Activation Status</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>8</td>
                                    <td>Successful</td>
                                    <td>{{$suc}}</td>
                                </tr>
                                <tr>
                                    <td>9</td>
                                    <td>Rejected</td>
                                    <td>{{$rej}}</td>
                                </tr>
                                <tr>
                                    <td>10</td>
                                    <td>Not successful</td>
                                    <td>{{$uns}}</td>
                                </tr>

                                <tr>
                                    <td>11</td>
                                    <td style="background-color: #7722ff; color: #FFF;">Successful activations</td>
                                    <td></td>
                                </tr>

                                <?php

                                foreach ($services as $key => $val){
                                    echo '<tr>
                                            <td>'.(isset($serviceName[$key]->order)?$serviceName[$key]->order:$cnt++).'</td>
                                            <td>'.(isset($serviceName[$key]->name)?$serviceName[$key]->name:$key).'</td>
                                            <td>'.$val.'</td>
                                            </tr>';

                                }




                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        $(document).ready(function() {
            $('#tab_0').DataTable({
                "iDisplayLength": 25,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        } );

        function filter(){

            var from = $('#from').val();
            var to = $('#to').val();
            var url = '/developer_version/public/cms/reports/'+from+'/'+to;
            window.location.href = url;

        }

        function writeData(arg){
            $('#tabledata').html('<table style="width: 100%; color: #666" id="tab_0">\
            <thead>\
            <tr>\
            <th>N</th>\
            <th>Name</th>\
            <th>Quantity</th>\
            </tr>\
            </thead>\
            <tbody>');
            var cnt = 0;
            for (var ii in arg){
                cnt++;
                $('#tabledata').append('<tr>\
                        <td>'+(cnt++)+'</td>\
                        <td style="background-color: #7722ff; color: #FFF;">'+sku2text(ii)+'</td>\
                <td></td>\
                </tr>');
                for (var jj in arg[ii]){

                }
            }

            $('#tabledata').append('</tbody>\
                    </table>');
        }

        function sku2text(arg){
            switch (arg){
                case 'logins':
                    return 'Private Cabinet Stats';
                    break;
                case 'password_type':
                    return 'Password Status';
                    break;
                case 'activation_status':
                    return 'Activation Status';
                    break;
                case 'services':
                    return 'Successful activations';
                    break;
                default:
                    return arg;
                    break;
            }
        }

    </script>
@endsection
