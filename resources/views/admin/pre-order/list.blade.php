@extends('admin')

@section('content')
    <div class="container_">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>{{Lang::get('admin_menu.pre-order')}}</h1>

                        <br>
                        <hr>
                        <br>
                        <div id="tabledata">





                            <table style="width: 100%; color: #666" id="tab_0">
                                <thead>
                                <tr>
                                    <th>Date/time</th>
                                    <th>Name</th>
                                    <th>Last name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Personal ID</th>
                                    <th>Device</th>
                                    <th>Storage</th>
                                    <th>Color</th>
                                    <th>Order Type</th>
                                    <th>Month</th>
                                    <th>Bonus</th>
                                    <th>City</th>
                                    <th>Office</th>
                                    <th>Delivery</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Date/time</th>
                                    <th>Name</th>
                                    <th>Last name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Personal ID</th>
                                    <th>Device</th>
                                    <th>Storage</th>
                                    <th>Color</th>
                                    <th>Order Type</th>
                                    <th>Month</th>
                                    <th>Bonus</th>
                                    <th>City</th>
                                    <th>Office</th>
                                    <th>Delivery</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($orders as $key => $val)
                                        <tr>
                                            <td>{{$val->autodate}}</td>
                                            <td>{{$val->name}}</td>
                                            <td>{{$val->lastname}}</td>
                                            <td>{{$val->phone}}</td>
                                            <td>{{$val->email}}</td>
                                            <td>{{$val->pid}}</td>
                                            <td>{{$val->device}}</td>
                                            <td>{{$val->storage}}</td>
                                            <td>{{$val->color}}</td>
                                            <td><?php if($val->ordertype == 1) echo 'Buy it now'; else echo 'Installment' ?></td>
                                            <td>{{$val->month}}</td>
                                            <td>{{$val->bonus}}</td>
                                            <td>{{$val->name_en}}</td>
                                            <td>{{$val->address_en}}</td>
                                            <td><?php if($val->delivered == 0) echo '<input type="checkbox" onchange="setDelivery(this)" id="'.$val->id.'">'; else echo '<input type="checkbox" onchange="setDelivery(this)" checked  id="'.$val->id.'">' ?></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        function setDelivery(elem) {


            var id = $(elem).attr('id');
            if(elem.checked) {
                var action = 1;
                var r = confirm("ადასტურებთ, რომ მომხმარებელმა მიიღო შეკვეთა?");
            } else {
                var action = 0;
                var r = confirm("ადასტურებთ, რომ მომხმარებელს არ მიუღია შეკვეთა?");
            }
            if (r == false) {
                return false;
            }

            $.ajax({
                method: "get",
                url: "developer_version/public/cms/preoder/action/",
                data: {id: id, action: action}
            }).done(function( msg ) {
                alert(msg);
            });
        }


        $(document).ready(function() {
            var table = $('#tab_0').DataTable({
                "iDisplayLength": 25,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel',
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                    },
                    'print'
                ]
            });
            $('#tab_0_filter').fadeOut();
            var cnt = 0;
            $('#tab_0 tfoot th').each( function () {
                if(cnt == 1 || cnt == 2 || cnt == 9 ||  cnt == 6 || cnt == 10){
                    var title = $(this).text();
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
                }
                cnt++;
            } );
            var searchcnt = 0;
            table.columns().every( function () {
                if(searchcnt == 1 || searchcnt == 2 || searchcnt == 9 || searchcnt == 6 ||  searchcnt == 10 ) {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                }
                searchcnt++;
            } );

            var selectcnt = 0;
            table.columns().every( function () {
                if(selectcnt == 3 || selectcnt == 4 || selectcnt == 5 || selectcnt == 7 || selectcnt == 8){
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                );

                                column
                                        .search( val ? '^'+val+'$' : '', true, false )
                                        .draw();
                            } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' );
                    } );
                }
                selectcnt++;
            } );



//            $('input.column_filter').on( 'keyup click', function () {
//                filterColumn( $(this).parents('tr').attr('data-column') );
//            } );
        } );





    </script>
    <style>
        input{
            width: 60px;
        }
    </style>
@endsection
