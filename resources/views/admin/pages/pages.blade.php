@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<!--heading gasatania calke failshi-->
			
			 <?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.sitemap'),'url'=>''], 
		    					1=> ['title'=>$group->generic_title,'url'=>'pages/get/'.$group->id],

			    			];
               
			  ?>

			@include('admin.header',['title'=>$titles,'url'=>'pages/create/'.$group->id.'/0']) 

			<div class="panel panel-default">
			<div class="table responsive">
					 <div class="table-head">
						 
				    	<div class="col-md-6">{{Lang::get('admin.title')}}</div>
				    	
				    	<div class="head-right">
				    		<div class="col-md-3">{{Lang::get('admin.attached')}}</div>
				    		<div class="col-md-2">{{Lang::get('admin.page_type')}}</div>
				    		<div class="col-md-1">Id</div>
				    		<div class="col-md-6">{{Lang::get('admin.actions')}}</div>
				    	</div>
				    	
					    
					</div>  
					 
					 
					<div id="pagetree">
						<button class="btn btn-success disabled" data-action="updateMenuTree" data-url="{{config('app.cms_slug')}}/pages/update-tree"><i class="fa-check"></i> {{Lang::get('admin.apply')}}</button> 
						<!--include pages list-->
                        @include('admin.pages.pages-list',['pages'=>$pages]) 
						  
					</div>	
					 
				</div>

				
				
			</div>

		</div>
	</div>
</div>

@endsection
