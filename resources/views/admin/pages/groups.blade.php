@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			
			 <?php $titles =  [
		    					0=> ['title'=>Lang::get('admin_menu.sitemap'),'url'=>''], 
		    					1=> ['title'=>Lang::get('admin_menu.menu'),'url'=>''],

			    			];

		    ?>				

			@include('admin.header',['title'=>$titles,'url'=>'groups/add/']) 


			<div class="panel panel-default">
			<table class="table responsive">
				    <tr>
				    	<th class="col-md-8">{{Lang::get('admin.title')}}</th>
				    	
				    	<th class="col-md-2">Id</th> 
				    	<th class="col-md-2">{{Lang::get('admin.actions')}}</th>
				    </tr>
					<tbody>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					@foreach (@$groups as $group)
					    <tr>	
					    	<td class="changeTitle">
					    		<a href="{{config('app.cms_slug')}}/pages/get/{{$group->id}}" data-url="{{config('app.cms_slug')}}/groups/edit/{{$group->id}}"  data-type="text">{{ $group->generic_title }}</a> 
					    		<div class="hide form-block">	
					    			<input type="text" name="" class="input form-control" value="{{ $group->generic_title }}"/>
					    		</div>	
					    	</td>
					    	
					    	<td>{{$group->id}}</td>

					    	<td width=" ">
					    		<a href="{{config('app.cms_slug')}}/pages/create/{{$group->id}}/0" class="btn btn-success" title="{{Lang::get('admin.add')}}"><i class="fa-plus"></i></a>
					    		<a href="{{config('app.cms_slug')}}/groups/edit/{{$group->id}}" class="btn btn-warning editcollection" title="{{Lang::get('admin.edit')}}"> <i class="fa-pencil"></i></a>
					    		<button class="btn btn-success save" style="display:none;" title="{{Lang::get('admin.save')}}">{{Lang::get('admin.save')}}<i class="fa-ok"></i></button>
					    		<button class="btn btn-danger cancel"  style="display:none;" title="{{Lang::get('admin.cancel')}}">{{Lang::get('admin.cancel')}}</button>
					    	
					    		<a href="{{config('app.cms_slug')}}/groups/delete/{{$group->id}}" 
					    			class="btn btn-danger deleteColl"  title="{{Lang::get('admin.delete')}} ">
					    			
					    			<i class="fa-remove"></i>
					    		</a>

					    	</td>
					    </tr>	
					@endforeach
					 
					</tbody>
				</table>

				
				
			</div>

		</div>
	</div>
</div>
@endsection
