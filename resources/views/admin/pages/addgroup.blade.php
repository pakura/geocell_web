@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		    
		    <?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.sitemap'),'url'=>''], 
		    					1=> ['title'=>Lang::get('admin_menu.menu'),'url'=>'groups'],
		    					2=> ['title'=>Lang::get('admin.add_group'),'url'=>''],

			    			];

		    ?>

			@include('admin.header',['title'=>$titles,'url'=>'']) 				
				
			<div class="panel panel-default">	
			    <h3>{{Lang::get('admin.add_group')}}</h3>	

				{!!  Form::open(['url'=>config('app.cms_slug').'/groups/save/']) !!}

					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					
					<div class="form-group">
						<label class="control-label" for="generic_title">{{Lang::get('admin.generic')}}</label>
						<input type="text" class="form-control input-dark" name="generic_title" value="{{ old('generic_title') }}"  />
					</div>					
					
					<div class="form-group">
						<button type="submit" class="btn btn-success">
							
							{{Lang::get('admin.save')}}
						</button>
					</div>
					
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
