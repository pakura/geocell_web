@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.sitemap'),'url'=>''], 
		    					1=> ['title'=>$group->generic_title,'url'=>'pages/get/'.$group->id],
		    					2=> ['title'=>Lang::get('admin.add'),'url'=>'pages/get'],

			    			];
		     ?>

			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::open(['url'=>config('app.cms_slug').'/pages/save/']) !!}

					@include('admin.pages.form')
					
				{!! Form::close() !!}
			
		</div>
	</div>
</div>
@endsection
