 <div class="row">
	<div class="col-md-12">

  		<?php 
  			$langs = config('app.langs'); 
  			$disabled = (@$page)?'':'disabled';
  			$toggle = (@$page)?'tab':'';
  		?>

		 	
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>
			
			@foreach ($langs as $key=>$val)
				<li class="{{$disabled}}">
					<a href="#profile-{{$key}}" data-toggle="{{$toggle}}">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">{{$val}}</span>
					</a>
				</li>
			@endforeach	 
			<li class="{{$disabled}} showGallery"  >
				<a href="#files" data-toggle="{{$toggle}}">
					<span class="visible-xs"><i class="fa-cog"></i></span>
					<span class="hidden-xs">Files</span>
				</a>
			</li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="general">
				
				{!! Form::hidden('group_id', @$group->id?@$group->id : @$page->group_id ) !!}	
				{!! Form::hidden('parent_id', @$parent_id?@$parent_id : @$page->parent_id ) !!}	

				<div class="form-group">
					{!! Form::label('generic_title',Lang::get('admin.generic')) !!}
					{!! Form::text('generic_title', null, ['class'=>'form-control']) !!}					 
				</div>	
				
				<div class="form-group">
					{!! Form::label('slug',Lang::get('admin.slug')) !!}
					{!! Form::text('slug', null, ['class'=>'form-control']) !!}					 
				</div>				
				
				<div class="form-group">
					{!! Form::label('page_type',Lang::get('admin.page_type')) !!}
					{!! Form::select('page_type', config('app.page_types'), null, ['class'=>'form-control']) !!}					 
				</div>
                
                <div class="form-group coll_list" @if(@$collections)style="display:block;"  @endif>
					{!! Form::label('attached_collection_id',Lang::get('admin.attach')) !!}
					<select name = "attached_collection_id" class="form-control" id="attached_collection_id">
						@if(@$collections)
							
							@foreach($collections as $id=>$title)
							 	<option value="{{$id}}" @if($id==$page->attached_collection_id) selected @endif>{{$title}}</option>	
							@endforeach	
						@endif
					</select> 		 
				</div>

				<div class="form-group">
					{!! Form::label('controller',Lang::get('admin.controller')) !!}
					{!! Form::select('controller',  array('' =>Lang::get('admin.choosecontroller')) + $controllers , null, ['class'=>'form-control']) !!}					 
				</div>
				

				<div class="row"> 
	                <div class="form-group ov_hidden col-md-3">
						{!! Form::label('creation_date',Lang::get('admin.create')) !!}        
						
						<div class="date-and-time">
							{!! Form::text('creation_date', null, ['class'=>'form-control datepicker', 'data-format'=>'dd-mm-yyyy']) !!}	
							{!! Form::text('creation_date_time', '10:00 AM', ['class'=>'form-control timepicker', 'data-template'=>'dropdown']) !!}	
				 
	                    </div>
					</div>

					<div class="form-group ov_hidden col-md-3">
						{!! Form::label('expire_date',Lang::get('admin.expire')) !!}

						<div class="date-and-time">
							{!! Form::text('expire_date', null , ['class'=>'form-control datepicker', 'data-format'=>'dd-mm-yyyy']) !!}	
							{!! Form::text('expire_date_time', '08:00 PM', ['class'=>'form-control timepicker', 'data-template'=>'dropdown']) !!}	
				 
	                    </div>					 
					</div>
                </div>
                
                <div class="row"> 
	                <div class="form-group col-md-3">
						{!! Form::label('redirect_link',Lang::get('admin.redirect')) !!}
						{!! Form::text('redirect_link', null, ['class'=>'form-control','placeholder'=>'http://']) !!}					 
					</div>
				</div>
					
				<div class="form-group">
					{!! Form::label('visibility',Lang::get('admin.visibility')) !!}
					{!! Form::checkbox('visibility',null, 1, ['class'=>'cbr']) !!}					 
				</div>

			</div>
			
		    @foreach ($langs as $key=>$val)
			   
				<div class="tab-pane" id="profile-{{$key}}">
				
					<div class="form-group">
						{!! Form::label("{$key}[title]",Lang::get("admin.title")) !!}
						{!! Form::text("{$key}[title]",  @$page["data"][$key]["title"], ["class"=>"form-control"]) !!}					 
					</div>	

					<div class="form-group">
						{!! Form::label("{$key}[short_title]",Lang::get("admin.short_title")) !!}
						{!! Form::text("{$key}[short_title]", @$page["data"][$key]["short_title"], ["class"=>"form-control"]) !!}					 
					</div>

					<div class="form-group">
						{!! Form::label("{$key}[meta_content]",Lang::get("admin.meta_content")) !!}
						{!! Form::text("{$key}[meta_content]", @$page["data"][$key]["meta_content"], ["class"=>"form-control"]) !!}					 
					</div>
					<div class="form-group">
						{!! Form::label("{$key}[meta_description]",Lang::get("admin.meta_description")) !!}
						{!! Form::text("{$key}[meta_description]",  @$page["data"][$key]["meta_description"], ["class"=>"form-control"]) !!}					 
					</div>

					<div class="form-group collapsed-desc">
						{!! Form::label("{$key}[description]",Lang::get("admin.description")) !!} 
						
						<i class="fa-plus expand-desc-icon"></i>
						<i class="fa-minus collapse-desc-icon"></i>

					    <div class="hide_description">
					    	{!! Form::textarea("{$key}[description]",  @$page["data"][$key]["description"], ["class"=>"textarea hidden"]) !!}	
					    </div>					 
					</div>
					
					<div class="form-group">
						{!! Form::label("{$key}[content]",Lang::get("admin.content")) !!}
						{!! Form::textarea("{$key}[content]",   @$page["data"][$key]["content"], ["class"=>"textarea"]) !!}					 
					</div>
						
				</div>
			@endforeach	
			
			<div class="tab-pane" id="files">
					
				 <div action="{{config('app.cms_slug')}}/pages/uploadfiles/{{@$page->id}}" class="dropzone dz-clickable">
				 	
				 
				 	<div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                  
				 </div>

			</div>
		</div>
		
		
	</div>				
</div>
<div class="form-group">

	<button type="submit" class="btn btn-success">
		
		{{Lang::get('admin.save')}}
	</button>
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/pages/get/{{@$group->id?@$group->id:@$page->group_id}}" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>
					
			