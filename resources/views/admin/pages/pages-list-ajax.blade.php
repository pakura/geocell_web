<?php 
	function build_menu($pages,$parent=0)  { 
?>   
	
	
	    <ol class="dd-list">
		  @foreach ($pages as $page)
		  
		    @if ($page->parent_id == $parent) 

		       	<li class="dd-item" data-id="{{$page->id}}">

		       	 	<div class="dd-handle">
		       	 	 		
		       	 	 	<input class="cbr choosepage" name="page[]" type="checkbox" value="{{$page->id}}" >
		       	 	 	<span>{{$page->generic_title}} </span>
		       	 	 	
		       	 	</div>
		       	 
		        
			        @if (Helper::has_children($pages,$page->id) )
			          
			          {{ build_menu($pages,$page->id) }}

			        @endif 

		       </li>
		      
		    @endif

		  @endforeach
	    </ol>
    
<?php } ?> 






				
	<div class="tabs-vertical-env">
    	<ul  class="nav tabs-vertical">
    	    <?php $active=true; ?>
    		@for ($i=0; $i<count($groups);$i++)
    			 <li  class="{{$active?'active':''}}"><a href="#group-{{$groups[$i]->id}}" data-toggle="tab">{{$groups[$i]->generic_title}} </a></li>
    			 {!! $active=false; !!}
    		@endfor
    	</ul>

    	<div class="tab-content">
    	    <?php $active=true; ?>
    	    @for ($i=0; $i<count($groups);$i++)

			<div class="tab-pane {{$active?'active':''}}" id="group-{{$groups[$i]->id}}">
				{!! $active=false; !!}
				<div class="dd" id="nestable">
				    <?php if(!isset($pages[$groups[$i]->id])) { continue; } ?>
	                

						{{ build_menu($pages[$groups[$i]->id]) }}

					 
				</div>

			</div>
			@endfor
		</div>
    </div>	

 
						 

							
