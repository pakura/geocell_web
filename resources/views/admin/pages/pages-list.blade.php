<?php 
	function build_menu($pages,$parent=0)  { 
?>   
	
	
	    <ol class="dd-list">
		  @foreach ($pages as $page)
		  
		    @if ($page->parent_id == $parent) 

		       	<li class="dd-item" data-id="{{$page->id}}">

		       	 	<div class="dd-handle">
		       	 	 	
		       	 	 	
		       	 	</div>
		       	 	<div class="dd-slug">
		       	 		<a href="{{config('app.cms_slug')}}/pages/edit/{{$page->id}}">	
		       	 	 		 {{$page->generic_title}} 
		       	 	 	</a>
		       	 	 	<br>
		       	 	    <a class="slug" href="{{$page->full_slug}}" target="_blank">	
		       	 	     {{$page->full_slug}}
                        </a>
		       	 	</div>
	       	 	    
	       	 	    <div class="page-actions">
	       	 	 		    <div class="bootstrap-tagsinput col-md-3">
							   @if ($page->ctitle)
					    			<span class="tag  label-info btn"> 
						    			<a href="{{config('app.cms_slug')}}/collections/get/{{$page->coll_type}}" class="tagcontent">{{$page->ctitle}}</a>
						    			<span data-role="remove" data-url="{{config('app.cms_slug')}}/pages/removeCollection/{{$page->id}}"></span> 
					    			</span>
				    			@endif
					    	</div>

	       	 	 			<div class="col-md-2 ids">
					    		    @if($page->page_type) 
					    		   		{{ Helper::pageType($page->page_type) }}
					    		   	@endif  
					    	</div>	
					    	<div class="col-md-1  ids">
					    		{{$page->id}}
					    	</div>	
					    	<div  class="col-md-6">
					    	    <a href="#" data-url="{{config('app.cms_slug')}}/pages/edit-visibility/{{$page->id}}" class="btn btn-turquoise visibility {{$page->visibility?'':' eye-off'}}" title="{{Lang::get('admin.visibility')}}" >
					    	    	<i class="fa-eye"></i>
					    	    </a>

					    	    <a href="{{$page->full_slug}}"   class="btn btn-default" target="_blank" title="{{Lang::get('admin.preview')}}" >
					    	    	<i class="fa-search-plus"></i>
					    	    </a>

					    	    <a href="{{config('app.cms_slug')}}/pages/edit/{{$page->id}}/#files"  class="btn btn-default" title="{{Lang::get('admin.files')}}" >
					    	    	<i class="fa-paperclip"></i>
					    	    </a>
					    	    <a href="{{config('app.cms_slug')}}/pages/create/{{$page->group_id}}/{{$page->id}}" class="btn btn-success" title="{{Lang::get('admin.addsub')}}" >
					    	    	<i class="fa-plus"></i>
					    	    </a>
					    	    <a href="{{config('app.cms_slug')}}/pages/edit/{{$page->id}}" class="btn btn-warning" title="{{Lang::get('admin.edit')}}">
					    	    	<i class="fa-pencil"></i>
					    	    </a>

					    		<a href="{{config('app.cms_slug')}}/pages/delete/{{$page->id}}" class="btn btn-danger" onclick="return confirm('Are you sure?')" title="{{Lang::get('admin.delete')}}"> 
					    			<i class="fa-remove"></i>
					    		</a> 

					    	</div>
	       	 	 		 
	       	 	 	</div>   
		        
			        @if (Helper::has_children($pages,$page->id) )
			          
			          {{ build_menu($pages,$page->id) }}

			        @endif 

		       </li>
		      
		    @endif

		  @endforeach
	    </ol>
    
<?php } ?> 

<div class="dd" id="nestable">
	
	{{ build_menu($pages) }}
	
	 
	<button class="btn btn-success disabled" data-action="updateMenuTree" data-url="{{config('app.cms_slug')}}/pages/update-tree"><i class="fa-check"></i> {{Lang::get('admin.apply')}}</button>

	
	<input type="hidden" id="nestable-output">
	
</div>

						 

							
