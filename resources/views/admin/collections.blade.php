@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			
			 <?php 
			 $types = config('app.collection_types');

			 $titles =  [
		    					0=> ['title'=>Lang::get('admin.coll_'.$type),'url'=>''], 
		    					1=> ['title'=>Lang::get('admin.coll_sub_'.$type),'url'=>'collections/get/'.$type]

		    				]; 
		    	   		
		    ?>				

			@include('admin.header',['title'=>$titles,'url'=>'collections/add/'.$type]) 


			<div class="panel panel-default">
			<table class="table responsive">
				    <tr>
				    	<th class="col-md-4">{{Lang::get('admin.title')}}</th>
				    	
				    	<th class="col-md-4">{{Lang::get('admin.apages')}}</th>
				    	<th class="col-md-1">Id</th>
				    	<th class="col-md-3">{{Lang::get('admin.actions')}}</th>
				    </tr>
					<tbody>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					@foreach ($collections as $collection)
					    @if(Auth::user()->hasPermission($collection->id) || Auth::user()->hasPermissionToCollection($collection->collection_type) ) 
					    <tr>	
					    	<td class="changeTitle">
					    		<a href="{{config('app.cms_slug')}}/{{$types[$type]}}/get/{{$collection->id}}" data-url="{{config('app.cms_slug')}}/collections/edit/{{$type}}/{{$collection->id}}" data-type="text">{{ $collection->generic_title }}</a> 
					    		<div class="hide form-block">	
					    			<input type="text" name="" class="input form-control" value="{{ $collection->generic_title }}" />
					    		</div>	
					    	</td>
					    	
					    	<td class="bootstrap-tagsinput attached_pages_{{$collection->id}}">
					    	 <!--  <input type="text" class="form-control" id="tagsinput-1" data-role="tagsinput" value="Sample tag, Another great tag, Awesome!"/>   -->	
					    		@foreach ($collection->pages as $page)
					    			<span class="tag  label-info btn" id="page-{{$page->id}}"> 
						    			<a href="{{config('app.cms_slug')}}/pages/edit/{{$page->id}}">{{$page->generic_title}}</a>
						    			<span data-role="remove" data-url="{{config('app.cms_slug')}}/pages/removeCollection/{{$page->id}}"></span> 
					    			</span>
					    		@endforeach
					    	</td>
					        
					        <td>{{$collection->id}}</td>

					    	<td width=" ">
					    		<a href="{{config('app.cms_slug')}}/{{$types[$type]}}/create/{{$collection->id}}" class="btn btn-success" title="{{Lang::get('admin.add')}}"> 
					    			<i class="fa-plus"></i>
					    		</a>
					    		
					    		<a href="#" class="btn btn-info attach_collection" title="{{Lang::get('admin.atpages')}}" data-url="{{config('app.cms_slug')}}/pages/getall" data-collid="{{$collection->id}}" data-colltype="{{$collection->collection_type}}"> 
					    			<i class="fa-chain"></i>
					    		</a>

					    		<a href="{{config('app.cms_slug')}}/collections/edit/{{$type}}/{{$collection->id}}" class="btn btn-warning editcollection" title="{{Lang::get('admin.edit')}}"> <i class="fa-pencil"></i></a>

					    		<button class="btn btn-success save" data-updatelist="yes" style="display:none;" title="{{Lang::get('admin.save')}}">{{Lang::get('admin.save')}}<i class="fa-ok"></i></button>
					    		<button class="btn btn-danger cancel"  style="display:none;" title="{{Lang::get('admin.cancel')}}">{{Lang::get('admin.cancel')}}</button>
					    	
					    		<a href="{{config('app.cms_slug')}}/collections/delete/{{$type}}/{{$collection->id}}" 
					    			class="btn btn-danger @if($collection->pages) disable @endif deleteColl"  title="{{Lang::get('admin.delete')}} ">
					    			
					    			<i class="fa-remove"></i>
					    		</a>

					    	</td>
					    </tr>
					    @endif	  
					@endforeach
					 
					</tbody>
				</table>

				
				
			</div>

		</div>
	</div>
</div>
@endsection
