@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<!--heading gasatania calke failshi-->
			
			 <?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.user'),'url'=>''], 
		    					1=> ['title'=>Lang::get('admin_menu.cmsuser'),'url'=>'admin-users'],

			    			];

			  ?>

			@include('admin.header',['title'=>$titles,'url'=>'admin-users/create/']) 

			@include('errors.admin')

			{!!  Form::model($user,['url'=>config('app.cms_slug').'/admin-users/update/'.$user->id]) !!}

				@include('admin.admin_users.form')
				
			{!! Form::close() !!}

		</div>
	</div>
</div>
@endsection