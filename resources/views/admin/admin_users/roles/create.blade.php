@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		    
		    <?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.user'),'url'=>''], 
		    					1=> ['title'=>Lang::get('admin_menu.cmsuserroles'),'url'=>'admin-user-roles'],
		    					2=> ['title'=>Lang::get('admin.add_role'),'url'=>''],

			    			];

		    ?>

			@include('admin.header',['title'=>$titles,'url'=>'']) 				
			
			@include('errors.admin')
				
			<div class="panel panel-default">	
			    <h3>{{Lang::get('admin.add_role')}}</h3>	

				{!!  Form::open(['url'=>config('app.cms_slug').'/admin-user-roles/save/']) !!}

					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					
					<div class="form-group">
						<label class="control-label" for="role_name">{{Lang::get('admin.role_name')}}</label>
						<input type="text" class="form-control input-dark" name="role_name" value="{{ old('role_name') }}"  />
					</div>					
					
					<div class="form-group">
						<button type="submit" class="btn btn-success">
							
							{{Lang::get('admin.save')}}
						</button>
					</div>
					
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
