@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		    
		    <?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.user'),'url'=>''], 
		    					1=> ['title'=>Lang::get('admin_menu.cmsuserroles'),'url'=>'admin-user-roles'],
		    					2=> ['title'=>Lang::get('admin.add_role'),'url'=>''],

			    			];

		    ?>

			@include('admin.header',['title'=>$titles,'url'=>'']) 				
			
			@include('errors.admin')
				
			<div class=" ">	
			   <!--  <h3>{{Lang::get('admin.add_role')}}</h3>	 -->

				{!!  Form::model($role,['url'=>config('app.cms_slug').'/admin-user-roles/update/'.$role->id]) !!}

					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="role_id"  id="role_id" value="{{$role->id}}">
					
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#general" data-toggle="tab">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs">General</span>
							</a>
						</li>
                         
						<li>
							<a href="#permissions" data-toggle="tab">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs">Permissions</span>
							</a>
						</li>
					
			        </ul>
			        <div class="tab-content">
			
						<div class="tab-pane active" id="general">
							<div class="form-group">
								<label class="control-label" for="role_name">{{Lang::get('admin.role_name')}}</label>
								<input type="text" class="form-control input-dark" name="role_name" value="{{ $role->role_name}}" />
							</div>					
						</div>

						<div class="tab-pane " id="permissions">
							<div class="form-group">
								
								<div class="row list-head">
									<div class="col-md-4">{{Lang::get('admin.choose_permission')}}</div>
									<div class="col-md-2">{{trans('admin.acces_type')}} </div>
									
								</div>

						    	<div class=" ">
 
								 				    	    
									<ul  id="colls">
										<li data-value="-1" >
												<div class="onerowhover row">
												   <div class="col-md-4 cbrcolored"> 
												   		<input class="cbr cbrcolored choosepage" name="collection_id" data-type="2" type="checkbox" value="-1"   @if( isset($permissions[-1]) && $permissions[-1]>1) checked @endif  > {{ trans('admin.allcolls')}} 
													</div> 
												   <div class="col-md-2">
												 		<select name="acces_type" class="form-control">
											    	        @foreach (config('app.access_types') as $key=>$access_type)
											    	        	<option value='{{$key}}'>{{trans($access_type)}}</option>
											    	        @endforeach								    	    	
											    	    </select>
												 	</div>	
												</div> 	
											 	<div class="clear"></div>

												<ul>
												    @foreach(config('app.collection_types') as $type=>$value)
												    
													<li data-value="-{{$type}}">
														<div class="onerowhover row">
														    <div class="col-md-4">
																<input class="cbr choosepage" name="collection_id" data-type="2" type="checkbox" value="-{{$type}}"  @if( isset($permissions[-$type]) && $permissions[-$type]>1) checked @endif >
																All {{$value}} collections
																
														    </div>
														    <div class="col-md-2">
														 		<select name="acces_type" class="form-control">
													    	        @foreach (config('app.access_types') as $key=>$access_type)
													    	        	<option value='{{$key}}'>{{trans($access_type)}}</option>
													    	        @endforeach								    	    	
													    	    </select>
														 	</div>	
														</div> 	
													 	<div class="clear"></div>

														@if(isset($collections[$type])) 
														<ul>
														    
															@foreach ($collections[$type] as $collection)
																<li>
																	<div class="onerowhover row">
														    			<div class="col-md-4">
																			<input class="cbr choosepage" name="collection_id" data-type="{{$type}}" type="checkbox" value="{{$collection->id}}" 
																			 @if( isset($permissions[$collection->id]) && $permissions[$collection->id]>1) checked @endif >
																	 		
																	 		{{$collection->generic_title}} 
																	 	</div>
																	    <div class="col-md-2">
																	 		<select name="acces_type" class="form-control">
																    	        @foreach (config('app.access_types') as $key=>$access_type)
																    	        	<option value='{{$key}}'>{{trans($access_type)}}</option>
																    	        @endforeach								    	    	
																    	    </select>
																	 	</div>	
																	</div> 	
																 	
																 	<div class="clear"></div>
																</li>	
															@endforeach
														</ul>
														@endif
													</li>
													@endforeach 
												</ul>
										</li> 

										<li data-value="-1" >
										    <!--All pages groups are in collections -1 -->
											<div class="onerowhover row">
												<div class="col-md-4 cbrcolored"> 
													<input class="cbr  choosepage" name="collection_id" data-type="1" type="checkbox" value="-1" /> {{trans('admin.allgroups')}}
												</div>	
												<div class="col-md-2">
											 		<select name="acces_type" class="form-control">
										    	        @foreach (config('app.access_types') as $key=>$access_type)
										    	        	<option value='{{$key}}'>{{trans($access_type)}}</option>
										    	        @endforeach								    	    	
										    	    </select>
											 	</div>	
											</div>	
											<div class="clear"></div>
											<ul >
											    @foreach($pagesGroups as $group)
											    
												<li data-value="-{{$group->id}}">
													<div class="onerowhover row">
														<div class="col-md-4 "> 
															<input class="cbr choosepage" name="collection_id" data-type="1" type="checkbox" value="-{{$group->id}}" />
													   		{{$group->generic_title}}

													   	</div>	
														<div class="col-md-2">
													 		<select name="acces_type" class="form-control">
												    	        @foreach (config('app.access_types') as $key=>$access_type)
												    	        	<option value='{{$key}}'>{{trans($access_type)}}</option>
												    	        @endforeach								    	    	
												    	    </select>
													 	</div>	
													</div>	
													<div class="clear"></div>

													   @if(isset($pages[$group->id])) 
														<ul>
														    
															@foreach ($pages[$group->id] as $page)
																<li>
																	<div class="onerowhover row">
																		<div class="col-md-4 "> 	
																			<input class="cbr choosepage" name="collection_id" data-type="1" type="checkbox" value="{{$page->id}}" >
																	 		{{$page->generic_title}} 

																	 	</div>	
																		<div class="col-md-2">
																	 		<select name="acces_type" class="form-control">
																    	        @foreach (config('app.access_types') as $key=>$access_type)
																    	        	<option value='{{$key}}'>{{trans($access_type)}}</option>
																    	        @endforeach								    	    	
																    	    </select>
																	 	</div>	
																	</div>	
																	<div class="clear"></div>
																</li>	
															@endforeach
														</ul>
														@endif

												</li>
												@endforeach 
											</ul>
										</li> 			
									</ul>
									 
								</div>
							   	

							</div>
						</div>

					</div>	


					<div class="form-group">
						<button type="submit" class="btn btn-success">
							
							{{Lang::get('admin.save')}}
						</button>
					</div>
					
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
