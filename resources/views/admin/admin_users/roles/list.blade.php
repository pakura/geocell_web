@foreach (@$userRoles as $role) 
<div class="row" data-action="parent">
	<div class="form-group col-md-2">
		 {{$role->role_name}}
	</div>	
	<div class="form-group col-md-2">
		 
	</div>	
 	

	<div class="form-group col-md-1 col-md-offset-6">
		 <a href="#" class="btn btn-danger remove-role"  data-roleid="{{$role->id}}" data-url="{{config('app.cms_slug')}}/admin-users/remove-role/{{@$userid}}/{{$role->id}}">
			<i class="fa-remove"></i>
		</a> 
		 
	</div>	

</div>		
@endforeach
