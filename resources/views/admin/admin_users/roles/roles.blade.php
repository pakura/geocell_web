@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			
			 <?php $titles =  [
		    					0=> ['title'=>Lang::get('admin_menu.user'),'url'=>''], 
		    					1=> ['title'=>Lang::get('admin_menu.cmsuserroles'),'url'=>'admin-user-roles'],

			    			];

		    ?>				

			@include('admin.header',['title'=>$titles,'url'=>'admin-user-roles/create']) 


			<div class="panel panel-default">
			<table class="table responsive">
				    <tr>
				    	<th class="col-md-8">{{Lang::get('admin.title')}}</th>
				    	
				    	<th class="col-md-2">Id</th> 
				    	<th class="col-md-2">{{Lang::get('admin.actions')}}</th>
				    </tr>
					<tbody>
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					@foreach (@$roles as $role)
					    <tr>	
					    	<td class="changeTitle">
					    		<a href="{{config('app.cms_slug')}}/admin-user-roles/edit/{{$role->id}}" data-url="{{config('app.cms_slug')}}/admin-user-roles/edit/{{$role->id}}"  data-type="text">{{ $role->role_name }}</a> 
					    		<div class="hide form-block">	
					    			<input type="text" name="" class="input form-control" value="{{ $role->role_name }}"/>
					    		</div>	
					    	</td>
					    	
					    	<td>{{$role->id}}</td>

					    	<td width=" ">
					    	
					    		<a href="{{config('app.cms_slug')}}/admin-user-roles/edit/{{$role->id}}" class="btn btn-warning " title="{{Lang::get('admin.edit')}}"> <i class="fa-pencil"></i></a>
					    		<button class="btn btn-success save" style="display:none;" title="{{Lang::get('admin.save')}}">{{Lang::get('admin.save')}}<i class="fa-ok"></i></button>
					    		<button class="btn btn-danger cancel"  style="display:none;" title="{{Lang::get('admin.cancel')}}">{{Lang::get('admin.cancel')}}</button>
					    	
					    		<a href="{{config('app.cms_slug')}}/admin-user-roles/delete/{{$role->id}}" 
					    			class="btn btn-danger deleteColl"  title="{{Lang::get('admin.delete')}} ">
					    			
					    			<i class="fa-remove"></i>
					    		</a>

					    	</td>
					    </tr>	
					@endforeach
					 
					</tbody>
				</table>

				
				
			</div>

		</div>
	</div>
</div>
@endsection
