
 <div class="row">
	<div class="col-md-12">

  		<?php 
  			$langs = config('app.langs'); 
  			$disabled = (@$user)?'':'disabled';
  			$toggle = (@$user)?'tab':'';
  		?>

		 	
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>
			<!--show roles if user type is not admin-->
            @if (@$user->type==1) 
			<li class="{{$disabled}}">
				<a href="#roles" data-toggle="{{$toggle}}">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">Roles</span>
				</a>
			</li>
			@endif
		
        </ul>

        <div class="tab-content">
			
			<div class="tab-pane active" id="general">

				 
				<div class="form-group">
					{!! Form::label('first_name',Lang::get('admin.first_name')) !!}
					{!! Form::text('first_name', null, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('last_name',Lang::get('admin.last_name')) !!}
					{!! Form::text('last_name', null, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('email',Lang::get('admin.email')) !!}
					{!! Form::text('email', null, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('phone',Lang::get('admin.phone')) !!}
					{!! Form::text('phone', null, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('type',Lang::get('admin.type')) !!}
					{!! Form::select('type', config('app.user_types'), null, ['class'=>'form-control']) !!}					 
				</div>

				@if (!@$user)
					<div class="form-group">
						{!! Form::label('password',Lang::get('admin.password')) !!}
						{!! Form::password('password', ['class'=>'form-control']) !!}					 
					</div>

					<div class="form-group">
						{!! Form::label('password_confirmation',Lang::get('admin.password_confirmation')) !!}
						{!! Form::password('password_confirmation', ['class'=>'form-control']) !!}					 
					</div>
				@endif	
				 
 			</div>

 			<!--show permissions if user type is not admin-->
            @if (@$user->type==1) 

 			<div class="tab-pane" id="roles">
 				 
					<div class="row list-head">
						<div class="col-md-2">{{Lang::get('admin.role_name')}}</div>
						<div class="col-md-2"> </div>

						<div class="col-md-1 col-md-offset-6">{{Lang::get('admin.actions')}}</div>
						
					</div>
					<div  id="roles-list" class="list-content">	

						@if (count(@$user->roles))
						 					 
							@include('admin.admin_users.roles.list',['userRoles'=>@$user->roles,'userid'=>$user->id])
						 
						@endif
					</div>	

					<div class="row">
						<div class="form-group col-md-2">
							{!! Form::label('role_id',Lang::get('admin.roles')) !!}
							<select name="role_id" id="role_id" class="form-control" required>
								<option value="">{{trans('admin.selectroles')}}</option>
								@foreach (@$roles as $roleid=>$role_name)
								    {!! $disabledoption =  (in_array(@$roleid, @$user->roles->lists('id')) ? ' disabled="disabled"' : '') !!}
									<option value="{{$roleid}}" {{$disabledoption}}> {{$role_name}} </option>
								    
								@endforeach
							</select>
							{!! Form::hidden('user_id',$user->id, ['id'=>'user_id']) !!} 				 
						</div>

						<div class="form-group col-md-2">
							<a href="#" class="btn btn-success add-role" data-url= "{{config('app.cms_slug')}}/admin-users/add-role">
								<i class="fa-plus"></i>
							</a>
						</div>	

						<div class="form-group col-md-1">

							
						</div>	
						<div class="form-group col-md-1">
							 
						</div>	
						 
					</div>
				 
 			</div>

 			@endif

 		</div> <!--tab content-->
 			
	</div>				
</div>

<div class="form-group">

	<input type="submit" class="btn btn-success" value="{{Lang::get('admin.save')}}"/>
		
		
	 
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/admin-users" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>			