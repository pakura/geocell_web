@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		<?php $titles = [
	    					0=> ['title'=>Lang::get('admin_menu.guestbook'),'url'=>'collections/get/13'], 
	    					1=> ['title'=>$guestbook->ctitle,'url'=>'guestbook/get/'.$guestbook->cid],
	    					2=> ['title'=>$guestbook->generic_title,'url'=>''],

		    			]; 
		?>
			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::model($guestbook,['url'=>config('app.cms_slug').'/guestbook/update/'.$guestbook->cid.'/'.$guestbook->id]) !!}
					
					@include('admin.guestbook.form')
					
				{!! Form::close() !!}
			
		</div>

	 
	</div>
</div>
@endsection
