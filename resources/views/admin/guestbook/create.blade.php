@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.guestbook'),'url'=>'collections/get/13'], 
		    					1=> ['title'=>$collection->generic_title,'url'=>'guestbook/get/'.$collection->id],
		    					2=> ['title'=>Lang::get('admin.add'),'url'=>''],

			    			];
		     ?>

			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::open(['url'=>config('app.cms_slug').'/guestbook/save/'.$collection->id]) !!}

					@include('admin.guestbook.form')
					
				{!! Form::close() !!}
			
		</div>
	</div>
</div>
@endsection
