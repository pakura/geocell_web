 <div class="row">
	<div class="col-md-12">

  		<?php 
  			$langs = config('app.langs'); 
  			$disabled = (@$guestbook)?'':'disabled';
  			$toggle = (@$guestbook)?'tab':'';
  		?>

		 	
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">{{Lang::get('admin.general')}}</span>
				</a>
			</li>
			<li class="{{$disabled}}">
				<a href="#item_content" data-toggle="{{$toggle}}">
					<span class="visible-xs"><i class="fa-user"></i></span>
					<span class="hidden-xs">{{Lang::get('admin.content')}}</span>
				</a>
			</li>
			
		 
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="general">
				
				<div class="form-group">
					{!! Form::label('generic_title',Lang::get('admin.generic')) !!}
					{!! Form::text('generic_title', null, ['class'=>'form-control']) !!}					 
				</div>	
				
				<div class="form-group">
					{!! Form::label('slug',Lang::get('admin.slug')) !!}
					{!! Form::text('slug', null, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('collection_id',Lang::get('admin.collection')) !!}
					{!! Form::select('collection_id', $collections, @$collection->id, ['class'=>'form-control']) !!}					 
				</div>
				
				<div class="row"> 
	                <div class="form-group ov_hidden col-md-3">
						{!! Form::label('creation_date',Lang::get('admin.create')) !!}        
						
						<div class="date-and-time">
							{!! Form::text('creation_date', null, ['class'=>'form-control datepicker', 'data-format'=>'dd-mm-yyyy']) !!}	
							{!! Form::text('creation_date_time', '10:00 AM', ['class'=>'form-control timepicker', 'data-template'=>'dropdown']) !!}	
				 
	                    </div>
					</div>

					<div class="form-group ov_hidden col-md-3">
						{!! Form::label('expire_date',Lang::get('admin.expire')) !!}

						<div class="date-and-time">
							{!! Form::text('expire_date', null , ['class'=>'form-control datepicker', 'data-format'=>'dd-mm-yyyy']) !!}	
							{!! Form::text('expire_date_time', '08:00 PM', ['class'=>'form-control timepicker', 'data-template'=>'dropdown']) !!}	
				 
	                    </div>					 
					</div>
                </div>

				<div class="form-group">
					{!! Form::label('visibility',Lang::get('admin.visibility')) !!}
					{!! Form::checkbox('visibility',1, 1, ['class'=>'cbr']) !!}					 
				</div>

			</div>
			
		   
			   
			<div class="tab-pane" id="item_content">
			
				<div class="form-group">
					{!! Form::label('name',Lang::get('admin.name')) !!}
					{!! Form::text('name', @$guestbook->content->name, ['class'=>'form-control']) !!}					 
				</div>	

				<div class="form-group">
					{!! Form::label('email',Lang::get('admin.email')) !!}
					{!! Form::text('email', @$guestbook->content->email, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('title',Lang::get('admin.title')) !!}
					{!! Form::text('title', @$guestbook->content->title, ['class'=>'form-control']) !!}					 
				</div>	

				<div class="form-group">
					{!! Form::label('content',Lang::get('admin.content')) !!}
					{!! Form::textarea('content', @$guestbook->content->content, ['class'=>'form-control']) !!}					 
				</div>
					
			</div>
			 
 
		</div>
		
		
	</div>				
</div>
<div class="form-group">

	<input type="submit" class="btn btn-success" value="{{Lang::get('admin.save')}}"/>
		
		
	 
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/guestbook/get/{{@$collection->id?@$collection->id:@$guestbook->collection_id}}" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>
					
			