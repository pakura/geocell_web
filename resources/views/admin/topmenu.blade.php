<nav class="navbar user-info-navbar">
		 
	<ul class="user-info-menu left-links list-inline list-unstyled">
		<li  class="hidden-sm hidden-xs">
			<a href="#" class=" " data-toggle="sidebar">
				<i class="fa-bars"></i>
				<!-- <span>Toggle Sidebar</span> -->
			</a>
		</li>
		 
	</ul>
  
	<ul class="user-info-menu right-links list-inline list-unstyled">
		<li class="search-form" style="min-height: 76px;"><!-- You can add "always-visible" to show make the search input visible -->
				
			<form name="userinfo_search_form" method="get" action="#">
				<input type="text" name="s" class="form-control search-field" placeholder="Type to search...">
				
				<button type="submit" class="btn btn-link">
					<i class="linecons-search"></i>
				</button>
			</form>
			
		</li>
		<li class="dropdown user-profile">
			<a href="#" data-toggle="dropdown">
				<img src="admin/images/user-1.png" alt="user-image" class="img-circle img-inline userpic-32" width="28">
				<span>
					{{ @Auth::user()->name }}
					<i class="fa-angle-down"></i>
				</span>
			</a>
			
			<ul class="dropdown-menu user-profile-menu list-unstyled">
			
				<li>
					<a href="#settings">
						<i class="fa-wrench"></i>
						{{Lang::get('admin.settings')}}
					</a>
				</li>
				
				<li class="last">
					<a href="auth/logout">
						<i class="fa-lock"></i>
						{{Lang::get('admin.logout')}}
					</a>
				</li>
			</ul>
		</li>
		 
			
		 
	</ul>
	 
 
</nav> 