@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		<?php $titles = [
	    					0=> ['title'=>'ussd','url'=>''], 
	    					1=> ['title'=>$ussd->description_en,'url'=>''],

		    			]; 
		?>
			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::model($ussd,['url'=>config('app.cms_slug').'/ussd/update/'.$ussd->id]) !!}
					
					@include('admin.ussd.form')
					
				{!! Form::close() !!}
			
		</div>

	 
	</div>
</div>
@endsection
