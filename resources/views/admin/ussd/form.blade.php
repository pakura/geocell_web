 <div class="row">
	<div class="col-md-12">

  		<?php 
  			$langs = config('app.langs'); 
  		
  		?>

		 	
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>
			
		<!-- 	@foreach ($langs as $key=>$val)
				<li class="">
					<a href="#profile-{{$key}}" data-toggle="tab">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">{{$val}}</span>
					</a>
				</li>
			@endforeach	  -->
		 
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="general">
				
				<div class="form-group">
					{!! Form::label('code','Code') !!}
					{!! Form::text('code', null, ['class'=>'form-control']) !!}					 
				</div>	
				
				
				<div class="form-group">
					{!! Form::label('description_ge','Description ge') !!}
					{!! Form::text('description_ge', null, ['class'=>'form-control']) !!}					 
				</div>	

				<div class="form-group">
					{!! Form::label('description_en','Description en') !!}
					{!! Form::text('description_en', null, ['class'=>'form-control']) !!}					 
				</div>	

				<div class="form-group">
					{!! Form::label('brand_ge','Brand ge') !!}
					{!! Form::text('brand_ge', null, ['class'=>'form-control']) !!}					 
				</div>	

				<div class="form-group">
					{!! Form::label('brand_en','Brand en') !!}
					{!! Form::text('brand_en', null, ['class'=>'form-control']) !!}					 
				</div>	

			</div>
			
	
 
		</div>
		
		
	</div>				
</div>
<div class="form-group">

	<input type="submit" class="btn btn-success" value="{{Lang::get('admin.save')}}"/>
		
		
	 
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/settings/developer/{{config('app.locale')}}" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>
					
			