@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<!--heading gasatania calke failshi-->
			
			 <?php $titles = [
		    					0=> ['title'=>'USSD','url'=>''], 
		    					 

			    			];

			  ?>

			@include('admin.header',['title'=>$titles,'url'=>'ussd/create']) 


			<div class="panel panel-default">


				<table class="table responsive">
						 <thead>
							<tr>
						    	<th class="col-md-2">{{Lang::get('admin.title')}}</th>
						    	
						    	<th class="col-md-6">{{Lang::get('admin.value')}}</th>
						    	
						    	<th class="col-md-2"></th>
						    	<th class="col-md-2">{{Lang::get('admin.actions')}}</th>
						    </tr>
						</thead>  
						 
						<tbody>
						
						@foreach ($ussds as $ussd)
						    <tr>	
						    	<td>
						    		{{ $ussd->code }}
						    	</td>
						    	<td  class="changeTitle" >

						    	    
						    		<a href="{{config('app.cms_slug')}}/ussd/edit/{{$ussd->id}}"   data-type="text">{{ $ussd->description_en }}</a>
							    		 
						    	</td>	

						    	<td></td>
						    	
						    	<td>
						    	    <a href="{{config('app.cms_slug')}}/ussd/edit/{{$ussd->id}}" class="btn btn-warning" title="{{Lang::get('admin.edit')}}"> 
						    	    	<i class="fa-pencil"></i>
						    	    </a>
 


						    	</td>
						    </tr>
						@endforeach
						 
						</tbody>
					</table>

				
				
			</div>

		</div>
	</div>
</div>
@endsection
