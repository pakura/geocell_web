@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.gallbanners'),'url'=>''], 
		    					1=> ['title'=>$collection->generic_title,'url'=>'banners/get/'.$collection->id],
		    					2=> ['title'=>Lang::get('admin.addnews'),'url'=>''],

			    			];
		     ?>

			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::open(['url'=>config('app.cms_slug').'/banners/save/'.$collection->id]) !!}

					@include('admin.banners.form')
					
				{!! Form::close() !!}
			
		</div>
	</div>
</div>
@endsection
