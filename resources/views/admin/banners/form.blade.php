 <div class="row">
	<div class="col-md-12">

  		<?php 
  			$langs = config('app.langs'); 
  			$disabled = (@$banner)?'':'disabled';
  			$toggle = (@$banner)?'tab':'';
  		?>

		 	
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>
			
			@foreach ($langs as $key=>$val)
				<li class="{{$disabled}}">
					<a href="#profile-{{$key}}" data-toggle="{{$toggle}}">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">{{$val}}</span>
					</a>
				</li>
			@endforeach	 
			 
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="general">
				
				<div class="form-group">
					{!! Form::label('generic_title',Lang::get('admin.generic')) !!}
					{!! Form::text('generic_title', null, ['class'=>'form-control']) !!}					 
				</div>	
				
				<div class="form-group">
					{!! Form::label('collection_id',Lang::get('admin.collection')) !!}
					{!! Form::select('collection_id', $collections, @$collection->id, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('url',Lang::get('admin.link')) !!}
					{!! Form::text('url', null, ['class'=>'form-control']) !!}					 
				</div>

				
				<div class="row">
					<div class="col-md-4">
						{!! Form::label('photo',Lang::get('admin.photo')) !!}
						{!! Form::text('photo', null, ['class'=>'form-control','id'=>'file_img']) !!}	
	                </div> 
					<a href="admin/js/ResponsiveFilemanagerMaster/filemanager/dialog.php?type=2&field_id=file_img" class="btn iframe-btn file_up" type="button"><i class="mce-ico mce-i-browse"></i></a>	
				</div>
				<div class="form-group">
					{!! Form::label('open_blank',Lang::get('admin.open_blank')) !!}
					{!! Form::checkbox('open_blank',0, null, ['class'=>'cbr']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('visibility',Lang::get('admin.visibility')) !!}
					{!! Form::checkbox('visibility',1, 1, ['class'=>'cbr']) !!}					 
				</div>

			</div>
			
		    @foreach ($langs as $key=>$val)
			   
				<div class="tab-pane" id="profile-{{$key}}">
				
					<div class="form-group">
						{!! Form::label("{$key}[title]",Lang::get("admin.title")) !!}
						{!! Form::text("{$key}[title]",  @$banner["data"][$key]["title"], ["class"=>"form-control"]) !!}					 
					</div>		

					<div class="form-group collapsed-desc">
						{!! Form::label("{$key}[description]",Lang::get("admin.description")) !!} 
						
						<i class="fa-plus expand-desc-icon"></i>
						<i class="fa-minus collapse-desc-icon"></i>

					    <div class="hide_description">
					    	{!! Form::textarea("{$key}[description]",  @$banner["data"][$key]["description"], ["class"=>"textarea hidden"]) !!}	
					    </div>					 
					</div>
					<!-- <div class="form-group">
						{!! Form::label('language_logo[$key]',Lang::get('admin.logo')) !!}
						{!! Form::file('language_logo['.$key.']', ['class'=>'form-control']) !!}					 
					</div>

					<div class="form-group">
						{!! Form::label('brand_url[$key]',Lang::get('admin.brand_url')) !!}
						{!! Form::text('brand_url['.$key.']', @$brand->brand_url[$key], ['class'=>'form-control']) !!}						 
					</div> -->
						
				</div>
			@endforeach	
			
			 
		</div>
		
		
	</div>				
</div>
<div class="form-group">

	<input type="submit" class="btn btn-success" value="{{Lang::get('admin.save')}}"/>
		
		
	 
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/banners/get/{{@$collection->id?@$collection->id:@$banner->collection_id}}" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>
					
			