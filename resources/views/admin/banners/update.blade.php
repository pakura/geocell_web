@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		<?php $titles = [
	    					0=> ['title'=>Lang::get('admin_menu.gallbanners'),'url'=>''], 
		    				1=> ['title'=>$banner->ctitle,'url'=>'banners/get/'.$banner->cid],
	    					2=> ['title'=>$banner->generic_title,'url'=>''],

		    			]; 
		?>
			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
            @include('errors.admin')

				{!!  Form::model($banner,['url'=>config('app.cms_slug').'/banners/update/'.$banner->cid.'/'.$banner->id]) !!}
					
					@include('admin.banners.form')
					
				{!! Form::close() !!}
			
		</div>


	</div>
</div>
@endsection
