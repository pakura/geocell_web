@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		<?php $titles = [
	    					0=> ['title'=>Lang::get('admin_menu.faq'),'url'=>'collections/get/12'], 
	    					1=> ['title'=>$faq->ctitle,'url'=>'faq/get/'.$faq->cid],
	    					2=> ['title'=>$faq->generic_title,'url'=>''],

		    			]; 
		?>
			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::model($faq,['url'=>config('app.cms_slug').'/faq/update/'.$faq->cid.'/'.$faq->id]) !!}
					
					@include('admin.faq.form')
					
				{!! Form::close() !!}
			
		</div>

	 
	</div>
</div>
@endsection
