 <div class="row">
	<div class="col-md-12">

  		<?php 
  			$langs = config('app.langs'); 
  			$disabled = (@$faq)?'':'disabled';
  			$toggle = (@$faq)?'tab':'';
  		?>

		 	
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>
			
			@foreach ($langs as $key=>$val)
				<li class="{{$disabled}}">
					<a href="#profile-{{$key}}" data-toggle="{{$toggle}}">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">{{$val}}</span>
					</a>
				</li>
			@endforeach	 
		 
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="general">
				
				<div class="form-group">
					{!! Form::label('generic_title',Lang::get('admin.generic')) !!}
					{!! Form::text('generic_title', null, ['class'=>'form-control']) !!}					 
				</div>	
				
				<div class="form-group">
					{!! Form::label('slug',Lang::get('admin.slug')) !!}
					{!! Form::text('slug', null, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('collection_id',Lang::get('admin.collection')) !!}
					{!! Form::select('collection_id', $collections, @$collection->id, ['class'=>'form-control']) !!}					 
				</div>
				
				<div class="row"> 
	                <div class="form-group ov_hidden col-md-3">
						{!! Form::label('creation_date',Lang::get('admin.create')) !!}        
						
						<div class="date-and-time">
							{!! Form::text('creation_date', null, ['class'=>'form-control datepicker', 'data-format'=>'dd-mm-yyyy']) !!}

							<div style="display:none;">
								{!! Form::label('creation_date_time',Lang::get('admin.expire')) !!}
							</div>	
							{!! Form::text('creation_date_time', null, ['class'=>'form-control timepicker', 'data-template'=>'dropdown']) !!}	
				 
	                    </div>
					</div>

					<div class="form-group ov_hidden col-md-3">
						{!! Form::label('expire_date',Lang::get('admin.expire')) !!}

						<div class="date-and-time">
							{!! Form::text('expire_date', null , ['class'=>'form-control datepicker', 'data-format'=>'dd-mm-yyyy']) !!}	

							<div style="display:none;">
								{!! Form::label('expire_date_time',Lang::get('admin.expire')) !!}
							</div>

							{!! Form::text('expire_date_time',null, ['class'=>'form-control timepicker', 'data-template'=>'dropdown']) !!}	
				 
	                    </div>					 
					</div>
                </div>

				<div class="form-group">
					{!! Form::label('visibility',Lang::get('admin.visibility')) !!}
					{!! Form::checkbox('visibility',1, 1, ['class'=>'cbr']) !!}					 
				</div>

			</div>
			
		    @foreach ($langs as $key=>$val)
			   
				<div class="tab-pane" id="profile-{{$key}}">
				
					<div class="form-group">
						{!! Form::label("{$key}[question]",Lang::get('admin.question')) !!}
						{!! Form::text("{$key}[question]", @$faq["data"][$key]["question"], ['class'=>'form-control']) !!}					 
					</div>	

					<div class="form-group">
						{!! Form::label("{$key}[answer]",Lang::get('admin.answer')) !!}
						{!! Form::text("{$key}[answer]", @$faq["data"][$key]["answer"], ['class'=>'textarea']) !!}					 
					</div>

						
				</div>
			@endforeach	
 
		</div>
		
		
	</div>				
</div>
<div class="form-group">

	<input type="submit" class="btn btn-success" value="{{Lang::get('admin.save')}}"/>
		
		
	 
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/faq/get/{{@$collection->id?@$collection->id:@$faq->collection_id}}" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>
					
			