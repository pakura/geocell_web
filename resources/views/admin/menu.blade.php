
<header class="logo-env">
					
	<!-- logo -->
	<div class="logo">
		<a href="{{config('app.cms_slug')}}" class="logo-expanded">
			<!-- <img src="/admin/images/logo@2x.png" width="80" alt=""> -->
			 
		</a>
		
		<a href="{{config('app.cms_slug')}}" class="logo-collapsed">
			 
			<!-- <img src="/admin/images/logo-collapsed@2x.png" width="40" alt=""> -->
		</a>
	</div>
	
	<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
	<div class="mobile-menu-toggle visible-xs">
		<a href="#" data-toggle="user-info-menu">
			<i class="fa-bell-o"></i>
			<span class="badge badge-success">7</span>
		</a>
		
		<a href="#" data-toggle="mobile-menu">
			<i class="fa-bars"></i>
		</a>
	</div>
	
 
	
				
</header>

<ul id="main-menu" class="main-menu">
				
	<li class="has-sub">
		<a href="#">
			<i class="linecons-cog"></i>						
			<span class="title">{{Lang::get('admin_menu.dashboard')}}</span>
		</a>
		<ul>
			<li>
				<a href="{{config('app.cms_slug')}}/reports">
					<span class="title">{{Lang::get('admin_menu.reports')}}</span>
				</a>
			</li>
			<li>
				<a href="{{config('app.cms_slug')}}/newsletters">
					<span class="title">{{Lang::get('admin_menu.newsletter')}}</span>
				</a>
			</li>
			<li>
				<a href="{{config('app.cms_slug')}}/charts">
					<span class="title">{{Lang::get('admin_menu.charts')}}</span>
				</a>
			</li>
			<li>
				<a href="{{config('app.cms_slug')}}/pre-order">
					<span class="title">{{Lang::get('admin_menu.pre-order')}}</span>
				</a>
			</li>
			<li>
				<a href="{{config('app.cms_slug')}}/iphone-stock">
					<span class="title">iphone pre-order Stock</span>
				</a>
			</li>

		</ul>

	</li>

	<li class="has-sub">		
		<a href="#">
			<i class="linecons-desktop"></i>
			<span class="title">{{Lang::get('admin_menu.sitemap')}} </span>
		</a>
		<ul>
			<!--menu groups-->

			@include('admin.menu_partial',['subItems'=>$groups,'type'=>'pages'])
			<li>
				<a href="{{config('app.cms_slug')}}/groups">
					<span class="title"> {{Lang::get('admin_menu.menu')}}</span>
				</a>
			</li>

		</ul>

	</li>

	<li class="has-sub">
		<a href="#">
			<i class="linecons-desktop"></i>
			<span class="title">{{Lang::get('admin_menu.about')}} </span>
		</a>
		<ul>
			<li>
				<a href="{{config('app.cms_slug')}}/awards/get">
					<span class="title"> {{Lang::get('admin_menu.awards')}}</span>
				</a>
			</li>
		</ul>
	</li>

	@if(Auth::user()->hasPermission(6))
  
	<li class="has-sub">		
		<a href="#">
			<i class="linecons-note"></i>
			<span class="title">{{Lang::get('admin_menu.newsarticles')}} </span>
		</a>
		<ul id="collections">

			{!! drawSubMenuList($menuCollections[6],'articles') !!}


			<li>
				<a href="{{config('app.cms_slug')}}/collections/get/6">
					<span class="title">{{Lang::get('admin_menu.news')}}</span>
				</a>
			</li>

			<li>
				<a href="{{config('app.cms_slug')}}/internationalline/en">
					<span class="title">{{Lang::get('admin_menu.international')}}</span>
				</a>
			</li>


		</ul>
	</li>

	@endif

	@if(Auth::user()->hasPermission(8))
	<li class="has-sub">		
		<a href="#">
			<i class="linecons-photo"></i>
			<span class="title">{{Lang::get('admin_menu.gallbanners')}} </span>
		</a>
		<ul>
			
			<li>
				<a href="{{config('app.cms_slug')}}/collections/get/8">
					<span class="title">{{Lang::get('admin_menu.gallist')}}</span>
				</a>
			</li>
			<li>
				<a href="{{config('app.cms_slug')}}/collections/get/16">
					<span class="title">{{Lang::get('admin_menu.banners')}}</span>
				</a>
			</li>
		<!-- 	<li>
				<a href="{{config('app.cms_slug')}}/banner-list">
					<span class="title">{{Lang::get('admin_menu.bannerlist')}}</span>
				</a>
			</li> -->

		</ul>
	</li> 
    @endif	

    @if(Auth::user()->hasPermission(15))
  
	<li class="has-sub">		
		<a href="#">
			<i class="fa-list-ul"></i>
			<span class="title">{{Lang::get('admin_menu.custom_list')}} </span>
		</a>
		<ul id="collections">
            @if(array_key_exists(15,$menuCollections))
				{!! drawSubMenuList($menuCollections[15],'custom_lists') !!}

			@endif
			<li>
				<a href="{{config('app.cms_slug')}}/collections/get/15">
					<span class="title">{{Lang::get('admin_menu.custom_list_m')}}</span>
				</a>
			</li>
			
		</ul>
	</li>

	@endif
   
   @if(Auth::user()->hasPermission(4))
	<li class="has-sub">		
		
		<a href="#">
			<i class="linecons-diamond"></i>
			<span class="title">{{Lang::get('admin_menu.stock')}} </span>
		</a>
		<ul>
			
			<li>
				<a href="{{config('app.cms_slug')}}/stocks">
					<span class="title">{{Lang::get('admin_menu.stocklist')}}</span>
				</a>
			</li>
			<li>
				<a href="{{config('app.cms_slug')}}/brands">
					<span class="title">{{Lang::get('admin_menu.brands')}}</span>
				</a>
			</li>
			<li>
				<a href="{{config('app.cms_slug')}}/product-options">
					<span class="title">{{Lang::get('admin_menu.prodoptions')}}</span>
				</a>
			</li>
			<li>
				<a href="{{config('app.cms_slug')}}/collections/get/4">
					<span class="title">{{Lang::get('admin_menu.prodcats')}}</span>
				</a>
			</li>
			<li>
				<a href="{{config('app.cms_slug')}}/import-product">
					<span class="title">{{Lang::get('admin_menu.iprodlist')}} </span>
				</a>
			</li>
			<li>
				<a href="{{config('app.cms_slug')}}/export-product">
					<span class="title">{{Lang::get('admin_menu.eprodlist')}}</span>
				</a>
			</li>

		</ul>
	</li>
    @endif	
    
    @if(Auth::user()->hasPermission(12))
	<li class="has-sub">		
		<a href="#">
			<i class="fa-question"></i>
			<span class="title">{{Lang::get('admin_menu.faq')}} </span>
		</a>
		<ul>
			{!! drawSubMenuList($menuCollections[12],'faq') !!}

				
			<li>
				<a href="{{config('app.cms_slug')}}/collections/get/12">
					<span class="title">{{Lang::get('admin_menu.faqmanagement')}}</span>
				</a>
			</li>
			
		</ul>
	</li>
    @endif

    @if(Auth::user()->hasPermission(13))
		<li class="has-sub">
			<a href="#">
				<i class="fa-bullhorn"></i>
				<span class="title">{{Lang::get('admin_menu.guestbook')}} </span>
			</a>
		<ul>
			{!! drawSubMenuList($menuCollections[13],'guestbook') !!}			
			
			
			<li>
				<a href="{{config('app.cms_slug')}}/collections/get/13">
					<span class="title">{{Lang::get('admin_menu.gbmanagement')}}</span>
				</a>
			</li>
			

		</ul>
	</li>
	@endif
    
    @if(Auth::user()->isAdmin)
	<li class="has-sub">			
		<a href="#">
			<i class="linecons-user"></i>
			<span class="title">{{Lang::get('admin_menu.user')}}</span>
		</a>
		<ul>
			
			<li>
				<a href="{{config('app.cms_slug')}}/admin-users">
					<span class="title">{{Lang::get('admin_menu.cmsuser')}}</span>
				</a>
			</li>
			<li>
				<a href="{{config('app.cms_slug')}}/admin-user-roles">
					<span class="title">{{Lang::get('admin_menu.cmsuserroles')}}</span>
				</a>
			</li>
			<!-- <li>
				<a href="{{config('app.cms_slug')}}/admin-uer-permissions">
					<span class="title">{{Lang::get('admin_menu.cmsuserperm')}}</span>
				</a>
			</li> 
			<li>
				<a href="{{config('app.cms_slug')}}/site-user-list">
					<span class="title">{{Lang::get('admin_menu.siteuser')}}</span>
				</a>
			</li>
			--> 

		</ul>
	</li>
	@endif

	<li class="has-sub">			
		
		<a href="#">
			<i class="linecons-params"></i>

			<span class="title">{{Lang::get('admin_menu.settings')}}</span>
		</a>	
		<ul>
			@if(Auth::user()->isAdmin)
				<li>
					<a href="{{config('app.cms_slug')}}/settings/get/{{config('app.locale')}}">
						<span class="title">{{Lang::get('admin_menu.sitesett')}}</span>
					</a>
				</li>

				<li>
					<a href="{{config('app.cms_slug')}}/settings/developer/{{config('app.locale')}}">
						<span class="title">Developer</span>
					</a>
				</li>

				<li>
					<a href="{{config('app.cms_slug')}}/language-data/get/{{config('app.locale')}}">
						<span class="title">{{Lang::get('admin_menu.langdata')}}</span>
					</a>
				</li>


				<li>
					<a href="{{config('app.cms_slug')}}/ussd/get">
						<span class="title">USSD</span>
					</a>
				</li>
			@endif
			<!-- <li>
				<a href="{{config('app.cms_slug')}}/backup-restore">
					<span class="title">Backup / Restore</span>
				</a>
			</li> -->
			<li>
				<a href="{{config('app.cms_slug')}}/text-converter">
					<span class="title">{{Lang::get('admin_menu.textconv')}}</span>
				</a>
			</li>
			 

		</ul>
	</li>
<!--
	<li class="has-sub">		
		<a href="#">
			<i class="linecons-globe"></i>
			<span class="title">{{Lang::get('admin_menu.tandt')}}</span>
		</a>

		<ul>
			
			<li>
				<a href="{{config('app.cms_slug')}}/ticket-list">
					<span class="title">{{Lang::get('admin_menu.ticket')}}</span>
				</a>
			</li>
			<li>
				<a href="{{config('app.cms_slug')}}/chat">
					<span class="title">{{Lang::get('admin_menu.chat')}}</span>
				</a>
			</li>

		</ul>
	</li>
-->
	<li class="has-sub">			
		<a href="#">
			<i class="linecons-cloud"></i>
			<span class="title">{{Lang::get('admin_menu.logs')}}</span>
		</a>
		<ul>
			
			<li>
				<a href="{{config('app.cms_slug')}}/cms-log">
					<span class="title">{{Lang::get('admin_menu.cmslog')}}</span>
				</a>
			</li>
			<li>
				<a href="{{config('app.cms_slug')}}/site-log">
					<span class="title">{{Lang::get('admin_menu.sitelog')}}</span>
				</a>
			</li>

		</ul>
	</li>


</ul>		