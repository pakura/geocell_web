 <div class="row">
	<div class="col-md-12">

 
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>			
		 
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="general">
				
				<div class="form-group">
					{!! Form::label('settings_key',Lang::get('admin.key')) !!}
					{!! Form::text('settings_key', null, ['class'=>'form-control']) !!}					 
				</div>	
				
				<div class="form-group">
					{!! Form::label('type',Lang::get('admin.type')) !!}
					{!! Form::select('type', config('app.settings_type'), null, ['class'=>'form-control']) !!}					 
				</div>

			</div>
			
		    <div class="form-group">
				{!! Form::label('title',Lang::get('admin.title')) !!}
				{!! Form::text('title',  @$setting->title, ['class'=>'form-control']) !!}					 
			</div>	

			<div class="form-group">
				{!! Form::label('initial',Lang::get('admin.initial')) !!}
				{!! Form::text('initial', @$setting->initial, ['class'=>'form-control']) !!}					 
			</div>
 
		</div>
		
		
	</div>				
</div>
<div class="form-group">

	<input type="submit" class="btn btn-success" value="{{Lang::get('admin.save')}}"/>
		
		
	 
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/settings/developer/{{config('app.locale')}}" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>
					
			