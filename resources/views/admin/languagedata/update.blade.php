@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		<?php $titles = [
	    					0=> ['title'=>Lang::get('admin_menu.settings'),'url'=>''], 
	    					1=> ['title'=>$setting->generic_title,'url'=>''],

		    			]; 
		?>
			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::model($setting,['url'=>config('app.cms_slug').'/settings/update/'.$setting->id]) !!}
					
					@include('admin.settings.form-update')
					
				{!! Form::close() !!}
			
		</div>

	 
	</div>
</div>
@endsection
