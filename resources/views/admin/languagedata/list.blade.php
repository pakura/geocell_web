@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<!--heading gasatania calke failshi-->
			
			 <?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.langdata'),'url'=>''], 
		    					 

			    			];

			  ?>

			@include('admin.header',['title'=>$titles,'url'=>'language-data/create']) 

			<ul class="nav nav-tabs">
				{{--  $key==config('app.locale')? 'active':''--}}				
				@foreach (config('app.langs') as $key=>$val)
					<li class="">
						<a href="{{config('app.cms_slug')}}/language-data/get/{{$key}}" >
							<span class="visible-xs"><i class="fa-user"></i></span>
							<span class="hidden-xs">{{$val}}</span>
						</a>
					</li>
				@endforeach	
					 
				 
			</ul>
			<div class="panel panel-default">


				<table class="table responsive">
						 <thead>
							<tr>
						    	<th class="col-md-6">{{Lang::get('admin.title')}}</th>
						    	
						    	<th class="col-md-2">{{Lang::get('admin.value')}}</th>
						    	
						    	<th class="col-md-2"></th>
						    	<th class="col-md-2">{{Lang::get('admin.actions')}}</th>
						    </tr>
						</thead>  
						 
						<tbody>
						
						@foreach ($languagedata as $langData)
						    <tr>	
						    	<td>
						    		{{ $langData->data_key }}
						    	</td>
						    	<td  class="changeTitle" >

						    	    
							    		<a href="javascript:;" data-url="{{config('app.cms_slug')}}/language-data/edit/{{$langData->id}}" data-type="text">{{ $langData->title }}</a>
							    		<div class="hide form-block">
							    			<input type="text" name="" class="input form-control" value="{{ $langData->title }}" />
							    		</div>	
						    		

						    	

						    	</td>	

						    	<td></td>
						    	
						    	<td>
						    	    <a href="{{config('app.cms_slug')}}/language-data/edit/{{$langData->id}}" class="btn btn-warning editcollection" title="{{Lang::get('admin.edit')}}"> 
						    	    	<i class="fa-pencil"></i>
						    	    </a>

						    	    <button class="btn btn-success save" style="display:none;" title="{{Lang::get('admin.save')}}">{{Lang::get('admin.save')}}<i class="fa-ok"></i></button>
					    			<button class="btn btn-danger cancel"  style="display:none;" title="{{Lang::get('admin.cancel')}}">{{Lang::get('admin.cancel')}}</button>


						    	</td>
						    </tr>	
						@endforeach
						 
						</tbody>
					</table>

				
				
			</div>

		</div>
	</div>
</div>
@endsection
