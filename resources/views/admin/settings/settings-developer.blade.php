@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<!--heading gasatania calke failshi-->
			
			 <?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.settings'),'url'=>''], 
		    					 

			    			];
			    $setting_types = config('app.settings_type');			
			  ?>

			@include('admin.header',['title'=>$titles,'url'=>'settings/create']) 

			<ul class="nav nav-tabs">
				{{--  $key==config('app.locale')? 'active':''--}}				
				@foreach (config('app.langs') as $key=>$val)
					<li class="">
						<a href="{{config('app.cms_slug')}}/settings/developer/{{$key}}" >
							<span class="visible-xs"><i class="fa-user"></i></span>
							<span class="hidden-xs">{{$val}}</span>
						</a>
					</li>
				@endforeach					 
			</ul>

			<div class="panel panel-default">
				<table class="table responsive">
						 <thead>
							<tr>
						    	<th class="col-md-2">{{Lang::get('admin.title')}}</th>
						    	
						    	<th class="col-md-2">{{Lang::get('admin.key')}}</th>
						    	<th class="col-md-2">{{Lang::get('admin.type')}}</th>
						    	<th class="col-md-2">{{Lang::get('admin.initial')}}</th>
						    	<th class="col-md-2"></th>
						    	<th class="col-md-2">{{Lang::get('admin.actions')}}</th>
						    </tr>
						</thead>  
						 
						<tbody id="item_sort" data-url="{{config('app.cms_slug')}}/settings/reorder">
						@foreach ($settings as $setting)
						    <tr class="ui-sortable-handle" data-action="parent" id="{{$setting->id}}">	
						    	<td>
						    		<a href="{{config('app.cms_slug')}}/settings/edit-developer/{{$setting->id}}">{{ $setting->title }}</a> 
						    	</td>
						    	<td>
						    		{{ $setting->settings_key }}
						    	</td>	

						    	<td>
						    		{{ $setting_types[$setting->type] }}
						    	</td>

						    	<td>
						    		{{ $setting->initial }}
						    	</td>

						    	<td></td>
						    	
						    	<td>
						    	    <a href="{{config('app.cms_slug')}}/settings/edit-developer/{{$setting->id}}" class="btn btn-warning" title="{{Lang::get('admin.edit')}}"> <i class="fa-pencil"></i></a>

						    		<a href="{{config('app.cms_slug')}}/settings/delete/{{$setting->id}}" class="btn btn-danger" title="{{Lang::get('admin.delete')}}" onclick="return confirm('Are you sure?')"> <i class="fa-remove"></i></a> 

						    	</td>
						    </tr>	
						@endforeach
						 
						</tbody>
					</table>

				
				
			</div>

		</div>
	</div>
</div>
@endsection
