@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<!--heading gasatania calke failshi-->
			
			 <?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.settings'),'url'=>''], 
		    					 

			    			];

			  ?>

			@include('admin.header',['title'=>$titles,'url'=>'settings/create']) 

			<ul class="nav nav-tabs">
				{{--  $key==config('app.locale')? 'active':''--}}				
				@foreach (config('app.langs') as $key=>$val)
					<li class="">
						<a href="{{config('app.cms_slug')}}/settings/get/{{$key}}" >
							<span class="visible-xs"><i class="fa-user"></i></span>
							<span class="hidden-xs">{{$val}}</span>
						</a>
					</li>
				@endforeach	
					 
				 
			</ul>
			<div class="panel panel-default">


				<table class="table responsive">
						 <thead>
							<tr>
						    	<th class="col-md-6">{{Lang::get('admin.title')}}</th>
						    	
						    	<th class="col-md-2">{{Lang::get('admin.value')}}</th>
						    	
						    	<th class="col-md-2"></th>
						    	<th class="col-md-2">{{Lang::get('admin.actions')}}</th>
						    </tr>
						</thead>  
						 
						<tbody>
						
						@foreach ($settings as $setting)
						    <tr>	
						    	<td>
						    		{{ $setting->title }}
						    	</td>
						    	<td  class="changeTitle" >

						    	    @if($setting->type===1)
							    		<a href="#" data-url="{{config('app.cms_slug')}}/settings/edit/{{$setting->id}}" data-type="text">{{ $setting->value }}</a>
							    		<div class="hide form-block">
							    			<input type="text" name="" class="input form-control" value="{{ $setting->value }}" />
							    		</div>	
						    		@endif

						    		@if($setting->type===2)
						    			<?php $values = explode(",", $setting->initial); ?>					    			

							    		<a href="#" data-url="{{config('app.cms_slug')}}/settings/edit/{{$setting->id}}" data-type="list">{{ $values[$setting->value] }}</a>
							    		<div class="hide form-block"> 
							    			{!! Form::select('value', $values, $setting->value, ['class'=>'form-control']) !!}
							    		</div>	
						    		@endif

						    		@if($setting->type===3)
						    			<a href="#" data-url="{{config('app.cms_slug')}}/settings/edit/{{$setting->id}}" data-type="image" >
							    			@if($setting->value)
							    				<img src="{{$setting->value}}" width="60">
							    			@endif
							    		</a>	
							    		<div class="hide form-block">
							    			<a  href="/admin/js/ResponsiveFilemanagerMaster/filemanager/dialog.php?type=2&field_id=file_{{$setting->id}}" class="btn iframe-btn file_up fa-cloud-upload" type="button" data-url="{{config('app.cms_slug')}}/settings/edit/{{$setting->id}}" ></a>
							    		    <input type="text" name="" id="file_{{$setting->id}}" class="input form-control changefile" value="{{ $setting->value }}" />
							    		</div>
							    		
						    		@endif

						    	</td>	

						    	<td></td>
						    	
						    	<td>
						    	    <a href="{{config('app.cms_slug')}}/settings/edit/{{$setting->id}}" class="btn btn-warning editcollection" title="{{Lang::get('admin.edit')}} "><i class="fa-pencil"></i></a>

						    	    <button class="btn btn-success save" style="display:none;" title="{{Lang::get('admin.save')}}">{{Lang::get('admin.save')}}<i class="fa-ok"></i></button>
					    			<button class="btn btn-danger cancel"  style="display:none;" title="{{Lang::get('admin.cancel')}}">{{Lang::get('admin.cancel')}}</button>

						    		<!-- <a href="{{config('app.cms_slug')}}/settings/delete/{{$setting->id}}" class="btn btn-danger" onclick="return confirm('Are you sure?')">{{Lang::get('admin.delete')}} <i class="fa-remove"></i></a> 
 -->

						    	</td>
						    </tr>	
						@endforeach
						 
						</tbody>
					</table>

				
				
			</div>

		</div>
	</div>
</div>
@endsection
