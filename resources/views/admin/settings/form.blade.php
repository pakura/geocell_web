 <div class="row">
	<div class="col-md-12">

  		<?php 
  			$langs = config('app.langs'); 
  		
  		?>

		 	
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>
			
			@foreach ($langs as $key=>$val)
				<li class="">
					<a href="#profile-{{$key}}" data-toggle="tab">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">{{$val}}</span>
					</a>
				</li>
			@endforeach	 
		 
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="general">
				
				<div class="form-group">
					{!! Form::label('settings_key',Lang::get('admin.key')) !!}
					{!! Form::text('settings_key', null, ['class'=>'form-control']) !!}					 
				</div>	
				
				<div class="form-group">
					{!! Form::label('type',Lang::get('admin.type')) !!}
					{!! Form::select('type', config('app.settings_type'), null, ['class'=>'form-control']) !!}					 
				</div>

			</div>
			
		    @foreach ($langs as $key=>$val)
			   
				<div class="tab-pane" id="profile-{{$key}}">
				
					<div class="form-group">
						{!! Form::label('title[$key]',Lang::get('admin.title')) !!}
						{!! Form::text('title['.$key.']',  @$setting->title[$key], ['class'=>'form-control']) !!}					 
					</div>	

					<div class="form-group">
						{!! Form::label('initial[$key]',Lang::get('admin.initial')) !!}
						{!! Form::text('initial['.$key.']', @$setting->initial[$key], ['class'=>'form-control']) !!}					 
					</div>

						
				</div>
			@endforeach	
 
		</div>
		
		
	</div>				
</div>
<div class="form-group">

	<input type="submit" class="btn btn-success" value="{{Lang::get('admin.save')}}"/>
		
		
	 
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/settings/developer/{{config('app.locale')}}" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>
					
			