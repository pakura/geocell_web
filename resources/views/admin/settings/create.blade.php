@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.settings'),'url'=>''], 
		    					1=> ['title'=>Lang::get('admin.add'),'url'=>''],

			    			];
		     ?>

			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::open(['url'=>config('app.cms_slug').'/settings/save']) !!}

					@include('admin.settings.form')
					
				{!! Form::close() !!}
			
		</div>
	</div>
</div>
@endsection
