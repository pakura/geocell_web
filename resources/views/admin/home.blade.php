@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					You are logged in as admin!
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
