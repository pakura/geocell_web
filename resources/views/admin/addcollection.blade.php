@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		    
		    <?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.newsarticles'),'url'=>''], 
		    					1=> ['title'=>Lang::get('admin_menu.news'),'url'=>'collections/get/6']

		    				]; 
		    ?>

			@include('admin.header',['title'=>$titles,'url'=>'']) 				
				
			<div class="panel panel-default">	
			    <h3>{{Lang::get('admin.add_collection')}}</h3>	
				<form action="{{config('app.cms_slug')}}/collections/save/{{$type}}" method="post" role="form" id="login" class="login-form fade-in-effect">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					
					<div class="form-group">
						<label class="control-label" for="generic_title">{{Lang::get('admin.generic')}}</label>
						<input type="text" class="form-control input-dark" name="generic_title" value="{{ old('generic_title') }}"  />
					</div>					
					
					<div class="form-group">
						<button type="submit" class="btn btn-success">
							
							{{Lang::get('admin.save')}}
						</button>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
