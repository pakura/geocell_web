@extends('admin')

@section('content')
    <div class="container_">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>{{Lang::get('admin_menu.newsletter')}}</h1>
                        <br>
                        <br>
                        <div style="background-color: #CCC; border-radius: 6px; width: 100%; padding: 12px; height: 90px">
                            {!! Form::open(array('url'=>'/cms/newsletters/backlist','method'=>'POST', 'files'=>true)) !!}
                            <div class="form-group ov_hidden col-md-6">
                                <label for="creation_date">Upload blacklist: </label>

                                <div class="date-and-time">
                                    {!! Form::file('image') !!}
                                </div>
                            </div>

                            {!! Form::submit('Submit', array('class'=>'btn btn-success')) !!}
                            {!! Form::close() !!}
                        </div>
                        <br>
                        <hr>
                        <br>

                        <div id="tabledata">
                            <table style="width: 100%; color: #666" id="tab_0">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Email</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $key => $sub)
                                    <tr>
                                        <td>{{$sub->id}}</td>
                                        <td>{{$sub->email}}</td>
                                        <td style="cursor: pointer"><a href="/developer_version/public/cms/deleteblacklist/{{$sub->id}}"> Delete </a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        $(document).ready(function() {
            $('#tab_0').DataTable({
                "iDisplayLength": 25,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            });
        } );

        function deleteSub(id){
            $.ajax({
                method: "POST",
                url: "/developer_version/public/cms/deleteblacklist",
                data: { id: id }
            })
            .done(function( msg ) {
                if(msg == false){
                    return;
                }
                showCountry(JSON.parse(msg), gsmtype);
            });
        }

</script>
@endsection
