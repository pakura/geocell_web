@extends('admin')

@section('content')
    <div class="container_">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>{{Lang::get('admin_menu.newsletter')}}</h1>
                        <br>
                        <br>
                        <div style="background-color: #CCC; border-radius: 6px; width: 100%; padding: 12px; height: 90px">
                            <div class="form-group ov_hidden col-md-3">
                                <label for="creation_date">Filter From: </label>

                                <div class="date-and-time">
                                    <input class="form-control datepicker" data-format="yyyy-mm-dd" name="creation_date" type="text" id="from" value="<?= isset($from)?$from:date('Y-m').'-01' ?>">
                                </div>
                            </div>
                            <div class="form-group ov_hidden col-md-3">
                                <label for="creation_date">To: </label>

                                <div class="date-and-time">
                                    <input class="form-control datepicker" data-format="yyyy-mm-dd" name="creation_date" type="text" id="to" value="<?= isset($to)?$to:date('Y-m-d') ?>">
                                </div>
                            </div>
                            <div class="form-group ov_hidden col-md-3">
                                <a href="https://geocell.ge/developer_version/public/cms/blacklist"> <button style="float: left; margin-top: 24px;" class="btn btn-success">blacklists</button></a>
                            </div>
                            <button style="float: right; margin-top: 18px; width: 200px" class="btn btn-success" onclick="filter()">Filter</button>
                        </div>
                        <br>
                        <hr>
                        <br>
                        <div id="tabledata">
                            {{--<table cellpadding="3" cellspacing="0" border="0" style="width: 100%; margin:25px 7px;">--}}
                               {{--<tbody>--}}
                               {{--<tr>--}}
                               {{--<td>Search: </td>--}}
                                {{--<td id="filter_col1" data-column="1" style="height: 30px;">--}}
                                    {{--<td align="center"><input type="text" class="column_filter" id="col1_filter" placeholder="Column - Email"></td>--}}
                                    {{--<td style="display: none" align="center"><input type="checkbox" class="column_filter" id="col1_regex"></td>--}}
                                    {{--<td style="display: none" align="center"><input type="checkbox" class="column_filter" id="col1_smart" checked="checked"></td>--}}
                                {{--</td>--}}
                                {{--<td id="filter_col2" data-column="2" style="height: 30px;">--}}
                                    {{--<td align="center"><input type="text" class="column_filter" id="col2_filter" placeholder="Column - Phone"></td>--}}
                                    {{--<td style="display: none" align="center"><input type="checkbox" class="column_filter" id="col2_regex"></td>--}}
                                    {{--<td style="display: none" align="center"><input type="checkbox" class="column_filter" id="col2_smart" checked="checked"></td>--}}
                                {{--</td>--}}
                                {{--<td id="filter_col3" data-column="3" style="height: 30px;">--}}
                                    {{--<td align="center"><input type="text" class="column_filter" id="col3_filter" placeholder="Column - Note"></td>--}}
                                    {{--<td style="display: none" align="center"><input type="checkbox" class="column_filter" id="col3_regex"></td>--}}
                                    {{--<td style="display: none" align="center"><input type="checkbox" class="column_filter" id="col3_smart" checked="checked"></td>--}}
                                {{--</td>--}}
                                {{--<td id="filter_col4" data-column="4" style="height: 30px;">--}}
                                    {{--<td align="center"><input type="text" class="column_filter" id="col4_filter" placeholder="Column - Remarks"></td>--}}
                                    {{--<td style="display: none" align="center"><input type="checkbox" class="column_filter" id="col4_regex"></td>--}}
                                    {{--<td style="display: none" align="center"><input type="checkbox" class="column_filter" id="col4_smart" checked="checked"></td>--}}
                                {{--</td>--}}
                               {{--</tr>--}}
                                {{--</tbody>--}}
                            {{--</table>--}}







                            <table style="width: 100%; color: #666" id="tab_0">
                                <thead>
                                <tr>
                                    <th>опции</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>GSM Type</th>
                                    <th>Stuff</th>
                                    <th>Phone name</th>
                                    <th>4G compatible</th>
                                    <th>B2B/B2C</th>
                                    <th>Note</th>
                                    <th>Remarks</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Date</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Status</th>
                                    <th>GSM Type</th>
                                    <th>Stuff</th>
                                    <th>Phone name</th>
                                    <th>4G compatible</th>
                                    <th>B2B/B2C</th>
                                    <th>Note</th>
                                    <th>Remarks</th>
                                    <th>Edit</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($data as $key => $sub)
                                        <tr>
                                            <td>{{$sub->autodate}}</td>
                                            <td @if($sub->black_list == 1) style="color: #ff605b" @endif >{{$sub->email}}</td>
                                            <td>{{isset($sub->phone)?$sub->phone:'No GSM'}}</td>
                                            <td> @if($sub->status == 1) Subscribed @else Unsubscribed @endif </td>
                                            <td> @if($sub->gsm_type == 2) Prepaid @else Postpaid @endif </td>
                                            <td> @if($sub->sub_type == 0) No staff member @else Staff member @endif </td>
                                            <td>{{isset($sub->phone_name)?$sub->phone_name:'No Phone'}}</td>
                                            <td> @if(isset($sub->g4)) @if($sub->g4 == 1) Yes @else No @endif @else No data @endif </td>
                                            <td> @if(isset($sub->b2b)) @if($sub->b2b == 0) B2B @else B2C @endif @else No data @endif </td>
                                            <th>{{isset($sub->note)?$sub->note:''}}</th>
                                            <th>{{isset($sub->remarks)?$sub->remarks:''}}</th>
                                            <th><a href="/developer_version/public/cms/newsletters/edit/{{$sub->id}}">Edit</a></th>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            var table = $('#tab_0').DataTable({
                "iDisplayLength": 25,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel',
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                    },
                    'print'
                ]
            });
            $('#tab_0_filter').fadeOut();
            var cnt = 0;
           $('#tab_0 tfoot th').each( function () {
               if(cnt == 1 || cnt == 2 || cnt == 9 ||  cnt == 6 || cnt == 10){
                   var title = $(this).text();
                   $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
               }
               cnt++;
            } );
            var searchcnt = 0;
            table.columns().every( function () {
                if(searchcnt == 1 || searchcnt == 2 || searchcnt == 9 || searchcnt == 6 ||  searchcnt == 10 ) {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                }
                searchcnt++;
            } );

            var selectcnt = 0;
            table.columns().every( function () {
                if(selectcnt == 3 || selectcnt == 4 || selectcnt == 5 || selectcnt == 7 || selectcnt == 8){
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                            );

                            column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                        } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' );
                    } );
                }
                selectcnt++;
            } );

//            $('input.column_filter').on( 'keyup click', function () {
//                filterColumn( $(this).parents('tr').attr('data-column') );
//            } );
        } );

        function filter(){

            var from = $('#from').val();
            var to = $('#to').val();
            var url = '/developer_version/public/cms/newsletters/'+from+'/'+to;
            window.location.href = url;

        }

        function writeData(arg){
            $('#tabledata').html('<table style="width: 100%; color: #666" id="tab_0">\
            <thead>\
            <tr>\
            <th>N</th>\
            <th>Name</th>\
            <th>Quantity</th>\
            </tr>\
            </thead>\
            <tbody>');
            var cnt = 0;
            for (var ii in arg){
                cnt++;
                $('#tabledata').append('<tr>\
                        <td>'+(cnt++)+'</td>\
                        <td style="background-color: #7722ff; color: #FFF;">'+sku2text(ii)+'</td>\
                <td></td>\
                </tr>');
                for (var jj in arg[ii]){

                }
            }

            $('#tabledata').append('</tbody>\
                    </table>');
        }

        function sku2text(arg){
            switch (arg){
                case 'logins':
                    return 'Private Cabinet Stats';
                    break;
                case 'password_type':
                    return 'Password Status';
                    break;
                case 'activation_status':
                    return 'Activation Status';
                    break;
                case 'services':
                    return 'Successful activations';
                    break;
                default:
                    return arg;
                    break;
            }
        }

    </script>
    <style>
        input{
            width: 60px;
        }
    </style>
@endsection
