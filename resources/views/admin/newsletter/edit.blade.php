@extends('admin')

@section('content')
    <div class="container_">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>{{Lang::get('admin_menu.newsletter')}}</h1>
                        <br>
                        <br>

                        <form action="/developer_version/public/cms/newsletters/save/{{$data->id}}" method="post">
                            <label for="note">Note:</label><br>
                            <textarea id="note" name="note">{{$data->note}}</textarea>
                            <br><br><br>
                            <label for="remark">Remarks:</label><br>
                            <textarea id="remark" name="remark">{{$data->remarks}}</textarea>
                            <br><br><br>
                            <label for="blacklisted">Mark for adding into blacklist</label><br>
                            <input type="checkbox" name="blacklisted" id="blacklisted" @if($data->black_list == 1) checked @endif>
                            <br><br><br><br>
                            <input type="submit" value="Save" class="btn btn-success"/>
                            <a href="/newsletters"><button class="btn btn-warning">Cancel</button></a>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

