@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		<?php $titles = [
	    					0=> ['title'=>Lang::get('admin_menu.gallbanners'),'url'=>'collections/get/8'], 
	    					1=> ['title'=>Lang::get('admin_menu.gallist'),'url'=>'collections/get/8'],
	    					2=> ['title'=>$gallery->ctitle,'url'=>'gallery/get/'.$gallery->cid],
	    					3=> ['title'=>$gallery->generic_title,'url'=>''],

		    			]; 
		?>
			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
            @include('errors.admin')

				{!!  Form::model($gallery,['url'=>config('app.cms_slug').'/gallery/update/'.$gallery->collection_id.'/'.$gallery->id]) !!}
					
					@include('admin.galleries.form')
					
				{!! Form::close() !!}
			
		</div>


	</div>
</div>
@endsection
