@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.newsarticles'),'url'=>'collections/get/8'],
		    					1=> ['title'=>Lang::get('admin_menu.gallist'),'url'=>'collections/get/8'], 
		    					2=> ['title'=>$collection->generic_title,'url'=>'gallery/get/'.$collection->id],
		    					3=> ['title'=>Lang::get('admin.addnews'),'url'=>''],

			    			];
		     ?>

			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::open(['url'=>config('app.cms_slug').'/gallery/save/'.$collection->id]) !!}

					@include('admin.galleries.form')
					
				{!! Form::close() !!}
			
		</div>
	</div>
</div>
@endsection
