 <div class="row">
	<div class="col-md-12">

  		<?php 
  			$langs = config('app.langs'); 
  			$disabled = (@$gallery)?'':'disabled';
  			$toggle = (@$gallery)?'tab':'';
  		?>

		 	
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>
			
			@foreach ($langs as $key=>$val)
				<li class="{{$disabled}}">
					<a href="#profile-{{$key}}" data-toggle="{{$toggle}}">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">{{$val}}</span>
					</a>
				</li>
			@endforeach	 
			
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="general">
				
				<div class="form-group">
					{!! Form::label('generic_title',Lang::get('admin.generic')) !!}
					{!! Form::text('generic_title', null, ['class'=>'form-control']) !!}					 
				</div>	
				
				<div class="form-group">
					{!! Form::label('slug',Lang::get('admin.slug')) !!}
					{!! Form::text('slug', null, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('collection_id',Lang::get('admin.collection')) !!}
					{!! Form::select('collection_id', $collections, @$collection->id, ['class'=>'form-control']) !!}					 
				</div>

				<div class="form-group">
					{!! Form::label('galleries_type',Lang::get('admin.galleries_type')) !!}
					{!! Form::select('galleries_type', array('1'=>'Photo',2=>'Video','3'=>'Embeded'), null, ['class'=>'form-control']) !!}					 
				</div>
				
				<div class="row"> 
	                <div class="form-group ov_hidden col-md-3">
						{!! Form::label('creation_date',Lang::get('admin.create')) !!}        
						
						<div class="date-and-time">
							{!! Form::text('creation_date', null, ['class'=>'form-control datepicker', 'data-format'=>'dd-mm-yyyy']) !!}	
							{!! Form::text('creation_date_time', '10:00 AM', ['class'=>'form-control timepicker', 'data-template'=>'dropdown']) !!}	
				 
	                    </div>
					</div>

					<div class="form-group ov_hidden col-md-3">
						{!! Form::label('expire_date',Lang::get('admin.expire')) !!}

						<div class="date-and-time">
							{!! Form::text('expire_date', null , ['class'=>'form-control datepicker', 'data-format'=>'dd-mm-yyyy']) !!}	
							{!! Form::text('expire_date_time', '08:00 PM', ['class'=>'form-control timepicker', 'data-template'=>'dropdown']) !!}	
				 
	                    </div>					 
					</div>
                </div>

				<div class="form-group">
					{!! Form::label('visibility',Lang::get('admin.visibility')) !!}
					{!! Form::checkbox('visibility',1, 1, ['class'=>'cbr']) !!}					 
				</div>

			</div>
			
		    @foreach ($langs as $key=>$val)
			   
				<div class="tab-pane" id="profile-{{$key}}">
				
					<div class="form-group">
						{!! Form::label('title[$key]',Lang::get('admin.title')) !!}
						{!! Form::text('title['.$key.']',  @$gallery->title[$key], ['class'=>'form-control']) !!}					 
					</div>	

					<div class="form-group">
						{!! Form::label('short_title[$key]',Lang::get('admin.short_title')) !!}
						{!! Form::text('short_title['.$key.']', @$gallery->short_title[$key], ['class'=>'form-control']) !!}					 
					</div>

					<div class="form-group collapsed-desc">
						{!! Form::label('description[$key]',Lang::get('admin.description')) !!} 
						
						<i class="fa-plus expand-desc-icon"></i>
						<i class="fa-minus collapse-desc-icon"></i>

					    <div class="hide_description">
					    	{!! Form::textarea('description['.$key.']', @$gallery->description[$key], ['class'=>'textarea hidden']) !!}	
					    </div>	
                                             			 
					</div>

				    	
				    <div class="row">
						<div class="form-group col-md-4 ov_hidden">
							<div class="col-md-10">
								{!! Form::label('file[$key]',Lang::get('admin.file')) !!}
								{!! Form::text('file['.$key.']', @$gallery->file[$key], ['class'=>'form-control','id'=>'file_'.$key]) !!}	
                            </div> 
							<a href="{{config('app.url')}}admin/js/ResponsiveFilemanagerMaster/filemanager/dialog.php?type=2&field_id=file_{{$key}}" class="btn iframe-btn file_up" type="button"><i class="mce-ico mce-i-browse"></i></a>	
						</div>		
						<div class="form-group col-md-4 ov_hidden">
							<div class="col-md-10">
								{!! Form::label('title_file[$key]',Lang::get('admin.title_mobile')) !!}
								{!! Form::text('title_file['.$key.']',  @$gallery->title_file[$key], ['class'=>'form-control','id'=>'title_file_'.$key]) !!}	
							</div>
							<a href="{{config('app.url')}}admin/js/ResponsiveFilemanagerMaster/filemanager/dialog.php?type=2&field_id=title_file_{{$key}}" class="btn iframe-btn file_up" type="button"><i class="mce-ico mce-i-browse"></i></a>						 
						</div>	 
					</div>	

					<div class="form-group">
						{!! Form::label('link[$key]',Lang::get('admin.embeded')) !!}
						{!! Form::text('link['.$key.']',  @$gallery->link[$key], ['class'=>'form-control']) !!}					 
					</div>
				
				</div>
			@endforeach	
			
		
		</div>
		
		
	</div>				
</div>
<div class="form-group">

	<input type="submit" class="btn btn-success" value="{{Lang::get('admin.save')}}"/>
		
		
	 
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/gallery/get/{{@$collection->id?@$collection->id:@$gallery->collection_id}}" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>
					
			