@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<!--heading gasatania calke failshi-->
			
			 <?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.gallbanners'),'url'=>'collections/get/8'], 
		    					1=> ['title'=>Lang::get('admin_menu.gallist'),'url'=>'collections/get/8'],
		    					2=> ['title'=>$collection->generic_title,'url'=>'gallery/get/'.$collection->id],

			    			];

			  ?>

			@include('admin.header',['title'=>$titles,'url'=>'gallery/create/'.$collection->id]) 

			<div class="panel panel-default">
				<div action="{{config('app.cms_slug')}}/gallery/uploadfiles/{{@$collection->id}}" class="dropzone dz-clickable">
				 	
				 
				 	<div class="dz-default dz-message"><span>Drop files here to upload</span></div>
	              
				 </div>
			</div>
	 
			<div class="col-md-12 col-md-offset-0 gallery-env show">

				<div class="album-images row ui-sortable" id="item_sort" data-url="{{config('app.cms_slug')}}/gallery/reorder">
					 
					   @foreach ($galleries as $gallery)	
							<div class="ui-sortable-handle col-lg-2 col-md-3 col-sm-4 col-xs-6" data-action="parent" id="{{$gallery->id}}">
								<div class="album-image">
									<a href="#" class="thumb" data-action="edit">
										<?php if(!$gallery->title_file) { $gallery->title_file = config('app.noimage'); } ?>
										<img src="{{$gallery->galleries_type==1?$gallery->file:$gallery->title_file}}" class="img-responsive">
									</a>
									
									<a href="#" class="name">
										<span>{{$gallery->generic_title}}</span>
										<em> </em>
									</a>
									
									<div class="image-options">
										<a href="{{config('app.cms_slug')}}/gallery/edit/{{$gallery->collection_id}}/{{$gallery->id}}"  ><i class="fa-pencil"></i></a>
										<a href="{{config('app.cms_slug')}}/gallery/delete/{{$gallery->collection_id}}/{{$gallery->id}}" onclick="return confirm('Are you sure?')">
											<i class="fa-trash"></i>
										</a>
									</div>
									
								    <div class="image-checkbox">
								    	<i class="{{$gallery->galleries_type==1?'fa-camera':'fa-video-camera'}}"></i> 
								    </div>
								</div>
							</div>
				        @endforeach
					
				</div>	
			</div>  <!--gallery-->

		</div>
	</div>
</div>
@endsection
