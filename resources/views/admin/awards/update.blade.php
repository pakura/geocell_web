@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		<?php $titles = [
	    					0=> ['title'=>Lang::get('admin_menu.about'),'url'=>''], 
	    					1=> ['title'=>Lang::get('admin_menu.awards'),'url'=>'awards/get'],
	    					2=> ['title'=>$award->generic_title,'url'=>''],

		    			]; 
		?>
			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
            @include('errors.admin')

				{!!  Form::model($award,['url'=>config('app.cms_slug').'/awards/update/'.$award->id]) !!}
					
					@include('admin.awards.form')
					
				{!! Form::close() !!}
			
		</div>

		<div class="album-images row">
				@if(count(@$award->images)) 
				   @foreach (@$award->images as $image)	
						<div class="col-lg-2 col-md-3 col-sm-4 col-xs-6" data-action="parent">
							<div class="album-image">
								<a href="#" class="thumb" data-action="edit">
									<img src="{{config('folders.awards')}}thumbs/{{$image->file}}" class="img-responsive">
								</a>
								
								<a href="#" class="name">
									<span>{{$image->file}}</span>
									<em> </em>
								</a>
								
								<div class="image-options">
									<a href="#" data-action="edit"  data-url="{{config('app.cms_slug')}}/awards/edit-file/{{$image->id}}"><i class="fa-pencil"></i></a>
									<a href="#" data-action="trash" data-url="{{config('app.cms_slug')}}/awards/delete-file/{{$image->id}}"><i class="fa-trash"></i></a>
								</div>
								
							    <div class="form-group imagetitle">
									{!! Form::label('title',Lang::get('admin.title')) !!}
									{!! Form::text('title', @$image->title, ['class'=>'form-control','data-url'=>config('app.cms_slug').'/awards/edit-file/'.$image->id]) !!}					 
								</div>
								<div class="image-checkbox">
									<input type="checkbox" class="cbr default_foto" name="default_foto" {{@$image->file_type==1?"checked":"" }} data-url="{{config('app.cms_slug').'/awards/update-default-foto/'.$image->id}}" data-relid="{{@$image->award_id}}">
								</div>
							</div>
						</div>
			        @endforeach
				@endif
			</div>

	</div>
</div>
@endsection
