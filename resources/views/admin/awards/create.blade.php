@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
			<?php $titles = [
		    					0=> ['title'=>Lang::get('admin_menu.about'),'url'=>''], 
		    					1=> ['title'=>Lang::get('admin_menu.awards'),'url'=>'awards'],
		    					2=> ['title'=>Lang::get('admin.addnews'),'url'=>''],

			    			];
		     ?>

			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
             @include('errors.admin')

				{!!  Form::open(['url'=>config('app.cms_slug').'/awards/save/']) !!}

					@include('admin.awards.form')
					
				{!! Form::close() !!}
			
		</div>
	</div>
</div>
@endsection
