@extends('admin')

@section('content')
    <div class="container_">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h1>{{Lang::get('admin_menu.pre-order')}}</h1>

                        <br>
                        <hr>
                        <br>
                        <div id="tabledata">





                            <table style="width: 100%; color: #666" id="tab_0">
                                <thead>
                                <tr>
                                    <th>Phone</th>
                                    <th>Storage</th>
                                    <th>Color</th>
                                    <th>Quantities</th>
                                    <th>Option</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>Phone</th>
                                    <th>Storage</th>
                                    <th>Color</th>
                                    <th>Quantities</th>
                                    <th>Option</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($stocks as $key => $val)
                                    <tr>
                                        <td>
                                            @if($val->product_id == 290)
                                                iPhone 7
                                            @else
                                                iPhone 7 Plus
                                            @endif
                                        </td>
                                        <td>{{$val->storage}}</td>
                                        <td>{{$val->colorname}}</td>
                                        <td><input type="number" value="{{$val->quantities}}" id="{{$val->id}}" class="quantity"></td>
                                        <td><button class="btn btn-success" onclick="saveQuant({{$val->id}})">Update</button></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        function saveQuant(id){
            var quant = parseInt($('#'+id).val());
            $.ajax({
                method: "GET",
                url: "/developer_version/public/cms/iphone-stock/update",
                data: { id: id, quant: quant }
            })
                    .done(function( msg ) {
                        alert( msg );
                    });
        }

        $(document).ready(function() {
            var table = $('#tab_0').DataTable({
                "iDisplayLength": 60,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel',
                    {
                        extend: 'pdfHtml5',
                        orientation: 'landscape',
                        pageSize: 'LEGAL'
                    },
                    'print'
                ]
            });
            $('#tab_0_filter').fadeOut();
            var cnt = 0;
            $('#tab_0 tfoot th').each( function () {
                if(cnt == 1 || cnt == 2 || cnt == 9 ||  cnt == 6 || cnt == 10){
                    var title = $(this).text();
                    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
                }
                cnt++;
            } );
            var searchcnt = 0;
            table.columns().every( function () {
                if(searchcnt == 1 || searchcnt == 2 || searchcnt == 9 || searchcnt == 6 ||  searchcnt == 10 ) {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                }
                searchcnt++;
            } );

            var selectcnt = 0;
            table.columns().every( function () {
                if(selectcnt == 3 || selectcnt == 4 || selectcnt == 5 || selectcnt == 7 || selectcnt == 8){
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                            .appendTo( $(column.footer()).empty() )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                        $(this).val()
                                );

                                column
                                        .search( val ? '^'+val+'$' : '', true, false )
                                        .draw();
                            } );

                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' );
                    } );
                }
                selectcnt++;
            } );

//            $('input.column_filter').on( 'keyup click', function () {
//                filterColumn( $(this).parents('tr').attr('data-column') );
//            } );
        } );



    </script>
    <style>
        input{
            width: 60px;
        }
    </style>
@endsection
