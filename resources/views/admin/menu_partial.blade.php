@foreach ($subItems as $item) 
	@if(Auth::user()->hasPermission($item->id) || Auth::user()->hasPermissionToCollection(6) )
	<li data-id="{{$item->id}}">
		<a href="{{config('app.cms_slug')}}/{{$type}}/get/{{$item->id}}">
			<span class="title">{{$item->generic_title}}</span>
		</a>
	</li>
	@endif
@endforeach
 
					