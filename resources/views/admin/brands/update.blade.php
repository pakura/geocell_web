@extends('admin')

@section('content')
<div class="container_">
	<div class="row">
		<div class="col-md-12 col-md-offset-0">
		<?php $titles = [
	    					0=> ['title'=>Lang::get('admin_menu.stock'),'url'=>''], 
	    					1=> ['title'=>Lang::get('admin_menu.brands'),'url'=>'brands'],
	    					2=> ['title'=>$brand->generic_title,'url'=>''],

		    			]; 
		?>
			@include('admin.header',['title'=>$titles,'url'=>'']) 	

		   
            @include('errors.admin')

				{!!  Form::model($brand,['url'=>config('app.cms_slug').'/brands/update/'.$brand->id]) !!}
					
					@include('admin.brands.form')
					
				{!! Form::close() !!}
			
		</div>


	</div>
</div>
@endsection
