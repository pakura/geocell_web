 <div class="row">
	<div class="col-md-12">

  		<?php 
  			$langs = config('app.langs'); 
  			$disabled = (@$brand)?'':'disabled';
  			$toggle = (@$brand)?'tab':'';
  		?>

		 	
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#general" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>
			
			@foreach ($langs as $key=>$val)
				<li class="{{$disabled}}">
					<a href="#profile-{{$key}}" data-toggle="{{$toggle}}">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">{{$val}}</span>
					</a>
				</li>
			@endforeach	 
			 
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="general">
				
				<div class="form-group">
					{!! Form::label('generic_title',Lang::get('admin.generic')) !!}
					{!! Form::text('generic_title', null, ['class'=>'form-control']) !!}					 
				</div>	
				
				<div class="form-group">
					{!! Form::label('slug',Lang::get('admin.slug')) !!}
					{!! Form::text('slug', null, ['class'=>'form-control']) !!}					 
				</div>

			 
				<div class="row">
					<div class="form-group col-md-4 ov_hidden">
						{!! Form::label('logo',Lang::get('admin.logo')) !!}
					    {!! Form::text('logo',null, ['class'=>'form-control','id'=>'file_logo']) !!}	
						 
					</div>	
						
						<a href="/admin/js/ResponsiveFilemanagerMaster/filemanager/dialog.php?type=2&field_id=file_logo" class="btn iframe-btn file_up" type="button"><i class="mce-ico mce-i-browse"></i></a>					 
					
				</div>

				<div class="form-group">
					{!! Form::label('visibility',Lang::get('admin.visibility')) !!}
					{!! Form::checkbox('visibility',1, 1, ['class'=>'cbr']) !!}					 
				</div>

			</div>
			
		    @foreach ($langs as $key=>$val)
			   
				<div class="tab-pane" id="profile-{{$key}}">
				
					<div class="form-group">
						{!! Form::label("{$key}[title]",Lang::get("admin.title")) !!}
						{!! Form::text("{$key}[title]",  @$brand["data"][$key]["title"], ["class"=>"form-control"]) !!}					 
					</div>	

					<div class="form-group collapsed-desc">
						{!! Form::label("{$key}[description]",Lang::get("admin.description")) !!} 
						
						<i class="fa-plus expand-desc-icon"></i>
						<i class="fa-minus collapse-desc-icon"></i>

					    <div class="hide_description">
					    	{!! Form::textarea("{$key}[description]",  @$brand["data"][$key]["description"], ["class"=>"textarea hidden"]) !!}	
					    </div>					 
					</div>

					<div class="row">
						<div class="form-group col-md-4 ov_hidden">
							{!! Form::label("{$key}[language_logo]",Lang::get('admin.logo')) !!}
							{!! Form::text("{$key}[language_logo]", @$brand["data"][$key]["language_logo"],['class'=>'form-control','id'=>'file_'.$key]) !!}	
						</div>	
							
							<a href="/admin/js/ResponsiveFilemanagerMaster/filemanager/dialog.php?type=2&field_id=file_{{$key}}" class="btn iframe-btn file_up" type="button"><i class="mce-ico mce-i-browse"></i></a>					 
						
					</div>
						
					<div class="form-group">
						{!! Form::label("{$key}[brand_url]",Lang::get('admin.brand_url')) !!}
						{!! Form::text("{$key}[brand_url]", @$brand["data"][$key]["brand_url"], ['class'=>'form-control']) !!}						 
					</div>
						
				</div>
			@endforeach	
			
			 
		</div>
		
		
	</div>				
</div>
<div class="form-group">

	<input type="submit" class="btn btn-success" value="{{Lang::get('admin.save')}}"/>
		
		
	 
    <input type="submit" name="saveclose" class="btn btn-warning" value="{{Lang::get('admin.saveandclose')}}"/>

	<a href="{{config('app.cms_slug')}}/brands" type="submit" class="btn btn-danger">
		{{Lang::get('admin.cancel')}}
	</a>

</div>
					
			