<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	
	<title>Geocell Portal - Login</title>

	<base href="{{config('app.url')}}" /> 

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="admin/css/fonts/linecons/css/linecons.css">
	<link rel="stylesheet" href="admin/css/fonts/fontawesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="admin/css/bootstrap.css">
	<link rel="stylesheet" href="admin/css/xenon-core.css">
	<link rel="stylesheet" href="admin/css/xenon-forms.css">
	<link rel="stylesheet" href="admin/css/xenon-components.css">
	<link rel="stylesheet" href="admin/css/xenon-skins.css">
	<link rel="stylesheet" href="admin/css/custom.css">


	<script src="admin/js/jquery-1.11.1.min.js"></script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	
</head>
<body class="page-body login-page login-light">

	
	<div class="login-container">
	
		<div class="row">
		
			<div class="col-sm-6">
			
				<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						// Reveal Login form
						setTimeout(function(){ $(".fade-in-effect").addClass('in'); }, 1);
						
						
						// Validation and Ajax action
						$("form#login").validate({
							rules: {
								username: {
									required: true
								},
								
								passwd: {
									required: true
								}
							},
							
							messages: {
								username: {
									required: 'Please enter your username.'
								},
								
								passwd: {
									required: 'Please enter your password.'
								}
							},
							
							// Form Processing via AJAX
						});
						
						// Set Form focus
						$("form#login .form-group:has(.form-control):first .form-control").focus();
					});
				</script>
				
				<!-- Errors container -->
				<div class="errors-container">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
									
				</div>
				
				<!-- Add class "fade-in-effect" for login form effect -->
				<form action="auth/login" method="post" role="form" id="login" class="login-form fade-in-effect">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					
					<div class="login-header">
						<a href="auth/login" class="logo">
							<img src="admin/images/logo@2x.png" alt="" width="80" />
							<span>log in</span>
						</a>
						
						<p>Dear user, log in to access the admin area!</p>
					</div>
	
					
					<div class="form-group">
						<label class="control-label" for="username">Username</label>
						<input type="text" class="form-control " name="email" value="{{ old('email') }}" id="username" autocomplete="off" />
					</div>
					
					<div class="form-group">
						<label class="control-label" for="passwd">Password</label>
						<input type="password" class="form-control" name="password" id="passwd" autocomplete="off" />
					</div>
					
					<div class="form-group">
						
							<div class="checkbox">
								<label>
									<input type="checkbox" name="remember"> Remember Me
								</label>
							</div>
						
					</div>
					
					<div class="form-group">
						<button type="submit" class="btn  btn-block text-left">
							<i class="fa-lock"></i>
							Log In
						</button>
					</div>

					
					<div class="login-footer">
						<a href="/password/email">Forgot your password?</a>
						
						<!-- <div class="info-links">
							<a href="#">ToS</a> -
							<a href="#">Privacy Policy</a>
						</div> -->
						
					</div>
					
				</form>
				
				
				
			</div>
			
		</div>
		
	</div>



	<!-- Bottom Scripts -->
	<script src="admin/js/bootstrap.min.js"></script>
	<script src="admin/js/TweenMax.min.js"></script>
	<script src="admin/js/resizeable.js"></script>
	<script src="admin/js/joinable.js"></script>
	<script src="admin/js/xenon-api.js"></script>
	<script src="admin/js/xenon-toggles.js"></script>
	<script src="admin/js/jquery-validate/jquery.validate.min.js"></script>
	<script src="admin/js/toastr/toastr.min.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="admin/js/xenon-custom.js"></script>

</body>
</html>