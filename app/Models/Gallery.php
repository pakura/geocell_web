<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\DateTrait;

class Gallery extends Model{

	use DateTrait;

	protected $table='galleries';
	
	protected $fillable = ['generic_title','collection_id','galleries_type','visibility','slug','creation_date','expire_date','creator_id','editor_id'];

	
	public static function reOrderItems($ids){
		for($i=0;$i<count($ids);$i++) {

			$item = self::find($ids[$i]);
	        $item->position = $i;
	        $item->save();
		}
	}

	public static function getItem($col_id){

		return self::where('collection_id', '=', $col_id)
					->where('c.language','=',config('app.locale'))
					->join('galleries_content as c','c.galleries_id','=','galleries.id')
					->select('galleries.*','c.file','c.title_file')
					->groupBy('galleries_id')
					->orderBy('galleries.position')
					->get();
	}
  	 
}
