<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\DateTrait;
use App\Models\Traits\ModelContentTrait;


class Ussd extends Model{

	use DateTrait;
	use ModelContentTrait;
	
	protected $table='ussd_codes';
	 
	
	protected $fillable = ['code','description_ge','description_en','brand','position','brand_ge','brand_en'];
 

	
	 
  	 
}
