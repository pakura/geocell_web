<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentTranslations extends Model{

	protected $table='';
	
	protected $fillable = [];
    
    //public $timestamps = false;
	
	function saveContents($id,$table,$rid,$fields){
		$this->setTable($table);
		$this->fillable[] = $fields;
		$insert = array();
        
		foreach(config('app.langs') as $lang => $value)
		{
		    $insert[] = array(
		         $rid => $id,
		        'language' => $lang
		    );
		}

        $this->create($insert); 
	} 
  	 
 


}
