<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\DateTrait;
use App\Models\Traits\ModelContentTrait;
use App\Models\Traits\AttachedFilesTrait;

class Products extends Model{

    use DateTrait;
    use ModelContentTrait;
    use AttachedFilesTrait;
    
	protected $table='products';
    protected $contentTable='products_content';
    protected $attachTable='products_attachments';
    protected $foreign_key = 'product_id';
	
	protected $fillable = ['generic_title','collection_id','template','sku','sku2','brand_id','visibility','slug','creation_date','expire_date','creator_id','editor_id','pinned','home_page'];

    protected $translation = ['title','short_title','description','description2','content','meta_content','meta_description','terms_and_conditions','services_and_features','service_activation_msg','service_activation_msg2'];

/*  protected $dates = ['creation_date', 'expire_date'];*/
	
 

    public static function addInStock($fields){
    	\DB::table('products_in_stock')->insert($fields);
    }
	
	public static function getProductInStocksList($product_id){
    	return \DB::table('products_in_stock as p')
    										->where('product_id','=',$product_id)
    								   		->join('stocks as s','p.stock_id','=','s.id')
    								   		->select('p.*','s.generic_title')
    								   		->get();
    }
    
    /*get key=> val list*/
    public static function getProductStockIds($stocks){
    	$ids= [];
    	for($i=0;$i<count($stocks);$i++){
    		$ids[$stocks[$i]->stock_id] = $stocks[$i]->generic_title;
    	}
    	 
    	return $ids;

    }
    
    public static function addPrice($fields){
    	return \DB::table('products_prices')->insertGetId($fields);
    }

    public static function getProductPrices($product_id){
    	return \DB::table('products_prices as p')
								 ->where('product_id','=',$product_id)
								 ->leftjoin('stocks as s','p.stock_id','=','s.id')
								 ->select('p.*','s.id as sid','s.generic_title as stitle')
								 ->get(); 

    }

    public static function getProductPrice($id){
        return \DB::table('products_prices as p')
                                 ->where('p.id','=',$id)
                                 ->leftjoin('stocks as s','p.stock_id','=','s.id')
                                 ->select('p.*','s.id as sid','s.generic_title as stitle')
                                 ->get(); 

    }

    /*public static function getProductCollectionOptions($col_id){
        return \DB::table('products_option_list_collections as p')
                                 ->where('p.collection_id','=',$col_id)
                                 ->join('products_option_list as pol','pol.id','=','p.pol_id')
                                 ->select('pol.*')
                                 ->get(); 

    }*/

    public static function getProductOptions($product_id){
        return \DB::table('products_options as po')
                    ->where('product_id','=',$product_id)
                    ->join('products_option_list as pol','pol.id','=','po.products_option_list_id')
                    ->select('po.*','pol.generic_title')
                    ->get(); 

    }

    public function getProductOptionListCollections($collection_id){
        return \DB::table('products_option_list_collections')
                  ->where('collection_id','=',$collection_id)->get();
    }

    public function addProductOptions($id,$collection_id){
        $options = $this->getProductOptionListCollections($collection_id);
        
        $insert=[];
        foreach ($options as $option) {
            $insert[] = [
                'product_id' => $id,
                'collection_id' => $collection_id,
                'products_option_list_id' => $option->pol_id
            ];
        }

         \DB::table('products_options')->insert($insert); 
    }
    
    public function saveProductOptionVal($id,$val,$sku){
        \DB::table('products_options')->where('id','=',$id)->update(['value'=>$val,'sku_code'=>$sku]);
    }

 /*   public function setPinnedAttribute($value)
    {
        if(isset($value)) {
            $this->attributes['pinned'] = 1;
        } 
        else { $this->attributes['pinned'] = 0; }
 
    }

    public function setHomePageAttribute($value)
    {
        if(isset($value)) {
            $this->attributes['home_page'] = 1;
        } 
 
    } 
 
 */
  	 
}
