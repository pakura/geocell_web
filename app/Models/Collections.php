<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collections extends Model{
    public static $type=0;  
	
	protected $fillable = ['generic_title','collection_type'];

	public static function add($title)
    {

		$id= self::create(array(
        	'generic_title'=>$title,
        	'collection_type' =>self::$type
        	));
        return $id;
	}
    
     public static function updateCollection($id)
     {

         self::where('id','=',$id)->update(
                array(
                'generic_title'=>$_POST['generic_title']
                )
            );

    }
    

    /* get by collection type + bind attached pages */

    public static function get($type)
    {
        
        $data = self::where('collection_type','=',$type)->get();
        
        /*$data->pages = \App\Pages::where('attached_collection_id','>',0)->get()->groupby('attached_collection_id');*/
       for($i=0;$i<count($data);$i++){

            $data[$i]->pages =  \DB::table('pages')->where('attached_collection_id','=',$data[$i]->id)->get();

        } 
         
        return $data; 

    }

    /* get groups of collections (for side bar menu to always display collections and not only ) */
    public static  function getAll(){
        
        return self::all()->groupby('collection_type');
       
    }

    /*For articles to choose collection id*/
    public static function getForSelectList($colType){
        
        return self::where('collection_type','=',$colType)->lists('generic_title','id');
    }
   
   

}
