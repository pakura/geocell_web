<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\DateTrait;

class Guestbook extends Model{

	use DateTrait;
	
	protected $table='guestbook';
	
	protected $fillable = ['collection_id','slug','generic_title','visibility','position','creation_date','expire_date'];

	
	 
  	 
}
