<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ModelContentTrait;

class ProductOptions extends Model{

	use ModelContentTrait;

	protected $table='products_option_list';
	protected $contentTable='products_option_list_content';
	protected $contentTableRid='products_option_list_id';
	protected $tablePivot='products_option_list_collections';
	protected $foreign_key = 'products_option_list_id';
	
	protected $fillable = ['generic_title','type','color','option_type'];
	protected $translation = ['title','description','initial'];
    
    public $timestamps = false;


	/*save POL collections*/
	function savePolCollections($id,$fields)
	{
		$saveCollections = [];
 
	    foreach ($fields['collection_id'] as $collection) {
	    	$saveCollections[] = array(
		        'collection_id' => $collection,
		        'pol_id' => $id
		    );


	    }
	    \DB::table($this->tablePivot)->where('pol_id','=',$id)->delete(); 
	    \DB::table($this->tablePivot)->insert($saveCollections); 
	}

	/*Get product options list collections ids*/
	function getPolCollections($pol_id)
	{
		return \DB::table($this->tablePivot)->where('pol_id','=',$pol_id)->lists('collection_id'); 
	}
     
    function addToProductsOptions($id,$collections){
    	$products = \DB::table('products')->whereIn('collection_id',$collections)->lists('collection_id','id');
    	 
    	$insert=[];
        foreach ($products as $product_id=>$collection_id) {
            $insert[] = [
                'product_id' => $product_id,
                'collection_id' => $collection_id,
                'products_option_list_id' => $id
            ];
        }

         \DB::table('products_options')->insert($insert); 
    }

     
	

}
