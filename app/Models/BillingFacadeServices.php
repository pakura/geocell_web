<?php namespace App\Models;


class BillingFacadeServices {
	protected $log_table = 'service_request_log';
    protected $test_phones = array('995577496432', '995577493082', '995577498641', '995577498638');
	/*prodaction urls*/
//	protected $wsdlurl = "https://91.151.128.70:8442/BillingFacadeVendor/GeocellPortalWS/GeocellPortalWS?wsdl";
//	protected $reservationwsdlurl = "https://91.151.128.70:8442/BillingFacadeVendor/GeocellPortalTransporterWS/GeocellPortalTransporterWS?wsdl";


	/*test development urls*/
	protected $wsdlurl = "http://91.151.128.64:8087/BillingFacadeVendor/GeocellPortalWS/GeocellPortalWS?wsdl";
	protected $reservationwsdlurl = "http://91.151.128.64:8087/BillingFacadeVendor/GeocellPortalTransporterWS/GeocellPortalTransporterWS?wsdl";
	protected $testInstalmentUrl = "http://10.10.23.198:8080/PhoneInstallmentGui2?wsdl";
	protected $directDebit = "http://91.151.128.64:8087/BillingFacadeVendor/GeocellPortalDirectDebitWS/GeocellPortalDirectDebitWS?wsdl";


/*
	current server ip 91.151.128.144

	Available Functions

	http://91.151.128.64:8087/BillingFacadeVendor/GeocellPortalWS/GeocellPortalWS?wsdl

	GSMInfo portalGetGSMInfo(String ident, String signature, String uniqId, String gsm)
	$signature=md5($ident.",".$uniq_id.",".$gsm.",".$agreedKey);

	CreditInfo portalGetGeoCreditInfo(String ident, String signature, String uniqId, String gsm)

	ResultObject portalGetGeoCredit(String ident, String signature, String uniqId, String gsm)

	ServiceItemList portalGetActiveServices(String ident, String signature, String uniqId, String gsm)

	ServiceItemList portalGetAvailableServices(String ident, String signature, String uniqId, String gsm)

	ResultItem portalTerminateService(String ident, String signature, String uniqId, String gsm, String key)

	ResultItem portalActivateService(String ident, String signature, String uniqId, String gsmA, String gsmB, String key)

	SubscriberData portalGetSubscriberData(String ident, String signature, String uniqId, String gsm)

	ResultList portalGetFriendList(String ident, String signature, String uniqId, String gsm)

	ResultObject portalFriendSlotAction(String ident, String signature, String uniqId, String gsm, String friendGSM, int action)

	http://10.10.23.195:8080/BillingFacadeVendor/GeocellPortalTransporterWS/GeocellPortalTransporterWS?wsdl

	RoamingPartnerList getRoamingPartners(String ident, String signature, String uniqId)

	PhoneInstallmentStatus portalGetPhoneInstallmentStatus(String ident, String signature, String uniqId, String gsm)

	ReservationList portalGetReservationStatus(String ident, String signature, String uniqId, List<String> gsms)

	CallDetailList portalCallDetailByGSM(String ident, String signature, String uniqId, String gsm, String fromDate, String toDate)
    CallDetailList portalCallDetailByGSMInInvoce(String ident, String signature, String uniqId, String gsm, String fromDate, String toDate)
    PhoneObject portalCheck4gCompatibility(String ident, String signature, String uniqId, String gsm)

*/

	protected $ident = "mobility";
	protected $agreedKey_old = "testprtl";
//	protected $ident = "mobility_portal";
//	protected $agreedKey_test = "fJ6@t4rG";
	protected $agreedKey = "testprtl";
	protected $uniq_id;
	protected $signature="";
	protected $client;
	protected $timestamp;
    protected $phone;
	protected $methods = array("getRoamingPartners", "portalGetPhoneInstallmentStatus", "portalGetReservationStatus","portalCallDetailByGSM","portalCallDetailByGSMInInvoce","portalCheck4gCompatibility", "portalGetESignedDocumentList", "portalGetESignedDocument");
	protected $security = array("portalRequestSecretCodeIndexes", "portalValidateSecretCode", "portalInvoceInformation", "portalInvoceInformationByGSM");
	protected $direcDeb = array("registerAccount", "updateAccount", "deactivateAccount", "requestActivationUrl", "getAccountsByGSM", "getTransactionsByAccountId");
	public function __construct()
	{
		$this->timestamp = microtime();
        $this->phone = \Session::get('phone');
	}

	public function __call($method,$arguments) {
//dd($arguments);
        $this->phone = isset($arguments[0])?$arguments[0]:null;
		$wsdl = "";
		//dd(file_get_contents($this->reservationwsdlurl));
		if(in_array($method, $this->methods))
            $wsdl = $this->reservationwsdlurl;
		elseif(in_array($method, $this->security))
            $wsdl = $this->reservationwsdlurl;
		elseif(in_array($method, $this->direcDeb))
			$wsdl = $this->directDebit;
		else
            $wsdl = $this->wsdlurl;

		if(method_exists($this, $method)) {
			if($this->initializeSoapClient($wsdl)==0) {
//				dd($arguments);
				return call_user_func_array(array($this,$method),$arguments);
			} else {
				return "Can't connect to WSDL";
			}
		}
	}

	private function saveLog($phone,$req,$res,$service_name=''){
		$result = json_decode($res);
        if(!isset($phone)){
            $phone = \Session::get('phone');
        }

		$result = isset($result->return->resultCode)?$result->return->resultCode:99999;
		\DB::table($this->log_table)->insert([
				'phone'=>$phone,
				'service_name'=>$service_name,
				'request'=>$req,
				'response'=>$res,
				'result_code'=>$result,
				'client_ip'=>$_SERVER['REMOTE_ADDR'],
				'created_at'=>date('Y-m-d H:i:s')
		]);

	}

	protected function initializeSoapClient($wsdl) {
		try {
            error_log($wsdl, 0);
			$this->client = new \SoapClient($wsdl, array( "trace" => 1,'cache_wsdl' => WSDL_CACHE_NONE, "exceptions" => 1));
			return 0;
		} catch (SoapFault $e) {
			echo 'error '.$e->faultstring;
			return -1;
		}
	}

	protected function portalGetGSMInfo($gsm) {
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);
		$params =
				array(
						"portalGetGSMInfo" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm
						)
				);

		$result = $this->client->__soapCall("portalGetGSMInfo", $params, $input_headers=null, $output_headers=null);
//		dd($params);
		return $result;
	}

	protected function portalGetGeoCreditInfo($gsm) {
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);
		$params =
				array(
						"portalGetGeoCreditInfo" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm
						)
				);
		$result = $this->client->__soapCall("portalGetGeoCreditInfo", $params, $input_headers=null, $output_headers=null);
		return $result;
	}

	protected function portalGetGeoCredit($gsm) {
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);
		$params =
				array(
						"portalGetGeoCredit" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm
						)
				);
		$result = $this->client->__soapCall("portalGetGeoCredit", $params, $input_headers=null, $output_headers=null);
		return $result;
	}

	protected function portalGetActiveServices($gsm) {
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);
		$params =
				array(
						"portalGetActiveServices" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm
						)
				);


		$result = $this->client->__soapCall("portalGetActiveServices", $params, $input_headers=null, $output_headers=null);
//die(json_encode($params).'<br><br>'.json_encode($result));
		return $result;
	}

	protected function portalGetAvailableServices($gsm) {
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);
		$params =
				array(
						"portalGetAvailableServices" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm
						)
				);
		$result = $this->client->__soapCall("portalGetAvailableServices", $params, $input_headers=null, $output_headers=null);
//		dd($result);
		return $result;
	}

	protected function portalTerminateService($gsm, $key) {

		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);
		$params =
				array(
						"portalTerminateService" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm,
								"arg4"		=> $key,
						)
				);

//		dd($params);
		$result = $this->client->__soapCall("portalTerminateService", $params, $input_headers=null, $output_headers=null);

		$this->saveLog($gsm,json_encode($params),json_encode($result),'portalTerminateService');
		return $result;
	}

	public function portalActivateService($gsm, $gsm_b, $key) {
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);

		$params =
				array(
						"portalActivateService" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm,
								"arg4"		=> ($gsm_b=='')?null:$gsm_b,
								"arg5"		=> $key,
						)
				);
		//dd($params);

		$result = $this->client->__soapCall("portalActivateService", $params, $input_headers=null, $output_headers=null);

		$this->saveLog($gsm,json_encode($params),json_encode($result),'portalActivateService');
		//dd($params);
		return $result;
	}

	public function portalFriendSlotAction($gsm, $gsm_b, $action) {

		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);

		$params =
				array(
						"portalFriendSlotAction" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm,
								"arg4"		=> ($gsm_b=='')?null:$gsm_b,
								"arg5"		=> $action,
						)
				);
		//dd($params);



		$result = $this->client->__soapCall("portalFriendSlotAction", $params, $input_headers=null, $output_headers=null);

		$this->saveLog($gsm,json_encode($params),json_encode($result),'portalFriendSlotAction');
		//dd($params);
		return $result;
	}

	public function portalGetFriendList($gsm) {

		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);

		$params =
				array(
						"portalGetFriendList" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm,

						)
				);
		//dd($params);

		$result = $this->client->__soapCall("portalGetFriendList", $params, $input_headers=null, $output_headers=null);
		//dd($params);
		return $result;
	}

	public function portalGetSubscriberData($gsm) {
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);
		$params =
				array(
						"portalGetSubscriberData" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm
						)
				);
        $result = $this->client->__soapCall("portalGetSubscriberData", $params, $input_headers=null, $output_headers=null);

//        echo "REQUEST:\n" . htmlentities($this->client->__getLastRequest()) . "\n";
//        die;
		$this->saveLog($gsm,json_encode($params),json_encode($result),'portalGetSubscriberData');

		return $result;
	}


	protected function portalCallDetailByGSM($gsm,$fromDate="",$toDate="") {
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);
		$params =
				array(
						"portalCallDetailByGSM" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm,
								"arg4"		=> $fromDate,
								"arg5"		=> $toDate,
						)
				);
		$result = $this->client->__soapCall("portalCallDetailByGSM", $params, $input_headers=null, $output_headers=null);
		$this->saveLog($gsm,json_encode($params),json_encode($result),'portalCallDetailByGSM');
		return $result;
	}

	protected function portalCallDetailByGSMInInvoce($gsm,$fromDate="",$toDate="") {
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);
		$params =
				array(
						"portalCallDetailByGSMInInvoce" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm,
								"arg4"		=> $fromDate,
								"arg5"		=> $toDate,
						)
				);
		$result = $this->client->__soapCall("portalCallDetailByGSMInInvoce", $params, $input_headers=null, $output_headers=null);

		$this->saveLog($gsm,json_encode($params),json_encode($result),'portalRequestSecretCodeIndexes');

		return $result;
	}


    protected function portalInvoceInformation($gsm,$userID,$fromDate="") {
        $this->uniq_id = microtime();
        $this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);
        $params =
            array(
                "portalInvoceInformation" => array(
                    "arg0"		=> $this->ident,
                    "arg1"		=> $this->signature,
                    "arg2"		=> $this->uniq_id,
                    "arg3"		=> $userID,
                    "arg4"		=> $fromDate
                )
            );
        $result = $this->client->__soapCall("portalInvoceInformation", $params, $input_headers=null, $output_headers=null);

        $this->saveLog($gsm,json_encode($params),json_encode($result),'portalInvoceInformation');

        return $result;
    }

    protected function portalInvoceInformationByGSM($gsm,$userID,$fromDate="") {
        $this->uniq_id = microtime();
        $this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);
        $params =
            array(
                "portalInvoceInformationByGSM" => array(
                    "arg0"		=> $this->ident,
                    "arg1"		=> $this->signature,
                    "arg2"		=> $this->uniq_id,
                    "arg3"		=> $gsm,
                    "arg4"		=> $fromDate
                )
            );
        $result = $this->client->__soapCall("portalInvoceInformationByGSM", $params, $input_headers=null, $output_headers=null);
        $this->saveLog($gsm,json_encode($params),json_encode($result),'portalInvoceInformationByGSM');

        return $result;
    }


	protected function getRoamingPartners() {

		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);
		$params =
				array(
						"getRoamingPartners" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id
						)
				);
		$result = $this->client->__soapCall("portalGetRoamingPartners", $params, $input_headers=null, $output_headers=null);
		return $result;
	}

	protected function portalGetPhoneInstallmentStatus($gsm) {
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);
		$params =
				array(
						"portalGetPhoneInstallmentStatus" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm
						)
				);
		$result = $this->client->__soapCall("portalGetPhoneInstallmentStatus", $params, $input_headers=null, $output_headers=null);
		return $result;
	}

	protected function portalGetReservationStatus($gsms = array()) {
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);
		$params =
				array(
						"portalGetReservationStatus" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsms
						)
				);
		$result = $this->client->__soapCall("portalGetReservationStatus", $params, $input_headers=null, $output_headers=null);
		return $result;
	}


	protected function portalCheck4gCompatibility($gsm) {
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);

		$params =
				array(
						"portalCheck4gCompatibility" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm
						)
				);
		$result = $this->client->__soapCall("portalCheck4gCompatibility", $params, $input_headers=null, $output_headers=null);

		//$this->saveLog($gsm,json_encode($params),json_encode($result));
		return $result;
	}


	public  function check4GInfo($gsm){

		//$check4G = \Session::has('check4G')?\Session::get('check4G'):$get4GInfo;

		if(\Session::has('check4G')) {
			return  \Session::get('check4G');
		}
		// dd(\Session::get('phone'));
		$get4GInfo = $this->__call('portalCheck4gCompatibility',[$gsm]);

		if(isset($get4GInfo->return->resultCode)) {
			\Session::set('check4G',$get4GInfo);

			return $get4GInfo;
		}

		return false;

	}

	public  function _getUserInfo($gsm){

		// dd(\Session::get('phone'));
		return $this->__call('portalGetGSMInfo',[$gsm]);

	}

	public function portalRequestSecretCodeIndexes($gsm){
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);
		$params =
				array(
						"portalRequestSecretCodeIndexes" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm
						)
				);

		$result = $this->client->__soapCall("portalRequestSecretCodeIndexes", $params, $input_headers=null, $output_headers=null);
		$this->saveLog($gsm,json_encode($params),json_encode($result),'portalRequestSecretCodeIndexes');
		return $result;
	}


	public function portalValidateSecretCode($arg){
		$arg = explode(',', $arg);
		$code = str_split($arg[1]);
		$code[0] = intval($code[0]);
		$code[1] = intval($code[1]);
		$code[2] = intval($code[2]);
		$key = $arg[2];
		$gsm = $arg[0];

		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$gsm.",".$this->agreedKey);
		$params =
				array(
						"portalValidateSecretCode" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm,
								"arg4"		=> $key,
								"arg5"		=> $code,
						)
				);

		$result = $this->client->__soapCall("portalValidateSecretCode", $params, $input_headers=null, $output_headers=null);
		$this->saveLog($gsm,json_encode($params),json_encode($result),'portalValidateSecretCode');
		return $result;
	}


	public function getAccountsByGSM($arg){
		$gsm = $arg;
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);
		$params =
				array(
						"getAccountsByGSM" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm,
								"arg4"		=> null,
								"arg5"		=> null
						)
				);
//		dd($params);
		$result = $this->client->__soapCall("getAccountsByGSM", $params, $input_headers=null, $output_headers=null);
//		dd($result);
		$this->saveLog($gsm,json_encode($params),json_encode($result),'getAccountsByGSM');
		return $result;
	}


	public function registerAccount($arg){
		$phone = \Session::get('phone');
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);
		$params =
				array(
						"registerAccount" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $arg->account,
								"arg4"		=> $arg->subscribers,
								"arg5"		=> null
						)
				);

//		dd($params);
		$result = $this->client->__soapCall("registerAccount", $params, $input_headers=null, $output_headers=null);
//		dd($result);
		$this->saveLog($phone,json_encode($params),json_encode($result),'registerAccount');
		return $result;
	}



	public function updateAccount($arg){
		$phone = \Session::get('phone');
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);
		$params =
				array(
						"updateAccount" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $arg->account,
								"arg4"		=> $arg->subscribers,
								"arg5"		=> null
						)
				);

//		dd($params);
		$result = @$this->client->__soapCall("updateAccount", $params, $input_headers=null, $output_headers=null);
//		dd($result);
		$this->saveLog($phone,json_encode($params),json_encode($result),'updateAccount');
		return $result;
	}



	public function requestActivationUrl($arg){
		$phone = \Session::get('phone');
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);
		$account_id = isset($arg->return->result->account)?$arg->return->result->account->accountId:null;
		if($account_id == null){
			$account_id = isset($arg->return->account->accountId)?$arg->return->account->accountId:null;
		}
		if($account_id == null){
			$account_id = isset($arg->account->accountId)?$arg->account->accountId:null;
		}
		$rand = rand(100,1000);
		$params =
				array(
						"requestActivationUrl" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> intval($account_id),
								"arg4"		=> 'https://geocell.ge/developer_version/public/ge/private/private-cabinet/direct-debit?status=ok&r='.$rand,
								"arg5"		=> 'https://geocell.ge/developer_version/public/ge/private/private-cabinet/direct-debit?status=f&r='.$rand,
								"arg6"		=> intval($account_id),
								"arg7"		=> null
						)
				);
		$result = @$this->client->__soapCall("requestActivationUrl", $params, $input_headers=null, $output_headers=null);

//		dd($result);
		$this->saveLog($phone,json_encode($params),json_encode($result),'requestActivationUrl');
		return $result;
	}


	public function deactivateAccount($arg){
		$phone = \Session::get('phone');
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);

		$params =
				array(
						"updateAccount" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $arg->accountId,
								"arg4"		=> $arg->accountId,
								"arg5"		=> null,
								"arg6"		=> null
						)
				);
//		dd($params);
		$result = $this->client->__soapCall("deactivateAccount", $params, $input_headers=null, $output_headers=null);
//		dd($result);
		$this->saveLog($phone,json_encode($params),json_encode($result),'deactivateAccount');
		return $result;
	}

	public function getTransactionsByAccountId($arg){
		$phone = \Session::get('phone');
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);

		$params =
				array(
						"getTransactionsByAccountId" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $arg,
								"arg4"		=> null,
								"arg5"		=> null,
								"arg6"		=> null
						)
				);
//		dd($params);
		$result = $this->client->__soapCall("getTransactionsByAccountId", $params, $input_headers=null, $output_headers=null);
		$this->saveLog($phone,json_encode($params),json_encode($result),'getTransactionsByAccountId');
		return $result;
	}


	public function portalGetESignedDocumentList($gsm){
		$phone = \Session::get('phone');
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);

		$params =
				array(
						"portalGetESignedDocumentList" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $gsm,
								"arg4"		=> null,
								"arg5"		=> null,
								"arg6"		=> null
						)
				);
		$result = $this->client->__soapCall("portalGetESignedDocumentList", $params, $input_headers=null, $output_headers=null);
		$this->saveLog($phone,json_encode($params),json_encode($result),'portalGetESignedDocumentList');
		return $result;
	}

	public function portalGetESignedDocument($docId){
		$phone = \Session::get('phone');
		$this->uniq_id = microtime();
		$this->signature = md5($this->ident.",".$this->uniq_id.",".$this->agreedKey);

		$params =
				array(
						"portalGetESignedDocument" => array(
								"arg0"		=> $this->ident,
								"arg1"		=> $this->signature,
								"arg2"		=> $this->uniq_id,
								"arg3"		=> $docId,
								"arg4"		=> null,
								"arg5"		=> null,
								"arg6"		=> null
						)
				);
		$result = $this->client->__soapCall("portalGetESignedDocument", $params, $input_headers=null, $output_headers=null);
		$this->saveLog($phone,json_encode($params),json_encode($result),'portalGetESignedDocument');
		return $result;
	}

}
