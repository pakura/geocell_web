<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ModelContentTrait;

class Stocks extends Model{
	use ModelContentTrait;

	protected $table='stocks';
	protected $contentTable='stocks_content';
	protected $foreign_key = 'stock_id';
	
	protected $fillable = ['generic_title','position','status','contact_phone','contact_email'];
	protected $translation = ['title','description','address'];

  	 
}
