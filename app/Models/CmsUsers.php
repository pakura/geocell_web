<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CmsUsers extends Model {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cms_users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['first_name','last_name', 'email', 'phone','password','status','type'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];


	public function roles(){
	    return $this->belongsToMany('App\Models\CmsUserRoles', 'user_roles', 'user_id', 'role_id');
	}


	public function setPasswordAttribute($value){
	    $this->attributes['password'] = bcrypt($value);
	}
}
