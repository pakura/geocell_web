<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ModelContentTrait;

class Banners extends Model{
    
    use ModelContentTrait;

	protected $table='banners';
	protected $contentTable='banners_content';
	protected $foreign_key = 'banner_id';

	protected $fillable = ['generic_title','collection_id','photo','url','open_blank'];
	protected $translation = ['title','description'];

	
	 
  	 
}
