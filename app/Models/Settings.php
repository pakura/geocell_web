<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model{

	protected $table='settings';
	
	protected $fillable = ['language','settings_key','value','title','position','initial','type'];
    
    public $timestamps = false;
	
	public static function reOrderItems($ids){
		for($i=0;$i<count($ids);$i++) {

			$item = self::find($ids[$i]);
	        $item->position = $i;
	        $item->save();
		}
	} 
  	 
}
