<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\DateTrait;
use App\Models\Traits\ModelContentTrait;
use App\Models\Traits\AttachedFilesTrait;

class Pages extends Model{
	
    use DateTrait;
    use ModelContentTrait;
    use AttachedFilesTrait;

    protected $table='pages';
    protected $contentTable='pages_content';
    protected $attachTable='pages_attachments';
    protected $foreign_key = 'page_id';
    
    protected $fillable = ['group_id','parent_id','page_type','attached_collection_id','controller','visibility','generic_title','slug','full_slug','redirect_link','creation_date','expire_date','creator_id','editor_id'];

    protected $translation = ['title','short_title','description','content','meta_content','meta_description'];
	
    /*remove collection from page*/ 
    public static function removeColl($pageid)
    {
    
         self::where('id','=',$pageid)->update(
            array(
            'attached_collection_id'=>0,
            'page_type'=>1,
            )
        );

    }

    /*attach collection to page*/ 
    public static function addColl($pageid,$input)
    {
    
         self::where('id','=',$pageid)->update(
            array(
            'attached_collection_id'=>$input['collid'],
            'page_type'=>$input['type'],
            )
        );

    }  



    /*get group items*/

    public static function getList($group_id){
        return self::where('group_id','=',$group_id)
                    ->leftjoin('collections as c','pages.attached_collection_id','=','c.id')
                    ->select('pages.*','c.id as cid','c.generic_title as ctitle','c.collection_type as coll_type')
                    ->orderBy('position')->get();
    }

    /* reorder pages */

   public static function updatePagesTree($data,$parent=0){
         for($i=0;$i<count($data);$i++) {
            
            $slug = self::getParentSlug($parent);
            self::updateParentId($data[$i]['id'],$parent,$slug,$i);

            if($child = @$data[$i]['children']){
                
                self::updatePagesTree($child,$data[$i]['id'],$slug);
                
            }
         }
    }
    
    public static function getParentSlug($parent){
        $row = self::find($parent);
        if($row) { return $row->full_slug; }
    }

    public static function updateParentId($id,$parent,$slug='',$position=0)
    {
        $page = self::find($id);
        $page->parent_id = $parent;
        $page->full_slug =$slug? $slug.'/'.$page->slug: $page->slug;
        $page->position = $position;
        $page->save();

    }
 
	

}
