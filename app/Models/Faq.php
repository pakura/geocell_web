<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\DateTrait;
use App\Models\Traits\ModelContentTrait;


class Faq extends Model{

	use DateTrait;
	use ModelContentTrait;
	
	protected $table='faq';
	protected $contentTable='faq_content';
	protected $foreign_key = 'faq_id';
	
	protected $fillable = ['collection_id','slug','generic_title','visibility','position','creation_date','expire_date'];
	protected $translation = ['question','answer'];

	
	 
  	 
}
