<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cms_users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['first_name','last_name', 'email', 'phone','password','status','type'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/*check if a user has permission to collection groups or sub items */
	public function hasPermission($role_page)
	{
		if($this->type==2){
			return true;
		}

		$permissions = $this->Permissions;

		if(isset($permissions['-1']) || isset($permissions[$role_page])) {
			return true;
		}
 
        $role_page = -$role_page;
		if( (isset($permissions[$role_page])  && $permissions[$role_page]>1) || in_array(abs($role_page), $permissions) ){
			return true;
		}

		return false;
	}

	/*check if a user has permission to collection group */

	public function hasPermissionToCollection($role_page)
	{
		if($this->type==2){
			return true;
		}

		$permissions = $this->Permissions;

		$role_page = -$role_page;
		if(isset($permissions['-1']) || (isset($permissions[$role_page])  && $permissions[$role_page]>1) ) {
			return true;
		}

		return false;
	}

	public function getPermissionsAttribute(){
		return \DB::table('user_roles as ur')->where('user_id','=',$this->id)
									 		->rightjoin('role_pages as rp','ur.role_id','=','rp.role_id')	
									  		->lists('rp.type','rp.collection_id');
	} 

	public function getIsAdminAttribute(){
		return $this->type==2?true:false;
	}

}
