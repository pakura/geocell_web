<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\DateTrait;
use App\Models\Traits\ModelContentTrait;
use App\Models\Traits\AttachedFilesTrait;

class Awards extends Model{

    use DateTrait;
    use ModelContentTrait;
    use AttachedFilesTrait;

	protected $table='awards';
	protected $contentTable='awards_content';
	protected $attachTable='awards_attachments';
	protected $foreign_key = 'award_id';
	
	protected $fillable = ['generic_title','visibility','slug','creation_date','expire_date','creator_id','editor_id'];
	protected $translation = ['title','short_title','description','content','meta_content','meta_description'];

	
	

  	 
}
