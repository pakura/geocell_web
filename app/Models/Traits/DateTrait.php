<?php namespace App\Models\Traits;

use Carbon\Carbon;

trait DateTrait {

 
    public function getCreationDateAttribute($value){
        return $this->getDateFormated($value);
    }

    public function getCreationDateTimeAttribute(){
      
        return  Carbon::parse($this->attributes['creation_date'])->format('H:m');
    }

    public function getExpireDateAttribute($value){
        return $this->getDateFormated($value);
    }

     public function getExpireDateTimeAttribute(){
         
        return  Carbon::parse($this->attributes['expire_date'])->format('H:m');
    }


    private function getDateFormated($date)
    {
        return Carbon::parse($date)->format('d-m-Y');
    }
   
  	 
}
