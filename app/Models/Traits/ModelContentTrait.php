<?php namespace App\Models\Traits;
 
trait ModelContentTrait {

 
    public static function collection($cid)
    {
        return \DB::table('collections')->where('id','=',$cid)->first();
    } 
    
    public function scopeGetByCollection($query,$colid)
    {
        $query->where('collection_id','=',$colid);
    }

    public function updateContent($id,$data){
        $this->find($id)->update($data);
        
        foreach(config('app.langs') as $lang => $value)
        {
          
            if(isset($data[$lang])){
                \DB::table($this->contentTable)->where($this->foreign_key, '=', $id)->where('language','=',$lang)->update($data[$lang]);
                
            }

        }

    }

    public function createWithontent($data){

        $pg = $this->create($data);
        $id = $pg->id; 

        $insert = array();
        
        foreach(config('app.langs') as $key => $value)
        {
            $insert[] = array(
                $this->foreign_key => $id,
                'language' => $key
            );
        }

        \DB::table($this->contentTable)->insert($insert); 

        return $pg;
    }

    public function getContentAttribute(){

        return \DB::table($this->contentTable)->where($this->foreign_key,'=',$this->id)->get(); 
    }

    public function getDataAttribute(){
        $cont = $this->content;
        $data  = [];

        for($i=0;$i<count($cont);$i++)
        {          
            foreach($this->translation as $field){
                $data[$cont[$i]->language][$field] = $cont[$i]->$field;
            } 
             

        }

        return $data;
    }

    public function getImagesAttribute(){

        return \DB::table($this->attachTable)->where($this->foreign_key,'=',$this->id)->get(); 
    }
   
  	 
}
