<?php namespace App\Models\Traits;
 
trait AttachedFilesTrait {

 
    public function saveAttachedFile($id,$filename){
        \DB::table($this->attachTable)->insert([
                                                $this->foreign_key=> $id,
                                                'file'=>$filename
                                                ]);
     }

    
    public function deleteAttachedFile ($id){
        \DB::table($this->attachTable)->where('id','=',$id)->delete();
    }  

    public function editAttachedFile ($id,$title){
        \DB::table($this->attachTable)->where('id','=',$id)->update(array('title' => $title));
    } 

    public function updateAttachedFile ($id,$input){
        
        $fileType = ($input['checked'])?1 : 0;
        
        \DB::table($this->attachTable)->where($this->foreign_key,'=',$input['relid'])->update(array('file_type' =>2));
        \DB::table($this->attachTable)->where('id','=',$id)->update(array('file_type' => $fileType));
    } 
  	 
}
