<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\DateTrait;
use App\Models\Traits\ModelContentTrait;
use App\Models\Traits\AttachedFilesTrait;

class CustomLists extends Model{

    use DateTrait;
    use ModelContentTrait;
    use AttachedFilesTrait;

	protected $table='custom_lists';
	protected $contentTable='custom_lists_content';
	protected $attachTable='custom_lists_attachments';
	protected $foreign_key = 'custom_lists_id';
	
	protected $fillable = ['generic_title','collection_id','visibility','slug','creation_date','expire_date','creator_id','editor_id'];
	protected $translation = ['title','short_title','description','content','meta_content','meta_description'];

	
	

  	 
}
