<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LanguageData extends Model{

	protected $table='language_data';
	
	protected $fillable = ['language','data_key','title'];
    
    public $timestamps = false;

  	public function scopeLang($query,$lang)
  	{
  		$query->where('language','=',$lang);
  	} 

}
