<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CmsUserRoles extends Model{
    
	protected $table = 'roles';
	protected $fillable = ['role_name'];

	
	public function cmsUsers(){
	    return $this->belongsToMany('App\Models\CmsUsers', 'user_roles', 'user_id', 'role_id');
	}
	
	/* optimize later !!! */
	public function savePermission($role_id,$data){

		$permissions = $this->getRolePermissions($role_id);
		
		/* check if it has access to all collections or page groups */
		if(array_key_exists('-1', $permissions) && isset($permissions['-1'])==$data['type']){
			return 'Access granted';
		} 
		/* check if record already exists*/
		elseif (array_key_exists($data['collection_id'], $permissions) && isset($permissions[$data['collection_id']])==$data['type']){
			return 'Access granted';
		}

		/* check if  collection type permission exists*/

		elseif($data['collection_id']>0){ 
			if($data['type']>1){
				$coltype = $this->getColletionType($data['collection_id']); 
			}
			else {
				$coltype = $this->getPageGroup($data['collection_id']); 
			}	
			
			$col_type = -$coltype;
			 
			if(array_key_exists($col_type, $permissions) && isset($permissions[$col_type])==$data['type']){
				return 'Access granted';
			}
			else {
				$this->saveRolePermission($role_id,$data);
			}
		}
		else{

			$this->saveRolePermission($role_id,$data);
		}

		
	} 
    

	public function getColletionType($id){

		$item =  \DB::table('collections')->where('id','=',$id)->first();
		return $item->collection_type;

	}

	public function getPageGroup($id){

		$item =  \DB::table('pages')->where('id','=',$id)->first();
		return $item->group_id;

	}


	public function saveRolePermission($role_id,$data){
		/*delete sub permissions later */

		\DB::table('role_pages')->insert(
				[
					'role_id'=>$role_id,
					'collection_id'=>$data['collection_id'],
					'type'=>$data['type'],
					'access_type'=>$data['access_type'],
				]
			);
	}

	public function removePermission($role_id,$data){

		\DB::table('role_pages')->where('role_id','=',$role_id)
								->where('collection_id','=',$data['collection_id'])
								->where('type','=',$data['type'])
								->delete();
					
	} 

/*	public function checkPermissionExist($role_id,$data){

		\DB::table('role_pages')->where('role_id','=',$role_id)
								->where('collection_id','=',$data['collection_id'])
								->where('type','=',$data['type'])
								->first();
					
	} */

	public function getRolePermissions($role_id){
		return \DB::table('role_pages')->where('role_id','=',$role_id)								
									   ->lists('type','collection_id');
	}
   

}
