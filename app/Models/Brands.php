<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\ModelContentTrait;

class Brands extends Model{

	use ModelContentTrait;

	protected $table='brands';
	protected $contentTable='brands_content';
	protected $foreign_key = 'brand_id';
	
	protected $fillable = ['generic_title','logo','slug','visibility','position'];
	protected $translation = ['title','description','language_logo','brand_url'];


	
	 
  	 
}
