<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Collections;

class AdminViewServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		view()->composer('admin.menu',function($view)
 		{
 			$groups = \DB::table('pages_groups')->get();
 			$view->with('menuCollections',Collections::getAll())->with('groups',$groups);
 		});

 		view()->composer('admin.articles.form',function($view)
 		{
 			$view->with('collections',Collections::getForSelectList(6));
 		});

 		view()->composer('admin.products.form',function($view)
 		{
 			$view->with('collections',Collections::getForSelectList(4));
 		});
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
