<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
 
/*Cms routes*/


Route::group(
    array('prefix' => config('app.cms_slug')), 
    function() {
		$ip = $_SERVER['REMOTE_ADDR'];
		$ip = substr($ip, 0, 7);

//    	if($_SERVER['REMOTE_ADDR']!='91.151.130.78'  && $ip!='10.0.6.' && $_SERVER['REMOTE_ADDR']!='10.0.6.187'  && $_SERVER['REMOTE_ADDR'] != '10.0.6.83') return redirect('/');

    	Route::pattern('id', '[0-9]+'); 
        Route::pattern('collection', '[0-9]+'); 
        Route::pattern('type', '[0-9]+'); 

        Route::get('/login', 'AdminHomeController@index');
		Route::get('/', 'AdminHomeController@index');

		/*awards*/
 		Route::get('/awards/get', 'AdminAwardsController@index');
		Route::get('/awards/create/', 'AdminAwardsController@create');
		Route::post('/awards/save/', 'AdminAwardsController@store');
		Route::get('/awards/edit/{id}', 'AdminAwardsController@edit');
		Route::post('/awards/update/{id}', 'AdminAwardsController@update');
		Route::get('/awards/delete/{id}', 'AdminAwardsController@destroy');


		/*Collections*/
		Route::get('/collections/get/{type}', 'AdminCollectionsController@getCollections');
		Route::post('/collections/getlist/{type}', 'AdminCollectionsController@getCollectionsList');
		Route::get('/collections/add/{type}', 'AdminCollectionsController@addCollection');
		Route::post('/collections/save/{type}', 'AdminCollectionsController@saveCollection');
		Route::post('/collections/edit/{type}/{id}', 'AdminCollectionsController@editCollection');
		Route::get('/collections/delete/{type}/{id}', 'AdminCollectionsController@deleteCollection');
 		
 		/*Articles*/
 		Route::get('/articles/get/{collection}', 'AdminArticlesController@index');
		Route::get('/articles/create/{collection}', 'AdminArticlesController@create');
		Route::post('/articles/save/{collection}', 'AdminArticlesController@store');
		Route::get('/articles/edit/{collection}/{id}', 'AdminArticlesController@edit');
		Route::post('/articles/update/{collection}/{id}', 'AdminArticlesController@update');
		Route::get('/articles/delete/{collection}/{id}', 'AdminArticlesController@destroy');
		
		Route::post('/articles/uploadfiles/{collection}/{id}', 'AdminArticlesController@uploadFiles');
		Route::post('/articles/delete-file/{collection}/{id}', 'AdminArticlesController@deleteFile');
		Route::post('/articles/edit-file/{collection}/{id}', 'AdminArticlesController@editFile');
		Route::post('/articles/update-default-foto/{collection}/{id}', 'AdminArticlesController@updateDefaultFoto'); 	  

		/*custom_lists*/
 		Route::get('/custom_lists/get/{collection}', 'AdminCustomListsController@index');
		Route::get('/custom_lists/create/{collection}', 'AdminCustomListsController@create');
		Route::post('/custom_lists/save/{collection}', 'AdminCustomListsController@store');
		Route::get('/custom_lists/edit/{collection}/{id}', 'AdminCustomListsController@edit');
		Route::post('/custom_lists/update/{collection}/{id}', 'AdminCustomListsController@update');
		Route::get('/custom_lists/delete/{collection}/{id}', 'AdminCustomListsController@destroy');
		
		Route::post('/custom_lists/uploadfiles/{collection}/{id}', 'AdminCustomListsController@uploadFiles');
		Route::post('/custom_lists/delete-file/{collection}/{id}', 'AdminCustomListsController@deleteFile');
		Route::post('/custom_lists/edit-file/{collection}/{id}', 'AdminCustomListsController@editFile');
		Route::post('/custom_lists/update-default-foto/{collection}/{id}', 'AdminCustomListsController@updateDefaultFoto'); 	  

		/*faq*/
 		Route::get('/faq/get/{collection}', 'AdminFaqController@index');
		Route::get('/faq/create/{collection}', 'AdminFaqController@create');
		Route::post('/faq/save/{collection}', 'AdminFaqController@store');
		Route::get('/faq/edit/{collection}/{id}', 'AdminFaqController@edit');
		Route::post('/faq/update/{collection}/{id}', 'AdminFaqController@update');
		Route::get('/faq/delete/{collection}/{id}', 'AdminFaqController@destroy');

		/*guestbook*/
 		Route::get('/guestbook/get/{collection}', 'AdminGuestbookController@index');
		Route::get('/guestbook/create/{collection}', 'AdminGuestbookController@create');
		Route::post('/guestbook/save/{collection}', 'AdminGuestbookController@store');
		Route::get('/guestbook/edit/{collection}/{id}', 'AdminGuestbookController@edit');
		Route::post('/guestbook/update/{collection}/{id}', 'AdminGuestbookController@update');
		Route::get('/guestbook/delete/{collection}/{id}', 'AdminGuestbookController@destroy');

		/*galleries*/
 		Route::get('/gallery/get/{collection}', 'AdminGalleryController@index');
		Route::get('/gallery/create/{collection}', 'AdminGalleryController@create');
		Route::post('/gallery/save/{collection}', 'AdminGalleryController@store');
		Route::get('/gallery/edit/{collection}/{id}', 'AdminGalleryController@edit');
		Route::post('/gallery/update/{collection}/{id}', 'AdminGalleryController@update');
		Route::get('/gallery/delete/{collection}/{id}', 'AdminGalleryController@destroy');
		Route::post('/gallery/reorder', 'AdminGalleryController@reOrder');
		Route::post('/gallery/uploadfiles/{colid}', 'AdminGalleryController@uploadFiles');

		/*products*/
 		Route::get('/products/get/{collection}', 'AdminProductsController@index');
		Route::get('/products/create/{collection}', 'AdminProductsController@create');
		Route::post('/products/save/{collection}', 'AdminProductsController@store');
		Route::get('/products/edit/{collection}/{id}', 'AdminProductsController@edit');
		Route::post('/products/update/{collection}/{id}', 'AdminProductsController@update');
		Route::get('/products/delete/{collection}/{id}', 'AdminProductsController@destroy');

		Route::post('/products/add-in-stock/', 'AdminProductsController@addProductInStock');
		Route::post('/products/remove-from-stock/{id}', 'AdminProductsController@removeProductFromStock');
		
		Route::post('/products/add-price', 'AdminProductsController@addProductPrice');
		Route::post('/products/remove-price/{id}', 'AdminProductsController@removeProductPrice');
		
		Route::post('/products/save-option/{id}', 'AdminProductsController@saveProductOptionValue');
		
		Route::post('/products/uploadfiles/{id}', 'AdminProductsController@uploadFiles');
		Route::post('/products/delete-file/{id}', 'AdminProductsController@deleteFile');
		Route::post('/products/edit-file/{id}', 'AdminProductsController@editFile');
		Route::post('/products/update-default-foto/{id}', 'AdminProductsController@updateDefaultFoto');

        Route::get('/pre-order/', 'AdminProductsController@preorderList');
        Route::get('/iphone-stock/update/', 'AdminProductsController@updateStock');
        Route::get('/iphone-stock/', 'AdminProductsController@stockList');
        Route::any('/preoder/action/', 'AdminProductsController@changeAction');

        /*product options*/
		Route::get('/product-options/', 'AdminProductOptionsController@index');
		Route::get('/product-options/create/', 'AdminProductOptionsController@create');
		Route::post('/product-options/save/', 'AdminProductOptionsController@store');
		Route::get('/product-options/edit/{id}', 'AdminProductOptionsController@edit');
		Route::post('/product-options/update/{id}', 'AdminProductOptionsController@update');
		Route::get('/product-options/delete/{id}', 'AdminProductOptionsController@destroy');


		/*brands*/
 		Route::get('/brands/', 'AdminBrandsController@index');
		Route::get('/brands/create/', 'AdminBrandsController@create');
		Route::post('/brands/save/', 'AdminBrandsController@store');
		Route::get('/brands/edit/{id}', 'AdminBrandsController@edit');
		Route::post('/brands/update/{id}', 'AdminBrandsController@update');
		Route::get('/brands/delete/{id}', 'AdminBrandsController@destroy');

		/*stocks*/
 		Route::get('/stocks/', 'AdminStocksController@index');
		Route::get('/stocks/create/', 'AdminStocksController@create');
		Route::post('/stocks/save/', 'AdminStocksController@store');
		Route::get('/stocks/edit/{id}', 'AdminStocksController@edit');
		Route::post('/stocks/update/{id}', 'AdminStocksController@update');
		Route::get('/stocks/delete/{id}', 'AdminStocksController@destroy');

		/*banners*/
 		Route::get('/banners/get/{collection}', 'AdminBannersController@index');
		Route::get('/banners/create/{collection}', 'AdminBannersController@create');
		Route::post('/banners/save/{collection}', 'AdminBannersController@store');
		Route::get('/banners/edit/{collection}/{id}', 'AdminBannersController@edit');
		Route::post('/banners/update/{collection}/{id}', 'AdminBannersController@update');
		Route::get('/banners/delete/{collection}/{id}', 'AdminBannersController@destroy');

		/*Pages*/
		Route::get('/groups/', 'AdminPagesController@getGroups');
		Route::get('/groups/add/', 'AdminPagesController@addGroup');
		Route::post('/groups/save/', 'AdminPagesController@saveGroup');
		Route::post('/groups/edit/{id}', 'AdminPagesController@editGroup');
		Route::get('/groups/delete/{id}', 'AdminPagesController@deleteGroup');

		Route::get('/pages/get/{gid}', 'AdminPagesController@index');
		Route::get('/pages/create/{gid}/{pid}', 'AdminPagesController@create');
		Route::post('/pages/save/', 'AdminPagesController@store');
		Route::get('/pages/edit/{id}', 'AdminPagesController@edit');
		Route::post('/pages/update/{id}', 'AdminPagesController@update');
		Route::get('/pages/delete/{id}', 'AdminPagesController@destroy');
		Route::post('/pages/update-tree/', 'AdminPagesController@updateTree');
        /*get all pages by group */
		Route::get('/pages/getall', 'AdminPagesController@getAll');
		
		Route::post('/pages/removeCollection/{id}', 'AdminPagesController@removeCollection');
		Route::post('/pages/addCollection/{id}', 'AdminPagesController@addCollection');
        Route::post('/pages/edit-visibility/{id}', 'AdminPagesController@updateVisibility');

		Route::post('/pages/uploadfiles/{id}', 'AdminPagesController@uploadFiles');
		Route::post('/pages/delete-file/{id}', 'AdminPagesController@deleteFile');
		Route::post('/pages/edit-file/{id}', 'AdminPagesController@editFile');
		Route::post('/pages/update-default-foto/{id}', 'AdminPagesController@updateDefaultFoto');       

		/*site settings*/
 		Route::get('/settings/get/{lang}/', 'AdminSettingsController@index');
 		Route::get('/settings/developer/{lang}/', 'AdminSettingsController@indexDeveloper');
		Route::get('/settings/create/', 'AdminSettingsController@create');
		Route::post('/settings/save/', 'AdminSettingsController@store');
		Route::post('/settings/edit/{id}', 'AdminSettingsController@edit');
		Route::get('/settings/edit-developer/{id}', 'AdminSettingsController@editDeveloper');
		Route::post('/settings/update/{id}', 'AdminSettingsController@update');
		Route::get('/settings/delete/{id}', 'AdminSettingsController@destroy');
		Route::post('/settings/reorder', 'AdminSettingsController@reOrder');

		/* Language Data*/
		Route::get('/language-data/get/{lang}', 'AdminLanguageDataController@index');
		Route::get('/language-data/create/', 'AdminLanguageDataController@create');
		Route::post('/language-data/save/', 'AdminLanguageDataController@store');
		Route::post('/language-data/edit/{id}', 'AdminLanguageDataController@edit');

		/* USSD*/
		Route::get('/ussd/get', 'AdminUssdController@index');
		Route::get('/ussd/create', 'AdminUssdController@create');
		Route::get('/ussd/edit/{id}', 'AdminUssdController@edit');
		Route::post('/ussd/save', 'AdminUssdController@store');
		Route::post('/ussd/update/{id}', 'AdminUssdController@update');

		/* Reports */
		Route::get('/reports', 'AdminReportsController@index');
		Route::get('/reports/{from}/{to}', 'AdminReportsController@index');


		/* newsletters */
		Route::get('/newsletters/edit/{id}', 'AdminNewsletterController@editList');
		Route::post('/newsletters/save/{id}', 'AdminNewsletterController@saveList');
		Route::get('/newsletters', 'AdminNewsletterController@index');
		Route::get('/newsletters/{from}/{to}', 'AdminNewsletterController@index');
		Route::post('/newsletters/backlist', 'AdminNewsletterController@blacklist');
		Route::get('/deleteblacklist/{id}', 'AdminNewsletterController@deleteblacklist');
		Route::get('/blacklist', 'AdminNewsletterController@showblacklist');



		/* Internationa Line */
		Route::get('/internationalline/add', 'AdminInternationalLineController@add');
		Route::post('/internationalline/save', 'AdminInternationalLineController@save');
		Route::get('/internationalline/edit/{id}', 'AdminInternationalLineController@edit');
		Route::post('/internationalline/update/{id}', 'AdminInternationalLineController@update');
		Route::get('/internationalline/{lang}', 'AdminInternationalLineController@index');
		Route::get('/internationalline/delete/{id}', 'AdminInternationalLineController@delete');


		/* Working Hour */
		Route::get('/workinghour', 'AdminWorkingHourController@index');
		Route::get('/workinghour/add', 'AdminWorkingHourController@add');
		Route::get('/workinghour/edit/{id}', 'AdminWorkingHourController@edit');
		Route::post('/workinghour/update/{id}', 'AdminWorkingHourController@update');
		Route::post('/workinghour/insert', 'AdminWorkingHourController@insert');
		Route::get('/workinghour/delete/{id}', 'AdminWorkingHourController@delete');







		Route::any('products/changecolor/{id}/{color}', 'AdminUploadFiles@changeImgColor');
		

		/*text converter*/
		Route::get('/text-converter/', function()
		{
		    return view('admin.textconverter');
		});

		/* admin users */

		Route::get('/admin-users', 'AdminCmsUserController@index');
		Route::get('/admin-users/create/', 'AdminCmsUserController@create');
		Route::post('/admin-users/save', 'AdminCmsUserController@save');
		Route::get('/admin-users/edit/{id}', 'AdminCmsUserController@edit');
		Route::post('/admin-users/update/{id}', 'AdminCmsUserController@update');
		Route::get('/admin-users/delete/{id}', 'AdminCmsUserController@delete');
		Route::post('/admin-users/add-role', 'AdminCmsUserController@addRole');
		Route::post('/admin-users/remove-role/{user_id}/{role_id}', 'AdminCmsUserController@removeRole');

    
		Route::get('/admin-user-roles', 'AdminRolesController@index');
		Route::get('/admin-user-roles/create/', 'AdminRolesController@create');
		Route::post('/admin-user-roles/save', 'AdminRolesController@save');
		Route::get('/admin-user-roles/edit/{id}', 'AdminRolesController@edit');
		Route::post('/admin-user-roles/update/{id}', 'AdminRolesController@update');
		Route::get('/admin-user-roles/delete/{id}', 'AdminRolesController@delete');

		Route::post('/admin-user-roles/add-permission/{id}', 'AdminRolesController@addPermision');
		Route::post('/admin-user-roles/remove-permission/{id}', 'AdminRolesController@removePermision');
    }


); /*cms groups*/


/*change routes to cms later! */

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


/*Route::get('cms/news/delete-collection/{id}', function($id)
{
    return  $id;
});*/
/*
Route::get('/', '\Site\Controllers\HomeController@index');

Route::get('{slug}/{params?}', 'site\PagesController@getPage')->where('params', '(.*)');*/





 