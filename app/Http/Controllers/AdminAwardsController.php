<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\File;

use App\Models\Awards;

class AdminAwardsController extends Controller {

    protected $model;

    public function __construct(Awards $award)
	{
		$this->middleware('auth');
	
		$this->model = $award;
	}

	/**
	 * Display a listing of the resource  by collection id.
	 *
	 * @return Response
	 */
	 
	public function index()
	{
		$awards = $this->model->all();	
		return view('admin.awards.awards',compact('awards'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	
		return view('admin.awards.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store(ArticlesRequest $request)
	{
        
        $pg = $this->model->createWithontent($request->all());
			 
		return redirect(config('app.cms_slug').'/awards/edit/'.$pg->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/* optimize later! */
	public function edit($id)
	{
		$award=$this->model->findOrFail($id);

        //dd($article);
    
		return view('admin.awards.update')->with('award',$award);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	/* optimize later! */

	public function update($id, ArticlesRequest $request)
	{

		$this->model->updateContent($id,$request->all());
        
        if(@$request->saveclose) {
       	  $url =config('app.cms_slug').'/awards/get/';
        }
        else  {
       		$url = config('app.cms_slug').'/awards/edit/'.$id;
        }
        return redirect($url);
        
	}
    
    /* optimize later ! */

    public function uploadFiles($colid,$id)
    {
    	
    	$file = \Input::file('file');
        
	    if($file->isValid()) {

	        $destinationPath = public_path().config('folders.articles');
	        $filename = $file->getClientOriginalName();

            $extension =$file->getClientOriginalExtension();
            
            if(file_exists($destinationPath.$filename))
            {
            	$rnd = rand(100,999); 
            	$filename=basename($filename, '.'.$extension).$rnd.'.'.$extension; 
            }
	      
     

	        $upload_success = $file->move($destinationPath, $filename);

	        if ($upload_success) {

	            \Image::make($destinationPath . $filename)->widen(200)->save($destinationPath . "thumbs/" . $filename);

	            $this->model->saveAttachedFile($id,$filename); 

	            return \Response::json('success', 200);

	        } else {
	            return \Response::json('error', 400);
	        }
	    }
    }

 	public function deleteFile($collid,$id){

 		$this->model->deleteAttachedFile($id);
 		 return \Response::json('success', 200);
 	}

 	public function editFile($collid,$id){
        $title= \Input::get('title');

        $this->model->editAttachedFile($id,$title);
 		
 		return \Response::json('success', 200);
 	}
    
    public function updateDefaultFoto($collid,$id){
        $input = \Input::all();

        $this->model->updateAttachedFile($id,$input);		

        $message = ($input['checked'])?trans('admin.addphoto') : trans('admin.removephoto');
        
 		return $message;
 	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$item= $this->model->findOrFail($id); 
		$item->delete();

		return \Redirect::back();
	}

}
