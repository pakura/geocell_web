<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\File;

use App\Models\Products;
use App\Models\Brands;
use App\Models\Stocks;

class AdminWorkingHourController extends Controller {
    protected $logins;
    protected $password_type;
    protected $activation_status;
    protected $services;

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('permissions');
    }

    public function index(){
        $lists = \DB::table('offices')->leftJoin('dictionary_cities', 'offices.city_id', '=', 'dictionary_cities.id')->select('offices.*', 'dictionary_cities.name_en')->get();
        return view('admin.workinghour.list', compact('lists'));
    }

    public function edit($id){
        $office = \DB::table('offices')->where('id', $id)->first();
        $cities = \DB::table('dictionary_cities')->get();
        return view('admin.workinghour.edit', compact('office', 'cities'));
    }

    public function delete($id){
        $office = \DB::table('offices')->where('id', $id)->delete();
        return redirect('cms/workinghour');
    }

    public function add(){
        $cities = \DB::table('dictionary_cities')->get();
        return view('admin.workinghour.add', compact('cities'));
    }

    public function update($id){
        $update = array(
            'address_en' => \Input::get('address_en'),
            'address_ge' => \Input::get('address_ge'),
            'city_id' => \Input::get('city_id'),
            'info_en' => \Input::get('info_en'),
            'info_ge' => \Input::get('info_ge'),
            'status' => \Input::get('status'),
            'shop' => \Input::get('shop'),
            'b2b' => \Input::get('b2b'),
            'coordinates' => \Input::get('coordinates'),
            'note_en' => \Input::get('note_en'),
            'note_ge' => \Input::get('note_ge')
        );

        $res = \DB::table('offices')->where('id', $id)->update($update);
        return redirect('cms/workinghour');
    }

    public function insert(){
        $update = array(
            'address_en' => \Input::get('address_en'),
            'address_ge' => \Input::get('address_ge'),
            'city_id' => \Input::get('city_id'),
            'info_en' => \Input::get('info_en'),
            'info_ge' => \Input::get('info_ge'),
            'status' => \Input::get('status'),
            'shop' => \Input::get('shop'),
            'b2b' => \Input::get('b2b'),
            'coordinates' => \Input::get('coordinates'),
            'note_en' => \Input::get('note_en'),
            'note_ge' => \Input::get('note_ge')
        );

        $res = \DB::table('offices')->insert($update);
        return redirect('cms/workinghour');
    }
}
