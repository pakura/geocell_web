<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminCmsUserRequest;
use App\Http\Requests\AdminCmsUserUpdateRequest;
use App\Models\CmsUsers;
use App\Models\CmsUserRoles;

class AdminCmsUserController extends Controller {
    
 

	public function __construct()
	{
 
        $this->middleware('auth');
		$this->middleware('adminrole');	
 
	}
    
    public function index()
    {
        
        $users = CmsUsers::get();
        return view('admin.admin_users.users',compact('users'));
    }

	public function create()
    {
        
        return view('admin.admin_users.create');
    }
    
    public function save(CmsUsers $user,AdminCmsUserRequest $request)
    {
        
        $saved = $user->create($request->all());
        return redirect(config('app.cms_slug').'/admin-users/edit/'.$saved->id);
    }
     
    public function edit($id,CmsUsers $users)
    {
        $user = $users->find($id);
        $roles = CmsUserRoles::all()->lists('role_name','id');
        return view('admin.admin_users.update',compact('user','roles'));
    }

    public function update($id,CmsUsers $users,AdminCmsUserUpdateRequest $request)
    { 
        
        $users->find($id)->update($request->all()); 

        if(@$request->saveclose) {
       	  $url =config('app.cms_slug').'/admin-users/';
       }
       else  {
       		$url = config('app.cms_slug').'/admin-users/edit/'.$id;
       }
       return redirect($url);
    }
    
    public function addRole(CmsUsers $users){
        $user = $users->find(\Request::get('user_id'));
        $user->roles()->attach(\Request::get('role_id'));
 
        return view('admin.admin_users.roles.list')->with('userRoles',$user->roles)
                                                   ->with('userid',\Request::get('user_id'));
    }  

    public function removeRole($user_id,$role_id){
        $user = CmsUsers::find($user_id);
        $user->roles()->detach($role_id);
    }  

    public function delete($id)
    {
        
        CmsUsers::destroy($id);
        return redirect(config('app.cms_slug').'/admin-users/');
    }

}
