<?php namespace App\Http\Controllers;

use App\Http\Requests\RolesRequest;
use App\Models\CmsUserRoles;
use App\Models\Collections;

class AdminRolesController extends Controller {

	 

	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('adminrole');   
		 
	}

	public function index()
	{
		$roles = CmsUserRoles::get();
        return view('admin.admin_users.roles.roles',compact('roles'));
	}

	public function create()
	{
		return view('admin.admin_users.roles.create');
	}

	public function save(CmsUserRoles $role,RolesRequest $request)
    {
        
        $saved = $role->create($request->all());
        return redirect(config('app.cms_slug').'/admin-user-roles/');
    }
     
    public function edit($id,CmsUserRoles $roles)
    {
        $role = $roles->find($id);
        $pagesGroups = \DB::table('pages_groups')->get();
        $pages = \App\Models\Pages::where('parent_id','=',0)->get()->groupby('group_id');
        $permissions = $roles->getRolePermissions($id); 
        $collections = Collections::getAll();

        return view('admin.admin_users.roles.update',compact('role','collections','pagesGroups','pages','permissions'));
    }

    public function update($id,CmsUserRoles $role,RolesRequest $request)
    {
        
        $role->find($id)->update($request->all()); 
   

        /*\DB::table('role_pages')->insert( [ 
            'role_id'=>$id,
            'page_id'=>$request->page_id,
            'collection_id'=>$request->page_id,
            ]
        );*/
        return redirect(config('app.cms_slug').'/admin-user-roles/edit/'.$id);
    }
    
    public function addPermision($role_id,CmsUserRoles $role){
        
        $fields = \Input::all();
       
        $role ->savePermission($role_id,$fields);
         
        return trans('admin.updated');
        
    }
    public function removePermision($role_id,CmsUserRoles $role){
        
        $fields = \Input::all();

        $role ->removePermission($role_id,$fields);
        
        return trans('admin.updated');
        
    }


    public function delete($id)
    {
        
        CmsUserRoles::destroy($id);
        return redirect(config('app.cms_slug').'/admin-user-roles/');
    }
 
		
	 
	

}
