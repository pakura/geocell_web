<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller; 

use App\Models\LanguageData;
 
class AdminLanguageDataController extends Controller {

    protected $model;
   
    public function __construct(LanguageData $model)
	{
		$this->middleware('auth');
		$this->middleware('adminrole');	

		$this->model = $model;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index($lang)
	{
		
		$languagedata = $this->model->lang($lang)->get();
	
		return view('admin.languagedata.list',compact('languagedata'));
	}

	
	
	public function create()
	{

		return view('admin.languagedata.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
 
	public function store()
	{
 
		$fields = \Input::all();
        $data = array();
        //dd($fields);	
		foreach(config('app.langs') as $key => $value)
		{
		    $data[] = array(
		        'data_key' => $fields['data_key'],
		        'language' => $key,
		        'title' => $fields['title'][$key],
		        
		    );
		}
		  

       $this->model->insert($data);
    
	 
		return redirect(config('app.cms_slug').'/language-data/get/en');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Edit value. (ajax)
	 *
	 * @param  int  $id
	 * @return Response
	 */
 
	public function edit($id)
	{
		$this->model->find($id)->update(
                array(
                'title'=>\Input::get('generic_title')
                )
            );
 
	}


 
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->model->destroy($id);
	}

}
