<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Collections;

class AdminCollectionsController extends Controller {

	public static $tp;

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('collections');
		self::$tp = config('app.collection_types');
	}

	public function index()
	{
		
	}

	public function addCollection($type)
	{
		return view('admin/addcollection')->with('type',$type);
	}

	public function saveCollection($type)
	{
		Collections::$type=$type;
		Collections::add(\Input::get('generic_title'));
		return redirect(config('app.cms_slug').'/collections/get/'.$type);
		
	}
    
    /*edit collection title from ajax call*/
	public function editCollection($type, $id){
     
    	Collections::updateCollection($id);
    	//return redirect(config('app.cms_slug').'/collections/get/'.$type);
        
    }
	
	/*ajax request */
    public function getCollectionsList($type)
	{

		$collections = Collections::get($type);
		return $collections;
	}

	public function getCollections($type)
	{

		$collections = Collections::get($type);

		return view('admin/collections')->with('collections',$collections)->with('type',$type);
	}
    
    public function deleteCollection($type, $id){
     
    	Collections::destroy($id);
    	return redirect(config('app.cms_slug').'/collections/get/'.$type);
        
    }
		
	 
	

}
