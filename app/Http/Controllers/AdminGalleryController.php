<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\File;

use App\Models\Gallery;
use App\Models\Collections;

class AdminGalleryController extends Controller {

    protected $contentTable='galleries_content';
    public static $collections;

    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('permissions');
		self::$collections = Collections::getForSelectList(8);
	}
      
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	/*get galleries by collection id*/
	public function index($cid)
	{

		$galleries = Gallery::where('collection_id', '=', $cid)
							->join('galleries_content as c','c.galleries_id','=','galleries.id')
							->select('galleries.*','c.file','c.title_file')
							->groupBy('galleries_id')
							->orderBy('galleries.position')
							->get();

		$collection = Collections::find($cid);
		return view('admin.galleries.galleries',compact('galleries','collection'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($colid)
	{
		$collection =Collections::find($colid);
		return view('admin.galleries.create',compact('galleries','collection'))->with('collections',self::$collections);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	/* optimize later! */
	public function store($colid,ArticlesRequest $request,Gallery $gallery)
	{
        
        $pg = $gallery->create($request->all());
        $id = $pg->id; 

        $insert = array();
        
		foreach(config('app.langs') as $key => $value)
		{
		    $insert[] = array(
		        'galleries_id' => $id,
		        'language' => $key
		    );
		}

        \DB::table($this->contentTable)->insert($insert); 
			 
		return redirect(config('app.cms_slug').'/gallery/edit/'.$request->collection_id.'/'.$id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/* optimize later! */
	public function edit($colid,$id)
	{
		$gallery =Gallery::where('galleries.collection_id','=',$colid)
			->join('collections as c','galleries.collection_id','=','c.id')
			->select('galleries.*','c.id as cid','c.generic_title as ctitle')
			->findOrFail($id);

        
        $titles=[]; 
        $short_title=[];
        $description=[];
        $file=[];
        $title_file=[];
        $link=[];

        /*Get galleries content*/

        $cont = \DB::table($this->contentTable)->where('galleries_id','=',$gallery['id'])->get(); 
        
		for($i=0;$i<count($cont);$i++)
		{		   
             
		    $titles[$cont[$i]->language] = $cont[$i]->title;
		    $short_title[$cont[$i]->language] = $cont[$i]->short_title;
		    $description[$cont[$i]->language] = $cont[$i]->description;
		    $file[$cont[$i]->language] = $cont[$i]->file;
		    $title_file[$cont[$i]->language] = $cont[$i]->title_file;
		    $link[$cont[$i]->language] = $cont[$i]->link;
		    

		}
		
		$gallery['title'] =  $titles;
		$gallery['short_title'] = $short_title;
		$gallery['description'] = $description;
		$gallery['file'] = $file;
		$gallery['title_file'] = $title_file;
		$gallery['link'] = $link;

	    
		return view('admin.galleries.update',compact('gallery'))->with('collections',self::$collections);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	/* optimize later! */

	public function update($colid,$id, ArticlesRequest $request,Gallery $gallery)
	{
		
		$gallery->find($id)->update($request->all()); 
        
		foreach(config('app.langs') as $key => $value)
		{
		    $update = array(
		        
		        'title' => $request->title[$key],
		        'short_title' => $request->short_title[$key],
		        'description' => $request->description[$key],
		        'file'        => $request->file[$key],
		        'title_file'  => $request->title_file[$key],
		        'link'        => $request->link[$key],
		    );

		  
		    \DB::table($this->contentTable)->where('galleries_id', '=', $id)->where('language','=',$key)->update($update);


		}
        
       if(@$request->saveclose) {
       	  $url =config('app.cms_slug').'/gallery/get/'.$request->collection_id;
       }
       else  {
       		$url = config('app.cms_slug').'/gallery/edit/'.$request->collection_id.'/'.$id;
       }
       return redirect($url);
        
	}
   

    public function uploadFiles($gallery_collection_id)
    {
    	$base_url =  \URL::to('/');
    	$file = \Input::file('file');
        
	    if($file->isValid()) {

	        $destinationPath = public_path().config('folders.gallery');
	        $filename = $file->getClientOriginalName();

            $extension =$file->getClientOriginalExtension();
            
            if(file_exists($destinationPath.$filename))
            {
            	$rnd = rand(100,999); 
            	$filename=basename($filename, '.'.$extension).$rnd.'.'.$extension; 
            }
	      
     

	        $upload_success = $file->move($destinationPath, $filename);

	        if ($upload_success) {

	        	/* full address of the image */
	        	$fileUrl = $base_url.config('folders.gallery').$filename;

	            //\Image::make($destinationPath . $filename)->widen(200)->save($destinationPath . "thumbs/" . $filename);

	            $id= Gallery::create(
	            				array(
										'collection_id'=> $gallery_collection_id,
										 'galleries_type'=>1
										)
	            				);

	            foreach(config('app.langs') as $key => $value)
					{
					    $insert[] = array(
					        'galleries_id' => $id->id,
					        'language' => $key,
					        'file'=>$fileUrl
					    );
					}

			        \DB::table($this->contentTable)->insert($insert);

	            return \Response::json('success', 200);
	           
	        } else {
	            return \Response::json('error', 400);
	        }
	    }
	    else { return \Response::json('error', 400); }
    }

    public function reOrder(){
    	$json = \Input::get('itemsOrder');
    	Gallery::reOrderItems($json);
    	return trans('admin.updated');
    }
 
 	public function deleteFile($id){


 	}

 	public function editFile($id){
        
 	}
    
    public function updateDefaultFoto($id){
        
 	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($colid,$id,Gallery $gallery)
	{
		$item= $gallery->where('collection_id','=',$colid)->findOrFail($id); 
		$item->delete();
		return \Redirect::back();
	}

}
