<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\File;

use App\Models\Products;
use App\Models\Brands;
use App\Models\Stocks;

class AdminProductsController extends Controller {

    protected $model;

    public function __construct(Products $product)
	{
		$this->middleware('auth');
		$this->middleware('permissions');

		$this->model = $product;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index($cid)
	{
		$products = $this->model->getByCollection($cid)->get();
		$collection = $this->model->collection($cid);
		$colors = \DB::table('products_option_list')->where('generic_title', 'like', 'color%')->groupBy('color')->select('color', 'generic_title')->get();
		return view('admin.products.products',compact('products','collection', 'colors'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($colid, Brands $brand,Stocks $stock)
	{
		$collection = $this->model->collection($colid);
		$brands = $brand->lists('generic_title', 'id');
		$stocks = $stock->lists('generic_title', 'id');
		$colors = \DB::table('products_option_list')->where('generic_title', 'like', 'color%')->groupBy('color')->select('color', 'generic_title')->get();
		return view('admin.products.create',compact('collection','brands','stocks', 'colors'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */


	public function store($colid,ArticlesRequest $request)
	{

        $pg = $this->model->createWithontent($request->all());

		$this->model->addProductOptions($pg->id,$colid);
		return redirect(config('app.cms_slug').'/products/edit/'.$pg->collection_id.'/'.$pg->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/* optimize later! */
	public function edit($colid,$id, Brands $brand,Stocks $stock)
	{
		$product =Products::where('products.collection_id','=',$colid)
			->join('collections as c','products.collection_id','=','c.id')
			->select('products.*','c.id as cid','c.generic_title as ctitle')
			->findOrFail($id);


		$product['prices'] = Products::getProductPrices($product['id']);
		$product['stocks'] = Products::getProductInStocksList($product['id']);

 		$product['stocks_ids'] = Products::getProductStockIds($product['stocks']);

		$brands = $brand->lists('generic_title', 'id');
		$stocks = $stock->lists('generic_title', 'id');
		$options = Products::getProductOptions($id);
		$colors = \DB::table('products_option_list')->where('color', 'like', '#%')->groupBy('color')->select('color', 'generic_title')->get();
		return view('admin.products.update', compact('product','brands','stocks','options', 'colors'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	/* optimize later! */

	public function update($colid,$id, ArticlesRequest $request)
	{

		$this->model->updateContent($id,$request->all());

       if(@$request->saveclose) {
       	  $url =config('app.cms_slug').'/products/get/'.$request->collection_id;
       }
       else  {
       		$url = config('app.cms_slug').'/products/edit/'.$request->collection_id.'/'.$id;
       }
       return redirect($url);

	}

    public function saveProductOptionValue($id){

    	$this->model->saveProductOptionVal($id,\Input::get('option_value'),\Input::get('sku_code'));
    	return trans('admin.updated');
    }

    /* add  product price */

    public function addProductPrice(){
    	$fields = \Input::all();
    	$id = Products::addPrice($fields);
    	return self::getProductPrice($id);
    }
    /* product prices list (ajax)*/
    public function getProductPrice($product_id){
    	$prices =  Products::getProductPrice($product_id);
    	return view('admin.products.prices',compact('prices'));
    }

	public function removeProductPrice($id){
    	\DB::table('products_prices')->where('id','=',$id)->delete();
    }

    /* add product to stock */
    public function addProductInStock(){
    	$fields = \Input::all();
    	Products::addInStock($fields);
    	return self::getProductInStocks($fields['product_id']);
    }

    /* product in stocks list (ajax) */
    public function getProductInStocks($product_id){
    	$product_stocks =  Products::getProductInStocksList($product_id);
    	return view('admin.products.stocks',compact('product_stocks'));
    }

    /* remove product from stock */
	public function removeProductFromStock($id){
    	\DB::table('products_in_stock')->where('id','=',$id)->delete();
    }



    public function uploadFiles($id)
    {

    	$file = \Input::file('file');

	    if($file->isValid()) {

	        $destinationPath = public_path().'/'.config('folders.products');
	        $filename = str_replace(' ', '-', $file->getClientOriginalName());

            $extension =$file->getClientOriginalExtension();

            if(file_exists($destinationPath.$filename))
            {
            	$rnd = rand(100,999);
            	$filename=basename($filename, '.'.$extension).$rnd.'.'.$extension;
            }



	        $upload_success = $file->move($destinationPath, $filename);

	        if ($upload_success) {

	            \Image::make($destinationPath . $filename)->widen(200)->save($destinationPath . "thumbs/" . $filename);

	             $this->model->saveAttachedFile($id,$filename);

	            return \Response::json('success', 200);
	        } else {
	            return \Response::json('error', 400);
	        }
	    }
    }

 	public function deleteFile($id){

 		$this->model->deleteAttachedFile($id);

 		return \Response::json('success', 200);
 	}

 	public function editFile($id){
        $title= \Input::get('title');

 		$this->model->editAttachedFile($id,$title);
 		 return \Response::json('success', 200);
 	}

    public function updateDefaultFoto($id){
        $input = \Input::all();

        $fileType = ($input['checked'])?1 : 0;
        $message = ($input['checked'])?trans('admin.addphoto') : trans('admin.removephoto');

 		 $this->model->updateAttachedFile($id,$input);
 		 return $message;
 	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($collid,$id)
	{
		$item= $this->model->where('collection_id','=',$collid)->findOrFail($id);
		$item->delete();
		return \Redirect::back();
	}


	public function preorderList(){
	    $orders = \DB::table('pre-order')->leftJoin('dictionary_cities', 'dictionary_cities.id', '=', 'pre-order.city')
            ->leftJoin('offices', 'offices.id', '=', 'pre-order.office')
            ->select('pre-order.*', 'dictionary_cities.name_en', 'offices.address_en')->get();
        return view('admin.pre-order.list',compact('orders'));
    }

    public function stockList(){
        $stocks = \DB::table('stock')->get();
        return view('admin.stock.list',compact('stocks'));

    }

    public function updateStock(){
        $id = \Input::get('id');
        $quant = intval(\Input::get('quant'));
        $res = \DB::table('stock')->where('id', $id)->update(['quantities' => $quant]);
        echo 'quantities has been updated';
    }

    public function changeAction(){
        $id = \Input::get('id');
        $action = \Input::get('action');
        $res = \DB::table('pre-order')->where('id', $id)->update(['delivered' => $action]);
        echo 'Delivery Status has been updated';
    }

}
