<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\File;
use App\Models\Pages;
use App\Models\collections;

class AdminPagesController extends Controller {

    protected $model;

	public function __construct(Pages $page)
	{
		$this->middleware('auth');
		$this->model=$page;
	}

	public function index($group_id)
	{
		$pages = Pages::getList($group_id);
		$group = self::getGroup($group_id);
		return view('admin.pages.pages',compact('pages','group'));
	}

    public function create($group_id,$parent_id=0)
	{
		$group = self::getGroup($group_id);
		$controllers = self::controllersList();
		return view('admin.pages.create',compact('group','parent_id','controllers'));
	}

    public function getGroup($id){
    	return \DB::table('pages_groups')->where('id','=',$id)->first();
    }
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	/* optimize later! */

	public function store(ArticlesRequest $request)
	{
        $types = config('app.collection_types');
        if(array_key_exists($request->page_type,$types) && !$request->attached_collection_id){
        	Collections::$type = $request->page_type;
        	$coll= Collections::add($types[$request->page_type].' list - '.$request->generic_title);

        	$request->offsetSet('attached_collection_id', $coll->id);
        }

		$parentSlug = Pages::getParentSlug($request->parent_id);

		$fullSlug=$parentSlug?$parentSlug.'/'.$request->slug:$request->slug;

        $request->offsetSet('full_slug', $fullSlug);

        $pg = $this->model->createWithontent($request->all());

		return redirect(config('app.cms_slug').'/pages/edit/'.$pg->id);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/* optimize later! */
	public function edit($id)
	{
		$page =Pages::where('pages.id','=',$id)
			->join('pages_groups as g','pages.group_id','=','g.id')
			->select('pages.*','g.id as gid','g.generic_title as gtitle')
			->first();


	    if($page->attached_collection_id) {
	    	$collections = \App\Models\Collections::whereCollectionType($page->page_type)->lists('generic_title','id');

	    }
	    $controllers = self::controllersList();

		return view('admin.pages.update',compact('page','controllers','collections'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	/* optimize later! */

	public function update($id, ArticlesRequest $request)
	{
        $types = config('app.collection_types');

        if(array_key_exists($request->page_type,$types) && !$request->attached_collection_id){
        	Collections::$type = $request->page_type;
        	$coll= Collections::add($types[$request->page_type].' list - '.$request->generic_title);

        	$request->offsetSet('attached_collection_id', $coll->id);
        }

        $parentSlug = Pages::getParentSlug($request->parent_id);

        $fullSlug=$parentSlug?$parentSlug.'/'.$request->slug:$request->slug;

        $request->offsetSet('full_slug',$fullSlug);

        $this->model->updateContent($id,$request->all());


       if(@$request->saveclose) {
       	  $url =config('app.cms_slug').'/pages/get/'.$request->group_id;
       }
       else  {
       		$url = config('app.cms_slug').'/pages/edit/'.$id;
       }
       return redirect($url);

	}


    public function uploadFiles($id)
    {

    	$file = \Input::file('file');

	    if($file->isValid()) {

	        $destinationPath = public_path().'/'.config('folders.pages');
	        $filename = str_replace(' ', '-', $file->getClientOriginalName());

            $extension =$file->getClientOriginalExtension();

            if(file_exists($destinationPath.$filename))
            {
            	$rnd = rand(100,999);
            	$filename=basename($filename, '.'.$extension).$rnd.'.'.$extension;
            }



	        $upload_success = $file->move($destinationPath, $filename);

	        if ($upload_success) {

	            \Image::make($destinationPath . $filename)->widen(200)->save($destinationPath . "thumbs/" . $filename);

	             $this->model->saveAttachedFile($id,$filename);

	            return \Response::json('success', 200);
	        } else {
	            return \Response::json('error', 400);
	        }
	    }
    }

 	public function deleteFile($id){

 		$this->model->deleteAttachedFile($id);
 		 return \Response::json('success', 200);
 	}

 	public function editFile($id){
        $title= \Input::get('title');

 		 $this->model->editAttachedFile($id,$title);
 		 return \Response::json('success', 200);
 	}

    public function updateDefaultFoto($id){
        $input = \Input::all();

        $this->model->updateAttachedFile($id,$input);

        $message = ($input['checked'])?trans('admin.addphoto') : trans('admin.removephoto');

 		return $message;
 	}

	public function updateVisibility($id){
		Pages::where('id','=',$id)->update(
                array(
                'visibility'=>\Input::get('visibility')
                )
            );

		return trans('admin.updated');
	}

	/* remove collection from page */
	public function removeCollection($id){
		Pages::removeColl($id);
		return trans('admin.updated');
	}
	public function addCollection($id){
		Pages::addColl($id,\Input::all());
		return trans('admin.updated');
	}

	public function updateTree(){
		$json = \Input::get('jsn');
		Pages::updatePagesTree($json);


	}

	public function addGroup()
	{
		return view('admin.pages.addgroup');
	}

	public function editGroup($id)
	{
		\DB::table('pages_groups')->where('id','=',$id)->update(
                array(
                'generic_title'=>$_POST['generic_title']
                )
            );
	}

	public function saveGroup()
	{
		\DB::table('pages_groups')->insert(array(
        	'generic_title'=>$_POST['generic_title'],
        	));
		return redirect('cms/groups');
	}

	public function getGroups()
	{

		$groups = \DB::table('pages_groups')->get();
		return view('admin.pages.groups')->with('groups',$groups);
	}

    public function deleteGroup($id){
    	\DB::table('pages_groups')->where('id','=',$id)->delete();
        return redirect('cms/groups');
    }

    public function destroy($id)
	{
		$page = $this->model->find($id);

		if(!$page->notDeletable) {
			$page->delete();
		}
		return \Redirect::back();
	}

	/* get all menu list for attaching collections (ajax call) */
	public function getAll(){

		 $groups = \DB::table('pages_groups')->get();
         $pages = Pages::all()->groupby('group_id');

		return view('admin.pages.pages-list-ajax',compact('pages','groups'));

	}

	/*Controllers list */

	public function controllersList()
	{

	    $files = File::files(base_path().'/site/Controllers');

	    $names=[];
	    foreach($files as $file)
	    {

	        $names[File::name($file)] =File::name($file);

	    }

	    return $names;
	}



}
