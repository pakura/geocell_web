<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\File;

use App\Models\Products;
use App\Models\Brands;
use App\Models\Stocks;

class AdminInternationalLineController extends Controller {
    protected $logins;
    protected $password_type;
    protected $activation_status;
    protected $services;

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('permissions');
    }

    public function index($lang){
        $lists = \DB::table('international_line')->where('language', $lang)->get();
        return view('admin.international.list', compact('lists'));
    }

    public function edit($id){
        $list = \DB::table('international_line')->where('id', $id)->first();
        return view('admin.international.edit', compact('list', 'id'));
    }

    public function add(){
        return view('admin.international.add');
    }

    public function update($id){
        $title = \Input::get('country');
        $price_no = \Input::get('price_nodiscount');
        $price_all = \Input::get('price_discount');
        $group = \Input::get('group');
        $discount = \Input::get('discount');

        $code = \Input::get('code');
        $res = \DB::table('international_line')->where('id', $id)->update([
            'title' => $title,
            'gel_min' => $price_all,
            'gel_min_nodiscount' => $price_no,
            'country_group' => $group,
            'letter' => strtoupper(mb_substr($title, 0, 1)),
            'has_discount' => $discount,
            'country_code' => $code
        ]);
        return redirect('https://geocell.ge/developer_version/public/cms/internationalline/en');
    }


    public function save(){
        $title = \Input::get('country');
        $price_no = \Input::get('price_nodiscount');
        $price_all = \Input::get('price_discount');
        $group = \Input::get('group');
        $language = \Input::get('language');
        $discount = \Input::get('discount');

        $code = \Input::get('code');
        $res = \DB::table('international_line')->insert([
            'title' => $title,
            'gel_min' => $price_all,
            'gel_min_nodiscount' => $price_no,
            'country_group' => $group,
            'language' => $language,
            'letter' => strtoupper(mb_substr($title, 0, 1)),
            'has_discount' => $discount,
            'country_code' => $code
        ]);
        return redirect('https://geocell.ge/developer_version/public/cms/internationalline/en');
    }

    public function delete($id){
        $res = \DB::table('international_line')->where('id', $id)->delete();
        return redirect('https://geocell.ge/developer_version/public/cms/internationalline/en');
    }

}
