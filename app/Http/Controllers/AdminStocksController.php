<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use App\Models\Stocks;

class AdminStocksController extends Controller {

    protected $model;
   
    public function __construct(Stocks $stock)
	{
		$this->middleware('auth');
		$this->middleware('productpages');

		$this->model = $stock;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$stocks = Stocks::all();
		 
		return view('admin.stocks.stocks',compact('stocks'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		return view('admin.stocks.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	/* optimize later! */
	public function store(ArticlesRequest $request)
	{
      
       $pg = $this->model->createWithontent($request->all());	 

		return redirect(config('app.cms_slug').'/stocks/edit/'.$pg->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function edit($id)
	{
		$stock =$this->model->find($id);
		 
		return view('admin.stocks.update')->with('stock',$stock);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */


	public function update($id, ArticlesRequest $request)
	{
	
   		$this->model->updateContent($id,$request->all());
   		
       	if(@$request->saveclose) {
       	  $url =config('app.cms_slug').'/stocks/';
       	}
        else  {
       		$url = config('app.cms_slug').'/stocks/edit/'.$id;
        }
      	return redirect($url);
        
	}
    
 

 
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Stocks::destroy($id);
		return \Redirect::back();
	}

}
