<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use App\Models\Products;
use App\Models\Brands;
use App\Models\Stocks;

class AdminNewsletterController extends Controller {
    protected $logins;
    protected $password_type;
    protected $activation_status;
    protected $services;

    public function __construct(){
        $this->middleware('auth');
        $this->middleware('permissions');
    }

    public function index($from = null, $to = null){
        if(!isset($from)){
            $from = '2016-05-01';
        }
        if(!isset($to)){
            $to = '2032-05-01';
        }
        $res = \DB::table('email_sub')->whereBetween('autodate', array($from, $to))->get();
        return view('admin.newsletter.list', with([
            'data' => $res,
            'from' => $from,
            'to' => $to
        ]));
    }

    public function editList($id){
        $res = \DB::table('email_sub')->where('id', $id)->first();
        $blacklist = \DB::table('blacklisted_mails')->where('email', $res->email)->first();
        if(isset($blacklist)){
            $res->blacklist = true;
        } else {
            $res->blacklist = false;
        }
        return view('admin.newsletter.edit')->with('data', $res);
    }

    public function saveList($id){
        $note = \Input::get('note');
        $remarks = \Input::get('remark');
        $blacklist = \Input::get('blacklisted');

        $res = \DB::table('email_sub')->where('id', $id)->update(['note' => $note, 'remarks' => $remarks]);
        $res = \DB::table('email_sub')->where('id', $id)->first();
        if(isset($blacklist)){
            $blacklisted = \DB::table('blacklisted_mails')->where('email', $res->email)->first();
            if(!isset($blacklisted)){
                $blacklisted = \DB::table('blacklisted_mails')->insert(['email' => $res->email]);
                $res = \DB::table('email_sub')->where('id', $id)->update(['black_list' => 1 ]);

            }
        } else {
            $blacklisted = \DB::table('blacklisted_mails')->where('email', $res->email)->first();
            if(isset($blacklisted)){
                $blacklisted = \DB::table('blacklisted_mails')->where('email', $res->email)->delete();
                $res = \DB::table('email_sub')->where('id', $id)->update(['black_list' => 0 ]);
            }
        }
        return redirect('https://geocell.ge/developer_version/public/cms/newsletters');
    }


    public function blacklist(){
        $file = Input::file('file');
        $destinationPath = 'uploads/blacklists';
        $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
        $fileName = rand(1111111,9999999).'.'.$extension; // renameing image
        Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
        \Session::flash('success', 'Upload successfully');
        $file = 'uploads/blacklists/'.$fileName;
        $content = File::get($file);
        header('Content-Type: text/html; charset=utf-8');
        $content = substr($content, 2, -1);
        $content = explode("\n", $content);
        $res = $this->updateBlacklist($content);
        unlink($file);
        return redirect('https://geocell.ge/developer_version/public/cms/blacklist');
    }

    private function updateBlacklist($lists){
//        dd($lists);
        $res = \DB::table('email_sub')->whereIn('email', $lists)->update(['black_list' => 1]);
        foreach($lists as $key => $mail){
            $res = \DB::table('blacklisted_mails')->where('email', $mail)->first();
            if(!isset($res)){
                $res = \DB::table('blacklisted_mails')->insert([ 'email' => $mail ]);
            }
        }
        return $res;
    }

    public function showblacklist(){
        $res = \DB::table('blacklisted_mails')->get();
        return view('admin.newsletter.blacklist')->with('data', $res);
    }

    public function deleteblacklist($id){
        $res = \DB::table('blacklisted_mails')->where('id', $id)->first();
        $delete = \DB::table('blacklisted_mails')->where('id', $id)->delete();
        $black = \DB::table('email_sub')->where('email', $res->email)->update(['black_list' => 0]);

        if(!isset($res)){
            return redirect('https://geocell.ge/developer_version/public/cms/blacklist');
        } else {
            return redirect('https://geocell.ge/developer_version/public/cms/blacklist');
        }
    }

}
