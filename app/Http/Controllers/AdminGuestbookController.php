<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use App\Models\Guestbook;
use App\Models\Collections;

class AdminGuestbookController extends Controller {

    protected $contentTable='guestbook_content';
    protected $model;
   
    public function __construct(Guestbook $guestbook)
	{
		$this->middleware('auth');
		$this->middleware('permissions');
		$this->model = $guestbook;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	/*get faq by collection id*/
	public function index($colid)
	{

		$all = $this->model->where('collection_id', '=', $colid)->get();
		$collection = Collections::find($colid);
		return view('admin.guestbook.guestbook',compact('all','collection'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	  public function create($colid)
	{
		$collection = Collections::find($colid);
		$collections = Collections::getForSelectList('13');
		
		return view('admin.guestbook.create',compact('collection','collections'));
	}  

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	/* optimize later! */
	 public function store($colid,ArticlesRequest $request)
	{
        $pg = $this->model->create($request->all());
        $id = $pg->id; 
     
	    $insert = array(
	        'guestbook_id' => $id,
	    );
		

        \DB::table($this->contentTable)->insert($insert); 
			 
		return redirect(config('app.cms_slug').'/guestbook/edit/'.$id);
	}  

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/* optimize later! */
	public function edit($colid,$id)
	{
		$guestbook = $this->model->where('guestbook.collection_id','=',$colid)
					->join('collections as c','guestbook.collection_id','=','c.id')
					->select('guestbook.*','c.id as cid','c.generic_title as ctitle')
					->findOrFail($id);

        // dd($faq);

        /*Get faq content*/

        $cont = \DB::table($this->contentTable)->where('guestbook_id','=',$guestbook->id)->first(); 

		$guestbook['content'] =  $cont;
		 
		 
	    $collections = Collections::getForSelectList('13'); 

	     
		return view('admin.guestbook.update',compact('guestbook','collections'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	/* optimize later! */

	public function update($colid,$id, ArticlesRequest $request)
	{
		$this->model->find($id)->update($request->all()); 
        
 	    $update = array(
		        
		        'name' => $request->name,
		        'email' => $request->email,
		        'title' => $request->title,
		        'content' => $request->content,
		        
		    );
		 \DB::table($this->contentTable)->where('guestbook_id', '=', $id)->update($update);
 
        
       if(@$request->saveclose) {
       	  $url =config('app.cms_slug').'/guestbook/get/'.$request->collection_id;
       }
       else  {
       		$url = config('app.cms_slug').'/guestbook/edit/'.$request->collection_id.'/'.$id;
       }
       return redirect($url);
        
	}
    


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($colid,$id)
	{   

		$item= $this->model->where('collection_id','=',$colid)->findOrFail($id); 
		$item->delete();
		return \Redirect::back();

	}

}
