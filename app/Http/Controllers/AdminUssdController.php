<?php namespace App\Http\Controllers;

use App\Http\Requests\UssdRequest;
use App\Http\Controllers\Controller;

use App\Models\Ussd;

class AdminUssdController extends Controller {

    protected $model;

    public function __construct(Ussd $ussd)
	{
		$this->middleware('auth');
	
		$this->model = $ussd;
	}

	/**
	 * Display a listing of the resource  by collection id.
	 *
	 * @return Response
	 */
	 
	public function index()
	{
		$ussds = $this->model->all();	
		return view('admin.ussd.list',compact('ussds'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	
		return view('admin.ussd.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store(UssdRequest $request)
	{
        
        $pg = $this->model->insert($request->all());
			 
		return redirect(config('app.cms_slug').'/ussd/edit/'.$pg->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/* optimize later! */
	public function edit($id)
	{
		$ussd=$this->model->findOrFail($id);

        //dd($article);
    
		return view('admin.ussd.update')->with('ussd',$ussd);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	/* optimize later! */

	public function update($id, UssdRequest $request)
	{

		$this->model->find($id)->update($request->all());
        
        if(@$request->saveclose) {
       	  $url =config('app.cms_slug').'/ussd/get/';
        }
        else  {
       		$url = config('app.cms_slug').'/ussd/edit/'.$id;
        }
        return redirect($url);
        
	}
    
     
  

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$item= $this->model->findOrFail($id); 
		$item->delete();

		return \Redirect::back();
	}

}
