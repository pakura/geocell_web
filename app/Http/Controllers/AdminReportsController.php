<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\File;

use App\Models\Products;
use App\Models\Brands;
use App\Models\Stocks;

class AdminReportsController extends Controller {
    protected $logins;
    protected $password_type;
    protected $activation_status;
    protected $services;

    public function __construct()
    {
        $this->services['S_INTERNET_15']=0;
        $this->services['S_INTERNET_19']=0;
        $this->services['S_INTERNET_26']=0;
        $this->services['S_INTERNET_35']=0;
        $this->services['S_INTERNET_40']=0;
        $this->services['S_INTERNET_44']=0;
        $this->services['S_INTERNET_45']=0;
        $this->services['S_INTERNET_6']=0;
        $this->services['S_METI_M']=0;
        $this->services['S_METI_M_PERIODIC']=0;
        $this->services['S_METI_S']=0;
        $this->services['S_METI_S_PERIODIC']=0;
        $this->services['S_METI_L']=0;
        $this->services['S_METI_U']=0;
        $this->services['S_METI_U_PERIODIC']=0;
        $this->services['S_METI_L_PERIODIC']=0;
        $this->services['S_SERVICE_110']=0;
        $this->services['S_SMARTPHONE_PACKAGE']=0;
        $this->services['S_SMS_1']=0;
        $this->services['S_SMS_2']=0;
        $this->services['S_SMS_DAYNIGHT']=0;
        $this->services['S_SOCIAL_NETWORK_PACKAGE']=0;
        $this->services['S_TARIFF_120']=0;
        $this->services['S_TARIFF_GLOBUS']=0;
        $this->services['S_TRIPLE_ZERO']=0;
        $this->services['S_UNLIMITED_FACEBOOK']=0;
        $this->services['S_UNLIMITED_ODNOKLASNIKI']=0;
        $this->services['S_UNLIMITED_OPERA_MINI']=0;
        $this->services['S_UNLIMITED_SMS']=0;
        $this->services['S_ROAMING_10MB']=0;
        $this->services['S_ROAMING_SERVICE']=0;
        $this->services['S_CLIP']=0;
        $this->services['S_CLIR']=0;
        $this->services['S_ROAMING_150MB']=0;
        $this->services['S_ROAMING_50MB']=0;
        $this->services['S_INTERNET_15_PERIODIC']=0;
        $this->services['S_INTERNET_19_PERIODIC']=0;
        $this->services['S_INTERNET_26_PERIODIC']=0;
        $this->services['S_INTERNET_35_PERIODIC']=0;
        $this->services['S_INTERNET_40_PERIODIC']=0;
        $this->services['S_INTERNET_6_PERIODIC']=0;
        $this->services['SO_CLIP']=0;
        $this->services['S_MMS']=0;
        $this->services['S_BALANCE_ON_OFF']=0;
        $this->services['S_BEEP']=0;
        $this->services['S_INTERNET_15_TWICE']=0;
        $this->services['S_INTERNET_15_TWICE']=0;
        $this->services['S_MISSED_CALL']=0;
        $this->services['S_INTERNATIONAL_DISCOUNT']=0;
    }


    public function index($from = null, $to = null){
        $this->collectData($from, $to);
//        dd($this->services);
        return view('admin.reports.view', with([
            'logins' => $this->logins,
            'pasType' => $this->password_type,
            'activation_status' => $this->activation_status,
            'services' => $this->services,
            'from' => $from,
            'to' => $to
        ]));
    }

    public function filter($from, $to){
        $this->collectData($from, $to);
        $returnData = array();
        $returnData['logins'] = $this->logins;
        $returnData['password_type'] = $this->password_type;
        $returnData['activation_status'] = $this->activation_status;
        $returnData['services'] = $this->services;
        return json_encode($returnData);
    }

    private function collectData($from = null, $to = null){
        if ($from == null || $to == null){
            $from = date('Y-m').'-01 00:00:00';
            $to = date('Y-m-d').' 23:59:59';
        }
        $from .= ' 00:00:00';
        $to .= ' 23:59:59';
        $this->logins = \DB::table('login_log')->selectRaw('count(*) as cnt')->whereBetween('autodate', array($from, $to))->groupBy('mobile')->get();
        $this->password_type = \DB::table('users_pass_type')->selectRaw('count(*) as cnt, type')->whereBetween('autodate', array($from, $to))->orderBy('autodate', 'desc')->groupBy('mobile', 'type')->get(); //whereBetween('autodate', array($from, $to))->
        $typecnt = array('sms' => 0, 'pass' => 0);
        foreach ($this->password_type as $pass){
            if ($pass->type == 0){
                $typecnt['sms']++;
            } else {
                $typecnt['pass']++;
            }
        }
        $this->password_type = $typecnt;
        $this->activation_status = \DB::table('service_request_log')->selectRaw('count(*) as cnt, result_code')->where('service_name', '=', 'portalActivateService')->whereBetween('created_at', array($from, $to))->groupBy('result_code')->get();

        $this->service = \DB::table('service_request_log')->select('request')->where(['service_name' => 'portalActivateService', 'result_code' => 1])->whereBetween('created_at', array($from, $to))->get();
        $serviceData = array();
        foreach ($this->service as $key => $serv){
            $servName = json_decode($serv->request)->portalActivateService->arg5;

            if (!isset($this->services[$servName])){
                $this->services[$servName] = 0;
            }
            $this->services[$servName]++;
        }


    }

}
