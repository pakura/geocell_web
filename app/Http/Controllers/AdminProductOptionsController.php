<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;
 
use App\Models\Collections;
use App\Models\ProductOptions;
 
class AdminProductOptionsController extends Controller {

   protected $model;
   
    public function __construct(ProductOptions $option)
	{
		$this->middleware('auth');
		$this->middleware('productpages');
		$this->model = $option;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		
		$options = ProductOptions::all();
	
		return view('admin.products.options.options',compact('options'));
	}

 
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		$collections = Collections::getForSelectList('4');
		return view('admin.products.options.create',compact('collections'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
 
	public function store(ArticlesRequest $request)
	{
 
		$pg = $this->model->createWithontent($request->all());
 
		$this->model->savePolCollections($pg->id,$request->all());	
		$this->model->addToProductsOptions($pg->id,$request->get('collection_id'));	
 
		return redirect(config('app.cms_slug').'/product-options/edit/'.$pg->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Edit value.
	 *
	 * @param  int  $id
	 * @return Response
	 */
 
	public function edit($id)
	{
	
        $option = $this->model->find($id);
        $collections = Collections::getForSelectList('4');
        /*list of collection which belongs to this product option*/
        $collections_ids =$option->getPolCollections($id);
		return view('admin.products.options.update',compact('option','collections','collections_ids'));
 
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

 

	public function update($id,ArticlesRequest $request)
	{
		 
        $this->model->updateContent($id,$request->all());
        $this->model->savePolCollections($id,$request->all());	
        
       /// $this->model->addToProductsOptions($id,$request->get('collection_id'));	

        if(@$request->saveclose) {
       	  $url =config('app.cms_slug').'/product-options/';
        }
        else  {
       		$url = config('app.cms_slug').'/product-options/edit/'.$id;
        }
        return redirect($url);
	}
    

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		ProductOptions::destroy($id);
		return \Redirect::back();
	}

}
