<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\File;

class AdminUploadFiles extends Controller {

    protected $attachTable='';

    public function __construct()
	{
		$this->middleware('auth');
	}



    public function uploadFiles($id)
    {
    	
    	$file = \Input::file('file');
        
	    if($file->isValid()) {

	        $destinationPath = public_path().config('folders.articles');
	        $filename = $file->getClientOriginalName();

            $extension =$file->getClientOriginalExtension();
            
            if(file_exists($destinationPath.$filename))
            {
            	$rnd = rand(100,999); 
            	$filename=basename($filename, '.'.$extension).$rnd.'.'.$extension; 
            }
	      
     

	        $upload_success = $file->move($destinationPath, $filename);

	        if ($upload_success) {

	            \Image::make($destinationPath . $filename)->widen(200)->save($destinationPath . "thumbs/" . $filename);

	            \DB::table($this->attachTable)->insert(
	            										array(
	            											'article_id'=> $id,
	            											'file'=>$filename
	            											)			
	            									);
	            return \Response::json('success', 200);
	        } else {
	            return \Response::json('error', 400);
	        }
	    }
    }

 	public function deleteFile($id){

 		\DB::table($this->attachTable)->where('id','=',$id)->delete();
 		 return \Response::json('success', 200);
 	}

 	public function editFile($id){
        $title= \Input::get('title');

 		\DB::table($this->attachTable)->where('id','=',$id)->update(array('title' => $title));
 		 return \Response::json('success', 200);
 	}
    
    public function updateDefaultFoto($id){
        $input = \Input::all();

        $fileType = ($input['checked'])?1 : 0;
        $message = ($input['checked'])?trans('admin.addphoto') : trans('admin.removephoto');
         
 		\DB::table($this->attachTable)->where('article_id','=',$input['relid'])->update(array('file_type' =>2));
 		\DB::table($this->attachTable)->where('id','=',$id)->update(array('file_type' => $fileType));
 		 return $message;
 	}

	public function changeImgColor($id, $color){
		$imgRow = \DB::table('products_attachments')->where('id', '=', $id)->update(array('color' => $color));
		if (!$imgRow){
			return 'error';
		} else {
			return 'ok';
		}
	}

}
