<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use App\Models\Brands;

class AdminBrandsController extends Controller {

    protected $model;
    public function __construct(Brands $brand)
	{
		$this->middleware('auth');
		$this->middleware('productpages');
		$this->model = $brand;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$brands = $this->model->all();
		 
		return view('admin.brands.brands',compact('brands'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		
		return view('admin.brands.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	/* optimize later! */
	public function store(ArticlesRequest $request)
	{

        $pg = $this->model->createWithontent($request->all());
	 
		return redirect(config('app.cms_slug').'/brands/edit/'.$pg->id);
	}
	/**
	 * Display according collection.
	 *
	 * @param  int  $id
	 * @return Response
	 */
 
	 
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/* optimize later! */
	public function edit($id)
	{
		$brand =$this->model->find($id);
		 
		return view('admin.brands.update',compact('brand'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function update($id, ArticlesRequest $request)
	{
		
   		$this->model->updateContent($id,$request->all());
        
       if(@$request->saveclose) {
       	  $url =config('app.cms_slug').'/brands/';
       }
       else  {
       		$url = config('app.cms_slug').'/brands/edit/'.$id;
       }
       return redirect($url);
        
	}

 
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$item= $this->model->findOrFail($id); 
		$item->delete();

		return \Redirect::back();
	}

}
