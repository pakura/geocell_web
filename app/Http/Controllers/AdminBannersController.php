<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use App\Models\Banners;
use App\Models\Collections;

class AdminBannersController extends Controller {

    protected $model;
   
    public function __construct(Banners $banner)
	{
		$this->middleware('auth');
		$this->model = $banner;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($collid)
	{
		$banners = $this->model->getByCollection($collid)->get();
		$collection = $this->model->collection($collid);
		 
		return view('admin.banners.banners',compact('banners','collection'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($collid)
	{
		$collection = Collections::find($collid);
		$collections = Collections::getForSelectList('16');
		return view('admin.banners.create',compact('collection','collections'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	/* optimize later! */
	public function store($collid,ArticlesRequest $request)
	{
       
       $pg = $this->model->createWithontent($request->all());
			 
		return redirect(config('app.cms_slug').'/banners/edit/'.$request->collection_id.'/'.$pg->id);
	}
	/**
	 * Display according collection.
	 *
	 * @param  int  $id
	 * @return Response
	 */
 
	 
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/* optimize later! */
	public function edit($collid,$id)
	{
		$banner = Banners::where('banners.collection_id','=',$collid)
			->join('collections as c','banners.collection_id','=','c.id')
			->select('banners.*','c.id as cid','c.generic_title as ctitle')
			->findOrFail($id);
		 
        $collections = Collections::getForSelectList('16'); 
		 
		return view('admin.banners.update',compact('banner','collections'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	/* optimize later! */

	public function update($collid,$id, ArticlesRequest $request)
	{	  
      
   		$this->model->updateContent($id,$request->all());
        
       if(@$request->saveclose) {
       	  $url =config('app.cms_slug').'/banners/get/'.$request->collection_id;
       }
       else  {
       		$url = config('app.cms_slug').'/banners/edit/'.$request->collection_id.'/'.$id;
       }
       return redirect($url);
        
	}
    

 
 
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($collid,$id)
	{
		$item= $this->model->getByCollection($collid)->findOrFail($id); 
		$item->delete();
		return \Redirect::back();
	}

}
