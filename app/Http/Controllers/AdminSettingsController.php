<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;
 

use App\Models\Settings;
 
class AdminSettingsController extends Controller {

   
   
    public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('adminrole');	
		
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index($lang)
	{
		
		$settings = Settings::where('language','=',$lang)->orderBy('position')->get();
	
		return view('admin.settings.settings',compact('settings'));
	}

	/**
	 * Display a listing of the resource for developer.
	 *
	 * @return Response
	 */

	public function indexDeveloper($lang)
	{

		$settings = Settings::where('language','=',$lang)->orderBy('position')->get();
	
		return view('admin.settings.settings-developer',compact('settings'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		return view('admin.settings.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
 
	public function store(Settings $setting)
	{
 
		$fields = \Input::all();
        $data = array();
        //dd($fields);	
		foreach(config('app.langs') as $key => $value)
		{
		    $data[] = array(
		        'settings_key' => $fields['settings_key'],
		        'type' => $fields['type'],
		        'language' => $key,
		        'title' => $fields['title'][$key],
		        'initial' => $fields['initial'][$key],
		    );
		}
		  

       Settings::insert($data);
    
	 
		return redirect(config('app.cms_slug').'/settings/developer/'.config('app.locale'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Edit value.
	 *
	 * @param  int  $id
	 * @return Response
	 */
 
	public function edit($id)
	{
		Settings::where('id','=',$id)->update(
                array(
                'value'=>\Input::get('generic_title')
                )
            );
 
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
 
	public function editDeveloper($id)
	{
		
		$setting = Settings::find($id);
        
		return view('admin.settings.update',compact('setting'));
 
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

 

	public function update($id, Settings $setting)
	{
		 
        $fields = \Input::all();
        
        $setting->find($id)->update($fields);
        if(@$fields['saveclose']) {
       	  $url =config('app.cms_slug').'/settings/developer/'.config('app.locale');
        }
        else  {
       		$url = config('app.cms_slug').'/settings/edit-developer/'.$id;
        }
        return redirect($url);
	}
    
     public function reOrder(){

    	Settings::reOrderItems(\Input::get('itemsOrder'));
    	return trans('admin.updated');
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Settings::destroy($id);
	}

}
