<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\File;

use App\Models\CustomLists;
use App\Models\Collections;

class AdminCustomListsController extends Controller {

    protected $model;

    public function __construct(CustomLists $model)
	{
		$this->middleware('auth');
		$this->middleware('permissions');

		$this->model = $model;
	}

	/**
	 * Display a listing of the resource  by collection id.
	 *
	 * @return Response
	 */
	 
	public function index($cid)
	{
 
		$articles = $this->model->getByCollection($cid)->get();

		$collection = $this->model->collection($cid);

		return view('admin.custom_lists.list',compact('articles','collection'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($colid)
	{
		$collection = $this->model->collection($colid);
		$collections = Collections::getForSelectList('15');
		return view('admin.custom_lists.create',compact('collection','collections'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store($colid,ArticlesRequest $request)
	{
        
        $pg = $this->model->createWithontent($request->all());
			 
		return redirect(config('app.cms_slug').'/custom_lists/edit/'.$pg->collection_id.'/'.$pg->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/* optimize later! */
	public function edit($colid,$id)
	{
		$article=$this->model->where('custom_lists.collection_id','=',$colid)
			    ->join('collections as c','custom_lists.collection_id','=','c.id')
				->select('custom_lists.*','c.id as cid','c.generic_title as ctitle')
				->findOrFail($id);

        //dd($article);
    
		return view('admin.custom_lists.update')->with('article',$article);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	/* optimize later! */

	public function update($colid,$id, ArticlesRequest $request)
	{

		$this->model->updateContent($id,$request->all());
        
        if(@$request->saveclose) {
       	  $url =config('app.cms_slug').'/custom_lists/get/'.$request->collection_id;
        }
        else  {
       		$url = config('app.cms_slug').'/custom_lists/edit/'.$request->collection_id.'/'.$id;
        }
        return redirect($url);
        
	}
    
    /* optimize later ! */

    public function uploadFiles($colid,$id)
    {
    	
    	$file = \Input::file('file');
        
	    if($file->isValid()) {

	        $destinationPath = public_path().config('folders.custom_lists');
	        $filename = $file->getClientOriginalName();

            $extension =$file->getClientOriginalExtension();
            
            if(file_exists($destinationPath.$filename))
            {
            	$rnd = rand(100,999); 
            	$filename=basename($filename, '.'.$extension).$rnd.'.'.$extension; 
            }
	      
     

	        $upload_success = $file->move($destinationPath, $filename);

	        if ($upload_success) {

	            \Image::make($destinationPath . $filename)->widen(200)->save($destinationPath . "thumbs/" . $filename);

	            $this->model->saveAttachedFile($id,$filename); 

	            return \Response::json('success', 200);

	        } else {
	            return \Response::json('error', 400);
	        }
	    }
    }

 	public function deleteFile($collid,$id){

 		$this->model->deleteAttachedFile($id);
 		 return \Response::json('success', 200);
 	}

 	public function editFile($collid,$id){
        $title= \Input::get('title');

        $this->model->editAttachedFile($id,$title);
 		
 		return \Response::json('success', 200);
 	}
    
    public function updateDefaultFoto($collid,$id){
        $input = \Input::all();

        $this->model->updateAttachedFile($id,$input);		

        $message = ($input['checked'])?trans('admin.addphoto') : trans('admin.removephoto');
        
 		return $message;
 	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($collid,$id)
	{
		$item= $this->model->getByCollection($collid)->findOrFail($id); 
		$item->delete();

		return \Redirect::back();
	}

}
