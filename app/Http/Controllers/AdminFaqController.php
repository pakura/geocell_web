<?php namespace App\Http\Controllers;

use App\Http\Requests\ArticlesRequest;
use App\Http\Controllers\Controller;
 

use App\Models\Faq;
use App\Models\Collections;

class AdminFaqController extends Controller {

    protected $model;
   
    public function __construct(Faq $faq)
	{
		$this->middleware('auth');
		$this->middleware('permissions');
		$this->model = $faq;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 
	public function index($cid)
	{

		$faqs = $this->model->getByCollection($cid)->get();
		$collection = $this->model->collection($cid);
		return view('admin.faq.faq',compact('faqs','collection'));
	}
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($colid)
	{
		$collection = Collections::find($colid);
		$collections = Collections::getForSelectList('12');
		
		return view('admin.faq.create',compact('collection','collections'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */

	public function store($collid,ArticlesRequest $request)
	{
        
        $pg = $this->model->createWithontent($request->all());
		return redirect(config('app.cms_slug').'/faq/edit/'.$request->collection_id.'/'.$pg->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/* optimize later! */
	public function edit($colid,$id)
	{
		$faq =Faq::where('faq.collection_id','=',$colid)
			->join('collections as c','faq.collection_id','=','c.id')
			->select('faq.*','c.id as cid','c.generic_title as ctitle')
			->findOrFail($id);

        // dd($faq);
       
	    $collections = Collections::getForSelectList('12'); 
		return view('admin.faq.update',compact('faq','collections'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function update($collid,$id, ArticlesRequest $request)
	{
 		
 	    $this->model->updateContent($id,$request->all());
        
       if(@$request->saveclose) {
       	  $url =config('app.cms_slug').'/faq/get/'.$request->collection_id;
       }
       else  {
       		$url = config('app.cms_slug').'/faq/edit/'.$request->collection_id.'/'.$id;
       }
       return redirect($url);
        
	}
    


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($collid,$id)
	{
		$item= $this->model->getByCollection($collid)->findOrFail($id); 
		$item->delete();
		return \Redirect::back();
	}

}
