<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArticlesRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return \Auth::check();
		
	}
 
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'generic_title' => 'required',
		];
	}

	public  function makeSlug($slug)
		{ 
		 	/*str_slug($slug, '-');*/
		 	$slug = strtolower($slug);
			$slug = preg_replace("/[^a-zA-Z0-9]+/", "-", $slug);  
			$slug = trim($slug, "-");
			$slug = '/'.$slug;
			$this->getInputSource()->set("slug", $slug);

			return $slug;
		}

	public function all(){
		$fields = parent::all();
		
		if(isset($fields['creation_date'])){
			$fields['creation_date'] = date("Y-m-d H:m", strtotime($fields['creation_date'].' '.$fields['creation_date_time']));
		}
		if(isset($fields['expire_date'])){
			$fields['expire_date'] = date("Y-m-d H:m", strtotime($fields['expire_date'].' '.$fields['expire_date_time']));
		}
			
		if(isset($fields['visibility'])){
			$fields['visibility'] = 1;
		}
		else{ $fields['visibility'] = 0; }

		if(isset($fields['pinned'])){
			$fields['pinned'] = 1;
		}
		else{ $fields['pinned'] = 0; }

		if(isset($fields['home_page'])){
			$fields['home_page'] = 1;
		}
		else{ $fields['home_page'] = 0; }

		$slug = (isset($fields['slug']))? $fields['slug']:$fields['generic_title']; 
		$fields['slug'] = str_slug('/'.$slug, '-');

		return $fields;
	} 

}
