<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class SiteUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return \Session::has('phone');
		
	}
 
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'personal_id' => 'unique',
		];
	}

	 
	public function all(){

		$fields = parent::all();
		$fields['birth_date'] = date("Y-m-d H:i:s", strtotime($fields['year'].'-'.$fields['month'].'-'.$fields['day'].' 01:00:00'));

			
		return $fields;
	} 

}
