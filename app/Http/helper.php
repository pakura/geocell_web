<?php

Class Helper {
	    
	public static function has_children($pages,$id) {
	  foreach ($pages as $page) {
	    if ($page->parent_id == $id)
	      return true;
	  }
	  return false;
	}

	public static function pageType($index){
		$types = config('app.page_types'); 
		return $types[$index];
	}
}

function drawSubMenuList($items,$type){

	$html='';
    if(!is_array($items)) { return ''; } 
	foreach ($items as $item) {
		if(Auth::user()->hasPermission($item->id) || Auth::user()->hasPermissionToCollection(6) ) { 
	
			$html.=	'<li data-id="'.$item->id.'">
						<a href="'.config('app.cms_slug').'/'.$type.'/get/'.$item->id.'">
							<span class="title">'.$item->generic_title.'</span>
						</a>
					</li>';
	
    	}
	}
	return $html;
 
}


function convertBalance($b) {
	
	if(\App::getLocale()=='ge') {
		$bm["0"] = " ბ";
		$bm["1"] = " კბ";
		$bm["2"] = " მბ";
		$bm["3"] = " გბ";
	}  else {
		$bm["0"] = " B";
		$bm["1"] = " KB";
		$bm["2"] = " MB";
		$bm["3"] = " GB";
	}
	$t = $bm[0];
	if($b<1024) {
		$b1 = $b;
		$t = $bm[0];
	}
	if($b>=1024 && $b < 1024 * 1024) {
		$b1 = $b / 1024;
		$t = $bm[1];
	}

	if($b>=1024 * 1024 && $b < 1024 * 1024 * 1024) {
		$b1 = $b / 1024 / 1024;
		$t = $bm[2];
	}

	if($b>=1024 * 1024 * 1024) {
		$b1 = $b / 1024 / 1024 / 1024;
		$t = $bm[3];
	}
	
	return ($b>0 ? number_format(floatval($b1), 2).$t : 0);
}

function convertMinutes($amt){
	if($amt>600000 || $amt==-1){
	   $txt = trans('site.unlimited');
	}
    else{
       $mins = floor($amt / 60);
       $secs = $amt % 60;  	
	   //$txt = number_format((($amt * 100 /60) / 100), 2);
       $txt = $mins.' '.trans('site.minutes').($secs>0?' '.$secs.' '.trans('site.seconds'):'');
    }   
	return $txt;
 }


 function convertDate($dt){
 	if(!$dt) { return;}
 	$dt = str_replace('/', '-',$dt);
 	$month = date('m', strtotime($dt) );
 	return date('d', strtotime($dt)).' '.array_get(trans('site.months'),$month-1).' '.date('Y', strtotime($dt) );
 }

 function convertDateTime($dt){
 	if(!$dt) { return;}
 	$dt = str_replace('/', '-',$dt);
 	$month = date('m', strtotime($dt) );
 	return date('d', strtotime($dt)).' '.array_get(trans('site.months'),$month-1).' '.date('Y H:i', strtotime($dt) );
 }

 function instalmentDate($dt, $br=false){
 	if(!$dt) { return;}
	 if ($br == true){
		 $br = '<br>';
	 } else {
		 $br = ' ';
	 }
 	$dt = str_replace('/', '-',$dt);
 	$month = date('m', strtotime($dt) );
 	return date('d', strtotime($dt)).$br.array_get(trans('site.months'),$month-1);
 }

function getUrlForSwitchLang(){
	$params =  \Request::segments();

	$str ='';
	for ($i=1; $i < count($params); $i++) { 
		  $str.='/'.$params[$i];
	}
	 
	return $str;
}

function checkExpDate($dt) {
	$time_now = time();
	$dtToCheck = strtotime(str_replace('/', '-',$dt));
 
	if($dtToCheck>$time_now) {
		return true;
	}
	return false;
}

function ddDeveloper($data){
   if($_SERVER['REMOTE_ADDR']==config('app.developer_ip')){
   		dd($data);
   }
}