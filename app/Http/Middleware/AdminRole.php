<?php namespace App\Http\Middleware;

use Closure;

class AdminRole
{
    /**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		// if user is admin let dig inside

		if($request->user()->type==2) {
			return $next($request);
		}
		 		 

        /* return 401 error for json request */ 
		if ( $request->isJson() || $request->wantsJson() ) {
            return response()->json([
                'error' => [
                    'status_code' => 401,
                    'code'        => 'INSUFFICIENT_PERMISSIONS',
                    'description' => 'You  don\'t have permission to access this resource.'
                ],
            ], 401);
        }

        return abort(401, 'You don\'t have permission to access this resource.');
		//return redirect(config('app.cms_slug'));

	}


}
