<?php namespace App\Http\Middleware;

use Site\Models\GeocellId;

class GeocellIdMiddleware {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $model;

	/**
	 * Create a new filter instance.
	 *
	 * @return void
	 */
	public function __construct(GeocellId $model)
	{
		$this->model = $model;
	}
     
     
	/**
	 * Handle an incoming request.
	 *
	 */
	public function handle()
	{
       

        if(\Session::has('site_auth_token')) {



			$auth = $this->model->_getStatus(\Session::get('site_auth_token'));



			if ($auth->success!=0)
			{
				$this->model->forgetUserSessionData();
			}

			
        }
        else {

        	$this->model->forgetUserSessionData();
        }

		
	}

	

}
