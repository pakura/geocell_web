<?php namespace App\Http\Middleware;

use Closure;

class CollectionsPermissions
{
    /**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		
		$hasperm = $this->checkPermission($request);
		// if sufficient permission let user to continue 
     
		if($hasperm)
		{
			return $next($request);
		}
		 
        /* return 401 error for json request */ 
		if ( $request->isJson() || $request->wantsJson() ) {
            return response()->json([
                'error' => [
                    'status_code' => 401,
                    'code'        => 'INSUFFICIENT_PERMISSIONS',
                    'description' => 'You  don\'t have permission to access this resource.'
                ],
            ], 401);
        }

        return abort(401, 'You don\'t have permission to access this resource.');
		//return redirect(config('app.cms_slug'));

	}


	private function checkPermission($request) {

		//get collection type
		$type = $request->route()->getParameter('type');
		$id = $request->route()->getParameter('id');

		$action = $request->segment(3);
        
        if($action=='get'){

        	// check user has permission to any page of collection
        	$perm = $request->user()->hasPermission($type); 


        }
        else {

        	if($id) {  $perm = $request->user()->hasPermission($id);  }
        	
        	else {

	        	// check user has permission to access collection
	        	$perm = $request->user()->hasPermissionToCollection($type); 

	        }	
        }
        
        return $perm;

	}


}
