<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Illuminate\Contracts\Routing\Middleware;

class Language implements Middleware {

	public function __construct(Application $app, Redirector $redirector, Request $request) {
		$this->app = $app;
		$this->redirector = $redirector;
		$this->request = $request;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		// Make sure current locale exists.
		 
		$locale = $request->segment(1);
		$default_lang = config('app.fallback_locale');

    
		if ( ($locale && (! array_key_exists($locale,config('app.locales')) &&  $locale!= config('app.cms_slug') && $locale!= 'auth' ) )    ) {
			$segments = implode('/', $request->segments());
			
			return $this->redirector->to($default_lang.'/'.$segments);
		}

		if(!$locale || $locale== config('app.cms_slug') || $locale== 'auth') {
			$this->app->setLocale($default_lang);
		}
		else { 
			$this->app->setLocale($locale);
		}	
		$lang = $this->app->getLocale();
		
		$translate = \App\Models\LanguageData::lang($lang)->lists('title','data_key');
       
        view()->share('translate', $translate);

	    view()->share('lang', $lang);
		
		return $next($request);
	}

}
