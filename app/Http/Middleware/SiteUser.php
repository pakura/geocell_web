<?php namespace App\Http\Middleware;

use Closure;
use Site\Models\GeocellId;

class SiteUser {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $model;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(GeocellId $model)
	{
		$this->model = $model;
	}
     
     
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
       

        if(\Session::has('site_auth_token')) {



			$auth = $this->model->_getStatus(\Session::get('site_auth_token'));



			if ($auth->success==0)
			{
				if ($request->ajax())
				{
					return view('site.lightbox.auth-sign-in');
				}
				else
				{
					return view('site.user.auth-sign-in');
				}
			}

			return $next($request);
        }
        else {

        	if ($request->ajax())
				{
					return view('site.lightbox.auth-sign-in');
				}
				else
				{
					return view('site.user.auth-sign-in');
				}
        }

		
	}

	

}
