<?php namespace App\Http\Middleware;

use Closure;

class Permissions
{
    /**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		// if user is admin let dig inside

		if($request->user()->type==2) {
			return $next($request);
		}
		 		 

		$userPermissions = $request->user()->Permissions;
        
		$perm = $this->checkIfUserHasPermissionsForRoute($request,$userPermissions);
         
		
		// if sufficient permission let user to continue 
     
		if($perm)
		{
			return $next($request);
		}
		 
        /* return 401 error for json request */ 
		if ( $request->isJson() || $request->wantsJson() ) {
            return response()->json([
                'error' => [
                    'status_code' => 401,
                    'code'        => 'INSUFFICIENT_PERMISSIONS',
                    'description' => 'You  don\'t have permission to access this resource.'
                ],
            ], 401);
        }

        return abort(401, 'You don\'t have permission to access this resource.');
		//return redirect(config('app.cms_slug'));

	}

	private function checkIfUserHasPermissionsForRoute($request,$userPermissions)
	{
		$action = $request->segment(2);

        if(in_array($action, config('app.collection_types'))) {
        	
        	if($request->route()->hasParameter('collection')){
        		/*check if user has access to all collectons */
        		if( isset($userPermissions['-1']) && $userPermissions['-1']>1 ) {
        			return true;
        		}

        		/*check if user has access to collecton type group */

        		$coltype = array_search($action, config('app.collection_types'));
        		$coltype = -$coltype;

        		if(isset($userPermissions[$coltype])  && $userPermissions[$coltype]>1){
        			return true;
        		}	

        		$perm = $request->route()->getParameter('collection');
        		
        		if( isset($userPermissions[$perm]) && $userPermissions[$perm]>1) {
        			return true;
        		}

        		return false;
        	}

        	return false;

        }

       return false;
	} 
}
