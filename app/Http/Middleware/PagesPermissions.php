<?php namespace App\Http\Middleware;

use Closure;

class PagesPermissions
{
    /**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		// if user is admin let dig inside

		if($request->user()->type==2) {
			return $next($request);
		}
		 		 

		$userPermissions = $request->user()->rolePermissions();
       // dd($userHasPermissions);
		$perm = $this->checkIfUserHasPermissionsForRoute($request,$userPermissions);
         
		
		// if sufficient permission let user to continue 
     
		if($perm)
		{
			return $next($request);
		}
		 
        /* return 401 error for json request */ 
		if ( $request->isJson() || $request->wantsJson() ) {
            return response()->json([
                'error' => [
                    'status_code' => 401,
                    'code'        => 'INSUFFICIENT_PERMISSIONS',
                    'description' => 'You  don\'t have permission to access this resource.'
                ],
            ], 401);
        }

        return abort(401, 'You don\'t have permission to access this resource.');
		//return redirect(config('app.cms_slug'));

	}

	private function checkIfUserHasPermissionsForRoute($request,$userPermissions)
	{
		$action = $request->segment(2);

        $request->route()->hasParameter('coll_id');

       return false;
	} 
}
