<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Upload Folders
	|--------------------------------------------------------------------------
	|
	*/

    'articles'=>'/uploads/articles/',
    'pages'=>'uploads/pages/',
    'gallery'=>'/uploads/gallery/',
    'products'=>'uploads/products/',

	
];
